/*****************************************************************************
 *
 *	Module:			WriteLinks.c
 *	Description:	This module contains code for extracting the link 
 *                  lengths distribution.
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "Home.h"

/*---------------------------------------------------------------------------
 *
 *	Struct:			NodeData
 * 					Data structure for links info.
 *
 *-------------------------------------------------------------------------*/
typedef struct {
	int     domainID;
	int     index;
	double  bx, by, bz;
	double  nx, ny, nz;
} ArmData_t;

typedef struct {
	int       domainID;
	int       index;
	int       globalIndex;
	int       numArms;
	ArmData_t *arms;
	double    x, y, z;
	double    oldx, oldy, oldz;
} NodeData_t;

/*---------------------------------------------------------------------------
 *
 *	Function:		NumUniqueArmPlanes
 *	Description:	Count and return the number of unique glide planes 
 *					belonging to a node's arms.  
 *
 *-------------------------------------------------------------------------*/
int NumUniqueArmPlanes(NodeData_t *nodeData) {
	
		int		k, l, numplanes;
		real8	nxtmp, nytmp, nztmp, planes[100];
		real8	tol = 1e-5;
	
/*
 * 		Determine how many unique glide planes the node is on.
 */
		for (k = 0; k < nodeData->numArms; k++) {
			nxtmp = nodeData->arms[k].nx;
			nytmp = nodeData->arms[k].ny;
			nztmp = nodeData->arms[k].nz;
			Normalize(&nxtmp,&nytmp,&nztmp);
			if (k==0) {
				planes[0] = nxtmp;
				planes[1] = nytmp;
				planes[2] = nztmp;
				numplanes = 1;
			} else {
				for (l=0; l<numplanes; l++) {
					Orthogonalize(&nxtmp,&nytmp,&nztmp,
					              planes[3*l],planes[3*l+1],planes[3*l+2]);
				}
				
				if ((nxtmp*nxtmp+nytmp*nytmp+nztmp*nztmp)>tol) {
					Normalize(&nxtmp,&nytmp,&nztmp);
					planes[3*numplanes] = nxtmp;
					planes[3*numplanes+1] = nytmp;
					planes[3*numplanes+2] = nztmp;
					numplanes++;
				}
			}
		}
		
		return(numplanes);
}

#ifdef PARALLEL
/*---------------------------------------------------------------------------
 *
 *	Function:		PackSendRemNodes
 * 					Communication function for links data.
 *
 *-------------------------------------------------------------------------*/
void PackSendRemNodes(Home_t *home)
{
		int     j, k, indNodes, indBuff, sizeBuff[2];
		double  *outBuff;
		Node_t  *node;
		
		indNodes = 0;
		indBuff = 0;
		for (k = 0; k < home->newNodeKeyPtr; k++) {
			if ((node = home->nodeKeys[k]) == (Node_t *)NULL) continue;
			indBuff += 9 + node->numNbrs*8;
			indNodes++;
		}
		
		sizeBuff[0] = indNodes;
		sizeBuff[1] = indBuff;
		MPI_Send(sizeBuff, 2, MPI_INT, 0, 10, MPI_COMM_WORLD);
		
		if (indNodes == 0) return;
		
		outBuff = (double*)malloc(sizeof(double)*indBuff);
		
		indBuff = 0;
		for (k = 0; k < home->newNodeKeyPtr; k++) {
			if ((node = home->nodeKeys[k]) == (Node_t *)NULL) continue;
			
			outBuff[indBuff++] = (double)node->myTag.domainID;
			outBuff[indBuff++] = (double)node->myTag.index;
			outBuff[indBuff++] = (double)node->numNbrs;
			
			for (j = 0; j < node->numNbrs; j++) {
				outBuff[indBuff++] = (double)node->nbrTag[j].domainID;
				outBuff[indBuff++] = (double)node->nbrTag[j].index;
				
				outBuff[indBuff++] = node->burgX[j];
				outBuff[indBuff++] = node->burgY[j];
				outBuff[indBuff++] = node->burgZ[j];
				
				outBuff[indBuff++] = node->nx[j];
				outBuff[indBuff++] = node->ny[j];
				outBuff[indBuff++] = node->nz[j];
			}
			
			outBuff[indBuff++] = node->x;
			outBuff[indBuff++] = node->y;
			outBuff[indBuff++] = node->z;
				
			outBuff[indBuff++] = node->oldx;
			outBuff[indBuff++] = node->oldy;
			outBuff[indBuff++] = node->oldz;
		}
		
		MPI_Send(outBuff, indBuff, MPI_DOUBLE, 0, 11, MPI_COMM_WORLD);
		
		free(outBuff);
		
		return;
}
#endif

/*---------------------------------------------------------------------------
 *
 *	Function:		WriteLinksData
 *	Description:	Determine links data (parallel version)
 * 					Nicolas, 06/21/2017
 *
 *-------------------------------------------------------------------------*/
void WriteLinksData(Home_t *home, char *baseFileName) 
{
		int		i, j, k, m, n, numNbrs, arm, nextarm, nplanes;
		int     numArms, numNodes, maxNodes, numLinks;
		real8	bx, by, bz;
		real8	len, lenold, lendot, dA, Adot, Asgn, seglen, vavg, totalLength;
		real8	dx, dy, dz, dxold, dyold, dzold, dxavg, dyavg, dzavg;
		real8	dx1, dy1, dz1, dx2, dy2, dz2;
		real8	vX1, vY1, vZ1, vX2, vY2, vZ2;
		real8 	ex, ey, ez, hhx, hhy, hhz, nx, ny, nz;
		real8	dyad[3][3], localstrain[3][3], localeRate;
		Node_t	*node;
		Param_t *param;
		int     thisDomain, nLocalNodes, nGlobalNodes;
        char    fileName[128];
		FILE   *fp;
#ifdef PARALLEL
        MPI_Status status;
#endif

		param = home->param;
		thisDomain = home->myDomain;
		
/*
 *		Parallel transfers: send all segments to master
 */		
		NodeData_t  *allNodes;
		int         **tagMap;

		nLocalNodes = 0;
		for (k = 0; k < home->newNodeKeyPtr; k++) {
			if ((node = home->nodeKeys[k]) == (Node_t *)NULL) continue;
			nLocalNodes++;
		}
		
#ifdef PARALLEL		
		int nLocalTags[home->numDomains];
		int nGlobalTags[home->numDomains];
		for (i = 0; i < home->numDomains; i++) {
			nLocalTags[i] = 0;
		}
		nLocalTags[thisDomain] = home->newNodeKeyPtr;
		
		MPI_Reduce(&nLocalNodes, &nGlobalNodes, 1, MPI_INT, MPI_SUM,
                   0, MPI_COMM_WORLD);
		
		MPI_Reduce(&nLocalTags, &nGlobalTags, home->numDomains, MPI_INT, MPI_SUM,
                   0, MPI_COMM_WORLD);
#else
		int nGlobalTags[1];
		nGlobalTags[0] = home->newNodeKeyPtr;
		nGlobalNodes = nLocalNodes;
#endif       
        
        if (thisDomain == 0) {
			allNodes = (NodeData_t*)malloc(sizeof(NodeData_t)*nGlobalNodes);
			tagMap = (int**)malloc(sizeof(int*)*home->numDomains);
			for (i = 0; i < home->numDomains; i++) {
				tagMap[i] = (int*)malloc(sizeof(int)*nGlobalTags[i]);
			}
		}
        
#ifdef PARALLEL
		if (thisDomain == 0) {
			
			// Populate all nodes list with local nodes
			nLocalNodes = 0;
			for (k = 0; k < home->newNodeKeyPtr; k++) {
				if ((node = home->nodeKeys[k]) == (Node_t *)NULL) continue;
				
				allNodes[nLocalNodes].domainID    = thisDomain;
				allNodes[nLocalNodes].index       = node->myTag.index;
				allNodes[nLocalNodes].globalIndex = nLocalNodes;
				
				allNodes[nLocalNodes].numArms = node->numNbrs;
				allNodes[nLocalNodes].arms = (ArmData_t*)malloc(sizeof(ArmData_t)*node->numNbrs);
				for (j = 0; j < node->numNbrs; j++) {
					
					allNodes[nLocalNodes].arms[j].domainID = node->nbrTag[j].domainID;
					allNodes[nLocalNodes].arms[j].index    = node->nbrTag[j].index;
					
					allNodes[nLocalNodes].arms[j].bx = node->burgX[j];
					allNodes[nLocalNodes].arms[j].by = node->burgY[j];
					allNodes[nLocalNodes].arms[j].bz = node->burgZ[j];
					
					allNodes[nLocalNodes].arms[j].nx = node->nx[j];
					allNodes[nLocalNodes].arms[j].ny = node->ny[j];
					allNodes[nLocalNodes].arms[j].nz = node->nz[j];
				}
				
				allNodes[nLocalNodes].x = node->x;
				allNodes[nLocalNodes].y = node->y;
				allNodes[nLocalNodes].z = node->z;
				
				allNodes[nLocalNodes].oldx = node->oldx;
				allNodes[nLocalNodes].oldy = node->oldy;
				allNodes[nLocalNodes].oldz = node->oldz;
				
				tagMap[thisDomain][k] = nLocalNodes;
				
				nLocalNodes++;
			}
			
			// Receive remote nodes from other CPUs
			for (i = 1; i < home->numDomains; i++) {
				
				int sizeRemData[2];
				MPI_Recv(&sizeRemData, 2, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &status);
				int remDomain = status.MPI_SOURCE;
				
				if (sizeRemData[0] > 0) {
					
					double *inBuff = (double*)malloc(sizeof(double)*sizeRemData[1]);
					MPI_Recv(inBuff, sizeRemData[1], MPI_DOUBLE, remDomain, 11, MPI_COMM_WORLD, &status);
					
					int indBuff = 0;
					for (k = 0; k < sizeRemData[0]; k++) {
						
						int remDom = (int)inBuff[indBuff++];
						int remInd = (int)inBuff[indBuff++];
						
						if (remDom != remDomain) Fatal("Domain error in WriteLinksData");
						
						allNodes[nLocalNodes].domainID    = remDom;
						allNodes[nLocalNodes].index       = remInd;
						allNodes[nLocalNodes].globalIndex = nLocalNodes;
						
						int numArms = (int)inBuff[indBuff++];
						allNodes[nLocalNodes].numArms = numArms;
						allNodes[nLocalNodes].arms = (ArmData_t*)malloc(sizeof(ArmData_t)*numArms);
						for (j = 0; j < numArms; j++) {
							
							allNodes[nLocalNodes].arms[j].domainID = (int)inBuff[indBuff++];
							allNodes[nLocalNodes].arms[j].index    = (int)inBuff[indBuff++];
							
							allNodes[nLocalNodes].arms[j].bx = inBuff[indBuff++];
							allNodes[nLocalNodes].arms[j].by = inBuff[indBuff++];
							allNodes[nLocalNodes].arms[j].bz = inBuff[indBuff++];
							
							allNodes[nLocalNodes].arms[j].nx = inBuff[indBuff++];
							allNodes[nLocalNodes].arms[j].ny = inBuff[indBuff++];
							allNodes[nLocalNodes].arms[j].nz = inBuff[indBuff++];
						}
						
						allNodes[nLocalNodes].x = inBuff[indBuff++];
						allNodes[nLocalNodes].y = inBuff[indBuff++];
						allNodes[nLocalNodes].z = inBuff[indBuff++];
						
						allNodes[nLocalNodes].oldx = inBuff[indBuff++];
						allNodes[nLocalNodes].oldy = inBuff[indBuff++];
						allNodes[nLocalNodes].oldz = inBuff[indBuff++];
						
						tagMap[remDom][remInd] = nLocalNodes;
						
						nLocalNodes++;
					}
					
					free(inBuff);
				}
			}
			
		} else {
			// Send local nodes to master CPU
			PackSendRemNodes(home);
		}
#else
		nLocalNodes = 0;
		for (k = 0; k < home->newNodeKeyPtr; k++) {
			if ((node = home->nodeKeys[k]) == (Node_t *)NULL) continue;
			
			allNodes[nLocalNodes].domainID    = thisDomain;
			allNodes[nLocalNodes].index       = node->myTag.index;
			allNodes[nLocalNodes].globalIndex = nLocalNodes;
			
			allNodes[nLocalNodes].numArms = node->numNbrs;
			allNodes[nLocalNodes].arms = (ArmData_t*)malloc(sizeof(ArmData_t)*node->numNbrs);
			for (j = 0; j < node->numNbrs; j++) {
				
				allNodes[nLocalNodes].arms[j].domainID = node->nbrTag[j].domainID;
				allNodes[nLocalNodes].arms[j].index    = node->nbrTag[j].index;
				
				allNodes[nLocalNodes].arms[j].bx = node->burgX[j];
				allNodes[nLocalNodes].arms[j].by = node->burgY[j];
				allNodes[nLocalNodes].arms[j].bz = node->burgZ[j];
				
				allNodes[nLocalNodes].arms[j].nx = node->nx[j];
				allNodes[nLocalNodes].arms[j].ny = node->ny[j];
				allNodes[nLocalNodes].arms[j].nz = node->nz[j];
			}
			
			allNodes[nLocalNodes].x = node->x;
			allNodes[nLocalNodes].y = node->y;
			allNodes[nLocalNodes].z = node->z;
			
			allNodes[nLocalNodes].oldx = node->oldx;
			allNodes[nLocalNodes].oldy = node->oldy;
			allNodes[nLocalNodes].oldz = node->oldz;
			
			tagMap[thisDomain][k] = nLocalNodes;
			
			nLocalNodes++;
		}
#endif
		
/*
 * 		From here, all nodes and segments have been sent to
 * 		the master CPU. Only the master computes and outputs.
 */
		if (thisDomain == 0) {
		
/*
 *      	Set data file name.
 */
			snprintf(fileName, sizeof(fileName), "%s/%s",
					 DIR_LINKDATA, baseFileName);
        
/*
 *			First task must open the data file for writing
 *			to overwrite any existing file of the same name.
 */
			if ((fp = fopen(fileName, "w")) == (FILE *)NULL) {
				Fatal("WriteLinksData: Open error %d on %s\n", errno, fileName);
			}
			printf(" +++ Writing links file(s) %s\n", baseFileName);
		
		
			NodeData_t *lastnode, *nextnode, *endnode;
			NodeData_t **NodesSeq;
			NodesSeq = malloc(nGlobalNodes*sizeof(NodeData_t*));
			
			maxNodes = 2*nGlobalNodes;
			
			numLinks = 0;
			totalLength = 0.0;
			
/*
*			Loop over all nodes, looking for physical nodes.
*/		
			for (k = 0; k < nGlobalNodes; k++) {
			
				NodeData_t *nodeData = &allNodes[k];
				numArms =  nodeData->numArms;
			
				nplanes = NumUniqueArmPlanes(nodeData);
				if ((nplanes==1) && (numArms==2)) continue;

/*
 *				Loop over all arms. Hop along the line until another
 *				physical node is encountered. Print the resulting
 *				info for that line.
 */
				for (i = 0; i < numArms; i++) {
						
					bx = nodeData->arms[i].bx;
					by = nodeData->arms[i].by;
					bz = nodeData->arms[i].bz;
					
					nextarm = i;
					lastnode = nodeData;
					NodesSeq[0] = lastnode;
					numNodes = 1;
					
					//Initial data to zero
					len = 0.0;
					lenold = 0.0;
					lendot = 0.0;
					Adot = 0.0;
					vavg = 0.0;
					
					while (1) {
						
						int nextnodedom = lastnode->arms[nextarm].domainID;
						int nextnodeid = lastnode->arms[nextarm].index;
						int nextnodeglobalid = tagMap[nextnodedom][nextnodeid];
						nextnode = &allNodes[nextnodeglobalid];
						
						arm = -1;
						for (j = 0; j < nextnode->numArms; j++) {
							if (nextnode->arms[j].domainID == lastnode->domainID &&
								nextnode->arms[j].index    == lastnode->index) {
								arm = j;
								break;
							}
						}
						
						NodesSeq[numNodes] = nextnode;
						numNodes++;
							
						//Length
						dx = nextnode->x - lastnode->x;
						dy = nextnode->y - lastnode->y;
						dz = nextnode->z - lastnode->z;
						ZImage(home->param, &dx, &dy, &dz);
						
						seglen = sqrt(dx*dx + dy*dy + dz*dz);
						len += seglen;
						
						dxold = nextnode->oldx - lastnode->oldx;
						dyold = nextnode->oldy - lastnode->oldy;
						dzold = nextnode->oldz - lastnode->oldz;
						ZImage(home->param, &dxold, &dyold, &dzold);
						
						seglen = sqrt(dxold*dxold + dyold*dyold + dzold*dzold);
						lenold += seglen;
						
						//Area sweep rate, same as DeltaPlasticStrain_FCC
						ex = nextnode->x - lastnode->oldx; 
						ey = nextnode->y - lastnode->oldy; 
						ez = nextnode->z - lastnode->oldz; 

						hhx = lastnode->x - nextnode->oldx;
						hhy = lastnode->y - nextnode->oldy;
						hhz = lastnode->z - nextnode->oldz;

						ZImage(param, &ex, &ey, &ez);
						ZImage(param, &hhx, &hhy, &hhz);

						nx = (ey*hhz - ez*hhy) * 0.5;
						ny = (ez*hhx - ex*hhz) * 0.5;
						nz = (ex*hhy - ey*hhx) * 0.5;
			   
						dyad[0][0] = nx*bx; dyad[0][1] = nx*by; dyad[0][2] = nx*bz;
						dyad[1][0] = ny*bx; dyad[1][1] = ny*by; dyad[1][2] = ny*bz;
						dyad[2][0] = nz*bx; dyad[2][1] = nz*by; dyad[2][2] = nz*bz;
			   
						for (m = 0; m < 3; m++){
						   for (n = 0; n < 3; n++){
							   real8 tmpDstn;
							   tmpDstn = (dyad[m][n]+dyad[n][m])*0.5/param->simVol;
							   localstrain[m][n] = tmpDstn; // Fixed BUG += here!
						   }
						}
						
						localeRate =
							param->edotdir[0]*localstrain[0][0]*param->edotdir[0] +
							param->edotdir[0]*localstrain[0][1]*param->edotdir[1] +
							param->edotdir[0]*localstrain[0][2]*param->edotdir[2] +
							param->edotdir[1]*localstrain[1][0]*param->edotdir[0] +
							param->edotdir[1]*localstrain[1][1]*param->edotdir[1] +
							param->edotdir[1]*localstrain[1][2]*param->edotdir[2] +
							param->edotdir[2]*localstrain[2][0]*param->edotdir[0] +
							param->edotdir[2]*localstrain[2][1]*param->edotdir[1] +
							param->edotdir[2]*localstrain[2][2]*param->edotdir[2];

						//This needs to be checked
						if (localeRate > 0.0) {
							Asgn = 1.0;
						} else if (localeRate < 0.0) {
							Asgn = -1.0;
						} else {
							Asgn = 0.0;
						}
						
						dA = sqrt(nx*nx + ny*ny + nz*nz);
						
						Adot += Asgn*dA/param->realdt;

						//If this isn't a physical node, keep going!
						nplanes = NumUniqueArmPlanes(nextnode);
						if ((nextnode->numArms != 2) || (nplanes>1) || (numNodes>maxNodes)) break;

						nextarm = 1 - arm;
						lastnode = nextnode;
					}

					//Avoid infinite loop if something goes wrong...
					if (numNodes > maxNodes) { 
						fprintf(fp, "numNodes>maxNodes\n");	
						continue;
					}
					
					endnode = nextnode;
					//Prevent double counting lines
					if (nodeData->globalIndex >= endnode->globalIndex) continue;
					
					//elongation rate
					lendot = (len-lenold)/param->realdt;
					
					//average velocity
					vavg = Adot/len;
					
					
					//estimate line curvature (radius)
					double R = 0.0;
					double L0 = 0.0;
					if (numNodes > 2) {
						
						double Xn[numNodes], Yn[numNodes], Zn[numNodes];
						double t[3], h, maxh, p[3];
						
						Xn[0] = NodesSeq[0]->oldx;
						Yn[0] = NodesSeq[0]->oldy;
						Zn[0] = NodesSeq[0]->oldz;
						
						for (j = 1; j < numNodes; j++) {
							Xn[j] = NodesSeq[j]->oldx;
							Yn[j] = NodesSeq[j]->oldy;
							Zn[j] = NodesSeq[j]->oldz;
							PBCPOSITION(param, Xn[j-1], Yn[j-1], Zn[j-1], &Xn[j], &Yn[j], &Zn[j]);
						}
						
						t[0] = Xn[numNodes-1]-Xn[0];
						t[1] = Yn[numNodes-1]-Yn[0];
						t[2] = Zn[numNodes-1]-Zn[0];
						L0 = Normal(t); // chord length
						NormalizeVec(t);
						
						maxh = 0.0; // maximum sagitta
						for (j = 1; j < numNodes-1; j++) {
							p[0] = Xn[j]-Xn[0];
							p[1] = Yn[j]-Yn[0];
							p[2] = Zn[j]-Zn[0];
							xvector(t[0], t[1], t[2], p[0], p[1], p[2], &nx, &ny, &nz);
							h = sqrt(nx*nx + ny*ny + nz*nz);
							maxh = MAX(h, maxh);
						}
						R = 0.5*maxh+L0*L0/8.0/maxh; // radius of curvature
						
					}
					
					numLinks++;
					totalLength += len;
					
					//Print info!
					fprintf(fp, "%d %d %d %e %e %e %e %e %e %e %e %e %e %e %e %e\n",
							nodeData->globalIndex, endnode->globalIndex, numNodes,
							nodeData->arms[i].bx, nodeData->arms[i].by, nodeData->arms[i].bz,
							nodeData->arms[i].nx, nodeData->arms[i].ny, nodeData->arms[i].nz,
							len, lenold, lendot, Adot, vavg, R, L0);
					
				}

			}
			
			free(NodesSeq);
				
			fclose(fp);
					
/*
 * 			Compute phi structural factor: phi = N^2/rho^3
 */			
			double phi = numLinks*numLinks/(totalLength*totalLength*totalLength)*param->simVol;
			
			double al, am, an;
			al = param->edotdir[0];
			am = param->edotdir[1];
			an = param->edotdir[2];

			double amag = sqrt(al*al+am*am+an*an);

			al /= amag;
			am /= amag;
			an /= amag;
			
			double sigijk = param->appliedStress[0]*al*al     +
							 param->appliedStress[1]*am*am     +
							 param->appliedStress[2]*an*an     +
							 2.0*param->appliedStress[3]*am*an +
							 2.0*param->appliedStress[4]*an*al +
							 2.0*param->appliedStress[5]*al*am;	
/*
 *      	Output phi and other properties.
 */
			snprintf(fileName, sizeof(fileName), "%s/phi", DIR_PROPERTIES);			
			fp = fopen(fileName, "a");
			fprintf(fp, "%d %e %e %d %e %e\n", home->cycle, 
			        param->eRate*param->timeNow*1e2, sigijk/1e6, numLinks, 
			        totalLength/param->simVol/param->burgMag/param->burgMag, phi);
			fclose(fp);
			
/*
 * 			Free memory
 */			
			for (i = 0; i < home->numDomains; i++) {
				free(tagMap[i]);
			}
			free(tagMap);
			for (k = 0; k < nGlobalNodes; k++) {
				free(allNodes[k].arms);
			}
			free(allNodes);
		
		}
		
		return;
	
}

