#!/usr/bin/tclsh

#Read status variable from the first argument (if any)
if { $argc == 0 } {
 set status 0
} elseif { $argc > 0 } {
 set status [lindex $argv 0]
}
puts "status = $status"

set datafile "runs/concentric_loop_test.data"
set version_number 2
set filesegments_number 1

set L 6
set cylinder_radius [expr $L/6]
set loop_rel_radius 0.8
set loop_radius [expr $cylinder_radius * $loop_rel_radius]

set xBoundMin [expr -$L/2]
set yBoundMin [expr -$L/2]
set zBoundMin [expr -$L/2]
set xBoundMax [expr  $L/2]
set yBoundMax [expr  $L/2]
set zBoundMax [expr  $L/2]

set nodes_number 50

set burg_x   1
set burg_y   0
set burg_z   0

set normal_x 0
set normal_y 0
set normal_z 1

set domainID 10

set fileID [open $datafile w]

puts $fileID "#"
puts $fileID "#	ParaDiS nodal data file (by create_loop_data.tcl) "
puts $fileID "#"
puts $fileID " "
puts $fileID "#"
puts $fileID "#	File version number"
puts $fileID "#"
puts $fileID " "
puts $fileID "$version_number"
puts $fileID " "
puts $fileID "#"
puts $fileID "#	Number of data file segments"
puts $fileID "#"
puts $fileID " "
puts $fileID "$filesegments_number"
puts $fileID " "
puts $fileID "#"
puts $fileID "#	Minimum coordinate values (x, y, z)"
puts $fileID "#"
puts $fileID ""
puts $fileID "$xBoundMin $yBoundMin $zBoundMin   "
puts $fileID " "
puts $fileID "#"
puts $fileID "#	Maximum coordinate values (x, y, z)"
puts $fileID "#"
puts $fileID " "
puts $fileID "$xBoundMax $yBoundMax $zBoundMax   "
puts $fileID " "
puts $fileID "#"
puts $fileID "#	Node count"
puts $fileID "#"
puts $fileID " "
puts $fileID "$nodes_number   "
puts $fileID " "
puts $fileID "#"
puts $fileID "#	Domain geometry (x, y, z)"
puts $fileID "#"
puts $fileID " "
puts $fileID "1   1   1  "
puts $fileID " "
puts $fileID "#"
puts $fileID "#	Domain boundaries (x, y, z)"
puts $fileID "#"
puts $fileID " "
puts $fileID "   $xBoundMin"
puts $fileID "       $yBoundMin"
puts $fileID "           $zBoundMin"
puts $fileID "           $zBoundMax"
puts $fileID "       $yBoundMax"
puts $fileID "   $xBoundMax"
puts $fileID "#"
puts $fileID "#	Primary lines: node_tag, x, y, z, num_arms, constraint"
puts $fileID "#	Secondary lines: arm_tag, burgx, burgy, burgz, nx, ny, nz"
puts $fileID "#"
puts $fileID "#       length in unit of burgMag"
puts $fileID " "


set minus_burg_x [expr -$burg_x]
set minus_burg_y [expr -$burg_y]
set minus_burg_z [expr -$burg_z]

set Pi 3.1415926535897931

for {set n 0} {$n < $nodes_number} {incr n 1} {
    set x [format %22.16e [expr $loop_radius*cos(2*$Pi*($n+1.0)/$nodes_number)]]
    set y [format %22.16e [expr $loop_radius*sin(2*$Pi*($n+1.0)/$nodes_number)]]
    set z 0
    set arms_number 2
    set flag 0
    set n1 [expr $n+1]    
    set n2 [expr $n-1]
    if { $n1 == $nodes_number } { set n1 0 }
    if { $n2 == -1 } { set n2 [expr $nodes_number - 1 ] }

    puts $fileID "     $domainID,$n      $x $y $z $arms_number $flag"
    puts $fileID "           $domainID,$n1      $burg_x $burg_y $burg_z   "
    puts $fileID "                       $normal_x $normal_y $normal_z"
    puts $fileID "           $domainID,$n2      $minus_burg_x $minus_burg_y $minus_burg_z   "
    puts $fileID "                       $normal_x $normal_y $normal_z"
}

close $fileID

puts "$datafile created."
puts "bye."

