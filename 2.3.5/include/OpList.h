/***************************************************************************
 *
 *	OpList.h	Define the struct that holds all data needed
 *			to handle a cross-domain topology change.
 *
 **************************************************************************/


#ifndef _OPLIST_H
#define _OPLIST_H

#include "Typedefs.h"
#include "Node.h"
#include "Tag.h"

#define OpBlock_Count 500

struct _operate {
	OpType_t	type;
	int		dom1;
	int		idx1;
	int		dom2;
	int		idx2;
	int		dom3;
	int		idx3;
	real8		bx;
	real8		by;
	real8		bz;
	real8		x;
	real8		y;
	real8		z;
	real8		nx;
	real8		ny;
	real8		nz;
#ifdef _STACKINGFAULT
	real8		gammanx;
	real8		gammany;
	real8		gammanz;
#endif
};

#endif /* _OPLIST_H */
