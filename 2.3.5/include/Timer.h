/*************************************************************************
 *
 *  Timer.h - define the timing structures
 *
 ************************************************************************/

#ifndef _Timer_h
#define _Timer_h

#include "Typedefs.h"

struct _timer {
	real8	startTime; /* time at which most recent TimerStart called */
	real8	incr;      /* time of most recent event for this event type */
	real8	accum;     /* accumulated time for this event type */
	real8	save;      /* to save full force update times till */
			   /* next WriteStep */
	int	started;   /* 1 if event is active, 0 otherwise */
	char	*name;     /* label used during TimerPrint */
};

#define TIMER_BLOCK_SIZE 34

#define	TOTAL_TIME		0
#define	INITIALIZE		1
#define	SORT_NATIVE_NODES	2
#define	COMM_SEND_GHOSTS	3
#define	GHOST_COMM_BARRIER	4
#define	CELL_CHARGE		5
#define	CELL_CHARGE_BARRIER     6
#define	CALC_FORCE		7
#define	LOCAL_FORCE		8
#define	REMOTE_FORCE		9
#define CALC_FORCE_BARRIER      10
#define	CALC_VELOCITY		11
#define CALC_VELOCITY_BARRIER   12
#define	COMM_SEND_VELOCITY	13
#define SPLIT_MULTI_NODES       14
#define	COLLISION_HANDLING	15
#define	POST_COLLISION_BARRIER  16
#define COL_SEND_REMESH         17
#define COL_FIX_REMESH          18
#define COL_FORCE_UPDATE        19
#define	GENERATE_IO		20
#define	PLOT			21
#define	IO_BARRIER	        22
#define	REMESH_START_BARRIER  	23
#define	REMESH			24
#define	SEND_REMESH             25
#define	FIX_REMESH              26
#define	FORCE_UPDATE_REMESH     27
#define	REMESH_END_BARRIER  	28
#define	MIGRATION		29
#define	MIGRATION_BARRIER       30
#define	LOADCURVE		31
#define	LOAD_BALANCE		32
#define SEGFORCE_COMM           33

#endif
