/*****************************************************************************
 *
 *	Init.h		Prototypes for the initialization routines
 *
 ****************************************************************************/

#ifndef _Init_h
#define _Init_h

#include "Home.h"
#include "InData.h"

void   InitCellDomains(Home_t *home);
void   InitCellNatives(Home_t *home);
void   InitCellNeighbors(Home_t *home);
Home_t *InitHome(void);
void   Initialize(Home_t *home,int argc, char *argv[]);
void   RecvInitialNodeData(Home_t *home);
void   SendInitialNodeData(Home_t *home, InData_t *inData, int *msgCount,
           int **nodeLists, int *listCounts);

#endif
