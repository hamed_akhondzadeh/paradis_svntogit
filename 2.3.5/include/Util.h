/****************************************************************************
 *
 * Util.h  contains miscellaneous structs and prototypes, required by the
 *         Util.c module
 *
 ***************************************************************************/

#ifndef _Util_h
#define _Util_h

#include "InData.h"

#ifndef FFACTOR_LMIN
#define FFACTOR_LMIN  1e-8  /* minimum allowed segment length */
#define FFACTOR_LMIN2 1e-16 /* minimum allowed segment length squared */
#endif

/*
 *	Define some values that can be used by GenerateOutput()
 *	to indicate the current stage of the execution.  Needed
 *	because output generation differs depending on the point
 *	the program is at.
 */
#define	STAGE_INIT	1
#define	STAGE_CYCLE	2
#define	STAGE_TERM	3

#define	FIRST_BLOCK	1
#define	LAST_BLOCK	2

/*
 *	Define a set of flags corresponding to the types of
 *	output that may be generated.  Note: these are intended
 *	as bit values that can be or'ed together.
 */

#define	GEN_ARM_DATA		0x0001
#define	GEN_DENSITY_DATA	0x0002
#define	GEN_FLUX_DATA		0x0004
#define	GEN_GNUPLOT_DATA	0x0008
#define	GEN_POLEFIG_DATA	0x0010
#define	GEN_POVRAY_DATA		0x0020
#define	GEN_POSTSCRIPT_DATA	0x0040
#define	GEN_PROPERTIES_DATA	0x0080
#define	GEN_RESTART_DATA	0x0100
#define	GEN_TECPLOT_DATA	0x0200
#define	GEN_TIMER_DATA		0x0400
#define	GEN_TSB_DATA		0x0800
#define	GEN_VELOCITY_DATA	0x1000
#define	GEN_XWIN_DATA		0x2000
#define GEN_FRAG_DATA           0x4000
#define	GEN_ATOMEYE_DATA	0x8000

#define DotProduct(vec1,vec2)       \
            ((vec1[0])*(vec2[0]) +  \
             (vec1[1])*(vec2[1]) +  \
             (vec1[2])*(vec2[2]))

/* Prototypes */

void  CSpline(real8 *x, real8 *y, real8 *y2, int numPoints);
void  CSplint(real8 *xa, real8 *ya, real8 *y2, int numPoints,
          real8 x, real8 *y);
void  InterpolateL(real8 *xa, real8 *ya, int numPoints, real8 x, real8 *y);
int   InterpolateBL(real8 *x, real8 *y, real8 *val, int numElem,
          real8 targetX, real8 targetY, real8 *result);
void  ReadTabulatedData(char *fileName, int numCols, real8 ***colData,
          int *numRows);

real8 randm(int *seed);
int   Sign(real8 x);

void TimerClear(Home_t *home, int index);
void TimerClearAll(Home_t *home);
void TimerInit(Home_t *home);
void TimerInitDLBReset(Home_t *home);
void TimerStart(Home_t *home, int index);
void TimerStop(Home_t *home, int index);
void TimerSave(Home_t *home, int index);
void TimerPrint(Home_t *home);
void TimeAtRestart (Home_t *home, int stage);
void FoldBox(Param_t *param, real8 *x, real8 *y, real8 *z);

void GetUnitVector(int unitFlag, 
                   real8 vx0, real8 vy0, real8 vz0,
                   real8 vx1, real8 vy1, real8 vz1,
                   real8 *ux, real8 *uy, real8 *uz,
                   real8 *disMag);

real8 Normal(real8 a[3]);
void  NormalizedCrossVector(real8 a[3], real8 b[3], real8 c[3]);

void cross(real8 a[3], real8 b[3], real8 c[3]);

void  MatrixMult(real8 *a, int aRows, int aCols, int aLD,
                 real8 *b, int bCols, int bLD, real8 *c, int cLD);
int   MatrixInvert(real8 *mat, real8 *invMat, int order, int lda);
real8 Matrix22Det(real8 a[2][2]);
void  Matrix22Invert(real8 A[2][2], real8 B[2][2]);
void  Matrix22Vector2Mult(real8 a[2][2], real8 b[2], real8 c[2]);
real8 Matrix33Det(real8 a[3][3]);
int   Matrix33Invert(real8 A[3][3], real8 B[3][3]);
void  Matrix33Mult33(real8 a[3][3], real8 b[3][3], real8 c[3][3]);
void  Matrix33Vector3Multiply(real8 A[3][3], real8 x[3], real8 y[3]);
void  Matrix31Vector3Mult(real8 mat[3], real8 vec[3], real8 result[3][3]);
void  Vec3TransposeAndMult(real8 vec[3], real8 result[3][3]);
void  Matrix33Transpose(real8 mat[3][3], real8 trans[3][3]);
void  Vector3Matrix33Mult(real8 vec[3], real8 mat[3][3], real8 result[3]);

void xvector(real8 ax, real8 ay, real8 az,
             real8 bx, real8 by, real8 bz,
             real8 *cx, real8 *cy, real8 *cz);

void dSegImgStress(Home_t *home,
                   real8  Sigma[][3],
                   real8  px,    real8 py,    real8 pz,
                   real8  dlx,   real8 dly,   real8 dlz,
                   real8  burgX, real8 burgY, real8 burgZ,
                   real8  rx,    real8 ry,    real8 rz,
                   int pbc);

void SegmentStress(real8 MU, real8 NU,
                   real8 burgX, real8 burgY, real8 burgZ,
                   real8 xA, real8 yA, real8 zA,
                   real8 xB, real8 yB, real8 zB,
                   real8 x0, real8 y0, real8 z0,
                   real8 a, real8 Sigma[3][3]);

void SegmentStress_fullimage(Home_t *home,
                             real8 MU, real8 NU,
                             real8 burgX, real8 burgY, real8 burgZ,
                             real8 xA, real8 yA, real8 zA,
                             real8 xB, real8 yB, real8 zB,
                             real8 x0, real8 y0, real8 z0,
                             real8 a, real8 Sigma[3][3]);

void FreeNode (Home_t *home, int index);
int  GetFreeNodeTag (Home_t *home);
void MarkNodeForceObsolete(Home_t *home, Node_t *node);
void Meminfo(int *wss);
void ReadRijm(Home_t *home);
void ReadRijmPBC(Home_t *home);
void RemoveNode(Home_t *home, Node_t *node, int Log);
void ResetNodeArmForce (Home_t *home, Node_t *node);
void SortNativeNodes (Home_t *home);
void SubtractSegForce(Home_t *home, Node_t *node1, Node_t *node2);
void TimerPrint(Home_t *home);
void Plot(Home_t *home, int domIndex, int blkFlag);
void WriteArms(Home_t *home, char *baseFileName, int ioGroup,
        int firstInGroup, int writePrologue, int writeEpilogue);
void WritePoleFig(Home_t *home, char *baseFileName, int ioGroup,
        int firstInGroup, int writePrologue, int writeEpilogue);
void WriteDensFlux(char *fname, Home_t *home);
void WriteDensityField(Home_t *home, char *fileName);
void WritePovray(Home_t *home, char *baseFileName, int ioGroup,
        int firstInGroup, int writePrologue, int writeEpilogue);
void WriteAtomEye(Home_t *home, char *baseFileName, int ioGroup,
        int firstInGroup, int writePrologue, int writeEpilogue);
void WriteTSB(Home_t *home, char *baseFileName, int ioGroup, int firstInGroup,
        int writePrologue, int writeEpilogue, int numArms);
void *CreateFragmentList(Home_t *home, int *totalFragmentCount);
void WriteFragments(Home_t *home, char *baseFileName, int ioGroup,
                    int firstInGroup, int writePrologue, int writeEpilogue,
                    void **fragmentList, int totalFragCount);

void Normalize(real8 *ax, real8 *ay, real8 *az);
void Orthogonalize(real8 *ax, real8 *ay, real8 *az,
                   real8 bx, real8 by, real8 bz);

#ifdef _CYGWIN
extern "C" void Fatal(char *format, ...);
#else
#if __cplusplus
extern "C" void Fatal(char *format, ...);
#else
void Fatal(char *format, ...);
#endif
#endif

void Uniq (int*, int*) ;
void LocateCell(Home_t *home, int *cellID, int cellIndex[3], real8 coord[3]);
void DecodeCellIdx (Home_t *home, int idx, int *iX, int *jY, int *kZ)  ;
int EncodeCellIdx (Home_t *home, int iX, int jY, int kZ) ;
void DecodeCell2Idx (Home_t *home, int idx, int *iX, int *jY, int *kZ)  ;
int EncodeCell2Idx (Home_t *home, int iX, int jY, int kZ) ;
void DecodeDomainIdx (Home_t *home, int idx, 
                      int *iXdom, int *jYdom, int *kZdom) ;
int EncodeDomainIdx (Home_t *home, int iXdom, int jYdom, int kZdom) ;
Node_t *GetNodeFromTag (Home_t *home, Tag_t tag) ;
Node_t *GetNodeFromIndex (Home_t *home, int domID, int index ) ;
int NodeConnectivity (Node_t *node) ;

void ZImage (Param_t *param, real8* x, real8* y, real8* z);
void PBCPOSITION(Param_t *param, real8 x0, real8 y0, real8 z0,
		 real8 *x, real8 *y, real8 *z);

int OrderNodes(const void *a, const void *b) ;
int CollisionNodeOrder(Home_t *home, Tag_t *tagA, Tag_t *tagB);
Node_t *GetNeighborNode (Home_t *home, Node_t *node, int n) ;

int GetArmID (Home_t *home, Node_t *node1, Node_t *node2) ;

int ChangeConnection (Home_t *home, Node_t *node1, Tag_t *tag2,
                      Tag_t *tag3, int Log) ;
void PrintNode (Node_t *node) ;

#ifndef _STACKINGFAULT
void GetBurgersVectorNormal (Home_t *home, Node_t *node1, Node_t *node2,
                             real8 *bx, real8 *by, real8 *bz,
                             real8 *nx, real8 *ny, real8 *nz) ;

void InsertArm (Home_t *home, Node_t *nodeA, Tag_t *nodeBtag,
                real8 bx, real8 by, real8 bz,
                real8 nx, real8 ny, real8 nz, int Log);
#else
void GetBurgersVectorNormal (Home_t *home, Node_t *node1, Node_t *node2,
                             real8 *bx, real8 *by, real8 *bz,
                             real8 *nx, real8 *ny, real8 *nz,
                             real8 *gammanx, real8 *gammany, real8 *gammanz) ;

void InsertArm (Home_t *home, Node_t *nodeA, Tag_t *nodeBtag,
                real8 bx, real8 by, real8 bz,
                real8 nx, real8 ny, real8 nz, 
                real8 gammanx, real8 gammany, real8 gammanz, int Log);
#endif

void ResetSegForces(Home_t *home, Node_t *nodeA, Tag_t *nodeBtag,
                    real8 fx, real8 fy, real8 fz, int globalOp);
void ResetSegForces2(Home_t *home, Node_t *nodeA, Tag_t *nodeBtag,
                     real8 f1x, real8 f1y, real8 f1z,
                     real8 f2x, real8 f2y, real8 f2z, int globalOp);
void ResetNodalVelocity(Home_t *home, Node_t *nodeA, real8 vx, real8 vy,
                        real8 vz, int globalOp);
void ResetGlidePlane(Home_t *home, real8 newPlane[3], Tag_t *tag1,
                     Tag_t *tag2, int globalOp);
void RepositionNode(Home_t *home, real8 newPos[3], Tag_t *tag, int globalOp);

/*
 *	The DEL_SEG_* definitions are to be used as the <del_seg_factor>
 *	argument in invocations of the ChangeArmBurg() function.  This
 *	value indicates what portion of length of any segment deleted by
 *	the function will be accumulated in the <delSegLength> variable.
 */
#define	DEL_SEG_NONE	0.0
#define	DEL_SEG_HALF	0.5
#ifndef _STACKINGFAULT
void ChangeArmBurg (Home_t *home, Node_t *node1, Tag_t *tag2,
                    real8 bx, real8 by, real8 bz, real8 nx,
		    real8 ny, real8 nz, int Log, real8 del_seg_factor);
#else
void ChangeArmBurg (Home_t *home, Node_t *node1, Tag_t *tag2,
                    real8 bx, real8 by, real8 bz, real8 nx,
		    real8 ny, real8 nz, real8 gammanx, real8 gammany, real8 gammanz,
		    int Log, real8 del_seg_factor);
#endif

void AllocNodeArms(Node_t *node, int n);
void FreeNodeArms(Node_t *node);
void ReallocNodeArms(Node_t *node, int n);
int  NodeCmpByTag(const void *, const void *);
void CompressArmLists(Node_t *node) ;
void RecycleNodeTag(Home_t *home, int tagIdx);
int  GetRecycledNodeTag(Home_t *home);


/* OpList handling */
void ClearOpList (Home_t *home) ;
void PrintOpList (Home_t *home) ;
void InitOpList (Home_t *home) ;
void ExtendOpList (Home_t *home) ;
void FreeOpList (Home_t *home) ;

#ifndef _STACKINGFAULT
void AddOp (Home_t *home,
            OpType_t type, int dom1, int idx1,
            int dom2, int idx2, int dom3, int idx3,
            real8 bx, real8 by, real8 bz,
            real8 x, real8 y, real8 z,
            real8 nx, real8 ny, real8 nz) ;
#else
void AddOp (Home_t *home,
            OpType_t type, int dom1, int idx1,
            int dom2, int idx2, int dom3, int idx3,
            real8 bx, real8 by, real8 bz,
            real8 x, real8 y, real8 z,
            real8 nx, real8 ny, real8 nz,
            real8 gammanx, real8 gammany, real8 gammanz) ;
#endif


#ifndef INVBURGID
#define INVBURGID(b) ((b) - ( ((b)%2)*2-1 ))
#endif


void RemoteStress(Home_t *home,Node_t *node,Node_t *rNodeA,Node_t *rNodeB,
                     real8 Lx, real8 Ly, real8 Lz, int ti, int i,
                     real8 cntx, real8 cnty, real8 cntz,
                     real8 gsig[][3]);

void deWitInteraction(real8 MU, real8 NU,
                      real8  Sigma[][3],
                      real8  px, real8  py, real8  pz,
                      real8  tp1, real8  tp2, real8  tp3,
                      real8  burgX, real8  burgY, real8  burgZ,
                      real8  *cntx, real8  *cnty, real8  *cntz);

void testdeWitStress2();




/* Node Force Calculation */

void SelfForce(int coreOnly, real8 MU, real8 NU,
               real8 bx, real8 by, real8 bz,
               real8 x1, real8 y1, real8 z1,
               real8 x2, real8 y2, real8 z2,
               real8 a,  real8 Ecore,
               real8 f1[3], real8 f2[3]);
void ExtPKForce(real8 str[3][3],
                real8 bx, real8 by, real8 bz,
                real8 X1, real8 Y1, real8 Z1,
                real8 X2, real8 Y2, real8 Z2,
                real8 f1[3], real8 f2[3]);

#ifdef _STACKINGFAULT
void StackingFaultForce(real8 gammanx, real8 gammany, real8 gammanz,
                        real8 X1, real8 Y1, real8 Z1,
                        real8 X2, real8 Y2, real8 Z2,
                        real8 f1[3], real8 f2[3]);
#endif

void PKForce(real8 sigb[3],
             real8 X1, real8 Y1, real8 Z1,
             real8 X2, real8 Y2, real8 Z2,
             real8 f1[3], real8 f2[3]);

void GetFluxNDensity(Home_t *home,real8 bx, real8 by,real8 bz,
                     real8 ANX[3],real8 ANY[3],real8 ANZ[3],
                     real8 zeta[3],real8 zetaold[3],
                     real8 delxnode, real8 delynode, real8 delznode,
                     real8 delxnbr, real8 delynbr, real8 delznbr,
                     real8 fluxndens[10]);

void GetFieldPointStress(Home_t *home, real8 x, real8 y, real8 z,
                         real8 totStress[3][3]);

#endif





void Write_Node_Force(Home_t *home,char *format);
