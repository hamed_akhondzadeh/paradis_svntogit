/***************************************************************************
 *
 *  FEM.h  interface between ParaDiS &  FEM code of Meijie Tang & Guanshui Xu
 *  Updated: Meijie Tang, Mar18, 2004
 **************************************************************************/


#ifndef _FEM_H
#define _FEM_H


/* the C names are dummy names not used. The purpose is to keep track 
 * which fortran subroutines are called by the C 
 */ 

#define FEM_Mesh_Data_Brick fem_mesh_data_brick_
#define FEM_Mesh_Data_Cylinder fem_mesh_data_cylinder_
#define FEM_Mesh_Data_Void fem_mesh_data_void_

#define FEM_Boundary_Node_Force_Term fem_boundary_node_force_term_
#define FEM_Analysis fem_analysis_
#define FEM_Point_Stress fem_point_stress_

#define FEM_Node_On_Surface node_on_surface_ 
#define FEM_Segment_Surface_Intersection fem_segment_surface_intersection_


#define FEM_Set_Step fem_set_step_
#define DD_Stress_on_FEM_Boundary stress_on_boundary_
#define ParadisStress paradisstress_
#define Yoffe_Image_Stress sh_image_stress_num_

void ParadisStress(double r[3], double sig_seg[6], double sig_node_img[6],
                   double sig_node_yf[6]);

void FEM_Init();
void FEM_Step();
void FEM_NodePropInit(); 

extern struct _home *home0;

#endif /* _FEM_H */

