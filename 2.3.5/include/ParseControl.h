/*
  ParseControl.h
  by Wei Cai  caiwei@mit.edu
  Last Modified : Mon Mar  4 15:41:36 2002

  FUNCTION  :
*/

#ifndef _PARSECONTROL_H
#define _PARSECONTROL_H

#include "Home.h"
#include "InData.h"
#include "Util.h"

#define	CPP_FILE_SUFFIX	".ext"	/* suffix added to the name of the copy of */
				/* the control file created by running the */
				/* input control file through cpp          */

#ifdef __cplusplus
extern "C" void ParseControl(char *fname, InData_t *inData);
extern "C" void HardCodedControl(InData_t *inData);
extern "C" int ReadDecompFromCtrlFile(FILE *fp, InData_t *inData);
extern "C" void WriteAllVars(FILE *fp);
#else
void ParseControl(char *fname, InData_t *inData);
#endif /* __cplusplus */

#endif /* _PARSECONTROL_H */

