/****************************************************************************
 *
 *      Module:         CrossSlipFCC.c
 *
 *      Author:         Converted/adapted from CrossSlipBCC.c
 *
 *      Description:    This module contains functions for allowing
 *                      dislocations in FCC materials to cross slip to
 *                      a glide plane other than its original plane
 *
 *      Includes public functions:
 *          CrossSlipFCC()
 *
 *      Includes private functions:
 *          NodePinned()
 *
 *      To do:
 *             1. Add energy barrier and calculation of cross slip rate 
 *                (instead of instantaneous cross slip)
 *
 *
 ***************************************************************************/
#include <math.h>
#include "Home.h"
#include "Util.h"


/*---------------------------------------------------------------------------
 *
 *      Function:       PinnedNode (same as that in CrossSlipBCC.c)
 *
 *      Description:    Determine if a node should be considered 'pinned'
 *                      during the a cross slip procedure.  Nodes are
 *                      to be pinned if any of the following are true:
 *
 *                        - the node has a constraint of 7
 *                        - the node is owned by another domain
 *                        - the node has any segments in a plane other
 *                          than the one indicated by <planeIndex>.
 *                        - the node is attached to any segments owned
 *                          by a remote domain.
 *
 *      Arguments:
 *         node         pointer to node 
 *         planeIndex   row index of a glide plane in <glidedir>
 *         glidedir     3X3 matrix of possible glide planes.  each
 *                      row representing a single plane.
 *
 *      Returns:   1 if the node should be considered pinned and unmovable
 *                 0 in all other cases
 *
 *-------------------------------------------------------------------------*/
static int NodePinned(Home_t *home, Node_t *node, int planeIndex,
                      real8 glidedir[3][3])
{
        int    i, j, k;
        int    planetest;
        real8  planetestmin, ptest, ptest2;
        real8  segplane[3];

/*
 *      If the node is pinned due to constraints no need to
 *      check anything else.
 */
        if (node->constraint == 7) {
            return(1);
        }

#ifdef _HALFSPACE
        if (node->constraint == 6) {
            return(1);
        }

#endif

/*
 *      If the node is not owned by the current domain, it
 *      may not be repositioned.
 */
        if (node->myTag.domainID != home->myDomain) {
            return(1);
        }

/*
 *      Loop over all segments attached to the node.
 */
        for (i = 0; i < node->numNbrs; i++) {

/*
 *          If the segment is owned by another domain, treat this node
 *          as 'pinned'... otherwise, the remote domain *could* use
 *          the segment in a collision but might not have the correct
 *          nodal position since no communication is done between
 *          the cross slip and collision handling procedures.
 *
 *          NOTE: during cross slip, we'll use the same segment
 *                ownership rules we use during collision handling.
 */
            if (!DomainOwnsSeg(home, OPCLASS_COLLISION, home->myDomain,
                               &node->nbrTag[i])) {
                return(1);
            }

/*
 *          Check the glide plane for the segment, and if the segment
 *          has a different glide plane index than <planeIndex>, the
 *          node should be considered 'pinned'
 */
            segplane[X] = node->nx[i];
            segplane[Y] = node->ny[i];
            segplane[Z] = node->nz[i];

            planetest = 0;
            planetestmin = 10.0;

            for (k = 0; k < 3; k++) {

                ptest = DotProduct(glidedir[k], segplane);
                ptest2 = ptest * ptest;

                if (ptest < planetestmin) {
                    planetest = k;
                    planetestmin = ptest2;
                }

            }

            if (planeIndex != planetest) {
                return(1);
            }
        }

        return(0);
}


static int NodePinned2(Home_t *home, Node_t *node, int planeIndex,
                      real8 glidedir[3][3])
{
        int    i, j, k;
        int    planetest;
        real8  planetestmin, ptest, ptest2;
        real8  segplane[3];

/*
 *      If the node is pinned due to constraints no need to
 *      check anything else.
 */
        if (node->constraint == 7) {
            return(1);
        }

/*
 *      If the node is not owned by the current domain, it
 *      may not be repositioned.
 */
        if (node->myTag.domainID != home->myDomain) {
            return(1);
        }

/*
 *      Loop over all segments attached to the node.
 */
        for (i = 0; i < node->numNbrs; i++) {

/*
 *          If the segment is owned by another domain, treat this node
 *          as 'pinned'... otherwise, the remote domain *could* use
 *          the segment in a collision but might not have the correct
 *          nodal position since no communication is done between
 *          the cross slip and collision handling procedures.
 *
 *          NOTE: during cross slip, we'll use the same segment
 *                ownership rules we use during collision handling.
 */
            if (!DomainOwnsSeg(home, OPCLASS_COLLISION, home->myDomain,
                               &node->nbrTag[i])) {
                return(1);
            }

/*
 *          Check the glide plane for the segment, and if the segment
 *          has a different glide plane index than <planeIndex>, the
 *          node should be considered 'pinned'
 */
            segplane[X] = node->nx[i];
            segplane[Y] = node->ny[i];
            segplane[Z] = node->nz[i];

            planetest = 0;
            planetestmin = 10.0;

            for (k = 0; k < 3; k++) {

                ptest = DotProduct(glidedir[k], segplane);
                ptest2 = ptest * ptest;

                if (ptest < planetestmin) {
                    planetest = k;
                    planetestmin = ptest2;
                }

            }

            if (planeIndex != planetest) {
                return(1);
            }
        }

        return(0);
}



/*---------------------------------------------------------------------------
 *
 *      Function:       CrossSlipFCC
 *
 *      Description:    Examines all nodes local to the domain, determines
 *                      if the node should cross slip, if the node is
 *                      permitted to cross slip, and if so, adjusts node
 *                      positions and segment glide planes as necessary.
 *
 *      Last Modified:  07/27/2009 - original version
 *
 *-------------------------------------------------------------------------*/
void CrossSlipFCC(Home_t *home)
{
        int    i, j, m, n;
        int    plane1, plane2, fplane;
        int    pinned1, pinned2;
        int    opClass, thisDom;
        int    resetNodePos, resetNbr1Pos, resetNbr2Pos;
        real8  tmp, eps, thetacrit, sthetacrit, s2thetacrit, areamin;
        real8  test1, test2, test3, testmax1, testmax2, testmax3;
        real8  vec1dotb, vec2dotb, vec3dotb, fdotglide;
        real8  nodep[3], nbr1p[3], nbr2p[3];
        real8  fnode[3], burg[3], vec1[3], vec2[3], vec3[3];
        real8  segplane1[3], segplane2[3], newplane[3];
        real8  tmp3[3], tmp3B[3], tmp3C[3];
        real8  glidedir[3][3];
        Node_t *node, *nbr1, *nbr2;
        Param_t *param;

        eps = 1.0e-06;
        thetacrit = 2.0 / 180.0 * M_PI;
        sthetacrit = sin(thetacrit);
        s2thetacrit = sthetacrit * sthetacrit;
        areamin = home->param->remeshAreaMin;

        param = home->param;
        thisDom = home->myDomain;

/*
 *      For purposes of determining segment ownership, we need to treat
 *      cross slip operations the same as we do during collision handling.
 */
        opClass = OPCLASS_COLLISION;

/* 
 *
 *      FIRST LOOP FOR REGULAR NODES
 *
 */


/*
 *      Loop through all 2-nodes native to this domain
 */
        for (i = 0; i < home->newNodeKeyPtr; i++) {

            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }

	    if (node->constraint != 0) continue; 

            if (node->numNbrs != 2) {
                continue;
            }

            burg[0] = node->burgX[0];
            burg[1] = node->burgY[0];
            burg[2] = node->burgZ[0];

            Normalize(&burg[X], &burg[Y], &burg[Z]);

/*
 *          Only consider glide dislocations
 */
            /* Burgers vectors should be of [1 1 0] type */
            if ( !( ( fabs(fabs(burg[X])-fabs(burg[Y])) < eps ) && ( fabs(burg[Z]) < eps ) ) &&
                 !( ( fabs(fabs(burg[Y])-fabs(burg[Z])) < eps ) && ( fabs(burg[X]) < eps ) ) &&
                 !( ( fabs(fabs(burg[Z])-fabs(burg[X])) < eps ) && ( fabs(burg[Y]) < eps ) )   ) {
                 printf("burg (%e,%e,%e) not of [1 1 0] type\n",burg[X],burg[Y],burg[Z]);
                 continue;
            }
            if ( ( fabs(burg[X]) < eps ) && ( fabs(burg[Y]) < eps ) && ( fabs(burg[Z]) < eps ) ) {
                 continue;
            }

/*
 *          We must not change the glide plane for any segment
 *          that is not 'owned' by the node in the current domain.
 *          Otherwise, neighboring domains *could* change the
 *          glide plane for the same segment... this would be bad.
 */
            if ((!DomainOwnsSeg(home, opClass, thisDom, &node->nbrTag[0])) ||
                (!DomainOwnsSeg(home, opClass, thisDom, &node->nbrTag[1]))) {
                continue;
            }

            nbr1 = GetNeighborNode(home, node, 0);
            nbr2 = GetNeighborNode(home, node, 1);

/*
 *          Get the positions of all three nodes and convert the neighbor
 *          node positions to the PBC coordinates relative to the main
 *          node.  These adjusted positions will be used during the
 *          cross slip algorithm, and if updated during the process, will
 *          be copied back into the respective nodal data structures and
 *          shifted back into the primary image space.
 */
            nodep[X] = node->x; nodep[Y] = node->y; nodep[Z] = node->z;
            nbr1p[X] = nbr1->x; nbr1p[Y] = nbr1->y; nbr1p[Z] = nbr1->z;
            nbr2p[X] = nbr2->x; nbr2p[Y] = nbr2->y; nbr2p[Z] = nbr2->z;

            PBCPOSITION(param, nodep[X], nodep[Y], nodep[Z],
                        &nbr1p[X], &nbr1p[Y], &nbr1p[Z]);
            PBCPOSITION(param, nodep[X], nodep[Y], nodep[Z],
                        &nbr2p[X], &nbr2p[Y], &nbr2p[Z]);

            resetNodePos = 0;
            resetNbr1Pos = 0;
            resetNbr2Pos = 0;

/*
 *          If the node is a point on a long screw then we can consider
 *          it for possible cross slip.
 */
            vec1[X] = nbr1p[X] - nbr2p[X];
            vec1[Y] = nbr1p[Y] - nbr2p[Y];
            vec1[Z] = nbr1p[Z] - nbr2p[Z];

            vec2[X] = nodep[X] - nbr1p[X];
            vec2[Y] = nodep[Y] - nbr1p[Y];
            vec2[Z] = nodep[Z] - nbr1p[Z];

            vec3[X] = nodep[X] - nbr2p[X];
            vec3[Y] = nodep[Y] - nbr2p[Y];
            vec3[Z] = nodep[Z] - nbr2p[Z];

            fnode[X] = node->fX;
            fnode[Y] = node->fY;
            fnode[Z] = node->fZ;

/*
 *          Calculate some test conditions
 */
            test1 = DotProduct(vec1, burg);
            test2 = DotProduct(vec2, burg);
            test3 = DotProduct(vec3, burg);

            test1 = test1 * test1;
            test2 = test2 * test2;
            test3 = test3 * test3;

            testmax1 = DotProduct(vec1, vec1);
            testmax2 = DotProduct(vec2, vec2);
            testmax3 = DotProduct(vec3, vec3);

/*
 *          Find which glide planes the segments are on
 *          e.g. for burg = [ 1  1  0 ], the two glide directions are
 *                          [ 1 -1  2 ] and
 *                          [ 1 -1 -2 ] 
 */
            tmp = 1.0; 
            for (n = 0; n < 3; n++) {
               if ( fabs(burg[n]) > eps ) {
                  glidedir[0][n] = (burg[n] * tmp > 0) ? 1.0 : -1.0;
                  glidedir[1][n] = (burg[n] * tmp > 0) ? 1.0 : -1.0;
                  tmp = -1.0;
               } 
               else {
                  glidedir[0][n] =  2.0;
                  glidedir[1][n] = -2.0;
               }
            }
            /* for FCC there are only two slip planes for screw dislocation */
            glidedir[2][0] = glidedir[2][1] = glidedir[2][2] = 0.0;

            /* normalization */
            for (n = 0; n < 3; n++) {
                glidedir[0][n] *= sqrt(1.0/6.0);
                glidedir[1][n] *= sqrt(1.0/6.0);
            }

#if 0 /* debug */
            printf(" burg = [ %e %e %e ]\n", burg[X], burg[Y], burg[Z]);
            printf(" dir1 = [ %e %e %e ]\n", glidedir[0][X], glidedir[0][Y], glidedir[0][Z]);
            printf(" dir2 = [ %e %e %e ]\n", glidedir[1][X], glidedir[1][Y], glidedir[1][Z]);
#endif
            segplane1[X] = node->nx[0];
            segplane1[Y] = node->ny[0];
            segplane1[Z] = node->nz[0];

            segplane2[X] = node->nx[1];
            segplane2[Y] = node->ny[1];
            segplane2[Z] = node->nz[1];

            Matrix33Vector3Multiply(glidedir, segplane1, tmp3);
            Matrix33Vector3Multiply(glidedir, segplane2, tmp3B);
            Matrix33Vector3Multiply(glidedir, fnode, tmp3C);

            plane1 = 0;
            plane2 = 0;
            fplane = 0;

            /* for FCC there are only two slip planes for screw dislocation */
            for (j = 1; j < 2; j++) {
                plane1 = (fabs(tmp3[j])  < fabs(tmp3[plane1]) ) ? j : plane1;
                plane2 = (fabs(tmp3B[j]) < fabs(tmp3B[plane2])) ? j : plane2;
                fplane = (fabs(tmp3C[j]) > fabs(tmp3C[fplane])) ? j : fplane;
            }

            NormalizedCrossVector(burg, glidedir[fplane], newplane);

#if 0 /* debug */
            if (((testmax2 - test2) < (4.0 * testmax2 * s2thetacrit)) &&
                ((testmax3 - test3) < (4.0 * testmax3 * s2thetacrit)) &&
                ((testmax1 - test1) < (testmax1 * s2thetacrit)) && 
                (node->constraint != 7) ) 
            {
                 printf("test2 (%e, %e)  test3 (%e, %e)  test1 (%e, %e)  node->constraint = %d\n",
                        (testmax2 - test2), (4.0 * testmax2 * s2thetacrit),
                        (testmax3 - test3), (4.0 * testmax3 * s2thetacrit),
                        (testmax1 - test1), (testmax1 * s2thetacrit),
                        node->constraint );
                 printf("plane1 = %d  plane2 = %d  fplane = %d\n", plane1,plane2,fplane);
                 printf("tmp3  = (%e, %e, %e)\n", tmp3[0],tmp3[1],tmp3[2]);
                 printf("tmp3B = (%e, %e, %e)\n", tmp3B[0],tmp3B[1],tmp3B[2]);
                 printf("tmp3C = (%e, %e, %e)\n", tmp3C[0],tmp3C[1],tmp3C[2]);
                 printf("newplane = (%e, %e, %e)\n", newplane[0],newplane[1],newplane[2]);
            }
#endif                   


            if (((testmax2 - test2) < (4.0 * testmax2 * s2thetacrit)) &&
                ((testmax3 - test3) < (4.0 * testmax3 * s2thetacrit)) &&
                ((testmax1 - test1) < (testmax1 * s2thetacrit)) &&
                (plane1 == plane2) && (plane1 != fplane)) {
/*
 *              Both segments are close to screw and the average direction
 *              is close to screw.
 *
 *              Determine if the neighbor nodes should be considered
 *              immobile.
 */

                pinned1 = NodePinned(home, nbr1, plane1, glidedir);
                pinned2 = NodePinned(home, nbr2, plane2, glidedir);

#if 1 /* debug */
                if (node->constraint != 7)
                  printf("segment close to screw orientation pinned1 = %d  pinned2 = %d constraint = %d\n",
                          pinned1,pinned2,node->constraint);
#endif

                if (pinned1) {
                    if (!pinned2) {
/*
 *                      Neighbor 2 can be moved, so proceed with the cross-slip
 *                      operation.
 */
                        vec1dotb = DotProduct(vec1, burg);

                        nbr2p[X] = nbr1p[X] - (vec1dotb * burg[X]);
                        nbr2p[Y] = nbr1p[Y] - (vec1dotb * burg[Y]);
                        nbr2p[Z] = nbr1p[Z] - (vec1dotb * burg[Z]);

                        vec2dotb = DotProduct(vec2, burg);

                        nodep[X] = nbr1p[X] + (vec2dotb * burg[X]);
                        nodep[Y] = nbr1p[Y] + (vec2dotb * burg[Y]);
                        nodep[Z] = nbr1p[Z] + (vec2dotb * burg[Z]);

                        fdotglide = DotProduct(fnode, glidedir[fplane]);
                        tmp = areamin / fabs(vec1dotb) * 2.0 *
                               (1.0 + eps) * Sign(fdotglide);

                        nodep[X] += tmp * glidedir[fplane][X];
                        nodep[Y] += tmp * glidedir[fplane][Y];
                        nodep[Z] += tmp * glidedir[fplane][Z];

                        resetNbr2Pos = 1;
                        resetNodePos = 1;

/*
 *                      Now reset the glide plane for both segments
 */
                        ResetGlidePlane(home, newplane, &node->myTag,
                                        &nbr1->myTag, 1);
                        ResetGlidePlane(home, newplane, &node->myTag,
                                        &nbr2->myTag, 1);
                    }
                } else {
/*
 *                  Neighbor 1 can be moved, so proceed with the cross-slip
 *                  operation.
 */
                        vec1dotb = DotProduct(vec1, burg);

                        nbr1p[X] = nbr2p[X] + (vec1dotb * burg[X]);
                        nbr1p[Y] = nbr2p[Y] + (vec1dotb * burg[Y]);
                        nbr1p[Z] = nbr2p[Z] + (vec1dotb * burg[Z]);

                        vec3dotb = DotProduct(vec3, burg);

                        nodep[X] = nbr2p[X] + (vec3dotb * burg[X]);
                        nodep[Y] = nbr2p[Y] + (vec3dotb * burg[Y]);
                        nodep[Z] = nbr2p[Z] + (vec3dotb * burg[Z]);

                        fdotglide = DotProduct(fnode, glidedir[fplane]);
                        tmp = areamin / fabs(vec1dotb) * 2.0 *
                               (1.0 + eps) * Sign(fdotglide);

                        nodep[X] += tmp * glidedir[fplane][X];
                        nodep[Y] += tmp * glidedir[fplane][Y];
                        nodep[Z] += tmp * glidedir[fplane][Z];

                        resetNbr1Pos = 1;
                        resetNodePos = 1;

/*
 *                      Now reset the glide plane for both segments
 */
                        ResetGlidePlane(home, newplane, &node->myTag,
                                        &nbr1->myTag, 1);
                        ResetGlidePlane(home, newplane, &node->myTag,
                                        &nbr2->myTag, 1);
                }
            } else if (((testmax2 - test2) < (testmax2 * s2thetacrit)) &&
                       ((testmax3 - test3) < (testmax3 * s2thetacrit)) &&
                       (plane2 != fplane) && (plane1 != fplane)) {
/*
 *              Segments are on different planes than each other
 *
 *              Determine if the neighbor nodes should be considered
 *              immobile.
 */

                pinned1 = NodePinned(home, nbr1, plane1, glidedir);
                pinned2 = NodePinned(home, nbr2, plane2, glidedir);

                if ((!pinned1) && (!pinned2)) {

                    vec2dotb = DotProduct(vec2, burg);

                    nbr1p[X] = nodep[X] - (vec2dotb * burg[X]);
                    nbr1p[Y] = nodep[Y] - (vec2dotb * burg[Y]);
                    nbr1p[Z] = nodep[Z] - (vec2dotb * burg[Z]);

                    vec3dotb = DotProduct(vec3, burg);

                    nbr2p[X] = nodep[X] - (vec3dotb * burg[X]);
                    nbr2p[Y] = nodep[Y] - (vec3dotb * burg[Y]);
                    nbr2p[Z] = nodep[Z] - (vec3dotb * burg[Z]);

		    vec1dotb = DotProduct(vec1, burg);
                    fdotglide = DotProduct(fnode, glidedir[fplane]);
                    tmp = areamin / fabs(vec1dotb) * 2.0 *
                           (1.0 + eps) * Sign(fdotglide);

                    nodep[X] += tmp * glidedir[fplane][X];
                    nodep[Y] += tmp * glidedir[fplane][Y];
                    nodep[Z] += tmp * glidedir[fplane][Z];

                    resetNbr1Pos = 1;
                    resetNbr2Pos = 1;
                    resetNodePos = 1;

/*
 *                  Now reset the glide plane for both segments
 */
                    ResetGlidePlane(home, newplane, &node->myTag,
                                    &nbr1->myTag, 1);
                    ResetGlidePlane(home, newplane, &node->myTag,
                                    &nbr2->myTag, 1);
                }
            } else if (((testmax2 - test2) < (testmax2 * sthetacrit)) &&
                       (plane1 != plane2) && (plane2 == fplane)) {

/*
 *              Zipper condition met for first segment.  If the first
 *              neighbor is not pinned, proceed with the cross-slip event
 */
                pinned1 = NodePinned(home, nbr1, plane1, glidedir);

                if (!pinned1) {

                    vec2dotb = DotProduct(vec2, burg);

                    nbr1p[X] = nodep[X] - (vec2dotb * burg[X]);
                    nbr1p[Y] = nodep[Y] - (vec2dotb * burg[Y]);
                    nbr1p[Z] = nodep[Z] - (vec2dotb * burg[Z]);

                    resetNbr1Pos = 1;

                    ResetGlidePlane(home, newplane, &node->myTag,
                                    &nbr1->myTag, 1);
                }

            } else if (((testmax3 - test3) < (testmax3 * sthetacrit)) &&
                       (plane1 != plane2) && (plane1 == fplane)) {

/*
 *              Zipper condition met for second segment
 */
                pinned2 = NodePinned(home, nbr2, plane2, glidedir);

                if (!pinned2) {

                    vec3dotb = DotProduct(vec3, burg);

                    nbr2p[X] = nodep[X] - (vec3dotb * burg[X]);
                    nbr2p[Y] = nodep[Y] - (vec3dotb * burg[Y]);
                    nbr2p[Z] = nodep[Z] - (vec3dotb * burg[Z]);

                    resetNbr2Pos = 1;

                    ResetGlidePlane(home, newplane, &node->myTag,
                                    &nbr2->myTag, 1);
                }
            }

/*
 *          If any of the nodes were repositioned, shift the new coordinates
 *          back into the primary image space and update the corresponding
 *          node structures.  The operation will also be sent to the
 *          remote domains for processing.
 */
            if (resetNodePos) {
                FoldBox(param, &nodep[X], &nodep[Y], &nodep[Z]);
                RepositionNode(home, nodep, &node->myTag, 1);
            }

            if (resetNbr1Pos) {
                FoldBox(param, &nbr1p[X], &nbr1p[Y], &nbr1p[Z]);
                RepositionNode(home, nbr1p, &nbr1->myTag, 1);
            }

            if (resetNbr2Pos) {
                FoldBox(param, &nbr2p[X], &nbr2p[Y], &nbr2p[Z]);
                RepositionNode(home, nbr2p, &nbr2->myTag, 1);
            }

        }  /* end loop over all nodes */




#ifdef _HALFSPACE


/* 
 *
 *      SECOND LOOP FOR SURFACE NODES 
 *
 */


/*
 *      Loop through all 2-nodes native to this domain
 */
        for (i = 0; i < home->newNodeKeyPtr; i++) 
	  {

            if ((node = home->nodeKeys[i]) == (Node_t *)NULL)
	      continue;
	    
	    if (node->constraint != 6) continue; 
            if (node->numNbrs != 1) continue;

            burg[0] = node->burgX[0];
            burg[1] = node->burgY[0];
            burg[2] = node->burgZ[0];

            Normalize(&burg[X], &burg[Y], &burg[Z]);

/*
 *          Only consider glide dislocations
 */
            /* Burgers vectors should be of [1 1 0] type */
            if ( !( ( fabs(fabs(burg[X])-fabs(burg[Y])) < eps ) && ( fabs(burg[Z]) < eps ) ) &&
                 !( ( fabs(fabs(burg[Y])-fabs(burg[Z])) < eps ) && ( fabs(burg[X]) < eps ) ) &&
                 !( ( fabs(fabs(burg[Z])-fabs(burg[X])) < eps ) && ( fabs(burg[Y]) < eps ) )   ) 
	      {
		printf("burg (%e,%e,%e) not of [1 1 0] type\n",burg[X],burg[Y],burg[Z]);
		continue;
	      }

            if ( ( fabs(burg[X]) < eps ) && ( fabs(burg[Y]) < eps ) && ( fabs(burg[Z]) < eps ) ) 
	      {
		continue;
	      }

/*
 *          We must not change the glide plane for any segment
 *          that is not 'owned' by the node in the current domain.
 *          Otherwise, neighboring domains *could* change the
 *          glide plane for the same segment... this would be bad.
 */
            if (!DomainOwnsSeg(home, opClass, thisDom, &node->nbrTag[0]))
                continue;

            nbr1 = GetNeighborNode(home, node, 0);

/*
 *          Get the positions of all three nodes and convert the neighbor
 *          node positions to the PBC coordinates relative to the main
 *          node.  These adjusted positions will be used during the
 *          cross slip algorithm, and if updated during the process, will
 *          be copied back into the respective nodal data structures and
 *          shifted back into the primary image space.
 */
            nodep[X] = node->x; nodep[Y] = node->y; nodep[Z] = node->z;
            nbr1p[X] = nbr1->x; nbr1p[Y] = nbr1->y; nbr1p[Z] = nbr1->z;

            PBCPOSITION(param, nodep[X], nodep[Y], nodep[Z],
                        &nbr1p[X], &nbr1p[Y], &nbr1p[Z]);

            resetNodePos = 0;
            resetNbr1Pos = 0;

/*
 *          If the node is a point on a long screw then we can consider
 *          it for possible cross slip.
 */
            vec2[X] = nodep[X] - nbr1p[X];
            vec2[Y] = nodep[Y] - nbr1p[Y];
            vec2[Z] = nodep[Z] - nbr1p[Z];

            fnode[X] = node->fX;
            fnode[Y] = node->fY;
            fnode[Z] = node->fZ;

/*
 *          Calculate some test conditions
 */
            test2 = DotProduct(vec2, burg);
            test2 = test2 * test2;
            testmax2 = DotProduct(vec2, vec2);

/*
 *          Find which glide planes the segments are on
 *          e.g. for burg = [ 1  1  0 ], the two glide directions are
 *                          [ 1 -1  2 ] and
 *                          [ 1 -1 -2 ] 
 */
            tmp = 1.0; 
            for (n = 0; n < 3; n++) 
	      {
		if ( fabs(burg[n]) > eps ) 
		  {
		    glidedir[0][n] = (burg[n] * tmp > 0) ? 1.0 : -1.0;
		    glidedir[1][n] = (burg[n] * tmp > 0) ? 1.0 : -1.0;
		    tmp = -1.0;
		  } 
		else 
		  {
		    glidedir[0][n] =  2.0;
		    glidedir[1][n] = -2.0;
		  }
	      }
            /* for FCC there are only two slip planes for screw dislocation */
            glidedir[2][0] = glidedir[2][1] = glidedir[2][2] = 0.0;

            /* normalization */
            for (n = 0; n < 3; n++) 
	      {
		glidedir[0][n] *= sqrt(1.0/6.0);
                glidedir[1][n] *= sqrt(1.0/6.0);
	      }

#if 0 /* debug */
            printf(" burg = [ %e %e %e ]\n", burg[X], burg[Y], burg[Z]);
            printf(" dir1 = [ %e %e %e ]\n", glidedir[0][X], glidedir[0][Y], glidedir[0][Z]);
            printf(" dir2 = [ %e %e %e ]\n", glidedir[1][X], glidedir[1][Y], glidedir[1][Z]);
#endif
            segplane1[X] = node->nx[0];
            segplane1[Y] = node->ny[0];
            segplane1[Z] = node->nz[0];
	    
            Matrix33Vector3Multiply(glidedir, segplane1, tmp3);
            Matrix33Vector3Multiply(glidedir, fnode, tmp3C);
	    
            plane1 = 0;
            fplane = 0;

            /* for FCC there are only two slip planes for screw dislocation */
            for (j = 1; j < 2; j++) 
	      {
                plane1 = (fabs(tmp3[j])  < fabs(tmp3[plane1]) ) ? j : plane1;
                fplane = (fabs(tmp3C[j]) > fabs(tmp3C[fplane])) ? j : fplane;
	      }

            NormalizedCrossVector(burg, glidedir[fplane], newplane);


	    if (((testmax2 - test2) < (4.0 * testmax2 * s2thetacrit)) &&
		(plane1 != fplane))
	      {
/*
 *              Both segments are close to screw and the average direction
 *              is close to screw.
 *
 *              Determine if the neighbor nodes should be considered
 *              immobile.
 */

		pinned1 = NodePinned2(home, nbr1, plane1, glidedir);


		if (!pinned1) 
		  {

/*
 *                Proceed with the cross-slip operation:
 *                Only node is moved. The new cross-slip position
 *                should be on the surface
 */

		    real8 t = nodep[Z],q;
		    
		    vec2dotb = DotProduct(vec2, burg);
		    
		    nodep[X] = nbr1p[X] + (vec2dotb * burg[X]);
		    nodep[Y] = nbr1p[Y] + (vec2dotb * burg[Y]);
		    nodep[Z] = nbr1p[Z] + (vec2dotb * burg[Z]);
		    
		    fdotglide = DotProduct(fnode, glidedir[fplane]);
		    tmp = areamin / fabs(vec2dotb) * 2.0 *
		      (1.0 + eps) * Sign(fdotglide);
		    
		    nodep[X] += tmp * glidedir[fplane][X];
		    nodep[Y] += tmp * glidedir[fplane][Y];
		    nodep[Z] += tmp * glidedir[fplane][Z];
/* 
 *                Now project node back to surface 
 */
		    q = (t-nbr1p[Z])/(nodep[Z]-nbr1p[Z]);
		    
		    nodep[X] = nbr1p[X] + (nodep[X]-nbr1p[X])*q;
		    nodep[Y] = nbr1p[Y] + (nodep[Y]-nbr1p[Y])*q;
		    nodep[Z] = t;
		    
		    resetNodePos = 1;

		    
/*
 *                Now reset the glide plane for the segments
 */
		    ResetGlidePlane(home, newplane, &node->myTag,
				    &nbr1->myTag, 1);
		    
		  }
	      }
	    
/*
 *          If any of the nodes were repositioned, shift the new coordinates
 *          back into the primary image space and update the corresponding
 *          node structures.  The operation will also be sent to the
 *          remote domains for processing.
 */
	    if (resetNodePos) 
	      {
		FoldBox(param, &nodep[X], &nodep[Y], &nodep[Z]);
		RepositionNode(home, nodep, &node->myTag, 1);
	      }
	    
	  }  /* end loop over all nodes */

#endif

        return;
}
