/**************************************************************************
 *
 *      Module:  PartialForces.c -- This module contains functions
 *               for recalculating forces ONLY on segments that are
 *               attached to nodes that have been flagged for a force
 *               update.
 *
 *
 *      NOTE:  This module may contain blocks of code which are
 *             conditionally compiled based on the _FEM and _FEMIMGSTRESS
 *             definitions.  These pre-processor definitions are only
 *             set when ParaDiS is being coupled with some locally developed
 *             finite element modules which are not included as part of
 *             the ParaDiS release.  Therefore, these blocks of code
 *             can be ignored by non-LLNL developers.
 *
 *************************************************************************/
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#include "Home.h"
#include "Util.h"

#if defined _FEM | defined _FEMIMGSTRESS
#include "FEM.h"
#endif

#ifdef _HALFSPACE
#include "HS.h"
#endif

/*-------------------------------------------------------------------------
 *
 *      Function:     PartialForces
 *
 *      Description:  This function zeros out forces on any segments
 *                    attached to nodes that have been flagged to have
 *                    forces updated, then handles recalculation of
 *                    forces on those segments and exchanges the
 *                    forces as neccessqry with remote domains in the
 *                    same fashion as the full force calculation.
 *
 *-----------------------------------------------------------------------*/
#ifdef _HALFSPACE
void PartialForces(Home_t *home, HalfSpace_t *halfspace)
#else
void PartialForces(Home_t *home)
#endif
{
        int     i, j, armID, nbrArm, nbrIsLocal;
        real8   sigb[3], f1[3], f2[3];
        Node_t  *node, *nbr;
        Param_t *param;

        param      = home->param;
   
        TimerStart(home, CALC_FORCE);

/*
 *      If a node is flagged to have forces reset, zero out
 *      the node's total force and all arm specific forces.
 *      For other nodes, zero out only the arm specific
 *      forces for arms connected to the flagged nodes.
 */
        ZeroNodeForces(home, PARTIAL);

/*
 *      If elastic interaction is not enabled, use a simple line
 *      tension model for calculating forces.  (Useful for doing
 *      initial testing of code with a quick run.)
 */
        if (!param->elasticinteraction) {

            TimerStart(home, LOCAL_FORCE) ;

            for (i = 0; i < home->newNodeKeyPtr; i++) {

                if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                    continue;
                }

                for (armID = 0; armID < node->numNbrs; armID++) {

                    nbr = GetNeighborNode(home, node, armID);

                    if (((node->flags & NODE_RESET_FORCES) == 0) &&
                        ((nbr->flags & NODE_RESET_FORCES) == 0)) {
                        continue;
                    }

                    if ((nbr->myTag.domainID == home->myDomain)&&
                        (OrderNodes(node, nbr) >= 0)) continue;

#ifdef _HALFSPACE
                    LineTensionForce(home, halfspace,
				     node->x, node->y, node->z,
                                     nbr->x, nbr->y, nbr->z,
                                     node->burgX[armID], node->burgY[armID],
                                     node->burgZ[armID], f1, f2);
#else
                    LineTensionForce(home, node->x, node->y, node->z,
                                     nbr->x, nbr->y, nbr->z,
                                     node->burgX[armID], node->burgY[armID],
                                     node->burgZ[armID], f1, f2);
#endif

                    AddtoNodeForce(node, f1);
                    AddtoArmForce(node, armID, f1);

                    if (nbr->myTag.domainID == home->myDomain) {
                        AddtoNodeForce(nbr, f2);
                        nbrArm = GetArmID(home, nbr, node);
                        AddtoArmForce(nbr, nbrArm, f2);
                    }
                }
            }

            TimerStop(home, LOCAL_FORCE) ;
            TimerStop(home, CALC_FORCE);

            return;
        }


        TimerStart(home, REMOTE_FORCE) ;

/*
 *      To compute the remote stress, we need to
 *      - Initialize sigbRem 
 *      - Compute SigbRem for Rij tables.
 *
 *      When _HALFSPACE is enabled, then we also need 
 *      to add the HS image stress to each segment's sigbRem
 *      even to full N2 forces calculations, and even when FMM is on.
 *
 *      Exception:
 *        - If full n^2 seg/seg forces are being calculated, we don't
 *          do any remote force calcs... unless the HALFSPACE code is enabled,
 *          in which case we still need to factor in some remote stress.
 */

#ifdef _HALFSPACE
	InitSegSigbRem(home, PARTIAL);
#endif

/* 
 *          Below compute sigbRem when Full N2 forces is not
 *          enabled and when FMM is not enabled.
 */
#ifndef FULL_N2_FORCES
            if (!param->fmEnabled) {
	      ComputeSegSigbRem(home, PARTIAL);
	    }
#endif

/* 
 *    When _HALFSPACE is enabled, we ALWAYS conpute the HS contribution
 *    of sigbRem. sigbrem is used later in LocalSegForces.
 */

#ifdef _HALFSPACE
	ComputeHSSegSigbRem(home, halfspace, PARTIAL);
#endif

        TimerStop(home, REMOTE_FORCE) ;
       
/*
 *      Now handle all the force calculations that must be done by the
 *      local domain.  This includes self-force, PK force, and far-field
 *      forces for all native segments plus any segment-to-segment
 *      interactions for segment pairs 'owned' by this domain.
 *
 *      All force calulations for a given segment will be calculated
 *      only once, and the calculating domain will distribute calculated
 *      forces to remote domains as necessary.
 */
        TimerStart(home, LOCAL_FORCE) ;

#ifdef _HALFSPACE
#ifndef _NOVIRTUALSEG
	LocalVirtualSegForces(home, halfspace, PARTIAL);
	//virtual_segment_force(home,halfspace,PARTIAL);
#endif
#endif

#ifdef _HALFSPACE
        LocalSegForces(home, halfspace, PARTIAL);
#else
        LocalSegForces(home, PARTIAL);
#endif
        TimerStop(home, LOCAL_FORCE) ;

#ifdef _HALFSPACE
#ifdef _WRITENODEFORCE
	Write_Node_Force(home,"PartialForce");
#endif
#endif


/*
 *      We need to zero out the 'reset forces' flag for all nodes
 *      now; local and ghost nodes.
 */
        for (i=0; i<home->newNodeKeyPtr; i++) {
            if ((node = home->nodeKeys[i]) != (Node_t *)NULL) {
                node->flags &= ~NODE_RESET_FORCES;
            }
        }

        node = home->ghostNodeQ;

        while (node) {
            node->flags &= ~NODE_RESET_FORCES;
            node = node->next;
        }


        TimerStop(home, CALC_FORCE);

#if PARALLEL
#ifdef SYNC_TIMERS
        TimerStart(home, CALC_FORCE_BARRIER);
        MPI_Barrier(MPI_COMM_WORLD);
        TimerStop(home, CALC_FORCE_BARRIER);
#endif
#endif
        return;
}
