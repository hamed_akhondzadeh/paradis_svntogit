/***************************************************************************
 *
 *	Module:		ParadisProto.h
 *	Description:	This header is mainly just a dumping ground for
 *			the miscellaneous funtion prototypes that have
 *			not been included elsewhere.  This helps eliminate
 *			some of the compiler whines...
 *
 ***************************************************************************/
#ifndef _ParadisProto_h
#define _ParadisProto_h

#include "stdio.h"
#include "Tag.h"
#include "Home.h"

#ifdef _HALFSPACE
#include "HS.h"
#endif

#ifndef MAX
#define MAX(a,b) ((a)>(b)?(a):(b))
#endif

#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif


#ifdef __cplusplus
extern "C" void Getline(char *string, int len, FILE *fp);
#else
void Getline(char *string, int len, FILE *fp);
#endif

void StressDueToSeg(real8 px, real8 py, real8 pz,
                    real8 p1x, real8 p1y, real8 p1z,
                    real8 p2x, real8 p2y, real8 p2z,
                    real8 bx, real8 by, real8 bz,
                    real8 a, real8 MU, real8 NU, real8 *stress);

void AddTagMapping(Home_t *home, Tag_t *oldTag, Tag_t *newTag);
void AddtoArmForce(Node_t *node, int arm, real8 f[3]);
void AddtoNodeForce(Node_t *node, real8 f[3]);
void GetVelocityStatistics(Home_t *home);
void AssignNodeToCell(Home_t *home, Node_t *node);

#ifdef _HALFSPACE
void TrapezoidIntegrator(Home_t *home, HalfSpace_t *halfspace);
#else
void TrapezoidIntegrator(Home_t *home);
#endif

void BroadcastDecomp(Home_t *home, void *decomp);
int  CalcNodeVelocities(Home_t *home, int zeroOnErr);
void CellCharge(Home_t *home);
/*void ChooseCrossSlipPlane(Home_t *home,
	real8 Fx, real8 Fy, real8 Fz, real8 Fesc,
	real8  burgX01, real8  burgY01, real8  burgZ01,
	real8  normX01, real8  normY01, real8  normZ01,
	real8  L, real8 lsgn,
	real8  *normX,  real8  *normY,  real8 *normZ,
	real8  *constr, int *xslip); */
void CommSendGhosts(Home_t *home);
void CommSendVelocity(Home_t *home);
void ComputeForces(Home_t *home, Node_t *node1, Node_t *node2,
        Node_t *node3, Node_t *node4, real8 *f1, real8 *f2,
        real8 *f3, real8 *f4);
void ComputeSegSigbRem(Home_t *home, int reqType);
void CrossSlipFCC(Home_t *home);
void CrossSlipBCC(Home_t *home);

#ifdef _HALFSPACE
void DeltaPlasticStrain(Home_t *home, HalfSpace_t *halfspace);
#else
void DeltaPlasticStrain(Home_t *home);
#endif

void DistributeTagMaps(Home_t *home);
void EstRefinementForces(Home_t *home, Node_t *node1, Node_t *node2,
        real8 newPos[3], real8 vec[3], real8 f0Seg1[3], real8 f1Seg1[3],
        real8 f0Seg2[3], real8 f1Seg2[3]);
void EstCoarsenForces(Home_t *home, Node_t *node1, Node_t *node2,
        Node_t *node3, real8 f0Seg[3], real8 f1Seg[3]);
void FindFSegComb(Home_t *home, real8 p0[3], real8 p1[3], real8 p2[3],
        real8 burg1[3], real8 burg2[3], real8 fp0seg1[3],
        real8 fp1seg1[3], real8 fp1seg2[3], real8 fp2seg2[3],
        real8 f0new[3], real8 f1new[3]);
void FindSubFSeg(Home_t *home, real8 p1[3], real8 p2[3], real8 burg[3],
        real8 oldfp1[3], real8 oldfp2[3], real8 newpos[3],
        real8 f0seg1[3], real8 f1seg1[3], real8 f0seg2[3],
        real8 f1seg2[3]);


// ADDED SA : For LocalStress2 to work
//void IncrDomSegCommCnts(Home_t *home, Node_t *node1, Node_t *node2,
//			int *sendDomCnt, int **sendDomList,
//			int *msgCnts);
//int CellPriority(Home_t *home, int cellID1, int cellID2);


void FixRemesh(Home_t *home);
void FixVelocities(Home_t *home);

#ifdef _HALFSPACE
void ForwardEulerIntegrator(Home_t *home, HalfSpace_t *halfspace);
#else
void ForwardEulerIntegrator(Home_t *home);
#endif

void FreeCellCenters(void);
void FreeCorrectionTable(void);
void FreeInitArrays(Home_t *home, InData_t *inData);
void FreeInNodeArray(InData_t *inData, int numNodes);
void FreeRijm(void);
void FreeRijmPBC(void);
void GenerateOutput(Home_t *home, int stage);
void GetDensityDelta(Home_t *home);
void GetMinDist(
        real8 p1x, real8 p1y, real8 p1z, real8 v1x, real8 v1y, real8 v1z,
        real8 p2x, real8 p2y, real8 p2z, real8 v2x, real8 v2y, real8 v2z,
        real8 p3x, real8 p3y, real8 p3z, real8 v3x, real8 v3y, real8 v3z,
        real8 p4x, real8 p4y, real8 p4z, real8 v4x, real8 v4y, real8 v4z,
        real8 *dist2, real8 *ddist2dt, real8 *L1, real8 *L2);
void GetNbrCoords(Home_t *home, Node_t *node, int arm, real8 *x, real8 *y,
        real8 *z);
void HandleCollisions(Home_t *home);
void HeapAdd(int **heap, int *heapSize, int *heapCnt, int value);
int  HeapRemove(int *heap, int *heapCnt);
void InitRemoteDomains(Home_t *home);
void InputSanity(Home_t *home);

#ifdef _HALFSPACE
void LineTensionForce(Home_t *home, HalfSpace_t *halfspace, 
		      real8 x1, real8 y1, real8 z1,
		      real8 x2, real8 y2, real8 z2, 
		      real8 bx, real8 by, real8 bz,
		      real8 f1[3], real8 f2[3]);
#else
void LineTensionForce(Home_t *home, real8 x1, real8 y1, real8 z1,
        real8 x2, real8 y2, real8 z2, real8 bx, real8 by, real8 bz,
        real8 f1[3], real8 f2[3]);
#endif


#if _HALFSPACE
void LocalSegForces(Home_t *home, HalfSpace_t *halfspace, int reqType);
#else
void LocalSegForces(Home_t *home, int reqType);
#endif


void Migrate(Home_t *home);

#ifdef _HALFSPACE
void NodeForce(Home_t *home, HalfSpace_t *halfspace);
#else
void NodeForce(Home_t *home);
#endif

int  NodeOwnsSeg(Home_t *home, Node_t *node1, Node_t *node2);

#ifdef _HALFSPACE
void PartialForces(Home_t *home, HalfSpace_t *halfspace);
#else
void PartialForces(Home_t *home);
#endif

void ReadBoundaries(FILE *fp, int *nX, int *nY, int *nZ,
        real8 **xBounds, real8 ***yBounds, real8 ****zBounds);
void ReadNodeDataFile(Home_t *home, InData_t *inData, char *dataFile);

#ifdef _HALFSPACE
void ReevaluateForces(Home_t *home, HalfSpace_t *halfspace);
#else
void ReevaluateForces(Home_t *home);
#endif

void ReadVersionNum(FILE *fp, int *versionNum);
void RemapArmTag(Home_t *home, Tag_t *oldTag, Tag_t *newTag);

#ifdef  _HALFSPACE
void Remesh(Home_t *home, HalfSpace_t *halfspace);
#else
void Remesh(Home_t *home);
#endif

void RemeshRule_2(Home_t *home);
void RemeshRule_3(Home_t *home);
void RestoreOldDecomp(Home_t *home, InData_t *inData);
void SegSegForce(real8 p1x, real8 p1y, real8 p1z,
	real8 p2x, real8 p2y, real8 p2z,
	real8 p3x, real8 p3y, real8 p3z,
	real8 p4x, real8 p4y, real8 p4z,
	real8 bpx, real8 bpy, real8 bpz,
	real8 bx, real8 by, real8 bz,
	real8 a, real8 MU, real8 NU,
	int seg12Local, int seg34Local,
	real8 *fp1x, real8 *fp1y, real8 *fp1z,
	real8 *fp2x, real8 *fp2y, real8 *fp2z,
	real8 *fp3x, real8 *fp3y, real8 *fp3z,
	real8 *fp4x, real8 *fp4y, real8 *fp4z);
void SetLatestRestart(char *fileName);
void SetOldVelocities(Home_t *home);

#ifdef _HALFSPACE
void SetOneNodeForce(Home_t *home, HalfSpace_t *halfspace, Node_t *node1);
#else
void SetOneNodeForce(Home_t *home, Node_t *node1);
#endif

void SetTimeStep(Home_t *home);
void SortNodesForCollision(Home_t *home);
void Tecplot(Home_t *home, char *baseFileName, int ioGroup, int firstInGroup,
        int writePrologue, int writeEpilogue, int numSegs);
void UniformDecomp(Home_t *home, void **decomp);
void WriteVelocity(Home_t *home, char *baseFileName, int ioGroup,
        int firstInGroup, int writePrologue, int writeEpilogue);
void ZeroNodeForces(Home_t *home, int reqType);


#ifdef CHECK_MEM_USAGE
void _CheckMemUsage(Home_t *home, char *msg);
#define CheckMemUsage(a,b) _CheckMemUsage((a),(b))
#else
#define CheckMemUsage(a,b) {}
#endif

#endif
