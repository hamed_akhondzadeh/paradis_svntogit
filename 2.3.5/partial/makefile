
#############################################################################
#
#    makefile: controls the build of various ParaDiS support utilities
#
#    Builds the following utilities:
#
#        paradisgen     --  problem generator
#        paradisrepart  --  creates a new problem decomposition with a new
#                           domain geometry from an previous nodal data file
#        paradisconvert --
#        ctablegen      --
#
############################################################################
include ../makefile.sys
include ../makefile.setup

## The following flags for data output

DEFS += -D_STACKINGFAULT

#DEFS += -D_WRITENODEFORCE

SRCDIR = ../src
INCDIR = ../include
BINDIR = ../bin

#
#	The utilities use various source modules from the parallel
#       code located in the parent directory.  Maintain a list of
#	these files.
#
#	These modules are compiled in the parent directory with a
#	different set of preprocessor definitions than are needed
#	here, so we need to create links in this directory back to
#	the source modlues and create separate object modules for
#	these sources.
#

EXTERN_C_SRCS = AddNode.c                \
      CellCharge.c             \
      CheckArms.c              \
      Collision.c              \
      CommSendGhosts.c         \
      CommSendMirrorNodes.c    \
      CommSendRemesh.c         \
      CommSendSecondaryGhosts.c \
      CommSendSegments.c       \
      CommSendVelocity.c       \
      CorrectionTable.c        \
      CrossSlipFCC.c           \
      CrossSlipBCC.c           \
      ParadisInit.c            \
      ParadisFinish.c          \
      Decomp.c                 \
      DeltaPlasticStrain.c     \
      DLBfreeOld.c             \
      deWitInteraction.c       \
      FccCrossSlip.c           \
      FMComm.c                 \
      FMSigma2.c               \
      FMSupport.c              \
      FreeInitArrays.c         \
      ForwardEulerIntegrator.c \
      GenerateOutput.c         \
      GetDensityDelta.c        \
      GetNativeNodes.c         \
      GetNewNativeNode.c       \
      GetNewGhostNode.c        \
      Gnuplot.c                \
      Heap.c                   \
      InitCellDomains.c        \
      InitCellNatives.c        \
      InitCellNeighbors.c      \
      InitHome.c               \
      InitRemoteDomains.c      \
       InputSanity.c         \
      LoadCurve.c              \
       Main.c      \
      Meminfo.c                \
      MemCheck.c               \
      Migrate.c                \
       MobilityLaw_BCC_0.c   \
      MobilityLaw_BCC_0b.c     \
      MobilityLaw_BCC_glide.c  \
       MobilityLaw_FCC_0.c   \
      MobilityLaw_FCC_2.c      \
      MobilityLaw_Relax.c      \
      NodeVelocity.c           \
      Parse.c                  \
      ParadisStep.c            \
      PartialForces.c       \
       Plot.c                \
      QueueOps.c               \
      ReadBinaryRestart.c      \
      Remesh.c              \
      RBDecomp.c               \
      RSDecomp.c               \
      RemapInitialTags.c       \
      RemeshRule_2.c           \
      RemeshRule_3.c           \
      RemoteSegForces.c        \
      ResetNodeArmForce.c      \
       SegmentStress.c       \
      SortNativeNodes.c        \
      SortNodesForCollision.c  \
      Tecplot.c                \
      Timer.c                  \
      TrapezoidIntegrator.c    \
      WriteArms.c              \
      WriteAtomEye.c           \
      WriteBinaryRestart.c     \
      WriteDensFlux.c          \
      WriteDensityField.c      \
      WriteFragments.c         \
      WriteForceTimes.c        \
      WritePoleFig.c           \
      WritePovray.c            \
      WriteProp.c              \
      WriteTSB.c               \
      WriteVelocity.c          

EXTERN_CPP_SRCS = DisplayC.C       \
                  display.C 

EXTERN_SRCS = $(EXTERN_C_SRCS) $(EXTERN_CPP_SRCS)
EXTERN_OBJS = $(EXTERN_C_SRCS:.c=.o) $(EXTERN_CPP_SRCS:.C=.o)

#
#	Define the exectutable, source and object modules for
#	the problem generator.
#

PARADISPARTIAL     = paradispartial
PARADISPARTIAL_BIN = $(BINDIR)/$(PARADISPARTIAL)

PARADISPARTIAL_C_SRCS =                  \
                   FixRemesh.c           \
                   Initialize.c          \
                   InitSendDomains.c     \
                   LocalSegForces.c      \
                   NodeForce.c           \
                   Param.c               \
                   ReadRestart.c         \
                   RemoveNode.c          \
                   Topology.c            \
                   Util.c                \
                   WriteRestart.c        

PARADISPARTIAL_INCS = -I Include 
PARADISPARTIAL_LIBS = 

PARADISPARTIAL_SRCS = $(PARADISPARTIAL_C_SRCS) $(PARADISPARTIAL_CPP_SRCS)
PARADISPARTIAL_OBJS = $(PARADISPARTIAL_C_SRCS:.c=.o) $(PARADISPARTIAL_CPP_SRCS:.C=.o)



###########################################################################
#
#	Define a rule for converting .c files to object modules.
#	All modules are compile serially in this directory
#
###########################################################################

.c.o:		makefile ../makefile.sys ../makefile.setup
		$(CC) $(OPT) $(CCFLAG) $(PARADISPARTIAL_INCS) $(INCS) -c $<

.C.o:		makefile ../makefile.sys ../makefile.setup
		$(CPP) $(OPT) $(CPPFLAG) $(INCS) -c $<


###########################################################################
#
#	Define all targets and dependencies below
#
###########################################################################

all:		$(EXTERN_OBJS) $(PARADISPARTIAL) 

clean:
		rm -f *.o $(EXTERN_SRCS) $(PARADISPARTIAL_BIN)

depend:		 *.c $(SRCDIR)/*.c $(INCDIR)/*.h makefile
		makedepend -Y$(INCDIR) *.c  -fmakefile.dep

#
#	Create any necessary links in the current directory to source
#	modules located in the SRCDIR directory
#

$(EXTERN_SRCS): $(SRCDIR)/$@
		- @ ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1

# For vip
#$(EXTERN_SRCS): $(SRCDIR)/$@
#                ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1

$(PARADISPARTIAL):	$(PARADISPARTIAL_BIN)
$(PARADISPARTIAL_BIN): $(PARADISPARTIAL_SRCS) $(PARADISPARTIAL_OBJS) $(EXTERN_OBJS) $(HEADERS)
		$(CPP) $(OPT) $(PARADISPARTIAL_OBJS) $(EXTERN_OBJS) -o $@  $(LIB) $(PARADISPARTIAL_LIBS)

