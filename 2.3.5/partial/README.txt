screw dissociation (tests/partial_fr_src.ctrl) and 
LC junction (tests/partial_LC_junction.ctrl) test cases work,
provided that SplitMultiNode is turned off
(by #define MULTI_NODE_SPLIT_FREQ 1000000 in Topology.c).

Relaxed junction structure incorrect (not asymmetric as expected).
Need to fix by setting Ecore of different dislocations separately.

Need to plot dislocation lines with colors for different Burgers
vector magnitudes.

