#include <sys/types.h>
#include <sys/stat.h>
#include "Home.h"
#include "Util.h"
        
/*---------------------------------------------------------------------------
 *
 *      Function:    WriteTSB
 *      Description: Write all nodal data ito TeraScale Browser (TSB)
 *                   format for visualization
 *
 *      Args:
 *          baseFileName     Base name of the plot file.  Plot data
 *                           will be written to 1 or more file segments
 *                           named <baseFileName>.n
 *          ioGroup          I/O group number associated with this domain
 *          firstInGroup     1 if this domain is the first processor in
 *                           its I/O group, zero otherwise.
 *          writePrologue    1 if this process should write all needed
 *                           headers and do any initialization associated
 *                           with the plot file, zero otherwise
 *          writeEpilogue    1 if this process should write all needed
 *                           trailers and do any terminal processing
 *                           associated with the plot file, zero otherwise
 *          numArms          total number of arms == SUM(node->numNbrs) for
 *                           all nodes. 
 *
 * FIX ME! numArms only needed by task doing writePrologue.
 *
 *-------------------------------------------------------------------------*/
void WriteTSB(Home_t *home, char *baseFileName, int ioGroup, int firstInGroup,
                      int writePrologue, int writeEpilogue, int numArms)
{
        int     i, iarm, nbond, color, newNodeKeyPtr;
        int     radius;
        real8   ux, uy, uz, disMag;
        real8   bx, by, bz;
        real8   dx, dy, dz;
        real8   nbrx, nbry, nbrz;
        char    fileName[256];
        char    *Colors[5];
        Param_t *param;
        Node_t  *node, *nbrNode;
        FILE    *fp;
        struct stat statbuf;
        
        
        param=home->param;
        
        Colors[0] = "Blue";
        Colors[1] = "Green";
        Colors[2] = "Red";
        Colors[3] = "Yellow";
        Colors[4] = "White";
           
        radius = param->writetsbspec[1];

        if (radius <= 0) {
            radius = 50;
        }

/*
 *      Set data file name.  Only append a sequence number to
 *      the data file name if the data is to be spread across
 *      multiple files.
 */
        if (param->numIOGroups == 1) {
            snprintf(fileName, sizeof(fileName), "%s/%s", DIR_TSB,
                     baseFileName);
        } else {
            snprintf(fileName, sizeof(fileName), "%s/%s.%d", DIR_TSB,
                     baseFileName, ioGroup);
        }

#ifdef PARALLEL
#ifdef DO_IO_TO_NFS
/*
 *      It appears that when multiple processes on different hosts
 *      write to the same NFS-mounted file (even if access is
 *      sequentialized), consistency problems can arise resulting
 *      in corrupted data files.  Explicitly doing a 'stat' of the
 *      file on each process immediately before opening it *seems*
 *      to clear up the problem.
 */
        memset(&statbuf, 0, sizeof(statbuf));
        (void) stat(fileName, &statbuf);
#endif
#endif

/*
 *      If this process is the first member of its I/O group
 *      it needs to create the output file
 */
        if (firstInGroup) {

/*
 *          First task in the I/O group must open the data file for writing
 *          to overwrite any existing file of the same name.
 */
            if ((fp = fopen(fileName, "w")) == (FILE *)NULL) {
                Fatal("WriteTSB: Open error %d on %s\n", errno, fileName);
            }

            if (writePrologue) {
                printf(" +++ Writing TSB file(s) %s\n", baseFileName);
        
/*
 *              Print standard header for TSB file
 */
                fprintf(fp,
                        "600 600        // pixels\n"
                        "0  0  0        // viewPoint\n"
                        "0  0  0        // screenCenter\n"
                        "1     0     0  // horizontal direction\n"
                        "0 0 0 0.2 0.5  // lightSource,ambient,diff\n"
                        "8.000000 8.000000    // hScreenSize, vScreenSize\n"
                        "LightBlue 0.6 1.0    // background color\n"
                        "0.3  Green     // molecular bondradius, "
                        "molecular bond color\n"
                        "0              // natoms\n"
                        "0              // nfunctions\n");

                nbond = numArms / 2;

                fprintf(fp,"%d          // number of extra-bonds\n", nbond);
            }
        } else {
/*
 *          Any task NOT first in its I/O group must open the data file
 *          in an append mode so everything gets added to the end of
 *          the file.
 */
            if ((fp = fopen(fileName, "a")) == (FILE *)NULL) {
                Fatal("WriteTSB: Open error %d on %s\n", errno, fileName);
            }
        }


        newNodeKeyPtr = home->newNodeKeyPtr;
        
        for(i=0;i<newNodeKeyPtr;i++) {
        
            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }
/*
 *          Check each of the node's arms. If nbrTag is lower than myTag,
 *          print the arm info
 */
            for (iarm = 0; iarm < node->numNbrs; iarm++) {

                nbrNode = GetNeighborNode(home, node, iarm);

                if (OrderNodes(node, nbrNode) > 0) {
        
                    bx = node->burgX[iarm];
                    by = node->burgY[iarm];
                    bz = node->burgZ[iarm];
        
                    if ((fabs(fabs(bx)-fabs(by)) < 1e-3) &&
                        (fabs(fabs(bx)-fabs(bz)) < 1e-3)) {
                        color=1;
                    } else {
                        color=2;
                    }
                         
                    nbrx = nbrNode->x;
                    nbry = nbrNode->y;
                    nbrz = nbrNode->z;
        
                    dx = nbrx - node->x;
                    dy = nbry - node->y;
                    dz = nbrz - node->z;

                    ZImage(param, &dx, &dy, &dz);
                         
                    fprintf(fp, "%13.6e %13.6e %13.6e   "
                            "%13.6e %13.6e %13.6e  %4d  %s\n",
                            node->x, node->y, node->z,
                            node->x+dx, node->y+dy, node->z+dz,
                            radius, Colors[color]);
                }
            }
        }
           
        fclose(fp);
        
        return;
}
