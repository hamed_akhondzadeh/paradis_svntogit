/***************************************************************************
 *
 *  Module:      FEM_Main.c
 *  Description: main routine for ParaDiS-FEM simulation
 *  Updated:     Meijie Tang, Apr14, 2006. 
 *
 **************************************************************************/
#include <stdio.h>
#include <time.h>
#include "Home.h"
#include "Init.h"
#include "FEM.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef FPES_ON
#include <fpcontrol.h>
#endif

void ParadisInit(int argc, char *argv[],Home_t **homeptr);
void ParadisStep(Home_t *home);
void ParadisFinish(Home_t *home);

Home_t *home0; 

main (int argc, char *argv[])
{
        int     cycleEnd, memSize, initialDLBCycles;
        time_t  tp;
        Home_t  *home;
        Param_t *param;

/*
 *      On some systems, the getrusage() call made by Meminfo() to get
 *      the memory resident set size does not work properly.  In those
 *      cases, the function will try to return the current heap size 
 *      instead.  This initial call allows meminfo() to get a copy of
 *      the original heap pointer so subsequent calls can calculate the
 *      heap size by taking the diference of the original and current
 *      heap pointers.
 */
        Meminfo(&memSize);

/*
 *      on linux systems (e.g. MCR) if built to have floating point exceptions
 *      turned on, invoke macro to do so
 */
   
#ifdef FPES_ON
        unmask_std_fpes();
#endif

        ParadisInit(argc, argv, &home);
/*
 *      home0 is a global variable used to reference the home
 *      structure withou having to pass it through to all the
 *      FEM functions.
 */
	home0 = home;  

#ifdef _IMGSTRESS    
        FEM_Init();
        AdjustNodePosition(home, 0);
        Migrate(home);
        RecycleGhostNodes(home);
        SortNativeNodes(home);
        CommSendGhosts(home);
#endif
       
        home->cycle      = home->param->cycleStart;

        param            = home->param;
        cycleEnd         = param->cycleStart + param->maxstep;
        initialDLBCycles = param->numDLBCycles;

/*
 *      Perform the needed number (if any) of load-balance-only
 *      steps before doing the main processing loop.  These steps
 *      perform only the minimal amount of stuff needed to
 *      estimate per-process load, move boundaries and migrate
 *      nodes among processsors to get a good initial balance.
 */
        TimerStart(home, INITIALIZE);

        if ((home->myDomain == 0) && (initialDLBCycles != 0)) {
            time(&tp);
            printf("  +++ Beginning %d load-balancing steps at %s",
                   initialDLBCycles, asctime(localtime(&tp)));
        }

        while (param->numDLBCycles > 0) {
            ParadisStep(home);
            home->cycle++;
            param->numDLBCycles--;
        }

        if ((home->myDomain == 0) && (initialDLBCycles != 0)) {
            time(&tp);
            printf("  +++ Completed load-balancing steps at %s",
                   asctime(localtime(&tp)));
        }

        TimerStop(home, INITIALIZE);

/*
 *      The cycle number may have been incremented during the initial
 *      load-balance steps, so reset it to the proper starting
 *      value before entering the main processing loop.
 */
        TimerClearAll(home);
        home->cycle = home->param->cycleStart;

        while (home->cycle < cycleEnd) {

#ifdef _FEMSTRESS
            fem_set_step_(&home->cycle);
            FEM_Step();
#endif
            ParadisStep(home);
            TimerClearAll(home);
        }

        ParadisFinish(home);

        exit(0);
}
