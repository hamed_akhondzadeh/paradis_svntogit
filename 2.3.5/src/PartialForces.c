/**************************************************************************
 *
 *      Module:  PartialForces.c -- This module contains functions
 *               for recalculating forces ONLY on segments that are
 *               attached to nodes that have been flagged for a force
 *               update.
 *
 *
 *      NOTE:  This module may contain blocks of code which are
 *             conditionally compiled based on the _FEM and _FEMIMGSTRESS
 *             definitions.  These pre-processor definitions are only
 *             set when ParaDiS is being coupled with some locally developed
 *             finite element modules which are not included as part of
 *             the ParaDiS release.  Therefore, these blocks of code
 *             can be ignored by non-LLNL developers.
 *
 *************************************************************************/
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#include "Home.h"
#include "Util.h"

#if defined _FEM | defined _FEMIMGSTRESS
#include "FEM.h"
#endif


/*-------------------------------------------------------------------------
 *
 *      Function:     PartialForces
 *
 *      Description:  This function zeros out forces on any segments
 *                    attached to nodes that have been flagged to have
 *                    forces updated, then handles recalculation of
 *                    forces on those segments and exchanges the
 *                    forces as neccessqry with remote domains in the
 *                    same fashion as the full force calculation.
 *
 *-----------------------------------------------------------------------*/
void PartialForces(Home_t *home)
{
        int     i, j, armID, nbrArm, nbrIsLocal;
        real8   sigb[3], f1[3], f2[3];
        Node_t  *node, *nbr;
        Param_t *param;

        param      = home->param;
   
        TimerStart(home, CALC_FORCE);

/*
 *      If a node is flagged to have forces reset, zero out
 *      the node's total force and all arm specific forces.
 *      For other nodes, zero out only the arm specific
 *      forces for arms connected to the flagged nodes.
 */
        ZeroNodeForces(home, PARTIAL);

/*
 *      If elastic interaction is not enabled, use a simple line
 *      tension model for calculating forces.  (Useful for doing
 *      initial testing of code with a quick run.)
 */
        if (!param->elasticinteraction) {

            TimerStart(home, LOCAL_FORCE) ;

            for (i = 0; i < home->newNodeKeyPtr; i++) {

                if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                    continue;
                }

                for (armID = 0; armID < node->numNbrs; armID++) {

                    nbr = GetNeighborNode(home, node, armID);

                    if (((node->flags & NODE_RESET_FORCES) == 0) &&
                        ((nbr->flags & NODE_RESET_FORCES) == 0)) {
                        continue;
                    }

                    if ((nbr->myTag.domainID == home->myDomain)&&
                        (OrderNodes(node, nbr) >= 0)) continue;

                    LineTensionForce(home, node->x, node->y, node->z,
                                     nbr->x, nbr->y, nbr->z,
                                     node->burgX[armID], node->burgY[armID],
                                     node->burgZ[armID], f1, f2);

                    AddtoNodeForce(node, f1);
                    AddtoArmForce(node, armID, f1);

                    if (nbr->myTag.domainID == home->myDomain) {
                        AddtoNodeForce(nbr, f2);
                        nbrArm = GetArmID(home, nbr, node);
                        AddtoArmForce(nbr, nbrArm, f2);
                    }
                }
            }

            TimerStop(home, LOCAL_FORCE) ;
            TimerStop(home, CALC_FORCE);

            return;
        }


        TimerStart(home, REMOTE_FORCE) ;

/*
 *      If _FEM is defined, we need to add the FEM image stress
 *      to each segment's sigbRem.  If FMM code is also enabled, we'll
 *      need to explicitly initialize the sigbRem values before 
 *      computing the FEM contribution (if the FMM code is not enabled,
 *      ComputeSegSigbRem() will explicitly set the values, so no
 *      initialization is necessary).
 *
 *      NOTE: If full n^2 seg/seg forces are being calculated, we don't
 *      do any remote force calcs... unless the FEM code is hooked in,
 *      in which case we still need to factor in some Yoffe stress.
 */
#ifndef FULL_N2_FORCES
        if (param->fmEnabled == 0) {
            ComputeSegSigbRem(home, PARTIAL);
        }
#endif

        if ((param->zBoundType == Free) ||
            (param->yBoundType == Free) ||
            (param->xBoundType == Free)) {

#ifdef _FEM
/*
 *          With the FEM stuff hooked in, if we're not doing full n^2
 *          force calcs, we need to explicitly reinitialize the
 *          segment sigbRem values only if the fmm code is enable.
 *          If n^2 forces ARE being calculated, no remote forces
 *          are calculated, so the segbRem must be reinitialzied
 *          in all cases.
 */
#ifndef FULL_N2_FORCES
            if (param->fmEnabled) {
                InitSegSigbRem(home, PARTIAL);
            } 
#else
            InitSegSigbRem(home, PARTIAL);
#endif
            ComputeFEMSegSigbRem(home, PARTIAL);
#endif
        }

        TimerStop(home, REMOTE_FORCE) ;
       
/*
 *      Now handle all the force calculations that must be done by the
 *      local domain.  This includes self-force, PK force, and far-field
 *      forces for all native segments plus any segment-to-segment
 *      interactions for segment pairs 'owned' by this domain.
 *
 *      All force calulations for a given segment will be calculated
 *      only once, and the calculating domain will distribute calculated
 *      forces to remote domains as necessary.
 */
        TimerStart(home, LOCAL_FORCE) ;
        LocalSegForces(home, PARTIAL);
        TimerStop(home, LOCAL_FORCE) ;


/*
 *      We need to zero out the 'reset forces' flag for all nodes
 *      now; local and ghost nodes.
 */
        for (i=0; i<home->newNodeKeyPtr; i++) {
            if ((node = home->nodeKeys[i]) != (Node_t *)NULL) {
                node->flags &= ~NODE_RESET_FORCES;
            }
        }

        node = home->ghostNodeQ;

        while (node) {
            node->flags &= ~NODE_RESET_FORCES;
            node = node->next;
        }


        TimerStop(home, CALC_FORCE);

#if PARALLEL
#ifdef SYNC_TIMERS
        TimerStart(home, CALC_FORCE_BARRIER);
        MPI_Barrier(MPI_COMM_WORLD);
        TimerStop(home, CALC_FORCE_BARRIER);
#endif
#endif
        return;
}
