/***************************************************************************
 *
 *  Function    : ResetNodeArmForce
 *  Description : Zero out the segment force on all arms connected to node.
 *                This is used to force a reevaluation of segment forces
 *                during timestep subcycling
 *
 ***************************************************************************/

#include "Home.h"
#include "Util.h"

void ResetNodeArmForce (Home_t *home, Node_t *node)
{

   int iarm, jarm, j ;
   Node_t *nbr, *nbrNbr ;

   node->fX = node->fY = node->fZ = 0;
   for (iarm = 0 ; iarm < node->numNbrs ; iarm++) {

/* find the neighbor's index for this arm */

      nbr = GetNeighborNode (home, node, iarm) ;
      jarm = - 1 ;

      if(!Connected(nbr, node, &jarm))
      {
          Fatal ("ResetNodeArmForce: linkage broken\n") ;
      }
#if 0
      node->segFx[iarm] = node->segFy[iarm] = node->segFz[iarm] = 0.0 ;
       nbr->segFx[jarm] =  nbr->segFy[jarm] =  nbr->segFz[jarm] = 0.0 ;
#else
      node->sigbLoc[iarm*3]=node->sigbLoc[iarm*3+1]=node->sigbLoc[iarm*3+2]=0;
       nbr->sigbLoc[jarm*3]= nbr->sigbLoc[jarm*3+1]= nbr->sigbLoc[jarm*3+2]=0;
       nbr->fX = nbr->fY = nbr->fZ = 0;
#endif
   }
   return ;
}
