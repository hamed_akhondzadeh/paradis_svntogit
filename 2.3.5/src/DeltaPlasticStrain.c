/***************************************************************************
 *   
 *      Author:       Moono Rhee
 *
 *      Function:     DeltaPlasticStrain
 *
 *      Description:  Calculate the plastic strain increment
 *                    by dislocation motion
 *
 *      Last Modified:  01/08/2002 - original version
 *                      05/20/2002 - M. Rhee: revisited to include
 *                                   dislocation density
 *                      03/19/2004 - M. Hiratani: Double counting of nodes
 *                                   is removed.  Accordingly, dislocation
 *                                   density was recorrected.
 *                      ...
 *
 ***************************************************************************/
#include "Home.h"
#include "Node.h"
#include "Util.h"
#include "Mobility.h"


void DeltaPlasticStrain(Home_t *home)
{
        int     whichburg,btype,m;
        int     burgGroup, negCount, zeroCount;
        int     i,iii,jjj,i0,j, unitFlag, nc, tagindex[100],k,l, icheck;
        int     index,index2,nn,mm;
        real8   Lx,Ly,Lz,bx,by,bz,ex,ey,ez,hhx,hhy,hhz,aa0,areaSwept,delgam;
        real8   d0,dx,dy,dz,xix,xiy,xiz,size,Vxa,Vya,Vza,nx,ny,nz;
        real8   dyad[3][3],dstn[3][3],dspn[3][3],incstn[3][3];
        real8   dyad0[3][3],dyad1[3][3],dyad2[3][3],pstn[3][3];
        real8   localstrain[3][3], localeRate, tmpDstn;
        real8   bmagsq;
        real8   deltaDensity,eps2p,eps2m;
        real8   aa,anx[3],any[3],anz[3],delxnode,delynode,delznode; 
        real8   delxnbr,delynbr,delznbr,tmpx,tmpy,tmpz,zetaold[3],zeta[3];
        real8   fluxndens[10],factor0,sign0,sign1,sign2;
        real8   loctotslip[12],locscrslip[12],locedgslip[12];   
        real8   tx,ty,tz,sbsign,sb,sq75;
        real8   burgv[7][3],bnorm[7][3],nanv[3][3][4];
        real8   tanv[3][3][4],Ltot[4][4],fluxtot[4][7];
        real8   eps=1.e-12;
        real8   sqt2,sqt3,sqt6,deltax2x,deltax2y,deltax2z;
        real8   seg[3],seg2[3],segd,segd2;
        real8   Ltemp[3],Ltempmax,Ltempmin,Lmat[2][2],Lmatinv[2][2],Ltemp2[2];
        real8   deltax1x,deltax1y,deltax1z,brglinktemp[7],tmpmin,tmpmax;
        real8   segleft[3],segleft2[3],qs[3],smat[2][2];
        real8   smatinv[2][2],stemp[3],stemp2[2];
        Node_t  *node;
        Node_t  *nbr;
        Param_t *param;

 
        nanv[0][0][0]=  0.; nanv[0][1][0]=  1.; nanv[0][2][0]= -1.;
        nanv[1][0][0]= -1.; nanv[1][1][0]=  0.; nanv[1][2][0]=  1.;
        nanv[2][0][0]=  1.; nanv[2][1][0]= -1.; nanv[2][2][0]=  0.;
 
        nanv[0][0][1]=  0.; nanv[0][1][1]= -1.; nanv[0][2][1]=  1.;
        nanv[1][0][1]=  1.; nanv[1][1][1]=  0.; nanv[1][2][1]=  1.;
        nanv[2][0][1]= -1.; nanv[2][1][1]= -1.; nanv[2][2][1]=  0.;
 
        nanv[0][0][2]=  0.; nanv[0][1][2]= -1.; nanv[0][2][2]= -1.;
        nanv[1][0][2]=  1.; nanv[1][1][2]=  0.; nanv[1][2][2]= -1.;
        nanv[2][0][2]=  1.; nanv[2][1][2]=  1.; nanv[2][2][2]=  0.;
 
        nanv[0][0][3]=  0.; nanv[0][1][3]=  1.; nanv[0][2][3]=  1.;
        nanv[1][0][3]= -1.; nanv[1][1][3]=  0.; nanv[1][2][3]= -1.;
        nanv[2][0][3]= -1.; nanv[2][1][3]=  1.; nanv[2][2][3]=  0.;
 
 
 
        tanv[0][0][0] = -2.0; tanv[0][1][0] =  1.0; tanv[0][2][0] =  1.0;
        tanv[1][0][0] =  1.0; tanv[1][1][0] = -2.0; tanv[1][2][0] =  1.0;
        tanv[2][0][0] =  1.0; tanv[2][1][0] =  1.0; tanv[2][2][0] = -2.0;
 
        tanv[0][0][1] =  2.0; tanv[0][1][1] =  1.0; tanv[0][2][1] =  1.0;
        tanv[1][0][1] =  1.0; tanv[1][1][1] =  2.0; tanv[1][2][1] = -1.0;
        tanv[2][0][1] =  1.0; tanv[2][1][1] = -1.0; tanv[2][2][1] =  2.0;
 
        tanv[0][0][2] =  2.0; tanv[0][1][2] =  1.0; tanv[0][2][2] = -1.0;
        tanv[1][0][2] =  1.0; tanv[1][1][2] =  2.0; tanv[1][2][2] =  1.0;
        tanv[2][0][2] = -1.0; tanv[2][1][2] =  1.0; tanv[2][2][2] =  2.0;
 
        tanv[0][0][3] =  2.0; tanv[0][1][3] = -1.0; tanv[0][2][3] =  1.0;
        tanv[1][0][3] = -1.0; tanv[1][1][3] =  2.0; tanv[1][2][3] =  1.0;
        tanv[2][0][3] =  1.0; tanv[2][1][3] =  1.0; tanv[2][2][3] =  2.0;
 

        sqt2 = M_SQRT1_2;
        sqt6 = 1.0 / sqrt(6.0);
 
        for (i = 0; i < 4; i++) {
            for (j = 0; j < 4; j++) {
                Ltot[i][j] = 0.0;
            }
            for (j = 0; j < 7; j++) {
                fluxtot[i][j] = 0.0;
            }
        }
 
        for (i = 0; i < 7; i++) {
            brglinktemp[i]= -99;
        }
 
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                 for (k = 0; k < 4; k++) {
                     nanv[i][j][k] = nanv[i][j][k] * sqt2;
                     tanv[i][j][k] = tanv[i][j][k] * sqt6;
                 }
            }
        }
 
        param = home->param;

        Lx = param->Lx;
        Ly = param->Ly;
        Lz = param->Lz;

        for (i = 0; i < 3; i++){
            for (j = 0; j < 3; j++){
                dstn[i][j] =0.0;
                dspn[i][j]=0.0;
                pstn[i][j]=0.0;
            }
        }

        param->disloDensity = 0.0;

        for (i = 0; i < param->numBurgGroups; i++) {
            param->partialDisloDensity[i] = 0.0;
        }

        for (i=0; i < 6; i++) {
            param->delpStrain[i] = 0.0;                                         
        }                                                                


        bmagsq = param->burgMag * param->burgMag;
        aa = M_SQRT1_2;

        burgv[0][0] =  0.5; burgv[0][1] =  0.5; burgv[0][2] =  0.5;
        burgv[1][0] = -0.5; burgv[1][1] =  0.5; burgv[1][2] =  0.5;
        burgv[2][0] =  0.5; burgv[2][1] = -0.5; burgv[2][2] =  0.5;
        burgv[3][0] =  0.5; burgv[3][1] =  0.5; burgv[3][2] = -0.5;
        burgv[4][0] =  1.0; burgv[4][1] =  0.0; burgv[4][2] =  0.0;
        burgv[5][0] =  0.0; burgv[5][1] =  1.0; burgv[5][2] =  0.0;
        burgv[6][0] =  0.0; burgv[6][1] =  0.0; burgv[6][2] =  1.0;
    
        for (i = 0; i < 4; i++) { /* 111 type only; otherwise, should be 7 */
            for (j=0;j<3;j++) {
                 bnorm[i][j]=burgv[i][j] / sqrt(0.75);
            }
        }

        for (iii = 0; iii < home->newNodeKeyPtr; iii++){

            if ((node = home->nodeKeys[iii]) == (Node_t *)NULL) {
                continue;
            }

            nc = node->numNbrs;

            localstrain[0][0] = 0.0;
            localstrain[0][1] = 0.0;
            localstrain[0][2] = 0.0;

            localstrain[1][0] = 0.0;
            localstrain[1][1] = 0.0;
            localstrain[1][2] = 0.0;

            localstrain[2][0] = 0.0;
            localstrain[2][1] = 0.0;
            localstrain[2][2] = 0.0;
        
/*
 *          do it for nc=2 only since most strain is produced
 *          by free nodes
 */        
            for (jjj = 0; jjj < nc; jjj++) {

               nbr = GetNeighborNode(home, node, jjj);

               /* Avoid double counting */
               if (OrderNodes(node, nbr) != -1) {
                   continue;
               }
           
               bx = node->burgX[jjj];
               by = node->burgY[jjj];
               bz = node->burgZ[jjj];

               ex = nbr->x - node->oldx; 
               ey = nbr->y - node->oldy; 
               ez = nbr->z - node->oldz; 

               hhx = node->x - nbr->oldx;
               hhy = node->y - nbr->oldy;
               hhz = node->z - nbr->oldz;

               ZImage(param, &ex, &ey, &ez);
               ZImage(param, &hhx, &hhy, &hhz);

               nx = (ey*hhz - ez*hhy) * 0.5;
               ny = (ez*hhx - ex*hhz) * 0.5;
               nz = (ex*hhy - ey*hhx) * 0.5;
           
               dyad[0][0] = nx*bx; dyad[0][1] = nx*by; dyad[0][2] = nx*bz;
               dyad[1][0] = ny*bx; dyad[1][1] = ny*by; dyad[1][2] = ny*bz;
               dyad[2][0] = nz*bx; dyad[2][1] = nz*by; dyad[2][2] = nz*bz;
           
               for (k = 0; k < 3; k++){
                  for (l = 0; l < 3; l++){
                      tmpDstn = (dyad[l][k]+dyad[k][l])*0.5/param->simVol;
                      incstn[l][k] = tmpDstn;
                      dstn[l][k] += tmpDstn;
                      dspn[l][k] += (dyad[l][k]-dyad[k][l])*0.5/param->simVol;
                      localstrain[l][k] += tmpDstn;
                  }
               }

               /* Modified for flux decomposition calculation 06/22/04 M.Rhee */
               delxnode = node->x - node->oldx;
               delynode = node->y - node->oldy;
               delznode = node->z - node->oldz;
               delxnbr = nbr->x - nbr->oldx;
               delynbr = nbr->y - nbr->oldy;
               delznbr = nbr->z - nbr->oldz;

               ZImage(param, &delxnode, &delynode, &delznode);
               ZImage(param, &delxnbr, &delynbr, &delznbr);

               tmpx = nbr->oldx - node->oldx;
               tmpy = nbr->oldy - node->oldy;
               tmpz = nbr->oldz - node->oldz;

               ZImage(param, &tmpx, &tmpy, &tmpz);

               zetaold[0] = tmpx;
               zetaold[1] = tmpy;
               zetaold[2] = tmpz;

               tmpx = nbr->x - node->x;
               tmpy = nbr->y - node->y;
               tmpz = nbr->z - node->z;

               ZImage(param, &tmpx, &tmpy, &tmpz);

               zeta[0] = tmpx;
               zeta[1] = tmpy;
               zeta[2] = tmpz;

               size=sqrt(zeta[0]*zeta[0] + zeta[1]*zeta[1] + zeta[2]*zeta[2]);

/*
 *             deltaDensity = size/param->simVol/bmagsq;
 */
               deltaDensity = size * param->burgVolFactor;
               param->disloDensity += deltaDensity;
               Normalize(&nx, &ny, &nz);
           
               deltax1x= node->x - node->oldx;
               deltax1y= node->y - node->oldy;
               deltax1z= node->z - node->oldz;

               ZImage(param, &deltax1x, &deltax1y, &deltax1z);

               deltax2x = nbr->x - nbr->oldx;
               deltax2y = nbr->y - nbr->oldy;
               deltax2z = nbr->z - nbr->oldz;

               ZImage(param, &deltax2x, &deltax2y, &deltax2z);

               seg[0] = nbr->x - node->x;                                  
               seg[1] = nbr->y - node->y;    
               seg[2] = nbr->z - node->z;

               ZImage(param, &seg[0], &seg[1], &seg[2]);

               seg2[0]= nbr->oldx - node->oldx;
               seg2[1]= nbr->oldy - node->oldy;
               seg2[2]= nbr->oldz - node->oldz;

               ZImage(param, &seg2[0], &seg2[1], &seg2[2]);

/*
 *             In addition to tracking total dislocation density, we need
 *             track dislocation density for specific sets of burgers
 *             vectors.  The mobility type (BCC, FCC, etc) determine
 *             the number of such groupings and which burgers vectors
 *             are of interest.
 *
 *             *************************************************
 *             ***                                           ***
 *             ***                  IMPORTANT!               ***
 *             ***   If you change any groupings of burgers  ***
 *             ***   vectors below, you must change the      ***
 *             ***   DENSITY_FILE_VERSION number defined     ***
 *             ***   in WriteProp.c!                         ***
 *             ***                                           ***
 *             *************************************************
 */
               burgGroup = -1;

               switch (param->materialType) {
               case MAT_TYPE_BCC:
/*
 *                 For BCC mobility types we track density of the following
 *                 groupings of burgers vectors.  Any burgers vectors not
 *                 mentioned are ignored.
 *
 *                     group #     burgers vector types
 *                       0         [ 1 1 1] [-1-1-1]
 *                       1         [-1 1 1] [ 1-1-1]
 *                       2         [ 1-1 1] [-1 1-1]
 *                       3         [ 1 1-1] [-1-1 1]
 *                       4         [ 1 0 0] [-1 0 0]
 *                                 [ 0 1 0] [ 0-1 0]
 *                                 [ 0 0 1] [ 0 0-1]
 */
                   zeroCount = (bx == 0.0) + (by == 0.0) + (bz == 0.0);

                   switch(zeroCount) {
                   case 0:
                       /* all types  [-]1 [-]1 [-]1 */

                       tx = bx;  ty = by;  tz = bz;

                       if ((tx * ty * tz) < 0.0) {
                           tx = -bx;  ty = -by;  tz = -bz;
                       }

                       /* types -1  1  1 */
                       if ((ty < 0.0) && (tz < 0.0)) burgGroup = 1;

                       /* types  1 -1  1 */
                       if ((tx < 0.0) && (tz < 0.0)) burgGroup = 2;

                       /* types  1  1 -1 */
                       if ((tx < 0.0) && (ty < 0.0)) burgGroup = 3;

                       /* types  1  1  1 */
                       if ((tx > 0.0) && (ty > 0.0) && (tz > 0.0))
                           burgGroup = 0;

                       break;

                   case 2:
/*
 *                     one of 100 types; group all these into a single category
 */
                       burgGroup = 4;
                       break;

                   default:
/*
 *                     Ignore all other types of burgers vectors
 */
                       break;
                   } /* end switch (zeroCount) */

                   break;

               case MAT_TYPE_FCC:
/*
 *                 For FCC mobility types we track density of the following
 *                 groupings of burgers vectors.  Any burgers vectors not
 *                 mentioned are ignored.
 *
 *                     group #     burgers vector types
 *                       0         [ 1 1 0] [-1-1 0]
 *                       1         [-1 1 0] [ 1-1 0]
 *                       2         [ 1 0 1] [-1 0-1]
 *                       3         [-1 0 1] [ 1 0-1]
 *                       4         [ 0 1 1] [ 0-1-1]
 *                       5         [ 0-1 1] [ 0 1-1]
 *                       6         all others
 */

                   zeroCount = (bx == 0.0) + (by == 0.0) + (bz == 0.0);
                   negCount  = (bx <  0.0) + (by <  0.0) + (bz  < 0.0);

                   switch (zeroCount) {
                   case 1:

                       tx = bx;  ty = by;  tz = bz;

                       if (negCount > 1) {
                           tx = -bx;  ty = -by;  tz = -bz;
                       }

                       /* types  [ 1  1  0] [-1 -1  0] */
                       if ((tx > 0.0) && (ty > 0.0)) burgGroup = 0;

                       /* types  [ 1 -1  0] [-1  1  0] */
                       else if ((tx != 0.0) && (ty != 0.0) &&
                                ((tx * ty) < 0.0)) burgGroup = 1;

                       /* types  [ 1  0  1] [-1  0 -1] */
                       else if ((tx > 0.0) && (tz > 0.0)) burgGroup = 2;

                       /* types  [-1  0  1] [ 1  0 -1] */
                       else if ((tx != 0.0) && (tz != 0.0) &&
                                ((tx * tz) < 0.0)) burgGroup = 3;

                       /* types  [ 0  1  1] [ 0 -1 -1] */
                       else if ((ty > 0.0) && (tz > 0.0)) burgGroup = 4;

                       /* types  [ 0  1 -1] [ 0 -1  1] */
                       else if ((ty != 0.0) && (tz != 0.0) &&
                                ((ty * tz) < 0.0)) burgGroup = 5;

                       break;

                   default:
/*
 *                     All other types of burgers vectors get grouped together
 */
                       burgGroup = 6;
                       break;

                   }  /* end switch (zeroCount) */
                
                   break;

               default:
                   Fatal("Unkown mobilityType (%d) in DeltaPlasticStrain()",
                         param->mobilityType);
                   break;
               }

/*
 *             And, if the burger's vector fell into one of groups of
 *             burgers vectors whose density we're tracking, increment
 *             that group's density value.
 */
               if (burgGroup >= 0) {
                   param->partialDisloDensity[burgGroup] += deltaDensity;
               }

/*
 *             Do flux decomposition stuff if necessary
 */
               if (param->fluxfile != 1) {
                   continue;
               }
 
               if ((fabs(bx) * fabs(by) * fabs(bz)) < eps) {
                   continue;
               }

/*
 *             max index for 4 burgers vector for now
 */
               index = -1;
               tmpmax=0.0;

               for (m = 0; m < 4; m++) {
                   brglinktemp[m] = fabs(bnorm[m][0]*bx +
                                         bnorm[m][1]*by +
                                         bnorm[m][2]*bz);
                   if (brglinktemp[m] > tmpmax) {
                       tmpmax = brglinktemp[m];
                       index = m;
                   }
               }

               if (index < 0) {
                   continue;
               }

               sbsign = bnorm[index][0]*bx +
                        bnorm[index][1]*by +
                        bnorm[index][2]*bz;

               sb = ((sbsign < 0) ? -1.0 : 1.0);

/*
 *             segx vector and delta2 vector defined above
 */
               segd = bnorm[index][0]*seg[0] +
                      bnorm[index][1]*seg[1] +
                      bnorm[index][2]*seg[2];

/*
 *             segd is the dotted value of seg length with
 *             burgers vector => Screw density
 */
               Ltot[index][0] += fabs(segd);

               segleft[0] = seg[0] - segd * bnorm[index][0];
               segleft[1] = seg[1] - segd * bnorm[index][1];
               segleft[2] = seg[2] - segd * bnorm[index][2];
 
/*
 *             min index2
 */
               for (m = 0; m < 3; m++) {
                   Ltemp[m] = fabs(tanv[m][0][index]*segleft[0] +
                                   tanv[m][1][index]*segleft[1] +
                                   tanv[m][2][index]*segleft[2]);
               }

               Ltempmin = 1.e30;

               for (m = 0; m < 3; m++) {
                   if ( Ltemp[m] < Ltempmin) {
                       Ltempmin = Ltemp[m];
                       index2 = m;
                   }
               }
 
/*
 *             nonglide term first, non glide fluxes Eq. 14
 *
 *                (l_DD(t+dt) cross delx2) burg/burgmag / 2.0 / delta time
 *
 *             cross of (seg, delatx2)
 */
               xvector(seg[0], seg[1], seg[2], deltax2x, deltax2y, deltax2z,
                       &tmpx, &tmpy, &tmpz);

               fluxtot[index][0] += sb*0.5*(tmpx*bnorm[index][0] +
                                            tmpy*bnorm[index][1] +
                                            tmpz*bnorm[index][2]);

               if (index2 == 0) {
 
                   Lmat[0][0] = tanv[1][1][index];
                   Lmat[0][1] = tanv[1][2][index];
                   Lmat[1][0] = tanv[2][1][index];
                   Lmat[1][1] = tanv[2][2][index];

                   Matrix22Invert(Lmat, Lmatinv);

                   Ltemp2[0] = Lmatinv[0][0]*segleft[1] +
                               Lmatinv[0][1]*segleft[2];

                   Ltemp2[1] = Lmatinv[1][0]*segleft[1] +
                               Lmatinv[1][1]*segleft[2];

                   Ltot[index][2] += fabs(Ltemp2[0]);
                   Ltot[index][3] += fabs(Ltemp2[1]);

                   xvector(tanv[1][0][index], tanv[1][1][index],
                           tanv[1][2][index], deltax2x, deltax2y,
                           deltax2z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][2] += sb * 0.5 * Ltemp2[0] *
                                        (tmpx*nanv[1][0][index] +
                                         tmpy*nanv[1][1][index] +
                                         tmpz*nanv[1][2][index]);
       
                   xvector(tanv[2][0][index], tanv[2][1][index],
                           tanv[2][2][index], deltax2x, deltax2y,
                           deltax2z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][3] += sb * 0.5 * Ltemp2[1] *
                                        (tmpx*nanv[2][0][index] +
                                         tmpy*nanv[2][1][index] +
                                         tmpz*nanv[2][2][index]);
               } else if (index2 == 1) {
 
                   Lmat[0][0] = tanv[0][0][index];
                   Lmat[0][1] = tanv[0][2][index];
                   Lmat[1][0] = tanv[2][0][index];
                   Lmat[1][1] = tanv[2][2][index];

                   Matrix22Invert(Lmat, Lmatinv);

                   Ltemp2[0] = Lmatinv[0][0]*segleft[0] +
                               Lmatinv[0][1]*segleft[2];

                   Ltemp2[1] = Lmatinv[1][0]*segleft[0] +
                               Lmatinv[1][1]*segleft[2];

                   Ltot[index][1] += fabs(Ltemp2[0]);
                   Ltot[index][3] += fabs(Ltemp2[1]);

                   xvector(tanv[0][0][index], tanv[0][1][index],
                           tanv[0][2][index], deltax2x, deltax2y,
                           deltax2z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][1] += sb * 0.5 * Ltemp2[0] *
                                        (tmpx*nanv[0][0][index] +
                                         tmpy*nanv[0][1][index] +
                                         tmpz*nanv[0][2][index]);
	     
                   xvector(tanv[2][0][index], tanv[2][1][index],
                           tanv[2][2][index], deltax2x, deltax2y,
                           deltax2z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][3] += sb * 0.5 * Ltemp2[1] *
                                        (tmpx*nanv[2][0][index] +
                                         tmpy*nanv[2][1][index] +
                                         tmpz*nanv[2][2][index]);
               } else {
                   Lmat[0][0] = tanv[0][0][index];
                   Lmat[0][1] = tanv[0][1][index];
                   Lmat[1][0] = tanv[1][0][index];
                   Lmat[1][1] = tanv[1][1][index];

                   Matrix22Invert(Lmat, Lmatinv);

                   Ltemp2[0] = Lmatinv[0][0]*segleft[0] +
                               Lmatinv[0][1]*segleft[1];

                   Ltemp2[1] = Lmatinv[1][0]*segleft[0] +
                               Lmatinv[1][1]*segleft[1];

                   Ltot[index][1] += fabs(Ltemp2[0]);
                   Ltot[index][2] += fabs(Ltemp2[1]);

                   xvector(tanv[0][0][index], tanv[0][1][index],
                           tanv[0][2][index], deltax2x, deltax2y,
                           deltax2z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][1] += sb * 0.5 * Ltemp2[0] *
                                        (tmpx*nanv[0][0][index] +
                                         tmpy*nanv[0][1][index] +
                                         tmpz*nanv[0][2][index]);
 
                   xvector(tanv[1][0][index], tanv[1][1][index],
                           tanv[1][2][index], deltax2x, deltax2y,
                           deltax2z,&tmpx,&tmpy,&tmpz);

                   fluxtot[index][2] += sb * 0.5 * Ltemp2[1] *
                                        (tmpx*nanv[1][0][index] +
                                         tmpy*nanv[1][1][index] +
                                         tmpz*nanv[1][2][index]);
               }
 
/*
 *             Screw portion, first...
 */
               xvector(bnorm[index][0], bnorm[index][1], bnorm[index][2],
                       deltax2x, deltax2y, deltax2z, &tmpx, &tmpy, &tmpz);

               qs[0] = -sb * 0.5 * segd * tmpx;
               qs[1] = -sb * 0.5 * segd * tmpy;
               qs[2] = -sb * 0.5 * segd * tmpz;

               stemp[0] = fabs(nanv[0][0][index]*qs[0] +
                               nanv[0][1][index]*qs[1] +
                               nanv[0][2][index]*qs[2]);

               stemp[1] = fabs(nanv[1][0][index]*qs[0] +
                               nanv[1][1][index]*qs[1] +
                               nanv[1][2][index]*qs[2]);

               stemp[2] = fabs(nanv[2][0][index]*qs[0] +
                               nanv[2][1][index]*qs[1] +
                               nanv[2][2][index]*qs[2]);
 
/*
 *             Find min index2
 */
               if ((fabs(stemp[0]) < fabs(stemp[1])) &&
                   (fabs(stemp[0]) < fabs(stemp[2]))) {
                   index2 = 0;
               } else if ((fabs(stemp[1]) < fabs(stemp[0])) &&
                        (fabs(stemp[1]) < fabs(stemp[2]))) {
                   index2 = 1;
               } else {
                   index2 = 2;
               }
 
               if (index2 == 0) {
                   smat[0][0] = nanv[1][1][index];
                   smat[0][1] = nanv[1][2][index];
                   smat[1][0] = nanv[2][1][index];
                   smat[1][1] = nanv[2][2][index];

                   Matrix22Invert(smat, smatinv);

                   stemp2[0] = smatinv[0][0]*qs[1] + smatinv[0][1]*qs[2];
                   stemp2[1] = smatinv[1][0]*qs[1] + smatinv[1][1]*qs[2];
      
                   fluxtot[index][5] += stemp2[0];
                   fluxtot[index][6] += stemp2[1];
               } else if (index2 == 1 ) {
                   smat[0][0] = nanv[0][0][index];
                   smat[0][1] = nanv[0][2][index];
                   smat[1][0] = nanv[2][0][index];
                   smat[1][1] = nanv[2][2][index];

                   Matrix22Invert(smat, smatinv);

                   stemp2[0] = smatinv[0][0]*qs[0] + smatinv[0][1]*qs[2];
                   stemp2[1] = smatinv[1][0]*qs[0] + smatinv[1][1]*qs[2];
 
                   fluxtot[index][4] += stemp2[0];
                   fluxtot[index][6] += stemp2[1];
               } else {
                   smat[0][0] = nanv[0][0][index];
                   smat[0][1] = nanv[0][1][index];
                   smat[1][0] = nanv[1][0][index];
                   smat[1][1] = nanv[1][1][index];

                   Matrix22Invert(smat, smatinv);

                   stemp2[0] = smatinv[0][0]*qs[0] + smatinv[0][1]*qs[1];
                   stemp2[1] = smatinv[1][0]*qs[0] + smatinv[1][1]*qs[1];

                   fluxtot[index][4] += stemp2[0];
                   fluxtot[index][5] += stemp2[1];
               }
 
 
/********************* the other half ****************************/
 
/*
 *             segx vector and delta2 vector defined above
 */
               segd2 = bnorm[index][0]*seg2[0] +
                       bnorm[index][1]*seg2[1] +
                       bnorm[index][2]*seg2[2];

               segleft2[0] = seg2[0] - segd2 * bnorm[index][0];
               segleft2[1] = seg2[1] - segd2 * bnorm[index][1];
               segleft2[2] = seg2[2] - segd2 * bnorm[index][2];
 
 
/*
 *             min index2
 */
               for (m = 0; m < 3; m++) {
                   Ltemp[m] = fabs(tanv[m][0][index]*segleft2[0] +
                                   tanv[m][1][index]*segleft2[1] +
                                   tanv[m][2][index]*segleft2[2]);
               }

               Ltempmin = 1.0e30;

               for (m = 0; m < 3; m++) {
                   if (Ltemp[m] < Ltempmin) {
                       Ltempmin = Ltemp[m];
                       index2 = m;
                   }
               }
 
/*
 *             nonglide term first, non glide fluxes Eq. 14
 *
 *                 (l_DD(t+dt) cross delx2 ) burg/burgmag / 2.0 / delta time
 */
               xvector(seg2[0], seg2[1], seg2[2],
                       deltax1x, deltax1y, deltax1z,
                       &tmpx, &tmpy, &tmpz);

               fluxtot[index][0] += sb * 0.5 * (tmpx*bnorm[index][0] +
                                                tmpy*bnorm[index][1] +
                                                tmpz*bnorm[index][2]);
 
               if (index2 == 0) {
                   Lmat[0][0] = tanv[1][1][index];
                   Lmat[0][1] = tanv[1][2][index];
                   Lmat[1][0] = tanv[2][1][index];
                   Lmat[1][1] = tanv[2][2][index];

                   Matrix22Invert(Lmat, Lmatinv);

                   Ltemp2[0] = Lmatinv[0][0]*segleft2[1] +
                               Lmatinv[0][1]*segleft2[2];

                   Ltemp2[1] = Lmatinv[1][0]*segleft2[1] +
                               Lmatinv[1][1]*segleft2[2];
 
                   xvector(tanv[1][0][index], tanv[1][1][index],
                           tanv[1][2][index], deltax1x, deltax1y,
                           deltax1z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][2] += sb * 0.5 * Ltemp2[0] *
                                        (tmpx*nanv[1][0][index] +
                                         tmpy*nanv[1][1][index] +
                                         tmpz*nanv[1][2][index]);
 
                   xvector(tanv[2][0][index], tanv[2][1][index],
                           tanv[2][2][index], deltax1x, deltax1y,
                           deltax1z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][3] += sb * 0.5 * Ltemp2[1] *
                                        (tmpx*nanv[2][0][index] +
                                         tmpy*nanv[2][1][index] +
                                         tmpz*nanv[2][2][index]);
               } else if (index2 == 1) {
                   Lmat[0][0] = tanv[0][0][index];
                   Lmat[0][1] = tanv[0][2][index];
                   Lmat[1][0] = tanv[2][0][index];
                   Lmat[1][1] = tanv[2][2][index];

                   Matrix22Invert(Lmat, Lmatinv);

                   Ltemp2[0] = Lmatinv[0][0]*segleft2[0] +
                               Lmatinv[0][1]*segleft2[2];

                   Ltemp2[1] = Lmatinv[1][0]*segleft2[0] +
                               Lmatinv[1][1]*segleft2[2];

                   xvector(tanv[0][0][index], tanv[0][1][index],
                           tanv[0][2][index], deltax1x, deltax1y,
                           deltax1z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][1] += sb * 0.5 * Ltemp2[0] *
                                        (tmpx*nanv[0][0][index] +
                                         tmpy*nanv[0][1][index] +
                                         tmpz*nanv[0][2][index]);
 
                   xvector(tanv[2][0][index], tanv[2][1][index],
                           tanv[2][2][index], deltax1x, deltax1y,
                           deltax1z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][3] += sb * 0.5 * Ltemp2[1] *
                                        (tmpx*nanv[2][0][index] +
                                         tmpy*nanv[2][1][index] +
                                         tmpz*nanv[2][2][index]);
               } else {
                   Lmat[0][0] = tanv[0][0][index];
                   Lmat[0][1] = tanv[0][1][index];
                   Lmat[1][0] = tanv[1][0][index];
                   Lmat[1][1] = tanv[1][1][index];

                   Matrix22Invert(Lmat, Lmatinv);

                   Ltemp2[0] = Lmatinv[0][0]*segleft2[0] +
                               Lmatinv[0][1]*segleft2[1];

                   Ltemp2[1] = Lmatinv[1][0]*segleft2[0] +
                               Lmatinv[1][1]*segleft2[1];

                   xvector(tanv[0][0][index], tanv[0][1][index],
                           tanv[0][2][index], deltax1x, deltax1y,
                           deltax1z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][1] += sb * 0.5 * Ltemp2[0] *
                                        (tmpx*nanv[0][0][index] +
                                         tmpy*nanv[0][1][index] +
                                         tmpz*nanv[0][2][index]);
 
                   xvector(tanv[1][0][index], tanv[1][1][index],
                           tanv[1][2][index], deltax1x, deltax1y,
                           deltax1z, &tmpx, &tmpy, &tmpz);

                   fluxtot[index][2] += sb * 0.5 * Ltemp2[1] *
                                        (tmpx*nanv[1][0][index] +
                                         tmpy*nanv[1][1][index] +
                                         tmpz*nanv[1][2][index]);
               }
 
/*
 *             Screw portion, second...
 */
               xvector(bnorm[index][0], bnorm[index][1], bnorm[index][2],
                       deltax1x, deltax1y, deltax1z, &tmpx, &tmpy, &tmpz);

               qs[0] = -sb * 0.5 * segd2 * tmpx;
               qs[1] = -sb * 0.5 * segd2 * tmpy;
               qs[2] = -sb * 0.5 * segd2 * tmpz;

               stemp[0] = fabs(nanv[0][0][index]*qs[0] +
                               nanv[0][1][index]*qs[1] +
                               nanv[0][2][index]*qs[2]);

               stemp[1] = fabs(nanv[1][0][index]*qs[0] +
                               nanv[1][1][index]*qs[1] +
                               nanv[1][2][index]*qs[2]);

               stemp[2] = fabs(nanv[2][0][index]*qs[0] +
                               nanv[2][1][index]*qs[1] +
                               nanv[2][2][index]*qs[2]);
 
/*
 *             Find min index2
 */
               if ((fabs(stemp[0]) < fabs(stemp[1])) &&
                   (fabs(stemp[0]) < fabs(stemp[2]))) {
                   index2 = 0;
               } else if ((fabs(stemp[1]) < fabs(stemp[0])) &&
                          (fabs(stemp[1]) < fabs(stemp[2]))) {
                   index2 = 1;
               } else {
                   index2 = 2;
               }
 
               if (index2 == 0){
                   smat[0][0] = nanv[1][1][index];
                   smat[0][1] = nanv[1][2][index];
                   smat[1][0] = nanv[2][1][index];
                   smat[1][1] = nanv[2][2][index];

                   Matrix22Invert(smat, smatinv);

                   stemp2[0] = smatinv[0][0]*qs[1] + smatinv[0][1]*qs[2];
                   stemp2[1] = smatinv[1][0]*qs[1] + smatinv[1][1]*qs[2];
 
                   fluxtot[index][5] += stemp2[0];
                   fluxtot[index][6] += stemp2[1];
               } else if (index2 == 1 ){
                   smat[0][0] = nanv[0][0][index];
                   smat[0][1] = nanv[0][2][index];
                   smat[1][0] = nanv[2][0][index];
                   smat[1][1] = nanv[2][2][index];

                   Matrix22Invert(smat, smatinv);

                   stemp2[0] = smatinv[0][0]*qs[0] + smatinv[0][1]*qs[2];
                   stemp2[1] = smatinv[1][0]*qs[0] + smatinv[1][1]*qs[2];
 
                   fluxtot[index][4] += stemp2[0];
                   fluxtot[index][6] += stemp2[1];
               } else {
                   smat[0][0] = nanv[0][0][index];
                   smat[0][1] = nanv[0][1][index];
                   smat[1][0] = nanv[1][0][index];
                   smat[1][1] = nanv[1][1][index];

                   Matrix22Invert(smat, smatinv);

                   stemp2[0] = smatinv[0][0]*qs[0] + smatinv[0][1]*qs[1];
                   stemp2[1] = smatinv[1][0]*qs[0] + smatinv[1][1]*qs[1];
 
                   fluxtot[index][4] += stemp2[0];
                   fluxtot[index][5] += stemp2[1];
               }
#if 0
               printf("Ltot ---------------- \n");
               for (nn = 0; nn < 4; nn++) {
                   printf("%e %e %e %e \n", Ltot[nn][0], Ltot[nn][1],
                          Ltot[nn][2], Ltot[nn][3]);
               }
   
               printf("fluxtot printing ---- \n");
               for (nn = 0; nn < 4; nn++) {
                   printf(" %e %e %e %e %e %e %e\n", fluxtot[nn][0],
                          fluxtot[nn][1], fluxtot[nn][2], fluxtot[nn][3],
                          fluxtot[nn][4], fluxtot[nn][5], fluxtot[nn][6]);
               }
#endif
           }  /* for (jjj = 0; jjj < nc; ...) */
	   


/**********************************************************************/


/*
 *          computed localstrain[3][3] for the node
 */
            if ((param->loadType == 1) && (param->indxErate == 1)) {
                localeRate =
                    param->edotdir[0]*localstrain[0][0]*param->edotdir[0] +
                    param->edotdir[0]*localstrain[0][1]*param->edotdir[1] +
                    param->edotdir[0]*localstrain[0][2]*param->edotdir[2] +
                    param->edotdir[1]*localstrain[1][0]*param->edotdir[0] +
                    param->edotdir[1]*localstrain[1][1]*param->edotdir[1] +
                    param->edotdir[1]*localstrain[1][2]*param->edotdir[2] +
                    param->edotdir[2]*localstrain[2][0]*param->edotdir[0] +
                    param->edotdir[2]*localstrain[2][1]*param->edotdir[1] +
                    param->edotdir[2]*localstrain[2][2]*param->edotdir[2];

                if (localeRate > 0.0) {
                    node->sgnv = 1;
                } else if (localeRate<0.0) {
                    node->sgnv = -1;
                } else {
                    node->sgnv = 0;
                }
            }

        }  /* for (iii = 0; iii < home->newNodeKeyPtr; ...) */

/*
 *       Strain increment using decomposition here
 */
        sq75= sqrt(.75);

        for (i0 = 0; i0 < 4; i0++) {

            dyad[0][0] = burgv[i0][0]*bnorm[i0][0];
            dyad[0][1] = burgv[i0][0]*bnorm[i0][1];
            dyad[0][2] = burgv[i0][0]*bnorm[i0][2];

            dyad[1][0] = burgv[i0][1]*bnorm[i0][0];
            dyad[1][1] = burgv[i0][1]*bnorm[i0][1];
            dyad[1][2] = burgv[i0][1]*bnorm[i0][2];

            dyad[2][0] = burgv[i0][2]*bnorm[i0][0];
            dyad[2][1] = burgv[i0][2]*bnorm[i0][1];
            dyad[2][2] = burgv[i0][2]*bnorm[i0][2];

            for (k = 0; k < 3; k++){
                for (l = 0; l < 3; l++){
                    pstn[k][l] += fluxtot[i0][0] * (dyad[l][k]+dyad[k][l]) *
                                  0.5 / param->simVol;
                }
            }
              
            dyad0[0][0] = burgv[i0][0]*nanv[0][0][i0];
            dyad0[0][1] = burgv[i0][0]*nanv[0][1][i0];
            dyad0[0][2] = burgv[i0][0]*nanv[0][2][i0]; 

            dyad0[1][0] = burgv[i0][1]*nanv[0][0][i0];
            dyad0[1][1] = burgv[i0][1]*nanv[0][1][i0];
            dyad0[1][2] = burgv[i0][1]*nanv[0][2][i0]; 

            dyad0[2][0] = burgv[i0][2]*nanv[0][0][i0];
            dyad0[2][1] = burgv[i0][2]*nanv[0][1][i0];
            dyad0[2][2] = burgv[i0][2]*nanv[0][2][i0]; 

            dyad1[0][0] = burgv[i0][0]*nanv[1][0][i0];
            dyad1[0][1] = burgv[i0][0]*nanv[1][1][i0];
            dyad1[0][2] = burgv[i0][0]*nanv[1][2][i0]; 

            dyad1[1][0] = burgv[i0][1]*nanv[1][0][i0];
            dyad1[1][1] = burgv[i0][1]*nanv[1][1][i0];
            dyad1[1][2] = burgv[i0][1]*nanv[1][2][i0]; 

            dyad1[2][0] = burgv[i0][2]*nanv[1][0][i0];
            dyad1[2][1] = burgv[i0][2]*nanv[1][1][i0];
            dyad1[2][2] = burgv[i0][2]*nanv[1][2][i0]; 

            dyad2[0][0] = burgv[i0][0]*nanv[2][0][i0];
            dyad2[0][1] = burgv[i0][0]*nanv[2][1][i0];
            dyad2[0][2] = burgv[i0][0]*nanv[2][2][i0]; 

            dyad2[1][0] = burgv[i0][1]*nanv[2][0][i0];
            dyad2[1][1] = burgv[i0][1]*nanv[2][1][i0];
            dyad2[1][2] = burgv[i0][1]*nanv[2][2][i0]; 

            dyad2[2][0] = burgv[i0][2]*nanv[2][0][i0];
            dyad2[2][1] = burgv[i0][2]*nanv[2][1][i0];
            dyad2[2][2] = burgv[i0][2]*nanv[2][2][i0]; 

            for (k = 0; k < 3; k++) {
                for(l = 0; l < 3; l++) {
                  
                    pstn[k][l] += 0.5 *
                        ((fluxtot[i0][1]+fluxtot[i0][4]) * (dyad0[l][k]+dyad0[k][l]) +
                         (fluxtot[i0][2]+fluxtot[i0][5]) * (dyad1[l][k]+dyad1[k][l]) +
                         (fluxtot[i0][3]+fluxtot[i0][6]) * (dyad2[l][k]+dyad2[k][l])) / param->simVol;
                }
            }
        }  /* for (i0 = 0; i0 < 4; ...) */

/*
 *      Density decomposition
 */
        factor0 = param->simVol*bmagsq;

        for (i = 0; i < 4; i++) {
            for (j = 0; j < 4; j++) {
                param->dLtot[i][j] = Ltot[i][j] / factor0;
            }
            for (j = 0; j < 7; j++) {
                param->dfluxtot[i][j] = fluxtot[i][j] / param->simVol / 
                                        param->realdt;
            }
        }
 
#if 0
        printf("Ltot ---------------- \n");
        for (nn = 0; nn < 4; nn++) {
            printf("%e %e %e %e \n", Ltot[nn][0], Ltot[nn][1],
                   Ltot[nn][2], Ltot[nn][3]);
        }
#endif


/*
 *      accumulate delta strain this subcycle into delta strain this cycle
 */
        param->delpStrain[0] = dstn[0][0];
        param->delpStrain[1] = dstn[1][1];
        param->delpStrain[2] = dstn[2][2];
        param->delpStrain[3] = dstn[1][2];
        param->delpStrain[4] = dstn[0][2];
        param->delpStrain[5] = dstn[0][1];
        
        param->delpSpin[0] = dspn[0][0];
        param->delpSpin[1] = dspn[1][1];
        param->delpSpin[2] = dspn[2][2];
        param->delpSpin[3] = dspn[1][2];
        param->delpSpin[4] = dspn[0][2];
        param->delpSpin[5] = dspn[0][1];
                                                                                
        
#if 0
        printf("param->delpStrain[0]= %e\n",param->delpStrain[0]);              
        printf("param->delpStrain[1]= %e\n",param->delpStrain[1]);              
        printf("param->delpStrain[2]= %e\n",param->delpStrain[2]);              
        printf("param->delpStrain[3]= %e\n",param->delpStrain[3]);              
        printf("param->delpStrain[4]= %e\n",param->delpStrain[4]);              
        printf("param->delpStrain[5]= %e\n",param->delpStrain[5]);              
        printf("param->delpSpin[0]= %e\n",param->delpSpin[0]);
        printf("param->delpSpin[1]= %e\n",param->delpSpin[1]);
        printf("param->delpSpin[2]= %e\n",param->delpSpin[2]);
        printf("param->delpSpin[3]= %e\n",param->delpSpin[3]);
        printf("param->delpSpin[4]= %e\n",param->delpSpin[4]);
        printf("param->delpSpin[5]= %e\n",param->delpSpin[5]);
#endif                                                                  

        return;
}
