/**************************************************************************
 *
 *      Module:       FccCrossSlip
 *      Description:  Contains functions to determine if one {111} glide
 *                    is switched to another by cross-slip for screws
 *                    in FCC metals. Since cross slip is a discrete event 
 *                    it is handled outside of predictor-corrector loop.  
 *
 *      Includes functions:
 *
 *            CrossSlip()
 *            CrossSlipNode()
 *            ChooseCrossSlipPlane()
 ***************************************************************************/
#include "Home.h"
#include "Util.h"
#include "Mobility.h"
#include "Comm.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
 
#ifdef PARALLEL
#include "mpi.h"
#endif


 /*---------------------------------------------------------------------------
 *
 *      Function:       CrossSlip
 *      Description:    This is a driver function to check all nodes 
 *                      for xslip.
 *
 *-------------------------------------------------------------------------*/
void CrossSlip(Home_t *home);
void CrossSlipNode(Home_t *home, Node_t *node);
void ChooseCrossSlipPlane (Home_t *home, real8 Fx, real8 Fy, real8 Fz, real8 Fesc
,
                           real8  burgX01, real8  burgY01, real8  burgZ01,
                           real8  normX01, real8  normY01, real8  normZ01,
                           real8  L, real8 lsgn,
                           real8  *normX,  real8  *normY,  real8 *normZ,
                           real8  *constr, int *xslip );

void CrossSlip(Home_t *home)
{
        int     i;
        Node_t  *node;
 
/*
 *      Check if plane is switched to crossSlip plane for each 2-arm node 
 *      native to this domain.
 */
        for (i=0; i < home->newNodeKeyPtr; i++){ 
         
            node = home->nodeKeys[i]; 
            if (!node) continue;
         
            CrossSlipNode(home, node); 
        }   

}

 /*---------------------------------------------------------------------------
 *                       
 *      Function:       CrossSlipNode  
 *      Description:    This function to check free nodes with 2-perfect
 *                      screw arms on {111} if the glide plane should be
 *                      switched by xslip. 
 *-------------------------------------------------------------------------*/
void CrossSlipNode(Home_t *home, Node_t *node)
{
        int     i, j, numnbrs, nss, myDomain, nj0, nj1, xslip;
        real8   bx, by, bz;
        real8   dx, dy, dz;
        real8   mx, my, mz;
        real8   pnx, pny, pnz, nmag, invnmag;
        real8   pnx1, pny1, pnz1, a;
        real8   devnormp, devnormip, devnormi111 , devnorm;
        real8   mag, invMag;
        real8   invbMag2, bMag2, costheta, costheta2, invsqrt1mcostheta2;
        real8   eps = 1.0e-12, eps1 = 1.0e-2, eps2 = 0.95;
        real8   epsphysnode = 1.0e-2;
        real8   epsbv = 1.0e-2, epsnorm = 1.0e-2, epsdev = 1.0e-2, epsnorm111 = 0.1;
        real8   invsqrt3 = 0.5773502691, one3rd = 0.333333333333;
        real8   nForce[3], nVel[3], velCorr[3];
        real8   Fx, Fy, Fz, Fesc; 
        real8   normX, normY, normZ, constr;
        real8   lx1, lx12,ly12, lz12, lr, lsgn;
        real8   slipdir;
        
        Node_t  *nbrNode, *nbr0, *nbr1;
        Param_t *param;

        param = home->param;

        numnbrs = node->numNbrs ; 
        if ((node->constraint == 7)||(numnbrs != 2)) {
            return;
        }
 
        /* we are having a 2-node */
        myDomain = home->myDomain ;
 
        /* Get the neighboring node information */
 /*       nbr0 = GetNeighborNode (home, node, 0);
        if ((nbr0->myTag.domainID==myDomain)
          &&(OrderNodes(node,nbr0)>=0)) return;
        nbr1 = GetNeighborNode (home, node, 1);
        if ((nbr1->myTag.domainID==myDomain)
         &&(OrderNodes(node,nbr1)>=0)) return;
  */
        for (i = 0; i < 2; i++) {
 
            bx = node->burgX[i];
            by = node->burgY[i];
            bz = node->burgZ[i];
 
            bMag2 = (bx*bx + by*by + bz*bz);
            invbMag2 = 1.0 / bMag2;
/*
 *          Calculate the length of the arm and its tangent line direction
 */
            nbrNode = GetNeighborNode(home, node, i);
 
            dx = nbrNode->x - node->x;
            dy = nbrNode->y - node->y;
            dz = nbrNode->z - node->z;
 
            ZImage(param, &dx, &dy, &dz);

            mag     = sqrt(dx*dx + dy*dy + dz*dz);
            invMag  = 1.0 / mag;
 
            dx *= invMag;
            dy *= invMag;
            dz *= invMag;
/*
 *          Calculate how close to screw the arm is
 */
            costheta = (dx*bx + dy*by + dz*bz);
            costheta2 = (costheta*costheta) * invbMag2;
 
            if ((1.0 - costheta2) < eps) return;
 
/* physical (xtallographic) planes carreid over from previous steps */
 
            pnx = node->nx[i];
            pny = node->ny[i];
            pnz = node->nz[i];
            nmag = sqrt(pnx*pnx + pny*pny + pnz*pnz);
            invnmag = 1/nmag;
            if(nmag > epsnorm)
            {
                pnx *= invnmag;
                pny *= invnmag;
                pnz *= invnmag;
            }

/* indicatator for deviation from {111} */
             
            devnormp = fabs((pnx*pnx-one3rd))+fabs((pny*pny-one3rd))
                      +fabs((pnz*pnz-one3rd));
             
            if (devnormp > epsnorm) return;

/* store data */
            if(i==0)
            {
              lx1  = dx; 
              lx12 = bx; ly12 = by; lz12 = bz; 
              pnx1 = pnx; pny1 = pny; pnz1 = pnz;
/* if anti-parallel, change to parallel */

              if(bx*lx1<0.0)
              {
                lsgn = -1;
                lx12 *= -1; ly12 *= -1; lz12 *= -1;
              }
             } 

/* if neighboring planes are different, the node is physical, and exit */
             a = pnx1*pnx+pny1*pny+pnz1*pnz;
             a*= a;
             if( a < 1 - epsphysnode)
             {  
                printf("TopologyXslip: Physical Node and No Xslip\n");
                return;
             }

/* only for free node with 2-screw arms on {111}  */
         
            lr += mag;

        } /* loop over 2arms */

        lr *=0.5;

    /* Masato: constriction interval should fall in a certain range
     */
        if(lr<=param->minSeg) return;
        if(lr<=param->rann)
        {
          /* node with too short segments, cannot cross slip
           * we don't change the plane
           */
           return;
        }

/* nodal force F, and force on the unit edge component Fsp */
        Fx = node->fX ; Fy = node->fY ; Fz = node->fZ ;
/* Force per unit length */
        Fx/=lr; Fy/=lr; Fz/=lr;

    /* Cross slip probability function
     * data for Cu, Ni based on Rao et al.
     * need more data for Al
     */
 
    /* Selection of possible cross slip plane */
 
       Fesc = 0;
       Fesc = 0.5*(node->sigbeLoc[0] + node->sigbeRem[0]
                   +node->sigbeLoc[1] + node->sigbeRem[1]);
#if 0       
       printf("FCC2-EscaigComponent: Loc0 Rem0 Loc1 Rem1,%e,%e,%e,%e\n"
       ,node->sigbeLoc[0],node->sigbeRem[0]
       ,node->sigbeLoc[1],node->sigbeRem[1]);
#endif    

       ChooseCrossSlipPlane (home, Fx, Fy, Fz, Fesc,
                          lx12, ly12, lz12,
                          pnx1, pny1, pnz1, lr, lsgn,
                          &normX, &normY, &normZ, &constr, &xslip);

    /* if constriction interval is larger than segment length
     * cross slip is not possible
     */
       if(constr > lr) return;
    /* stay in the original {111} plane */
       if(xslip==0) return;

    /* We are now ready to perform cross slip */
    /* move the node slightly forward onto the new plane */

       xvector(normX, normY, normZ, lx12, ly12, lz12, &mx, &my, &mz);
 
       slipdir = mx*Fx + my*Fy + mz*Fz; 

       if(slipdir > 0)
          slipdir = 1.0;
       else
          slipdir = -1.0;
       
       node->x += 0.1*slipdir*mx; 
       node->y += 0.1*slipdir*my; 
       node->z += 0.1*slipdir*mz; 

       printf("FccCrossSlip: assign new glide plane !\n");

    /* change the glide plane cross slip node itself */

       node->nx[0] = normX; node->ny[0] = normY; node->nz[0] = normZ;
       node->nx[1] = normX; node->ny[1] = normY; node->nz[1] = normZ;
 
    /* give the information of the changed plane to the two neighbor nodes */
    /* get the two new neighbors of node */

       nbr0 = GetNeighborNode (home, node, 0);
       if ((nbr0->myTag.domainID==home->myDomain)
        &&(OrderNodes(node,nbr0)>=0))
       {   return;
       }
       nbr1 = GetNeighborNode (home, node, 1);
       if ((nbr1->myTag.domainID==home->myDomain)
        &&(OrderNodes(node,nbr1)>=0))
       {   return;
       }

    /*nbr0*/
       if (nbr0->myTag.domainID==home->myDomain)
       {
            nj0 = GetArmID(home, nbr0, node);
            nbr0->nx[nj0] = normX;
            nbr0->ny[nj0] = normY;
            nbr0->nz[nj0] = normZ;
       } /* glide plane normal assignment for new neighbor node 1 */
 
    /*nbr1*/
       if (nbr1->myTag.domainID==home->myDomain)
       {
            nj1 = GetArmID(home, nbr1, node);
            nbr1->nx[nj1] = normX;
            nbr1->ny[nj1] = normY;
            nbr1->nz[nj1] = normZ;
       }
 
       return;
}
 
/***************************************************************************
 * Function: ChooseCrossSlipPlane 
 * Description: 1. Finds the (111) plane according to the activation rate
 *              2. Compute constriction interval and return 
 *               
 * Input:
 *  Fx, Fy, Fz (Schmidt force)
 *  Fesc (Escaig force)  
 *  burgX01, burgY01, burgZ01 (Burgers vector)
 *  normX01, normY01, normZ01 (original plane normal)
 *   
 * Output:
 *  normX, normY, normZ (new plane normal)
 *  constr (constriction interval)  
 *  xslip (cross slip (1) or not (0))
 *   
 * Parameters:
 *  TempK = temperature (Param)
 *  dt = time step (Param) 
 *  fE0 = critical Escaig force for cross slip (local)
 *  fS0 = critical Schmidt force for cross slip (local)
 *  dG0 = activation free energy in eV (local) 
 *  omega0 = Debye frequency 
 *  L0 = characteristic length
 *  dG  = activation free enthalpy (local)
 *  slipnum = no. of slip planes to be considered for x-slip event
 *  planeprob(slipnum) = normalized accumulated density functions for 3 planes
 *  pdensity = total probabilty density 
 *  prefac2 = conversion factor eV to K -> e/kB
 *   
 **************************************************************************/
void ChooseCrossSlipPlane (Home_t *home, real8 Fx, real8 Fy, real8 Fz, real8 Fesc
,                           
                           real8  burgX01, real8  burgY01, real8  burgZ01,
                           real8  normX01, real8  normY01, real8  normZ01,
                           real8  L, real8 lsgn,   
                           real8  *normX,  real8  *normY,  real8 *normZ,
                           real8  *constr, int *xslip )
{
    Param_t *param;
    real8 bsgn, cnx[3], cny[3], cnz[3], ex, ey, ez;
    real8 forcedot, prob;
    real8 planerand, rand_max;
    real8 omega0, L0, fS, fE, fS0, fE0, dG0, dG, p, q, dt;
    real8 fxnx, fxny, fxnz;
    real8 TempK, eV2K;
    real8 chkNormal, chksp;
    real8 bspefaq, bsptx, bspty, bsptz, bsplx, bsply, bsplz;
    real8 bspex, bspey, bspez;
    real8 tmp;
    int   i,j, slipnum, prime, cross;
 
/* check the parameter later !!!! */
 
    param   = home->param;
    TempK   = param->TempK;
    dt      = param->deltaTT;
 
    if(TempK == 0.0)
    {
        printf("Mobility_FCC_1: temperature = zero !!");
        return;
    }
 
    eV2K = 1.1594e4;
    slipnum = 2;
 
    /* Ni */
    dG0     = 2.0; /* eV */
    dG0    *= eV2K/TempK; /* dimensionless */
    omega0  = 1e13; /* 1/s */
    L0      = 1; /* b */
 
    p       = .666666666666667;
    q       = 1.5;
    fS0     = 0.033*param->shearModulus; /* sig.b */
    /* assuming the magnitude of b is always one */
 
    fE0     = 0.028*param->shearModulus; /* sig.b */
    /* assuming the magnitude of b is always one */
 
#ifndef FFACTOR_BV
#define FFACTOR_BV 1e-3
#endif
 
/* initial value */
 
    *normX = 0.0; *normY = 0.0; *normZ = 0.0;
    *xslip = 0;
    *constr = 100;
 
 
/* x-slip only for 1/2<110> dislocations */
 
    if( fabs((fabs(burgX01)-fabs(burgY01))>FFACTOR_BV||burgZ01!= 0) &&
        fabs((fabs(burgY01)-fabs(burgZ01))>FFACTOR_BV||burgX01!= 0 ) &&
        fabs((fabs(burgZ01)-fabs(burgX01))>FFACTOR_BV||burgY01!= 0 ) )
    {/* this is not 1/2<110> type dislocation, no cross slip */
            return;
    }
 
/* define possible 2 cross-slip planes 0: original, 1: cross slip */
    cnx[0]=normX01; cny[0]=normY01; cnz[0]=normZ01;
    if( burgX01 == 0 ) { cnx[1] = normX01; cny[1]=-normY01; cnz[1] = -normZ01; }
    if( burgY01 == 0 ) { cnx[1] = -normX01; cny[1]=normY01; cnz[1] = -normZ01; }
    if( burgZ01 == 0 ) { cnx[1] = -normX01; cny[1]=-normY01; cnz[1] = normZ01; }
 
/* fE = tau_e(2)-tau_e(1)
 * if fE > 0 then Escaig force is assisting the cross slip
 */
    fE = Fesc ;
 
/* Schmidt stress in cross slip plane minus that in original plane*/
    fxnx = Fy*cnz[1] - Fz*cny[1] ;
    fxny = Fz*cnx[1] - Fx*cnz[1] ;
    fxnz = Fx*cny[1] - Fy*cnx[1] ;
    fS = sqrt(fxnx*fxnx+fxny*fxny+fxnz*fxnz);
 
    fxnx = Fy*cnz[0] - Fz*cny[0] ;
    fxny = Fz*cnx[0] - Fx*cnz[0] ;
    fxnz = Fx*cny[0] - Fy*cnx[0] ;
    fS -= sqrt(fxnx*fxnx+fxny*fxny+fxnz*fxnz);
 
/*
  dG = dG0 * pow(1 - pow(fE/fE0,pE), qE)
           * pow(1 - pow(abs(fS/fS0),pS), qS);
 */
 
    if (fS<0)
    {/* no driving force, no cross slip */
            return;
    }
    tmp = fE/fE0 + fS/fS0;
    if (tmp<0)
    {/* no driving force, no cross slip */
            return;
    }
    if (tmp>1)
    {/* no barrier */
            dG = 0;
    }
    else
    {
            dG = dG0 * pow( 1- pow(tmp,p), q );
    }
 
    if(dG > 50.0)
    {
            printf("Mobility_FCC_1: temperature is low !!\n");
            return;
    }
 
    prob = (omega0*dt)*(L/L0)*exp(-dG);
 
    rand_max = (real8) RAND_MAX;
    planerand = (real8) rand() / rand_max;
 
    if(planerand > prob)
    {/* no cross slip */
            printf("Mobility_FCC_1or2: cross slip NOT selected %e/%e\n",
                    planerand, prob);
            return;
    }
 
    printf("Mobility_FCC_1: cross slip selected\n");
    *normX=cnx[1];*normY=cny[1];*normZ=cnz[1];
    *xslip = 1;
 
    return;
}

