/****************************************************************************
 *
 *      Function:    WriteDensFlux
 *      Description: Writes information required for Tom's 
 *                   dislocation density-based continuum model
 *                     - Density and flux for each slip system
 *                     - Amount of slip (in strain) for all slip systems
 *                
 *      Author:      Moon Rhee 06/23/2004
 *
 ****************************************************************************/
#include "Home.h"
#include "Util.h"


void WriteDensFlux(char *fluxname, Home_t *home)
{
        int     i;
        real8   al, am, an, amag, sigijk, pstnijk;
        real8   totalEdge1, totalEdge2, totalEdge3, totalEdge4;
        real8   allEdge,allScrew;
        real8   gdisloden[12], gdenscrew[4], ang1, ang2, tmpstn;
        char    fileName[256];
        FILE    *fp, *fp01;
        Param_t *param;

        param = home->param;

        if (home->myDomain != 0) {
            return;
        }
 
        param = home->param;

        al = param->edotdir[0];
        am = param->edotdir[1];
        an = param->edotdir[2];

        amag = sqrt(al*al + am*am + an*an);

        al /= amag;
        am /= amag;
        an /= amag;

        sigijk = param->appliedStress[0]*al*al +
                 param->appliedStress[1]*am*am +
                 param->appliedStress[2]*an*an +
                 2.0*param->appliedStress[3]*am*an +
                 2.0*param->appliedStress[4]*an*al +
                 2.0*param->appliedStress[5]*al*am;

        tmpstn = param->eRate * param->timeNow;

        pstnijk =  param->totpStn[0]*al*al     +
                   param->totpStn[1]*am*am     +
                   param->totpStn[2]*an*an     +
                   2.0*param->totpStn[3]*am*an +
                   2.0*param->totpStn[4]*an*al +
                   2.0*param->totpStn[5]*al*am;

/*
 *      Flux Decomposition 9/15/06
 */
        snprintf(fileName, sizeof(fileName), "%s/Ltot_b1", DIR_FLUXDATA);
        fp = fopen(fileName,"a");

        totalEdge1= param->Ltot[0][1]+param->Ltot[0][2]+param->Ltot[0][3];
        totalEdge2= param->Ltot[1][1]+param->Ltot[1][2]+param->Ltot[1][3];
        totalEdge3= param->Ltot[2][1]+param->Ltot[2][2]+param->Ltot[2][3];
        totalEdge4= param->Ltot[3][1]+param->Ltot[3][2]+param->Ltot[3][3];

        allEdge = totalEdge1+totalEdge2+totalEdge3+totalEdge4;
        allScrew = param->Ltot[0][0]
                 + param->Ltot[1][0]
                 + param->Ltot[2][0]
                 + param->Ltot[3][0];

        fprintf(fp, "%e %e %e %e %e %e %e %e %e \n", pstnijk, tmpstn,
                param->Ltot[0][0], param->Ltot[0][1],
                param->Ltot[0][2], param->Ltot[0][3],
                totalEdge1, allEdge, allScrew);
        fclose(fp);

        snprintf(fileName, sizeof(fileName), "%s/Ltot_b2", DIR_FLUXDATA);
        fp = fopen(fileName,"a");
        fprintf(fp, "%e %e %e %e %e %e %e %e %e \n", pstnijk, tmpstn,
                param->Ltot[1][0], param->Ltot[1][1],
                param->Ltot[1][2], param->Ltot[1][3],
                totalEdge2, allEdge, allScrew);
        fclose(fp);


        snprintf(fileName, sizeof(fileName), "%s/Ltot_b3", DIR_FLUXDATA);
        fp = fopen(fileName,"a");
        fprintf(fp, "%e %e %e %e %e %e %e %e %e \n", pstnijk, tmpstn,
                param->Ltot[2][0], param->Ltot[2][1],
                param->Ltot[2][2], param->Ltot[2][3],
                totalEdge3, allEdge, allScrew);
        fclose(fp);


        snprintf(fileName, sizeof(fileName), "%s/Ltot_b4", DIR_FLUXDATA);
        fp = fopen(fileName,"a");
        fprintf(fp, "%e %e %e %e %e %e %e %e %e \n", pstnijk, tmpstn,
                param->Ltot[3][0], param->Ltot[3][1],
                param->Ltot[3][2], param->Ltot[3][3],
                totalEdge4, allEdge, allScrew);
        fclose(fp);


        snprintf(fileName, sizeof(fileName), "%s/fluxtot_b1", DIR_FLUXDATA);
        fp = fopen(fileName,"a");
        fprintf(fp, "%e %e %e %e %e %e %e %e %e \n", pstnijk, tmpstn,
                param->fluxtot[0][0], param->fluxtot[0][1],
                param->fluxtot[0][2], param->fluxtot[0][3],
                param->fluxtot[0][4], param->fluxtot[0][5],
                param->fluxtot[0][6]);
        fclose(fp);


        snprintf(fileName, sizeof(fileName), "%s/fluxtot_b2", DIR_FLUXDATA);
        fp = fopen(fileName,"a");
        fprintf(fp, "%e %e %e %e %e %e %e %e %e \n", pstnijk, tmpstn,
                param->fluxtot[1][0], param->fluxtot[1][1],
                param->fluxtot[1][2], param->fluxtot[1][3],
                param->fluxtot[1][4], param->fluxtot[1][5],
                param->fluxtot[1][6]);
        fclose(fp);


        snprintf(fileName, sizeof(fileName), "%s/fluxtot_b3", DIR_FLUXDATA);
        fp = fopen(fileName,"a");
        fprintf(fp, "%e %e %e %e %e %e %e %e %e \n", pstnijk, tmpstn,
                param->fluxtot[2][0], param->fluxtot[2][1],
                param->fluxtot[2][2], param->fluxtot[2][3],
                param->fluxtot[2][4], param->fluxtot[2][5],
                param->fluxtot[2][6]);
        fclose(fp);


        snprintf(fileName, sizeof(fileName), "%s/fluxtot_b4", DIR_FLUXDATA);
        fp = fopen(fileName,"a");
        fprintf(fp, "%e %e %e %e %e %e %e %e %e \n",pstnijk, tmpstn,
                param->fluxtot[3][0], param->fluxtot[3][1],
                param->fluxtot[3][2], param->fluxtot[3][3],
                param->fluxtot[3][4], param->fluxtot[3][5],
                param->fluxtot[3][6]);
        fclose(fp);

        return;
}

