/**************************************************************************
 *
 *      Mopdule:     ParadisInit.c
 *      Description: Contains functions for doing one-time initializations
 *                   before entering the main loop of the application.
 *
 *      Includes functions:
 *          ParadisInit()
 *          PrintBanner()
 *
 *************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <pwd.h>
#include <time.h>
#include <string.h>
#include "Home.h"
#include "Init.h"
#include "Util.h"

#ifdef PARALLEL
#include "mpi.h"
#endif


/*-------------------------------------------------------------------------
 *
 *      Function:    PrintBanner
 *      Description: Prints a banner to stdout with some basic info
 *                   about the current execution.
 *
 *------------------------------------------------------------------------*/
static void PrintBanner(Home_t *home, int argc, char *argv[])
{
        int            i;
	uid_t          uid;
	time_t         currTime;
	char           *separator, *execName, *execPath;
	char           workingDir[512];
	char           tmpExecName[512];
	char           currTimeStr[128];
        char           cmdLine[512];
        char           userName[32];
	struct utsname utsInfo;
	struct passwd  *pwInfo;

	time(&currTime);
	strcpy(currTimeStr, ctime(&currTime));
	currTimeStr[strlen(currTimeStr)-1] = 0;

	strcpy(tmpExecName, argv[0]);
	separator = strrchr(tmpExecName, '/');
        if (separator == (char *)NULL) {
		execPath = ".";
		execName = tmpExecName;
	} else {
		*separator = 0;
		execName = separator + 1;
		execPath = tmpExecName;
	}

	(void *)getcwd(workingDir, sizeof(workingDir) - 1);

	(void)uname(&utsInfo);

	uid = getuid();
#ifndef _BGL
	pwInfo = getpwuid(uid);
	strcpy(userName, pwInfo->pw_name);
#else
	sprintf(userName, "%d", uid);
#endif

        cmdLine[0] = 0;
        for (i = 1; i < argc; i++) {
            strcat(cmdLine, argv[i]);
            strcat(cmdLine, " ");
        }

	printf("***********************************************************\n");
	printf("**** \n");
	printf("**** Time of run:     %s\n", currTimeStr);
	printf("**** Executable name: %s\n", execName);
	printf("**** Executable path: %s\n", execPath);
	printf("**** Working dir:     %s\n", workingDir);
	printf("**** Execution host:  %s (%s %s %s)\n",
               utsInfo.nodename, utsInfo.sysname,
               utsInfo.release, utsInfo.machine);
	printf("**** User name:       %s\n", userName);
	printf("**** Number of tasks: %d\n", home->numDomains);
	printf("**** Command line:    %s\n", cmdLine);
	printf("**** \n");
	printf("***********************************************************\n");

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    ParadisInit
 *      Description: Create the 'home' structure, setup timing categories
 *                   and initialize MPI.
 *
 *------------------------------------------------------------------------*/
void ParadisInit(int argc, char *argv[], Home_t **homeptr)
{
        Home_t         *home;

        home = InitHome();
        *homeptr = home;
    
        TimerInit(home);
    
#ifdef PARALLEL
        MPI_Init(&argc, &argv); 
        MPI_Comm_rank(MPI_COMM_WORLD, &home->myDomain);
        MPI_Comm_size(MPI_COMM_WORLD, &home->numDomains);
#else
        home->myDomain = 0;
        home->numDomains = 1;
#endif
    
        TimerStart(home, TOTAL_TIME);

        if (home->myDomain == 0) PrintBanner(home, argc, argv);

        TimerStart(home, INITIALIZE);
        Initialize(home,argc,argv);  
        TimerStop(home, INITIALIZE);
    
#ifdef PARALLEL
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        if (home->myDomain == 0) printf("ParadisInit finished\n");

        return;
}
