/***************************************************************************
 *
 *      Module:       CellCharge
 *      Description:  Sum the contributions from each segment in a cell to
 *                    the total charge tensor in that cell, and distribute
 *                    the sum to all processors. The result is that all
 *                    processors have the net charge tensor of each cell in
 *                    the problem.
 *
 *      Includes functions:
 *
 *          CellCharge()
 *          FMSetTaylorExpansions()
 *          FMCellCharge()
 *          MonopoleCellCharge()
 *
 *      Last Modified: 04/08/2008 - Added explicit initialization of
 *                                  taylor coefficients for highest layer
 *                                  fmm cell if PBC is disabled.
 *
 ***************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "Home.h"
#include "Util.h"
#include "FM.h"


/*-------------------------------------------------------------------------
 *
 *      Function:     FMSetTaylorExpansions
 *      Description:  This is basically a control function to handle
 *                    the downward pass through the FM hierarchy
 *                    calculating calculating the taylor expansions
 *                    for each cell, and sending down fully aggregated
 *                    multipole expansions as necessary.
 *
 *      Last Modified: 04/08/2008 - Fixed call to FMFindNearNbrs()
 *
 *-----------------------------------------------------------------------*/
void FMSetTaylorExpansions(Home_t *home)
{
        int       i, j, k, l, p, layerID, offset, zeroCharge;
        int       cx, cy, cz, dx, dy, dz, px, py, pz;
        int       tx, ty, tz, itx, ity, itz;
        int       cellID, cellID1, pCellID, nCellID;
        int       isImmediateNbr, trimOverlap;
        int       *bMin, *bMax, *tMin, *tMax;
        int       skipX[3], skipY[3], skipZ[3];
        int       tmp1Min[3], tmp1Max[3], tmp2Min[3], tmp2Max[3];
        int       mpOrder, tOrder, maxOrder;
        real8     r, s, t, MU, NU;
        real8     burgX, burgY, burgZ;
        real8     *eeta, *ep;
        real8     R[3], nCtr[3];
        real8     cMin[3];
        real8     *tCoeff;
        Cell_t    *cell1;
        Param_t   *param;
        FMLayer_t *layer, *pLayer;
        FMCell_t  *cell, *pCell, *nCell;


        param = home->param;

        MU = param->shearModulus;
        NU = param->pois;

        mpOrder  = param->fmMPOrder;
        tOrder   = param->fmTaylorOrder;
        maxOrder = mpOrder+tOrder;

        tCoeff = (real8 *)malloc(home->fmNumTaylorCoeff * sizeof(real8));
/*
 *      Loop through all FM layers except the lowest (coarsest),
 *      do the downward communication pass to shift taylor expansions
 *      from current layer cells to the next layer down as well
 *      distributing the accumulated lower layers' multipole
 *      expansions to the necessary cells at the lower layer.
 */
        for (layerID = 0; layerID < param->fmNumLayers-1; layerID++) {

            FMCommDownPass(home, layerID);

/*
 *          Each domain now adjusts the taylor expansions for cells
 *          it owns at the next lower layer based on the multipole
 *          expansions for all near neighbor cells excluding immediate
 *          neighbors.
 */
            layer = &home->fmLayer[layerID+1];

            bMin = layer->ownedMin;
            bMax = layer->ownedMax;

/*
 *          Loop through all cells owned by this domain at the next
 *          layer down (i.e. more refined) in the hierarchy.
 */
            for (cx = bMin[X]; cx <= bMax[X]; cx++) {
                for (cy = bMin[Y]; cy <= bMax[Y]; cy++) {
                    for (cz = bMin[Z]; cz <= bMax[Z]; cz++) {

/*
 *                      Get the ID and indices for the current FM cell
 *                      and figure out indices of the immediately
 *                      neighboring FM cells.
 */
                        cellID = EncodeIndex(layer->lDim,cx,cy,cz);
                        cell = layer->fmcData[cellID].cell;

                        skipX[0] = cx-1; skipX[1] = cx; skipX[2] = cx+1;
                        skipY[0] = cy-1; skipY[1] = cy; skipY[2] = cy+1;
                        skipZ[0] = cz-1; skipZ[1] = cz; skipZ[2] = cz+1;

/*
 *                      Now find all the "near" neighbors of the current cell
 *                      and loop though each one.
 */
                        tmp1Min[X] = cx;
                        tmp1Min[Y] = cy;
                        tmp1Min[Z] = cz;

                        tmp1Max[X] = cx;
                        tmp1Max[Y] = cy;
                        tmp1Max[Z] = cz;

                        trimOverlap = 0;

                        FMFindNearNbrs(home, layerID+1, tmp1Min, tmp1Max,
                                       tmp2Min, tmp2Max, trimOverlap);

                        tMin = tmp2Min;
                        tMax = tmp2Max;
    
/*
 *                      The initial indices may in fact represent periodic
 *                      cells well outside the bounds of the problem, so
 *                      we fiddle with the itx/y/z indices to come up
 *                      with the tx/y/z indices matching the image of the
 *                      cell that is within the primary problem space
 */
                        for (itx = tMin[X]; itx <= tMax[X]; itx++) {
                            tx = itx % layer->lDim[X];
                            tx = GETPBCINDEX(tx, layer->lDim[X]);
                            for (ity = tMin[Y]; ity <= tMax[Y]; ity++) {
                                ty = ity % layer->lDim[Y];
                                ty = GETPBCINDEX(ty, layer->lDim[Y]);
                                for (itz = tMin[Z]; itz <= tMax[Z]; itz++) {
                                    tz = itz % layer->lDim[Z];
                                    tz = GETPBCINDEX(tz, layer->lDim[Z]);

/*
 *                                  If this neighbor is an immediate
 *                                  neighbor of the owned cell (or the
 *                                  cell itself), skip it.
 */
                                    if ((itx==skipX[0] || itx==skipX[1] ||
                                         itx==skipX[2]) &&
                                        (ity==skipY[0] || ity==skipY[1] ||
                                         ity==skipY[2]) &&
                                        (itz==skipZ[0] || itz==skipZ[1] ||
                                         itz==skipZ[2])) {
                                        continue;
                                    }

/*
 *                                  Find the vector from the center of the 
 *                                  current cell to the center of the multipole
 *                                  expansion in the neighboring cell.
 */
                                    nCellID = EncodeIndex(layer->lDim,tx,ty,tz);
                                    nCell = layer->fmcData[nCellID].cell;

                                    R[X] = (layer->cellSize[X] * (cx-itx));
                                    R[Y] = (layer->cellSize[Y] * (cy-ity));
                                    R[Z] = (layer->cellSize[Z] * (cz-itz));

/*
 *                                  Calculate the contribution to this cell's
 *                                  taylor expansion from the neighbor's
 *                                  multipole expansion
 */
                                    MkTaylor(MU, NU, mpOrder, tOrder, maxOrder,
                                             R, nCell->mpCoeff, tCoeff);

                                    for (i = 0; i<home->fmNumTaylorCoeff; i++) {
                                        cell->taylorCoeff[i] += tCoeff[i];
                                    }

                                }  /* loop over itz */
                            }  /* loop over ity */
                        }  /* loop over itx */
                    }  /* loop over cz */
                }  /* loop over cy */
            }  /* loop over cx */

        }  /* loop over layers */

        free(tCoeff);

/* 
 * Conditionnal convergence correction
 */
	MeanStressCorrection(home);
	
/*
 *      Once the downward pass is complete, each domain still has to 
 *      distribute the taylor expansion coeeficients for any cell
 *      it owns (at the most refined FM layer) to all domains
 *      interssecting that cell.
 */
        FMDistTaylorExp(home);

        return;
}


void FMCellCharge(Home_t *home)
{
        int       i, inode, inbr, layerID, numCharges;
        int       cx, cy, cz, tcx, tcy, tcz;
        int       cellID, tCellID;
        int       *bMin, *bMax;
        real8     p1[3], p2[3];
        real8     vec1[3], vec2[3], burg[3];
        real8     *etatemp = (real8 *)NULL;
        Param_t   *param;
        Node_t    *node, *nbr;
        FMLayer_t *layer, *tLayer;
        FMCell_t  *cell, *tCell;


        param = home->param;

        etatemp = (real8 *)malloc(home->fmNumMPCoeff * sizeof(real8));

/*
 *      Zero out mulitpole expansion data for all cells intersecting this
 *      domain at the lowest FM layer.
 */
        layer = &home->fmLayer[param->fmNumLayers-1];
        numCharges = home->fmNumMPCoeff;

        bMin = layer->intersectMin;
        bMax = layer->intersectMax;

        for (cx = bMin[X]; cx <= bMax[X]; cx++) {
            for (cy = bMin[Y]; cy <= bMax[Y]; cy++) {
                for (cz = bMin[Z]; cz <= bMax[Z]; cz++) {
                    cellID = EncodeIndex(layer->lDim, cx, cy, cz);
                    cell = layer->fmcData[cellID].cell;
                    memset(cell->mpCoeff, 0, numCharges * sizeof(real8));
                }
            }
        }

/*
 *      loop through the native nodes.  For each node look at
 *      its neighbors.  If neighbor has a higher tag than node,
 *      then it is a non-redundant segment.  For consistency with
 *      the way segments are chosen for local force calculation,
 *      a segment is considered to belong to the cell of its
 *      higher-tagged node.
 */
        for (inode = 0; inode < home->newNodeKeyPtr; inode++) {

            if ((node = home->nodeKeys[inode]) == (Node_t *)NULL) continue;

            p1[X] = node->x;
            p1[Y] = node->y;
            p1[Z] = node->z;

/*
 *          Find out which cell the current node is in and convert
 *          that cell's indices into the FM cell indices and ID for the
 *          corresponding FM cell at the bottom (most refined) FM layer.
 */
            DecodeCellIdx (home, node->cellIdx, &cx, &cy, &cz);

            cx--;
            cy--;
            cz--;

            layer  = &home->fmLayer[param->fmNumLayers-1];
            cellID = EncodeIndex(layer->lDim, cx, cy, cz);
            cell   = layer->fmcData[cellID].cell;

            if (cell == (FMCell_t *)NULL) {
                Fatal("NULL FM cell ptr");
            }

            for (inbr = 0; inbr < node->numNbrs; inbr++) {

               nbr = GetNeighborNode (home, node, inbr);

               if (NodeOwnsSeg(home, node, nbr) == 0) {
                   continue;
               }

               p2[X] = nbr->x;
               p2[Y] = nbr->y;
               p2[Z] = nbr->z;

               PBCPOSITION(param, p1[X], p1[Y], p1[Z], &p2[X], &p2[Y], &p2[Z]);

               burg[X] = node->burgX[inbr];
               burg[Y] = node->burgY[inbr];
               burg[Z] = node->burgZ[inbr];
        
/*
 *             Set vector from the segment starting point to segment
 *             end point
 */
               vec2[X] = p2[X] - p1[X];
               vec2[Y] = p2[Y] - p1[Y];
               vec2[Z] = p2[Z] - p1[Z];

/*
 *             Set vector from the segment starting point to the 
 *             FM cell expansion center and increment the total
 *             multipole expansion for the lowest layer FM cell
 *             containing the segment with the contribution from this segment.
 */
               vec1[X] = p1[X] - cell->cellCtr[X];
               vec1[Y] = p1[Y] - cell->cellCtr[Y];
               vec1[Z] = p1[Z] - cell->cellCtr[Z];

               makeeta(param->fmMPOrder, vec1, vec2, burg, etatemp);

               for (i = 0; i < home->fmNumMPCoeff; i++) {
                   cell->mpCoeff[i] += etatemp[i];
               }

           }  /* end for (inbr = 0; ...)  */
        }  /* end for (inode = 0; ...)  */

        free(etatemp);
        etatemp = (real8 *)NULL;

/*
 *      We have the multipole contribution from each native segment
 *      to the cells at the lowest FM layer, now we have to pass the
 *      data up the FM hierarchy.
 */
        for (layerID = param->fmNumLayers-1; layerID >= 0; layerID--) {
            FMCommUpPass(home, layerID);
        }

/*
 *      Calculate the Taylor expansion at the highest (coarsest) layer
 *      of the FM hierarchy.  This taylor expansion will account for
 *      all necessary periodic images of the full problem space except
 *      for the near neighbor images of the primary image.
 */
        if ((param->xBoundType == Periodic) ||
            (param->yBoundType == Periodic) ||
            (param->zBoundType == Periodic)) {
            DoTableCorrection(home);
	    
        } else {
/*
 *          If periodic boundaries are not enabled, we have to explicitly
 *          initialize the taylor coefficients at the highest FM layer.
 *          With periodic boundaries, this is taken care of in
 *          DoTableCorrection().
 */
            layer = &home->fmLayer[0];
            cell  = layer->fmcData[0].cell;
            if (cell != (FMCell_t *)NULL) {
                memset(cell->taylorCoeff, 0,
                       home->fmNumTaylorCoeff * sizeof(real8));
            }
        }

/*
 *      Now call the function that will handle passing all necessary
 *      data back down the FM hierarchy shifting the taylor expansions
 *      from ancestor to descendant cells as needed.
 */
        FMSetTaylorExpansions(home);
        return;
}


void MonopoleCellCharge (Home_t *home)
{

   int nCells, i, j, inode, inbr, iCell, jCell, kCell, cellIdx, chgIdx ;
   real8 probXmin, probYmin, probZmin, probXmax, probYmax, probZmax ;
   real8 Lx, Ly, Lz, cellXsize, cellYsize, cellZsize ;
   real8 x1, y1, z1, dx, dy, dz, xm, ym, zm ;
   real8 b[3], dl[3] ;
   real8 *cellCharge ;

   Param_t *param ;
   Node_t *node, *nbr ;

/* Set up the problem limits */

   param = home->param ;

   probXmin = param->minSideX ; probXmax = param->maxSideX ;
   probYmin = param->minSideY ; probYmax = param->maxSideY ;
   probZmin = param->minSideZ ; probZmax = param->maxSideZ ;

   Lx = probXmax - probXmin ;
   Ly = probYmax - probYmin ;
   Lz = probZmax - probZmin ;

   cellXsize = Lx / param->nXcells ;
   cellYsize = Ly / param->nYcells ;
   cellZsize = Lz / param->nZcells ;

   nCells = param->nXcells * param->nYcells * param->nZcells ;

/* Allocate and zero out a local charge array */

   cellCharge = (real8 *) malloc (9 * nCells * sizeof(real8)) ;

   for (i = 0 ; i < 9*nCells ; i++)
      cellCharge[i] = 0.0 ;

/* loop through the native nodes. For each node look at its neighbors.
 * If neighbor has a higher tag than node, then it is a non-redundant
 * segment.
 */

   for (inode = 0 ; inode < home->newNodeKeyPtr ; inode++) {

      node = home->nodeKeys[inode] ;
      if (!node) continue ;
      x1 = node->x ; y1 = node->y ; z1 = node->z ;

/* For consistency with the way segments are chosen for local force 
 * calculation, a segment is considered to belong to the cell containing
 * the node owning the segment
 */

      DecodeCellIdx (home, node->cellIdx, &iCell, &jCell, &kCell) ;
      iCell-- ; jCell-- ; kCell-- ;

      cellIdx = kCell + param->nZcells*jCell + 
                        param->nZcells*param->nYcells*iCell ;

      for (inbr = 0 ; inbr < node->numNbrs ; inbr++) {

         nbr = GetNeighborNode (home, node, inbr) ;

         if (NodeOwnsSeg(home, node, nbr) == 0) {
             continue;
         }

         dx = nbr->x - x1 ;
         dy = nbr->y - y1 ;
         dz = nbr->z - z1 ;

         ZImage (param, &dx, &dy, &dz) ;

         b[0] = node->burgX[inbr] ;
         b[1] = node->burgY[inbr] ;
         b[2] = node->burgZ[inbr] ;
        
         dl[0] = dx ;
         dl[1] = dy ;
         dl[2] = dz ;

/* Initialize chgIdx to the first element (charge[0][0]) of the tensor
 * for the segment's cell. The tensor is then looped thru in row order
 */

         chgIdx = 9 * cellIdx ;
         for (i = 0 ; i < 3 ; i++) {
            for (j = 0 ; j < 3 ; j++) {
               cellCharge[chgIdx++] += b[i]*dl[j] ;
            }
         }

      }  /* end for (inbr = 0 ; ...)  */
   }  /* end for (inode = 0 ; ...)  */

/* Sum the individual cell charges over all processors (only a few at
 * most will contribute to any particular cell) and leave the sum for each
 * cell on all processors.
 */

#ifdef PARALLEL
   MPI_Allreduce (cellCharge, home->cellCharge, 9*nCells, MPI_DOUBLE,
                  MPI_SUM, MPI_COMM_WORLD) ;
   free (cellCharge) ;
#else
   if (home->cellCharge) {
       free(home->cellCharge);
   }
   home->cellCharge = cellCharge ;
#endif

#if 0
/*
 *  For debug purposes only...
 */
{
    int       i, x, y, z, cellID;
    real8     *charge;
    FILE      *fp;
    char      filename[128];
    int       dim[3];

    sprintf(filename, "cellCharge_task_%d", home->myDomain);
    fp = fopen(filename, "w");

    dim[0] = param->nXcells;
    dim[1] = param->nYcells;
    dim[2] = param->nZcells;

    for (x = 0; x < dim[0]; x++) {
        for (y = 0; y < dim[1]; y++) {
            for (z = 0; z < dim[2]; z++) {

                cellID = EncodeIndex(dim, x, y, z);
                charge = &home->cellCharge[cellID*9];
                fprintf(fp, "Cell %d (%d,%d,%d)\n", cellID, x, y, z);

                for (i = 0; i < 9; i++) {
                    fprintf(fp, "    %e\n", charge[i]);
                }
            }
        }
    }

    fclose(fp);
}
#endif

   return ;
}


void CellCharge(Home_t *home)
{
        TimerStart(home, CELL_CHARGE);

/*
 *      If we are not doing full n^2 force calculations, we need
 *      to prepare for the remote force calcs... but if we are
 *      doing full n^2 force calcs, this stuff is irrelevant.
 */
#ifndef FULL_N2_FORCES
        if (home->param->fmEnabled)
            FMCellCharge(home);
        else
            MonopoleCellCharge(home);
#endif

        TimerStop(home, CELL_CHARGE);

#if PARALLEL
#ifdef SYNC_TIMERS
        TimerStart(home, CELL_CHARGE_BARRIER);
        MPI_Barrier(MPI_COMM_WORLD);
        TimerStop(home, CELL_CHARGE_BARRIER);
#endif
#endif
        return;
}



/*---------------------------------------------------------------------------
 *
 *      Function:    EvalMeanTaylor
 *      Description: Compute the average stress from a taylor expansion 
 *                   in the simulation cell to account for conditionally
 *                   convergence problem.
 *
 *      Arguments:
 *          sigma    3X3 matrix in which the calculated average stress 
 *                   is returned to the caller.
 *
 *-------------------------------------------------------------------------*/
void EvalMeanTaylor(Home_t *home,real8 aveStress[3][3])
{
        int   i, j, k, m, nx, ny, nz;
	int uorder;
        real8 rp;
        real8 pw[NMAX+1][3];
	real8 Min[3], Max[3],Volume,sigma[3][3],stress[3][3];
	Param_t *param;

	int cx,cy,cz;
	int cellID,Ncells;
	int *bMin, *bMax;
	FMLayer_t *layer;
	FMCell_t  *cell;

	param = home->param;
	
	uorder = param->fmTaylorOrder;
	layer = &home->fmLayer[param->fmNumLayers-1];

	bMin = layer->ownedMin;
	bMax = layer->ownedMax;

	/* Cell size bounds*/
	Min[0] = param->minSideX/layer->lDim[X];
	Max[0] = param->maxSideX/layer->lDim[X];
	
	Min[1] = param->minSideY/layer->lDim[Y];
	Max[1] = param->maxSideY/layer->lDim[Y];
	
	Min[2] = param->minSideZ/layer->lDim[Z];
	Max[2] = param->maxSideZ/layer->lDim[Z];
	      
	Ncells = layer->lDim[X]*layer->lDim[Y]*layer->lDim[Z];
	Volume = (Max[0]- Min[0])*(Max[1]- Min[1])*(Max[2]- Min[2])*Ncells;

	// Compute int_Min ^ Max x^n
	for (i = 0; i <= uorder; i++) 
	  for (j = 0; j < 3; j++) 
	    {
	      pw[i][j] = (pow(Max[j],i+1)-pow(Min[j],i+1))/((i+1)*1.0);
	    }

	// Sum up the stress for all cells at the layer  
	for (j = 0; j < 3; j++) 
	  for (m = 0; m < 3; m++) 
	    {
	      stress[j][m] = 0.0;
	    }

	for (cx = bMin[X]; cx <= bMax[X]; cx++) {
	  for (cy = bMin[Y]; cy <= bMax[Y]; cy++) {
	    for (cz = bMin[Z]; cz <= bMax[Z]; cz++) {

	      cellID = EncodeIndex(layer->lDim,cx,cy,cz);
	      cell = layer->fmcData[cellID].cell;
	      
	      for (j = 0; j < 3; j++) 
		for (m = 0; m < 3; m++) 
		  {
		    sigma[j][m] = 0.0;
		  }	   

	      k = 0;
	      
	      for (i = 0; i <= uorder; i++) {
		for (nz = 0; nz <= i; nz++) {
		  for (ny = 0; ny <= i-nz; ny++) {
                    nx = i-ny-nz;
                    rp = pw[nx][0] * pw[ny][1] * pw[nz][2];
                    for (j = 0; j < 3; j++) {
		      for (m = 0; m < 3; m++) {
			sigma[j][m] += rp * cell->taylorCoeff[k*9+j*3+m];
		      }
                    }
                    k = k+1;
		  }
		}
	      }


	      for (j = 0; j < 3; j++) 
		for (m = 0; m < 3; m++) 
		  {
		    stress[j][m] += sigma[j][m];
		  }

	    }  /* loop over cz */
	  }  /* loop over cy */
	}  /* loop over cx */


	
#ifdef PARALLEL
        MPI_Allreduce(stress, aveStress, 9, MPI_DOUBLE,
                      MPI_SUM, MPI_COMM_WORLD);
	
	for (j = 0; j < 3; j++) 
	  for (m = 0; m < 3; m++) 
	    aveStress[j][m] /= Volume;

#else
        for (m = 0; m < 3; m++)
            for (k = 0; k < 3; k++)
	      aveStress[m][k] = stress[m][k]/Volume;
#endif

        return;
}


void MeanStressCorrection(Home_t *home)
{
  real8 aveStress[3][3];
  Param_t *param;
  int cellID,i,nx,ny,nz,j,m;
  FMLayer_t *layer;
  int *bMin, *bMax;
  int cx,cy,cz,k,Ncells;
  FMCell_t  *cell;

  param = home->param;
  layer = &home->fmLayer[param->fmNumLayers-1];

  bMin = layer->ownedMin;
  bMax = layer->ownedMax;

  EvalMeanTaylor(home,aveStress);

  for (cx = bMin[X]; cx <= bMax[X]; cx++) 
    for (cy = bMin[Y]; cy <= bMax[Y]; cy++)
      for (cz = bMin[Z]; cz <= bMax[Z]; cz++) 
	{
	  cellID = EncodeIndex(layer->lDim,cx,cy,cz);
	  cell = layer->fmcData[cellID].cell;

	  for (j = 0; j < 3; j++)
	    for (m = 0; m < 3; m++) 
	      {
		cell->taylorCoeff[j*3+m] -= aveStress[j][m];
	      }
	}
}
