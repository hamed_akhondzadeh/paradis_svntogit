/***************************************************************************
 *
 *  Function    : GetNativeNodes
 *  Description : Return an array of pointers to all the native nodes in
 *                the domain.
 *
 **************************************************************************/

#include "Home.h"

void GetNativeNodes (Home_t *home, Node_t ***nodeList, int *listLen)
{

   int iloc, ikey ;
   Node_t **ptrList ;

   ptrList = (Node_t **) malloc (home->newNodeKeyPtr * sizeof(Node_t *)) ;

/* Copy only the non-null node pointers from nodeKeys to ptrList */

   iloc = 0 ;
   for (ikey = 0 ; ikey < home->newNodeKeyPtr ; ikey++) {

      if (home->nodeKeys[ikey] != 0) {
         ptrList[iloc++] = home->nodeKeys[ikey] ;
      }
   }

   *listLen = iloc ;
   *nodeList = (Node_t **) realloc (ptrList, iloc*sizeof(Node_t *)) ;

   return ;
}
