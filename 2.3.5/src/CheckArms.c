/***************************************************************************
 *
 *  Function    : CheckArms
 *  Description : loop through all active nodes, for each loop through its
 *                arms. For each arm, find the other node endpoint, and
 *                ensure that the two versions of the arm values agree.
 *
 **************************************************************************/

#include "Home.h"
#include "Util.h"

#define EPSILON 1.E-12

void CheckArms(Home_t *home)
{
        int    i, iarm, jarm, found;
        real8  burgXdiff, burgYdiff, burgZdiff, nxdiff, nydiff, nzdiff;
        real8  sigbLocxdiff, sigbLocydiff, sigbLoczdiff;
        real8  sigbRemxdiff, sigbRemydiff, sigbRemzdiff;
        Node_t *nodeA, *nodeB;
        
        for (i = 0; i < home->newNodeKeyPtr; i++) {
        
            nodeA = home->nodeKeys[i];
            if (!nodeA) continue;
        
            for (iarm = 0; iarm < nodeA->numNbrs; iarm++) {
        
                if (nodeA->nbrTag[iarm].domainID == -1)
                    Fatal("CheckArms: empty arm slot found!");
        
                if (nodeA->nbrTag[iarm].domainID == home->myDomain &&
                    nodeA->nbrTag[iarm].index > i) {
                    continue;
                }
        
                nodeB = GetNodeFromTag(home, nodeA->nbrTag[iarm]);

/*
 *              Find the arm of nodeB that is connected to nodeA
 */
                found = 0;
                for (jarm = 0; jarm < nodeB->numNbrs; jarm++) {
                    if (nodeB->nbrTag[jarm].domainID == home->myDomain &&
                    nodeB->nbrTag[jarm].index == i) {
                        found = 1;
                        break;
                    }
                }

                if (!found) {
                    PrintNode(nodeA);
                    PrintNode(nodeB);
                    Fatal("CheckArms: No linkage back to (%d,%d) from (%d,%d)",
                          nodeA->myTag.domainID, nodeA->myTag.index,
                          nodeB->myTag.domainID, nodeB->myTag.index);
                }
        
/*
 *              Compare Burgers vector of two versions of arm
 */
                burgXdiff = nodeA->burgX[iarm] + nodeB->burgX[jarm];
                burgYdiff = nodeA->burgY[iarm] + nodeB->burgY[jarm];
                burgZdiff = nodeA->burgZ[iarm] + nodeB->burgZ[jarm];
        
                if ((fabs(burgXdiff) > EPSILON) ||
                    (fabs(burgYdiff) > EPSILON) ||
                    (fabs(burgZdiff) > EPSILON)) {
                    PrintNode(nodeA);
                    PrintNode(nodeB);
                    Fatal("CheckArms: inconsistent burgers vector values!");
                }
/*
 *              Compare normal of two versions of arm
 */
        
                nxdiff = nodeA->nx[iarm] - nodeB->nx[jarm];
                nydiff = nodeA->ny[iarm] - nodeB->ny[jarm];
                nzdiff = nodeA->nz[iarm] - nodeB->nz[jarm];
        
                if ((fabs(nxdiff) > EPSILON) ||
                    (fabs(nydiff) > EPSILON) ||
                    (fabs(nzdiff) > EPSILON)) {
                    PrintNode(nodeA);
                    PrintNode(nodeB);
                    Fatal("CheckArms: inconsistent nx value!");
                }
        
#if 0
                sigbLocxdiff = nodeA->sigbLoc[3*iarm] -
                               nodeB->sigbLoc[3*jarm];

                sigbLocydiff = nodeA->sigbLoc[3*iarm+1] -
                               nodeB->sigbLoc[3*jarm+1];

                sigbLoczdiff = nodeA->sigbLoc[3*iarm+2] -
                               nodeB->sigbLoc[3*jarm+2];

                sigbRemxdiff = nodeA->sigbRem[3*iarm] -
                               nodeB->sigbRem[3*jarm];

                sigbRemydiff = nodeA->sigbRem[3*iarm+1] -
                               nodeB->sigbRem[3*jarm+1];

                sigbRemzdiff = nodeA->sigbRem[3*iarm+2] -
                               nodeB->sigbRem[3*jarm+2];
#endif
#if 0
                if (fabs(sigbLocxdiff) > EPSILON)
                    Fatal("CheckArms: inconsistent sigbLocx value!");
                if (fabs(sigbLocydiff) > EPSILON)
                    Fatal("CheckArms: inconsistent sigbLocy value!");
                if (fabs(sigbLoczdiff) > EPSILON)
                    Fatal("CheckArms: inconsistent sigbLocz value!");
                if (fabs(sigbRemxdiff) > EPSILON)
                    Fatal("CheckArms: inconsistent sigbRemx value!");
                if (fabs(sigbRemydiff) > EPSILON)
                    Fatal("CheckArms: inconsistent sigbRemy value!");
                if (fabs(sigbRemzdiff) > EPSILON)
                    Fatal("CheckArms: inconsistent sigbRemz value!");
#endif
        }
        }
        return;
        }
