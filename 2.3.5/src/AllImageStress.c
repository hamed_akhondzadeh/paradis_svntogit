/***************************************************************************
 *
 *      Function:    AllImageStress
 *      Description: Calculate the stress boundary conditions for
 *                   FEM on the fem nodes at free surfaces in order to 
 *                   obtain the image stresses due to the traction
 *                   free boundary condition. 
 *
 *                   Note: This subroutine is structurally similar
 *                   to AllSesgmentStress.c & AllYoffeStress.c
 * 
 *                   For any given fem node position, it has the following three
 *                   parts of contributions: 
 *                   1) for segments intersect the same surface of fem node, (
 *                      e.g., AB with A at the surface), the stress contribution
 *                      is sigma_B'B_inf; 
 *                   2) for segments AB that do not intersect any surface: 
 *                      sigma_AB_inf;
 *                   3) for segments AB intersect other surfaces, with A at a
 *                      surface different from the fem node's surface: 
 *                      sigma_AB_inf + sigma_AB'_y-img, where B' is the extending
 *                      end of segment AB (i.e., to extend AB to infinity AB')
 * 
 *                   M. Tang 6/21/2004 
 *
 **************************************************************************/
#include "Home.h"
#include "Util.h"
#include <math.h>
#include "FEM.h"

void AllImageStress(Home_t *home, real8 xm,real8 ym,real8 zm,
                    real8 totStress[][3], real8 totStressImg[6])
{
        int     i, mm, kk, nc2, ti2;
        int     nodeAonsurface, nodeBonsurface;
        int     surfaceIDA, surfaceIDB; 
        int     isign; 
        int     node_number[2],node_numberA[2],node_numberB[2]; 
        real8   Lx, Ly, Lz, xmin, ymin, zmin, xmax, ymax, zmax;
        real8   x1,y1,z1,x2,y2,z2,dx,dy,dz,xA,yA,zA,xB,yB,zB,xC,yC,zC,dr;
        real8   sigma[3][3],sigmaSH[6];
        real8   MU, NU, burgX, burgY, burgZ, a;
        real8   r[3], rs[3], rm[3], b[3];
        real8   surf_norm[3],surf_normA[3],surf_normB[3];
        Node_t  *rNodeA, *rNodeB;
        Param_t *param;
        
        param = home->param;
        Lx = param->Lx;
        Ly = param->Ly;
        Lz = param->Lz;
        
        MU = param->shearModulus;
        NU = param->pois;
        a  = param->rc;
           
/*
 *      Initialize stress to zero
 */
        for (mm = 0; mm < 3; mm++) {
            for (kk = 0; kk < 3; kk++) {
                totStress[mm][kk] = 0.0;
            }
        }
        
        for (mm = 0; mm < 6; mm++) {
            totStressImg[mm] = 0.0;
        }
        
        node_on_surface_(&node_number, &surf_norm, &xm, &ym, &zm); 

        if (node_number[0] ==0 || node_number[0] < 0 ) {
            Fatal("AllImageStress(): ERROR: point should be on surface!"); 
            return; 
        }
        
/*
 *      Loop through all segments
 */
        for (i = 0; i < home->newNodeKeyPtr; i++) {
            if ((rNodeB = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }
        
            xB = rNodeB->x;
            yB = rNodeB->y;
            zB = rNodeB->z;
        
            nc2 = rNodeB->numNbrs;
        
            node_numberB[0] = rNodeB->fem_Surface[0];
            node_numberB[1] = rNodeB->fem_Surface[1];
            surf_normB[0] = rNodeB->fem_Surface_Norm[0];
            surf_normB[1] = rNodeB->fem_Surface_Norm[1];
            surf_normB[2] = rNodeB->fem_Surface_Norm[2];

            if (node_numberB[0] < 0) {
                Fatal("AllImageStress(): node B is outside box already in AllImageStress.c"); 
            }
        
            for (ti2 = 0; ti2 < nc2; ti2++) {
                   
                rNodeA = GetNeighborNode(home, rNodeB, ti2);
                if (!rNodeA) continue;
                       
                if (OrderNodes(rNodeA, rNodeB) != 1) continue;
        
                dx = xB - rNodeA->x;
                dy = yB - rNodeA->y;
                dz = zB - rNodeA->z;
        
/*
 *              (PBC disabled for this function)
 *
 *              ZImage(param, &dx, &dy, &dz);
 */
                xA = xB - dx;
                yA = yB - dy;
                zA = zB - dz;
        
                node_numberA[0] = rNodeA->fem_Surface[0];
                node_numberA[1] = rNodeA->fem_Surface[1];
                surf_normA[0] = rNodeA->fem_Surface_Norm[0];
                surf_normA[1] = rNodeA->fem_Surface_Norm[1];
                surf_normA[2] = rNodeA->fem_Surface_Norm[2];

                if (node_numberA[0] < 0) {
                    Fatal("AllImageStress(): node A is outside box already in AllImageStress.c"); 
                }

                burgX = rNodeB->burgX[ti2];
                burgY = rNodeB->burgY[ti2];
                burgZ = rNodeB->burgZ[ti2];
        
/*
 *              MT if both nodes lie at surfaces: 
 *                  if they are on the same surface, ignore it
 *                  if they are on different surfaces, could happen when 
 *                  it's a short segment. 
 */ 
                r[0]=xm;
                r[1]=ym;
                r[2]=zm;

                b[0]=burgX;
                b[1]=burgY;
                b[2]=burgZ; 
        
                if ((node_numberA[0] > 0 ) && (node_numberB[0] > 0 )) {
                    if (node_numberA[0]==node_numberB[0] &&
                        node_numberA[1]==node_numberB[1]) {
                        continue;
                    } else {
                        Fatal("a segment touches two different "
                              "surfaces at once");
                    } 
                }
        
/*
 *              If segment A-B is intersecting the surface of the field point
 */
                nodeAonsurface = (node_number[0]==node_numberA[0] &&
                                  node_number[1]==node_numberA[1]);
                nodeBonsurface = (node_number[0]==node_numberB[0] &&
                                  node_number[1]==node_numberB[1]);
        
/*
 *              Initialize sigmaSH
 */
                for (mm = 0; mm < 6; mm++) {
                    sigmaSH[mm] = 0.0;
                }
        
                if (node_numberA[0] > 0) {
                    if (nodeAonsurface) {
                        dr = sqrt(dx*dx+dy*dy+dz*dz);
                        dr = 1e5 / dr;
/*
 *                      In this case: C = B'
 */ 
                        xC = xB + dx*dr;
                        yC = yB + dy*dr;
                        zC = zB + dz*dr;

                        SegmentStress(MU, NU, burgX, burgY, burgZ,
                                      xB, yB, zB, xC, yC, zC,
                                      xm, ym, zm, a, sigma);
/*
                        printf("xm,ym,zm = %f %f %f\n", xm, ym, zm); 
                        printf("xC,yC,zC = %f %f %f\n", xC, yC, zC);
                        printf("xB,yB,zB = %f %f %f\n", xB, yB, zB);
                        printf("sigma = %f %f %f %f %f %f\n",
                               sigma[0][0], sigma[0][1], sigma[0][2],
                               sigma[1][1], sigma[1][2], sigma[2][2] );
*/
                    } else {
/*
 *                      C = A'
 */ 
                        dr = sqrt(dx*dx+dy*dy+dz*dz);
                        dr = 1e5 / dr;
                        xC = xB - dx*dr;
                        yC = yB - dy*dr;
                        zC = zB - dz*dr;
                        SegmentStress(MU, NU, burgX, burgY, burgZ,
                                      xB, yB, zB, xC, yC, zC,
                                      xm, ym, zm, a, sigma);
                        rs[0] = xA;
                        rs[1] = yA;
                        rs[2] = zA; 

                        rm[0] = xB;
                        rm[1] = yB;
                        rm[2] = zB;

                        isign = 1;

                        sh_image_stress_num_(r, rs, rm, surf_normA, &isign, b,
                                           &MU, &NU, sigmaSH);
/*
                        printf("r = %f %f %f\n", r[0], r[1], r[2]); 
                        printf("rs= %f %f %f\n", rs[0], rs[1], rs[2]);
                        printf("rm = %f %f %f\n", rm[0], rm[1], rm[2]);
                        printf("surf_normA = %f %f %f\n",
                               surf_normA[0], surf_normA[1], surf_normA[2]);
                        printf("sigmaSH = %f %f %f %f %f %f\n",
                               sigmaSH[0], sigmaSH[1], sigmaSH[2],
                               sigmaSH[3], sigmaSH[4], sigmaSH[5]);
*/
                    }
                } else if (node_numberB[0] > 0) {
                    if (nodeBonsurface) {
                        dr = sqrt(dx*dx+dy*dy+dz*dz);
                        dr = 1e5 / dr;
                        xC = xA - dx*dr;
                        yC = yA - dy*dr;
                        zC = zA - dz*dr;
                        SegmentStress(MU, NU, burgX, burgY, burgZ,
                                      xC, yC, zC, xA, yA, zA,
                                      xm, ym, zm, a, sigma);
/*
                        printf("node=B\n"); 
                        printf("xm,ym,zm = %f %f %f\n", xm, ym, zm); 
                        printf("xA,yA,zA = %f %f %f\n", xA, yA, zA);
                        printf("xC,yC,zC = %f %f %f\n", xC, yC, zC);
                        printf("sigma = %f %f %f %f %f %f\n",
                               sigma[0][0], sigma[0][1], sigma[0][2],
                               sigma[1][1], sigma[1][2], sigma[2][2] );
*/
                    } else {
                        dr = sqrt(dx*dx+dy*dy+dz*dz);
                        dr = 1e5 / dr;
                        xC = xA + dx*dr;
                        yC = yA + dy*dr;
                        zC = zA + dz*dr;
                        SegmentStress(MU, NU, burgX, burgY, burgZ,
                                      xC, yC, zC, xA, yA, zA,
                                      xm, ym, zm, a, sigma);
                        rs[0] = xB;
                        rs[1] = yB;
                        rs[2] = zB; 

                        rm[0] = xA;
                        rm[1] = yA;
                        rm[2] = zA;

                        isign = -1; 

                        sh_image_stress_num_(r, rs, rm, surf_normB, &isign, b,
                                           &MU, &NU, sigmaSH);
                    }
                } else {
                    SegmentStress(MU, NU, burgX, burgY, burgZ,
                                  xB, yB, zB, xA, yA, zA,
                                  xm, ym, zm, a, sigma);
                }
        
                for (mm = 0; mm < 3; mm++) {
                    for (kk = 0; kk < 3; kk++) {
                        totStress[mm][kk] += sigma[mm][kk];
                    }
                }

                for (mm = 0; mm < 6; mm++) {
                    totStressImg[mm] += sigmaSH[mm];
                }
                   
            }  /* end for (iNbr2...) */
        }  /* end for (i=0;...) */

        return;
}
