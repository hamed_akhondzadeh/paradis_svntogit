/**************************************************************************
 *
 *      Module:       Mobility_BCC_glide
 *      Description:  Contains functions for calculating mobility of nodes
 *                    in BCC metals.  Based on MobilityLaw_BCC_0b.c
 *                    This is really only slightly modified from
 *                    mobility BCC_0b by first obtaining the velocity assuming 
 *                    climbing exists, then project out the climb component 
 *                    from the velocity. 
 *
 *      Authors:      Meijie Tang         Nov. 1, 2007
 *
 *      Includes functions:
 *
 *            Mobility_BCC_glide()
 *                
 ***************************************************************************/
#include "Home.h"
#include "Util.h"
#include "Mobility.h"
#include <stdio.h>
#include <math.h>
#include <sys/param.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef _FEM
#include "FEM.h"
#endif


/**************************************************************************
 *
 *      Function:     Mobility_BCC_glide
 *      Description:  This function calculates the velocity for a single
 *                    specified node.
 *
 *      Returns: 0 on success
 *               1 if velocity could not be determiend
 * 
 *************************************************************************/
int Mobility_BCC_glide(Home_t *home, Node_t *node)
{
        int     i, j, nbrs, nss;
        real8   vN;
        real8   bx, by, bz;
        real8   dx, dy, dz;
        real8   mx, my, mz;
        real8   nx, ny, nz; 
        real8   temp;
        real8   mag, mag2, halfMag, invMag;
        real8   invbMag2, bMag2, costheta, costheta2;
        real8   Bline, Bscrew, Bedge, Bglide, Bclimb, Beclimb;
        real8   Bscrew2, Bedge2, Beclimb2;
        real8   invBscrew2, invBedge2;
        real8   BlmBsc, BclmBsc, BglmBsc, BlmBecl;
        real8   eps = 1.0e-12, eps1 = 1.0e-2, eps2 = 0.95;
        real8   nForce[3], nVel[3], velCorr[3];
        real8   Btotal[3][3] = {{0.0, 0.0, 0.0},
                                {0.0, 0.0, 0.0},
                                {0.0, 0.0, 0.0}};
        real8   invBtotal[3][3], tensCorr[3][3];

        real8   normX[3],normY[3],normZ[3],veca[3],vecb[3],vecc[3];
        int     SameNorm, norms; 
        real8   tor=1.e-5; 

        Node_t  *nbrNode, *nbr0, *nbr1;
        Param_t *param;

/*
 *      If the node is a "fixed" node, we cannot move it, so just zero
 *      out the velocity and return.
 */
        if (node->constraint == 7) {
            node->vX = 0.0;
            node->vY = 0.0;
            node->vZ = 0.0;
            return(0);
        }

        param = home->param;

        Bscrew     = 1.0 / param->MobScrew;
        Bedge      = 1.0 / param->MobEdge;

        /* Beclimb    = 1.0 / param->MobClimb; */ 
        Beclimb    = 1.0e10; 
        

        Bscrew2    = Bscrew * Bscrew;
        Bedge2     = Bedge * Bedge;
        Beclimb2   = Beclimb * Beclimb;

        Bline      = 1.0e-2 * MIN(Bscrew, Bedge);
        BlmBsc     = Bline - Bscrew;
        BlmBecl    = Bline - Beclimb; 

        invBscrew2 = 1.0 / (Bscrew*Bscrew);
        invBedge2  = 1.0 / (Bedge*Bedge);

        nbrs = node->numNbrs;

/*
 *      If any of the arms of the node has a burgers vector that
 *      has explicitly been set to be sessile (via control file
 *      inputs), the node may not be moved.
 */
        nss = (int)param->sessileburgspec[0];

        if (nss > 0) {
            real8 ssbx, ssby, ssbz, sslx, ssly, sslz;

            for (i = 0; i < nss; i++) {

                ssbx = param->sessileburgspec[i*3+1];
                ssby = param->sessileburgspec[i*3+2];
                ssbz = param->sessileburgspec[i*3+3];

                sslx = param->sessilelinespec[i*3+1];
                ssly = param->sessilelinespec[i*3+2];
                sslz = param->sessilelinespec[i*3+3];
            
                for (j = 0; j < nbrs; j++) {

                    bx = node->burgX[j];
                    by = node->burgY[j];
                    bz = node->burgZ[j];
                    
                    if (((bx == ssbx) && (by == ssby) && (bz == ssbz)) ||
                        ((bx == -ssbx) && (by == -ssby) && (bz == -ssbz))){
                        if ((sslx == 0) && (ssly == 0) && (sslz == 0)) {
                            node->vX = 0.0;
                            node->vY = 0.0;
                            node->vZ = 0.0;
                            return(0);
                        } else {
                            nbrNode = GetNeighborNode(home, node, j);

                            dx = nbrNode->x - node->x;
                            dy = nbrNode->y - node->y;
                            dz = nbrNode->z - node->z;

                            ZImage (param, &dx, &dy, &dz) ;

                            mag = sqrt(dx*dx + dy*dy + dz*dz);
                            invMag = 1.0 / mag;

                            dx *= invMag;
                            dy *= invMag;
                            dz *= invMag;

                            if (((fabs(dx-sslx) < 1e-2) &&
                                 (fabs(dy-ssly) < 1e-2) &&
                                 (fabs(dz-sslz) < 1e-2)) ||
                                ((fabs(dx+sslx) < 1e-2) &&
                                 (fabs(dy+ssly) < 1e-2) &&
                                 (fabs(dz+sslz) < 1e-2))) {
                                node->vX = 0.0;
                                node->vY = 0.0;
                                node->vZ = 0.0;
                                return(0);
                            }
                        }
                    }
                }
            }
        }

/*
 *      find out the independent glide planes the node's neighbor segments have 
 */

#if 0
        for (i = 0; i< nbrs; i++) {
            printf("nx, ny, nz = %f %f %f\n", node->nx[i], node->ny[i],
                   node->nz[i]);
        }
#endif

        norms = 0; 
        normX[0] = node->nx[0]; normY[0] = node->ny[0]; normZ[0] = node->nz[0]; 
        Normalize(&normX[0],&normY[0],&normZ[0]);

/*
 *      Modified per Tom Arsenlis: Once you find a norm, project the other
 *      normals to be perpendicular
 */
        if(nbrs > 1) { 
            i=1;
            while((norms<2)&&(i<nbrs)){
                nx = node->nx[i];
                ny = node->ny[i];
                nz = node->nz[i];

                Normalize(&nx,&ny,&nz); 

                if ( norms==0) {
                    temp=normX[0]*nx+normY[0]*ny+normZ[0]*nz;
                    if (fabs(1.0e0-temp)>tor ) {
                        norms = 1;
                        nx-=temp*normX[0];
                        ny-=temp*normY[0];
                        nz-=temp*normZ[0];
                        Normalize(&nx,&ny,&nz);
                        normX[1]=nx;
                        normY[1]=ny; 
                        normZ[1]=nz;
                        normX[2]=normY[0]*normZ[1]-normZ[0]*normY[1];
                        normY[2]=normZ[0]*normX[1]-normX[0]*normZ[1];
                        normZ[2]=normX[0]*normY[1]-normY[0]*normX[1];
                    }
                } else {/* norms==1*/
                    temp=normX[2]*nx+normY[2]*ny+normZ[2]*nz;
                    if (fabs(temp)>tor ) {
                        norms = 2;
                    }
                }

                i++;
            } /* end while */
        } /* end if (nbrs > 1) */   

        node->vX = 0.0e0;
        node->vY = 0.0e0;
        node->vZ = 0.0e0;

        if (norms<2){
/*
 *          Loop over all arms of the node, adding each arm's contribution
 *          to the drag matrix.
 */
            for (i = 0; i < nbrs; i++) {  

                bx = node->burgX[i];
                by = node->burgY[i];
                bz = node->burgZ[i];

                bMag2 = (bx*bx + by*by + bz*bz);
                invbMag2 = 1.0 / bMag2;

/*
 *              Calculate the length of the arm and its tangent line direction
 */
                nbrNode = GetNeighborNode(home, node, i);

                dx = nbrNode->x - node->x;
                dy = nbrNode->y - node->y;
                dz = nbrNode->z - node->z;

                ZImage(param, &dx, &dy, &dz);

                mag2    = dx*dx + dy*dy + dz*dz;
/*
 *              If the segment is zero length (which can happen when
 *              the mobility function is being called from SplitMultiNodes())
 *              just skip the segment.
 */
                if (mag2 < eps) {
                    continue;
                }

                mag     = sqrt(mag2);
                halfMag = mag/2.0;
                invMag  = 1.0 / mag;

                dx *= invMag;
                dy *= invMag;
                dz *= invMag;

/*
 *              Calculate how close to screw the arm is
 */
                costheta = (dx*bx + dy*by + dz*bz);
                costheta2 = (costheta*costheta) * invbMag2;

/*
 *              [0 0 1] arms don't move as readily as other arms, so must be
 *              handled specially.
 */
                if (fabs(bx*by*bz) < eps) {
                    if (nbrs == 2) {
                        Btotal[0][0] += halfMag * Beclimb;
                        Btotal[1][1] += halfMag * Beclimb;
                        Btotal[2][2] += halfMag * Beclimb;
                    } else {
                        Btotal[0][0] += halfMag * (dx*dx * BlmBecl + Beclimb);
                        Btotal[0][1] += halfMag * (dx*dy * BlmBecl);
                        Btotal[0][2] += halfMag * (dx*dz * BlmBecl);
                        Btotal[1][1] += halfMag * (dy*dy * BlmBecl + Beclimb);
                        Btotal[1][2] += halfMag * (dy*dz * BlmBecl);
                        Btotal[2][2] += halfMag * (dz*dz * BlmBecl + Beclimb);
                    }
                } else  {
/*
 *                  Arm is not [0 0 1], so build the drag matrix assuming the
 *                  dislocation is screw type
 */
                    Btotal[0][0] += halfMag * (dx*dx * BlmBsc + Bscrew);
                    Btotal[0][1] += halfMag * (dx*dy * BlmBsc);
                    Btotal[0][2] += halfMag * (dx*dz * BlmBsc);
                    Btotal[1][1] += halfMag * (dy*dy * BlmBsc + Bscrew);
                    Btotal[1][2] += halfMag * (dy*dz * BlmBsc);
                    Btotal[2][2] += halfMag * (dz*dz * BlmBsc + Bscrew);

/*
 *                  Now correct the drag matrix for dislocations that are
 *                  not screw
 */
                    if ((1.0 - costheta2) > eps) {


                        nx = node->nx[i];
                        ny = node->ny[i];
                        nz = node->nz[i];

#if 0
                        printf("nx, ny, nz = %f %f %f\n", nx, ny, nz); 
#endif

                        xvector(nx, ny, nz, dx, dy, dz, &mx, &my, &mz);

                        Bglide = sqrt(invBedge2 + (invBscrew2-invBedge2) *
                                      costheta2);
                        Bglide = 1.0 / Bglide;
                        Bclimb = sqrt(Beclimb2 + (Bscrew2 - Beclimb2) *
                                      costheta2);

#ifdef NAN_CHECK
                        if (isnan(Bglide) != 0) {
                            Fatal("Mobility_BCC_glide: Bglide = NaN\n"
                                "  Bglide = sqrt(invBedge2 + "
                                "(invBscrew2-invBedge2)*costheta2)\n"
                                "  where invBedge2 = %lf, invBscrew2 = %lf, "
                                "costheta2 = %lf", invBedge2, invBscrew2,
                                costheta2);
                        }

                        if (isnan(Bclimb) != 0) {
                            Fatal("Mobility_BCC_glide: Bclimb = NaN\n"
                                "  Bclimb = sqrt(Beclimb2 + "
                                "(Bscrew2-Beclimb2)*costheta2)\n"
                                "  where Beclimb2 = %lf, Bscrew2 = %lf, "
                                "costheta2 = %lf", Beclimb2, Bscrew2,
                                costheta2);
                        }
#endif
                        BclmBsc = Bclimb - Bscrew;
                        BglmBsc = Bglide - Bscrew;


                        Btotal[0][0] += halfMag * (nx*nx * BclmBsc +
                                        mx*mx * BglmBsc);
                        Btotal[0][1] += halfMag * (nx*ny * BclmBsc +
                                        mx*my * BglmBsc);
                        Btotal[0][2] += halfMag * (nx*nz * BclmBsc +
                                        mx*mz * BglmBsc);
                        Btotal[1][1] += halfMag * (ny*ny * BclmBsc +
                                        my*my * BglmBsc);
                        Btotal[1][2] += halfMag * (ny*nz * BclmBsc +
                                        my*mz * BglmBsc);
                        Btotal[2][2] += halfMag * (nz*nz * BclmBsc +
                                        mz*mz * BglmBsc);
                    }
                }  /* End non-[0 0 1] arm */
            }  /* End loop over arms */

            Btotal[1][0] = Btotal[0][1];
            Btotal[2][0] = Btotal[0][2];
            Btotal[2][1] = Btotal[1][2];
/*
 *          At this point we should check if the matrix is invertable and
 *          if it isn't, find the eigen values and eigen vectors of the drag
 *          matrix, then invert the drag matrix keeping zero eigen values
 *          as zero.
 *
 *          FIX ME!  For now, we're assuming the matrix is invertable.
 */
        
            nForce[0] = node->fX;
            nForce[1] = node->fY;
            nForce[2] = node->fZ;

            if (Matrix33Invert(Btotal, invBtotal) == 0) {
                Fatal("Mobility_BCC_glide: Cannot invert 3X3 matrix!");
            }
            Matrix33Vector3Multiply(invBtotal, nForce, nVel);

/*
 *          orthogonolize the velocity with all indepdent glide
 *          normal vectors. 
 */ 
            for (i = 0; i <= norms; i++) {
                nx = normX[i]; ny=normY[i]; nz=normZ[i]; 
                temp=nVel[0]*nx+nVel[1]*ny+nVel[2]*nz;
                nVel[0]-=  temp*nx;
                nVel[1]-=  temp*ny;
                nVel[2]-=  temp*nz;
            }

            node->vX = nVel[0];
            node->vY = nVel[1];
            node->vZ = nVel[2];
        }

#if 0
/*
 *THE FOLLOWING CORRECTION DOES NOT WORK WELL AND NEEDS
 *MORE WORK BEFORE IT CAN BE INCLUDED IN THE CODE!
 *
 *Correction to adjust Bline
 */
        if (nbrs == 2) {
            nbr0 = GetNeighborNode(home, node, 0);
            nbr1 = GetNeighborNode(home, node, 1);

            dx = nbr1->x - nbr0->x;
            dy = nbr1->y - nbr0->y;
            dz = nbr1->z - nbr0->z;

            ZImage(param, &dx, &dy, &dz);
            mag = sqrt(dx*dx + dy*dy + dz*dz);
            invMag = 1.0 / mag;

            dx *= invMag;
            dy *= invMag;
            dz *= invMag;

            tensCorr[0][0] = 1.0 - (0.9 * dx*dx);
            tensCorr[0][1] = 0.0 - (0.9 * dx*dy);
            tensCorr[0][2] = 0.0 - (0.9 * dx*dz);
            tensCorr[1][1] = 1.0 - (0.9 * dy*dy);
            tensCorr[1][2] = 0.0 - (0.9 * dy*dz);
            tensCorr[2][2] = 1.0 - (0.9 * dz*dz);

            tensCorr[1][0] = tensCorr[0][1];
            tensCorr[2][0] = tensCorr[0][2];
            tensCorr[2][1] = tensCorr[1][2];

            Matrix33Vector3Multiply(tensCorr, nVel, velCorr);

            node->vX = velCorr[0];
            node->vY = velCorr[1];
            node->vZ = velCorr[2];
        }
#endif

        node->nconstraint = 0;
   
#ifdef _FEM
/*
 *      The velocity of any surface node along the negative surface
 *      normal direction should be zero to prevent the node moving into
 *      the box.  Make a call to adjsut the velocity accordingly.
 *
 *      Note: If the node moves outside the box, it's allowed here, then
 *      position is adjusted in AdjustNodePosition().
 */
        AdjustSurfaceNodeVel(home, node);
#endif

        return(0);
}
