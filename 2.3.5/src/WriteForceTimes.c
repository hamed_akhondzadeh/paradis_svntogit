/***************************************************************
 *
 *  Function    : WriteForceTimes
 *  Description : Write out time each domain takes to do force
 *                calc each timestep
 *
 ***************************************************************/

#include "Home.h"
#include "Util.h"
#include "Timer.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

void WriteForceTimes (Home_t *home)
{
   FILE *file;
   real8 myForceTime ;
   real8 *times ;
   int i ;

   if (home->myDomain == 0) {
	   times = (real8 *) malloc (home->numDomains * sizeof(real8)) ;
   }

   myForceTime = home->timers[CALC_FORCE].incr ;

#ifdef PARALLEL
   MPI_Gather (&myForceTime, 1, MPI_DOUBLE, times, 1, MPI_DOUBLE,
		   0, MPI_COMM_WORLD) ;
#endif

   if (home->myDomain == 0) {

	   file = fopen ("force_times", "a") ;
	   fprintf (file, "\n**** CYCLE %d  ****\n\n", home->cycle) ;

	   for (i = 0 ; i < home->numDomains ; i++) {

		   fprintf (file, "dom %4d: time %12.6f\n",
				   i, times[i]) ;

	   }
	   fclose (file) ;
   }
   return ;
}
