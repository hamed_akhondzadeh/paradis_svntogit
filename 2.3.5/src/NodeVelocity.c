/**************************************************************************
 *
 *      Module:       NodeVelocity.c
 *      Description:  Contains functions to control setting nodal
 *                    velocities, generating velocity statistics,
 *                    and setting the timestep.
 *
 ***************************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>

#include "Home.h"
#include "Mobility.h"
#include "Util.h"

#ifdef PARALLEL
#include "mpi.h"
#endif


#define MIN_DELTA_T 1.0e-11
#define FFACTOR_MAXDT 1e-4

/*
 *     Define the types of velocity statistics to be accumulated.
 *     NOTE: V_MAXSTATS should always be the final item in the
 *     enumerated list below.
 */
typedef enum {
        V_NODE_COUNT = 0,
        V_AVERAGE_X,
        V_AVERAGE_Y,
        V_AVERAGE_Z,
        V_VAR,
        V_MAXSTATS
} VStatTypes_t;

/*-------------------------------------------------------------------------
 *
 *      Function:    GetVelocityStatistics
 *      Description: If gathering of velocity statistics is enabled,
 *                   gather the statistics (defined by the VStatTypes_t
 *                   above)
 *
 *------------------------------------------------------------------------*/
void GetVelocityStatistics(Home_t *home)
{
        int      i, nodeCount;
        real8    velStatsLocal[V_MAXSTATS], velStatsGlobal[V_MAXSTATS];
        real8    v2, vx, vy, vz;
        real8    v2sq, vAveragesq, vStDev;
        real8    vAverage, vAveragex, vAveragey, vAveragez;
        Param_t  *param;
        Node_t   *node;


#ifdef VEL_STATISTICS

        param = home->param;

/*
 *      Zero out some stuff before starting
 */
        for (i = 0; i < V_MAXSTATS; i++) {
            velStatsLocal[i]  = 0.0;
            velStatsGlobal[i] = 0.0;
        }

/*
 *      Loop over all the nodes, accumulating all the necessary data
 */
        for (i=0; i<home->newNodeKeyPtr;i++) {

            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }

            vx = node->vX;
            vy = node->vY;
            vz = node->vZ;

            v2 = vx*vx+vy*vy+vz*vz;

/*
 *        If we're gathering statistics, accumulate the necessary data.
 *        Otherwise just find the highest nodal velocity.
 */
            velStatsLocal[V_AVERAGE_X] += vx;
            velStatsLocal[V_AVERAGE_Y] += vy;
            velStatsLocal[V_AVERAGE_Z] += vz;
            velStatsLocal[V_VAR]       += v2;
            velStatsLocal[V_NODE_COUNT]++;
        }

        if (velStatsLocal[V_NODE_COUNT] > 0) {

            MPI_Allreduce(velStatsLocal, velStatsGlobal, V_MAXSTATS,
                          MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

            nodeCount = velStatsGlobal[V_NODE_COUNT];

            vAveragex = velStatsGlobal[V_AVERAGE_X] / nodeCount;
            vAveragey = velStatsGlobal[V_AVERAGE_Y] / nodeCount;
            vAveragez = velStatsGlobal[V_AVERAGE_Z] / nodeCount;
 
            vAveragesq = vAveragex*vAveragex +
                         vAveragey*vAveragey +
                         vAveragez*vAveragez;
 
            vStDev = sqrt(velStatsGlobal[V_VAR] / nodeCount) - vAveragesq;

            param->vStDev   = vStDev;
            param->vAverage = sqrt(vAveragesq);
        }

#endif  /* ifdef VEL_STATISTICS */

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    CalcNodeVelocities
 *      Description: Driver function that will invoke the appropriate
 *                   mobility function to update the velocity of every
 *                   native node, then apply the velocity cutoff if
 *                   applicable.
 *
 *      Arguments:
 *          zeroOnErr  Flag indicating if nodal velocity should be
 *                     zeroed for any node for which the mobility
 *                     function was unable to calculate a velocity.
 *
 *      Returns:  0 if velocity was successfully calculated for all
 *                  nodes
 *                1 if the mobility functions were unable to converge
 *                  on a velocity for one or more nodes.
 *
 *------------------------------------------------------------------------*/
int CalcNodeVelocities(Home_t *home, int zeroOnErr)
{
        int     i, mobError = 0;
        Node_t  *node;
        Param_t *param;

        TimerStart(home, CALC_VELOCITY);

        param = home->param;

/*
 *      Loop over all the local nodes calculating the velocity
 */
        for (i = 0; i < home->newNodeKeyPtr; i++){

            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }

/*
 *          We set a pointer to the appropriate mobility function
 *          during initialization, so just invoke the function now.
 */
            mobError |= param->mobilityFunc(home, node);

/*
 *          Only certain of the mobility functions may actually
 *          return an error, but when they do, we might as well
 *          stop because we'll probably be cutting the timestep
 *          and recalculating forces before we try again.
 */
            if (mobError) {
                if (zeroOnErr) {
                    node->vX = 0.0;
                    node->vY = 0.0;
                    node->vZ = 0.0;
                }

                break;
            }
        }

        GetVelocityStatistics(home);

        TimerStop(home, CALC_VELOCITY);
#if PARALLEL
#ifdef SYNC_TIMERS
        TimerStart(home, CALC_VELOCITY_BARRIER);
        MPI_Barrier(MPI_COMM_WORLD);
        TimerStop(home, CALC_VELOCITY_BARRIER);
#endif
#endif

        return(mobError);
}
