/***************************************************************************
 *
 *      Function:    AllYoffeStress
 *      Description: Calculate Yoffe image stresses due to all segments
 *                   that intersect with free surfaces, and at any given
 *                   field point. 
 *
 *                   Note: This subroutine is structurally similar
 *                   to AllSegmentStress.c & AllImageStress.c 
 *
 *                   M. Tang, 6/21/2004. 
 *                   Modification is made in order to get correct 
 *                   answers for parallel computing. 
 *                   G. Hommes. 9/7/2007
 *
 ***************************************************************************/
#include "Home.h"
#include "Util.h"
#include <math.h>
#include "FEM.h"

static int yoffeUseOldPosition = 0;


void YoffeSetPositionFlag(int flag)
{
        yoffeUseOldPosition = flag;
}


void AllYoffeStress(Home_t *home, real8 xm,real8 ym,real8 zm, real8
                    totStressSH[6])
{
        int     i, mm, kk, nc2, ti2;
        int     isign, done, doNativeSegs, doGhostSegs; 
        int     node_numberA[2],node_numberB[2]; 
        real8   surf_normA[3], surf_normB[3];
        real8   Lx, Ly, Lz, xmin, ymin, zmin, xmax, ymax, zmax;
        real8   x1,y1,z1,x2,y2,z2,dx,dy,dz,xA,yA,zA,xB,yB,zB,xC,yC,zC,dr;
        real8   MU, NU, burgX, burgY, burgZ, a;
        real8   r[3], rs[3], rm[3], b[3];
        real8   sigmaSH[6];
        Node_t  *rNodeA, *rNodeB, *nextGhostNode;
        Param_t *param;
        
/*
 *      Calculate Yoffe image stresses due to all segments that intersect
 *      with free surfaces at point (xm, ym, zm) 
 *
 *      no PBC is assumed
 */
        param = home->param;

        Lx = param->Lx;
        Ly = param->Ly;
        Lz = param->Lz;
        
        MU = param->shearModulus;
        NU = param->pois;
        a  = param->rc;
           
/*
 *      Initialize stress to zero
 */
        for (mm = 0; mm < 6; mm++) {
            totStressSH[mm] = 0.0;
        }
             
        i = 0;
        done = 0;
        doGhostSegs = 0;
        doNativeSegs = 1;
        nextGhostNode = home->ghostNodeQ;

/*
 *      Loop through all nearby segments (native and ghost)
 */
        while (!done) {
/*
 *          First use the native node list
 */
            if (doNativeSegs) {
                if (i >= home->newNodeKeyPtr) {
                    doNativeSegs = 0;
                    doGhostSegs = 1;
                    continue;
                }
                if ((rNodeB = home->nodeKeys[i++]) == (Node_t *)NULL) {
                    continue;
                }
            }

/*
 *          If the native nodes have all been checked, use the
 *          ghost node queue.
 */
            if (doGhostSegs) {
                if ((rNodeB = nextGhostNode) == (Node_t *)NULL) {
                    done = 1;
                    continue;
                }
                nextGhostNode = rNodeB->next;
            }

/*
 *          In some cases (such as timestep integration) we need
 *          to use the old nodal positions.  The <yoffeUseOldPosition>
 *          flag should be set prior to the invocation of this function
 *          to indicate whether the old or new positions are to be used.
 */
            if (yoffeUseOldPosition) {
                xB = rNodeB->oldx;
                yB = rNodeB->oldy;
                zB = rNodeB->oldz;
            } else {
                xB = rNodeB->x;
                yB = rNodeB->y;
                zB = rNodeB->z;
            }

            node_numberB[0] = rNodeB->fem_Surface[0];
            node_numberB[1] = rNodeB->fem_Surface[1];
            surf_normB[0] = rNodeB->fem_Surface_Norm[0];
            surf_normB[1] = rNodeB->fem_Surface_Norm[1];
            surf_normB[2] = rNodeB->fem_Surface_Norm[2];
               
            nc2 = rNodeB->numNbrs;
        
            for (ti2 = 0; ti2 < nc2; ti2++) {
                   
                rNodeA = GetNeighborNode(home, rNodeB, ti2);
                if (!rNodeA) continue;
                       
                if (OrderNodes(rNodeA, rNodeB) != 1) continue;
        
                if (yoffeUseOldPosition) {
                    dx = xB - rNodeA->oldx;
                    dy = yB - rNodeA->oldy;
                    dz = zB - rNodeA->oldz;
                } else {
                    dx = xB - rNodeA->x;
                    dy = yB - rNodeA->y;
                    dz = zB - rNodeA->z;
                }
                   
/*
 *              (PBC disabled for this function)
 *
 *              ZImage(param, &dx, &dy, &dz);
 */
                   
                xA = xB - dx;
                yA = yB - dy;
                zA = zB - dz;
         
                burgX = rNodeB->burgX[ti2];
                burgY = rNodeB->burgY[ti2];
                burgZ = rNodeB->burgZ[ti2];
        
                node_numberA[0] = rNodeA->fem_Surface[0];
                node_numberA[1] = rNodeA->fem_Surface[1];
                surf_normA[0] = rNodeA->fem_Surface_Norm[0];
                surf_normA[1] = rNodeA->fem_Surface_Norm[1];
                surf_normA[2] = rNodeA->fem_Surface_Norm[2];

/*
 *              MT if both nodes lie at surfaces: 
 *
 *              if they are on the same surface, ignore it
 *              if they are on different surfaces, this can happen
 *              for a very short segment. 
 */ 
        
                r[0]=xm;
                r[1]=ym;
                r[2]=zm;

                b[0]=burgX;
                b[1]=burgY;
                b[2]=burgZ; 
        
                if ((node_numberA[0] > 0) && (node_numberB[0] > 0)) {
                    if (node_numberA[0] == node_numberB[0] &&
                        node_numberA[1] == node_numberB[1] ) {
                        continue;
                    } else {
                        continue; 
                    } 
                }
        
/*
 *              Initialize sigmaSH
 */
                for (mm = 0; mm < 6; mm++) {
                    sigmaSH[mm] = 0.0;
                }

                if (node_numberA[0] > 0) {

                    rs[0] = xA;
                    rs[1] = yA;
                    rs[2] = zA; 

                    rm[0] = xB;
                    rm[1] = yB;
                    rm[2] = zB;

                    isign = 1;

                    sh_image_stress_num_(r, rs, rm, surf_normA, &isign, b,
                                       &MU, &NU, sigmaSH);
/*
                    printf("r = %f %f %f\n", r[0], r[1], r[2]); 
                    printf("rs= %f %f %f\n", rs[0], rs[1], rs[2]);
                    printf("rm = %f %f %f\n", rm[0], rm[1], rm[2]); 
                    printf("surf_normA = %f %f %f\n",
                           surf_normA[0], surf_normA[1], surf_normA[2]);
                    printf("sigmaSH = %f %f %f %f %f %f\n",
                           sigmaSH[0], sigmaSH[1], sigmaSH[2],
                           sigmaSH[3], sigmaSH[4], sigmaSH[5] );
*/
                } else if (node_numberB[0] > 0) {

                    rs[0] = xB;
                    rs[1] = yB;
                    rs[2] = zB; 

                    rm[0] = xA;
                    rm[1] = yA;
                    rm[2] = zA;

                    isign = -1; 

                    sh_image_stress_num_(r, rs, rm, surf_normB, &isign, b,
                                       &MU, &NU, sigmaSH);
/*
                    printf("rs= %f %f %f\n", rs[0], rs[1], rs[2]);
                    printf("rm = %f %f %f\n", rm[0], rm[1], rm[2]); 
                    printf("surf_normA = %f %f %f\n",
                           surf_normA[0], surf_normA[1], surf_normA[2]);
                    printf("sigmaSH = %f %f %f %f %f %f\n",
                           sigmaSH[0], sigmaSH[1], sigmaSH[2],
                           sigmaSH[3], sigmaSH[4], sigmaSH[5]);
*/
                }
        
                for (mm = 0; mm < 6; mm++) {
                    totStressSH[mm] += sigmaSH[mm];
                }
                   
            }  /* end for (iNbr2...) */
        }  /* end for (i=0;...) */


        return;
}
