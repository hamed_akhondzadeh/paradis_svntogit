/***************************************************************************
 *
 *      Function:     AllSegmentStress
 *      Description:  Calculate stress field of all segments in the
 *                    simulation box
 *
 *      Notes:  This function assumes that periodic boundary conditions
 *              are NOT enabled.
 *
 ***************************************************************************/
#include "Home.h"
#include "Util.h"
#include <math.h>
#include "FEM.h"


/*
 *      Calculate stress at point (xm, ym, zm) due to all
 *      dislocation segments.   No PBC is assumed
 */
void AllSegmentStress(Home_t *home,  real8 xm, real8 ym, real8 zm,
                      real8 totStress[][3])
{
        int     i, mm, kk, nc2, ti2;
        real8   Lx, Ly, Lz;
        real8   xmin, ymin, zmin, xmax, ymax, zmax;
        real8   x1, y1, z1, x2, y2, z2;
        real8   dx, dy, dz, xA, yA, zA, xB, yB, zB, xC, yC, zC, dr;
        real8   MU, NU, burgX, burgY, burgZ, a;
        real8   surf_normA[3], surf_normB[3];
	real8   node_numberA[2], node_numberB[2]; 
        real8   localStress[3][3];
        real8   sigma[3][3], sigmaImg[3][3];
        Node_t  *rNodeA, *rNodeB;
        Param_t *param;

        param = home->param;

        Lx = param->Lx;
        Ly = param->Ly;
        Lz = param->Lz;
        
        MU = param->shearModulus;
        NU = param->pois;
        a  = param->rc;
           
/*
 *      Initialize stress to zero
 */
        for (mm = 0; mm < 3; mm++) {
            for (kk = 0; kk < 3; kk++) {
                localStress[mm][kk] = 0;
            }
        }
             
/*
 *      Loop through all segments owned by this domain
 */
        for (i = 0; i < home->newNodeKeyPtr; i++) {
            rNodeB = home->nodeKeys[i];
            if (!rNodeB) continue;
        
            xB = rNodeB->x;
            yB = rNodeB->y;
            zB = rNodeB->z;

	    node_numberB[0] = rNodeB->fem_Surface[0];
	    node_numberB[1] = rNodeB->fem_Surface[1];
        
            nc2 = rNodeB->numNbrs;
        
            for (ti2 = 0; ti2 < nc2; ti2++) {
                   
                rNodeA = GetNeighborNode(home, rNodeB, ti2);
                if (!rNodeA) continue;
                       
                if (OrderNodes(rNodeA, rNodeB) != 1) continue;
        
                dx = xB - rNodeA->x;
                dy = yB - rNodeA->y;
                dz = zB - rNodeA->z;
                   
/*
 *              (PBC disabled for this function)
 *
 *              ZImage (param, &dx, &dy, &dz);
 */
                   
                xA = xB - dx;
                yA = yB - dy;
                zA = zB - dz;
    
        
		node_numberA[0] = rNodeA->fem_Surface[0];
		node_numberA[1] = rNodeA->fem_Surface[1];
		
                burgX = rNodeB->burgX[ti2];
                burgY = rNodeB->burgY[ti2];
                burgZ = rNodeB->burgZ[ti2];
        
                if ((node_numberA[0] > 0) && (node_numberB[0] > 0)) {
                    if (node_numberA[0] == node_numberB[0]  &&
                        node_numberA[1] == node_numberB[1]) { 
                            continue;
                    } else {
                        Fatal ("a segment touches two different "
                               "surfaces at once");
                    } 
                }
        
                if (node_numberA[0] > 0) {

                    dr = sqrt(dx*dx + dy*dy + dz*dz);
                    dr = 1e5 / dr;

                    xC = xB - dx*dr;
                    yC = yB - dy*dr;
                    zC = zB - dz*dr;

                    SegmentStress(MU, NU, burgX, burgY, burgZ,
                                  xB, yB, zB, xC, yC, zC,
                                  xm, ym, zm, a, sigma);

/*
                    printf("xm,ym,zm = %f %f %f\n", xm, ym, zm); 
                    printf("xB,yB,zB = %f %f %f\n", xB, yB, zB);
                    printf("xC,yC,zC = %f %f %f\n", xC, yC, zC);
                    printf("sigma = %f %f %f %f %f %f\n",
                           sigma[0][0], sigma[0][1], sigma[0][2],
                           sigma[1][1], sigma[1][2], sigma[2][2]);
*/
                } else if (node_numberB[0] > 0) {

                    dr = sqrt(dx*dx + dy*dy + dz*dz);
                    dr = 1e5 / dr;

                    xC = xA + dx*dr;
                    yC = yA + dy*dr;
                    zC = zA + dz*dr;

                    SegmentStress(MU, NU, burgX, burgY, burgZ,
                                xC, yC, zC, xA, yA, zA,
                                xm, ym, zm, a, sigma);
                } else {

                    SegmentStress(MU, NU, burgX, burgY, burgZ,
                                xB, yB, zB, xA, yA, zA,
                                xm, ym, zm, a, sigma);
/*
                    printf("burgX,Y,Z = %f %f %f\n", burgX, burgY, burgZ); 
                    printf("xm,ym,zm = %f %f %f\n", xm, ym, zm); 
                    printf("xB,yB,zB = %f %f %f\n", xB, yB, zB);
                    printf("xA,yA,zA = %f %f %f\n", xA, yA, zA);
                    printf("sigma = %f %f %f %f %f %f\n",
                           sigma[0][0], sigma[0][1], sigma[0][2],
                           sigma[1][1], sigma[1][2], sigma[2][2]);
*/
                }
        
                for (mm = 0; mm < 3; mm++) {
                    for (kk = 0; kk < 3; kk++) {
                        localStress[mm][kk] += sigma[mm][kk];
                    }
                }
            }  /* end for (ti2 = 0; ...) */
        }  /* end for (i = 0;...) */
        
/*
 *      For serial runs, the local stress field is the complete
 *      stress field for the problem, but for parallel applications
 *      we have to sum the local stress fields from all processors
 *      into the total stress field.
 */
#ifdef PARALLEL
        MPI_Allreduce(localStress, totStress, 9, MPI_DOUBLE,
                      MPI_SUM, MPI_COMM_WORLD);
#else
        for (mm = 0; mm < 3; mm++) {
            for (kk = 0; kk < 3; kk++) {
                totStress[mm][kk] = localStress[mm][kk];
            }
        }
#endif
        return;        
}
