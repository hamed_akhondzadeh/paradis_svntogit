/******************************************************************************
 *
 *  Function    : AddNode
 *  Description : Link a new node between nodeA and nodeB.
 *
 *                Set force on broken arm segments to zero, so force
 *                will be recalculated next time through. New arm seg
 *                forces will already be initialized to zero   t.p.
 *
 *****************************************************************************/

#include "Home.h"

void AddNode (Home_t *home, Node_t *nodeA, Node_t *newNode, Node_t *nodeB)
{

   int idxAB, idxBA ;

/* Find the link indices for A->B and B->A  */

   if ((nodeA==0)||(nodeB==0))
       Fatal ("AddNode: segment has empty nodes");
   
   Connected (nodeA, nodeB, &idxAB) ;
   Connected (nodeB, nodeA, &idxBA) ;

   if (idxAB < 0 || idxBA < 0) {
	Fatal("AddNode: %s link missing A=(%d,%d) NEW=(%d,%d) B=(%d,%d)",
		(idxAB < 0) ? "A->B" : "B->A",
		nodeA->myTag.domainID, nodeA->myTag.index,
		newNode->myTag.domainID, newNode->myTag.index,
		nodeB->myTag.domainID, nodeB->myTag.index);
   }


   nodeA->nbrTag[idxAB].index    = newNode->myTag.index ;
   nodeA->nbrTag[idxAB].domainID = newNode->myTag.domainID ;
#if 0
   nodeA->segFx[idxAB] = nodeA->segFy[idxAB] = nodeA->segFz[idxAB] = 0.0 ;
#endif
   nodeA->sigbLoc[3*idxAB]  =0;
   nodeA->sigbLoc[3*idxAB+1]=0;
   nodeA->sigbLoc[3*idxAB+2]=0;
   nodeA->sigbRem[3*idxAB]  =0;
   nodeA->sigbRem[3*idxAB+1]=0;
   nodeA->sigbRem[3*idxAB+2]=0;
   
   nodeB->nbrTag[idxBA].index    = newNode->myTag.index ;
   nodeB->nbrTag[idxBA].domainID = newNode->myTag.domainID ;
#if 0
   nodeB->segFx[idxBA] = nodeB->segFy[idxBA] = nodeB->segFz[idxBA] = 0.0 ;
#endif
   nodeB->sigbLoc[3*idxBA]  =0;
   nodeB->sigbLoc[3*idxBA+1]=0;
   nodeB->sigbLoc[3*idxBA+2]=0;
   nodeB->sigbRem[3*idxBA]  =0;
   nodeB->sigbRem[3*idxBA+1]=0;
   nodeB->sigbRem[3*idxBA+2]=0;
   
   newNode->nbrTag[0].index      = nodeA->myTag.index ;
   newNode->nbrTag[0].domainID   = nodeA->myTag.domainID ;
   newNode->nbrTag[1].index      = nodeB->myTag.index ;
   newNode->nbrTag[1].domainID   = nodeB->myTag.domainID ;

#ifdef debug_topology
   printf("AddNode (%d,%d) - (%d,%d) - (%d,%d)\n",
          nodeA->myTag.domainID, nodeA->myTag.index,
          newNode->myTag.domainID, newNode->myTag.index,
          nodeB->myTag.domainID, nodeB->myTag.index );
   PrintNode(newNode);
#endif
   return ;
}
