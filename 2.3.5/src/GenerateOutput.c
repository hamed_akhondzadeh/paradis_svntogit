/*-------------------------------------------------------------------------
 *
 *      Module:      GenerateOutput.c
 *      Description: 
 *
 *      Includes public functions:
 *
 *          DoParallelIO()
 *          GenerateOutput()
 *          GetOutputTypes()
 *          GetParIOGroup()
 *
 *      Includes private functions:
 *          FreeBinFileArrays()
 *          SetBinFileArrays()
 *
 *------------------------------------------------------------------------*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "Home.h"
#include "Node.h"
#include "Comm.h"
#include "Util.h"
#include "WriteProp.h"
#include "DisplayC.h"
#include "Restart.h"

#ifdef PARALLEL
#include <mpi.h>
#endif



/*-------------------------------------------------------------------------
 *
 *      Function:    GetOutputTypes
 *      Description: Given the stage of program execution, determine
 *                   which types of output need to be generated and
 *                   increment any applicable file sequence counters for 
 *                   those output types. -- Note the counters are not to
 *                   be updated while dumping data during program termination.
 *      Args:
 *          stage        indicates the program execution stage which
 *                       determines the types of output this function
 *                       might produce.  Valid values for this field are:
 *
 *                               STAGE_INIT
 *                               STAGE_CYCLE
 *                               STAGE_TERM
 *
 *          outputTypes  pointer to an integer which on exit from this
 *                       subroutine will contain a bitmask with 1 bit
 *                       set for each type of output to be generated.
 *
 *------------------------------------------------------------------------*/
static void GetOutputTypes(Home_t *home, int stage, int *outputTypes)
{
        real8   timeNow, dumpTime;
        Param_t *param;

        static int xWinAlive = 1;

        param = home->param;
        timeNow = param->timeNow;

/*
 *      If X-window plotting is enabled (and active) and the code
 *      is not in the process of terminating, set the flag to do
 *      the plotting.
 */
#ifndef NO_XWINDOW
        if ((stage == STAGE_INIT) || (stage == STAGE_CYCLE)) {
            if (xWinAlive) {
                if (home->myDomain == 0) {
                    xWinAlive = WinAlive();
                }
#ifdef PARALLEL
                MPI_Bcast(&xWinAlive, 1, MPI_INT, 0, MPI_COMM_WORLD);
#endif
                if (xWinAlive) {
                    *outputTypes |= GEN_XWIN_DATA;
                }
            }
        }
#endif

/*
 *      If restart file dumps are enabled, do so if the code is
 *      in the termination stage, or it is at the end of a cycle
 *      and either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between restart dumps
 *      OR the current cycle is a multiple of the specified restart
 *      dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->savecn) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_RESTART_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->savecndt > 0.0) {
                    dumpTime = param->savecntime + param->savecndt;
                    if (timeNow >= dumpTime) {
                        param->savecntime = timeNow;
                        param->savecncounter++;
                        *outputTypes |= GEN_RESTART_DATA;
                    }
                } else if ((home->cycle % param->savecnfreq) == 0) {
                    param->savecncounter++;
                    *outputTypes |= GEN_RESTART_DATA;
                }
            }
        }

/*
 *      If GNUPLOT file dumps are enabled, do so if the code is
 *      in the termination stage, or at the end of a cycle and
 *      either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between file dumps
 *      OR the current cycle is a multiple of the specified dump
 *      frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->gnuplot) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_GNUPLOT_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->gnuplotdt > 0.0) {
                    dumpTime = param->gnuplottime + param->gnuplotdt;
                    if (timeNow >= dumpTime) {
                        param->gnuplottime = timeNow;
                        param->gnuplotcounter++;
                        *outputTypes |= GEN_GNUPLOT_DATA;
                    }
                } else if ((home->cycle % param->gnuplotfreq) == 0) {
                    param->gnuplotcounter++;
                    *outputTypes |= GEN_GNUPLOT_DATA;
                }
            }
        }

/*
 *      If TECPLOT file dumps are enabled, do so if the code is
 *      in the termination stage, or it is at the end of a cycle
 *      and either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between file dumps
 *      OR the current cycle is a multiple of the specified 
 *      dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->tecplot) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_TECPLOT_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->tecplotdt > 0.0) {
                    dumpTime = param->tecplottime + param->tecplotdt;
                    if (timeNow >= dumpTime) {
                        param->tecplottime = timeNow;
                        param->tecplotcounter++;
                        *outputTypes |= GEN_TECPLOT_DATA;
                    }
                } else if ((home->cycle % param->tecplotfreq) == 0) {
                    param->tecplotcounter++;
                    *outputTypes |= GEN_TECPLOT_DATA;
                }
            }
        }

/*
 *      If velocity file dumps are enabled, do so if the code is
 *      in the termination stage, or it is at the end of a cycle
 *      and either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between file dumps
 *      OR the current cycle is a multiple of the specified 
 *      dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->velfile) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_VELOCITY_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->velfiledt > 0.0) {
                    dumpTime = param->velfiletime + param->velfiledt;
                    if (timeNow >= dumpTime) {
                        param->velfiletime = timeNow;
                        param->velfilecounter++;
                        *outputTypes |= GEN_VELOCITY_DATA;
                    }
                } else if ((home->cycle % param->velfilefreq) == 0) {
                    param->velfilecounter++;
                    *outputTypes |= GEN_VELOCITY_DATA;
                }
            }
        }

/*
 *      If arm file dumps are enabled, do so if the code is
 *      in the termination stage, or it is at the end of a cycle
 *      and either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between file dumps
 *      OR the current cycle is a multiple of the specified 
 *      dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->armfile) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_ARM_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->armfiledt > 0.0) {
                    dumpTime = param->armfiletime + param->armfiledt;
                    if (timeNow >= dumpTime) {
                        param->armfiletime = timeNow;
                        param->armfilecounter++;
                        *outputTypes |= GEN_ARM_DATA;
                    }
                } else if ((home->cycle % param->armfilefreq) == 0) {
                    param->armfilecounter++;
                    *outputTypes |= GEN_ARM_DATA;
                }
            }
        }

/*
 *      If POLEFIG file dumps are enabled, do so if the code is
 *      in the termination stage, or it is at the end of a cycle
 *      and either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between file dumps
 *      OR the current cycle is a multiple of the specified 
 *      dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->polefigfile) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_POLEFIG_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->polefigdt > 0.0) {
                    dumpTime = param->polefigtime + param->polefigdt;
                    if (timeNow >= dumpTime) {
                        param->polefigtime = timeNow;
                        param->polefigcounter++;
                        *outputTypes |= GEN_POLEFIG_DATA;
                    }
                } else if ((home->cycle % param->polefigfreq) == 0) {
                    param->polefigcounter++;
                    *outputTypes |= GEN_POLEFIG_DATA;
                }
            }
        }
    
/*
 *      If POVRAY file dumps are enabled, do so if the code is
 *      in the termination stage, or it is at the end of a cycle
 *      and either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between file dumps
 *      OR the current cycle is a multiple of the specified 
 *      dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->povray) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_POVRAY_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->povraydt > 0.0) {
                    dumpTime = param->povraytime + param->povraydt;
                    if (timeNow >= dumpTime) {
                        param->povraytime = timeNow;
                        param->povraycounter++;
                        *outputTypes |= GEN_POVRAY_DATA;
                    }
                } else if ((home->cycle % param->povrayfreq) == 0) {
                    param->povraycounter++;
                    *outputTypes |= GEN_POVRAY_DATA;
                }
            }
        }

/*
 *      If ATOMEYE file dumps are enabled, do so if the code is
 *      in the termination stage, or it is at the end of a cycle
 *      and either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between file dumps
 *      OR the current cycle is a multiple of the specified 
 *      dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->atomeye) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_ATOMEYE_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->atomeyedt > 0.0) {
                    dumpTime = param->atomeyetime + param->atomeyedt;
                    if (timeNow >= dumpTime) {
                        param->atomeyetime = timeNow;
                        param->atomeyecounter++;
                        *outputTypes |= GEN_ATOMEYE_DATA;
                    }
                } else if ((home->cycle % param->atomeyefreq) == 0) {
                    param->atomeyecounter++;
                    *outputTypes |= GEN_ATOMEYE_DATA;
                }
            }
        }

/*
 *      If TeraScale Browser file dumps are enabled, do so if
 *      the code is in the termination stage, or it is at the
 *      end of a cycle and either the elapsed simulation time
 *      since the last dump has exceeded the allowable delta time
 *      between file dumps OR the current cycle is a multiple of
 *      the specified dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->tsbfile && (param->writetsbspec[0] > 0)) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_TSB_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->tsbfiledt > 0.0) {
                    dumpTime = param->tsbfiletime + param->tsbfiledt;
                    if (timeNow >= dumpTime) {
                        param->tsbfiletime = timeNow;
                        param->tsbfilecounter++;
                        *outputTypes |= GEN_TSB_DATA;
                    }
                } else if ((home->cycle % param->tsbfilefreq) == 0) {
                    param->tsbfilecounter++;
                    *outputTypes |= GEN_TSB_DATA;
                }
            }
        }

/*
 *      If chain fragment file dumps are enabled, do so if the code is
 *      in the termination stage, or it is at the end of a cycle
 *      and either the elapsed simulation time since the last dump
 *      has exceeded the allowable delta time between file dumps
 *      OR the current cycle is a multiple of the specified 
 *      dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->fragfile) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_FRAG_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->fragdt > 0.0) {
                    dumpTime = param->fragtime + param->fragdt;
                    if (timeNow >= dumpTime) {
                        param->fragtime = timeNow;
                        param->fragcounter++;
                        *outputTypes |= GEN_FRAG_DATA;
                    }
                } else if ((home->cycle % param->fragfreq) == 0) {
                    param->fragcounter++;
                    *outputTypes |= GEN_FRAG_DATA;
                }
            }
        }

/*
 *      Check if creation of the density field file is enabled.  Only
 *      done at code termination time...
 */
        if ((stage == STAGE_TERM) &&
            ((param->savedensityspec[0] > 0) &&
             (param->savedensityspec[1] > 0) &&
             (param->savedensityspec[2] > 0))) {
            *outputTypes |= GEN_DENSITY_DATA;
        }

/*
 *      If timer file dumps are enabled, do so if the code
 *      is in the termination stage, or it is at the end
 *      of a cycle and either the elapsed simulation time
 *      since the last dump has exceeded the allowable delta time
 *      between file dumps OR the current cycle is a multiple of
 *      the specified dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->savetimers) {
            if (stage == STAGE_TERM) {
                *outputTypes |= GEN_TIMER_DATA;
            } else if (stage == STAGE_CYCLE) {
                if (param->savetimersdt > 0.0) {
                    dumpTime = param->savetimerstime + param->savetimersdt;
                    if (timeNow >= dumpTime) {
                        param->savetimerstime = timeNow;
                        param->savetimerscounter++;
                        *outputTypes |= GEN_TIMER_DATA;
                    }
                } else if ((home->cycle % param->savetimersfreq) == 0) {
                    param->savetimerscounter++;
                    *outputTypes |= GEN_TIMER_DATA;
                }
            }
        }

/*
 *      If FLUX file dumps are enabled, do so if the code is
 *      at the end of a cycle and either the elapsed simulation
 *      time since the last dump has exceeded the allowable delta
 *      time between file dumps OR the current cycle is a multiple
 *      of the specified dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->fluxfile && (stage == STAGE_CYCLE)) {
            if (param->fluxdt > 0.0) {
                dumpTime = param->fluxtime + param->fluxdt;
                if (timeNow >= dumpTime) {
                    param->fluxtime = timeNow;
                    param->fluxcounter++;
                    *outputTypes |= GEN_FLUX_DATA;
                }
            } else if ((home->cycle % param->fluxfreq) == 0) {
                param->fluxcounter++;
                *outputTypes |= GEN_FLUX_DATA;
            }
        }

/*
 *      If properties file dumps are enabled, do so if the code is
 *      at the end of a cycle and either the elapsed simulation
 *      time since the last dump has exceeded the allowable delta
 *      time between file dumps OR the current cycle is a multiple
 *      of the specified dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if (param->saveprop && (stage == STAGE_CYCLE)) {
            if (param->savepropdt > 0.0) {
                dumpTime = param->saveproptime + param->savepropdt;
                if (timeNow >= dumpTime) {
                    param->saveproptime = timeNow;
                    *outputTypes |= GEN_PROPERTIES_DATA;
                }
            } else if ((home->cycle % param->savepropfreq) == 0) {
                *outputTypes |= GEN_PROPERTIES_DATA;
            }
        }

/*
 *      If POSTSCRIPT file dumps are enabled, do so if the code is
 *      at the end of a cycle and either the elapsed simulation
 *      time since the last dump has exceeded the allowable delta
 *      time between file dumps OR the current cycle is a multiple
 *      of the specified dump frequency.
 *
 *      NOTE: The last two conditions are mutually exclusive. If
 *            a delta time has been specified to determine the 
 *            time to dump files, no check of the current
 *            cycle against the dump frequency will be done.
 */
        if ((param->psfile) && (stage == STAGE_CYCLE)) {
            if (param->psfiledt > 0.0) {
                dumpTime = param->psfiletime + param->psfiledt;
                if (timeNow > dumpTime) {
                    *outputTypes |= GEN_POSTSCRIPT_DATA;
                }
            } else {
                if ((home->cycle % param->psfilefreq) == 0) {
                    *outputTypes |= GEN_POSTSCRIPT_DATA;
                }
            }
        }

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    GetParIOGroup
 *      Description: Determine the parallel I/O group of which the current
 *                   task is a member, as well as the previous and
 *                   next tasks in the same group.
 *      Args:
 *          thisGroup   pointer to location in which to return to the
 *                      caller the I/O group of which the current
 *                      task is a member.
 *          prevInGroup pointer to location in which to return the id
 *                      of the task immediately preceeding the current
 *                      task in its I/O group.  If the current task is
 *                      the first in the group, this value will contain
 *                      the current task id.
 *          nextInGroup pointer to location in which to return the id
 *                      of the task immediately following the current
 *                      task in its I/O group.  If the current task is
 *                      the last in the group, this value will contain
 *                      the current task id.
 *          firstInGroup location in which to return to the caller the
 *                      ID of the first domain in <thisGroup>
 *          lastInGroup location in which to return to the caller the
 *                      ID of the last domain in <thisGroup>
 *
 *      Last Modified: 09/10/2008 - gh  Modified to return first/last in
 *                                  in group values, plus fixed bug that
 *                                  returned bad nextInGroup value.
 *
 *------------------------------------------------------------------------*/
static void GetParIOGroup(Home_t *home, int *thisGroup, int *prevInGroup,
                          int *nextInGroup, int *firstInGroup,
                          int *lastInGroup)
{
        int     thisDomain, numDomains, numIOGroups;
        int     smallGrpSize, largeGrpSize, numSmallGrps, numLargeGrps;
        Param_t *param;

        thisDomain   = home->myDomain;
        numDomains   = home->numDomains;
        param        = home->param;
        numIOGroups  = param->numIOGroups;

        smallGrpSize = numDomains / numIOGroups;
        numLargeGrps = numDomains % numIOGroups;
        largeGrpSize = smallGrpSize + (numLargeGrps > 0);
        numSmallGrps = numIOGroups - numLargeGrps;

        if ((numLargeGrps * largeGrpSize) > thisDomain) {
            *thisGroup = thisDomain / largeGrpSize;
            *firstInGroup = *thisGroup * largeGrpSize;
            *lastInGroup = *firstInGroup + largeGrpSize - 1;
        } else {
            *thisGroup = numLargeGrps +
                         ((thisDomain - (numLargeGrps * largeGrpSize)) /
                          smallGrpSize);
            *firstInGroup = numLargeGrps * largeGrpSize +
                            (*thisGroup - numLargeGrps) * smallGrpSize;
            *lastInGroup = *firstInGroup + smallGrpSize -1;
        }

        *prevInGroup = MAX(thisDomain-1, *firstInGroup);
        *nextInGroup = MIN(thisDomain+1, *lastInGroup);

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    GetCounts
 *
 *      Description: Obtain both local and global node/segment counts.
 *
 *      Args:
 *          numNodes      location in which to return to caller the total
 *                        number of nodes in the simulation.
 *          numSegs       location in which to return to caller the total
 *                        number of unique segments in the simulation.
 *          numArms       location in which to return the sum arm count
 *                        of all nodes in the simulation
 *          numLocalNodes location in which to return the number of nodes
 *                        in the local domain.
 *          numLocalSeg   location in which to return the number of unique
 *                        segments in the local domain.
 *
 *      Last Modified: 09/10/2008 - gh  Modified to add the <numLocalNodes>
 *                                  and <numLocalSegs> parameters.
 *
 *------------------------------------------------------------------------*/
static void GetCounts(Home_t *home, int *numNodes, int *numSegs, int *numArms,
                      int *numLocalNodes, int *numLocalSegs)
{
        int    i, j;
        int    locVals[3], globalVals[3];
        Node_t *node;

        locVals[0] = 0;
        locVals[1] = 0;
        locVals[2] = 0;

        globalVals[0] = 0;
        globalVals[1] = 0;
        globalVals[2] = 0;

        for (i = 0; i < home->newNodeKeyPtr; i++) {

            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }

            locVals[0] += 1;
            locVals[2] += node->numNbrs;

            for (j = 0; j < node->numNbrs; j++) {
                if ((home->myDomain == node->nbrTag[j].domainID) &&
                    (node->nbrTag[j].index < i)) {
                    continue;
                }
                locVals[1] += 1;
            }
        }

#ifdef PARALLEL
        MPI_Reduce(locVals, globalVals, 3, MPI_INT, MPI_SUM, 0,
                   MPI_COMM_WORLD);

        *numNodes = globalVals[0];
        *numSegs  = globalVals[1];
        *numArms  = globalVals[2];
#else
        *numNodes = locVals[0];
        *numSegs  = locVals[1];
        *numArms  = locVals[2];
#endif

        *numLocalNodes = locVals[0];
        *numLocalSegs  = locVals[1];

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    SetBinFileArrays
 *
 *      Description: When writing HDF5 files, the nodal data is aggregated
 *                   into arrays of like elements.  This function allocates
 *                   the needed arrays, pulls node and segment data from the
 *                   Node_t structures, and fills in the new arrays so they
 *                   are ready to write.
 *
 *      Args:
 *          binData  pointer to structure with arrays for binary writes.
 *                   This structure will be populated for the caller.
 *
 *      Last Modified: 09/10/2008 - original version
 *
 *------------------------------------------------------------------------*/
static void SetBinFileArrays(Home_t *home, BinFileData_t *binData)
{
        int    i, j, size;
        int    nOffset, sOffset;
        Node_t *node;


        if (binData->nodeCount == 0) {
            return;
        }

/*
 *      Allocate 1 element per node for these arrays
 */
        size = binData->nodeCount * sizeof(int);
        binData->nodeIndex = (int *)malloc(size);
        binData->nodeConstraint = (int *)malloc(size);
        binData->nodeNumSegs = (int *)malloc(size);

/*
 *      Need 3 elements per node for position data
 */
        size = binData->nodeCount * sizeof(double) * 3;
        binData->nodePos = (real8 *)malloc(size);

/*
 *      Need 3 ints to define endpoints for each segment.  First
 *      int is index of first node (always the local domain, so
 *      no need to store domain id), last two ints for tag of
 *      2nd endpoint which may or may not be in the local domain.
 */
        size = binData->segCount * sizeof(int) * 3;
        binData->segTags = (int *)malloc(size);

/*
 *      Need 3 elements per segment for these arrays
 */
        size = binData->segCount * sizeof(real8) * 3;
        binData->burgersVec = (real8 *)malloc(size);
        binData->glidePlane = (real8 *)malloc(size);

/*
 *      Go through each node and store it's data in the appropriate
 *      arrays.
 */
        nOffset = 0;
        sOffset = 0;

        for (i = 0; i < home->newNodeKeyPtr; i++) {

            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }

            binData->nodeIndex[nOffset] = node->myTag.index;
            binData->nodeConstraint[nOffset] = node->constraint;
            binData->nodeNumSegs[nOffset] = node->numNbrs;
            binData->nodePos[nOffset*3  ] = node->x;
            binData->nodePos[nOffset*3+1] = node->y;
            binData->nodePos[nOffset*3+2] = node->z;

            nOffset++;
/*
 *          Add any segment with a remote neighbor to the segment
 *          list.  Completely local segments only get added if the
 *          current node 'owns' the segment.
 */
            for (j = 0; j < node->numNbrs; j++) {

                if ((home->myDomain == node->nbrTag[j].domainID) &&
                    (node->nbrTag[j].index < i)) {
                    continue;
                }

                binData->segTags[sOffset*3  ] = node->myTag.index;
                binData->segTags[sOffset*3+1] = node->nbrTag[j].domainID;
                binData->segTags[sOffset*3+2] = node->nbrTag[j].index;

                binData->burgersVec[sOffset*3  ] = node->burgX[j];
                binData->burgersVec[sOffset*3+1] = node->burgY[j];
                binData->burgersVec[sOffset*3+2] = node->burgZ[j];

                binData->glidePlane[sOffset*3  ] = node->nx[j];
                binData->glidePlane[sOffset*3+1] = node->ny[j];
                binData->glidePlane[sOffset*3+2] = node->nz[j];

                sOffset++;
            }
        }

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    FreeBinFileArrays
 *
 *      Description: This function frees the arrays that had been written
 *                   to the HDF5 file.
 *
 *      Args:
 *          binData  pointer to structure with array pointers.  On
 *                   completion, the arrays will have been freed and the
 *                   array pointers in <binData> set to NULL.
 *
 *      Last Modified: 09/10/2008 - original version
 *
 *------------------------------------------------------------------------*/
static void FreeBinFileArrays(BinFileData_t *binData)
{
        if (binData->nodeIndex != (int *)NULL) {
            free(binData->nodeIndex);
        }

        if (binData->nodeConstraint != (int *)NULL) {
            free(binData->nodeConstraint);
        }

        if (binData->nodeNumSegs != (int *)NULL) {
            free(binData->nodeNumSegs);
        }

        if (binData->segTags != (int *)NULL) {
            free(binData->segTags);
        }

        if (binData->nodePos != (real8 *)NULL) {
            free(binData->nodePos);
        }

        if (binData->burgersVec != (real8 *)NULL) {
            free(binData->burgersVec);
        }

        if (binData->glidePlane != (real8 *)NULL) {
            free(binData->glidePlane);
        }

        binData->nodeIndex = (int *)NULL;
        binData->nodeConstraint = (int *)NULL;
        binData->nodeNumSegs = (int *)NULL;
        binData->segTags = (int *)NULL;
        binData->nodePos = (real8 *)NULL;
        binData->burgersVec = (real8 *)NULL;
        binData->glidePlane = (real8 *)NULL;

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    DoParallelIO
 *      Description: 
 *
 *      Args:
 *          outputTypes  integer bitfield.  All non-zero bits correspond
 *                       to specific types of output that are to be
 *                       generated at this stage of program execution.
 *                       See Util.h for the mapping of output types
 *                       to bit positions.
 *          stage        indicates the program execution stage which
 *                       determines the types of output this function
 *                       might produce.
 *
 *------------------------------------------------------------------------*/
static void DoParallelIO(Home_t *home, int outputTypes, int stage)
{
        int     ioGroup, prevInGroup, nextInGroup, numIOGroups;
        int     thisDomain, isFirstInGroup;
        int     writePrologue, writeEpilogue;
        int     writeToken = 0, numSegs = 0, numArms = 0, totFragmentCount = 0;
        char    baseName[128];
        void    *fragmentList;
        Param_t *param;
        BinFileData_t binData;
#ifdef PARALLEL
        MPI_Status reqStatus;
#endif

        thisDomain  = home->myDomain;
        param       = home->param;

        numIOGroups = param->numIOGroups;

        memset(&binData, 0, sizeof(BinFileData_t));

/*
 *      Determine the parallel I/O group of which this task is
 *      a part, as well as its predecessor and successor in the
 *      group.
 */
        GetParIOGroup(home, &ioGroup, &prevInGroup, &nextInGroup,
                      &binData.firstInGroup, &binData.lastInGroup);

        isFirstInGroup = (thisDomain == binData.firstInGroup);

/*
 *      Domain zero (first member of first I/O group) will always
 *      do output file creation and initialization, and the last
 *      member of the last I/O group always adds any necessary
 *      trailers/whatever to the file
 */
        writePrologue = (thisDomain == 0);

        writeEpilogue = ((ioGroup == (numIOGroups-1)) &&
                         (thisDomain == nextInGroup));

/*
 *      Certain output routines require total counts of nodes,
 *      segments, etc.  If we're doing any of these types of output
 *      then do a global operation to get these values before
 *      beginning the I/O.
 */
        if ((outputTypes & GEN_RESTART_DATA) ||
            (outputTypes & GEN_TSB_DATA)     ||
            (outputTypes & GEN_TECPLOT_DATA)) {
            GetCounts(home, &param->nodeCount, &numSegs, &numArms,
                      &binData.nodeCount, &binData.segCount);
        }

/*
 *      If we're creating chain fragment files to be fed into
 *      VisIt, have each process create its own fragment list,
 *      and get the system-wide fragment count before starting
 *      any I/O.
 */
        if ((outputTypes & GEN_FRAG_DATA) != 0) {
            fragmentList = CreateFragmentList(home, &totFragmentCount);
        }

/*
 *      If we're about to write a binary restart file, set up the
 *      arrays that will be dumped directly to the binary file.
 *      Do this now so that when it is the task's turn to write, the
 *      data is prepped and ready to be written.
 */
        if (((outputTypes & GEN_RESTART_DATA) != 0) &&
             (param->writeBinRestart != 0)) {
            SetBinFileArrays(home, &binData);
        }

/*
 *      If this process is not the first in its I/O group, wait
 *      for the write token from the predecessor before continueing
 */
#ifdef PARALLEL
        if (thisDomain != prevInGroup) {
            MPI_Recv(&writeToken, 1, MPI_INT, prevInGroup, MSG_TOKEN_RING,
                     MPI_COMM_WORLD, &reqStatus);
        }
#endif


/*
 *      Call all I/O functions...
 */
        if ((outputTypes & GEN_RESTART_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "rs%04d",
                         param->savecncounter);
            } else {
                snprintf(baseName, sizeof(baseName), "restart.cn");
            }
            if (param->writeBinRestart) {
                WriteBinaryRestart(home, baseName, ioGroup, isFirstInGroup,
                                   writePrologue, writeEpilogue, &binData);
                FreeBinFileArrays(&binData);
            } else {
                WriteRestart(home, baseName, ioGroup, isFirstInGroup,
                             writePrologue, writeEpilogue);
            }
        }


        if ((outputTypes & GEN_GNUPLOT_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "0t%04d",
                         param->gnuplotcounter);
            } else {
                snprintf(baseName, sizeof(baseName), "gnuplot.final");
            }
            Gnuplot(home, baseName, ioGroup, isFirstInGroup,
                    writePrologue, writeEpilogue);
        }


        if ((outputTypes & GEN_TECPLOT_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "tecdata%04d",
                         param->tecplotcounter);
            } else {
                snprintf(baseName, sizeof(baseName), "tecdata.final");
            }
            Tecplot(home, baseName, ioGroup, isFirstInGroup,
                    writePrologue, writeEpilogue, numSegs);
        }


        if ((outputTypes & GEN_VELOCITY_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "vel%04d",
                         param->velfilecounter);
            } else {
                snprintf(baseName, sizeof(baseName), "vel.final");
            }
            WriteVelocity(home, baseName, ioGroup, isFirstInGroup,
                          writePrologue, writeEpilogue);
        }


        if ((outputTypes & GEN_ARM_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "arm%04d",
                         param->armfilecounter);
            } else {
                snprintf(baseName, sizeof(baseName), "arm.final");
            }
            WriteArms(home, baseName, ioGroup, isFirstInGroup,
                      writePrologue, writeEpilogue);
        }


        if ((outputTypes & GEN_POLEFIG_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "polefig%04d",
                         param->polefigcounter);
            } else {
                snprintf(baseName, sizeof(baseName), "polefig.final");
            }
            WritePoleFig(home, baseName, ioGroup, isFirstInGroup,
                         writePrologue, writeEpilogue);
        }


        if ((outputTypes & GEN_POVRAY_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "povframe%04d",
                         param->povraycounter);
            } else {
                snprintf(baseName, sizeof(baseName), "povframe.final");
            }
            WritePovray(home, baseName, ioGroup, isFirstInGroup,
                        writePrologue, writeEpilogue);
        }


        if ((outputTypes & GEN_ATOMEYE_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "paradis%04d",
                         param->atomeyecounter);
            } else {
                snprintf(baseName, sizeof(baseName), "paradis.final");
            }
            WriteAtomEye(home, baseName, ioGroup, isFirstInGroup,
                        writePrologue, writeEpilogue);
        }


        if ((outputTypes & GEN_TSB_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "tsbdata%04d",
                         param->tsbfilecounter);
            } else {
                snprintf(baseName, sizeof(baseName), "tsbdata.final");
            }
            WriteTSB(home, baseName, ioGroup, isFirstInGroup,
                     writePrologue, writeEpilogue, numArms);
        }


        if ((outputTypes & GEN_FRAG_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "fragments%04d",
                         param->fragcounter);
            } else {
                snprintf(baseName, sizeof(baseName), "fragments.final");
            }
            WriteFragments(home, baseName, ioGroup, isFirstInGroup,
                           writePrologue, writeEpilogue, &fragmentList,
                           totFragmentCount);
        }

/*
 *      If this process is not the last in its I/O group, send
 *      the write token to the next process in the I/O group.
 */
#ifdef PARALLEL
        if (thisDomain != nextInGroup) {
            MPI_Send(&writeToken, 1, MPI_INT, nextInGroup, MSG_TOKEN_RING,
                     MPI_COMM_WORLD);
        }
#endif

/*
 *      If we wrote the text restart file we still need to store the
 *      name of the recently written restart file to disk.  This
 *      involves an explicit syncronization point in the code since
 *      we don't want to do this until all processes have completed
 *      writing their restart data.
 */
        if ((outputTypes & GEN_RESTART_DATA) != 0) {
            if (stage == STAGE_CYCLE) {
                snprintf(baseName, sizeof(baseName), "rs%04d",
                         param->savecncounter);
            } else {
                snprintf(baseName, sizeof(baseName), "restart.cn");
            }
#ifdef PARALLEL
            MPI_Barrier(MPI_COMM_WORLD);
#endif
            if (thisDomain == 0) {
                SetLatestRestart(baseName);
            }
        }

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    GenerateOutput
 *      Description: This subroutine controls generation of all types
 *                   of output appropriate to the current stage of program
 *                   execution.
 *      Args:
 *          stage    indicates the program execution stage which determines
 *                   the types of output this function might produce.  Valid
 *                   values for this field are:
 *
 *                           STAGE_INIT
 *                           STAGE_CYCLE
 *                           STAGE_TERM
 *
 *------------------------------------------------------------------------*/
void GenerateOutput(Home_t *home, int stage)
{
        int     outputTypes = 0, decTimerSeqNum = 0;
        real8   localDensity, globalDensity;
        char    baseFile[128], fileName[128];
        time_t  tp;
        Param_t *param;
        FILE    *fp;


        TimerStart(home, GENERATE_IO);

        param = home->param;

/*
 *      If we're at the end of a cycle, update the cycle number and
 *      print a status message to stdout.
 */
        if (stage == STAGE_CYCLE) {

            param->timeNow += param->realdt;
            home->cycle++;

            if ((param->loadType != 2) && (home->myDomain == 0)) {
                time(&tp);
                printf("cycle=%-8d  realdt=%e  timeNow=%e  %s",
                        home->cycle, param->timeNow-param->timeStart,
                        param->timeNow, asctime(localtime(&tp)));
            }
        }

/*
 *      Determine what types of output need to be generated at
 *      this stage.
 */
        GetOutputTypes(home, stage, &outputTypes);

/*
 *      If we are writing either properties data or a restart file,
 *      accumulate the total dislocation density for the entire system
 *      to be written further on.
 */
#ifdef PARALLEL
        localDensity = param->disloDensity;
        MPI_Allreduce(&localDensity, &globalDensity, 1, MPI_DOUBLE,
                      MPI_SUM, MPI_COMM_WORLD);
        param->disloDensity = globalDensity;
#endif

#ifndef NO_XWINDOW
/*
 *      The X-Window plotting is a different beast since all
 *      the data must get filtered through processor zero...
 *      so for now we handle it the old way.
 */
        if ((outputTypes & GEN_XWIN_DATA) != 0) {
            CommSendMirrorNodes(home, stage);
        }

/*
 *      And now the postscript files which are also created from
 *      the X-Window info on processor zero.
 */
        if ((outputTypes & GEN_POSTSCRIPT_DATA) != 0) {
            if (home->myDomain == 0) {
                printf(" +++ Writing PostScript file\n");
            }
            sleep(2);  /* to insure all segments are drawn */
            WinWritePS();
        }
#endif

/*
 *      Dump slip amount and strain decomposition?
 */
        if ((outputTypes & GEN_FLUX_DATA) != 0) {
            sprintf(fileName,  "densflux%04d",home->param->fluxcounter);
            WriteDensFlux(fileName,home);
        }

        if (outputTypes & GEN_DENSITY_DATA) {
            WriteDensityField(home, "densityfield.out");
        }
/*
 *      Preserve various properites i.e. density, etc.
 *               (property vs. time files)
 */
        if ((outputTypes & GEN_PROPERTIES_DATA) != 0) {

            WriteProp(home, DENSITY);  /* Actually, length of disloctions   */
                                       /* both total and broken down by     */
                                       /* groups of burgers vector types    */

            WriteProp(home, DENSITY_DELTA);  /* Per-burgers vector density */
                                             /* gain/loss                  */

            WriteProp(home, EPSDOT);   /* Resultant strain rate (scalar)    */

            WriteProp(home, ALL_EPS);  /* Plastic strain tensor             */

            WriteProp(home, EPS);      /* Resultant plastic strain (scalar) */

        }

/*
 *      A number of the types of output will be done (potentially)
 *      in parallel.  Handle those in a separate routine but only
 *      as long as the user-provided flag <skipIO> is not set.
 */
        if (((outputTypes & GEN_RESTART_DATA)  != 0) ||
            ((outputTypes & GEN_GNUPLOT_DATA)  != 0) ||
            ((outputTypes & GEN_VELOCITY_DATA) != 0) ||
            ((outputTypes & GEN_ARM_DATA)      != 0) ||
            ((outputTypes & GEN_POLEFIG_DATA)  != 0) ||
            ((outputTypes & GEN_POVRAY_DATA)   != 0) ||
            ((outputTypes & GEN_ATOMEYE_DATA)  != 0) ||
            ((outputTypes & GEN_FRAG_DATA)     != 0) ||
            ((outputTypes & GEN_TSB_DATA)      != 0) ||
            ((outputTypes & GEN_TECPLOT_DATA)  != 0)) {
            if (!param->skipIO) {
                DoParallelIO(home, outputTypes, stage);
            }
        }

#ifdef debug_load_balance
        WriteForceTimes(home);
#endif
        TimerStop(home, GENERATE_IO);

/*
 *      Measure dead time waiting for all processes to complete I/O
 */
#ifdef PARALLEL
#ifdef SYNC_TIMERS
        TimerStart(home, IO_BARRIER);
        MPI_Barrier(MPI_COMM_WORLD);
        TimerStop(home, IO_BARRIER);
#endif
#endif

/*
 *      If necessary dump latest timing data into a file.  If we're at
 *      process termination, don't restart the timers.
 */
        if ((outputTypes & GEN_TIMER_DATA) != 0) {
            TimerStop(home, TOTAL_TIME);
            TimeAtRestart(home, stage);
            if (stage == STAGE_CYCLE) {
                TimerStart(home, TOTAL_TIME);
            }
        }

        return;
}
