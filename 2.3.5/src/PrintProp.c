/****************************************************************************
 *
 *  Function    : PrintProp
 *  Description : Depending on the input parameter, write a particular
 *                time-dependent property out to that property's diagnostic
 *                file, along with a timestamp.
 *
 ****************************************************************************/

#include "Home.h"
#include "WriteProp.h"
#include "Util.h"

void PrintProp (Home_t *home)
{
   Param_t *param ;

   int i;
   real8 al,am,an,amag,sigijk,pstnijk,dpstnijk;

   param = home->param ;

   al=param->edotdir[0];
   am=param->edotdir[1];
   an=param->edotdir[2];

   amag=sqrt(al*al+am*am+an*an);

   al /= amag; am /= amag; an /= amag;

   if (home->myDomain == 0) {
      sigijk=  param->appliedStress[0]*al*al
             + param->appliedStress[1]*am*am
             + param->appliedStress[2]*an*an
             + 2.*param->appliedStress[3]*am*an
             + 2.*param->appliedStress[4]*an*al
             + 2.*param->appliedStress[5]*al*am;

      pstnijk=  param->totpStn[0]*al*al
             + param->totpStn[1]*am*am
             + param->totpStn[2]*an*an
             + 2.*param->totpStn[3]*am*an
             + 2.*param->totpStn[4]*an*al
             + 2.*param->totpStn[5]*al*am;

      printf("timeNow = %e, disloDensity = %e, pstnijk = %e, sigijk = %e (BGL)\n", 
                       param->timeNow, param->disloDensity, pstnijk, sigijk);
   }

   return ;
}
