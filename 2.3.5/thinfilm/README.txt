DDTF code structure
Sylvie Aubry (11/14/08)


The thin film code is a part of ParaDiSv2.2.

TF_Main.c

TF_Main.c is a copy of Main.c.
On top of regular ParaDiS routines, it includes calls to specific thinfilm 
routines such as

- TF_Init

This routine initializes the thinfilm pointer and defines thinfilm specific variables such as Fourier modes (n_x and n_y), virtual segments etc.
It also declare dynamically all arrays needed for the thinfilm spectral methods such as A,B,C,E,F,G, Tx,Ty etc. This routine also defined the cartesian grid, (k_x, k_y), A,B,C,E,F,G matrices. Note that if the spectral method is not turned on (_TFIMGSTRESS not defined in the makefile), then these arrays are not allocated and are not defined. They are also not deleted in TF_Finish.

- TF_Step

This routine is called only if the spectral method is activated (_TFIMGSTRESS not defined in the makefile). It computes tractions at the thinfilm surfaces and the A,B,B,E,F,G matrices.


-TF_Remesh

Remesh rules specific to the thinfilm surfaces. This is always called.

- TF_Finish

Delete arrays that are specific to the thinfilm method and the spectral method in particular. These arrays are not deleted if the spectral method is not activated because they were not created.

Other routines
Other ParaDiS routines have been modified to treat the thinfilm case.

- NodeForce.c which is called in ParadisStep.c

Forces and 
\sigma \cdot b 
are computed using thinfilms routines and not regular ParaDiS ones.

- Mobility_BCC0.c and Mobility_FCC0.c 

have been modified to have v_z = 0 when a node is flagged 6 that is when it is on the surface.


That's it.

