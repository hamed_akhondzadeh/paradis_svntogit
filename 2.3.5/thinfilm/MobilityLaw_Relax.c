/**************************************************************************
 *
 *      Module:       MobilityLaw_Relax
 *      Description:  Contains functions for calculating mobility of nodes
 *                    using a simple steepest descent.
 *
 *      Includes functions:
 *
 *            MobilityLaw_Relax()
 *            MobilityLaw_Relax_node()
 *                
 ***************************************************************************/
#include "Home.h"
#include "Util.h"
#include "Mobility.h"
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#define MIN(a,b)  ((a)<(b)?(a):(b))

/*
#define debug_nodevelocity
*/

/**************************************************************************
 *
 *      Function:     MobilityLaw_Relax
 *      Description:  Simple driver function which loops through all the
 *                    nodes local to the domain, calling the mobility
 *                    function for each node.
 *
 *      Returns:  0 on success
 *                1 if velocity could not be determined
 *
 *
 *************************************************************************/
int Mobility_Relax(Home_t *home, Node_t *node)
{
        Param_t *param;
	int nbrs;
	real8 Mx, My, Mz;
/*
 *      If the node is a "fixed" node, we cannot move it, so just zero
 *      out the velocity and return.
 */
        if (node->constraint == 7) {
	  node->vX = 0.0;
	  node->vY = 0.0;
	  node->vZ = 0.0;
	  return(0);
	}
	    
	param = home->param;
	nbrs = node->numNbrs;
	
	Mx = param->MobScrew;
	My = param->MobEdge;
	Mz = param->MobClimb;
	
	node->vX = Mx*node->fX;
	node->vY = My*node->fY;
	node->vZ = Mz*node->fZ;

#ifdef _THINFILM
	if (node->constraint == 6)
	  node->vZ = 0.0;
#endif
    
#ifdef debug_nodevelocity
/*
 *      Print the force and velocity of every node
 */
        for (i=0; i < home->newNodeKeyPtr; i++) { 

            node = home->nodeKeys[i];
            if (!node) continue;

            printf("node(%d,%d) v=(%e %e %e) f=(%e %e %e)\n",
                   node->myTag.domainID, node->myTag.index,
                   node->vX, node->vY, node->vZ,
                   node->fX, node->fY, node->fZ);
        }
#endif

        return(0);
}

