#include <stdio.h>
#include "Param.h"
#include "math.h"
#include "Home.h"

#ifdef _ANISOTROPIC

#include "ANI.h"

/* To do:
   1. Get rid of Qup, Qdown, etc in the integration, it is a simple sum !
*/

/* number of integration points for the integral formalism */
#ifndef QSB_NINT
#define QSB_NINT 10
#endif

void QSB_compute(real8 tdir[3],
                 real8 Q[3][3],real8 S[3][3],real8 B[3][3],
		 int flag[3])
{
/* input:  tdir[3], flag[3]

           if flag[0]==1 then Q is computed
           if flag[1]==1 then S is computed
           if flag[2]==1 then B is computed
           
   output: Q[3][3], S[3][3], B[3][3]
 */

  int i,j,k,l;
  real8 q11,q12,norm,nq[3],mq[3],m0[3],n0[3],T[3][3];
  real8 n[3],m[3],nn[3][3],nm[3][3],mm[3][3],mn[3][3],nninv[3][3],temp[3][3];
  real8 dS[3][3],dQ[3][3],dB[3][3],Q0[3][3],S0[3][3],B0[3][3];
  real8 dQdown[3][3],dSdown[3][3],dQup[3][3],dSup[3][3],dBdown[3][3],dBup[3][3];

  int npoints = QSB_NINT; 
  real8 domega = M_PI/(npoints*1.0);

  if (flag[0]!=1 && flag[1]!=1 && flag[2]!=1) {
     printf(" no flag is set to 1.  Q, S, B matrices not calculated!\n");
     return;
  }

  if (flag[0]==1) {
     for (i = 0; i < 3; i++) for (j = 0; j < 3; j++) Q0[i][j] = 0.0;
  }
  if (flag[1]==1 || flag[2]==1) {
     for (i = 0; i < 3; i++) for (j = 0; j < 3; j++) S0[i][j] = 0.0;
  }
  if (flag[2]==1) {
     for (i = 0; i < 3; i++) for (j = 0; j < 3; j++) B0[i][j] = 0.0;
     for (i = 0; i < 3; i++) for (j = 0; j < 3; j++) temp[i][j] = 0.0;
  }

  /* establish right-handed coordinate system */
  if (fabs(tdir[0]*tdir[0] + tdir[1]*tdir[1]) > 1.e-10)
  {
      q11 = fabs(tdir[1])/sqrt(tdir[0]*tdir[0] + tdir[1]*tdir[1]);
      if (fabs(tdir[1]) > 1.e-10) 
	q12 = -tdir[0]*q11/tdir[1];
      else
	q12 = 1.0;
      
      norm = sqrt(q11*q11 + q12*q12);
      m0[0] = q11/norm;
      m0[1] = q12/norm;
      m0[2] = 0.0; 
  }
  else
  {
      m0[0]=1.0;m0[1]=0.0;m0[2]=0.0;
  }

  norm = sqrt(m0[0]*m0[0] + m0[1]*m0[1] + m0[2]*m0[2]);
  m0[0]/=norm; m0[1]/=norm; m0[2]/=norm;
  cross(tdir,m0,n0);
  norm = sqrt(n0[0]*n0[0] + n0[1]*n0[1] + n0[2]*n0[2]);
  n0[0]/=norm; n0[1]/=norm; n0[2]/=norm;

  /* make a transformation matrix to get the rotating 
     "integration vectors" */
  T[0][0] = m0[0]; T[0][1] = n0[0]; T[0][2] = tdir[0];
  T[1][0] = m0[1]; T[1][1] = n0[1]; T[1][2] = tdir[1];
  T[2][0] = m0[2]; T[2][1] = n0[2]; T[2][2] = tdir[2];

  /* INTEGRATION
     Fixed trapezoidal scheme
     First step "i=0" */
  nq[0] = 1.0; nq[1]= 0.0;nq[2]=0.0;
  mq[0] = 0.0; mq[1]=-1.0;mq[2]=0.0;

  /* Operate with T matrix to rotate integration vectors */
  for (k = 0; k < 3; k++)
  {
      n[k] = T[k][0]*nq[0] + T[k][1]*nq[1] + T[k][2]*nq[2];
      m[k] = T[k][0]*mq[0] + T[k][1]*mq[1] + T[k][2]*mq[2];
  }
  
  norm = sqrt(m[0]*m[0] + m[1]*m[1] + m[2]*m[2]);
  m[0]/= norm; m[1]/= norm; m[2]/= norm;
  
  /* get bracket matrices (in loop will be omega-dependent) */
  brackets(n,n,nn);
  brackets(n,m,nm);
  brackets(m,m,mm);
  brackets(m,n,mn);
	
  /* Invert nn */
  Inv3x3(nn,nninv);

  /* integrand evaluated at start point */

  /* Q */
  if (flag[0]==1) {
    for (i = 0; i < 3; i++) for (j = 0; j < 3; j++) dQ[i][j] = -nninv[i][j];
  }

  /* S */
  if (flag[1]==1 || flag[2]==1) {
    Multiply3x3(nninv,nm,dS);
    for (i = 0; i < 3; i++) for (j = 0; j < 3; j++) dS[i][j] = -dS[i][j];
  }
  
  /* B */
  if (flag[2]==1) {
    Multiply3x3(mn,dS,temp);
    for (i = 0; i < 3; i++) for (j = 0; j < 3; j++) dB[i][j] = mm[i][j] + temp[i][j];
  }
  
  /* Set first values for integration loop */
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	dQdown[i][j] = dQ[i][j];
	dSdown[i][j] = dS[i][j];
	dBdown[i][j] = dB[i][j];
      }

  /* now the rest */
  for (i = 1; i <= npoints; i++)
    {
      nq[0] = cos(i*domega);
      nq[1] = sin(i*domega);
      nq[2] = 0.0;

      mq[0] = sin(i*domega);
      mq[1] =-cos(i*domega);
      mq[2] = 0.0;
      
      /* get integration vectors : i=0 already done */
      for (l = 0; l < 3; l++)
      {
	  n[l]=T[l][0]*nq[0]+T[l][1]*nq[1]+T[l][2]*nq[2];
	  m[l]=T[l][0]*mq[0]+T[l][1]*mq[1]+T[l][2]*mq[2];
      }

      norm = sqrt(m[0]*m[0] + m[1]*m[1] + m[2]*m[2]);
      m[0]/=norm;m[1]/=norm;m[2]/=norm;

      /* bracket matrices */
      brackets(n,n,nn);
      brackets(n,m,nm);
      brackets(m,n,mn);
      brackets(m,m,mm);

      Inv3x3(nn,nninv);
      Multiply3x3(nninv,nm,dSup);

      /* get upper values for integrands */
      if (flag[0]==1) {
         for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) dQup[j][k] = - nninv[j][k];
      }
      if (flag[1]==1 || flag[2]==1) {
         for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) dSup[j][k] = - dSup[j][k];
      }
      if (flag[2]==1) {
          Multiply3x3(mn,dSup,temp);
          for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) dBup[j][k] = mm[j][k] + temp[j][k];
      }

      /* trapezoidal integration step */
      if (flag[0]==1) {
         for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) 
            Q0[j][k] += 0.5*domega*(dQdown[j][k] + dQup[j][k]);
      }
      if (flag[1]==1) {
         for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) 
	    S0[j][k] += 0.5*domega*(dSdown[j][k] + dSup[j][k]);
      }
      if (flag[2]==1) {
         for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) 
	    B0[j][k] += 0.5*domega*(dBdown[j][k] + dBup[j][k]);
      }
      
      /* re-use upper values for lower values of next step */
      if (flag[0]==1) {
          for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) dQdown[j][k] = dQup[j][k];
      }
      if (flag[1]==1) {
          for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) dSdown[j][k] = dSup[j][k];
      }
      if (flag[2]==1) {
          for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) dBdown[j][k] = dBup[j][k];
      }

    } // end i loop on npoints
  
  /* 
   * Assign output values for Q and S
   * these are divided by pi for integration range 0 to pi
   * domega is pi/npoints
   * in definition Q,S actually over 2 pi, integrate 0 to 2 pi
   * symmetry allows us to halve this
   */
  
  if (flag[0]==1) {
     for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) Q[j][k] = Q0[j][k]/M_PI;
  }
  if (flag[1]==1) {
     for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) S[j][k] = S0[j][k]/M_PI;
  }
  if (flag[2]==1) {
     for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) B[j][k] = B0[j][k]/M_PI;
  }

} /* end of QSB_compute */

void QS_compute(real8 tdir[3],real8 Q[3][3],real8 S[3][3])
{
  real8 B[3][3];
  int flag[3]={1,1,0};
  QSB_compute(tdir,Q,S,B,flag);
}

void Q_compute(real8 tdir[3],real8 Q[3][3])
{
  real8 S[3][3],B[3][3];
  int flag[3]={1,0,0};
  QSB_compute(tdir,Q,S,B,flag);
} 
  
void S_compute(real8 tdir[3],real8 S[3][3])
{
  real8 Q[3][3],B[3][3];
  int flag[3]={0,1,0};
  QSB_compute(tdir,Q,S,B,flag);
} 

void B_compute(real8 tdir[3],real8 B[3][3])
{
  real8 Q[3][3],S[3][3];
  int flag[3]={0,0,1};
  QSB_compute(tdir,Q,S,B,flag);
} 
#endif
