/*****************************************************************************
 *
 *      Module:         Collision.c
 *      Description:    This module contains various functions used
 *                      for detecting various types of collisions
 *                      (segment/segment, node/node, zipping) and
 *                      dealing with those collisions.
 *
 *                      Each domain handles local collisions and
 *                      distributes the necessary topological changes
 *                      to remote domains via the same mechanism
 *                      employed by remesh.
 *
 *                      NOTE: Certain strict rules govern what topological
 *                      changes are permitted based on noda and segment
 *                      ownership.  See comments at the beginning of
 *                      the module Topology.c for details of the
 *                      rule.  Additional restrictions may be implemented
 *                      for the collision handling; see code below
 *                      for details of any such restrictions.
 *
 *      Included functions:
 *
 *          FindCollisionPoint()
 *          GetMinDist()
 *          HandleCollisions()
 *
 *      Last Modified: 12/08/2008 gh - Replaced FindCollisionPoint() which was
 *                                     throwing nodes long distances when
 *                                     collisions involved multiple near-
 *                                     parallel planes.
 *
 *****************************************************************************/
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include "Home.h"
#include "Util.h"
#include "Comm.h"
#include "Mobility.h"

static int dbgDom;


/*---------------------------------------------------------------------------
 *
 *      Function:       FindCollisionPoint
 *      Description:    This function attempts to select a collision
 *                      point on a plane common to the two nodes.
 *
 *
 *      Arguments:
 *          node1, node2   pointers to the two node structures
 *          x, y, z        pointers to locations in which to return
 *                         the coordinates of the point at which the
 *                         two nodes should be collided.
 *
 *-------------------------------------------------------------------------*/
static void FindCollisionPoint(Home_t *home, Node_t *node1, Node_t *node2,
                               real8 *x, real8 *y, real8 *z)
{ 
        int     i, j, m, n;
        real8   L, invL, tmp;
        real8   norm, invnorm;
        real8   n1mag2, n2mag2, eps;
        real8   dx, dy, dz;
        real8   n1x, n1y, n1z;
        real8   n2x, n2y, n2z;
        real8   dirx, diry, dirz;
        real8   p1[3], p2[3], n1[3], n2[3];
        real8   plane[3], vector[3];
        real8   tmp33[3][3];
        Node_t  *nbrNode;
        Param_t *param;

        param = home->param;

        eps = 1.0e-12;

        p1[0] = node1->x;
        p1[1] = node1->y;
        p1[2] = node1->z;

        p2[0] = node2->x;
        p2[1] = node2->y;
        p2[2] = node2->z;

        PBCPOSITION(param, p1[0], p1[1], p1[2], &p2[0], &p2[1], &p2[2]);

/*
 *      If a node is a 'fixed' node it can't be relocated, so use
 *      that node's coordinates as the collision point
 */
        if (node1->constraint == 7) {
            *x = p1[0];
            *y = p1[1];
            *z = p1[2];
            return;
        } else if (node2->constraint == 7) {
            *x = p2[0];
            *y = p2[1];
            *z = p2[2];
            return;
        }

/*
 *      If strict glide planes are not being enforced, we
 *      can use 'fuzzy' glide planes so that we don't
 *      throw nodes long distances if the planes are
 *      close to parallel but don't quite meet the criteria
 *      for being parallel.
 */
        if (param->enforceGlidePlanes == 0) {

            real8   factor;
            real8   vector[3], result[3];
            real8   Matrix[3][3], invMatrix[3][3];

/*
 *          <factor> determines how 'fuzzy' planes are.  The smaller the
 *          value, the fuzzier the plane constraints.
 */
            factor = 5.0;

            vector[0] = (p1[0] + p2[0]) / 2.0;
            vector[1] = (p1[1] + p2[1]) / 2.0;
            vector[2] = (p1[2] + p2[2]) / 2.0;

            for (m = 0; m < 3; m++) {
                for (n = 0; n < 3; n++) {
                    Matrix[m][n] = (real8)(m == n);
                }
            }

            for (i = 0; i < node1->numNbrs; i++) {

                nbrNode = GetNeighborNode(home, node1, i);

                dx = p1[0] - nbrNode->x;
                dy = p1[1] - nbrNode->y;
                dz = p1[2] - nbrNode->z;

                ZImage(param, &dx, &dy, &dz);

                L = sqrt(dx*dx + dy*dy + dz*dz);
                invL = 1.0 / L;
                dirx = dx * invL;
                diry = dy * invL;
                dirz = dz * invL;

                xvector(dirx, diry, dirz, node1->burgX[i], node1->burgY[i],
                        node1->burgZ[i], &n1x, &n1y, &n1z);

                xvector(dirx, diry, dirz, node1->vX, node1->vY, node1->vZ,
                        &n2x, &n2y, &n2z);

                n1mag2 = n1x*n1x + n1y*n1y + n1z*n1z;
                n2mag2 = n2x*n2x + n2y*n2y + n2z*n2z;

                if (n1mag2 > eps) {
                    norm = sqrt(n1mag2);
                    invnorm = 1.0 / norm;
                    plane[0] = n1x * invnorm;
                    plane[1] = n1y * invnorm;
                    plane[2] = n1z * invnorm;
                } else if (n2mag2 > eps) {
                    norm = sqrt(n2mag2);
                    invnorm = 1.0 / norm;
                    plane[0] = n2x * invnorm;
                    plane[1] = n2y * invnorm;
                    plane[2] = n2z * invnorm;
                } else continue;
                
                Matrix31Vector3Mult(plane, plane, tmp33);

                for (m = 0; m < 3; m++) {
                    for (n = 0; n < 3; n++) {
                        Matrix[m][n] += factor * tmp33[m][n];
                    }
                }

                tmp = DotProduct(plane, p1);

                for (m = 0; m < 3; m++) {
                    vector[m] += factor * tmp * plane[m];
                }
            }

            for (i = 0; i < node2->numNbrs; i++) {

                nbrNode = GetNeighborNode(home, node2, i);

                dx = p2[0] - nbrNode->x;
                dy = p2[1] - nbrNode->y;
                dz = p2[2] - nbrNode->z;

                ZImage(param, &dx, &dy, &dz);

                L = sqrt(dx*dx + dy*dy + dz*dz);
                invL = 1.0 / L;
                dirx = dx * invL;
                diry = dy * invL;
                dirz = dz * invL;

                xvector(dirx, diry, dirz, node2->burgX[i], node2->burgY[i],
                        node2->burgZ[i], &n1x, &n1y, &n1z);

                xvector(dirx, diry, dirz, node2->vX, node2->vY, node2->vZ,
                        &n2x, &n2y, &n2z);

                n1mag2 = n1x*n1x + n1y*n1y + n1z*n1z;
                n2mag2 = n2x*n2x + n2y*n2y + n2z*n2z;

                if (n1mag2 > eps) {
                    norm = sqrt(n1mag2);
                    invnorm = 1.0 / norm;
                    plane[0] = n1x * invnorm;
                    plane[1] = n1y * invnorm;
                    plane[2] = n1z * invnorm;
                } else if (n2mag2 > eps) {
                    norm = sqrt(n2mag2);
                    invnorm = 1.0 / norm;
                    plane[0] = n2x * invnorm;
                    plane[1] = n2y * invnorm;
                    plane[2] = n2z * invnorm;
                } else continue;
                
                Matrix31Vector3Mult(plane, plane, tmp33);

                for (m = 0; m < 3; m++) {
                    for (n = 0; n < 3; n++) {
                        Matrix[m][n] += factor * tmp33[m][n];
                    }
                }

                tmp = DotProduct(plane, p2);
    
                for (m = 0; m < 3; m++) {
                    vector[m] += factor * tmp * plane[m];
                }
            }

            if (!Matrix33Invert(Matrix, invMatrix)) {
                Fatal("FindCollisionPoint(): Unable to invert matrix!");
            }

            Matrix33Vector3Multiply(invMatrix, vector, result);

            *x = result[0];
            *y = result[1];
            *z = result[2];

        } else {
/*
 *         enforceGlidePlanes == 1, so strict glide planes are being used...
 */
           int     conditionsmet, Nsize;
           real8   newplanecond, npc2, onemnpc4, detN;
           real8   Nmat[3][3] = {{0.0,0.0,0.0}, {0.0,0.0,0.0}, {0.0,0.0,0.0}};
           real8   Matrix[6][6], invMatrix[6][6];
           real8   V[6], result[6];

           newplanecond = 0.875;
           npc2         = newplanecond * newplanecond;

           tmp          = 1.0 - newplanecond;
           onemnpc4     = tmp * tmp * tmp * tmp;

           vector[0] = 0.0;
           vector[1] = 0.0;
           vector[2] = 0.0;

           Nsize = 0;

           for (i = 0; i < node1->numNbrs; i++) {

               if (Nsize < 3) {

                   nbrNode = GetNeighborNode(home, node1, i);

                   dx = p1[0] - nbrNode->x;
                   dy = p1[1] - nbrNode->y;
                   dz = p1[2] - nbrNode->z;

                   ZImage(param, &dx, &dy, &dz);

                   L = sqrt(dx*dx + dy*dy + dz*dz);
                   invL = 1.0 / L;
                   dirx = dx * invL;
                   diry = dy * invL;
                   dirz = dz * invL;

                   xvector(dirx, diry, dirz, node1->burgX[i], node1->burgY[i],
                           node1->burgZ[i], &n1x, &n1y, &n1z);

                   xvector(dirx, diry, dirz, node1->vX, node1->vY, node1->vZ,
                           &n2x, &n2y, &n2z);

                   n1mag2 = n1x*n1x + n1y*n1y + n1z*n1z;
                   n2mag2 = n2x*n2x + n2y*n2y + n2z*n2z;

                   if (n1mag2 > eps) {
                       norm = sqrt(n1mag2);
                       invnorm = 1.0 / norm;
                       plane[0] = n1x * invnorm;
                       plane[1] = n1y * invnorm;
                       plane[2] = n1z * invnorm;
                   } else if (n2mag2 > eps) {
                       norm = sqrt(n2mag2);
                       invnorm = 1.0 / norm;
                       plane[0] = n2x * invnorm;
                       plane[1] = n2y * invnorm;
                       plane[2] = n2z * invnorm;
                   } else continue;


                   switch (Nsize) {
                   case 0:
                       conditionsmet = 1;
                       break;

                   case 1:
                       tmp = Nmat[0][0]*plane[0] +
                             Nmat[0][1]*plane[1] +
                             Nmat[0][2]*plane[2];
                       conditionsmet = (tmp*tmp < npc2);
                       break;

                   default:
                      Nmat[2][0] = plane[0];
                      Nmat[2][1] = plane[1];
                      Nmat[2][2] = plane[2];

                      detN = Matrix33Det(Nmat);
                      conditionsmet = (detN*detN > onemnpc4);
                   }

                   if (conditionsmet) {
                       Nmat[Nsize][0] = plane[0];
                       Nmat[Nsize][1] = plane[1];
                       Nmat[Nsize][2] = plane[2];
                       vector[Nsize] = DotProduct(plane, p1);
                       Nsize++;
                   }
               }
           }

           for (i = 0; i < node2->numNbrs; i++) {

               if (Nsize < 3) {

                   nbrNode = GetNeighborNode(home, node2, i);

                   dx = p2[0] - nbrNode->x;
                   dy = p2[1] - nbrNode->y;
                   dz = p2[2] - nbrNode->z;

                   ZImage(param, &dx, &dy, &dz);

                   L = sqrt(dx*dx + dy*dy + dz*dz);
                   invL = 1.0 / L;
                   dirx = dx * invL;
                   diry = dy * invL;
                   dirz = dz * invL;

                   xvector(dirx, diry, dirz, node2->burgX[i], node2->burgY[i],
                           node2->burgZ[i], &n1x, &n1y, &n1z);

                   xvector(dirx, diry, dirz, node2->vX, node2->vY, node2->vZ,
                           &n2x, &n2y, &n2z);

                   n1mag2 = n1x*n1x + n1y*n1y + n1z*n1z;
                   n2mag2 = n2x*n2x + n2y*n2y + n2z*n2z;

                   if (n1mag2 > eps) {
                       norm = sqrt(n1mag2);
                       invnorm = 1.0 / norm;
                       plane[0] = n1x * invnorm;
                       plane[1] = n1y * invnorm;
                       plane[2] = n1z * invnorm;
                   } else if (n2mag2 > eps) {
                       norm = sqrt(n2mag2);
                       invnorm = 1.0 / norm;
                       plane[0] = n2x * invnorm;
                       plane[1] = n2y * invnorm;
                       plane[2] = n2z * invnorm;
                   } else continue;
 

                   switch (Nsize) {
                   case 1:
                       tmp = Nmat[0][0]*plane[0] +
                             Nmat[0][1]*plane[1] +
                             Nmat[0][2]*plane[2];
                       conditionsmet = (tmp*tmp < npc2);
                       break;
                   default:
                      Nmat[2][0] = plane[0];
                      Nmat[2][1] = plane[1];
                      Nmat[2][2] = plane[2];
                      detN = Matrix33Det(Nmat);
                      conditionsmet = (detN*detN > onemnpc4);
                   }
 
                   if (conditionsmet) {
                       Nmat[Nsize][0] = plane[0];
                       Nmat[Nsize][1] = plane[1];
                       Nmat[Nsize][2] = plane[2];
                       vector[Nsize] = DotProduct(plane, p2);
                       Nsize++;
                   }
               }
           }

/*
 *         Upper left 3X3 of Matrix is identity matrix.
 *         Matrix rows 3 thru 3+(Nsize-1) colums 0 thru 2 are Nmat.
 *         Matrix columns 3 thru 3+(Nsize-1) rows 0 thru 2 are transpose of Nmat.
 *         All remaining elements are zeroed.
 */
           for (i = 0; i < 6; i++) {
               for (j = 0; j < 6; j++) {
                   Matrix[i][j] = 0.0;
               }
           }

           Matrix[0][0] = 1.0;
           Matrix[1][1] = 1.0;
           Matrix[2][2] = 1.0;

           for (i = 0; i < Nsize; i++) {
               for (j = 0; j < 3; j++) {
                   Matrix[3+i][j] = Nmat[i][j];
                   Matrix[j][3+i] = Nmat[i][j];
               }
           }
           V[0] = 0.5 * (p1[0] + p2[0]);
           V[1] = 0.5 * (p1[1] + p2[1]);
           V[2] = 0.5 * (p1[2] + p2[2]);
           V[3] = vector[0];
           V[4] = vector[1];
           V[5] = vector[2];

           Nsize += 3;

           MatrixInvert((real8 *)Matrix, (real8 *)invMatrix, Nsize, 6);
           MatrixMult((real8 *)invMatrix, Nsize, Nsize, 6,
                      (real8 *)V, 1, 1,
                      (real8 *)result, 1);

           *x = result[0];
           *y = result[1];
           *z = result[2];

       }  /* enforceGlidePlanes != 0 */

       return;
}


/*---------------------------------------------------------------------------
 *
 *      Function:       GetMinDist
 *      Description:    Find the minimum distance between the two segments
 *                      p1->p2 andp3->p4.
 *
 *      Arguments:
 *          p1x, p1y, p1z  Coordinates of the first endpoint of segment 1
 *          v1x, v1y, v1z  Velocity of the node at point p1
 *          p2x, p2y, p2z  Coordinates of the second endpoint of segment 1
 *          v2x, v2y, v2z  Velocity of the node at point p2
 *          p3x, p3y, p3z  Coordinates of the first endpoint of segment 2
 *          v3x, v3y, v3z  Velocity of the node at point p3
 *          p4x, p4y, p4z  Coordinates of the second endpoint of segment 2
 *          v4x, v4y, v4z  Velocity of the node at point p4
 *          dist2          pointer to location in which to return the square
 *                         of the minimum distance between the two points
 *          ddist2dt       pointer to location in which to return the 
 *                         time rate of change of the distance between
 *                         L1 and L2
 *          L1             pointer to location at which to return the 
 *                         normailzed position on seg 1 closest to segment 2
 *          L2             pointer to location at which to return the 
 *                         normailzed position on seg 2 closest to segment 1
 *
 *-------------------------------------------------------------------------*/
void GetMinDist(real8 p1x, real8 p1y, real8 p1z,
       real8 v1x, real8 v1y, real8 v1z,
       real8 p2x, real8 p2y, real8 p2z, real8 v2x, real8 v2y, real8 v2z,
       real8 p3x, real8 p3y, real8 p3z, real8 v3x, real8 v3y, real8 v3z,
       real8 p4x, real8 p4y, real8 p4z, real8 v4x, real8 v4y, real8 v4z,
       real8 *dist2, real8 *ddist2dt, real8 *L1, real8 *L2)
{
        int    i, pos;
        real8  A, B, C, D, E, F, G;
        real8  eps = 1.0e-12;
        real8  ddistxdt, ddistydt, ddistzdt;
        real8  seg1Lx, seg1Ly, seg1Lz;
        real8  seg2Lx, seg2Ly, seg2Lz;
        real8  seg1vx, seg1vy, seg1vz;
        real8  seg2vx, seg2vy, seg2vz;
        real8  distx, disty, distz;
        real8  dist[4];

        seg1Lx = p2x - p1x;
        seg1Ly = p2y - p1y;
        seg1Lz = p2z - p1z;

        seg2Lx = p4x - p3x;
        seg2Ly = p4y - p3y;
        seg2Lz = p4z - p3z;

        seg1vx = v2x - v1x;
        seg1vy = v2y - v1y;
        seg1vz = v2z - v1z;

        seg2vx = v4x - v3x;
        seg2vy = v4y - v3y;
        seg2vz = v4z - v3z;

        A = seg1Lx*seg1Lx + seg1Ly*seg1Ly + seg1Lz*seg1Lz;
        B = ((seg1Lx*(p1x-p3x)) +
             (seg1Ly*(p1y-p3y)) +
             (seg1Lz*(p1z-p3z))) * 2.0;
        C = (seg1Lx*seg2Lx + seg1Ly*seg2Ly + seg1Lz*seg2Lz) * 2.0;
        D = ((seg2Lx*(p3x-p1x)) +
             (seg2Ly*(p3y-p1y)) +
             (seg2Lz*(p3z-p1z))) * 2.0;
        E = seg2Lx*seg2Lx + seg2Ly*seg2Ly + seg2Lz*seg2Lz;
        F = (p1x*p1x + p1y*p1y + p1z*p1z) + (p3x*p3x + p3y*p3y + p3z*p3z);
        G = C*C - 4*A*E;


/*
 *      If segment 1 is just a point...
 */
        if (A < eps) {
            *L1 = 0.0;
            if (E < eps) *L2 = 0.0;
            else *L2 = -0.5 * D / E;

/*
 *      If segment 2 is just a point...
 */
        } else if (E < eps) {
            *L2 = 0.0;
            if (A < eps) *L1 = 0.0;
            else *L1 = -0.5 * B / A;
/*
 *      If segments are parallel
 */
        } else if (fabs(G) < eps) {
            dist[0] = (p3x-p1x)*(p3x-p1x) +
                      (p3y-p1y)*(p3y-p1y) +
                      (p3z-p1z)*(p3z-p1z);
            dist[1] = (p4x-p1x)*(p4x-p1x) +
                      (p4y-p1y)*(p4y-p1y) +
                      (p4z-p1z)*(p4z-p1z);
            dist[2] = (p3x-p2x)*(p3x-p2x) +
                      (p3y-p2y)*(p3y-p2y) +
                      (p3z-p2z)*(p3z-p2z);
            dist[3] = (p3x-p2x)*(p3x-p2x) +
                      (p3y-p2y)*(p3y-p2y) +
                      (p3z-p2z)*(p3z-p2z);

            *dist2 = dist[0];

            for (i = 1; i < 4; i++) {
                if (dist[i] < *dist2) {
                    *dist2 = dist[i];
                    pos = i+1;
                }
            }

            *L1 = floor((real8)pos/2.0);
            *L2 = (real8)(pos-1 % 2);

        } else {
            *L2 = (2.0*A*D + B*C) / G;
            *L1 = 0.5 * (C* *L2 - B) / A;
        }

/*
 *      Make sure L1 and L2 are between 0 and 1
 */
        *L1 = MIN(MAX(*L1, 0.0), 1.0);
        *L2 = MIN(MAX(*L2, 0.0), 1.0);
        
/*
 *      Lastly, calculate the distance^2 and the time rate of change
 *      between the points at L1 and L2.
 */
        distx = p1x + (seg1Lx * *L1) - p3x - (seg2Lx * *L2);
        disty = p1y + (seg1Ly * *L1) - p3y - (seg2Ly * *L2);
        distz = p1z + (seg1Lz * *L1) - p3z - (seg2Lz * *L2);

        *dist2 = distx*distx + disty*disty + distz*distz;

        ddistxdt = (v1x + (seg1vx * *L1) - v3x - (seg2vx * *L2)) *
                   (p1x + (seg1Lx * *L1) - p3x - (seg2Lx * *L2));
        ddistydt = (v1y + (seg1vy * *L1) - v3y - (seg2vy * *L2)) *
                   (p1y + (seg1Ly * *L1) - p3y - (seg2Ly * *L2));
        ddistzdt = (v1z + (seg1vz * *L1) - v3z - (seg2vz * *L2)) *
                   (p1z + (seg1Lz * *L1) - p3z - (seg2Lz * *L2));

        *ddist2dt = 2.0 * (ddistxdt + ddistydt + ddistzdt);

        return;
}



/*---------------------------------------------------------------------------
 *
 *      Function:       HandleCollisions
 *      Description:    Loop though all native nodes, identify segments
 *                      or nodes that should be collided (or zipped) by
 *                      the current domain and handle all such collisions.
 *
 *                      Note the following restrictions on collisions:
 *
 *                      - A 'fixed' node (i.e. constraint == 7) cannot
 *                        be deleted during a collision
 *                      - A node which has been the target of a merge
 *                        (this timestep) may not be deleted during any 
 *                        other type of collision in this timestep.
 *
 *      All nodes with arms owned by another domain have previously
 *      been marked as non-deletable nodes for this cycle.
 */
void HandleCollisions(Home_t *home)
{
        int     i, j, k, q, arm12, arm21, arm34, arm43;
        int     thisDomain, splitStatus, mergeStatus;
        int     armAB, armBA;
        int     globalOp = 1, didCollision;
        int     close2node1, close2node2, close2node3, close2node4;
        int     undeletable1, undeletable2, splitSeg1, splitSeg2;
        int     cell2Index, nbrCell2Index, nextIndex;
        int     cell2X, cell2Y, cell2Z, cx, cy, cz;
        int     localCollisionCnt, globalCollisionCnt;
        real8   maxSep, maxSep2, mindist2, dist2, ddist2dt, L1, L2, eps, half;
        real8   dx, dy, dz;
        real8   x1, y1, z1, vx1, vy1, vz1;
        real8   x2, y2, z2, vx2, vy2, vz2;
        real8   x3, y3, z3, vx3, vy3, vz3;
        real8   x4, y4, z4, vx4, vy4, vz4;
        real8   seg1Lx, seg1Ly, seg1Lz, seg1Vx, seg1Vy, seg1Vz;
        real8   seg2Lx, seg2Ly, seg2Lz, seg2Vx, seg2Vy, seg2Vz;
        real8   minLen2, segLen2;
        real8   newx, newy, newz;
        real8   newvx, newvy, newvz;
        real8   p0[3], p1[3], p2[3], pnew[3], burg1[3];
        real8   oldfp0[3], oldfp1[3];
        real8   f0seg1[3], f1seg1[3], f0seg2[3], f1seg2[3];
        real8   nodeVel[3], newNodeVel[3];
        real8   newPos[3], newVel[3];
        Tag_t   oldTag1, oldTag2;
        Node_t  *node1, *node2, *node3, *node4, *tmpNbr;
        Node_t  *mergenode1, *mergenode2, *deadNode, *targetNode;
        Node_t  *splitNode1, *splitNode2;
        Param_t *param;

        thisDomain = home->myDomain;
        param      = home->param;

        mindist2 = param->rann * param->rann;
        maxSep   = param->rann + (2.0 * param->maxSeg);
        maxSep2  = maxSep*maxSep;
        eps      = 1.0e-12;
        half     = 0.5;
        minLen2  = MAX(mindist2, (half*param->minSeg)*(half*param->minSeg));

        localCollisionCnt = 0;
        globalCollisionCnt = 0;

#ifdef DEBUG_TOPOLOGY_DOMAIN
        dbgDom = DEBUG_TOPOLOGY_DOMAIN;
#else
        dbgDom = -1;
#endif

        TimerStart(home, COLLISION_HANDLING);

/*
 *      Start looping through native nodes looking for segments to collide...
 */
        for (i = 0; i < home->newNodeKeyPtr; i++) {

            if ((node1 = home->nodeKeys[i]) == (Node_t *)NULL) continue;
            if (node1->flags & NO_COLLISIONS) continue;

            didCollision = 0;

/*
 *          Loop through all cell2s neighboring the node.  Only
 *          nodes in these neighboring cell2s are candidates for
 *          collisions.
 *
 *          NOTE: All nodes are assigned cell2 membership prior to
 *          entering collision handling.  However, nodes added during
 *          node separation and collision handling are not given
 *          cell2 membership for this cycle.  In most cases this
 *          is not a problem since those nodes have usually been
 *          exempted from subsequent collisions this cycle.  There
 *          are certain circumstances, though, in which the exemptions
 *          have not been set.  If we encounter one such node that
 *          has no cell2 membership, skip it for this cycle, or bad
 *          things happen.
 */
            cell2Index = node1->cell2Idx;
            if (cell2Index < 0) {
                continue;
            }

            DecodeCell2Idx(home, cell2Index, &cell2X, &cell2Y, &cell2Z);

            for (cx = cell2X - 1; cx <= cell2X + 1; cx++) {
             for (cy = cell2Y - 1; cy <= cell2Y + 1; cy++) {
              for (cz = cell2Z - 1; cz <= cell2Z + 1; cz++) {

                nbrCell2Index = EncodeCell2Idx(home, cx, cy, cz);

/*
 *              Loop though all nodes in the neighbor cell2
 */
                nextIndex = home->cell2[nbrCell2Index];

                while (nextIndex >= 0) {

                    if (didCollision) break;

                    node3 = home->cell2QentArray[nextIndex].node;
                    nextIndex = home->cell2QentArray[nextIndex].next;

                    if (node3 == (Node_t *)NULL) continue;
                    if (node3->flags & NO_COLLISIONS) continue;

                    if (CollisionNodeOrder(home, &node1->myTag,
                                           &node3->myTag) >= 0) {
                        continue;
                    }

/*
 *                  Loop over all arms of node1.  Skip any arms that
 *                  terminate at node3 (those hinge arms will be dealt
 *                  with later)
 */
                    for (arm12 = 0; arm12 < node1->numNbrs; arm12++) {

                        if (didCollision) break;

                        node2 = GetNodeFromTag(home, node1->nbrTag[arm12]);

                        if (node2 == (Node_t *)NULL) continue;
                        if (node2->flags & NO_COLLISIONS) continue;

                        if (CollisionNodeOrder(home, &node1->myTag,
                                               &node2->myTag) > 0) {
                            continue;
                        }

                        if ((node2->myTag.domainID == node3->myTag.domainID) &&
                            (node2->myTag.index    == node3->myTag.index   )) {
                            continue;
                        }

/*
 *                      Segment node1/node2 may only be used in a collision
 *                      if the segment is owned by the current domain.
 */
                        if (!DomainOwnsSeg(home, OPCLASS_COLLISION,
                                           thisDomain, &node2->myTag)) {
                            continue;
                        }

/*
 *                      Loop over all arms of node3.  Skip any segments that
 *                      terminate at node2 (those hinge arms will be dealt
 *                      with later), and skip any segments not owned by
 *                      node3 -- we'll deal with those segments when we
 *                      hit the node owning the segment
 *                      
 */
                        for (arm34 = 0; arm34 < node3->numNbrs; arm34++) {

                            if (didCollision) break;

                            node4 = GetNodeFromTag(home, node3->nbrTag[arm34]);

                            if (node4 == (Node_t *)NULL) continue;
                            if (node4->flags & NO_COLLISIONS) continue;

                            if ((node4->myTag.domainID==node2->myTag.domainID)&&
                                (node4->myTag.index   ==node2->myTag.index)) {
                                continue;
                            }
            
                            if (CollisionNodeOrder(home, &node3->myTag,
                                                   &node4->myTag) > 0) {
                                continue;
                            }

/*
 *                          At this point, segment node3/node4 is owned by
 *                          node3.  If node3 is not native to this domain,
 *                          the segment may not be used in a collision since
 *                          the domain doing to collision must own both
 *                          segments.
 */
                            if (node3->myTag.domainID != thisDomain) {
                                continue;
                            }

                            x1 = node1->x; y1 = node1->y; z1 = node1->z;
                            x2 = node2->x; y2 = node2->y; z2 = node2->z;
                            x3 = node3->x; y3 = node3->y; z3 = node3->z;
                            x4 = node4->x; y4 = node4->y; z4 = node4->z;

                            vx1 = node1->vX; vy1 = node1->vY; vz1 = node1->vZ;
                            vx2 = node2->vX; vy2 = node2->vY; vz2 = node2->vZ;
                            vx3 = node3->vX; vy3 = node3->vY; vz3 = node3->vZ;
                            vx4 = node4->vX; vy4 = node4->vY; vz4 = node4->vZ;
    
                            PBCPOSITION(param, x1, y1, z1, &x2, &y2, &z2);
                            PBCPOSITION(param, x1, y1, z1, &x3, &y3, &z3);
                            PBCPOSITION(param, x3, y3, z3, &x4, &y4, &z4);
    
/*
 *                          Find the minimum distance between the two segments
 *                          and determine if they should be collided.
 */
                            GetMinDist(x1, y1, z1, vx1, vy1, vz1,
                                       x2, y2, z2, vx2, vy2, vz2,
                                       x3, y3, z3, vx3, vy3, vz3,
                                       x4, y4, z4, vx4, vy4, vz4,
                                       &dist2, &ddist2dt, &L1, &L2);
    
/*
 *                          Requiring the time rate of change of dist2 to be 
 *                          larger than zero excludes connected links.
 *                          Non-endpoint condition looks for co-planar line
 *                          segments that are already intersecting whose
 *                          collision was not anticipated due to a large
 *                          timestep
 */
                            if (((dist2<mindist2) && (ddist2dt<-eps)) ||
                                (dist2<eps)) {
/*
 *                              Identify the first node to be merged.  If the
 *                              collision point is close to one of the nodal
 *                              endpoints, use that node, otherwise insert a
 *                              new node in the segment.
 *
 *                              NOTE: The current domain owns node1 but may
 *                              not own node2.  If it does not own node2, we 
 *                              cannot allow the collision to use node2
 *                              even if the collision point is close to 
 *                              that node.
 */
                                seg1Lx = x1 - x2;
                                seg1Ly = y1 - y2;
                                seg1Lz = z1 - z2;
    
                                seg1Vx = vx1 - vx2;
                                seg1Vy = vy1 - vy2;
                                seg1Vz = vz1 - vz2;
    
                                close2node1 = (((seg1Lx*seg1Lx +
                                                 seg1Ly*seg1Ly +
                                                 seg1Lz*seg1Lz) *
                                                (L1 * L1)) < mindist2);
    
                                close2node2 = (((seg1Lx*seg1Lx +
                                                 seg1Ly*seg1Ly +
                                                 seg1Lz*seg1Lz) *
                                                ((1-L1) * (1-L1))) < mindist2);

                                if ((node2->myTag.domainID != thisDomain) &&
                                    close2node2) {
                                    continue;
                                }

                                if (close2node1) {
                                    mergenode1 = node1;
                                    splitSeg1 = 0;
                                } else if (close2node2) {
                                    mergenode1 = node2;
                                    splitSeg1 = 0;
                                } else {
                                    splitSeg1 = 1;
                                }

				/* 
				 * If both nodes are pinned do not allow a new point to be inserted in the segment.
				 * Choose one end instead
				 */ 

				/*
				if (node1->constraint == 7 && node2->constraint == 7)
				  {
				    real8 isclose1, isclose2;
				    isclose1 = (seg1Lx*seg1Lx + seg1Ly*seg1Ly + seg1Lz*seg1Lz) * (L1 * L1);
				    isclose2 = (seg1Lx*seg1Lx + seg1Ly*seg1Ly + seg1Lz*seg1Lz) * ((1-L1) * (1-L1));
				    if ( isclose1 < isclose2 ) 
				      {
					mergenode1 = node1;
					splitSeg1 = 0;
				      }
				    else
				      {
					mergenode1 = node2;
					splitSeg1 = 0;
				      }
				  }
				*/



/*
 *                              Identify the second node to be merged
 *
 *                              Note: The current domain owns node3 but may not
 *                              own node4.  If it does not own node4, we 
 *                              cannot allow the collision to use node4
 *                              even if the collision point is close to 
 *                              that node.
 */
                                seg2Lx = x3 - x4;
                                seg2Ly = y3 - y4;
                                seg2Lz = z3 - z4;
    
                                seg2Vx = vx3 - vx4;
                                seg2Vy = vy3 - vy4;
                                seg2Vz = vz3 - vz4;
    
                                close2node3 = (((seg2Lx*seg2Lx +
                                                 seg2Ly*seg2Ly +
                                                 seg2Lz*seg2Lz) *
                                                (L2 * L2)) < mindist2);
    
                                close2node4 = (((seg2Lx*seg2Lx +
                                                 seg2Ly*seg2Ly +
                                                 seg2Lz*seg2Lz) *
                                                ((1-L2) * (1-L2))) < mindist2);
    
                                if ((node4->myTag.domainID != thisDomain) &&
                                    close2node4) {
                                    continue;
                                }

                                if (close2node3) {
                                    mergenode2 = node3;
                                    splitSeg2 = 0;
                                } else if (close2node4) {
                                    mergenode2 = node4;
                                    splitSeg2 = 0;
                                } else {
                                    undeletable2 = 0;
                                    splitSeg2 = 1;
                                }
    
/* 
 * If both nodes are pinned do not allow a new point to be inserted in the segment.
 * Choose one end instead
 */  
				/*
				if (node3->constraint == 7 && node4->constraint == 7)
				  {
				    real8 isclose3, isclose4;
				    isclose3 = (seg2Lx*seg2Lx + seg2Ly*seg2Ly + seg2Lz*seg2Lz) * (L2 * L2);
				    isclose4 = (seg2Lx*seg2Lx + seg2Ly*seg2Ly + seg2Lz*seg2Lz) * ((1-L2) * (1-L2));
				    if ( isclose3 < isclose4 ) 
				      {
					mergenode2 = node3;
					splitSeg2 = 0;
				      }
				    else
				      {
					mergenode2 = node4;
					splitSeg2 = 0;
				      }
				  }
				*/


/*
 *                              If we need to add a new node to the first
 *                              segment, do it now.
 */
                                if (splitSeg1) {

                                     newx = x1 * (1.0-L1) + x2*L1;
                                     newy = y1 * (1.0-L1) + y2*L1;
                                     newz = z1 * (1.0-L1) + z2*L1;
    
                                     newvx = vx1 * (1.0-L1) + vx2*L1;
                                     newvy = vy1 * (1.0-L1) + vy2*L1;
                                     newvz = vz1 * (1.0-L1) + vz2*L1;
    
/*
 *                                   Estimate resulting forces on all segments
 *                                   involved in the split.
 */
                                     arm12 = GetArmID(home, node1, node2);
                                     arm21 = GetArmID(home, node2, node1);
    
                                     burg1[X] = node1->burgX[arm12];
                                     burg1[Y] = node1->burgY[arm12];
                                     burg1[Z] = node1->burgZ[arm12];
    
                                     oldfp0[X] = node1->armfx[arm12];
                                     oldfp0[Y] = node1->armfy[arm12];
                                     oldfp0[Z] = node1->armfz[arm12];
    
                                     oldfp1[X] = node2->armfx[arm21];
                                     oldfp1[Y] = node2->armfy[arm21];
                                     oldfp1[Z] = node2->armfz[arm21];
    
                                     p0[X] = x1;   p1[X] = x2;
                                     p0[Y] = y1;   p1[Y] = y2;
                                     p0[Z] = z1;   p1[Z] = z2;
    
                                     pnew[X] = newx;
                                     pnew[Y] = newy;
                                     pnew[Z] = newz;
    
                                     newNodeVel[X] = newvx;
                                     newNodeVel[Y] = newvy;
                                     newNodeVel[Z] = newvz;

                                     nodeVel[X] = vx1;
                                     nodeVel[Y] = vy1;
                                     nodeVel[Z] = vz1;

                                     FindSubFSeg(home, p0, p1, burg1, oldfp0,
                                                 oldfp1, pnew, f0seg1, f1seg1,
                                                 f0seg2, f1seg2);
    
                                     oldTag1 = node1->myTag;
                                     oldTag2 = node2->myTag;

                                     FoldBox(param, &pnew[X], &pnew[Y], &pnew[Z]);

                                     splitStatus = SplitNode(home,
                                                             OPCLASS_COLLISION,
                                                             node1, p0, pnew,
                                                             nodeVel,
                                                             newNodeVel, 1,
                                                             &arm12, globalOp,
                                                             &splitNode1,
                                                             &splitNode2);
/*
 *                                   If we were unable to split the node
 *                                   go back to looking for more collision
 *                                   candidates.
 */
                                     if (splitStatus == SPLIT_FAILED) {
                                         continue;
                                     }

/*
 *                                   The force estimates above are good enough
 *                                   for the remainder of this timestep, but
 *                                   mark the force and velocity data for
 *                                   some nodes as obsolete so that more
 *                                   accurate forces will be recalculated
 *                                   either at the end of this timestep, or
 *                                   the beginning of the next.
 */
                                     mergenode1 = splitNode2;

                                     MarkNodeForceObsolete(home, splitNode2);
    
                                     for (q = 0; q < splitNode2->numNbrs; q++) {
                                         tmpNbr = GetNodeFromTag(home, splitNode2->nbrTag[q]);
                                         if (tmpNbr != (Node_t *)NULL) {
                                             tmpNbr->flags |= NODE_RESET_FORCES;
                                         }
                                     }
    
/*
 *                                   Reset nodal forces on nodes involved in the
 *                                   split.
 */
                                     ResetSegForces(home, splitNode1,
                                                    &splitNode2->myTag,
                                                    f0seg1[X], f0seg1[Y],
                                                    f0seg1[Z], 1);
    
                                     ResetSegForces(home, splitNode2,
                                                    &splitNode1->myTag,
                                                    f1seg1[X], f1seg1[Y],
                                                    f1seg1[Z], 1);
    
                                     ResetSegForces(home, splitNode2,
                                                    &node2->myTag,
                                                    f0seg2[X], f0seg2[Y],
                                                    f0seg2[Z], 1);
    
                                     ResetSegForces(home, node2,
                                                    &splitNode2->myTag,
                                                    f1seg2[X], f1seg2[Y],
                                                    f1seg2[Z], 1);
    
                                     (void)EvaluateMobility(home, splitNode1);
                                     (void)EvaluateMobility(home, splitNode2);
                                     (void)EvaluateMobility(home, node2);
    
/*
 *                                  When debugging, dump some info on
 *                                  topological changes taking place and
 *                                  the nodes involved
 */
#ifdef DEBUG_TOPOLOGY_CHANGES
                                    if ((dbgDom < 0)||(dbgDom == home->myDomain)) {
                                        printf("  Split-1(SegCollision): "
                                               "(%d,%d)--(%d,%d) ==> "
                                               "(%d,%d)--(%d,%d)--(%d,%d)\n",
                                               oldTag1.domainID, oldTag1.index,
                                               oldTag2.domainID, oldTag2.index,
                                               splitNode1->myTag.domainID,
                                               splitNode1->myTag.index,
                                               splitNode2->myTag.domainID,
                                               splitNode2->myTag.index,
                                               node2->myTag.domainID,
                                               node2->myTag.index);
                                        PrintNode(splitNode1);
                                        PrintNode(splitNode2);
                                        PrintNode(node2);
                                     }
#endif
                                }  /* if (splitSeg1) */
    
/*
 *                              If we need to add a new node to the second
 *                              segment, do it now.
 */
                                if (splitSeg2) {

                                     newx = x3 * (1.0-L2) + x4*L2;
                                     newy = y3 * (1.0-L2) + y4*L2;
                                     newz = z3 * (1.0-L2) + z4*L2;
    
                                     newvx = vx3 * (1.0-L2) + vx4*L2;
                                     newvy = vy3 * (1.0-L2) + vy4*L2;
                                     newvz = vz3 * (1.0-L2) + vz4*L2;
    
/*
 *                                   Estimate resulting forces on all segments
 *                                   involved in the split.
 */
                                     arm43 = GetArmID(home, node4, node3);
    
                                     burg1[X] = node3->burgX[arm34];
                                     burg1[Y] = node3->burgY[arm34];
                                     burg1[Z] = node3->burgZ[arm34];
    
                                     oldfp0[X] = node3->armfx[arm34];
                                     oldfp0[Y] = node3->armfy[arm34];
                                     oldfp0[Z] = node3->armfz[arm34];
    
                                     oldfp1[X] = node4->armfx[arm43];
                                     oldfp1[Y] = node4->armfy[arm43];
                                     oldfp1[Z] = node4->armfz[arm43];
    
                                     p0[X] = x3;   p1[X] = x4;
                                     p0[Y] = y3;   p1[Y] = y4;
                                     p0[Z] = z3;   p1[Z] = z4;
    
                                     pnew[X] = newx;
                                     pnew[Y] = newy;
                                     pnew[Z] = newz;
    
                                     newNodeVel[X] = newvx;
                                     newNodeVel[Y] = newvy;
                                     newNodeVel[Z] = newvz;

                                     nodeVel[X] = vx3;
                                     nodeVel[Y] = vy3;
                                     nodeVel[Z] = vz3;

                                     FindSubFSeg(home, p0, p1, burg1, oldfp0,
                                                 oldfp1, pnew, f0seg1, f1seg1,
                                                 f0seg2, f1seg2);
    
                                     oldTag1 = node3->myTag;
                                     oldTag2 = node4->myTag;

                                     FoldBox(param, &pnew[X], &pnew[Y], &pnew[Z]);

                                     splitStatus = SplitNode(home,
                                                            OPCLASS_COLLISION,
                                                            node3, p0,
                                                            pnew, nodeVel,
                                                            newNodeVel, 1,
                                                            &arm34, globalOp,
                                                            &splitNode1,
                                                            &splitNode2);
/*
 *                                   If we were unable to split the node
 *                                   go back to looking for more collision
 *                                   candidates.
 */
                                     if (splitStatus == SPLIT_FAILED) {
                                         continue;
                                     }
/*
 *                                   The force estimates above are good enough
 *                                   for the remainder of this timestep, but
 *                                   mark the force and velocity data for some
 *                                   nodes as obsolete so that more accurate
 *                                   forces will be recalculated either at the
 *                                   end of this timestep, or the beginning of
 *                                   the next.
 */
                                     mergenode2 = splitNode2;

                                     MarkNodeForceObsolete(home, splitNode2);
    
                                     for (q = 0; q < splitNode2->numNbrs; q++) {
                                         tmpNbr = GetNodeFromTag(home, splitNode2->nbrTag[q]);
                                         if (tmpNbr != (Node_t *)NULL) {
                                             tmpNbr->flags |= NODE_RESET_FORCES;
                                         }
                                     }
    
/*
 *                                   Reset nodal forces on nodes involved in the
 *                                   split.
 */
                                     ResetSegForces(home, splitNode1,
                                                    &splitNode2->myTag,
                                                    f0seg1[X], f0seg1[Y],
                                                    f0seg1[Z], 1);
    
                                     ResetSegForces(home, splitNode2,
                                                    &splitNode1->myTag,
                                                    f1seg1[X], f1seg1[Y],
                                                    f1seg1[Z], 1);
    
                                     ResetSegForces(home, splitNode2,
                                                    &node4->myTag,
                                                    f0seg2[X], f0seg2[Y],
                                                    f0seg2[Z], 1);
    
                                     ResetSegForces(home, node4,
                                                    &splitNode2->myTag,
                                                    f1seg2[X], f1seg2[Y],
                                                    f1seg2[Z], 1);
 
                                     (void)EvaluateMobility(home, splitNode1);
                                     (void)EvaluateMobility(home, splitNode2);
                                     (void)EvaluateMobility(home, node4);

/*
 *                                   When debugging, dump some info on
 *                                   topological changes taking place and
 *                                   the nodes involved
 */
#ifdef DEBUG_TOPOLOGY_CHANGES
                                     if ((dbgDom < 0) ||
                                         (dbgDom == home->myDomain)) {
                                         printf("  Split-2(SegCollision): "
                                               "(%d,%d)--(%d,%d) ==> "
                                               "(%d,%d)--(%d,%d)--(%d,%d)\n",
                                               oldTag1.domainID,
                                               oldTag1.index,
                                               oldTag2.domainID,
                                               oldTag2.index,
                                               splitNode1->myTag.domainID,
                                               splitNode1->myTag.index,
                                               splitNode2->myTag.domainID,
                                               splitNode2->myTag.index,
                                               node4->myTag.domainID,
                                               node4->myTag.index);
                                         PrintNode(splitNode1);
                                         PrintNode(splitNode2);
                                         PrintNode(node4);
                                    }
#endif
                                }  /* if (splitSeg2) */
    
    
/*
 *                             Select the physical position at which to place
 *                             the node resulting from the merge of the
 *                             two nodes.
 */
                               FindCollisionPoint(home, mergenode1, mergenode2,
                                                  &newPos[X], &newPos[Y],
                                                  &newPos[Z]);
    
                               FoldBox(param, &newPos[X],&newPos[Y],&newPos[Z]);
    
                               newVel[X] = half * (mergenode1->vX +
                                                   mergenode2->vX);
                               newVel[Y] = half * (mergenode1->vY +
                                                   mergenode2->vY);
                               newVel[Z] = half * (mergenode1->vZ +
                                                   mergenode2->vZ);
    
    
                               oldTag1 = mergenode1->myTag;
                               oldTag2 = mergenode2->myTag;

                               MergeNode(home, OPCLASS_COLLISION, mergenode1,
                                         mergenode2, newPos, &targetNode,
                                         &mergeStatus, globalOp);
    
/*
 *                             If the merge did not succeed, go back and
 *                             continue looking for collision candidates.
 */
                               if ((mergeStatus & MERGE_SUCCESS) == 0) {
                                   continue;
                               }
/*
 *                             When debugging, dump some info on topological
 *                             changes taking place and the nodes involved
 */
#ifdef DEBUG_TOPOLOGY_CHANGES
                               if ((dbgDom < 0) || (dbgDom == home->myDomain)) {
                                   printf("  Merge(SegCollision): "
                                          "(%d,%d) and (%d,%d) at (%d,%d)\n",
                                          oldTag1.domainID, oldTag1.index,
                                          oldTag2.domainID, oldTag2.index,
                                          targetNode->myTag.domainID,
                                          targetNode->myTag.index);
                                   PrintNode(targetNode);
                               }
#endif
/*
 *                             If the target node exists after the merge,
 *                             reset its velocity, and reset the topological
 *                             change exemptions for the target node; use
 *                             NodeTopologyExemptions() to get the basic
 *                             exemptions, then exempt all the node's arms
 *                             from additional segment collisions this cycle.
 */
                               if (targetNode != (Node_t *)NULL) {
/*
 *                                 Estimate velocity so mobility function
 *                                 has a reasonable starting point
 */
                                   targetNode->vX = newVel[X];
                                   targetNode->vY = newVel[Y];
                                   targetNode->vZ = newVel[Z];
    
                                   (void)EvaluateMobility(home, targetNode);

                                   targetNode->flags |= NO_COLLISIONS;
#ifdef DEBUG_LOG_MULTI_NODE_SPLITS
                                   targetNode->multiNodeLife = 0;
#endif
                               }
    
                               didCollision = 1;
                               localCollisionCnt++;

                            }  /* If conditions for collision are met */
                        }  /* Loop over node3 arms */
                    }  /* Loop over node1 arms */
                }  /* while(nextIndex >= 0) */
            }  /* Loop over neighboring cell2s */
           }
          }
        }  /* for (i = 0 ...) */

/*
 *      Now we have to loop for collisions on hinge joints (i.e zipping)
 */
        for (i = 0; i < home->newNodeKeyPtr; i++) {

            if ((node1 = home->nodeKeys[i]) == (Node_t *)NULL) continue;
            if (node1->flags & NO_COLLISIONS) continue;

            for (j = 0; j < node1->numNbrs; j++) {

                if (node1->myTag.domainID != node1->nbrTag[j].domainID)
                    continue;

                for (k = j + 1; k < node1->numNbrs; k++) {

                    if (node1->myTag.domainID != node1->nbrTag[k].domainID)
                        continue;

                    node3 = GetNodeFromTag(home, node1->nbrTag[j]);
                    node4 = GetNodeFromTag(home, node1->nbrTag[k]);

                    x1 = node1->x; y1 = node1->y; z1 = node1->z;
                    x3 = node3->x; y3 = node3->y; z3 = node3->z;
                    x4 = node4->x; y4 = node4->y; z4 = node4->z;

                    vx1 = node1->vX; vy1 = node1->vY; vz1 = node1->vZ;
                    vx3 = node3->vX; vy3 = node3->vY; vz3 = node3->vZ;
                    vx4 = node4->vX; vy4 = node4->vY; vz4 = node4->vZ;

                    PBCPOSITION(param, x1, y1, z1, &x3, &y3, &z3);
                    PBCPOSITION(param, x3, y3, z3, &x4, &y4, &z4);

                    x2  = x1;  y2  = y1;  z2  = z1;
                    vx2 = vx1; vy2 = vy1; vz2 = vz1;

/*
 *                  Find the minimum distance between the two segments
 *                  and determine if they should be collided.
 */
                    GetMinDist(x1, y1, z1, vx1, vy1, vz1,
                               x3, y3, z3, vx3, vy3, vz3,
                               x2, y2, z2, vx2, vy2, vz2,
                               x4, y4, z4, vx4, vy4, vz4,
                               &dist2, &ddist2dt, &L1, &L2);

                    if ((dist2 < mindist2) && (ddist2dt < -eps)) {

                        mergenode1 = node1;

                        seg2Lx = x3 - x4;
                        seg2Ly = y3 - y4;
                        seg2Lz = z3 - z4;
                        
                        seg2Vx = vx3 - vx4;
                        seg2Vy = vy3 - vy4;
                        seg2Vz = vz3 - vz4;

/*
 *                      If the collision point is close to one of the
 *                      existing nodes on segment 2, go ahead and use
 *                      the existing node, otherwise insert a new node
 *                      into the segment.
 */
                        close2node3 = (((seg2Lx*seg2Lx + seg2Vx*seg2Vx +
                                         seg2Ly*seg2Ly + seg2Vy*seg2Vy +
                                         seg2Lz*seg2Lz + seg2Vz*seg2Vz) *
                                        (L2 * L2)) < mindist2);

                        close2node4 = (((seg2Lx*seg2Lx + seg2Vx*seg2Vx +
                                         seg2Ly*seg2Ly + seg2Vy*seg2Vy +
                                         seg2Lz*seg2Lz + seg2Vz*seg2Vz) *
                                        ((1-L2) * (1-L2))) < mindist2);

/*
 *                      If the collision point is close to one of the
 *                      endpoints of the node3/node4 segment, but the
 *                      node in question is not owned by the current
 *                      domain, skip the collision this time around.
 */
                        if ((close2node3&&(node3->myTag.domainID!=thisDomain))||
                            (close2node4&&(node4->myTag.domainID!=thisDomain))){
                            continue;
                        }

                        if (close2node3) {
                             mergenode2 = node3;
                        } else if (close2node4) {
                             mergenode2 = node4;
                        } else { 
                             newPos[X] = x3*(1.0-L2) + x4*L2;
                             newPos[Y] = y3*(1.0-L2) + y4*L2;
                             newPos[Z] = z3*(1.0-L2) + z4*L2;

                             newVel[X] = vx3*(1.0-L2) + vx4*L2;
                             newVel[Y] = vy3*(1.0-L2) + vy4*L2;
                             newVel[Z] = vz3*(1.0-L2) + vz4*L2;

                             nodeVel[X] = node1->vX;
                             nodeVel[Y] = node1->vY;
                             nodeVel[Z] = node1->vZ;
/*
 *                           Estimate resulting forces on all segments
 *                           involved in the split.
 */
                             armAB = GetArmID(home, node1, node3);
                             armBA = GetArmID(home, node3, node1);

                             burg1[X] = node1->burgX[armAB];
                             burg1[Y] = node1->burgY[armAB];
                             burg1[Z] = node1->burgZ[armAB];

                             oldfp0[X] = node1->armfx[armAB];
                             oldfp0[Y] = node1->armfy[armAB];
                             oldfp0[Z] = node1->armfz[armAB];

                             oldfp1[X] = node3->armfx[armBA];
                             oldfp1[Y] = node3->armfy[armBA];
                             oldfp1[Z] = node3->armfz[armBA];

                             p0[X] = x1;   p1[X] = x3;
                             p0[Y] = y1;   p1[Y] = y3;
                             p0[Z] = z1;   p1[Z] = z3;

                             FindSubFSeg(home, p0, p1, burg1, oldfp0,
                                         oldfp1, newPos, f0seg1, f1seg1,
                                         f0seg2, f1seg2);

/*
 *                           Attempt to split the segment.  If the split
 *                           fails, we can't continue with this collision.
 */
                             oldTag1 = node1->myTag;
                             oldTag2 = node3->myTag;

                             FoldBox(param, &newPos[X], &newPos[Y], &newPos[Z]);

                             splitStatus = SplitNode(home, OPCLASS_COLLISION,
                                                     node1, p0, newPos,
                                                     nodeVel, newVel, 1,
                                                     &j, globalOp,
                                                     &splitNode1, &splitNode2);

                             if (splitStatus != SPLIT_SUCCESS) {
                                 continue;
                             }

/*
 *                           The force estimates above are good enough for
 *                           the remainder of this timestep, but mark the
 *                           force and velocity data for some nodes as obsolete
 *                           so that more accurate forces will be recalculated
 *                           either at the end of this timestep, or the
 *                           beginning of the next.
 */
                             mergenode2 = splitNode2;

                             MarkNodeForceObsolete(home, splitNode1);
                             MarkNodeForceObsolete(home, splitNode2);
                             MarkNodeForceObsolete(home, node3);

/*
 *                           Reset nodal forces for nodes involved in the
 *                           split.
 */
                             ResetSegForces(home,splitNode1,&splitNode2->myTag,
                                            f0seg1[X], f0seg1[Y], f0seg1[Z], 1);

                             ResetSegForces(home,splitNode2,&splitNode1->myTag,
                                            f1seg1[X], f1seg1[Y], f1seg1[Z], 1);

                             ResetSegForces(home, splitNode2, &node3->myTag,
                                            f0seg2[X], f0seg2[Y], f0seg2[Z], 1);

                             ResetSegForces(home, node3, &splitNode2->myTag,
                                            f1seg2[X], f1seg2[Y], f1seg2[Z], 1);

                             (void)EvaluateMobility(home, splitNode1);
                             (void)EvaluateMobility(home, splitNode2);
                             (void)EvaluateMobility(home, node3);

/*
 *                           When debugging, dump some info on topological
 *                           changes taking place and the nodes involved
 */
#ifdef DEBUG_TOPOLOGY_CHANGES
                             if ((dbgDom < 0)||(dbgDom == home->myDomain)) {
                                 printf("  Split-1(Hinge): "
                                        "(%d,%d)--(%d,%d) ==> "
                                        "(%d,%d)--(%d,%d)--(%d,%d)\n",
                                        oldTag1.domainID, oldTag1.index,
                                        oldTag2.domainID, oldTag2.index,
                                        splitNode1->myTag.domainID,
                                        splitNode1->myTag.index,
                                        splitNode2->myTag.domainID,
                                        splitNode2->myTag.index,
                                        node3->myTag.domainID,
                                        node3->myTag.index);
                                 PrintNode(splitNode1);
                                 PrintNode(splitNode2);
                                 PrintNode(node3);
                             }
#endif
                        }

/*
 *                      Try the collision
 */
                        FindCollisionPoint(home, mergenode1, mergenode2,
                                           &newPos[X], &newPos[Y], &newPos[Z]);

                        FoldBox(param, &newPos[X], &newPos[Y], &newPos[Z]);

                        newVel[X] = (mergenode1->vX+mergenode2->vX)*half;
                        newVel[Y] = (mergenode1->vY+mergenode2->vY)*half;
                        newVel[Z] = (mergenode1->vZ+mergenode2->vZ)*half;

/*
 *                      The merge is going to happen, so there's a few
 *                      more nodes we'll need to mark for force/velocity
 *                      re-evaluation.
 */
                        MarkNodeForceObsolete(home, node1);
                        MarkNodeForceObsolete(home, node3);
                        MarkNodeForceObsolete(home, node4);

                        for (q = 0; q < node1->numNbrs; q++) {
                            tmpNbr = GetNodeFromTag(home, node1->nbrTag[q]);
                            if (tmpNbr == (Node_t *)NULL) continue;
                            tmpNbr->flags |= NODE_RESET_FORCES;
                        }

                        for (q = 0; q < node3->numNbrs; q++) {
                            tmpNbr = GetNodeFromTag(home, node3->nbrTag[q]);
                            if (tmpNbr == (Node_t *)NULL) continue;
                            tmpNbr->flags |= NODE_RESET_FORCES;
                        }

                        for (q = 0; q < node4->numNbrs; q++) {
                            tmpNbr = GetNodeFromTag(home, node4->nbrTag[q]);
                            if (tmpNbr == (Node_t *)NULL) continue;
                            tmpNbr->flags |= NODE_RESET_FORCES;
                        }

                        oldTag1 = mergenode1->myTag;
                        oldTag2 = mergenode2->myTag;

                        MergeNode(home, OPCLASS_COLLISION, mergenode1,
                                  mergenode2, newPos, &targetNode,
                                  &mergeStatus, globalOp);

                        localCollisionCnt++;
/*
 *                      If the target node exists after the merge, reevaulate
 *                      the node velocity, mark the forces as obsolete,
 *                      and reset the topological change exemptions for
 *                      the target node and exempt all the node's arms
 *                      from subsequent segment collisions this cycle.
 */
                        if (targetNode != (Node_t *)NULL) {

                            (void)EvaluateMobility(home, targetNode);

                            targetNode->flags |= NODE_RESET_FORCES;
                            targetNode->flags |= NO_COLLISIONS;

#ifdef DEBUG_TOPOLOGY_CHANGES
                            if ((dbgDom < 0) || (dbgDom == home->myDomain)) {
                                printf("  Merge(SegCollision): "
                                       "(%d,%d) and (%d,%d) at (%d,%d)\n",
                                       oldTag1.domainID, oldTag1.index,
                                       oldTag2.domainID, oldTag2.index,
                                       targetNode->myTag.domainID,
                                       targetNode->myTag.index);
                                PrintNode(targetNode);
                            }
#endif
#ifdef DEBUG_LOG_MULTI_NODE_SPLITS
                            targetNode->multiNodeLife = 0;
#endif
                        }

                    }  /* conditions for zip were met */
                }  /* for (k = 0...) */
            }  /* for (j = 0...) */
        }  /* for (i = 0...) */


#ifdef DEBUG_LOG_COLLISIONS
#ifdef PARALLEL
        MPI_Reduce(&localCollisionCnt, &globalCollisionCnt, 1, MPI_INT, MPI_SUM,
                   0, MPI_COMM_WORLD);
#else
        globalCollisionCnt = localCollisionCnt;
#endif
        if (home->myDomain == 0) {
            printf("  Collision count = %d\n", globalCollisionCnt);
        }
#endif

        TimerStop(home, COLLISION_HANDLING);

        return;
}
