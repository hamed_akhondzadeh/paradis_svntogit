#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "Typedefs.h"
#include "Home.h"

#include "Param.h"
#include "ANI.h"

#define real8 double

#define MaxIter 1000000

int quadv(real8 *Q, int *fcnt, 
          void (*funfcn)(real8, real8*, real8*),
	  real8 a, real8 b, real8 tol, 
          real8 *param); 

int quadstep(real8 *Q, void (*funfcn)(real8, real8*, real8*),
             real8 a, real8 b,
             real8 *fa, real8 *fc, real8 *fb,
             real8 tol, int *fcnt, real8 hmin, real8 *param);


void SegSegForce_ANI(real8 p1x, real8 p1y, real8 p1z,
		     real8 p2x, real8 p2y, real8 p2z,
		     real8 p3x, real8 p3y, real8 p3z,
		     real8 p4x, real8 p4y, real8 p4z,
		     real8 bpx, real8 bpy, real8 bpz,
		     real8 bx, real8 by, real8 bz,
		     real8 a, real8 MU, real8 NU,
		     int seg12Local, int seg34Local,
		     real8 *fp1x, real8 *fp1y, real8 *fp1z,
		     real8 *fp2x, real8 *fp2y, real8 *fp2z,
		     real8 *fp3x, real8 *fp3y, real8 *fp3z,
		     real8 *fp4x, real8 *fp4y, real8 *fp4z);

/* define global variable */
Aniso_t *aniso0;

int main(void) {
  
  ANI_Alloc(&aniso0);
  
  real8 C11,C12, C44,Cpr;
  C12 = 20.1;
  C44 = 16.0001;
  Cpr = 16.0;
  C11 = 2*Cpr + C12;
  ANI_Assign_Cubic_Constants(aniso0, C12, C44, Cpr);


  int seg12Local, seg34Local;
  real8 p1x, p1y, p1z;
  real8 p2x, p2y, p2z;
  real8 p3x, p3y, p3z;
  real8 p4x, p4y, p4z;
  real8 bx, by, bz,a, MU, NU;
  real8 f1[3],f2[3];
  real8 b[3],t[3],n[3],m[3],L;
  real8 Qp[3][3], Sp[3][3], Bp[3][3];
  int coreOnly;
  real8 Ecore;

  p1x = 0; p1y = 2; p1z = 4;
  p2x = 1; p2y = 1; p2z = 3;
  bx  = 1; by  = 1; bz  = 1;

  b[0]=bx; b[1]=by; b[2]=bz;
  
  a = 0.1;
  MU = 16.0001;
  NU = C12/(C11+C12);
  
  coreOnly = 1;
  Ecore = 0;
  
  /* Get direction t */
  GetUnitVector(1, p1x, p1y, p1z, p2x, p2y, p2z, &t[0], &t[1], &t[2], &L);
  
  printf("t  = [ %g  %g  %g ];\n",t[0],t[1],t[2]);
  
  rhcoord(t,n,m);
  printf("n  = [ %g  %g  %g ];\n\n\n",n[0],n[1],n[2]);

  /* Call QSB_prime_get */
  QSBprime_get(t,b,n,Qp,Sp,Bp);
  printf("Qp = [ %22.14e  %22.14e  %22.14e ];\n",    Qp[0][0],Qp[1][0],Qp[2][0]);
  printf("     [ %22.14e  %22.14e  %22.14e ];\n",    Qp[0][1],Qp[1][1],Qp[2][1]);
  printf("     [ %22.14e  %22.14e  %22.14e ];\n\n",  Qp[0][2],Qp[1][2],Qp[2][2]);
  
  printf("Sp = [ %22.14e  %22.14e  %22.14e ];\n",    Sp[0][0],Sp[1][0],Sp[2][0]);
  printf("     [ %22.14e  %22.14e  %22.14e ];\n",    Sp[0][1],Sp[1][1],Sp[2][1]);
  printf("     [ %22.14e  %22.14e  %22.14e ];\n\n",  Sp[0][2],Sp[1][2],Sp[2][2]);

  printf("Bp = [ %22.14e  %22.14e  %22.14e ];\n",    Bp[0][0],Bp[1][0],Bp[2][0]);
  printf("     [ %22.14e  %22.14e  %22.14e ];\n",    Bp[0][1],Bp[1][1],Bp[2][1]);
  printf("     [ %22.14e  %22.14e  %22.14e ];\n\n\n",Bp[0][2],Bp[1][2],Bp[2][2]);

  /* Call SelForce function */
  SelfForce_ANI(coreOnly, MU, NU, bx, by, bz,
		p1x,p1y,p1z,p2x,p2y,p2z,
		a,Ecore,f1,f2);  

  printf("Forces from Anisotropic SelfForce\n\n");
  printf("f1 = [ %22.14e  %22.14e  %22.14e ];\n",f1[0],f1[1],f1[2]);
  printf("f2 = [ %22.14e  %22.14e  %22.14e ];\n\n\n",f2[0],f2[1],f2[2]);


  printf("MATLAB results:\n");
  printf("f1 = [  5.74803123004749e-01   2.87401561502375e-01   2.87401561502374e-01 ]\n");
  printf("f2 = [ -5.74803123004749e-01  -2.87401561502375e-01  -2.87401561502374e-01 ]\n");

}
  


void ANI_Alloc(Aniso_t **ani_ptr)
{
   Aniso_t *aniso;
   aniso = (Aniso_t *) calloc(1, sizeof(Aniso_t));
   *ani_ptr = aniso;
}

void ANI_Assign_Cubic_Constants(Aniso_t *aniso, real8 C12, real8 C44, real8 Cpr)
{
   int i,j,k,l;

   aniso->C12 = C12;
   aniso->C44 = C44;
   aniso->Cpr = Cpr;
   
   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
     {
        aniso->delta[i][j] = 0.0;
        for (k = 0; k < 3; k++)
	{
           aniso->eps[i][j][k] = 0.0;
	   for (l = 0; l < 3; l++)
	   {
	      aniso->C[i][j][k][l] = 0.0;
           }
	}
     }

   aniso->delta[0][0] = 1.0;
   aniso->delta[1][1] = 1.0;
   aniso->delta[2][2] = 1.0;

   aniso->eps[0][1][2] = 1.0;
   aniso->eps[1][2][0] = 1.0;
   aniso->eps[2][0][1] = 1.0;

   aniso->eps[2][1][0] =-1.0;
   aniso->eps[1][0][2] =-1.0;
   aniso->eps[0][2][1] =-1.0;

   /* C : elastic constant tensor */
   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
       for (k = 0; k < 3; k++)
         for (l = 0; l < 3; l++)
         {
	   aniso->C[i][j][k][l] += 
	     C12*aniso->delta[i][j]*aniso->delta[k][l]
	   + C44*(aniso->delta[i][k]*aniso->delta[j][l] 
	   + aniso->delta[i][l]*aniso->delta[j][k])

	   /* following line vanishes in isotropic approximation */
	   + 2*(Cpr - C44)*aniso->delta[i][j]*aniso->delta[k][l]*aniso->delta[i][k];
	}

}



/* 

This test's file results

r1 = [ 0  2  4 ];
r2 = [ 1  1  3 ];
b  = [ 1  1  1 ];
t  = [ 0.57735  -0.57735  -0.57735 ];
n  = [ -0.795794  -0.239677  -0.556117 ];


Qp = [   4.56790890099255e-03    7.47369179082991e-03   -9.75765451102627e-03 ];
     [   7.47369179082991e-03   -1.95152383616380e-02   -2.28396272019635e-03 ];
     [  -9.75765451102627e-03   -2.28396272019635e-03    1.49473294606455e-02 ];

Sp = [  -7.57444547234056e-08    1.83594861289700e-01    2.39701323858214e-01 ];
     [  -1.83594429845327e-01    2.28126805377637e-08   -5.61062766234408e-02 ];
     [  -2.39701137913140e-01    5.61068940128877e-02    5.29317741332690e-08 ];

Bp = [   1.30219928195539e+00    2.13057400123209e+00   -2.78167813124242e+00 ];
     [   2.13057400123209e+00   -5.56331790596601e+00   -6.51104130010331e-01 ];
     [  -2.78167813124242e+00   -6.51104130010331e-01    4.26111862401061e+00 ];


t = 5.773502691896257e-01 -5.773502691896258e-01 -5.773502691896258e-01
n = -7.957942583376840e-01 -2.396769541038979e-01 -5.561173042337860e-01
m = -1.826969213299504e-01 -7.805265045712059e-01 5.978295832412556e-01
Forces from Anisotropic SelfForce

f1 = [   5.74803123004750e-01    2.87401561502375e-01    2.87401561502375e-01 ];
f2 = [  -5.74803123004750e-01   -2.87401561502375e-01   -2.87401561502375e-01 ];


Matlab's results

t = [  5.77350269189626e-01  -5.77350269189626e-01  -5.77350269189626e-01 ];
n = [ -7.95794000000000e-01  -2.39677000000000e-01  -5.56117000000000e-01 ];


Qp = [   4.56790550844632e-03    7.47369297622442e-03   -9.75765400014152e-03 ];
     [   7.47369297622442e-03   -1.95152373398722e-02   -2.28396102391709e-03 ];
     [  -9.75765400014152e-03   -2.28396102391709e-03    1.49473318314259e-02 ];

Sp = [  -7.57444576187882e-08    1.83594890409417e-01    2.39701311308149e-01 ];
     [  -1.83594458965123e-01    2.28126931900685e-08   -5.61062349535556e-02 ];
     [  -2.39701125362972e-01    5.61068523430265e-02    5.29317644198848e-08 ];

Bp = [   1.30219831482330e+00    2.13057433916022e+00   -2.78167798560118e+00 ];
     [   2.13057433916022e+00   -5.56331761468552e+00   -6.51103646440954e-01 ];
     [  -2.78167798560118e+00   -6.51103646440954e-01    4.26111929986222e+00 ];


f1 = [  5.74803123004749e-01   2.87401561502375e-01   2.87401561502374e-01 ];
f2 = [ -5.74803123004749e-01  -2.87401561502375e-01  -2.87401561502374e-01 ];

*/
