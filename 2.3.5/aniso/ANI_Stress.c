#include <stdio.h>
#include <math.h>


#ifdef _ANISOTROPIC

#include "ANI.h"

void EvalWS(real8 s, real8 *f, real8 *param)
{
  int i;
  real8 a;
  real8 r1[3], r2[3], r3[3], r4[3];
  real8 b12[3], b34[3];
  real8 P[3];
  real8 stress34[3][3],sigb[3];
  real8 r12[3];
  real8 ft[3];

  r1[0] = param[0];
  r1[1] = param[1];
  r1[2] = param[2];

  r2[0] = param[3];
  r2[1] = param[4];
  r2[2] = param[5];

  r3[0] = param[6];
  r3[1] = param[7];
  r3[2] = param[8];

  r4[0] = param[9];
  r4[1] = param[10];
  r4[2] = param[11];

  b12[0] = param[12];
  b12[1] = param[13];
  b12[2] = param[14];

  b34[0] = param[15];
  b34[1] = param[16];
  b34[2] = param[17];

  a = param[18];

  r12[0] = r2[0] - r1[0];
  r12[1] = r2[1] - r1[1];
  r12[2] = r2[2] - r1[2];

  P[0] = r1[0] + r12[0] * s;
  P[1] = r1[1] + r12[1] * s;
  P[2] = r1[2] + r12[2] * s;

  ANI_WillisSteeds(r3,r4,P,b34,stress34,a);

  sigb[0] = stress34[0][0]*b12[0] +
            stress34[0][1]*b12[1] +
            stress34[0][2]*b12[2];
  sigb[1] = stress34[1][0]*b12[0] +
            stress34[1][1]*b12[1] +
            stress34[1][2]*b12[2];
  sigb[2] = stress34[2][0]*b12[0] +
            stress34[2][1]*b12[1] +
            stress34[2][2]*b12[2];

  cross(sigb, r12, ft);

  for(i = 0; i < 3; i++)
    {
      f[i]    = ft[i]*(1.0-s);
      f[i+3]  = ft[i]*s;
    }
}


void ANI_WillisSteeds(real8 A[3], real8 B[3], real8 P[3], real8 b[3],
		      real8 sigma[][3],real8 a)
{
  int i,j;
  real8 xi[3], D[3], PD[3];
  real8 d, DotAPxi;
  real8 eps = 1e-6;

  Init3x3(sigma);

  // Cut-off algorithm
  xi[0] = B[0]-A[0];
  xi[1] = B[1]-A[1];
  xi[2] = B[2]-A[2]; 
  Normalize2(xi);
 
  DotAPxi = Dot(A,P,xi);
  D[0] = DotAPxi * xi[0] + A[0];
  D[1] = DotAPxi * xi[1] + A[1];
  D[2] = DotAPxi * xi[2] + A[2];

  PD[0] = D[0]-P[0];
  PD[1] = D[1]-P[1];
  PD[2] = D[2]-P[2]; 
  d = Normal(PD);

  if (fabs(d-a)<=1e-6) d = a;

  if (d >= a) 
    {
      ANI_Stress_Seg_WS(A,B,P,b,sigma,a);
    }
  else
    {
      
      /* Collinear case */
      real8 B1[3], A2[3];
      real8 a2md2 = sqrt(a*a - d*d);

      B1[0] = D[0] - a2md2*xi[0]; 
      B1[1] = D[1] - a2md2*xi[1]; 
      B1[2] = D[2] - a2md2*xi[2]; 

      A2[0] = D[0] + a2md2*xi[0]; 
      A2[1] = D[1] + a2md2*xi[1]; 
      A2[2] = D[2] + a2md2*xi[2]; 

      real8 DotAA2xi, DotBB1xi, DotAB1xi,  DotBA2xi;
      DotAA2xi = Dot(A,A2,xi);
      DotBB1xi = Dot(B,B1,xi);
      DotAB1xi = Dot(A,B1,xi);
      DotBA2xi = Dot(B,A2,xi);

      if ( DotAA2xi < 0 || DotBB1xi > 0)
	{
	  ANI_Stress_Seg_WS(A,B,P,b,sigma,a);
	}
      else if ( DotAB1xi < 0 && DotAA2xi > 0)
	{
	  ANI_Stress_Seg_WS(A2,B,P,b,sigma,a);
	}
      else if ( DotBB1xi < 0 && DotBA2xi > 0)
	{
	  ANI_Stress_Seg_WS(A,B1,P,b,sigma,a);
	}
      else
	{
	  real8 stress[3][3];
	  Init3x3(stress);
	  ANI_Stress_Seg_WS(A,B1,P,b,sigma,a);
	  ANI_Stress_Seg_WS(A2,B,P,b,stress,a);
	    for (i = 0; i < 3; i++)
	      for (j = 0; j < 3; j++)
		sigma[i][j] += stress[i][j];	  
	}
    }

#if NAN_CHECK
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	if (isnan(sigma[i][j]) != 0) {
	  Print3("A",A);
	  Print3("B",B);
	  Print3("P",P);
	  Print3("D",D);
	  Fatal("stress in Stress.c is NAN!");
	}
      }
#endif

}

#ifndef _ANISO_FORCE_Steve
/* 
 * Similar to stress_seg_WS_main of DDLab
 */
void ANI_Stress_Seg_WS(real8 A[3], real8 B[3], real8 P[3], real8 b[3],
		       real8 sigma[][3],real8 a)
{
  int i,j;
  int p,s,q,k,l;
  real8 xi[3], D[3], PD[3];
  real8 h, eps, DotAPxi;
  real8 PA[3], PB[3];
  real8 normPA, normPB;
  real8 ev[3], n[3];
  real8 t1[3], t2[3], m1[3], m2[3];
  real8 Q1[3][3],S1[3][3],B1[3][3];
  real8 Q2[3][3],S2[3][3],B2[3][3];
  real8 nn[3][3], nm1[3][3], nm2[3][3], invnn[3][3];
  real8 tmp[3][3], tmp1[3][3];
  real8 M1[3][3], M2[3][3];
  real8 strain[3][3];

  eps = 1e-6;

  Init3x3(sigma);

  xi[0] = B[0]-A[0];
  xi[1] = B[1]-A[1];
  xi[2] = B[2]-A[2];  
  Normalize2(xi);

  // Determine d
  DotAPxi = Dot(A,P,xi);
  D[0] = DotAPxi * xi[0] + A[0];
  D[1] = DotAPxi * xi[1] + A[1];
  D[2] = DotAPxi * xi[2] + A[2];

  PD[0] = D[0]-P[0];
  PD[1] = D[1]-P[1];
  PD[2] = D[2]-P[2]; 
  h = Normal(PD); 

  // t1 = PA/|PA|
  PA[0] = P[0] - A[0];
  PA[1] = P[1] - A[1];
  PA[2] = P[2] - A[2];
  normPA = sqrt(PA[0]*PA[0] + PA[1]*PA[1] + PA[2]*PA[2]);
  if (normPA < a) normPA = a;
 
  // t2 = PB/|PB|
  PB[0] = P[0] - B[0];
  PB[1] = P[1] - B[1];
  PB[2] = P[2] - B[2];
  normPB = sqrt(PB[0]*PB[0] + PB[1]*PB[1] + PB[2]*PB[2]);
  if (normPB < a) normPB = a;

  if (h < eps)
    {
      Init3x3(sigma);
      stress_SelfForce_ANI(b[0],b[1],b[2], A[0],A[1],A[2],B[0],B[1],B[2],
			   a, sigma);
      
      for (i=0;i<3;i++) 
	for (j=0;j<3;j++) 
	  {
	    sigma[i][j] *= (1./normPB - 1./normPA);
	  }
      return;
    }
  else
    {
      t1[0] = PA[0]/normPA;t1[1] = PA[1]/normPA; t1[2]=PA[2]/normPA;
      t2[0] = PB[0]/normPB;t2[1] = PB[1]/normPB; t2[2]=PB[2]/normPB;
      

      ev[0] = PD[0]/h; ev[1] = PD[1]/h; ev[2] = PD[2]/h;
      cross(xi,ev,n);
    }

  QS_get(t1,Q1,S1);
  QS_get(t2,Q2,S2);

  cross(n,t1,m1);
  cross(n,t2,m2);
   
  brackets(n,n,nn  );
  brackets(n,m1,nm1);
  brackets(n,m2,nm2);

  Inv3x3(nn,invnn);

  Multiply3x3(invnn,nm2,tmp);
  Multiply3x3(tmp,Q2,tmp1);

  real8 S2t[3][3];
  Transpose3x3(S2,S2t);
  Multiply3x3(invnn,S2t,tmp);

  for (i=0;i<3;i++) 
    for (j=0;j<3;j++) 
      {
	M2[i][j] = tmp1[i][j] + tmp[i][j];
      } 

  Multiply3x3(invnn,nm1,tmp);
  Multiply3x3(tmp,Q1,tmp1);

  real8 S1t[3][3];
  Transpose3x3(S1,S1t);

  Multiply3x3(invnn,S1t,tmp);
 
  for (i=0;i<3;i++) 
    for (j=0;j<3;j++) 
      {
	M1[i][j] = tmp1[i][j] + tmp[i][j];
      }

  Init3x3(strain);
  for (p=0;p<3;p++) 
    for (s=0;s<3;s++)   
      for (j=0;j<3;j++) 
	for (q=0;q<3;q++) 
	  for (i=0;i<3;i++) 
	    for (k=0;k<3;k++) 
	      for (l=0;l<3;l++) 
		{
		  strain[p][s] += 
		    aniso0->eps[j][s][q]*b[i]*aniso0->C[i][j][k][l]*xi[q]*
		    (-m2[l]*Q2[p][k] + n[l]*M2[p][k]
		     +m1[l]*Q1[p][k] - n[l]*M1[p][k])/(4*M_PI*h);
		}


  // get stress from Hooke's law
  Init3x3(sigma);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
	for (l = 0; l < 3; l++)
	  {
	    sigma[i][j] += aniso0->C[i][j][k][l]*strain[k][l];
	  }
  
}
#else
void ANI_Stress_Seg_WS(real8 A[3], real8 B[3], real8 P[3], real8 b[3],
		       real8 sigma[][3],real8 a)
{
  int i,j,k,l,o;
  Param_t *param;
  real8 BA[3],PA[3],PB[3],t[3],n[3],normBA,normPA,normPB,norm,dot;
  real8 t1[3],t2[3];
  real8 F2mF1[3][3][3];
  real8 h,h2,u[3][3];
  real8 eps = 1e-4;
  real8 d, D[3], PD[3], DotAPxi;

  Init3x3(sigma);

  // t : the normalized segment line direction 
  // t = BA/|BA|
  BA[0] = B[0]-A[0];
  BA[1] = B[1]-A[1];
  BA[2] = B[2]-A[2];  
  normBA = sqrt(BA[0]*BA[0] + BA[1]*BA[1] + BA[2]*BA[2]);
  if (normBA < a) normBA = a;
  t[0]=BA[0]/normBA; t[1]=BA[1]/normBA; t[2]=BA[2]/normBA;

  // Determine d
  DotAPxi = Dot(A,P,t);
  D[0] = DotAPxi * t[0] + A[0];
  D[1] = DotAPxi * t[1] + A[1];
  D[2] = DotAPxi * t[2] + A[2];

  PD[0] = P[0]-D[0];
  PD[1] = P[1]-D[1];
  PD[2] = P[2]-D[2]; 
  d = Normal(PD); 

  // t1 = PA/|PA|
  PA[0] = P[0] - A[0];
  PA[1] = P[1] - A[1];
  PA[2] = P[2] - A[2];
  normPA = sqrt(PA[0]*PA[0] + PA[1]*PA[1] + PA[2]*PA[2]);
  if (normPA < a) normPA = a;
  t1[0] = PA[0]/normPA;t1[1] = PA[1]/normPA; t1[2]=PA[2]/normPA;
 
  // t2 = PB/|PB|
  PB[0] = P[0] - B[0];
  PB[1] = P[1] - B[1];
  PB[2] = P[2] - B[2];
  normPB = sqrt(PB[0]*PB[0] + PB[1]*PB[1] + PB[2]*PB[2]);
  if (normPB < a) normPB = a;
  t2[0] = PB[0]/normPB;t2[1] = PB[1]/normPB; t2[2]=PB[2]/normPB;

  dot = t[0]*t1[0] + t[1]*t1[1] + t[2]*t1[2];
  
  /* Case A,B,P collinear */
  //if (dot*dot >= 1.0 - eps || normPA <= a || normPB <= a)
  if (d <= 1e-6)
    {
      coll_F_irs(t,F2mF1);
      h = normPB*normPA/(normPB-normPA);
    }
  else
    {
      // n : normal to plane containing segment and field point
      // n = (BA x PA)/|BA x PA|
      cross(BA,PA,n);
      norm = Normal(n);
      n[0]/= (-norm); n[1]/= (-norm); n[2]/= (-norm);

      // Compute F2mF1
      getF2mF1(n,t1,t2,F2mF1);
  
      // h = perpendicular distance segment to field point
      h = normPA*sqrt(1.0000-dot*dot);
    }

  // get strain - full Willis-Steeds formula a la Lothe

  Init3x3(u);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
	for (l = 0; l < 3; l++)
	  {
	    real8 CC = aniso0->C[i][j][k][l];
	    for (o = 0; o < 3; o++)
	      {
		u[0][0] += aniso0->eps[j][0][o]*b[i]*CC*t[o]*F2mF1[l][0][k]; 
		u[0][1] += aniso0->eps[j][1][o]*b[i]*CC*t[o]*F2mF1[l][0][k]; 
		u[0][2] += aniso0->eps[j][2][o]*b[i]*CC*t[o]*F2mF1[l][0][k];
		
		u[1][0] += aniso0->eps[j][0][o]*b[i]*CC*t[o]*F2mF1[l][1][k]; 
		u[1][1] += aniso0->eps[j][1][o]*b[i]*CC*t[o]*F2mF1[l][1][k]; 
		u[1][2] += aniso0->eps[j][2][o]*b[i]*CC*t[o]*F2mF1[l][1][k]; 
		
		u[2][0] += aniso0->eps[j][0][o]*b[i]*CC*t[o]*F2mF1[l][2][k]; 
		u[2][1] += aniso0->eps[j][1][o]*b[i]*CC*t[o]*F2mF1[l][2][k]; 
		u[2][2] += aniso0->eps[j][2][o]*b[i]*CC*t[o]*F2mF1[l][2][k];
	      }
	  }

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	u[i][j] /= (4.0*M_PI*h);
      }	  
  
  // get stress from Hooke's law
  Init3x3(sigma);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
	for (l = 0; l < 3; l++)
	  {
	    sigma[i][j] += aniso0->C[i][j][k][l]*u[k][l];
	  }

#if NAN_CHECK
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	if (isnan(sigma[i][j]) != 0) {
	  Print3("A",A);
	  Print3("P",P);
	  Print3("B",B);
	  printf("a=%f\n",a);
	  printf("normPA=%f normPB=%f normBA=%f\n",normPA,normPB,normBA);
	  Fatal("stress in ANI_Stress.c is NAN!");
	}
      }
#endif

}






void getF2mF1(real8 n[3],real8 t1[3], real8 t2[3],
	      real8 F2mF1[3][3][3])
{
  int i,j,k;
  real8 F1[3][3][3],F2[3][3][3];
  real8 m1[3],m2[3];
  real8 nm1[3][3],nm2[3][3],nn[3][3];
  real8 nninv[3][3];
  real8 Q1[3][3],Q2[3][3];
  real8 S1[3][3],S2[3][3];
  real8 B1[3][3],B2[3][3];
  real8 mtemp[3][3],nninvnm1Q1[3][3],nninvnm2Q2[3][3];
  real8 nninvST1[3][3],nninvST2[3][3];

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
	{
	  F1[i][j][k] = 0.0;
	  F2[i][j][k] = 0.0;
	}


  // m1 and m2
  cross(n,t1,m1);
  cross(n,t2,m2);
  
  // Bracket matrices
  brackets(n,m1,nm1);
  brackets(n,m2,nm2);
  brackets(n,n, nn);
  
  // Invert nnmatrix
  Inv3x3(nn,nninv);

  // Compute Q and S for each imaginary dislocation
  QS_get(t1,Q1,S1);
  QS_get(t2,Q2,S2);

  // Now build F1 and F2
  Multiply3x3(nm1,Q1,mtemp);
  Multiply3x3(nninv,mtemp,nninvnm1Q1);
  
  Multiply3x3(nm2,Q2,mtemp);
  Multiply3x3(nninv,mtemp,nninvnm2Q2);
  
  Transpose3x3(S1,mtemp);
  Multiply3x3(nninv,mtemp,nninvST1);
  
  Transpose3x3(S2,mtemp);
  Multiply3x3(nninv,mtemp,nninvST2);

  // F2 - F1 is rightmost part of Willis-Steeds formula
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
	{
	  F1[i][j][k] += - m1[i]*Q1[j][k] +
	    n[i]*nninvnm1Q1[j][k]+ n[i]*nninvST1[j][k];
	  
	  F2[i][j][k] += - m2[i]*Q2[j][k] +
	    n[i]*nninvnm2Q2[j][k] + n[i]*nninvST2[j][k];
	}
  
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
	{
	  F2mF1[i][j][k] = F2[i][j][k] - F1[i][j][k];		    
	}
}



void coll_F_irs(real8 tdir[3],real8 Fbar_irs[3][3][3])
{
  int i,j,k,l,r,s;
  real8 m0[3],n0[3];
  real8 q11,q12;
  real8 norm;
  real8 T[3][3];
  real8 nq[3],mq[3],temp1[3][3],temp2[3][3];
  real8 n[3],m[3],nn[3][3],nm[3][3],mm[3][3],mn[3][3];
  real8 nninv[3][3],tnpnt[3][3];
  real8 dQ[3][3][3],nt[3][3],tn[3][3];
  real8 dQdown[3][3][3],dQup[3][3][3];
  real8 Q0[3][3][3];

  int npoints = 100;
  real8 domega = M_PI/(npoints*1.0);

  // Only integrate 0 to pi, exploiting symmetry of integrand

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
	{
	  Q0[i][j][k]     = 0.0;
	  dQdown[i][j][k] = 0.0;
	  dQup[i][j][k] = 0.0;
	  dQ[i][j][k]     = 0.0;
	}

  if (fabs(tdir[0]*tdir[0] + tdir[1]*tdir[1]) > 1.e-10)
    {
      q11 = fabs(tdir[1])/sqrt(tdir[0]*tdir[0] + tdir[1]*tdir[1]);
      if (fabs(tdir[1]) > 1.e-10)
        q12 = -tdir[0]*q11/tdir[1];
      else
        q12 = 1.0;

      norm = sqrt(q11*q11 + q12*q12);
      m0[0] = q11/norm;
      m0[1] = q12/norm;
      m0[2] = 0.0;
    }
  else
    {
      m0[0]=1.0;
      m0[1]=0.0;
      m0[2]=0.0;
    }

  norm = sqrt(m0[0]*m0[0] + m0[1]*m0[1] + m0[2]*m0[2]);
  m0[0]/=norm; m0[1]/=norm; m0[2]/=norm;
  cross(tdir,m0,n0);
  norm = sqrt(n0[0]*n0[0] + n0[1]*n0[1] + n0[2]*n0[2]);
  n0[0]/=norm; n0[1]/=norm; n0[2]/=norm;

  // make a transformation matrix to get the rotating
  // "integration vectors"
  T[0][0] = m0[0];
  T[0][1] = n0[0];
  T[0][2] = tdir[0];
  T[1][0] = m0[1];
  T[1][1] = n0[1];
  T[1][2] = tdir[1];
  T[2][0] = m0[2];
  T[2][1] = n0[2];
  T[2][2] = tdir[2];

  // INTEGRATION
  // Fixed trapezoidal scheme
  // First step "i=0"
  nq[0] = 1.0; nq[1]= 0.0;nq[2]=0.0;
  mq[0] = 0.0; mq[1]=-1.0;mq[2]=0.0;

  // Operate with T matrix to rotate integration vectors
  for (k = 0; k < 3; k++)
    {
      n[k] = T[k][0]*nq[0] + T[k][1]*nq[1] + T[k][2]*nq[2];
      m[k] = T[k][0]*mq[0] + T[k][1]*mq[1] + T[k][2]*mq[2];
    }

  norm = sqrt(m[0]*m[0] + m[1]*m[1] + m[2]*m[2]);
  m[0]/= norm; m[1]/= norm; m[2]/= norm;

  // get bracket matrices (in loop will be omega-dependent)
  brackets(n,n,nn);
  brackets(n,tdir,nt);
  brackets(tdir,n,tn);
  
  // Invert nn
  Inv3x3(nn,nninv);

  // Build the ((tn) + (nt)) matrix

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	tnpnt[i][j] = tn[i][j] + nt[i][j];
      }

  // Build (nn)^{-1} [(tn) + (nt)] (nn)^{-1} matrix
 
  Multiply3x3(tnpnt,nninv,temp1);
  Multiply3x3(nninv,temp1,temp2);

  // Special integrand evaluated at start point (called Q)

  for (i = 0; i < 3; i++)
    for (r = 0; r < 3; r++)
      for (s = 0; s < 3; s++)
	{
	  dQ[i][r][s] = tdir[s]*nninv[i][r] - n[s]*temp2[i][r];
	}

  // Set first values for integration loop
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
      {
        dQdown[i][j][k] = dQ[i][j][k];
      }

 // now the rest
  for (i = 1; i <= npoints; i++)
    {
      nq[0] = cos(i*domega);
      nq[1] = sin(i*domega);
      nq[2] = 0.0;

      mq[0] = sin(i*domega);
      mq[1] =-cos(i*domega);
      mq[2] = 0.0;

      // get integration vectors : i=0 already done
      for (l = 0; l < 3; l++)
        {
          n[l]=T[l][0]*nq[0]+T[l][1]*nq[1]+T[l][2]*nq[2];
          m[l]=T[l][0]*mq[0]+T[l][1]*mq[1]+T[l][2]*mq[2];
        }


      norm = sqrt(m[0]*m[0] + m[1]*m[1] + m[2]*m[2]);
      m[0]/=norm;m[1]/=norm;m[2]/=norm;

      // bracket matrices
      brackets(n,n,nn);
      brackets(n,tdir,nt);
      brackets(tdir,n,tn);

      // Invert (nn)
      Inv3x3(nn,nninv);

      for (r = 0; r < 3; r++)
	for (j = 0; j < 3; j++)
	  {
	    tnpnt[r][j] = tn[r][j] + nt[r][j];
	  }

      // Build (nn)^{-1} [(tn) + (nt)] (nn)^{-1} matrix
      
      Multiply3x3(tnpnt,nninv,temp1);
      Multiply3x3(nninv,temp1,temp2);
      
      
      // get upper values for integrands
      for (j = 0; j < 3; j++)
	for (k = 0; k < 3; k++)
	  for (l = 0; l < 3; l++)
	    {
	      dQup[j][k][l] = tdir[l]*nninv[j][k] - n[l]*temp2[j][k];
	    }
      
      // trapezoidal integration step
      for (j = 0; j < 3; j++)
	for (k = 0; k < 3; k++)
	  for (l = 0; l < 3; l++)
	    {
	      Q0[j][k][l] += 0.5*domega*(dQdown[j][k][l] + dQup[j][k][l]);
	    }
      
      // re-use upper values for lower values of next step
      for (j = 0; j < 3; j++)
	for (k = 0; k < 3; k++)
	  for (l = 0; l < 3; l++)
	    {
	      dQdown[j][k][l] = dQup[j][k][l];
	    }
      
    } // end i loop on npoints
  
  /*
   * Assign output values  
   */
  
  
  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      for (l = 0; l < 3; l++)
	{
	  Fbar_irs[j][k][l] = Q0[j][k][l]/M_PI;
	}
  
  // Stop integral at pi, so only divide by pi rather than 2*pi
}

#endif


#endif
