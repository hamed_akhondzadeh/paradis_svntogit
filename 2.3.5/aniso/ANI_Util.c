/***************************************************************************
 *
 *  Module      : ANI_Util.c
 *  Description : Connection with ANI module functions  
 *  (Sylvie Aubry June 12 2008)
 *
 **************************************************************************/

#include <stdio.h>
#include <math.h> 
#include "Param.h"
#include "Home.h"
#include "ANI.h"

#define VOIGT 0
#define REUSS 1

/* Node that Cpr = (C11 - C12) /2  */

#ifdef _ANISOTROPIC
void ANI_Init(Home_t *home, Aniso_t **ani_ptr,char *workingDir0)
{
   Param_t *param;
   real8 C12, C44, Cpr;

   ANI_Alloc(ani_ptr);

   param = home->param;
   C12 = param->C12;
   C44 = param->C44;
   Cpr = param->Cpr;

   if (home->myDomain == 0) 
   {
	/* Banner for Anisotropic elasticity */
	printf("\n\n");
	printf("****************************************************\n\n");
	printf("**  MODULE : ANISOTROPIC ELASTICITY FOR PARADIS   **\n\n");
	printf("****************************************************\n\n");
   }

   ANI_Assign_Cubic_Constants(*ani_ptr, C12, C44, Cpr);


   /* modifying Param structure */
	if (home->myDomain == 0) printf("Anisotropic constants: \n");
	if (home->myDomain == 0) printf("C11=%g C12=%g C44=%g\n",2*Cpr+C12,C12,C44);
	if (home->myDomain == 0) printf("Cpr=(C11-C12)/2=%g\n\n",Cpr);


   /* Changing MU and NU here for anisotropic elasticity */
#if VOIGT
	if (home->myDomain == 0) printf("Using Voigt averages for elastic constants\n");
	param->shearModulus =(2.00*Cpr+3.00*C44)/5.00;
	param->pois = 0.50*(2.00*Cpr+5.00*C12-2.00*C44)/(4.00*Cpr+5.00*C12+C44);
	param->YoungsModulus= (4.00*Cpr*Cpr+6.00*Cpr*C12 + 6.00*Cpr*C44 + 9.00*C12*C44) / 
	                      (4.00*Cpr+5.00*C12+C44);
	if (home->myDomain == 0) printf("Shear Modulus = %g\n",param->shearModulus);
	if (home->myDomain == 0) printf("Poisson Ratio = %g\n",param->pois);
	if (home->myDomain == 0) printf("Young Modulus = %g\n\n",param->YoungsModulus);
#endif
	
#if REUSS

	if (home->myDomain == 0) printf("Using Reuss averages for elastic constants\n");

	param->shearModulus =5.00*Cpr*C44/(2.00*C44+3.00*Cpr);
	param->pois = 0.50*(2.00*C44*C12+2.00*Cpr*Cpr + 3.00*Cpr*C12 -2.00*Cpr*C44) / 
                           (2.00*C44*C12+2.00*Cpr*Cpr + 3.00*Cpr*C12 +3.00*Cpr*C44);
	param->YoungsModulus= 5.00*Cpr*C44*(2.00*Cpr+3.00*C12) / (3.00*Cpr*C44+2.00*Cpr*Cpr+2.00*C12*C44 + 3.00*C12*Cpr);
	if (home->myDomain == 0) printf("Shear Modulus = %g\n",param->shearModulus);
	if (home->myDomain == 0) printf("Poisson Ratio = %g\n",param->pois);
	if (home->myDomain == 0) printf("Young Modulus = %g\n\n",param->YoungsModulus);
#endif

        /* we change core energy again because shearModulus may have changed */
	if (param->Ecore < 0.0)
	  {
	    // Change rC otherwise Ecore is still zero!
	    if (param->rc == 0.1) param->rc = 1;

	    param->Ecore = (param->shearModulus / (4*M_PI)) *
	      log(param->rc/0.1);
	    param->TensionFactor = param->Ecore*2/param->shearModulus;
	  }
	
	printf("Ecore is %f and rC is %f \n",param->Ecore,param->rc);

#ifdef _ANISO_Steve
	printf("Using Steve Fitzgerald anisotropic method\n");
#endif

#ifdef _USE_QSB_TABLES
	aniso0 = *ani_ptr;	
	DefineQSB(home,*ani_ptr,workingDir0);
#else
	if (home->myDomain == 0) 
	  printf("\nWill not use tables to compute Q, S and B\n\n\n");
#endif

}



void ANI_Alloc(Aniso_t **ani_ptr)
{
   Aniso_t *aniso;
   aniso = (Aniso_t *) calloc(1, sizeof(Aniso_t));
   *ani_ptr = aniso;
}

void ANI_Assign_Cubic_Constants(Aniso_t *aniso, real8 C12, real8 C44, real8 Cpr)
{
   int i,j,k,l;

   aniso->C12 = C12;
   aniso->C44 = C44;
   aniso->Cpr = Cpr;

   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
     {
        aniso->delta[i][j] = 0.0;
        for (k = 0; k < 3; k++)
	{
           aniso->eps[i][j][k] = 0.0;
	   for (l = 0; l < 3; l++)
	   {
	      aniso->C[i][j][k][l] = 0.0;
           }
	}
     }

   aniso->delta[0][0] = 1.0;
   aniso->delta[1][1] = 1.0;
   aniso->delta[2][2] = 1.0;

   aniso->eps[0][1][2] = 1.0;
   aniso->eps[1][2][0] = 1.0;
   aniso->eps[2][0][1] = 1.0;

   aniso->eps[2][1][0] =-1.0;
   aniso->eps[1][0][2] =-1.0;
   aniso->eps[0][2][1] =-1.0;

   /* C : elastic constant tensor */
   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
       for (k = 0; k < 3; k++)
         for (l = 0; l < 3; l++)
         {
	   aniso->C[i][j][k][l] += 
	     C12*aniso->delta[i][j]*aniso->delta[k][l]
	   + C44*(aniso->delta[i][k]*aniso->delta[j][l] 
	   + aniso->delta[i][l]*aniso->delta[j][k])

	   /* following line vanishes in isotropic approximation */
	   + 2*(Cpr - C44)*aniso->delta[i][j]*aniso->delta[k][l]*aniso->delta[i][k];
	}

}

void rhcoord(real8 t[3], real8 n[3], real8 m[3])
{
  real8 norm;
  real8 trand[3];
  unsigned int seed;
  int count;
  
  seed = 38943;
  
  norm = Normal(t);  
  if (norm < 1e-8) Fatal("Norm of t is zero in rhcoord");
  
  srand(seed++);
  trand[0] = (real8)(rand() / (RAND_MAX + 1.0));
  srand(seed++);
  trand[1] = (real8)(rand() / (RAND_MAX + 1.0));
  srand(seed++);
  trand[2] = (real8)(rand() / (RAND_MAX + 1.0));

  norm = Normal(trand);
  count = 0;
  while (norm < 1e-8)
    {
      srand(seed++);
      trand[0] = (real8)(rand() / (RAND_MAX + 1.0));
      srand(seed++);
      trand[1] = (real8)(rand() / (RAND_MAX + 1.0));
      srand(seed++);
      trand[2] = (real8)(rand() / (RAND_MAX + 1.0));      
      count++;
      if (count > 10) Fatal("Cannot find an acceptable random t ");
      norm = Normal(trand);
    }

  cross(t,trand,m);
  Normalize2(m);

  cross(t,m,n);
  Normalize2(n);
}



#endif




/*---------------------------------------------------------------
 *
 *                I/O functions 
 *
 *---------------------------------------------------------------
 */ 
real8 Dot(real8 A[3], real8 B[3], real8 C[3])
{
  /* returns AB.C = (B-A)*C */

  real8 BA[3];
  BA[0] = B[0]-A[0];
  BA[1] = B[1]-A[1];
  BA[2] = B[2]-A[2]; 

  return BA[0]*C[0] + BA[1]*C[1] +BA[2]*C[2];
}

void Normalize2(real8 r[3])
{
  Normalize(&(r[0]),&(r[1]),&(r[2]));
}

real8 ErrorCalc(real8 *A, real8 *B, int n)
{
  int i;
  real8 gMax  = fabs(A[0] - B[0]);
  real8 g0Max = fabs(A[0]);
  for (i = 1; i < n; i++)
    {
      if (gMax < fabs(A[i]-B[i])) gMax = fabs(A[i] - B[i]);
      if (g0Max < fabs(A[i]))    g0Max = fabs(A[i]); 
    }
  
  if (g0Max < 1e-6) return 0.0;

  return gMax/g0Max;
}

real8 Chop(real8 x)
{
  real8 chop = x;
  if (fabs(x)<1.e-8) chop=0.0;
  return chop;
}

real8 MaxArray(real8 *A,int n)
{
  int i;
  real8 MaxA = A[0];
  for (i = 1; i < n; i++)
    if (MaxA < A[i]) MaxA = A[i];
  return MaxA;
}


void Inv3x3(real8 A[3][3],real8 B[3][3])
{
  int i,j;
  real8 det, cofactor[3][3];

  det=A[0][0]*(A[1][1]*A[2][2]-A[1][2]*A[2][1]) 
    -A[0][1]*(A[1][0]*A[2][2]-A[1][2]*A[2][0]) 
    +A[0][2]*(A[1][0]*A[2][1]-A[1][1]*A[2][0]);


  cofactor[0][0]= A[1][1]*A[2][2]-A[1][2]*A[2][1];
  cofactor[0][1]=-A[1][0]*A[2][2]+A[1][2]*A[2][0];
  cofactor[0][2]= A[1][0]*A[2][1]-A[1][1]*A[2][0];

  cofactor[1][0]=-A[0][1]*A[2][2]+A[0][2]*A[2][1];
  cofactor[1][1]= A[0][0]*A[2][2]-A[0][2]*A[2][0];
  cofactor[1][2]=-A[0][0]*A[2][1]+A[2][0]*A[0][1];

  cofactor[2][0]= A[0][1]*A[1][2]-A[1][1]*A[0][2];
  cofactor[2][1]=-A[0][0]*A[1][2]+A[1][0]*A[0][2];
  cofactor[2][2]= A[0][0]*A[1][1]-A[0][1]*A[1][0];


  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++) 
      { 
	B[i][j] = cofactor[i][j]/det;
      }
}

void Multiply3x3(real8 A[3][3],real8 B[3][3], real8 AB[3][3])
{
  int i,j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)  
      {
	AB[i][j] = A[i][0]*B[0][j] + A[i][1]*B[1][j] + A[i][2]*B[2][j];
      }
}

void Transpose3x3(real8 A[3][3],real8 B[3][3])
{
  int i,j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)  
      {
	B[i][j] = A[j][i];
      }
}

void Print3x3(char *format,real8 A[3][3])
{
  printf("\n %s\n", format);
  printf("%.15e %.15e %.15e\n"  ,A[0][0],A[0][1],A[0][2]);
  printf("%.15e %.15e %.15e\n"  ,A[1][0],A[1][1],A[1][2]);
  printf("%.15e %.15e %.15e\n\n",A[2][0],A[2][1],A[2][2]);
}

void Print3(char *format,real8 A[3])
{
  printf("%s = ", format);
  printf("%.15e %.15e %.15e\n",A[0],A[1],A[2]);
}

void Print6(char *format,real8 A[6])
{
  printf("\n %s \n", format);
  printf("%.15e %.15e %.15e \n %.15e %.15e %.15e\n\n",A[0],A[1],A[2],
	 A[3],A[4],A[5]);
}

void Print3x3x3(char *format,real8 A[3][3][3])
{
  printf("\n %s\n", format);
  printf("%.15e %.15e %.15e\n",A[0][0][0],A[0][0][1],A[0][0][2]);
  printf("%.15e %.15e %.15e\n",A[0][1][0],A[0][1][1],A[0][1][2]);
  printf("%.15e %.15e %.15e\n",A[0][2][0],A[0][2][1],A[0][2][2]);

  printf("%.15e %.15e %.15e\n",A[1][0][0],A[1][0][1],A[1][0][2]);
  printf("%.15e %.15e %.15e\n",A[1][1][0],A[1][1][1],A[1][1][2]);
  printf("%.15e %.15e %.15e\n",A[1][2][0],A[1][2][1],A[1][2][2]);

  printf("%.15e %.15e %.15e\n",A[2][0][0],A[2][0][1],A[2][0][2]);
  printf("%.15e %.15e %.15e\n",A[2][1][0],A[2][1][1],A[2][1][2]);
  printf("%.15e %.15e %.15e\n",A[2][2][0],A[2][2][1],A[2][2][2]);

}

void Init3x3(real8 A[3][3])
{
  int i,j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)  
      {
	A[i][j] = 0.0;
      }  
}


void PrintNodesandNeighbors(char *format,Home_t *home)
{
  int i;
  Node_t *nodea, *nodeb;
  int count = 0;

  return;
  
  printf("\n %s\n", format);
  for (i = 0; i < home->newNodeKeyPtr; i++) 
    {
      nodea = home->nodeKeys[i];
      if (nodea == (Node_t *)NULL) continue;
      if (nodea->constraint == 7) continue;
      InfoNode(home,nodea);
      if (fabs(nodea->z) > 1e-4 && nodea->constraint !=7) exit(0); 
    }
}

void InfoNode(Home_t *home,Node_t *node)
{
  int j;
  Node_t *nbr;
  
  printf("node(%d,%d) z=%e velz=%f cst=%d has %d neighbors\n",
	 node->myTag.domainID, node->myTag.index, 
	 node->z,node->vZ,
	 node->constraint,node->numNbrs);
  
#if 0
  for (j = 0; j < node->numNbrs; j++) 
    {
      nbr = GetNeighborNode(home, node, j);
      printf("            nbr(%d,%d) x=%e,y=%e,z=%e cst=%d\n",
	     nbr->myTag.domainID, nbr->myTag.index, 
	     nbr->x, nbr->y, nbr->z,
	     nbr->constraint);
      
    }
  printf("\n");
#endif
}
