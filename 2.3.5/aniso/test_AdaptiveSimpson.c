#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>

#define real8 double

#define RET_VAL_DIM 3

void Print3(char *format,real8 A[3])
{
  printf("%s = ", format);
  printf("%.15e %.15e %.15e\n",A[0],A[1],A[2]);
}



#define MAX(a,b) ((a)>(b)?(a):(b))
#define MaxIter 1000000

int quadv(real8 *Q, int *fcnt, 
          void (*funfcn)(real8, real8*, real8*),
	  real8 a, real8 b, real8 tol, 
          real8 *param); 

int quadstep(real8 *Q, void (*funfcn)(real8, real8*, real8*),
             real8 a, real8 b,
             real8 *fa, real8 *fc, real8 *fb,
             real8 tol, int *fcnt, real8 hmin, real8 *param);

void expo(real8 x, real8 *ret, real8 *param);
void sino(real8 x, real8 *ret, real8 *param);
void oneovern(real8 x, real8 *ret, real8 *param);

int main()
{
  clock_t start, end;
  real8 elapsed;

  int i;
  int numiter = 0;
  int Niter = 1;
  real8 Intf[RET_VAL_DIM];
  real8 param[10];

  real8 a,b;

  a = 0.1;
  b = 0.5;
  param[0] = 3;

  real8 tol = 1e-4;
  
  printf("\n\nAdaptive Simpson Integration\n");
  printf("tolerance %f\n\n",tol);

  // Try sin(x) 
  start = clock();
  for (i=0;i<Niter;i++) quadv(Intf,&numiter,sino,a,b,tol,param);
  end = clock();
  elapsed = ((real8)(end)/Niter - (real8)(start))/ CLOCKS_PER_SEC;
  printf("Analytic %f %f %f\n",
	 -cos(b)+cos(a),-0.5*(cos(2*b) - cos(2*a)),-(cos(3*b) - cos(3*a))/3. );
  printf("Numeric  %f %f %f in %d iterations and in %f seconds\n\n",
	 Intf[0],Intf[1],Intf[2],numiter, elapsed);

  // Try exp(x)
  start = clock();
  for (i=0;i<Niter;i++) quadv(Intf,&numiter,expo,a,b,tol,param);
  end = clock();
  elapsed = ((real8)(end)/Niter - (real8)(start))/ CLOCKS_PER_SEC;
  printf("Analytic %f %f %f\n",
	 exp(b)-exp(a),0.5*(exp(2*b) - exp(2*a)),(exp(3*b) - exp(3*a))/3. );
  printf("Numeric  %f %f %f in %d iterations and in %f seconds\n\n",
	 Intf[0],Intf[1],Intf[2],numiter, elapsed);

  // Try 1/((1:n)+x)
  a = 1;
  b = 5;
  start = clock();
  for (i=0;i<Niter;i++) quadv(Intf,&numiter,oneovern,a,b,tol,param);
  end = clock();
  elapsed = ((real8)(end)/Niter - (real8)(start))/ CLOCKS_PER_SEC;

  printf("Analytic %f %f %f\n",
	 log(1+b)-log(1+a),log(2+b)-log(2+a),log(3+b)-log(3+a) );
  printf("Numeric  %f %f %f in %d iterations and in %f seconds\n\n",
	 Intf[0],Intf[1],Intf[2],numiter,elapsed);

}

void oneovern(real8 x, real8 *ret, real8 *param)
{
  int i;
  for(i = 0; i < param[0]; i++)
     ret[i] = 1.0 / (1.0 + i + x);
}

void expo(real8 x, real8 *ret, real8 *param)
{
  int i;
  for(i = 0; i < param[0]; i++)
     ret[i] = exp ((1.0 + i) * x);
}

void sino(real8 x, real8 *ret, real8 *param)
{
  int i;
  for(i = 0; i < param[0]; i++)
     ret[i] = sin ((1.0 + i) * x);
}

