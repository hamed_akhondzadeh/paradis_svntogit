
#############################################################################
#
#    makefile: controls the build of various ParaDiS support utilities
#
#    Builds the following utilities:
#
#        paradisgen     --  problem generator
#        paradisrepart  --  creates a new problem decomposition with a new
#                           domain geometry from an previous nodal data file
#        paradisconvert --
#        ctablegen      --
#
############################################################################
include ../makefile.sys
include ../makefile.setup

DEFS += -D_ANISOTROPIC
DEFS += -DFULL_N2_FORCES
#DEFS += -D_USE_QSB_TABLES
#DEFS += -D_CREATE_QSB_TABLES

DEFS += -D_CRITICAL_STRESS

# modifying number of integration points for integral formalism (default 10)
#DEFS += -DQSB_NINT=200

# switch aniso force implementation, Steve Fitzgerald's method is flag is turned on.
#DEFS += -D_ANISO_Steve

# debug
#DEFS += -D_WRITENODEFORCE

SRCDIR = ../src
INCDIR = ../include
BINDIR = ../bin

#
#	The utilities use various source modules from the parallel
#       code located in the parent directory.  Maintain a list of
#	these files.
#
#	These modules are compiled in the parent directory with a
#	different set of preprocessor definitions than are needed
#	here, so we need to create links in this directory back to
#	the source modlues and create separate object modules for
#	these sources.
#

EXTERN_C_SRCS = AddNode.c                \
      CellCharge.c             \
      CheckArms.c              \
      CommSendGhosts.c         \
      CommSendMirrorNodes.c    \
      CommSendRemesh.c         \
      CommSendSecondaryGhosts.c \
      CommSendSegments.c       \
      CommSendVelocity.c       \
      CrossSlipFCC.c           \
      CrossSlipBCC.c           \
      CorrectionTable.c        \
      DeltaPlasticStrain.c     \
      ForwardEulerIntegrator.c \
      ParadisInit.c            \
      ParadisFinish.c          \
      Decomp.c                 \
      DLBfreeOld.c             \
      deWitInteraction.c       \
      FccCrossSlip.c           \
      FixRemesh.c              \
      FMComm.c                 \
      FMSigma2.c               \
      FMSupport.c              \
      FreeInitArrays.c         \
      GenerateOutput.c         \
      GetDensityDelta.c        \
      GetNativeNodes.c         \
      GetNewNativeNode.c       \
      GetNewGhostNode.c        \
      Gnuplot.c                \
      Heap.c                   \
      InitCellDomains.c        \
      InitCellNatives.c        \
      InitCellNeighbors.c      \
      InitHome.c               \
      InitRemoteDomains.c      \
      InitSendDomains.c        \
      InputSanity.c            \
      LoadCurve.c       \
      Meminfo.c                \
      MemCheck.c               \
      Migrate.c                \
      MobilityLaw_BCC_0.c      \
      MobilityLaw_BCC_0b.c     \
      MobilityLaw_BCC_glide.c  \
      MobilityLaw_FCC_2.c      \
      MobilityLaw_Relax.c      \
      NodeVelocity.c           \
      Parse.c                  \
      PartialForces.c          \
      ParadisStep.c            \
      Plot.c                   \
      QueueOps.c               \
      ReadRestart.c            \
      ReadBinaryRestart.c      \
      RBDecomp.c               \
      RSDecomp.c               \
      RemapInitialTags.c       \
      Remesh.c                 \
      RemeshRule_2.c           \
      RemeshRule_3.c           \
      RemoteSegForces.c        \
      RemoveNode.c             \
      ResetNodeArmForce.c      \
      SortNativeNodes.c        \
      SortNodesForCollision.c  \
      Tecplot.c                \
      Timer.c                  \
      TrapezoidIntegrator.c    \
      Topology.c               \
      WriteArms.c              \
      WriteAtomEye.c           \
      WriteBinaryRestart.c     \
      WriteDensityField.c      \
      WriteFragments.c         \
      WriteForceTimes.c        \
      WritePoleFig.c           \
      WritePovray.c            \
      WriteProp.c              \
      WriteDensFlux.c          \
      WriteRestart.c           \
      WriteTSB.c               \
      WriteVelocity.c          \
      Util.c

EXTERN_CPP_SRCS = DisplayC.C       \
                  display.C 

EXTERN_SRCS = $(EXTERN_C_SRCS) $(EXTERN_CPP_SRCS)
EXTERN_OBJS = $(EXTERN_C_SRCS:.c=.o) $(EXTERN_CPP_SRCS:.C=.o)

#
#	Define the exectutable, source and object modules for
#	the problem generator.
#

PARADISANI     = paradisani
PARADISANI_BIN = $(BINDIR)/$(PARADISANI)

PARADISANI_C_SRCS = ANI_Main.c        \
                    ANI_Util.c        \
                    ANI_Frank-Read.c  \
		    ANI_Forces.c      \
		    ANI_Stress.c      \
                    AdaptiveSimpson.c \
                    Collision.c       \
                    Initialize.c             \
                    LocalSegForces.c  \
                    MobilityLaw_FCC_0.c      \
                    NodeForce.c       \
                    Param.c           \
                    QSB.c             \
                    QSB_Compute.c     \
                    QSB_Tabulate.c    

PARADISANI_INCS = -I Include 
PARADISANI_LIBS = 

PARADISANI_SRCS = $(PARADISANI_C_SRCS) $(PARADISANI_CPP_SRCS)
PARADISANI_OBJS = $(PARADISANI_C_SRCS:.c=.o) $(PARADISANI_CPP_SRCS:.C=.o)



###########################################################################
#
#	Define a rule for converting .c files to object modules.
#	All modules are compile serially in this directory
#
###########################################################################

.c.o:		makefile ../makefile.sys ../makefile.setup
		$(CC) $(OPT) $(CCFLAG) $(PARADISANI_INCS) $(INCS) -c $<

.C.o:		makefile ../makefile.sys ../makefile.setup
		$(CPP) $(OPT) $(CPPFLAG) $(INCS) -c $<


###########################################################################
#
#	Define all targets and dependencies below
#
###########################################################################

all:		$(EXTERN_OBJS) $(PARADISANI) 

clean:
		rm -f *.o $(EXTERN_SRCS) $(PARADISANI_BIN) test_QSB test_AdaptiveSimpson test_ANI_Forces test_SelfForce test_ABP_algo

depend:		 *.c $(SRCDIR)/*.c $(INCDIR)/*.h makefile
		makedepend -Y$(INCDIR) *.c  -fmakefile.dep

#
#	Create any necessary links in the current directory to source
#	modules located in the SRCDIR directory
#

$(EXTERN_SRCS): $(SRCDIR)/$@
		- @ ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1

# For vip
#$(EXTERN_SRCS): $(SRCDIR)/$@
#                ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1

$(PARADISANI):	$(PARADISANI_BIN)
$(PARADISANI_BIN): $(PARADISANI_SRCS) $(PARADISANI_OBJS) $(EXTERN_OBJS) $(HEADERS)
		$(CPP) $(OPT) $(PARADISANI_OBJS) $(EXTERN_OBJS) -o $@  $(LIB) $(PARADISANI_LIBS)

test_AdaptiveSimpson: test_AdaptiveSimpson.o AdaptiveSimpson.o
	$(CC) -o $@ $^ -lm

test_QSB: test_QSB.o QSB.o QSB_Compute.o QSB_Tabulate.o test_Utils.o
	$(CC) -o $@ $^ -lm

test_ANI_Forces: test_ANI_Forces.o ANI_Forces.o ANI_Stress.o AdaptiveSimpson.o QSB.o QSB_Compute.o QSB_Tabulate.o test_Utils.o
	$(CC) -o $@ $^ -lm

test_ABP_algo: test_ABP_algo.o ANI_Forces.o ANI_Stress.o AdaptiveSimpson.o QSB.o QSB_Compute.o QSB_Tabulate.o  test_Utils.o
	$(CC) -o $@ $^ -lm

test_SelfForce: test_SelfForce.o ANI_Forces.o ANI_Stress.o QSB.o QSB_Compute.o  QSB_Tabulate.o test_Utils.o AdaptiveSimpson.o
	$(CC) -o $@ $^ -lm
