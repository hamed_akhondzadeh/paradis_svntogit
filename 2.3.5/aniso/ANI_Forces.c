#include <stdio.h>
#include <math.h>

#ifdef _ANISOTROPIC

#include "ANI.h"

#ifndef LINE_INTEGRATOR
#define LINE_INTEGRATOR quadv
#endif

#ifndef LINE_INTEGRATOR_TOL 
#define LINE_INTEGRATOR_TOL 1e-6
#endif



int LINE_INTEGRATOR(real8 *Q, int *fcnt, 
                    void (*funfcn)(real8, real8*, real8*),
          	    real8 a, real8 b, real8 tol, 
                    real8 *param);



/* Uses Wei Cai and Jie Yin's method*/
#ifndef _ANISO_FORCE_Steve
void SelfForce_ANI(int coreOnly,real8 MU, real8 NU,
		   real8 bx, real8 by, real8 bz,
		   real8 x1, real8 y1, real8 z1,
		   real8 x2, real8 y2, real8 z2,
		   real8 a,  real8 Ecore, 
		   real8 f1[3], real8 f2[3])
{/* 
 * SelForce routine using Wei Cai method 
 * Similar to function fac_co_i.m routine to get the stress
 * Similar to seg_force_self.m to get the forces
 */

  int i,j;
  real8 stress[3][3], sigb[3], t[3];
  real8 L, factor;
  
  /* Get direction t */
  GetUnitVector(1, x1, y1, z1, x2, y2, z2, &t[0], &t[1], &t[2], &L);

  stress_SelfForce_ANI(bx, by, bz, x1, y1, z1, x2, y2, z2,
		       a, stress);

  sigb[0] = stress[0][0]*bx +
            stress[0][1]*by +
            stress[0][2]*bz;
  sigb[1] = stress[1][0]*bx +
            stress[1][1]*by +
            stress[1][2]*bz;
  sigb[2] = stress[2][0]*bx +
            stress[2][1]*by +
            stress[2][2]*bz;

  cross(sigb,t,f1);

  factor = -(log(L/a) + a/L -1.);

  f1[0] *= factor;
  f1[1] *= factor;
  f1[2] *= factor;
  
  f2[0] = -f1[0];
  f2[1] = -f1[1];
  f2[2] = -f1[2];

  /* Add core-self interaction contributions */
  real8 bs, bex,bey,bez,be2,bs2;
  real8 fL, fT;
  bs = bx*t[0] + by*t[1] + bz*t[2];
  bex = bx-bs*t[0]; bey = by-bs*t[1]; bez=bz-bs*t[2];
  be2 = (bex*bex+bey*bey+bez*bez);
  bs2 = bs*bs;

  /* Torsional contribution */
  fT = Ecore*2*bs*NU/(1-NU);

  /* Longitudinal contribution */
  fL =-Ecore*(bs2+be2/(1-NU)); 

  f2[0] += bex*fT + fL*t[0];
  f2[1] += bey*fT + fL*t[1];
  f2[2] += bez*fT + fL*t[2];

  f1[0] =-f2[0];
  f1[1] =-f2[1];
  f1[2] =-f2[2];

}



void stress_SelfForce_ANI(real8 bx, real8 by, real8 bz,
			  real8 x1, real8 y1, real8 z1,
			  real8 x2, real8 y2, real8 z2,
			  real8 a, real8 stress[3][3])
{
  real8 L;
  real8 t[3], b[3];
  real8 m[3], n[3];
  real8 Q[3][3], S[3][3], B[3][3];
  real8 Qp[3][3], Sp[3][3], Bp[3][3];
  real8 M0[3][3],tmp[3][3];
  real8 sigb[3];

  int k,Nint;
  int i,j,p,w,q,r,s,l;
  real8 factor,dphi,phi,z[3];
  real8 straintot[3][3];
  real8 *strain[3][3];
  real8 zz[3][3],zt[3][3],tz[3][3],invzz[3][3];

  b[0] = bx; b[1] = by; b[2] = bz;

  Nint = 1000;
  dphi = 2*M_PI/Nint;

  for (i=0;i<3;i++) 
    for (j=0;j<3;j++) 
      {
	strain[i][j] = (double *)malloc(sizeof(double)*(Nint+2));
      }

  /* Get direction t */
  GetUnitVector(1, x1, y1, z1, x2, y2, z2, &t[0], &t[1], &t[2], &L);

  /* Get two arbitrary directions m and n */
  rhcoord(t,n,m);

  /* Get Q,B,S matrices */
  QSB_get(t,Q,S,B);
  QSBprime_get(t,b,n,Qp,Sp,Bp);

  for (k = 0; k <= Nint+1; k++)
    for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
	strain[i][j][k] = 0.0;

  for (k = 1; k <= Nint+1; k++)
    {

      phi = (k-1)*dphi;
      z[0] = n[0]*cos(phi) - m[0]*sin(phi);
      z[1] = n[1]*cos(phi) - m[1]*sin(phi);
      z[2] = n[2]*cos(phi) - m[2]*sin(phi);
      brackets(z,z,zz);
      brackets(t,z,tz);
      brackets(z,t,zt);

      Inv3x3(zz,invzz);
      
      for (i = 0; i < 3; i++)
	for (j = 0; j < 3; j++)
	  {
	    M0[i][j] = tz[i][j]+zt[i][j];
	  }

      Multiply3x3(invzz,M0,tmp);
      Multiply3x3(tmp,invzz,M0);

      for (i = 0; i < 3; i++)
	for (p = 0; p < 3; p++)
	  for (j = 0; j < 3; j++)
	    for (w = 0; w < 3; w++)
	      for (q = 0; q < 3; q++)
		for (r = 0; r < 3; r++)
		  for (s = 0; s < 3; s++)
		    {
		      strain[i][p][k] -= (aniso0->eps[p][j][w]*b[q]*aniso0->C[w][q][r][s]*t[j]*(t[s]*invzz[i][r]-z[s]*M0[i][r]))/(8*M_PI*M_PI);
		    }
    }


  /* Trapezoidal integration */
  Init3x3(straintot);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	for (p = 2; p <= Nint; p++)
	  {
	    straintot[i][j] += dphi*strain[i][j][p];
	  }
	
	straintot[i][j] += 0.5*dphi*(strain[i][j][1]+strain[i][j][Nint+1]);
      }
  
  Init3x3(stress);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
	for (l = 0; l < 3; l++)
	  {
	    stress[i][j] += aniso0->C[i][j][k][l]*straintot[k][l];
	  }

  for (i=0;i<3;i++) 
    for (j=0;j<3;j++) 
      {
	free(strain[i][j]);
      }

}

#else

/* SelForce routine using Steve Fitzgerald method */
void SelfForce_ANI(int coreOnly,real8 MU, real8 NU,
		   real8 bx, real8 by, real8 bz,
		   real8 x1, real8 y1, real8 z1,
 		   real8 x2, real8 y2, real8 z2,
		   real8 a,  real8 Ecore, 
		   real8 f1[3], real8 f2[3])
{
    int i,j;
    real8 tx, ty, tz, L, La, S, FourPi;
    real8 ctheta, cphi, stheta, sphi, G1, G2, G3;
    real8 ethetax, ethetay, ethetaz, ephix, ephiy, ephiz;
    real8 Bmat[3][3];
    real8 Btheta[3][3], Bphi[3][3];
    real8 Bphim[3][3],  Bphip[3][3];
    real8 Bthetaphi[3][3];
    real8 Bthetaup[3][3], Bthetadown[3][3], Bphiup[3][3], Bphidown[3][3];
    real8 t[3], tpp[3], tmm[3], tpm[3], tmp[3], bvec[3]; 
    real8 Bmm[3][3], Bpm[3][3], Bmp[3][3], Bpp[3][3];
    real8 bBb, bBthetab, bBphib;
    real8 dtheta, dphi, tthetaup[3], tthetadown[3], tphiup[3], tphidown[3];
    real8 torsionvec[3], elastorsionvec[3], corelongvec[3], coretorsionvec[3];
    real8 factor;

    FourPi = 4*M_PI; 

    // Delta theta, phi for numerical derivative
    dtheta = 0.000001;
    dphi   = 0.000001;

    GetUnitVector(1, x1, y1, z1, x2, y2, z2, &tx, &ty, &tz, &L);

    // non-singular theory length parameter
    La=sqrt(L*L+a*a);
    
    //Lengthscale dependent parts for elastic torsion (defined to agree 
    //with existing ParaDiS function in isotropic limit)
    G1 = -a*a/(L*La) - L/(2*La);
    G2 = -2*La/L + log((La + L)/a);
    G3 =  a/L;

    // line direction in vector form for QSB call
    t[0] = tx;
    t[1] = ty;
    t[2] = tz;

    // Burgers vector in vector form
    bvec[0] = bx;
    bvec[1] = by;
    bvec[2] = bz;

    //Get sines and cosines of coordinates without using inverse functions
    ctheta = tz;
    if ( fabs(ctheta-1.0) < 1e-3) 
      {
	stheta = 0.0;
	cphi   = 1.0;
	sphi   = 0.0;
      } 
    else if (fabs(ctheta+1.0) < 1e-3) 
      {
	stheta =  0.0;
 	cphi   = -1.0; 
	sphi   =  0.0;
      } 
    else 
      {
	stheta = sqrt(1 - ctheta*ctheta);
	cphi   = tx/stheta;
	sphi   = ty/stheta;
      }

    //These are the four shifted line directions for the numerical derivatives
    tthetaup[0]   = t[0] + dtheta*ctheta*cphi;
    tthetaup[1]   = t[1] + dtheta*ctheta*sphi;
    tthetaup[2]   = t[2] - dtheta*stheta;

    tthetadown[0] = t[0] - dtheta*ctheta*cphi;
    tthetadown[1] = t[1] - dtheta*ctheta*sphi;
    tthetadown[2] = t[2] + dtheta*stheta;

    tphiup[0]     = t[0] - dphi*stheta*sphi;
    tphiup[1]     = t[1] + dphi*stheta*cphi;
    tphiup[2]     = t[2];

    tphidown[0]   = t[0] + dphi*stheta*sphi;
    tphidown[1]   = t[1] - dphi*stheta*cphi;
    tphidown[2]   = t[2];

    //Unit vector in theta direction
    ethetax = cphi*ctheta;
    ethetay = sphi*ctheta;
    ethetaz =     -stheta;

    //In phi direction   
    ephix = -sphi;
    ephiy =  cphi;
    ephiz =  0.0;

    //Get matrix B 
    B_get(t,Bmat);

    //Get matrices Bthetaup etc 
    B_get(tthetaup,Bthetaup);
    B_get(tthetadown,Bthetadown);
    B_get(tphiup,Bphiup);
    B_get(tphidown,Bphidown);

    //Divide by 4 pi (different definitions of B matrix)
    for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
	{
	  Bmat[i][j] /= FourPi;
	  Bthetaup[i][j]   /= FourPi;
	  Bthetadown[i][j] /= FourPi;
	  Bphiup[i][j]     /= FourPi;
	  Bphidown[i][j]   /= FourPi;
	}
	  
    // Perform numerical derivatives dB/dtheta, dB/dphi
    for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
	{
	  Bphi[i][j]   = (Bphiup[i][j]   - Bphidown[i][j])/(2.0*dphi);
	  Btheta[i][j] = (Bthetaup[i][j] - Bthetadown[i][j])/(2.0*dtheta);
	}
  
    //initialize b.B.b etc scalars
    bBb = 0.0;
    bBphib = 0.0;
    bBthetab = 0.0;

    //Dot B and both variations with Burgers' vector on both sides
    for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
 	{
	  bBb      += bvec[i]*Bmat  [i][j]*bvec[j];
	  bBphib   += bvec[i]*Bphi  [i][j]*bvec[j];
	  bBthetab += bvec[i]*Btheta[i][j]*bvec[j];
	}

    //Divide e_phi by sin theta unless sin theta is zero
    //Define torsion vector, same for core and elastic
    if (stheta >  0.00001) //Don't need FABS, 0 < theta < pi
      {
	torsionvec[0] = -bBphib*ephix/stheta - bBthetab*ethetax;
	torsionvec[1] = -bBphib*ephiy/stheta - bBthetab*ethetay;
	torsionvec[2] =                      - bBthetab*ethetaz;// ephiz = 0.0
      }
    else 
      {    
	printf("Special limit being used    \n");
	//possible worry if these are too small for the tabulation, 
	//should be OK though
	//dtheta = 100.0*dtheta;
	//dphi = 100.0*dphi;

	//Use l'Hopital's rule. (dB/dphi)/sin(theta) at theta = 0 is 
	//(d2B/dtheta dphi)/cos(theta) at theta = 0

	//Evaluate t either side of theta = 0 and phi = 0; t++, t--, t+-, t-+

	tmm[0] =-dtheta;
	tmm[1] = dtheta*dphi;
	tmm[2] = 1.0;
		
	tpp[0] = dtheta; 
	tpp[1] = dtheta*dphi;
	tpp[2] = 1.0;
	
	tpm[0] = dtheta;
	tpm[1] =-dtheta*dphi;
	tpm[2] = 1.0;
		
	tmp[0] =-dtheta; 
	tmp[1] =-dtheta*dphi;
	tmp[2] = 1.0;
	  
	//Get matrices either side of theta/phi = 0, B++, B-- etc. 
	B_get(tmm,Bmm);
	B_get(tpp,Bpp);
	B_get(tpm,Bpm);
        B_get(tmp,Bmp);

	for (i = 0; i < 3; i++)
	  for (j = 0; j < 3; j++)
	    {//mixed derivative for l'Hopital's rule
	      Bthetaphi[i][j] = (0.25/(dtheta*dphi))*
		(Bpp[i][j]-Bpm[i][j]-Bmp[i][j]+Bmm[i][j]);
	    }
	
	for (i = 0; i < 3; i++)
	  for (j = 0; j < 3; j++)
	    {
	      //NB the 1.4 is a fudge factor to correct a small error 
	      //of unknown origin
	      
	      Bthetaphi[i][j] = 1.4*Bthetaphi[i][j]/FourPi;
	    }

	//reset bBphib to zero, since it has already been calculated
	bBphib = 0.0;

	// dot with Burgers' vector
	for (i = 0; i < 3; i++)
	  for (j = 0; j < 3; j++)
	    {
	      bBphib +=  bvec[i]*Bthetaphi[i][j]*bvec[j];
	    }
	//no need to divide by sin(dtheta) since it's now cos(dtheta) 
	//=~ 1 from l'Hopital

	//Special case torsion vector
	torsionvec[0] = -bBphib*ephix - bBthetab*ethetax;
	torsionvec[1] = -bBphib*ephiy - bBthetab*ethetay;
	torsionvec[2] =               - bBthetab*ethetaz;
      }

    //Elastic torsion
    if (coreOnly) 
      {
	elastorsionvec[0] = 0.0;
	elastorsionvec[1] = 0.0;
	elastorsionvec[2] = 0.0;
      }
    else 
      {
	factor = 0.5*G2 + 
	         0.5*G1*(1-NU)/NU + 
	         0.5*G3*(1+NU)/NU;
	elastorsionvec[0] = torsionvec[0]*factor;
	elastorsionvec[1] = torsionvec[1]*factor;
	elastorsionvec[2] = torsionvec[2]*factor;
      }


    //Core longitudinal (line tension)
    factor = bBb*FourPi*Ecore/MU;
    corelongvec[0] = -tx*factor;
    corelongvec[1] = -ty*factor;
    corelongvec[2] = -tz*factor;

    //Core torsion
    factor = FourPi*Ecore/MU;
    coretorsionvec[0] = factor*torsionvec[0];
    coretorsionvec[1] = factor*torsionvec[1];
    coretorsionvec[2] = factor*torsionvec[2];
    
    //Output vector forces
    f2[0] = elastorsionvec[0] + coretorsionvec[0] + corelongvec[0];
    f2[1] = elastorsionvec[1] + coretorsionvec[1] + corelongvec[1];
    f2[2] = elastorsionvec[2] + coretorsionvec[2] + corelongvec[2];

    f1[0] = -f2[0];
    f1[1] = -f2[1];
    f1[2] = -f2[2];
}
#endif

/* Seg Seg Force Function */
void SegSegForce_ANI(real8 p1x, real8 p1y, real8 p1z,
		     real8 p2x, real8 p2y, real8 p2z,
		     real8 p3x, real8 p3y, real8 p3z,
		     real8 p4x, real8 p4y, real8 p4z,
		     real8 bpx, real8 bpy, real8 bpz,
		     real8 bx, real8 by, real8 bz,
		     real8 a, real8 MU, real8 NU,
		     int seg12Local, int seg34Local,
		     real8 *fp1x, real8 *fp1y, real8 *fp1z,
		     real8 *fp2x, real8 *fp2y, real8 *fp2z,
		     real8 *fp3x, real8 *fp3y, real8 *fp3z,
		     real8 *fp4x, real8 *fp4y, real8 *fp4z)
{

  /* 
   * First segment starts at p1, ends at p2
   * Second segment starts at p3, ends at p4
   * Burgers vectors b12 = bp and b34 = b
   * Computes forces on nodes f1, f2, f3 and f4
   * Follows Jie's routine RemoteNodeForce_aniso_int.
   */
  
  int i,numIter;
  real8 normA,normB,norm12,norm34;
  real8 f12[6], f34[6];
  real8 param12[20],param34[20];
  real8 tol = LINE_INTEGRATOR_TOL*aniso0->C[0][0][0][0];

  // Compute f1 and f2
  if (seg12Local)
    {
      param12[0]  = p1x;  param12[1]  = p1y;  param12[2]  = p1z;
      param12[3]  = p2x;  param12[4]  = p2y;  param12[5]  = p2z;
      param12[6]  = p3x;  param12[7]  = p3y;  param12[8]  = p3z;
      param12[9]  = p4x;  param12[10] = p4y;  param12[11] = p4z;
      param12[12] = bpx;  param12[13] = bpy;  param12[14] = bpz;
      param12[15] = bx;   param12[16] = by;   param12[17] = bz;
      param12[18] = a;

      LINE_INTEGRATOR(f12,&numIter,EvalWS,0.0,1.0,tol,param12);
    }
  else
    {
      for (i = 0; i < 6; i++) f12[i] = 0.0;
    }

  // Compute f3 and f4
  if (seg34Local)
    {
      param34[0]  = p3x;  param34[1]  = p3y;  param34[2]  = p3z;
      param34[3]  = p4x;  param34[4]  = p4y;  param34[5]  = p4z;
      param34[6]  = p1x;  param34[7]  = p1y;  param34[8]  = p1z;
      param34[9]  = p2x;  param34[10] = p2y;  param34[11] = p2z;
      param34[12] = bx;   param34[13] = by;   param34[14] = bz;
      param34[15] = bpx;  param34[16] = bpy;  param34[17] = bpz;
      param34[18] = a;

      LINE_INTEGRATOR(f34,&numIter,EvalWS,0.0,1.0,tol,param34);
    }
    else
    {
      for (i = 0; i < 6; i++) f34[i] = 0.0;
    }

  *fp1x=f12[0]; *fp1y=f12[1]; *fp1z=f12[2];
  *fp2x=f12[3]; *fp2y=f12[4]; *fp2z=f12[5];

  *fp3x=f34[0]; *fp3y=f34[1]; *fp3z=f34[2];
  *fp4x=f34[3]; *fp4y=f34[4]; *fp4z=f34[5];
}

#endif

