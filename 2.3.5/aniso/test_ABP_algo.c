#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "Typedefs.h"
#include "Home.h"

#include "Param.h"
#include "ANI.h"

#define real8 double


void ANI_WillisSteeds(real8 A[3], real8 B[3], real8 P[3], real8 b[3],
		      real8 sigma[][3],real8 a);


/* define global variable */
Aniso_t *aniso0;

int main(void) {
 
  real8 a;
  real8 A[3],B[3],P[3],b[3];
  real8 s[3][3];
  
  ANI_Alloc(&aniso0);
  ANI_Assign_Cubic_Constants(aniso0, 20.1, 16.0001, 16.0);

  a = 1;
  b[0] = 1; b[1] = 1; b[2] = 1;


  printf("Case B = P\n");
  //A[0] = 2; A[1] = 2; A[2] = 2;
  //B[0] = -1; B[1] =-1; B[2] =-1;
  //P[0] = -1; P[1] = -1; P[2] = -1;

  A[0] = 8.167945709000146e-03; A[1]=8.177425708026931e-03; A[2]=8.002615815485115e-03;
  B[0] = -4.956686219454452e-02; B[1]=-4.955738246772952e-02; B[2]=-4.973284885982553e-02;
  P[0] = -4.956686219454454e-02; P[1]=-4.955738246769670e-02; P[2]=-4.973284885977591e-02;

  printf("A = [ %g  %g  %g ];\n",A[0],A[1],A[2]);
  printf("B = [ %g  %g  %g ];\n",B[0],B[1],B[2]);
  printf("P = [ %g  %g  %g ];\n",P[0],P[1],P[2]);
  printf("b = [ %g  %g  %g ];\n",b[0],b[1],b[2]);
  ANI_WillisSteeds(A,B,P,b,s,a);
  printf("stress = [ %22.14e  %22.14e  %22.14e ];\n",s[0][0],s[0][1],s[0][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[1][0],s[1][1],s[1][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[2][0],s[2][1],s[2][2]);
  printf("\n");

  exit(0);





  printf("Collinear case 1\n");
  A[0] = 1; A[1] = 0; A[2] = 0;
  B[0] = 2; B[1] = 0; B[2] = 0;
  P[0] = 3; P[1] = 0; P[2] = 0;
  printf("A = [ %g  %g  %g ];\n",A[0],A[1],A[2]);
  printf("B = [ %g  %g  %g ];\n",B[0],B[1],B[2]);
  printf("P = [ %g  %g  %g ];\n",P[0],P[1],P[2]);
  printf("b = [ %g  %g  %g ];\n",b[0],b[1],b[2]);
  ANI_WillisSteeds(A,B,P,b,s,a);
  printf("stress = [ %22.14e  %22.14e  %22.14e ];\n",s[0][0],s[0][1],s[0][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[1][0],s[1][1],s[1][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[2][0],s[2][1],s[2][2]);
  printf("\n");
  printf("MATLAB's results\n");
  printf("stress = [  2.31544926461436e-18  -1.95508833631438e-01   1.95508833631438e-01 ];\n");
  printf("         [ -1.95508833631438e-01   5.26224300679418e-17   0.00000000000000e+00 ];\n");
  printf("         [  1.95508833631438e-01   0.00000000000000e+00  -4.43052441522623e-17 ];\n");
  printf("\n\n");

  printf("Collinear case 2\n");
  A[0] = 1; A[1] = 0; A[2] = 0;
  B[0] = 2; B[1] = 0; B[2] = 0;
  P[0] =-1; P[1] = 0; P[2] = 0;
  b[0] = 1; b[1] = 1; b[2] = 1;
  printf("A = [ %g  %g  %g ];\n",A[0],A[1],A[2]);
  printf("B = [ %g  %g  %g ];\n",B[0],B[1],B[2]);
  printf("P = [ %g  %g  %g ];\n",P[0],P[1],P[2]);
  printf("b = [ %g  %g  %g ];\n",b[0],b[1],b[2]);
  ANI_WillisSteeds(A,B,P,b,s,a);
  printf("stress = [ %22.14e  %22.14e  %22.14e ];\n",s[0][0],s[0][1],s[0][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[1][0],s[1][1],s[1][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[2][0],s[2][1],s[2][2]);
  printf("\n");
  printf("MATLAB's results\n");
  printf("stress = [ -1.36202897918491e-19   6.51696112104795e-02  -6.51696112104795e-02 ];\n");
  printf("         [  6.51696112104795e-02  -5.26809318101655e-18   1.85037170770859e-17 ];\n");
  printf("         [ -6.51696112104795e-02   1.85037170770859e-17   4.77884695068246e-18 ];\n");
  printf("\n\n");

  printf("Collinear case 3\n");
  A[0] = 1; A[1] = 0; A[2] = 0;
  B[0] = 2; B[1] = 0; B[2] = 0;
  P[0] = 1.5; P[1] = 0; P[2] = 0;
  b[0] = 1; b[1] = 1; b[2] = 1;
  printf("A = [ %g  %g  %g ];\n",A[0],A[1],A[2]);
  printf("B = [ %g  %g  %g ];\n",B[0],B[1],B[2]);
  printf("P = [ %g  %g  %g ];\n",P[0],P[1],P[2]);
  printf("b = [ %g  %g  %g ];\n",b[0],b[1],b[2]);
  ANI_WillisSteeds(A,B,P,b,s,a);
  printf("stress = [ %22.14e  %22.14e  %22.14e ];\n",s[0][0],s[0][1],s[0][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[1][0],s[1][1],s[1][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[2][0],s[2][1],s[2][2]);
  printf("\n");
  printf("MATLAB's results\n");
  printf("stress = [  4.96323360014982e-16   8.88178419700125e-16   1.77635683940025e-15 ];\n");
  printf("         [  8.88178419700125e-16   1.17416655825292e-15   1.33226762955019e-15 ];\n");
  printf("         [  1.77635683940025e-15   1.33226762955019e-15   6.08646705084481e-16 ];\n");
  printf("\n\n");

  printf("Collinear case 4\n");
  A[0] = 1; A[1] = 0; A[2] = 0;
  B[0] = 2; B[1] = 0; B[2] = 0;
  P[0] = 1; P[1] = 0; P[2] = 0;
  b[0] = 1; b[1] = 1; b[2] = 1;
  printf("A = [ %g  %g  %g ];\n",A[0],A[1],A[2]);
  printf("B = [ %g  %g  %g ];\n",B[0],B[1],B[2]);
  printf("P = [ %g  %g  %g ];\n",P[0],P[1],P[2]);
  printf("b = [ %g  %g  %g ];\n",b[0],b[1],b[2]);
  ANI_WillisSteeds(A,B,P,b,s,a);
  printf("stress = [ %22.14e  %22.14e  %22.14e ];\n",s[0][0],s[0][1],s[0][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[1][0],s[1][1],s[1][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[2][0],s[2][1],s[2][2]);
  printf("\n");
  printf("MATLAB's results\n");
  printf("stress = [  2.20648694627956e-17   3.51915900536589e+00  -3.51915900536589e+00 ];\n");
  printf("         [  3.51915900536589e+00  -4.26794829946203e-16  -4.99600361081320e-16 ];\n");
  printf("         [ -3.51915900536589e+00  -4.99600361081320e-16   5.06052719260324e-16 ];\n");
  printf("\n\n");

  printf("Collinear case 5\n");
  A[0] = 1; A[1] = 0; A[2] = 0;
  B[0] = 2; B[1] = 0; B[2] = 0;
  P[0] = 2; P[1] = 0; P[2] = 0;
  b[0] = 1; b[1] = 1; b[2] = 1;
  printf("A = [ %g  %g  %g ];\n",A[0],A[1],A[2]);
  printf("B = [ %g  %g  %g ];\n",B[0],B[1],B[2]);
  printf("P = [ %g  %g  %g ];\n",P[0],P[1],P[2]);
  printf("b = [ %g  %g  %g ];\n",b[0],b[1],b[2]);
  ANI_WillisSteeds(A,B,P,b,s,a);
  printf("stress = [ %22.14e  %22.14e  %22.14e ];\n",s[0][0],s[0][1],s[0][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[1][0],s[1][1],s[1][2]);
  printf("         [ %22.14e  %22.14e  %22.14e ];\n",s[2][0],s[2][1],s[2][2]);
  printf("\n");
  printf("MATLAB's results\n");
  printf("stress = [  7.60012170385182e-17  -3.51915900536589e+00   3.51915900536589e+00 ];\n");
  printf("         [ -3.51915900536589e+00   6.51712570628321e-16  -4.99600361081320e-16 ];\n");
  printf("         [  3.51915900536589e+00  -4.99600361081320e-16  -3.78713174101902e-16 ];\n");
  printf("\n\n");

}


void ANI_Alloc(Aniso_t **ani_ptr)
{
   Aniso_t *aniso;
   aniso = (Aniso_t *) calloc(1, sizeof(Aniso_t));
   *ani_ptr = aniso;
}

void ANI_Assign_Cubic_Constants(Aniso_t *aniso, real8 C12, real8 C44, real8 Cpr)
{
   int i,j,k,l;

   aniso->C12 = C12;
   aniso->C44 = C44;
   aniso->Cpr = Cpr;

   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
     {
        aniso->delta[i][j] = 0.0;
        for (k = 0; k < 3; k++)
	{
           aniso->eps[i][j][k] = 0.0;
	   for (l = 0; l < 3; l++)
	   {
	      aniso->C[i][j][k][l] = 0.0;
           }
	}
     }

   aniso->delta[0][0] = 1.0;
   aniso->delta[1][1] = 1.0;
   aniso->delta[2][2] = 1.0;

   aniso->eps[0][1][2] = 1.0;
   aniso->eps[1][2][0] = 1.0;
   aniso->eps[2][0][1] = 1.0;

   aniso->eps[2][1][0] =-1.0;
   aniso->eps[1][0][2] =-1.0;
   aniso->eps[0][2][1] =-1.0;

   /* C : elastic constant tensor */
   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
       for (k = 0; k < 3; k++)
         for (l = 0; l < 3; l++)
         {
	   aniso->C[i][j][k][l] += 
	     C12*aniso->delta[i][j]*aniso->delta[k][l]
	   + C44*(aniso->delta[i][k]*aniso->delta[j][l] 
	   + aniso->delta[i][l]*aniso->delta[j][k])

	   /* following line vanishes in isotropic approximation */
	   + 2*(Cpr - C44)*aniso->delta[i][j]*aniso->delta[k][l]*aniso->delta[i][k];
	}

}






/*
 This test's results:

Collinear case 1
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ 3  0  0 ];
b = [ 1  1  1 ];


Called A B P 
Take the collinear case d=0.000000
h=-2.000000
stress = [  -3.14431175378688e-17   -1.95508833631439e-01    1.95508833631439e-01 ];
         [  -1.95508833631439e-01   -4.95613866873592e-17    3.33066907387547e-16 ];
         [   1.95508833631439e-01    3.33066907387547e-16   -6.33835429760301e-17 ];


Collinear case 2
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ -1  0  0 ];
b = [ 1  1  1 ];


Called A B P 
Take the collinear case d=0.000000
h=6.000000
stress = [   1.04810391792896e-17    6.51696112104795e-02   -6.51696112104797e-02 ];
         [   6.51696112104795e-02    1.65204622291197e-17   -9.71445146547012e-17 ];
         [  -6.51696112104797e-02   -9.71445146547012e-17    2.11278476586767e-17 ];



Collinear case 3
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ 1.5  0  0 ];
b = [ 1  1  1 ];


Called A B1 P and A2 B P
Take the collinear case d=0.000000
h=-0.125000
Take the collinear case d=0.000000
h=0.125000
stress = [   0.00000000000000e+00    0.00000000000000e+00    0.00000000000000e+00 ];
         [   0.00000000000000e+00    0.00000000000000e+00    0.00000000000000e+00 ];
         [   0.00000000000000e+00    0.00000000000000e+00    0.00000000000000e+00 ];


Collinear case 4
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ 1  0  0 ];
b = [ 1  1  1 ];


Called A2 B P

Take the collinear case d=0.000000
h=0.111111
stress = [   5.65976115681638e-16    3.51915900536589e+00   -3.51915900536590e+00 ];
         [   3.51915900536589e+00    8.92104960372465e-16   -5.77315972805081e-15 ];
         [  -3.51915900536590e+00   -5.77315972805081e-15    1.14090377356854e-15 ];


Called A B1 P

Take the collinear case d=0.000000
h=-0.111111
stress = [  -5.65976115681638e-16   -3.51915900536589e+00    3.51915900536590e+00 ];
         [  -3.51915900536589e+00   -8.92104960372465e-16    5.77315972805081e-15 ];
         [   3.51915900536590e+00    5.77315972805081e-15   -1.14090377356854e-15 ];


Matlab's result

Collinear 1
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ 3  0  0 ];
b = [ 1  1  1 ];


called ABP
stress = [  2.31544926461436e-18  -1.95508833631438e-01   1.95508833631438e-01 ];
         [ -1.95508833631438e-01   5.26224300679418e-17   0.00000000000000e+00 ];
         [  1.95508833631438e-01   0.00000000000000e+00  -4.43052441522623e-17 ];
Collinear 2
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ -1  0  0 ];
b = [ 1  1  1 ];


called ABP
stress = [ -1.36202897918491e-19   6.51696112104795e-02  -6.51696112104795e-02 ];
         [  6.51696112104795e-02  -5.26809318101655e-18   1.85037170770859e-17 ];
         [ -6.51696112104795e-02   1.85037170770859e-17   4.77884695068246e-18 ];


Collinear 3
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ 1.5  0  0 ];
b = [ 1  1  1 ];


called AB1P and A2BP
stress = [  4.96323360014982e-16   8.88178419700125e-16   1.77635683940025e-15 ];
         [  8.88178419700125e-16   1.17416655825292e-15   1.33226762955019e-15 ];
         [  1.77635683940025e-15   1.33226762955019e-15   6.08646705084481e-16 ];
Collinear 4
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ 1  0  0 ];
b = [ 1  1  1 ];


called A2BP
stress = [  2.20648694627956e-17   3.51915900536589e+00  -3.51915900536589e+00 ];
         [  3.51915900536589e+00  -4.26794829946203e-16  -4.99600361081320e-16 ];
         [ -3.51915900536589e+00  -4.99600361081320e-16   5.06052719260324e-16 ];
Collinear 5
A = [ 1  0  0 ];
B = [ 2  0  0 ];
P = [ 2  0  0 ];
b = [ 1  1  1 ];


called AB1P
stress = [  7.60012170385182e-17  -3.51915900536589e+00   3.51915900536589e+00 ];
         [ -3.51915900536589e+00   6.51712570628321e-16  -4.99600361081320e-16 ];
         [  3.51915900536589e+00  -4.99600361081320e-16  -3.78713174101902e-16 ];


*/
