#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "Home.h"

#define QUADV_MAX_ITER 10000
#define MAX(a,b) ((a)>(b)?(a):(b))

/* to see the source code in Matlab, use "open quadv.m" */

#ifndef RET_VAL_DIM
#define RET_VAL_DIM 6
#endif


real8 NormInf(int n,real8 *A, real8 *B);

int quadv(real8 *Q, int *fcnt, 
          void (*funfcn)(real8, real8*, real8*),
	  real8 a, real8 b, real8 tol, 
          real8 *param); 

int quadstep(real8 *Q, void (*funfcn)(real8, real8*, real8*),
             real8 a, real8 b,
             real8 *fa, real8 *fc, real8 *fb,
             real8 tol, int *fcnt, real8 hmin, real8 *param);

/* 
 * Routine quadv is taken from Matlab quadv. 
 * It computes the integral of a function f on interval [a b] with tolerance tol
 * using the adaptive Simpson quadrature
 * Function f accepts a scalar argument x and returns an array.
 *
 */
int quadv(real8 *Q, int *fcnt, 
          void (*funfcn)(real8, real8*, real8*),
	  real8 a, real8 b, real8 tol, 
          real8 *param)
{
  int warn = 0;
  int warn1, warn2, warn3;
  int i,j,recalculate;
  real8 h,hmin;
  real8 x[7];
  real8 Intf1[RET_VAL_DIM], Intf2[RET_VAL_DIM], Intf3[RET_VAL_DIM];
  real8 y[7][RET_VAL_DIM];

  real8 SmallValue = 1e-20;
  
  // Initialize with three unequal subintervals.
  h = 0.13579*(b-a);

  x[0] = a;
  x[1] = a+h;
  x[2] = a+2*h;
  x[3] =(a+b)/2;
  x[4] = b-2*h;
  x[5] = b-h;
  x[6] = b;

  for (j = 0; j < 7; j++) 
  {
      funfcn(x[j],y[j],param); 
  }
  
  *fcnt = 7;
  
  // Fudge endpoints to avoid infinities.
  recalculate = 0;
  for(i=0;i<RET_VAL_DIM;i++)
     if (isnan(y[1][i]) != 0) recalculate = 1;
  if (recalculate)
  {    
      funfcn(a+SmallValue*(b-a), y[1], param);
      (*fcnt)++;
  }

  recalculate = 0;
  for(i=0;i<RET_VAL_DIM;i++)
     if (isnan(y[6][i]) != 0) recalculate = 1;
  if (recalculate)
  {    
      funfcn(b-SmallValue*(b-a), y[6], param);
      (*fcnt)++;
  }

  // Call the recursive core integrator.
  hmin =  SmallValue;

  warn1 = quadstep(Intf1,funfcn,x[0],x[2],y[0],y[1],y[2],tol,fcnt,hmin,param);
  warn2 = quadstep(Intf2,funfcn,x[2],x[4],y[2],y[3],y[4],tol,fcnt,hmin,param);
  warn3 = quadstep(Intf3,funfcn,x[4],x[6],y[4],y[5],y[6],tol,fcnt,hmin,param);

  for (i = 0; i < RET_VAL_DIM; i++) 
  {
      Q[i] = Intf1[i] + Intf2[i] + Intf3[i];
  }

  warn = MAX(MAX(warn1,warn2),warn3);
  
  switch (warn)
    {
    case 1:
     printf("quadv: Minimum step size reached; singularity possible.\n");
     break;
    case 2:
      printf("quadv: Maximum function count exceeded; singularity likely.\n");
      break;
    case 3:
     printf("quadv: Infinite or Not-a-Number function value encountered.\n");
     exit(0);
     break;
    }
  return warn;
}


/*
 * Taken from Matlab quadstep
 * Recursive function for quadv
 */

int quadstep(real8 *Q, void (*funfcn)(real8, real8*, real8*),
             real8 a, real8 b,
             real8 *fa, real8 *fc, real8 *fb,
             real8 tol, int *fcnt, real8 hmin, real8 *param)
{
  int j;
  int warn = 0, warnac, warncb;
  real8 h, c, d, e, diff;
  real8 fd[RET_VAL_DIM], fe[RET_VAL_DIM];
  real8 Intf1[RET_VAL_DIM], Intf2[RET_VAL_DIM], Intfac[RET_VAL_DIM], Intfcb[RET_VAL_DIM];
  
  // Evaluate integrand twice in interior of subinterval [a,b].
  h = b - a;
  c = (a + b)/2;
  d = (a + c)/2;
  e = (c + b)/2;
  funfcn(d, fd, param);
  funfcn(e, fe, param);

  (*fcnt) += 2;

  // Three point Simpson's rule.
  for (j = 0; j < RET_VAL_DIM; j++)
  {
      Intf1[j] = (h/6.)*(fa[j] + 4*fc[j] + fb[j]);
  }
  
  // Five point double Simpson's rule.
  for (j = 0; j < RET_VAL_DIM; j++)
  {
      Intf2[j] = (h/12.)*(fa[j] + 4*fd[j] + 2*fc[j] + 
			     4*fe[j] + fb[j]);
  }

  // One step of Romberg extrapolation.
  for (j = 0; j < RET_VAL_DIM; j++)
  {
      Q[j] = Intf2[j] + (Intf2[j] - Intf1[j])/15.;
  }
  
  // Check termination criteria.
  for (j = 0; j < RET_VAL_DIM; j++)
  {
      if (isnan(Q[j]) != 0)
	{
	  // Infinite or Not-a-Number function value encountered.
	  warn = 3;
	  return warn;
	}
  }

  if ( *fcnt > QUADV_MAX_ITER)
  {
      // Maximum function count exceeded; singularity likely.
      warn = 2;
      return warn;
  }

  diff = NormInf(RET_VAL_DIM,Intf2,Q);

  if (diff <= tol)
  {
      // Accuracy over this subinterval is acceptable.
      warn = 0;
      return warn;
  }

  if (fabs(h) < hmin || c == a || c == b)
  {
      
      // Minimum step size reached; singularity possible.
      warn = 1;
      return warn;
  }


  // Subdivide into two subintervals.
  warnac = quadstep(Intfac,funfcn,a,c,fa,fd,fc,tol,fcnt,hmin,param);
  warncb = quadstep(Intfcb,funfcn,c,b,fc,fe,fb,tol,fcnt,hmin,param);

  for (j = 0; j < RET_VAL_DIM; j++)
  {
      Q[j] = Intfac[j] + Intfcb[j];
  }

  warn = MAX(warnac,warncb);
  
  return warn;
}




real8 NormInf(int n,real8 *A, real8 *B)
{
  int i;
  real8 norm;

  norm = fabs(A[0]-B[0]);
  for (i = 1; i < n; i++)
    {
      if (norm < fabs(A[i]-B[i])) norm = fabs(A[i]-B[i]); 
    }
  return norm;
}
