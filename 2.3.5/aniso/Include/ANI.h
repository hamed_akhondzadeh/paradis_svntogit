/***************************************************************************
 *
 *  ANI.h : interface between ParaDiS and anisotropic code
 *  (Sylvie Aubry Fri Feb 22 2008)
 * 
 **************************************************************************/

#ifndef _ANI_H
#define _ANI_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Home.h"

#ifdef _ANISOTROPIC

#define SIZETBL 200

struct _anisotropic 
{
  real8 C12;
  real8 C44;
  real8 Cpr;

  real8 delta[3][3];
  real8 eps[3][3][3]; 
  real8 C[3][3][3][3];

#ifdef _USE_QSB_TABLES
  real8 Qtbl[3][3][SIZETBL*SIZETBL];
  real8 Stbl[3][3][(SIZETBL+2)*(SIZETBL+2)];
  real8 Btbl[3][3][SIZETBL*SIZETBL];		
#endif

}*aniso0;

typedef struct _anisotropic Aniso_t;

/* Anisotropic specific functions */
void QSB_get(real8 tdir[3],real8 Q[3][3],real8 S[3][3],real8 B[3][3]);
void QS_get (real8 tdir[3],real8 Q[3][3],real8 S[3][3]);
void B_get  (real8 tdir[3],real8 B[3][3]);

void QSBprime_get(real8 t[3], real8 b[3], real8 n[3],
		  real8 Qp[3][3],real8 Sp[3][3],real8 Bp[3][3]);

void brackets(real8 U[3],real8 V[3],real8 UV[3][3]);
void rhcoord(real8 t[3], real8 n[3], real8 m[3]);
void DefineQSB(Home_t *home, Aniso_t *aniso, char *workingDir0);

#ifdef _USE_QSB_TABLES
void CreateQSB(Home_t *home, Aniso_t *aniso, char *workingDir0);
void ReadQSB(Home_t *home, Aniso_t *aniso, char *workingDir0);

void QSB_interpolate(real8 tdir[3],real8 Q[3][3],real8 S[3][3],real8 B[3][3]);
void QS_interpolate (real8 tdir[3],real8 Q[3][3],real8 S[3][3]);
void B_interpolate  (real8 tdir[3],real8 B[3][3]);
#endif

void QSB_compute(real8 tdir[3],real8 Q[3][3],
		 real8 S[3][3],real8 B[3][3],int flag[3]);

void QS_compute(real8 tdir[3],real8 Q[3][3],real8 S[3][3]);
void Q_compute(real8 tdir[3],real8 Q[3][3]);
void S_compute(real8 tdir[3],real8 S[3][3]);
void B_compute(real8 tdir[3],real8 B[3][3]);

/* ParaDiS routines in ANI_Util.c */
void ANI_Init(Home_t *home, Aniso_t **ani_ptr,char *workingDir0);
void ANI_Alloc(Aniso_t **ani_ptr);
void ANI_Assign_Cubic_Constants(Aniso_t *aniso, real8 C12, real8 C44, real8 Cpr);


/* Anisotropic stress routines */
void ANI_Stress_Seg_WS(real8 A[3], real8 B[3], real8 P[3], real8 b[3],
		       real8 sigma[][3],real8 a);
void ANI_WillisSteeds(real8 A[3], real8 B[3], real8 P[3], real8 b[3],
		      real8 sigma[][3],real8 a);
void EvalWS(real8 s, real8 *f, real8 *param);

void get_angles(real8 t[3],real8 * theta,real8 * phi, int * yflag); 
#ifdef _ANISO_Steve
void getF2mF1(real8 n[3],real8 t1[3], real8 t2[3],
	   real8 F2mF1[3][3][3]);
void coll_F_irs(real8 tdir[3],real8 Fbar_irs[3][3][3]);
#endif

/* Util functions */
void Inv3x3(real8 A[3][3],real8 B[3][3]);
void Multiply3x3(real8 A[3][3],real8 B[3][3], real8 AB[3][3]);
void Transpose3x3(real8 A[3][3],real8 B[3][3]);
void Init3x3(real8 A[3][3]);

void Normalize2(real8 r[3]);
real8 Dot(real8 A[3], real8 B[3], real8 C[3]);
real8 ErrorCalc(real8 *A, real8 *B, int n);
real8 MaxArray(real8 *A, int n);
real8 Chop(real8 x);

void Print3x3x3(char* format,real8 A[3][3][3]);
void Print3x3(char* format,real8 A[3][3]);
void Print3(char* format,real8 A[3]);
void Print6(char *format,real8 A[6]);
void Init3x3(real8 A[3][3]);
void PrintNodesandNeighbors(char *format,Home_t *home);

/* Iso/Aniso Force functions */
void stress_SelfForce_ANI(real8 bx, real8 by, real8 bz,
			  real8 x1, real8 y1, real8 z1,
			  real8 x2, real8 y2, real8 z2,
			  real8 a, real8 stress[3][3]);
void SelfForce_ANI(int coreOnly, real8 MU, real8 NU,
                   real8 bx, real8 by, real8 bz,
                   real8 x1, real8 y1, real8 z1,
                   real8 x2, real8 y2, real8 z2,
                   real8 a,  real8 Ecore,
                   real8 f1[3], real8 f2[3]);
void SegSegForce_ANI(real8 p1x, real8 p1y, real8 p1z,
                   real8 p2x, real8 p2y, real8 p2z,
                   real8 p3x, real8 p3y, real8 p3z,
                   real8 p4x, real8 p4y, real8 p4z,
                   real8 bpx, real8 bpy, real8 bpz,
                   real8 bx, real8 by, real8 bz,
                   real8 a, real8 MU, real8 NU,
                   int seg12Local, int seg34Local,
                   real8 *fp1x, real8 *fp1y, real8 *fp1z,
                   real8 *fp2x, real8 *fp2y, real8 *fp2z,
                   real8 *fp3x, real8 *fp3y, real8 *fp3z,
                   real8 *fp4x, real8 *fp4y, real8 *fp4z);
#else
void SelfForce_ISO(int coreOnly, real8 MU, real8 NU,
                   real8 bx, real8 by, real8 bz,
                   real8 x1, real8 y1, real8 z1,
                   real8 x2, real8 y2, real8 z2,
                   real8 a,  real8 Ecore,
                   real8 f1[3], real8 f2[3]);
void SegSegForce_ISO(real8 p1x, real8 p1y, real8 p1z,
                   real8 p2x, real8 p2y, real8 p2z,
                   real8 p3x, real8 p3y, real8 p3z,
                   real8 p4x, real8 p4y, real8 p4z,
                   real8 bpx, real8 bpy, real8 bpz,
                   real8 bx, real8 by, real8 bz,
                   real8 a, real8 MU, real8 NU,
                   int seg12Local, int seg34Local,
                   real8 *fp1x, real8 *fp1y, real8 *fp1z,
                   real8 *fp2x, real8 *fp2y, real8 *fp2z,
                   real8 *fp3x, real8 *fp3y, real8 *fp3z,
                   real8 *fp4x, real8 *fp4y, real8 *fp4z);
#endif

void InfoNode(Home_t *home,Node_t *node);



real8 DislocationLength(Home_t *home);
#ifdef _CRITICAL_STRESS
void PrintStress(real8 alpha, real8 s[6]);
#endif



#endif /* _ANI_H */
