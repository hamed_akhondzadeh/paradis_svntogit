#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>

#include "ANI.h"

#define real8 double


/* Functions headers */

#ifdef _USE_QSB_TABLES
void ReadQSB2(Aniso_t *aniso);
#endif


void cross(real8 a[3], real8 b[3], real8 c[3]);
real8 Normal(real8 a[3]);
void Init3x3(real8 A[3][3]);
void GetUnitVector(int unitFlag, 
                   real8 x1, real8 y1, real8 z1,
                   real8 x2, real8 y2, real8 z2,
                   real8 *ux, real8 *uy, real8 *uz,
                   real8 *disMag);
void Inv3x3(real8 A[3][3],real8 B[3][3]);
void Multiply3x3(real8 A[3][3],real8 B[3][3], real8 AB[3][3]);
void PKForce(real8 sigb[3],
             real8 x1, real8 y1, real8 z1,
             real8 x2, real8 y2, real8 z2,
             real8 f1[3], real8 f2[3]);
void Transpose3x3(real8 A[3][3],real8 B[3][3]);

void Fatal(char *format, ...) ;
void rhcoord(real8 t[3], real8 n[3], real8 m[3]);
real8 Dot(real8 A[3], real8 B[3], real8 C[3]);
void Normalize(real8 *ax, real8 *ay, real8 *az);
void Normalize2(real8 r[3]);
void Print3(char *format,real8 A[3]);
void Print3x3(char *format,real8 A[3][3]);

/* Functions definition */


void cross(real8 a[3], real8 b[3], real8 c[3])
{
    c[0]=a[1]*b[2]-a[2]*b[1];
    c[1]=a[2]*b[0]-a[0]*b[2];
    c[2]=a[0]*b[1]-a[1]*b[0];
}

real8 Normal(real8 a[3])
{
        return( sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]) );
}


void Init3x3(real8 A[3][3])
{
  int i,j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)  
      {
	A[i][j] = 0.0;
      }  
}


void GetUnitVector(int unitFlag, 
                   real8 x1, real8 y1, real8 z1,
                   real8 x2, real8 y2, real8 z2,
                   real8 *ux, real8 *uy, real8 *uz,
                   real8 *disMag)
{
        *ux = x2 - x1;                  
        *uy = y2 - y1;                  
        *uz = z2 - z1;                  

        *disMag = sqrt(*ux * *ux + *uy * *uy + *uz * *uz);
        
/*
 *      Assumption is that the points involved are node
 *      coordinates.  If the distance between the two points
 *      is too small, it means we have two nodes basically on
 *      top of each other which should not be, so we abort
 *      with an error.
 */
        if (*disMag < 1.0e-10) {                                                

           *ux = 0.0;                                 
           *uy = 0.0;                                 
           *uz = 0.0;

           printf("GetUnitVector:  disMag < min:\n"
                 "    pointA = (%20.15f %20.15f %20.15f)\n"
                 "    pointB = (%20.15f %20.15f %20.15f)",
                 x1, y1, z1, x2, y2, z2);

        } else {                                                

           if (unitFlag == 1) {
              *ux = *ux / *disMag;                      
              *uy = *uy / *disMag;                      
              *uz = *uz / *disMag;                      
           }

        }                                                

        return;
}


void Inv3x3(real8 A[3][3],real8 B[3][3])
{
  int i,j;
  real8 det, cofactor[3][3];

  det=A[0][0]*(A[1][1]*A[2][2]-A[1][2]*A[2][1])
    -A[0][1]*(A[1][0]*A[2][2]-A[1][2]*A[2][0])
    +A[0][2]*(A[1][0]*A[2][1]-A[1][1]*A[2][0]);


  cofactor[0][0]= A[1][1]*A[2][2]-A[1][2]*A[2][1];
  cofactor[0][1]=-A[1][0]*A[2][2]+A[1][2]*A[2][0];
  cofactor[0][2]= A[1][0]*A[2][1]-A[1][1]*A[2][0];

  cofactor[1][0]=-A[0][1]*A[2][2]+A[0][2]*A[2][1];
  cofactor[1][1]= A[0][0]*A[2][2]-A[0][2]*A[2][0];
  cofactor[1][2]=-A[0][0]*A[2][1]+A[2][0]*A[0][1];

  cofactor[2][0]= A[0][1]*A[1][2]-A[1][1]*A[0][2];
  cofactor[2][1]=-A[0][0]*A[1][2]+A[1][0]*A[0][2];
  cofactor[2][2]= A[0][0]*A[1][1]-A[0][1]*A[1][0];


  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
        B[i][j] = cofactor[i][j]/det;
      }
}

void Multiply3x3(real8 A[3][3],real8 B[3][3], real8 AB[3][3])
{
  int i,j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
        AB[i][j] = A[i][0]*B[0][j] + A[i][1]*B[1][j] + A[i][2]*B[2][j];
      }
}


void PKForce(real8 sigb[3],
             real8 x1, real8 y1, real8 z1,
             real8 x2, real8 y2, real8 z2,
             real8 f1[3], real8 f2[3])
{
    real8 xi[3], ft[3];
    int j;

    xi[0] = x2-x1;
    xi[1] = y2-y1;
    xi[2] = z2-z1;

    cross(sigb, xi, ft);

    for (j = 0; j < 3; j++) {
        f1[j] = ft[j] * 0.5;
        f2[j] = ft[j] * 0.5;
    }
}



void Transpose3x3(real8 A[3][3],real8 B[3][3])
{
  int i,j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)  
      {
	B[i][j] = A[j][i];
      }
}

real8 Dot(real8 A[3], real8 B[3], real8 C[3])
{
  /* returns AB.C = (B-A)*C */

  real8 BA[3];
  BA[0] = B[0]-A[0];
  BA[1] = B[1]-A[1];
  BA[2] = B[2]-A[2]; 

  return BA[0]*C[0] + BA[1]*C[1] +BA[2]*C[2];
}

void Normalize2(real8 r[3])
{
  Normalize(&(r[0]),&(r[1]),&(r[2]));
}

void rhcoord(real8 t[3], real8 n[3], real8 m[3])
{
  real8 norm;
  real8 trand[3];
  unsigned int seed;
  int count;
  
  seed = 38945;
  
  norm = Normal(t);  
  if (norm < 1e-8) Fatal("\nNorm of t is zero in rhcoord");
  
  srand(seed++);
  trand[0] = (real8)(rand() / (RAND_MAX + 1.0));
  srand(seed++);
  trand[1] = (real8)(rand() / (RAND_MAX + 1.0));
  srand(seed++);
  trand[2] = (real8)(rand() / (RAND_MAX + 1.0));

  norm = Normal(trand);
  count = 0;
  while (norm < 1e-8)
    {
      srand(seed++);
      trand[0] = (real8)(rand() / (RAND_MAX + 1.0));
      srand(seed++);
      trand[1] = (real8)(rand() / (RAND_MAX + 1.0));
      srand(seed++);
      trand[2] = (real8)(rand() / (RAND_MAX + 1.0));      
      count++;
      if (count > 10) Fatal("Cannot find an acceptable random t ");
      norm = Normal(trand);
    }

  cross(t,trand,m);
  Normalize2(m);

  cross(t,m,n);
  Normalize2(n);
}

void Print3(char *format,real8 A[3])
{
  printf("%s = ", format);
  printf("%.15e %.15e %.15e\n",A[0],A[1],A[2]);
}

void Print3x3(char *format,real8 A[3][3])
{
  printf("\n %s\n", format);
  printf("%.15e %.15e %.15e\n"  ,A[0][0],A[0][1],A[0][2]);
  printf("%.15e %.15e %.15e\n"  ,A[1][0],A[1][1],A[1][2]);
  printf("%.15e %.15e %.15e\n\n",A[2][0],A[2][1],A[2][2]);
}


void Fatal(char *format, ...) 
{
        char    msg[512];
        va_list args;

        va_start(args, format);
        vsnprintf(msg, sizeof(msg)-1, format, args);
        msg[sizeof(msg)-1] = 0;
        va_end(args);
        printf("Fatal: %s\n", msg);
        exit(1);
}


void Normalize(real8 *ax, real8 *ay, real8 *az)
{
        real8 a2, a;

        a2 = ((*ax)*(*ax) + (*ay)*(*ay) + (*az)*(*az));

        if (a2 > 0.0) {
            a = sqrt(a2);
            *ax /= a;
            *ay /= a;
            *az /= a;
        }

        return;
}

#ifdef _USE_QSB_TABLES

void ReadQSB2(Aniso_t *aniso)
{
  int ntheta,nphi,j,k;
  int lenQSB = SIZETBL;
  real8 Q[3][3],S[3][3],B[3][3];
  char workingDir0[512];
  FILE *Qfile,*Sfile,*Bfile;
  char name[512];
  
  printf("\nREADING TABLE FOR Q,S,B ...\n");

  (void *)getcwd(workingDir0, sizeof(workingDir0) - 1);
  
  strcpy(name,workingDir0);
  strcat(name,"/../inputs/Qtable.dat");
  
  printf("Reading Qtable file : %s \n",name);

  Qfile = fopen(name,"r");
  if (Qfile == NULL) 
    {
      printf("\nUnable to open Qtable\n Turn on DEFS += -D_CREATE_QSB_TABLES in makefile to create table\n");
      exit(0);
    }
  

  strcpy(name,workingDir0);
  strcat(name,"/../inputs/Btable.dat");
  
  printf("Reading Btable file : %s \n",name);
  Bfile = fopen(name,"r");

  if (Bfile == NULL) 
    {
      printf("\nUnable to open Btable\n Turn on DEFS += -D_CREATE_QSB_TABLES in makefile to create table\n");
      exit(0);
    }
  
  strcpy(name,workingDir0);
  strcat(name,"/../inputs/Stable.dat");
  
  printf("Reading Btable file : %s \n",name);

  Sfile = fopen(name,"r");
  
  if (Sfile == NULL) 
    {
      printf("\nUnable to open Stable\n Turn on DEFS += -D_CREATE_QSB_TABLES in makefile to create table\n");
      exit(0);
    }
      
  for (ntheta = 0; ntheta < lenQSB; ntheta++)
    for (nphi = 0; nphi < lenQSB; nphi++)
      {
	int t,p;
	fscanf(Qfile,"%d %d\n",&t,&p);
	fscanf(Qfile,"%lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
	       &(Q[0][0]),&(Q[0][1]),&(Q[0][2]),
	       &(Q[1][0]),&(Q[1][1]),&(Q[1][2]),
	       &(Q[2][0]),&(Q[2][1]),&(Q[2][2]));
	
	fscanf(Bfile,"%d %d\n",&t,&p);
	fscanf(Bfile,"%lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
	       &(B[0][0]),&(B[0][1]),&(B[0][2]),
	       &(B[1][0]),&(B[1][1]),&(B[1][2]),
	       &(B[2][0]),&(B[2][1]),&(B[2][2]));
	
	for (j = 0; j < 3; j++)
	  for (k = 0; k < 3; k++)
	    {
	      aniso->Qtbl[j][k][nphi +ntheta*lenQSB]=Q[j][k];
	      aniso->Btbl[j][k][nphi +ntheta*lenQSB]=B[j][k];
	    }
      }
  
  fclose(Qfile);
  fclose(Bfile);
  
  
  for (ntheta = 0; ntheta < lenQSB+2; ntheta++)
    for (nphi = 0; nphi < lenQSB+2; nphi++)
      {
	int p,t;
	fscanf(Sfile,"%d %d\n",&t,&p);
	fscanf(Sfile,"%lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
	       &(S[0][0]),&(S[0][1]),&(S[0][2]),
	       &(S[1][0]),&(S[1][1]),&(S[1][2]),
	       &(S[2][0]),&(S[2][1]),&(S[2][2]));
	
	for (j = 0; j < 3; j++)
	  for (k = 0; k < 3; k++)
	    {
	      aniso->Stbl[j][k][nphi +ntheta*(lenQSB+2)]=S[j][k];
	    }
      }
      
  fclose(Sfile);
  printf("Done.\n\n");
}

#endif
