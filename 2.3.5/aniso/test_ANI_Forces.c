#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "Typedefs.h"
#include "Home.h"

#include "Param.h"
#include "ANI.h"

#define real8 double

#define MaxIter 1000000

void ReadQSB2(Aniso_t *aniso);


int quadv(real8 *Q, int *fcnt, 
          void (*funfcn)(real8, real8*, real8*),
	  real8 a, real8 b, real8 tol, 
          real8 *param); 

int quadstep(real8 *Q, void (*funfcn)(real8, real8*, real8*),
             real8 a, real8 b,
             real8 *fa, real8 *fc, real8 *fb,
             real8 tol, int *fcnt, real8 hmin, real8 *param);


void SegSegForce_ANI(real8 p1x, real8 p1y, real8 p1z,
		     real8 p2x, real8 p2y, real8 p2z,
		     real8 p3x, real8 p3y, real8 p3z,
		     real8 p4x, real8 p4y, real8 p4z,
		     real8 bpx, real8 bpy, real8 bpz,
		     real8 bx, real8 by, real8 bz,
		     real8 a, real8 MU, real8 NU,
		     int seg12Local, int seg34Local,
		     real8 *fp1x, real8 *fp1y, real8 *fp1z,
		     real8 *fp2x, real8 *fp2y, real8 *fp2z,
		     real8 *fp3x, real8 *fp3y, real8 *fp3z,
		     real8 *fp4x, real8 *fp4y, real8 *fp4z);


/* define global variable */
Aniso_t *aniso0;

int main(void) {
 
  ANI_Alloc(&aniso0);

  ANI_Assign_Cubic_Constants(aniso0, 20.1, 16.0001, 16.0);

#ifdef _USE_QSB_TABLES
      ReadQSB2(aniso0);
#endif


  int seg12Local, seg34Local;
  real8 p1x, p1y, p1z;
  real8 p2x, p2y, p2z;
  real8 p3x, p3y, p3z;
  real8 p4x, p4y, p4z;
  real8 bpx, bpy, bpz, bx, by, bz;
  real8 a;
  real8 fp1x, fp1y, fp1z;
  real8 fp2x, fp2y, fp2z;
  real8 fp3x, fp3y, fp3z;
  real8 fp4x, fp4y, fp4z;
  real8 s, param12[20];
  real8 Intf[6];
  
  p1x = 1; p1y = 2; p1z = 3;
  p2x = 2; p2y = 4; p2z = 6;
  p3x = 3; p3y = 5; p3z = 7;
  p4x = 4; p4y = 9; p4z = 5;
  
  bpx = 1; bpy = 1; bpz = 1;
  bx  = 1; by  = 3; bz  = 1;

  a = 0.1;
  
  seg12Local = 1;
  seg34Local = 1;
  
  param12[0]  = p1x;  param12[1]  = p1y;  param12[2]  = p1z;
  param12[3]  = p2x;  param12[4]  = p2y;  param12[5]  = p2z;
  param12[6]  = p3x;  param12[7]  = p3y;  param12[8]  = p3z;
  param12[9]  = p4x;  param12[10] = p4y;  param12[11] = p4z;
  param12[12] = bpx;  param12[13] = bpy;  param12[14] = bpz;
  param12[15] = bx;   param12[16] = by;   param12[17] = bz;
  param12[18] = a;

  s = 0.0;
  EvalWS(s, Intf, param12);
  printf("Integrand at s = %g \n", s);
  printf("Intf = [ %22.14e  %22.14e  %22.14e \n",  Intf[0],Intf[1],Intf[2]);
  printf("         %22.14e  %22.14e  %22.14e ];\n",Intf[3],Intf[4],Intf[5]);

  printf("MATLAB's result\n");
  printf("Intf = [    0.414976564425137 4.100967243980167 -2.872303684128490]\n");
  printf("       [    0                 0                  0                ]\n");
  printf("\n\n\n");

  SegSegForce_ANI(p1x, p1y, p1z, p2x, p2y, p2z,
		  p3x, p3y, p3z, p4x, p4y, p4z,
		  bpx, bpy, bpz, bx, by, bz,
		  a, 0.0, 0.0, seg12Local, seg34Local,
		  &fp1x, &fp1y, &fp1z,
		  &fp2x, &fp2y, &fp2z,
		  &fp3x, &fp3y, &fp3z,
		  &fp4x, &fp4y, &fp4z);
  
  printf("r1 = [ %g  %g  %g ];\n",p1x,p1y,p1z);
  printf("r2 = [ %g  %g  %g ];\n",p2x,p2y,p2z);
  printf("r3 = [ %g  %g  %g ];\n",p3x,p3y,p3z);
  printf("r4 = [ %g  %g  %g ];\n\n\n",p4x,p4y,p4z);

  printf("bp = [ %g  %g  %g ];\n",bpx,bpy,bpz);
  printf("b  = [ %g  %g  %g ];\n\n\n",bx,by,bz);

  printf("Forces \n\n");
  printf("f1 = [ %22.14e  %22.14e  %22.14e ];\n",fp1x,fp1y,fp1z);
  printf("f2 = [ %22.14e  %22.14e  %22.14e ];\n",fp2x,fp2y,fp2z);
  printf("f3 = [ %22.14e  %22.14e  %22.14e ];\n",fp3x,fp3y,fp3z);
  printf("f4 = [ %22.14e  %22.14e  %22.14e ];\n\n\n",fp4x,fp4y,fp4z);
 

  printf("MATLAB's results\n");
  printf("f1 = [  3.33224502628243e-01   3.03256920137573e+00  -2.13278763512657e+00 ]\n");
  printf("f2 = [  6.11748143699590e-01   4.40011152570204e+00  -3.13732373170122e+00 ]\n");
  printf("f3 = [ -9.83574100620518e-01  -1.22320015407890e+00  -2.93818735846805e+00 ]\n");
  printf("f4 = [ -6.02892235491729e-01  -1.15095266297867e+00  -2.60335144370320e+00 ]\n");

}


void ANI_Alloc(Aniso_t **ani_ptr)
{
   Aniso_t *aniso;
   aniso = (Aniso_t *) calloc(1, sizeof(Aniso_t));
   *ani_ptr = aniso;
}

void ANI_Assign_Cubic_Constants(Aniso_t *aniso, real8 C12, real8 C44, real8 Cpr)
{
   int i,j,k,l;

   aniso->C12 = C12;
   aniso->C44 = C44;
   aniso->Cpr = Cpr;

   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
     {
        aniso->delta[i][j] = 0.0;
        for (k = 0; k < 3; k++)
	{
           aniso->eps[i][j][k] = 0.0;
	   for (l = 0; l < 3; l++)
	   {
	      aniso->C[i][j][k][l] = 0.0;
           }
	}
     }

   aniso->delta[0][0] = 1.0;
   aniso->delta[1][1] = 1.0;
   aniso->delta[2][2] = 1.0;

   aniso->eps[0][1][2] = 1.0;
   aniso->eps[1][2][0] = 1.0;
   aniso->eps[2][0][1] = 1.0;

   aniso->eps[2][1][0] =-1.0;
   aniso->eps[1][0][2] =-1.0;
   aniso->eps[0][2][1] =-1.0;

   /* C : elastic constant tensor */
   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
       for (k = 0; k < 3; k++)
         for (l = 0; l < 3; l++)
         {
	   aniso->C[i][j][k][l] += 
	     C12*aniso->delta[i][j]*aniso->delta[k][l]
	   + C44*(aniso->delta[i][k]*aniso->delta[j][l] 
	   + aniso->delta[i][l]*aniso->delta[j][k])

	   /* following line vanishes in isotropic approximation */
	   + 2*(Cpr - C44)*aniso->delta[i][j]*aniso->delta[k][l]*aniso->delta[i][k];
	}

}






/*
 This test's results:

Integrand at s = 0 
Intf = [   4.14976564808147e-01    4.10096724320030e+00   -2.87230368373625e+00 
           0.00000000000000e+00    0.00000000000000e+00   -0.00000000000000e+00 ];
r1 = [ 1  2  3 ];
r2 = [ 2  4  6 ];
r3 = [ 3  5  7 ];
r4 = [ 4  9  5 ];


bp = [ 1  1  1 ];
b  = [ 1  3  1 ];


Forces 

f1 = [   3.33222160837429e-01    3.03257774739168e+00   -2.13279255187360e+00 ];
f2 = [   6.11754969268679e-01    4.40008109731407e+00   -3.13730572129894e+00 ];
f3 = [  -9.83566907112618e-01   -1.22319379382402e+00   -2.93817104120436e+00 ];
f4 = [  -6.02894285970537e-01   -1.15095924990073e+00   -2.60336564278672e+00 ];


Matlab's result

   0.414976564425137
   4.100967243980167
  -2.872303684128490
                   0
                   0
                   0

f1 = [  3.33224502628243e-01   3.03256920137573e+00  -2.13278763512657e+00 ];
f2 = [  6.11748143699590e-01   4.40011152570204e+00  -3.13732373170122e+00 ];
f3 = [ -9.83574100620518e-01  -1.22320015407890e+00  -2.93818735846805e+00 ];
f4 = [ -6.02892235491729e-01  -1.15095266297867e+00  -2.60335144370320e+00 ];

*/
