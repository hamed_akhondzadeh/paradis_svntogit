Anisotropic Elasticity
Sylvie Aubry Jan 28 2010.

The aniso module is similar to DDLab/aniso. For more information refer
to Jie Yin, David Barnett and Wei Cai's paper titled "Computing forces
on dislocation segments in anisotopic elasticity".

The aniso module contains only the files that are modified from the
source code. Mainly, the aniso structure has to be defined, the
Seg-Seg forces and the Self-Force for anisotropic elasticity are
computed.

Several flags are available:

 - D_ANISOTROPIC : turns on or off anisotropic elasticity.

 - You have the option of computing the Q, S, B matrices with or
 without tables. If you want to use tables, then you have to turn on
 the _USE_QSB_TABLES flag. Tables files have to be present in the
 inputs directory. To create the tables, turn on the
 _CREATE_QSB_TABLES flag (along with the _USE_QSB_TABLES flag). Tables
 will be automatically created in the right directory. You can also
 specify the name of the table files in the input file. The syntax is
 as follows:


  TableQ        character     Define the name of the file to read in tables.
                              Defaults is inputs/Qtable.dat
  TableS        character     Define the name of the file to read in tables.
                              Defaults is inputs/Stable.dat
  TableB        character     Define the name of the file to read in tables.
                              Defaults is inputs/Btable.dat

 -Two methods have been implemented for anisotropic elasticity. The
 second one is by Steve Fitzgerald at Culham Centre for Fusion Energy
 +44 (0)1235 466350.  Turn on _ANISO_Steve to run his method.


A test case isalso included in the aniso module, it is called
aniso_fr_src.*. Forces from the Matlab code are also added for
comparison.  The file is compare_aniso_matlab.txt.

Also several tests files are in the aniso module. They verify several
components of the implementation.
			





