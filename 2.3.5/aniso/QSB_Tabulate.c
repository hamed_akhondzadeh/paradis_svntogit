#include <stdio.h>
#include <math.h>
#include "ANI.h"


#ifdef _USE_QSB_TABLES

/*********************************************************************
 * 
 *  Get Q,S,B from tables
 *
 *********************************************************************/

void get_angles(real8 t[3],real8 *theta,real8 *phi,int *yflag)
{
  int i;
  *yflag = 1;

  // use t -> -t if t_y < 0, to ensure we're in 0 < phi < pi
  if (t[1] < -1.e-8)
    {
      for (i = 0; i<3; i++)
	{
	  t[i] = -1.0*t[i];
	}
      *yflag = -1;
    }
  
  // get theta and phi

  *theta = acos(t[2]);

  if (fabs(*theta) < 1.e-10)
    {
      *phi = 0.0;
    }
  else if (fabs(t[0]) < 1.e-10)
    {
      *phi = M_PI*0.5;
    }
  else
    {
      *phi = atan2(t[1],t[0]);
    }
}

void QSB_interpolate(real8 t0[3],real8 Q[3][3],real8 S[3][3],real8 B[3][3])
{ 
  real8 Qdown[3][3],Sdown[3][3],Bdown[3][3];
  real8 Qup[3][3],Sup[3][3],Bup[3][3];
  real8 Stmp[3][3];
  real8 Qtup[3][3],Qpup[3][3],Stup[3][3],Spup[3][3],Btup[3][3],Bpup[3][3];
  real8 dth,dph,omth,omph;
  real8 theta=0.0,phi=0.0;
  real8 del2;
  int yflag;
  int j,k,l,m;
  int lenQSB = SIZETBL;
  real8 end = lenQSB*1.0;
  int thdown,phdown;
  int thup,phup;
  int Sthdown,Sphdown;
  int Sthup,Sphup;

  real8 t[3];
  t[0] = t0[0];
  t[1] = t0[1];
  t[2] = t0[2];
  Normalize2(t);
 
  // Get angles theta,phi
  get_angles(t,&theta,&phi,&yflag);

  thdown=floor(theta*lenQSB/M_PI);thdown=thdown%lenQSB;
  phdown=floor(phi*lenQSB/M_PI);phdown=phdown%lenQSB; 
    
  thup=ceil(theta*lenQSB/M_PI);thup=thup%lenQSB;
  phup=ceil(phi*lenQSB/M_PI);phup=phup%lenQSB;

  //Special for S, since its table is slightly bigger 
  Sthdown=floor(theta*end/M_PI);
  Sphdown=floor(phi*end/M_PI);
    
  Sthup=ceil(theta*end/M_PI);
  Sphup=ceil(phi*end/M_PI);

  // get Q,S lower, mixed and upper values 
  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      {
	Qdown[j][k] = aniso0->Qtbl[j][k][thdown*lenQSB + phdown];
	Bdown[j][k] = aniso0->Btbl[j][k][thdown*lenQSB + phdown];
	Sdown[k][j] = aniso0->Stbl[j][k][Sthdown*(lenQSB+2) + Sphdown];

	Qpup[j][k] =  aniso0->Qtbl[j][k][thdown*lenQSB + phup];
	Bpup[j][k] =  aniso0->Btbl[j][k][thdown*lenQSB + phup];
	Spup[k][j] =  aniso0->Stbl[j][k][Sthdown*(lenQSB+2) + Sphup];

	Qtup[j][k] =  aniso0->Qtbl[j][k][thup*lenQSB + phdown];
	Btup[j][k] =  aniso0->Btbl[j][k][thup*lenQSB + phdown];
	Stup[k][j] =  aniso0->Stbl[j][k][Sthup*(lenQSB+2) + Sphdown];

	Qup[j][k]  =  aniso0->Qtbl[j][k][thup*lenQSB + phup];
	Bup[j][k]  =  aniso0->Btbl[j][k][thup*lenQSB + phup];
	Sup[k][j]  =  aniso0->Stbl[j][k][Sthup*(lenQSB+2) + Sphup];
      }

  // interpolate for Q and S
  // angle - (lower tabulated angle) (same for S anyway)
  dth = theta - (thdown)*M_PI/(lenQSB*1.0);
  dph = phi - (phdown)*M_PI/(lenQSB*1.0);

  if (dth>=M_PI) dth-=M_PI;
  if (dph>=M_PI) dph-=M_PI;

  omth = M_PI/(lenQSB*1.0) - dth;
  omph = M_PI/(lenQSB*1.0) - dph;

  del2 = (M_PI/end)*(M_PI/end);

  for (l = 0; l < 3; l++)
    for (m = 0; m < 3; m++)
      {
	Q[l][m] =   (omth*omph*Qdown[l][m] + omth*dph*Qpup[l][m]
		   +   omph*dth*Qtup[l][m] +   dth*dph*Qup[l][m])/del2;
	B[l][m] =   (omth*omph*Bdown[l][m] + omth*dph*Bpup[l][m]
		   +   omph*dth*Btup[l][m] +   dth*dph*Bup[l][m])/del2;
	Stmp[l][m] =(omth*omph*Sdown[l][m] + omth*dph*Spup[l][m] 
		   +   omph*dth*Stup[l][m] +   dth*dph*Sup[l][m])/del2;
      }

  for (l = 0; l < 3; l++)
    for (m = 0; m < 3; m++)
      {
	S[l][m] = yflag*Stmp[m][l];
      }

}

void QS_interpolate(real8 t0[3],real8 Q[3][3],real8 S[3][3])
{ 
  int j,k,l,m;
  real8 Qdown[3][3],Sdown[3][3];
  real8 Qup[3][3],Sup[3][3];
  real8 Stmp[3][3];
  real8 Qtup[3][3],Qpup[3][3],Stup[3][3],Spup[3][3];
  real8 dth,dph,omth,omph;
  real8 theta=0.0,phi=0.0;
  real8 del2;
  int yflag;
  //real8 pi = 3.14159265356;

  int lenQSB = SIZETBL;
  real8 end = lenQSB*1.0;

  int thdown,phdown;
  int thup,phup;
  int Sthdown,Sphdown;
  int Sthup,Sphup;

  real8 t[3];
  t[0] = t0[0];
  t[1] = t0[1];
  t[2] = t0[2];

  Normalize2(t);

  // Get angles theta,phi
  get_angles(t,&theta,&phi,&yflag);

  thdown=floor(theta*lenQSB/M_PI);thdown=thdown%lenQSB;
  phdown=floor(phi*lenQSB/M_PI);phdown=phdown%lenQSB; 
    
  thup=ceil(theta*lenQSB/M_PI);thup=thup%lenQSB;
  phup=ceil(phi*lenQSB/M_PI);phup=phup%lenQSB;

  //Special for S, since its table is slightly bigger 
  Sthdown=floor(theta*end/M_PI);
  Sphdown=floor(phi*end/M_PI);
    
  Sthup=ceil(theta*end/M_PI);
  Sphup=ceil(phi*end/M_PI);

  // get Q,S lower, mixed and upper values 
  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      {
	Qdown[j][k] = aniso0->Qtbl[j][k][thdown*lenQSB + phdown];
	Sdown[k][j] = aniso0->Stbl[j][k][Sthdown*(lenQSB+2) + Sphdown];

	Qpup[j][k] =  aniso0->Qtbl[j][k][thdown*lenQSB + phup];
	Spup[k][j] =  aniso0->Stbl[j][k][Sthdown*(lenQSB+2) + Sphup];

	Qtup[j][k] =  aniso0->Qtbl[j][k][thup*lenQSB + phdown];
	Stup[k][j] =  aniso0->Stbl[j][k][Sthup*(lenQSB+2) + Sphdown];

	Qup[j][k]  =  aniso0->Qtbl[j][k][thup*lenQSB + phup];
	Sup[k][j]  =  aniso0->Stbl[j][k][Sthup*(lenQSB+2) + Sphup];
      }

  // interpolate for Q and S
  // angle - (lower tabulated angle) (same for S anyway)
  dth = theta - (thdown)*M_PI/(lenQSB*1.0);
  dph = phi - (phdown)*M_PI/(lenQSB*1.0);

  if (dth>=M_PI) dth-=M_PI;
  if (dph>=M_PI) dph-=M_PI;

  omth = M_PI/(lenQSB*1.0) - dth;
  omph = M_PI/(lenQSB*1.0) - dph;

  del2 = (M_PI/end)*(M_PI/end);

  for (l = 0; l < 3; l++)
    for (m = 0; m < 3; m++)
      {
	Q[l][m] =   (omth*omph*Qdown[l][m] + omth*dph*Qpup[l][m]
		   +   omph*dth*Qtup[l][m] +   dth*dph*Qup[l][m])/del2;
	Stmp[l][m] =(omth*omph*Sdown[l][m] + omth*dph*Spup[l][m] 
		   +   omph*dth*Stup[l][m] +   dth*dph*Sup[l][m])/del2;
      }

  for (l = 0; l < 3; l++)
    for (m = 0; m < 3; m++)
      {
	S[l][m] = yflag*Stmp[m][l];
      }

}

void B_interpolate(real8 t0[3],real8 B[3][3])
{ // from table
  real8 Bdown[3][3];
  real8 Bup[3][3];
  real8 Btup[3][3],Bpup[3][3];
  real8 dth,dph,omth,omph;
  real8 theta=0.0,phi=0.0;
  real8 del2;
  int yflag;
  //real8 pi = 3.14159265356;

  int j,k,l,m;
  int lenQSB = SIZETBL;
  real8 end = (lenQSB)*1.0;

  int thdown,phdown;
  int thup,phup;

  real8 t[3];
  t[0] = t0[0];
  t[1] = t0[1];
  t[2] = t0[2];
  Normalize2(t);

  // Get angles theta,phi
  get_angles(t,&theta,&phi,&yflag);

  thdown=floor(theta*lenQSB/M_PI);thdown=thdown%lenQSB;
  thup  =ceil (theta*lenQSB/M_PI);thup=thup%lenQSB;

  phdown=floor(phi*lenQSB/M_PI);
  phdown=phdown%lenQSB; 
  phup  =ceil (phi*lenQSB/M_PI);
  phup=phup%lenQSB;

  // get Q,S lower, mixed and upper values 
  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      {
	Bdown[j][k] = aniso0->Btbl[j][k][thdown*lenQSB + phdown];
	Bpup[j][k] =  aniso0->Btbl[j][k][thdown*lenQSB + phup];
	Btup[j][k] =  aniso0->Btbl[j][k][thup*lenQSB + phdown];
	Bup[j][k]  =  aniso0->Btbl[j][k][thup*lenQSB + phup];
      }

  // interpolate for Q and S
  // angle - (lower tabulated angle) (same for S anyway)
  dth = theta - (thdown)*M_PI/(lenQSB*1.0);
  dph = phi - (phdown)*M_PI/(lenQSB*1.0);

  if (dth>=M_PI) dth-=M_PI;
  if (dph>=M_PI) dph-=M_PI;

  omth = M_PI/(lenQSB*1.0) - dth;
  omph = M_PI/(lenQSB*1.0) - dph;

  del2 = (M_PI/end)*(M_PI/end);

  for (l = 0; l < 3; l++)
    for (m = 0; m < 3; m++)
      {
	B[l][m] =   (omth*omph*Bdown[l][m] + omth*dph*Bpup[l][m]
		   +   omph*dth*Btup[l][m] +   dth*dph*Bup[l][m])/del2;
      }

}



/*********************************************************************
 * 
 *  Create Q,S,B tables
 *
 *********************************************************************/

void DefineQSB(Home_t *home, Aniso_t *aniso, char *workingDir0)
{
#if _CREATE_QSB_TABLES 
  // Create files Qtable.dat,Stable.dat,Btable.dat
  CreateQSB(home, aniso, workingDir0); 
#endif
  
  ReadQSB(home, aniso, workingDir0);
}

void CreateQSB(Home_t *home, Aniso_t *aniso, char *workingDir0)
{
  int ntheta,nphi;
  real8 theta,phi;
  real8 t[3],Q[3][3],S[3][3],B[3][3];
  
  // Only proc 0 creates tables.
  if (home->myDomain == 0) 
    {
      FILE *Qfile,*Sfile,*Bfile;
      char name[512];      
      Param_t *param;
      param = home->param;

      printf("\nCREATING TABLE FOR Q,S,B ...\n");
      printf("Writing %s\n",param->TableQ);
      printf("Writing %s\n",param->TableS);
      printf("Writing %s\n",param->TableB);

      strcpy(name,workingDir0);
      strcat(name,"/");
      strcat(name,param->TableQ);
      Qfile = fopen(name,"w");

      strcpy(name,workingDir0);
      strcat(name,"/");
      strcat(name,param->TableS);
      Sfile = fopen(name,"w");

      strcpy(name,workingDir0);
      strcat(name,"/");
      strcat(name,param->TableB);
      Bfile = fopen(name,"w");

      int lenQSB = SIZETBL;
      
      real8 dtheta = M_PI/(lenQSB*1.0);
      real8 dphi = M_PI/(lenQSB*1.0);
      
      for (ntheta = 0; ntheta < lenQSB; ntheta++)
	for (nphi = 0; nphi < lenQSB; nphi++)
	  {
	    theta = ntheta*dtheta;
	    phi = nphi*dphi;
	    t[0] = sin(theta)*cos(phi);
	    t[1] = sin(theta)*sin(phi);
	    t[2] = cos(theta);
	    
	    Q_compute(t,Q);
	    
	    fprintf(Qfile,"%d %d\n",ntheta+1,nphi+1);
	    fprintf(Qfile,"%.15e %.15e %.15e\n %.15e %.15e %.15e\n%.15e %.15e %.15e\n",
		    Q[0][0],Q[0][1],Q[0][2],Q[1][0],Q[1][1],Q[1][2],
		    Q[2][0],Q[2][1],Q[2][2]);
	    
	    B_compute(t,B);

	    fprintf(Bfile,"%d %d\n",ntheta+1,nphi+1);
	    fprintf(Bfile,"%.15e %.15e %.15e\n %.15e %.15e %.15e\n%.15e %.15e %.15e\n",
		    B[0][0],B[0][1],B[0][2],B[1][0],B[1][1],B[1][2],
		    B[2][0],B[2][1],B[2][2]);
	  }
      
      fclose(Qfile);
      fclose(Bfile);
      
      for (ntheta = 0; ntheta < lenQSB+2; ntheta++)
	for (nphi = 0; nphi < lenQSB+2; nphi++)
	  {
	    theta = ntheta*dtheta;
	    phi = nphi*dphi;
	    t[0] = sin(theta)*cos(phi);
	    t[1] = sin(theta)*sin(phi);
	    t[2] = cos(theta);
	    
	    S_compute(t,S);

	    fprintf(Sfile,"%d %d\n",ntheta+1,nphi+1);
	    fprintf(Sfile,"%.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n",
		    S[0][0],S[0][1],S[0][2],
		    S[1][0],S[1][1],S[1][2],
		    S[2][0],S[2][1],S[2][2]);
	  }
      
      fclose(Sfile);
      
      printf("\nDone.\n\n");
      exit(0);
    }
}

void ReadQSB(Home_t *home, Aniso_t *aniso, char *workingDir0)
{
  int ntheta,nphi,j,k;
  int lenQSB = SIZETBL;
  real8 Q[3][3],S[3][3],B[3][3];

  FILE *Qfile,*Sfile,*Bfile;
  char name[512];
  
  Param_t *param;
  param = home->param;

/*
 *	Only processor zero reads in the table.  It will broadcast the
 *	table (and supporting data) to all other processors
 */

  if (home->myDomain == 0) 
    {
      printf("\nREADING TABLE FOR Q,S,B ...\n");
      printf("Reading file %s\n",param->TableQ);
      printf("Reading file %s\n",param->TableB);
      printf("Reading file %s\n",param->TableS);

      strcpy(name,workingDir0);
      strcat(name,"/");
      strcat(name,param->TableQ);

      Qfile = fopen(name,"r");
      if (Qfile == NULL) 
	{
	  printf("\nUnable to open Qtable\n Turn on DEFS += -D_CREATE_QSB_TABLES in makefile to create table\n");
	  exit(0);
	}

      strcpy(name,workingDir0);
      strcat(name,"/");
      strcat(name,param->TableB);

      Bfile = fopen(name,"r");

      if (Bfile == NULL) 
	{
	  printf("\nUnable to open Btable\n Turn on DEFS += -D_CREATE_QSB_TABLES in makefile to create table\n");
	  exit(0);
	}
      
      
      strcpy(name,workingDir0);
      strcat(name,"/");
      strcat(name,param->TableS);

      Sfile = fopen(name,"r");
      
      if (Sfile == NULL) 
	{
	  printf("\nUnable to open Stable\n Turn on DEFS += -D_CREATE_QSB_TABLES in makefile to create table\n");
	  exit(0);
	}
      
      for (ntheta = 0; ntheta < lenQSB; ntheta++)
	for (nphi = 0; nphi < lenQSB; nphi++)
	  {
	    int t,p;
	    fscanf(Qfile,"%d %d\n",&t,&p);
	    fscanf(Qfile,"%lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		   &(Q[0][0]),&(Q[0][1]),&(Q[0][2]),
		   &(Q[1][0]),&(Q[1][1]),&(Q[1][2]),
		   &(Q[2][0]),&(Q[2][1]),&(Q[2][2]));
	    
	    fscanf(Bfile,"%d %d\n",&t,&p);
	    fscanf(Bfile,"%lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		   &(B[0][0]),&(B[0][1]),&(B[0][2]),
		   &(B[1][0]),&(B[1][1]),&(B[1][2]),
		   &(B[2][0]),&(B[2][1]),&(B[2][2]));
	    
	    for (j = 0; j < 3; j++)
	      for (k = 0; k < 3; k++)
		{
		  aniso->Qtbl[j][k][nphi +ntheta*lenQSB]=Q[j][k];
		  aniso->Btbl[j][k][nphi +ntheta*lenQSB]=B[j][k];
		}
	  }
      
      fclose(Qfile);
      fclose(Bfile);
      
      
      for (ntheta = 0; ntheta < lenQSB+2; ntheta++)
	for (nphi = 0; nphi < lenQSB+2; nphi++)
	  {
	    int p,t;
	    fscanf(Sfile,"%d %d\n",&t,&p);
	    fscanf(Sfile,"%lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		   &(S[0][0]),&(S[0][1]),&(S[0][2]),
		   &(S[1][0]),&(S[1][1]),&(S[1][2]),
		   &(S[2][0]),&(S[2][1]),&(S[2][2]));
	    
	    for (j = 0; j < 3; j++)
	      for (k = 0; k < 3; k++)
		{
		  aniso->Stbl[j][k][nphi +ntheta*(lenQSB+2)]=S[j][k];
		}
	  }
      
      fclose(Sfile);
      printf("Done.\n\n");
    }

#ifdef PARALLEL
	MPI_Bcast(aniso->Qtbl, 3*3*SIZETBL*SIZETBL, MPI_DOUBLE, 0,
		  MPI_COMM_WORLD);
	MPI_Bcast(aniso->Stbl, 3*3*(SIZETBL+2)*(SIZETBL+2), MPI_DOUBLE, 0,
		  MPI_COMM_WORLD);
	MPI_Bcast(aniso->Btbl, 3*3*SIZETBL*SIZETBL, MPI_DOUBLE, 0,
		  MPI_COMM_WORLD);
#endif

}


#endif

