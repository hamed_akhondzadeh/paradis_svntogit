/***************************************************************************
 *
 *  Function    : ANI_FrankRead.c
 *  Description : subroutines for computing critical stress of FR source
 *
 **************************************************************************/
#include <stdio.h>
#include <time.h>
#include "Home.h"
#include "Init.h"

#include "ANI.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

#define _COMPLETE_RESTART 0

#ifdef _CRITICAL_STRESS

void ParadisInit(int argc, char *argv[],Home_t **homeptr);
void ParadisStep(Home_t *home);
void ParadisFinish(Home_t *home);
void PushFreeNodeQ(Home_t *home, Node_t *node);
void WriteRestart(Home_t *home, char *baseFileName, int ioGroup,
         int firstInGroup, int writePrologue, int writeEpilogue);
void cross(real8 a[3], real8 b[3], real8 c[3]);
real8 Normal(real8 a[3]);
static void FreeNodeArrays(Node_t *node);

void RemoveAllNodes(Home_t *home);
int FR_Status(Home_t *home, real8 xi[3], real8 y0);
real8 DislocationLength(Home_t *home);
real8 DislocationArea(Home_t *home, real8 r0[3]);
real8 Find_V_Max(Home_t *home);
real8 Find_Y_Max(Home_t *home, real8 xi[3]);

int Calc_FR_Critical_Stress(int argc, char *argv[], Home_t **homeptr)
{
   Home_t  *home;
   Param_t *param;
   int cycleEnd;
   char ctrlFile[512], dataFile[512], lengthFile[512], areaFile[512];
   InData_t     *inData;
   int i, iter, Niter, status;
   real8 stress0[6], xi[3], r0[3], alpha, alpha_min, alpha_max, dalpha, dalpha_min;
   real8 L0, L, Lstart, y0, A, Astart;
   FILE *fp, *fi, *fl, *fa;

#ifdef _ANISOTROPIC 
   char workingDir0[512];
   Aniso_t *aniso;
#endif

#ifdef FPES_ON
   unmask_std_fpes();
#endif

   home  = *homeptr;
   param = home->param;

   /* get stress orientation */
   for (i = 0; i < 6; i++)
       stress0[i] = param->appliedStress[i];

   /* default values, to be overwritten by file setting.dat */
   Niter = 5; alpha_min = 0; alpha_max = 0.5e-2; alpha = alpha_max; dalpha_min = 1e-5;

   /* read in parameters from file */
   fi = fopen("setting.dat","r");
   fscanf(fi,"%d %lf %lf %lf %lf", &Niter, &alpha_min, &alpha_max, &dalpha, &dalpha_min);
   fclose(fi);

   /* default line direction, to be overwritten by file linedir.dat */
   xi[0] = 0.0;  xi[1] = 1.0; xi[2] = -1.0;

   /* read in line direction from file */
   fi = fopen("linedir.dat","r");
   fscanf(fi,"%lf %lf %lf", xi, xi+1, xi+2);
   fclose(fi);

   /* default line center, to be overwritten by file center.dat */
   r0[0] = 0.0;  r0[1] = 0.0; r0[2] = 0.0;

   /* read in line direction from file */
   fi = fopen("center.dat","r");
   fscanf(fi,"%lf %lf %lf", r0, r0+1, r0+2);
   fclose(fi);

   printf("Entering Calc_FR_Critical_Stress\n\n");
   printf(" max number of steps at each stress is set in .ctrl file"
          " (maxstep = %d)\n\n", param->maxstep);
   printf(" max number of iterations (bisection) is Niter = %d\n", Niter);
   printf(" starting stress range [%8g, %8g]\n",alpha_min, alpha_max);

   /* save initial configuration into file */
   WriteRestart(home, "init", 0, 1, 1, 1);

   /* compute initial dislocation length */
   L0 = DislocationLength(home);
   y0 = Find_Y_Max(home, xi);

   printf("Initial dislocation length L0 = %g  y0 = %g\n", L0, y0);

   if (alpha_min > 0)
   {
      /* load previously relaxed structure at alpha_min */
      sprintf(dataFile, "restart/alpha%.6f.data", alpha_min*1e0); dataFile[14] = '_';
      fi = fopen(dataFile,"r");
      if (fi != NULL)
      {
          fclose(fi);
          printf("read nodes from %s\n",dataFile);
          RemoveAllNodes(home);
          printf("Loading initial nodal data again from %s\n\n",dataFile);
          inData = (InData_t *) calloc(1, sizeof(InData_t));
          ReadNodeDataFile(home, inData, dataFile);
          SortNativeNodes(home);
          CommSendGhosts(home);

          /* turn off remesh */
          //param->remeshRule = -1;
      }
      else
      {
          printf("File %s does not exist!\n",dataFile);
      }
   }

   fp = fopen("Frank-Read.txt","w");
   alpha = alpha_min + dalpha;
   for(iter = 0; iter < Niter; iter ++)
   {
       printf("Running ParaDiS at iter = %d/%d  alpha = %.6f\n\n", iter, Niter, alpha);
       status = -1; /* undecided */
 
       /* set stress according to alpha */
       for (i = 0; i < 6; i++)
           param->appliedStress[i] = stress0[i]*alpha;

       sprintf(lengthFile, "length%.6f.dat", alpha*1e0);
       fl = fopen(lengthFile,"w");

       sprintf(areaFile, "area%.6f.dat", alpha*1e0);
       fa = fopen(areaFile,"w");

       Lstart = DislocationLength(home);
       Astart = DislocationArea(home, r0);

       home->cycle = param->cycleStart; 
       cycleEnd    = param->cycleStart + param->maxstep;
       while(home->cycle < cycleEnd)
       {
           ParadisStep(home);

           L = DislocationLength(home);
           fprintf(fl,"%20.12e\n",L);
           fflush(fl);

           A = DislocationArea(home, r0);
           fprintf(fa,"%20.12e\n",A);
           fflush(fa);

           status = FR_Status(home, xi, y0);

           if ((status == 0) || (status == 1)) break;
     
       }
       fclose(fl);

       //if ((status == -1) && (L<=Lstart)) status = 0;
       if ((status == -1) && (A<=Astart)) status = 0;

       fprintf(fp, "alpha = %8g  status = %2d   [%8g, %8g]\n", 
               alpha, status, alpha_min, alpha_max);
       fflush(fp);

       /* turn off remesh */
       //param->remeshRule = -1;

       /* save configuration into file */
       if (1)
       {
           sprintf(dataFile, "alpha%.6f", alpha*1e0); dataFile[6] = '_';
           param->nodeCount = 0;
           for (i = 0; i < home->newNodeKeyPtr; i++) {
               if (home->nodeKeys[i] != (Node_t *)NULL) {
                   param->nodeCount ++;
               }
           }
           WriteRestart(home, dataFile, 0, 1, 1, 1);
       }

       if (status == 0)
       {
           printf("FR source has reached stable configuration\n\n");
           alpha_min = alpha;
           printf("alpha_min = %g\n", alpha_min);
           printf("increase alpha by (dalpha = %g)\n", dalpha);
           alpha = alpha_min + dalpha;

           /* turn off remesh */
           //param->remeshRule = -1;
       }
       if (status == 1)
       {
           printf("FR source is unstable\n\n");
           alpha_max = alpha;
           dalpha *= 0.1;
           if (dalpha < dalpha_min)
           {
              printf("desired accuracy reached (dalpha_min = %g)\n", dalpha_min);
              break;
           }
           alpha = alpha_min + dalpha;
           printf("return to alpha_min = %g\n", alpha_min);
           printf("increase alpha by (dalpha = %g)\n", dalpha);
       }

       /* reload config if stress changes */
       if ((iter < Niter - 1) && (status == 1))
       {
#if _COMPLETE_RESTART
           ParadisFinish(*homeptr);
           chdir("../..");
           ParadisInit(argc, argv, homeptr);
           home  = *homeptr;
           param = home->param;

#ifdef _ANISOTROPIC  
           (void *)getcwd(workingDir0, sizeof(workingDir0) - 1);
           ANI_Init(home,&aniso,workingDir0);
           aniso0 = aniso;
#endif	

#else /* Read nodal data again from file */
           RemoveAllNodes(home);
           if (alpha_min != 0)
           {
               sprintf(dataFile, "restart/alpha%.6f.data", alpha_min*1e0); dataFile[14]='_';
           }
           else
               strcpy(dataFile, "restart/init.data");
           printf("Loading initial nodal data again from %s\n\n",dataFile);
           inData = (InData_t *) calloc(1, sizeof(InData_t));
           ReadNodeDataFile(home, inData, dataFile);
           SortNativeNodes(home);
           CommSendGhosts(home);
#endif
       }
   }

   fclose(fp);
   printf("Leaving Calc_FR_Critical_Stress\n");
   return  0;
}

void RemoveAllNodes(Home_t *home)
{
   int i;
   for (i = 0; i < home->newNodeKeyPtr; i++) {
       if (home->nodeKeys[i] != (Node_t *)NULL) {
           PushFreeNodeQ(home, home->nodeKeys[i]);
           //FreeNodeArrays(home->nodeKeys[i]);
           //free(home->nodeKeys[i]); 
           home->nodeKeys[i] = NULL;
       }
   }
   /* need to free FreeNodeQ memory to prevent memory leak */
   /* ... */
}

int FR_Status(Home_t *home, real8 xi[3], real8 y0)
{
   int i, status;
   real8 v2, v2max, vmax, y, ymax, tmp;

   /* Check for stability */
   vmax = Find_V_Max(home);

   /* Check for instability */
   ymax = Find_Y_Max(home, xi);
 
   status = -1;                    /* default: status == -1 (undecided) */
   if (vmax < 1e-5 )    status = 0;/* conclude: FR source is stable */
   if (ymax > y0*1.5 ) status = 1;/* conclude: FR source is unstable */

   printf("  vmax = %g  ymax = %g  status = %d\n", vmax, ymax, status);

   return status;
}

real8 Find_V_Max(Home_t *home)
{
   int i, ind;
   real8 v2, v2max, vmax;
   Node_t *node;

   v2max = -1;
   for (i = 0; i < home->newNodeKeyPtr; i++) {
       if (home->nodeKeys[i] != (Node_t *)NULL) {
            node = home->nodeKeys[i];
            v2 = node->vX*node->vX + node->vY*node->vY + node->vZ*node->vZ;
            if (v2>v2max) 
            { 
                v2max = v2;
                ind = i;
            }
       }
   }
   vmax = sqrt(v2max);
   printf("  v[%d] = (%g, %g, %g)  r[%d] = (%g, %g, %g)\n", ind, 
         home->nodeKeys[ind]->vX,home->nodeKeys[ind]->vY,home->nodeKeys[ind]->vZ, ind,
         home->nodeKeys[ind]->x, home->nodeKeys[ind]->y, home->nodeKeys[ind]->z);
   return vmax;
}

real8 Find_Y_Max(Home_t *home, real8 xi[3])
{
   int i;
   real8 y, ymax, tmp;
   Node_t *node;

   tmp = sqrt(xi[0]*xi[0]+xi[1]*xi[1]+xi[2]*xi[2]);
   xi[0] /= tmp;  xi[1] /= tmp;  xi[2] /= tmp;  

   ymax = 0;
   for (i = 0; i < home->newNodeKeyPtr; i++) {
       if (home->nodeKeys[i] != (Node_t *)NULL) {
            node = home->nodeKeys[i];
            if (node->flags == 0) 
            {
                y = abs(node->x*xi[0] + node->y*xi[1] + node->z*xi[2]);
                if (y > ymax) ymax = y;
            }
       }
   }
   return ymax;
}

real8 DislocationLength(Home_t *home)
{
  int i,j;
  real8 length;
  Node_t *node, *nbr;

  length = 0;
  for (i = 0; i < home->newNodeKeyPtr; i++) 
  {
      node = home->nodeKeys[i];
      if (node == (Node_t *)NULL) continue;
      
      for (j = 0; j < node->numNbrs; j++) 
	{
	  nbr = GetNeighborNode(home, node, j);
	  if (node->constraint == 7 && nbr->constraint == 7) continue;
	  
	  if (OrderNodes(node, nbr) >= 0) continue;

	  length += sqrt( (node->x-nbr->x)*(node->x-nbr->x)+
	  	          (node->y-nbr->y)*(node->y-nbr->y)+
		          (node->z-nbr->z)*(node->z-nbr->z) );
	}
  }
  return length;
}

real8 DislocationArea(Home_t *home, real8 r0[3])
{
  int i,j;
  real8 area, r01[3], r02[3], r01xr02[3];
  Node_t *node, *nbr;

  area = 0;
  for (i = 0; i < home->newNodeKeyPtr; i++) 
  {
      node = home->nodeKeys[i];
      if (node == (Node_t *)NULL) continue;
      
      for (j = 0; j < node->numNbrs; j++) 
	{
	  nbr = GetNeighborNode(home, node, j);
	  if (node->constraint == 7 && nbr->constraint == 7) continue;
	  
	  if (OrderNodes(node, nbr) >= 0) continue;

          r01[0] = node->x - r0[0]; r01[1] = node->y - r0[1]; r01[2] = node->z - r0[2];
          r02[0] = nbr->x  - r0[0]; r02[1] = nbr->y  - r0[1]; r02[2] = nbr->z  - r0[2];

          cross(r01, r02, r01xr02);

	  area += Normal(r01xr02);
	}
  }
  return area;
}

/* copied from Topology.c */
static void FreeNodeArrays(Node_t *node)
{
            free(node->nbrTag);
            free(node->burgX);
            free(node->burgY);
            free(node->burgZ);
            free(node->armfx);
            free(node->armfy);
            free(node->armfz);
            free(node->nx);
            free(node->ny);
            free(node->nz);
            free(node->ux);
            free(node->uy);
            free(node->uz);
            free(node->ur);
            free(node->sigbLoc);
            free(node->sigbRem);
            free(node->sigbeLoc);
            free(node->sigbeRem);

            if (node->armCoordIndex != (int *)NULL) {
                free(node->armCoordIndex);
            }

        return;
}
#endif
