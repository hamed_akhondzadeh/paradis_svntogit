/**************************************************************************
 *
 *  Function    : Mobility_FCC_0
 *  Author      : Wei Cai, Seok-Woo Lee (updated 07/14/09)
 *  Description : Generic Mobility Law of FCC metals
 *                Each line has a glide plane from input
 *                and it never changes
 *                If the plane normal is not of {111} type, dislocation
 *                motion is constrained along line direction
 *                If node flag == 7, node velocity is zero
 *
 *  Returns:  0 on success
 *            1 if velcoity could not be determined
 *
 ***************************************************************************/

#include "Home.h"
#include "Util.h"
#include "Mobility.h"
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#define _ENABLE_LINE_CONSTRAINT 0

int Mobility_FCC_0(Home_t *home, Node_t *node)
{
    Param_t *param;
    real8 VelxNode, VelyNode, VelzNode, Veldotlcr;
    int i, j, cst, nc, nconstraint, nlc;
    real8 normX[100], normY[100], normZ[100], normx[100], normy[100], normz[100];
    real8 lineX[100], lineY[100], lineZ[100];
    real8 burgX, burgY, burgZ, a, b;
    real8 dx, dy, dz, lx, ly, lz, lr, LtimesB, Lx, Ly, Lz;
    real8 lcx, lcy, lcz, normdotlc;
    Node_t *nbr;
    real8 MobScrew, MobEdge, Mob;
    real8 bx, by, bz, br, dangle;

    param = home->param;

    Lx=param->Lx;
    Ly=param->Ly;
    Lz=param->Lz;

    MobScrew = param->MobScrew;
    MobEdge  = param->MobEdge;
    
    cst = node->constraint;    // cst: the number of glide constraints
    nc = node->numNbrs ;       // nc : the number of neighbor nodes

    
    /* Fixed node constraint (cst==7) */
    if(cst==7)
    {
        node->vX = node->vY = node->vZ = 0 ;
        return(0);
    }


    /* copy glide plane constraints and determine line constraints */
    for(i=0;i<nc;i++)
    {
        normX[i] = normx[i] = node->nx[i];
        normY[i] = normy[i] = node->ny[i];
        normZ[i] = normz[i] = node->nz[i];

        #define FFACTOR_NORMAL 1.0e-4
        if ( (fabs(fabs(node->nx[i]) - fabs(node->ny[i])) > FFACTOR_NORMAL) ||
             (fabs(fabs(node->ny[i]) - fabs(node->nz[i])) > FFACTOR_NORMAL) )
        { /* not {111} plane */
            nbr=GetNeighborNode(home,node,i);
            lineX[i] = nbr->x - node->x;
            lineY[i] = nbr->y - node->y; 
            lineZ[i] = nbr->z - node->z;
            ZImage (param, lineX+i, lineY+i, lineZ+i);
	}
	else
	{ /* no line constraint */
	    lineX[i] = lineY[i] = lineZ[i] = 0;
	}
    }
    
    /* normalize glide plane normal vectors and lc line vectors*/
    for(i=0;i<nc;i++)
    {
        a=sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);
	b=sqrt(lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i]);

        if(a>0)
        {
            normX[i]/=a;
            normY[i]/=a;
            normZ[i]/=a;

            normx[i]/=a;
            normy[i]/=a;
            normz[i]/=a;
        }
        if(b>0)
        {
            lineX[i]/=b;
            lineY[i]/=b;
            lineZ[i]/=b;
        }
    }


    /* Find independent glide constraints */ 
    nconstraint = nc;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
        }
        #define FFACTOR_ORTH 0.05
        if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH)
        {
            normX[i] = normY[i] = normZ[i] = 0;
            nconstraint--;
        }
    }

    /* Find independent line constraints */
    nlc = 0;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(lineX+i,lineY+i,lineZ+i,lineX[j],lineY[j],lineZ[j]);
        }
        if((lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i])<FFACTOR_ORTH)
        {
            lineX[i] = lineY[i] = lineZ[i] = 0;
        }
        else
        {
            nlc ++;
        }
    }

    /* find total dislocation length times drag coefficent (LtimesB)*/
    LtimesB=0;
    for(j=0;j<nc;j++)
    {
        nbr=GetNeighborNode(home,node,j);
        dx=nbr->x - node->x;
        dy=nbr->y - node->y;
        dz=nbr->z - node->z;
        ZImage (param, &dx, &dy, &dz) ;
        lr=sqrt(dx*dx+dy*dy+dz*dz);
        
        if (lr==0)
        { /* zero arm segment can happen after node split 
           * it is OK to have plane normal vector == 0
           * Skip (do nothing)
           */
        }
        else 
        {
           if((node->nx[j]==0)&&(node->ny[j]==0)&&(node->nz[j]==0))
           {
              printf("Mobility_FCC_0: glide plane norm = 0 for segment with nonzero length lr = %e!\n",lr);
           }

           lx=dx/lr; ly=dy/lr; lz=dz/lr;

           bx = node->burgX[j];
           by = node->burgY[j];
           bz = node->burgZ[j];
           br = sqrt(bx*bx+by*by+bz*bz);
           bx/=br; by/=br; bz/=br; /* unit vector along Burgers vector */

           dangle = fabs(bx*lx+by*ly+bz*lz);
           Mob=MobEdge+(MobScrew-MobEdge)*dangle;

           LtimesB+=(lr/Mob);
	}
    }
    LtimesB/=2;


    /* Velocity is simply proportional to total force per unit length */
    VelxNode = node->fX/LtimesB;
    VelyNode = node->fY/LtimesB;
    VelzNode = node->fZ/LtimesB;
    

    /* Orthogonalize with glide plane constraints */
    for(i=0;i<nc;i++)
    {
        if((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0))
        {
	     Orthogonalize(&VelxNode,&VelyNode,&VelzNode,
                         normX[i],normY[i],normZ[i]);
        }
    }


    /* Any dislocation with glide plane not {111} type can only move along its length
     * This rule includes LC junction which is on {100} plane
     */
#if _ENABLE_LINE_CONSTRAINT
    if(nlc==1)
    { /* only one line constraint */
        for(i=0;i<nc;i++)
        {
            if((lineX[i]!=0)||(lineY[i]!=0)||(lineZ[i]!=0))
            { 
   	        lcx = lineX[i];
	        lcy = lineY[i];
	        lcz = lineZ[i];
                break;
            }
        }

        /* project velocity along line */
        Veldotlcr = VelxNode*lcx+VelyNode*lcy+VelzNode*lcz;
        VelxNode = Veldotlcr*lcx;
        VelyNode = Veldotlcr*lcy;
        VelzNode = Veldotlcr*lcz;

	if (nconstraint<=0)
	{	
            Fatal("MobilityLaw_FCC_0: nconstraint <= 0, nlc = 1 is impossible!");
        }
        else if(nconstraint>=1)
  	{ /* a few plane constraints and one line constraint */
            for(i=0;i<nc;i++)
            {
		normdotlc = normx[i]*lcx + normy[i]*lcy + normz[i]*lcz;
		if(fabs(normdotlc)>FFACTOR_ORTH)
		{
                    /* set velocity to zero if line is not on every plane */
                    VelxNode = VelyNode = VelzNode = 0;
		    break;
		}
                else
                {
                   /* do nothing. Skip */
                }
	    }
	}
    }
    else if (nlc>=2)
    {
        /* Velocity is zero when # of independnet lc constratins is equal to or more than 2 */
        VelxNode = VelyNode = VelzNode = 0;
    }
    else
    { 
        /* nlc == 0, do nothing */
    }
#endif

    node->vX = VelxNode;
    node->vY = VelyNode;
    node->vZ = VelzNode;

    /* Modified mobility law for critical stress calculations
     *  confine lateral motion of nodes, make them evenly distributioned
     */
#ifdef _CRITICAL_STRESS
    Node_t *nbr1, *nbr2;
    real8 x0,y0,z0;
    real8 x1,y1,z1;
    real8 x2,y2,z2;
    real8 x12,y12,z12, norm, tminus[3], tplus[3];
    real8 x01[3],x02[3],t3[3],tn[3];
    real8 thedot,vmag,vn[3],vt[3],signdot;

    if (node->numNbrs == 2)
      {
	nbr1 = GetNeighborNode(home, node, 0);
	nbr2 = GetNeighborNode(home, node, 1);

        /* Wei Cai, 12/13/2010, comment out following two lines */
	//if (nbr1->constraint == 7) return(0);
	//if (nbr2->constraint == 7) return(0);

	x0 = node->x; y0 = node->y; z0 = node->z;
	x1 = nbr1->x; y1 = nbr1->y; z1 = nbr1->z;
	x2 = nbr2->x; y2 = nbr2->y; z2 = nbr2->z;

	//printf("vold = %f %f %f\n",node->vX,node->vY,node->vZ);

	x12 = x2-x1; y12 = y2-y1; z12 = z2-z1;
	norm = sqrt(x12*x12 + y12*y12 + z12*z12);
	if (norm < 1e-3) return (0);
	tminus[0] = x12/norm;
	tminus[1] = y12/norm;
	tminus[2] = z12/norm;

	x01[0] = x1-x0; x01[1] = y1-y0; x01[2] = z1-z0;
	x02[0] = x2-x0; x02[1] = y2-y0; x02[2] = z2-z0;
	tplus[0] = x01[0] + x02[0];
	tplus[1] = x01[1] + x02[1];
	tplus[2] = x01[2] + x02[2];

        // define velocity normal direction
        // compute v_normal
	cross(x01,x02,t3);
	norm = sqrt(t3[0]*t3[0] + t3[1]*t3[1] + t3[2]*t3[2]);
	if (norm < 1e-3) return (0);
	t3[0] /= norm;
	t3[1] /= norm;
	t3[2] /= norm;

	// normal velocity
	cross(t3,tminus,tn);
	thedot = node->vX * tn[0] + node->vY * tn[1] + node->vZ * tn[2];
	vn[0] = thedot * tn[0];
	vn[1] = thedot * tn[1];
	vn[2] = thedot * tn[2];

        // tangent velocity
        // independent from v_normal	
	vmag = 1e-6;
	thedot = tplus[0]*tminus[0]+tplus[1]*tminus[1]+tplus[2]*tminus[2];
	if (thedot >= 0.0) 
	  signdot =  1.0;
	else
	  signdot = -1.0;

	//vt[0] = vmag*signdot*tminus[0];
	//vt[1] = vmag*signdot*tminus[1];
	//vt[2] = vmag*signdot*tminus[2];

        /* Wei Cai, 12/13/2010 */
	vt[0] = vmag*thedot*tminus[0];
	vt[1] = vmag*thedot*tminus[1];
	vt[2] = vmag*thedot*tminus[2];

        //printf("vt = %g,%g,%g\n",vt[0],vt[1],vt[2]);

	// Sum up
	node->vX = vn[0] + vt[0];
	node->vY = vn[1] + vt[1];
	node->vZ = vn[2] + vt[2];

	//printf("vnew = %f %f %f\n\n",node->vX,node->vY,node->vZ);
      }

#endif

    return(0);
}
