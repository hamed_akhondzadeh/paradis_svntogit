#include <stdio.h>
#include <math.h>

#ifdef _ANISOTROPIC
#include "ANI.h"


#ifdef _USE_QSB_TABLES /* Get Q,B,S from tables */

void QSB_get(real8 t[3],real8 Q[3][3],real8 S[3][3],real8 B[3][3])
{ 
  QSB_interpolate(t, Q, S, B);
}
void QS_get(real8 t[3],real8 Q[3][3],real8 S[3][3])
{ 
  QS_interpolate(t, Q, S);
}
void B_get(real8 t[3],real8 B[3][3])
{
  B_interpolate(t, B);
}

#else /* compute Q,B,S from integral formalism */

void QSB_get(real8 t[3],real8 Q[3][3],real8 S[3][3],real8 B[3][3])
{
  int flag[3]={1,1,1};
  QSB_compute(t,Q,S,B,flag);
}

void QS_get(real8 t[3],real8 Q[3][3],real8 S[3][3])
{
  real8 B[3][3];
  int flag[3]={1,1,0};
  QSB_compute(t,Q,S,B,flag);
}

void B_get(real8 t[3],real8 B[3][3])
{
  real8 Q[3][3],S[3][3];
  int flag[3]={0,0,1};
  QSB_compute(t,Q,S,B,flag);
}

#endif  

/* 
 * Compute the derivative of angular stress factor
 * sigma with respect to theta
 */
void QSBprime_get(real8 t[3], real8 b[3], real8 N[3],
		  real8 Qp[3][3],real8 Sp[3][3],real8 Bp[3][3])
{
  int k,Nint;
  int i,j,p,l;
  real8 domega,omega;
  real8 M[3];
  real8 Q[3][3],B[3][3],S[3][3];
  real8 n[3], m[3];
  real8 nn[3][3],mm[3][3],mn[3][3],nm[3][3];
  real8 nt[3][3],tn[3][3],mt[3][3],tm[3][3];
  real8 invnn[3][3], F[3][3], A[3][3], D[3][3], E[3][3];
  real8 Fxnm[3][3], mnFnm[3][3];
  real8 tmp[3][3];

  real8* Qp0[3][3], *Sp0[3][3], *Bp0[3][3];

  cross(N,t,M);
  QSB_get(t,Q,S,B);

  Nint = 21;
  domega = 2*M_PI/Nint;
  
  for (i=0;i<3;i++) 
    for (j=0;j<3;j++) 
      {
	Qp0[i][j] = (double *)malloc(sizeof(double)*(Nint+2));
	Sp0[i][j] = (double *)malloc(sizeof(double)*(Nint+2));
	Bp0[i][j] = (double *)malloc(sizeof(double)*(Nint+2));
      }

  for (k = 1; k <= Nint+1; k++)
    {
      omega = (k-1)*domega;

      m[0] = M[0]*cos(omega)+N[0]*sin(omega);
      m[1] = M[1]*cos(omega)+N[1]*sin(omega);
      m[2] = M[2]*cos(omega)+N[2]*sin(omega);

      n[0] =-M[0]*sin(omega)+N[0]*cos(omega);
      n[1] =-M[1]*sin(omega)+N[1]*cos(omega);
      n[2] =-M[2]*sin(omega)+N[2]*cos(omega);

      brackets(n,n,nn);
      brackets(m,m,mm);
      brackets(m,n,mn);
      brackets(n,m,nm);
      brackets(n,t,nt);
      brackets(t,n,tn);
      brackets(m,t,mt);
      brackets(t,m,tm);

      Inv3x3(nn,invnn);
      
      Init3x3(F);
      for (i = 0; i < 3; i++)
	for (j = 0; j < 3; j++)
	  for (p = 0; p < 3; p++)
	    for (l = 0; l < 3; l++)
	      {
		F[i][j] += invnn[i][p]*(nt[p][l]+tn[p][l])*invnn[l][j];
	      }

      Init3x3(A);
      for (i = 0; i < 3; i++)
	for (j = 0; j < 3; j++)
	  for (p = 0; p < 3; p++)
	    {
	      A[i][j] += invnn[i][p]*(tm[p][j]*sin(omega)-nt[p][j]*cos(omega));
	    }

      Multiply3x3(F,nm,Fxnm);

      Multiply3x3(mn,F,tmp);
      Multiply3x3(tmp,nm,mnFnm);

      Multiply3x3(invnn,nm,tmp);

      Init3x3(D);
      for (i = 0; i < 3; i++)
	for (j = 0; j < 3; j++)
	  for (p = 0; p < 3; p++)
	      {
		D[i][j] += (tn[i][p]*cos(omega)-mt[i][p]*sin(omega))*tmp[p][j];
	      }


      Multiply3x3(mn,A,E);

      for (i = 0; i < 3; i++)
	for (j = 0; j < 3; j++)
	  {
	    Qp0[i][j][k] = -F[i][j]*sin(omega);      
	    Sp0[i][j][k] = -Fxnm[i][j]*sin(omega)+A[i][j];
	    Bp0[i][j][k] = -(mt[i][j]+tm[i][j])*cos(omega)+ mnFnm[i][j]*sin(omega)+D[i][j]-E[i][j];	    
	  }
    }


  /* Trapezoidal integration */
  Init3x3(Bp);
  Init3x3(Sp);
  Init3x3(Qp);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	for (k = 2; k <= Nint; k++)
	  {
	    Qp[i][j] -= Qp0[i][j][k]/Nint;
	    Sp[i][j] -= Sp0[i][j][k]/Nint;
	    Bp[i][j] += Bp0[i][j][k]/Nint;
	  }

	Qp[i][j] -= 0.5*(Qp0[i][j][1]+Qp0[i][j][Nint+1])/Nint;
	Sp[i][j] -= 0.5*(Sp0[i][j][1]+Sp0[i][j][Nint+1])/Nint;
	Bp[i][j] += 0.5*(Bp0[i][j][1]+Bp0[i][j][Nint+1])/Nint;
      }


  for (i=0;i<3;i++) 
    for (j=0;j<3;j++) 
      {
	free(Qp0[i][j]);
	free(Sp0[i][j]);
	free(Bp0[i][j]);
      }

}

#if 0
/* Obsolete */
void brackets(real8 U[3],real8 V[3],real8 UV[3][3],
	      real8 C12,real8 C44,real8 Cpr)
{
  // Bracket matrix UV_jk = U_i * Cijkl * V_l
  // Below only works for cubic materials  

  real8 A = 2*Cpr - C44 + C12;
  real8 UdotV = U[0]*V[0] + U[1]*V[1] + U[2]*V[2];

  UV[0][0]=A*U[0]*V[0]+C44*UdotV;
  UV[1][1]=A*U[1]*V[1]+C44*UdotV;
  UV[2][2]=A*U[2]*V[2]+C44*UdotV;
    
  UV[0][1]=C12*U[0]*V[1]+C44*U[1]*V[0];
  UV[1][2]=C12*U[1]*V[2]+C44*U[2]*V[1];
  UV[0][2]=C12*U[0]*V[2]+C44*U[2]*V[0];
    
  UV[1][0]=C12*U[1]*V[0]+C44*U[0]*V[1];
  UV[2][1]=C12*U[2]*V[1]+C44*U[1]*V[2];
  UV[2][0]=C12*U[2]*V[0]+C44*U[0]*V[2];
}
#endif

void brackets(real8 U[3],real8 V[3],real8 UV[3][3])
{
  // Bracket matrix UV_jk = U_i * Cijkl * V_l
  // Below only works for cubic materials  

  int i,j,k,l;
  
  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      UV[j][k] = 0.0;
  
  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      for (i = 0; i < 3; i++)
	for (l = 0; l < 3; l++)
	  {
	    UV[j][k] += U[i]*aniso0->C[i][j][k][l]*V[l];
	  }
}


#endif
