#include <stdio.h>
#include <math.h>
#include <stdlib.h>


#include "Typedefs.h"
#include "Home.h"

#include "Param.h"
#include "ANI.h"


#define real8 double

void ReadQSB2(Aniso_t *aniso);


/* define global variable */
Aniso_t *aniso0;

int main(void) {

  real8 C12, C44, Cpr;
  real8 tdir[3], Q[3][3], S[3][3], B[3][3];
  C12 = 20.1;  C44 = 16.0001;  Cpr = 16.0;
  
  ANI_Alloc(&aniso0);
  ANI_Assign_Cubic_Constants(aniso0, C12, C44, Cpr);

#ifdef _USE_QSB_TABLES
      ReadQSB2(aniso0);
#endif

    tdir[0] = 1; tdir[1] = 2; tdir[2] = 3;
    
    printf("TESTING QSB_get ROUTINE\n");

    QSB_get(tdir, Q, S, B);

    printf("Q = [ %22.14e  %22.14e  %22.14e ];\n",Q[0][0],Q[0][1],Q[0][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",Q[1][0],Q[1][1],Q[1][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",Q[2][0],Q[2][1],Q[2][2]);
    printf("\n");
    printf("MATLAB's results\n");
    printf("Q = -0.04239345673233  -0.00309330052999  -0.00463994243583]\n");
    printf("    -0.00309330052999  -0.04703338236803  -0.00927987484072]\n");
    printf("    -0.00463994243583  -0.00927987484072  -0.05476654717886]\n");
    printf("\n\n");    

    printf("S = [ %22.14e  %22.14e  %22.14e ];\n",S[0][0],S[0][1],S[0][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",S[1][0],S[1][1],S[1][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",S[2][0],S[2][1],S[2][2]);
    printf("\n");
    printf("MATLAB's results\n");
    printf("S = [   0.00000001011585  -0.24622968399093   0.16415332596821]\n");
    printf("    [   0.24622959485645  -0.00000001618540  -0.08207668617166]\n");
    printf("    [  -0.16415311355595   0.08207660967699   0.00000000606955]\n");
    printf("\n\n");    
    
    printf("B = [ %22.14e  %22.14e  %22.14e ];\n",B[0][0],B[0][1],B[0][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",B[1][0],B[1][1],B[1][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",B[2][0],B[2][1],B[2][2]);
    printf("\n");
    printf("MATLAB's results\n");
    printf("B = [21.73188830594059  -0.88182639221744  -1.32273784364622]\n");
    printf("    [-0.88182639221744  20.40915958188982  -2.64547359369008]\n");
    printf("    [-1.32273784364622  -2.64547359369008  18.20462101339558]\n");
    printf("\n\n\n");    
  

    printf("TESTING QS_get ROUTINE\n");
    QS_get(tdir, Q, S);

    printf("Q = [ %22.14e  %22.14e  %22.14e ];\n",Q[0][0],Q[0][1],Q[0][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",Q[1][0],Q[1][1],Q[1][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",Q[2][0],Q[2][1],Q[2][2]);
    printf("\n");
    printf("MATLAB's results\n");
    printf("Q = -0.04239345673233  -0.00309330052999  -0.00463994243583]\n");
    printf("    -0.00309330052999  -0.04703338236803  -0.00927987484072]\n");
    printf("    -0.00463994243583  -0.00927987484072  -0.05476654717886]\n");
    printf("\n\n");    

    printf("S = [ %22.14e  %22.14e  %22.14e ];\n",S[0][0],S[0][1],S[0][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",S[1][0],S[1][1],S[1][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",S[2][0],S[2][1],S[2][2]);
    printf("\n");
    printf("MATLAB's results\n");
    printf("S = [   0.00000001011585  -0.24622968399093   0.16415332596821]\n");
    printf("    [   0.24622959485645  -0.00000001618540  -0.08207668617166]\n");
    printf("    [  -0.16415311355595   0.08207660967699   0.00000000606955]\n");
    printf("\n\n");    


    printf("TESTING B_get ROUTINE\n");
    B_get(tdir, B);

    printf("B = [ %22.14e  %22.14e  %22.14e ];\n",B[0][0],B[0][1],B[0][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",B[1][0],B[1][1],B[1][2]);
    printf("    [ %22.14e  %22.14e  %22.14e ];\n",B[2][0],B[2][1],B[2][2]);
    printf("\n");
    printf("MATLAB's results\n");
    printf("B = [21.73188830594059  -0.88182639221744  -1.32273784364622]\n");
    printf("    [-0.88182639221744  20.40915958188982  -2.64547359369008]\n");
    printf("    [-1.32273784364622  -2.64547359369008  18.20462101339558]\n");
    printf("\n\n\n");    
  


  printf("copy these lines into Matlab and compare with the results from\n");
  printf("[Q,B,S]=stroh_i(t, c11, c12, c44, 1e4);\n");

}

void ANI_Alloc(Aniso_t **ani_ptr)
{
   Aniso_t *aniso;
   aniso = (Aniso_t *) calloc(1, sizeof(Aniso_t));
   *ani_ptr = aniso;
}

void ANI_Assign_Cubic_Constants(Aniso_t *aniso, real8 C12, real8 C44, real8 Cpr)
{
   int i,j,k,l;

   aniso->C12 = C12;
   aniso->C44 = C44;
   aniso->Cpr = Cpr;

   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
     {
        aniso->delta[i][j] = 0.0;
        for (k = 0; k < 3; k++)
	{
           aniso->eps[i][j][k] = 0.0;
	   for (l = 0; l < 3; l++)
	   {
	      aniso->C[i][j][k][l] = 0.0;
           }
	}
     }

   aniso->delta[0][0] = 1.0;
   aniso->delta[1][1] = 1.0;
   aniso->delta[2][2] = 1.0;

   aniso->eps[0][1][2] = 1.0;
   aniso->eps[1][2][0] = 1.0;
   aniso->eps[2][0][1] = 1.0;

   aniso->eps[2][1][0] =-1.0;
   aniso->eps[1][0][2] =-1.0;
   aniso->eps[0][2][1] =-1.0;

   /* C : elastic constant tensor */
   for (i = 0; i < 3; i++)
     for (j = 0; j < 3; j++)
       for (k = 0; k < 3; k++)
         for (l = 0; l < 3; l++)
         {
	   aniso->C[i][j][k][l] += 
	     C12*aniso->delta[i][j]*aniso->delta[k][l]
	   + C44*(aniso->delta[i][k]*aniso->delta[j][l] 
	   + aniso->delta[i][l]*aniso->delta[j][k])

	   /* following line vanishes in isotropic approximation */
	   + 2*(Cpr - C44)*aniso->delta[i][j]*aniso->delta[k][l]*aniso->delta[i][k];
	}

}




/* Matlab print out for Q,S,B 

t = [1 2 3]; c11 = 52.1; c12 = 20.1; c44 = 16.0001;

Q =

  -0.04239345673233  -0.00309330052999  -0.00463994243583
  -0.00309330052999  -0.04703338236803  -0.00927987484072
  -0.00463994243583  -0.00927987484072  -0.05476654717886


B =

  21.73188830594059  -0.88182639221744  -1.32273784364622
  -0.88182639221744  20.40915958188982  -2.64547359369008
  -1.32273784364622  -2.64547359369008  18.20462101339558


S =

   0.00000001011585  -0.24622968399093   0.16415332596821
   0.24622959485645  -0.00000001618540  -0.08207668617166
  -0.16415311355595   0.08207660967699   0.00000000606955

*/

