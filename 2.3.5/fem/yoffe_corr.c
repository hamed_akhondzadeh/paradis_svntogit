/* remove singularity in yoffe.c */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "FEM.h"

#define sh_image_stress_num__ sh_image_stress_num_ 

/* uncorrected function (with singularity) */
int sh_image_stress_num__(double* rr, double* rsurf, double* rmid, double* surf_norm,
                          int* isign, double* burgers, double* mu, double* nu, double* sigma);

/* static void cross(double A[3], double B[3], double C[3]) */
/* { /\* C = cross (A, B) *\/ */
/*     double AXB[3]; */
/*     AXB[2] = A[0]*B[1] - A[1]*B[0]; */
/*     AXB[0] = A[1]*B[2] - A[2]*B[1]; */
/*     AXB[1] = A[2]*B[0] - A[0]*B[2]; */
/*     C[0] = AXB[0]; */
/*     C[1] = AXB[1]; */
/*     C[2] = AXB[2]; */
/* } */
    
int sh_image_stress_num_corr(double* rr, double* rsurf, double* rmid, double* surf_norm,
                             int* isign, double* burgers, double* mu, double* nu, double* sigma)
{
    int i, k,ret;
    double costheta, dr[3], rand_vec[3], vec1[3], vec2[3], rp[3], drr, rv1, rv2, stress[6];
    double phi[4] = {0.0, M_PI/2.0, M_PI, M_PI*3.0/2.0};

    for(i=0;i<3;i++)
        dr[i] = rsurf[i] - rr[i];
    drr = sqrt(dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2]);
    
    /* skip if field point coincides with surface node */
    if (drr<1.0e-10)
    {
        fprintf(stderr,"Yoffe corrected: drr = %e\n", drr);
        for(i=0;i<6;i++) sigma[i] = 0.0;
        return -1;
    }
    
    costheta = fabs(dr[0]*surf_norm[0] + dr[1]*surf_norm[1] + dr[2]*surf_norm[2]);
    costheta /= sqrt( dr[0]*dr[0] + dr[1]*dr[1] + dr[2]*dr[2] );
    costheta /= sqrt( surf_norm[0]*surf_norm[0] + surf_norm[1]*surf_norm[1] + surf_norm[2]*surf_norm[2] );

    if (fabs(1.0 - costheta) < 1.0e-10)
    { /* this is the condition when the original code will fail */
        //fprintf(stderr,"1 - costheta = %e\n",1-costheta);
        rv1 = 0;
        while (fabs(rv1) < 1e-14)
	  {
            for(i=0;i<3;i++)
	      rand_vec[i] = 2*drand48()-1;
            cross(surf_norm, rand_vec, vec1);
            rv1 = sqrt(vec1[0]*vec1[0] + vec1[1]*vec1[1] + vec1[2]*vec1[2]);
	  }

        for(i=0;i<3;i++)
	  vec1[i] /= rv1;
        
	cross(surf_norm, vec1, vec2);
        rv2 = sqrt(vec2[0]*vec2[0] + vec2[1]*vec2[1] + vec2[2]*vec2[2]);
        
	for(i=0;i<3;i++)
	  vec2[i] /= rv2;

        for(i=0;i<6;i++) sigma[i] = 0.0;

        for(k=0;k<4;k++)
	  {
            for(i=0;i<3;i++)
	      rp[i] = rr[i] + 1.0e-6 * (cos(phi[k])*vec1[i] + sin(phi[k])*vec2[i]);

            ret = sh_image_stress_num__(rp, rsurf, rmid, surf_norm, isign, burgers, mu, nu, stress);
            for(i=0;i<6;i++) sigma[i] += stress[i];
	  }
        for(i=0;i<6;i++) sigma[i] /= 4.0;

    }
    else 
      {
        ret = sh_image_stress_num__(rr, rsurf, rmid, surf_norm, isign, burgers, mu, nu, sigma);
      }
    return ret;
}
