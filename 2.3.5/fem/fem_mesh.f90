!=======================================================================
!                           fem_mesh_allocation
!=======================================================================
subroutine fem_mesh_allocation

    use fem_data
   
   	allocate(fem_element_node(8,fem_number_of_element))
	allocate(fem_node_boundary_condition(3,fem_number_of_node))
	allocate(fem_node_coordinate(3,fem_number_of_node))
	allocate(fem_node_displacement(3,fem_number_of_node))
	allocate(fem_node_force(3,fem_number_of_node))
	allocate(fem_element_stress(6,1,fem_number_of_element))
	allocate(fem_gauss_stress(6,8,fem_number_of_element))
	allocate(fem_gauss_coordinate(3,8,fem_number_of_element))
	allocate(fem_node_stress(6,fem_number_of_node))
	
end subroutine fem_mesh_allocation
!=======================================================================
!                           fem_mesh_initialization
!=======================================================================
subroutine fem_mesh_initialization

    use fem_data
   
    fem_element_node            = 0
	fem_node_boundary_condition = 1
	fem_node_coordinate         = 0.d0
	fem_node_displacement       = 0.d0
	fem_node_force              = 0.d0
	fem_element_stress          = 0.d0
	fem_gauss_stress            = 0.d0
	fem_node_stress             = 0.d0

end subroutine fem_mesh_initialization
!=======================================================================
!                           fem_dd_mesh_allocation
!=======================================================================
subroutine fem_dd_mesh_allocation

    use fem_data
   
   	allocate(fem_element_surface_node(4,6,fem_number_of_element))
	allocate(fem_element_surface_neighbor(6,fem_number_of_element))
	allocate(fem_element_surface_img_id(6,fem_number_of_element))
	allocate(fem_element_surface_app_id(6,fem_number_of_element))
	allocate(fem_node_img_id(fem_number_of_node))
	allocate(fem_node_app_id(fem_number_of_node))
	allocate(fem_element_surface_norm(3,6,fem_number_of_element))
	allocate(fem_node_app_stress(6,fem_number_of_node))
	allocate(fem_node_img_stress(6,fem_number_of_node))
	allocate(fem_node_seg_stress(6,fem_number_of_node))
	allocate(fem_node_ana_stress(6,fem_number_of_node))
	
end subroutine fem_dd_mesh_allocation
!=======================================================================
!                           fem_dd_mesh_initialization
!=======================================================================
subroutine fem_dd_mesh_initialization

    use fem_data
   
	fem_element_surface_node        = 0
	fem_element_surface_neighbor    = 0
	fem_element_surface_img_id      = 0
	fem_element_surface_app_id      = 0
	fem_node_img_id                 = 0
	fem_node_app_id                 = 0
	fem_element_surface_norm        = 0.d0
	fem_node_app_stress             = 0.d0
	fem_node_img_stress             = 0.d0
	fem_node_seg_stress             = 0.d0
	fem_node_ana_stress             = 0.d0

end subroutine fem_dd_mesh_initialization
!=======================================================================
!                           fem_dd_mesh_data
!=======================================================================
subroutine fem_dd_mesh_data
!///
!   get element surface node
!///
!    write(6,'('' fem_get_element_surface_node'')')
    call fem_get_element_surface_node
!///
!   get element surface normal vector
!///
!    write(6,'('' fem_get_element_surface_norm'')')
    call fem_get_element_surface_norm
!///
!   get element surface neighboring element
!///
!    write(6,'('' fem_get_element_surface_neighbor'')')
    call fem_get_element_surface_neighbor
!///
!   get domain_surface_element
!///
!    write(6,'('' fem_get_boundary_surface_association'')')
	call fem_get_boundary_surface_association
!///
!   get surface with image force
!///
!    write(6,'('' fem_get_element_surface_img_id'')')
	call fem_get_element_surface_img_id
!///
!   compute surface with applied force
!///
!    write(6,'('' fem_get_element_surface_app_id'')')
	call fem_get_element_surface_app_id
!///
!   get element surface element nodes
!///
!    write(6,'('' fem_get_element_surface_element_node'')')
	call fem_get_element_surface_element_node
!///
!   get boundary surface element nodes
!///
!    write(6,'('' fem_get_boundary_surface_element_node'')')
	call fem_get_boundary_surface_element_node

end subroutine fem_dd_mesh_data
!=======================================================================
!                        fem_get_element_surface_node
!=======================================================================
subroutine fem_get_element_surface_node

	use fem_data

	implicit none

	integer			n

	do n = 1, fem_number_of_element
	fem_element_surface_node(1,1,n) = fem_element_node(1,n)
	fem_element_surface_node(2,1,n) = fem_element_node(2,n)
	fem_element_surface_node(3,1,n) = fem_element_node(3,n)
	fem_element_surface_node(4,1,n) = fem_element_node(4,n)

	fem_element_surface_node(1,2,n) = fem_element_node(4,n)
	fem_element_surface_node(2,2,n) = fem_element_node(3,n)
	fem_element_surface_node(3,2,n) = fem_element_node(7,n)
	fem_element_surface_node(4,2,n) = fem_element_node(8,n)

	fem_element_surface_node(1,3,n) = fem_element_node(8,n)
	fem_element_surface_node(2,3,n) = fem_element_node(7,n)
	fem_element_surface_node(3,3,n) = fem_element_node(6,n)
	fem_element_surface_node(4,3,n) = fem_element_node(5,n)

	fem_element_surface_node(1,4,n) = fem_element_node(5,n)
	fem_element_surface_node(2,4,n) = fem_element_node(6,n)
	fem_element_surface_node(3,4,n) = fem_element_node(2,n)
	fem_element_surface_node(4,4,n) = fem_element_node(1,n)

	fem_element_surface_node(1,5,n) = fem_element_node(1,n)
	fem_element_surface_node(2,5,n) = fem_element_node(4,n)
	fem_element_surface_node(3,5,n) = fem_element_node(8,n)
	fem_element_surface_node(4,5,n) = fem_element_node(5,n)

	fem_element_surface_node(1,6,n) = fem_element_node(2,n)
	fem_element_surface_node(2,6,n) = fem_element_node(6,n)
	fem_element_surface_node(3,6,n) = fem_element_node(7,n)
	fem_element_surface_node(4,6,n) = fem_element_node(3,n)
	enddo

end subroutine fem_get_element_surface_node
!=======================================================================
!                        fem_get_element_surface_norm
!=======================================================================
subroutine fem_get_element_surface_norm
	use fem_data

	implicit none

	integer			n
	integer			m
	
	integer			i
	integer			j
	real*8          xs(3,4)
	real*8          a(3)
	real*8          b(3)
	real*8          c(3)
	real*8          vector_length

	do n = 1, fem_number_of_element
	do m = 1, 6
	do i = 1, 4
	xs(:,i) = fem_node_coordinate(:,fem_element_surface_node(i,m,n))
	enddo

    a = xs(:,3)-xs(:,1)
    b = xs(:,2)-xs(:,4)
	call cross_product(c,a,b)
	fem_element_surface_norm(:,m,n) = c/vector_length(c)

!    write(6,'('' n m vec ='',2i5,3e15.5)') n,m, fem_element_surface_norm(:,m,n)

    enddo
    enddo
    		
end subroutine fem_get_element_surface_norm
!=======================================================================
!                        fem_get_element_surface_neighbor
!=======================================================================
subroutine fem_get_element_surface_neighbor

	use fem_data

	implicit none

	integer			m
	integer			n
	integer			i
	integer			j
	integer			kount

	do m = 1, fem_number_of_element
	do i = 1, 6
		n = 0
		do while((fem_element_surface_neighbor(i,m).eq.0).and.n.lt.fem_number_of_element)
			n = n + 1
			if(n.ne.m) then
				j = 1
				do while(j.lt.4)
					if(fem_element_surface_node(j,i,m).eq.fem_element_node(1,n).or.			&
					   fem_element_surface_node(j,i,m).eq.fem_element_node(2,n).or.			&
					   fem_element_surface_node(j,i,m).eq.fem_element_node(3,n).or.			&
					   fem_element_surface_node(j,i,m).eq.fem_element_node(4,n).or.			&
					   fem_element_surface_node(j,i,m).eq.fem_element_node(5,n).or.			&
					   fem_element_surface_node(j,i,m).eq.fem_element_node(6,n).or.			&
					   fem_element_surface_node(j,i,m).eq.fem_element_node(7,n).or.			&
					   fem_element_surface_node(j,i,m).eq.fem_element_node(8,n)) then
					    j = j + 1
					else
						j = j + 5
					endif
				enddo

				if(j.eq.4) then
					fem_element_surface_neighbor(i,m) = n
				endif
			endif
		enddo
	enddo
	enddo

end subroutine fem_get_element_surface_neighbor
!=======================================================================
!                      fem_get_boundary_surface_association
!=======================================================================
subroutine fem_get_boundary_surface_association

	use fem_data
	use tecplot_lib

	implicit none

    integer         n
    integer         i
    integer         kount
    
    kount = 0
    do n = 1, fem_number_of_element
    do i = 1, 6
    if(fem_element_surface_neighbor(i,n).eq.0) then
    kount = kount + 1
    endif
    enddo
    enddo

    allocate(fem_boundary_surface_association(2,kount))
    
    kount = 0
    do n = 1, fem_number_of_element
    do i = 1, 6
    if(fem_element_surface_neighbor(i,n).eq.0) then
    kount = kount + 1
    fem_boundary_surface_association(1,kount) = i
    fem_boundary_surface_association(2,kount) = n
    endif
    enddo
    enddo

    fem_number_of_boundary_surface = kount

end subroutine fem_get_boundary_surface_association
!=======================================================================
!                        fem_get_element_surface_img_id
!=======================================================================
subroutine fem_get_element_surface_img_id
	use fem_data

	implicit none

	integer			m
	integer			n
	integer			i
	integer			j
	integer			kount

	fem_element_surface_img_id = 0

	do n = 1, fem_number_of_element
	do i = 1, 6
	if(fem_node_img_id(fem_element_surface_node(1,i,n)).eq.1.and.	&
	   fem_node_img_id(fem_element_surface_node(2,i,n)).eq.1.and.	&
	   fem_node_img_id(fem_element_surface_node(3,i,n)).eq.1.and.	&
	   fem_node_img_id(fem_element_surface_node(4,i,n)).eq.1) then
	fem_element_surface_img_id(i,n) = 1
	endif
	enddo
	enddo

end subroutine fem_get_element_surface_img_id
!=======================================================================
!                        fem_get_element_surface_app_id
!=======================================================================
subroutine fem_get_element_surface_app_id
	use fem_data

	implicit none

	integer			m
	integer			n
	integer			i
	integer			j
	integer			kount

	fem_element_surface_app_id = 0

	do n = 1, fem_number_of_element
	do i = 1, 6
	if(fem_node_app_id(fem_element_surface_node(1,i,n)).eq.1.and.	&
	   fem_node_app_id(fem_element_surface_node(2,i,n)).eq.1.and.	&
	   fem_node_app_id(fem_element_surface_node(3,i,n)).eq.1.and.	&
	   fem_node_app_id(fem_element_surface_node(4,i,n)).eq.1) then
	fem_element_surface_app_id(i,n) = 1
	endif
	enddo
	enddo

end subroutine fem_get_element_surface_app_id
!=======================================================================
!                        fem_get_element_surface_element_node
!=======================================================================
subroutine fem_get_element_surface_element_node
	use fem_data

	implicit none

	integer			m
	integer			n
	integer			i
	integer			j
	integer			kount

    fem_number_of_element_surface = 6*fem_number_of_element
    allocate(fem_element_surface_element_node(4,fem_number_of_element_surface))

	kount = 0
	do n = 1, fem_number_of_element
	do i = 1, 6
	kount = kount + 1
	do j = 1, 4
	fem_element_surface_element_node(j,kount) = fem_element_surface_node(j,i,n)
	enddo
	enddo
	enddo
	
end subroutine fem_get_element_surface_element_node
!=======================================================================
!                        fem_get_boundary_surface_element_node
!=======================================================================
subroutine fem_get_boundary_surface_element_node
	use fem_data

	implicit none

	integer			n
	integer         surface
	integer         element

    allocate(fem_boundary_surface_element_node(4,fem_number_of_boundary_surface))

	do n = 1, fem_number_of_boundary_surface
	surface = fem_boundary_surface_association(1,n)
	element = fem_boundary_surface_association(2,n)
	fem_boundary_surface_element_node(:,n) = fem_element_surface_node(:,surface,element)
	enddo

end subroutine fem_get_boundary_surface_element_node
!=======================================================================
!                        trapezoidal_distribution
!=======================================================================
subroutine trapezoidal_distribution(a,		&
									b,		&
									l,		&
									s,		&
									x,		&
									n)
	implicit none

	real(8)		a
	real(8)		b
	real(8)		l
	real(8)		s
	real(8)		x
	integer		n


	x = (b-a)/l + 1
	n = log(b/a)/log(x) + 1
	s = l*(1-x)/(1-x**n)

end subroutine trapezoidal_distribution
!=======================================================================
!								one_block
!=======================================================================
subroutine one_block(element_node,					&
					 node_coordinate,				&
					 number_of_node,				&
					 number_of_element,				&
					 a,								&
					 b,								&
					 s,								&
					 x,								&
					 na,							&
					 nr)
	implicit none

	integer		number_of_node
	integer		number_of_element
	integer		element_node(8,number_of_element)		
	real(8)		node_coordinate(3,number_of_node)
	real(8)		a
	real(8)		b
	real(8)		s
	real(8)		x
	integer		na
	integer		nr

	integer		i
	integer		j
	integer		k
	integer		m
	integer		n

	integer		n0
	integer		n1
	integer		n2
	real(8)		r0
	real(8)		r1
	real(8)		r2
	real(8)		rr
	real(8)		cof(number_of_node)

	do k = 1, nr+1
	do j = 1, na+1
	do i = 1, na+1
	n = (k-1)*(na+1)*(na+1)+(j-1)*(na+1)+i

	if(k.eq.1) then
	node_coordinate(3,n) = a
	else
	node_coordinate(3,n) = a + s*(1-x**float(k-1))/(1-x)
	endif

	node_coordinate(1,n) = node_coordinate(3,n)*(i-1)/float(na) 
	node_coordinate(2,n) = node_coordinate(3,n)*(j-1)/float(na)

	enddo
	enddo
	enddo

	do k = 1, nr
	do j = 1, na
	do i = 1, na
!///
!	connectivity, i.e., relation of local nodes and global nodes
!///
	n = (k-1)*na*na+(j-1)*na+i
	m = (k-1)*(na+1)*(na+1)+(j-1)*(na+1)+i
	element_node(1,n) = m
	element_node(2,n) = m + (na+1)*(na+1)
	element_node(3,n) = m + (na+1)*(na+1)+1
	element_node(4,n) = m + 1
	element_node(5,n) = m + na + 1
	element_node(6,n) = m + na + 1 + (na+1)*(na+1)
	element_node(7,n) = m + na + 1 + (na+1)*(na+1) + 1
	element_node(8,n) = m + na + 1 + 1

	enddo
	enddo
	enddo
!///
!   transform to the void in the cube 
!///
	do k = 1, nr+1
	do j = 1, na+1
	do i = 1, na+1

	n0 = (k-1)*(na+1)*(na+1)+(j-1)*(na+1)+i
	n1 = (j-1)*(na+1)+i
	n2 = nr*(na+1)*(na+1)+(j-1)*(na+1)+i
	r0 = sqrt(node_coordinate(1,n0)**2+node_coordinate(2,n0)**2+node_coordinate(3,n0)**2)
	r1 = sqrt(node_coordinate(1,n1)**2+node_coordinate(2,n1)**2+node_coordinate(3,n1)**2)
    r2 = sqrt(node_coordinate(1,n2)**2+node_coordinate(2,n2)**2+node_coordinate(3,n2)**2)
	rr = a + (r0-r1)*(r2-a)/(r2-r1) 
	cof(n0) = rr/r0

	enddo
	enddo
	enddo

	do k = 1, nr+1
	do j = 1, na+1
	do i = 1, na+1

	n0 = (k-1)*(na+1)*(na+1)+(j-1)*(na+1)+i
	node_coordinate(1,n0) = node_coordinate(1,n0)*cof(n0)
	node_coordinate(2,n0) = node_coordinate(2,n0)*cof(n0)
	node_coordinate(3,n0) = node_coordinate(3,n0)*cof(n0)

	enddo
	enddo
	enddo


end subroutine one_block
!==========================================================================
!                           joint_hex_mesh
!==========================================================================
subroutine joint_hex_mesh(xtmp,				&
						  xsub,				&
						  ixtmp,			&
						  ixsub,			&
						  numnodtmp,		&
						  numnodsub,		&
						  numelmtmp,		&
						  numelmsub,		&
						  maxnod,			&
						  maxelm,			&
						  djoint)

	implicit none

	integer		maxnod
	integer		maxelm

	real*8		xtmp(3,maxnod)
	real*8		xsub(3,maxnod)
	integer		ixtmp(8,maxelm)
	integer		ixsub(8,maxelm)

	integer		numnodtmp
	integer		numnodsub
	integer		numelmtmp
	integer		numelmsub

	real*8		djoint

	integer		i
	integer		n
	integer		kount
	integer		match_node
	integer		mapnod(maxnod)

    kount = numnodtmp
    do i = 1,numnodsub
	call check_node(xtmp,		&
					maxnod,		&
					xsub(1,i),	&
					numnodtmp,	&
					djoint,		&
					match_node)
    if(match_node.eq.0) then
    kount  = kount + 1
    xtmp(1,kount)   = xsub(1,i)
    xtmp(2,kount)   = xsub(2,i)
    xtmp(3,kount)   = xsub(3,i)
    mapnod(i) = kount
    else
    mapnod(i) = match_node
    end if
    enddo
    numnodtmp = kount

    kount = numelmtmp
    do i = 1,numelmsub
    kount = kount+1
	do n = 1, 8
    ixtmp(n,kount) = mapnod(ixsub(n,i))
	enddo
	enddo
    numelmtmp = kount

end subroutine joint_hex_mesh
!==========================================================================
!                              check_node
!==========================================================================
subroutine check_node(x,		&
                      maxnod,	&
                      xc,		&
                      numnod,	&
                      epsl,		&
                      match_node)
    implicit none

	integer		maxnod
	real*8		x(3,maxnod)
	real*8		xc(3)
	integer		numnod
	real*8		epsl
    real*8		tol
	integer		match_node
	integer		i

    tol = 1.d-2*epsl
	match_node = 0
    do i = 1,numnod
    if(dsqrt((x(1,i)-xc(1))**2 +(x(2,i)-xc(2))**2+(x(3,i)-xc(3))**2).lt.tol) then
    match_node = i
	exit
    end if
    enddo
end subroutine check_node
!===========================================================================
!	                       yz_reflect_mesh 
!===========================================================================
subroutine yz_reflect_brick_mesh(x,				&
                                 ix,			&
						         maxnod,		&
                                 maxelm,		&
                                 numnod,		&
                                 numelm,		&
						         tol,			&
						         llog)
	implicit none

	integer		maxnod
	integer		maxelm
	integer		numnod
	integer		numelm
	real*8		tol
	integer		llog

	real*8		x(3,maxnod)
	integer		ix(8,maxelm)

	integer		i
	integer		kount

	integer, allocatable :: mapnod(:) 

	allocate(mapnod(maxnod))

	kount = numnod
	do i = 1, numnod
	if(dabs(x(1,i)).gt.tol*1.d-2) then
	kount = kount + 1
		if (kount.gt.maxnod) then
		write(llog,'(//'' ***error*** detected by subroutine yz_reflect_mesh:'', &
                             /'' number of nodes '',i5,'' exceeds maximum '',i5)') kount,maxnod
		stop
		endif
	x(1,kount)  =-x(1,i) 
	x(2,kount)  = x(2,i) 
	x(3,kount)  = x(3,i)
	mapnod(i)  = kount
	else
	x(1,i)  = 0.d0
	mapnod(i) = i
	end if
	enddo
	numnod = kount

	kount = numelm
	do i = 1,numelm
	kount = kount + 1
		if (kount.gt.maxelm) then
		write(llog,'(//'' ***error*** detected by subroutine yz_reflect_mesh:'', &
                             /'' number of element '',i5,'' exceeds maximum '',i5)') kount,maxelm
		stop
		endif
	ix(1,kount) = mapnod(ix(4,i))
	ix(2,kount) = mapnod(ix(3,i))
	ix(3,kount) = mapnod(ix(2,i))
	ix(4,kount) = mapnod(ix(1,i))
	ix(5,kount) = mapnod(ix(8,i))
	ix(6,kount) = mapnod(ix(7,i))
	ix(7,kount) = mapnod(ix(6,i))
	ix(8,kount) = mapnod(ix(5,i))
	enddo
	numelm = kount

	deallocate(mapnod)

end subroutine yz_reflect_brick_mesh
!===========================================================================
!	                       xz_reflect_brick_mesh 
!===========================================================================
subroutine xz_reflect_brick_mesh(x,				&
                                 ix,			&
						         maxnod,		&
                                 maxelm,		&
                                 numnod,		&
                                 numelm,		&
					             tol,			&
					             llog)
	implicit none

	integer		maxnod
	integer		maxelm
	integer		numnod
	integer		numelm
	real*8		tol
	integer		llog

	real*8		x(3,maxnod)
	integer		ix(8,maxelm)

	integer		i
	integer		kount

	integer, allocatable :: mapnod(:) 

	allocate(mapnod(maxnod))

	kount = numnod
	do i = 1, numnod
	if(dabs(x(2,i)).gt.tol*1.d-2) then
	kount = kount + 1
		if (kount.gt.maxnod) then
		write(llog,'(//'' ***error*** detected by subroutine xz_reflect_mesh:'', &
                              /'' number of nodes '',i5,'' exceeds maximum '',i5)') kount,maxnod
		stop
		endif
	x(1,kount)  = x(1,i) 
	x(2,kount)  =-x(2,i) 
	x(3,kount)  = x(3,i)
	mapnod(i)  = kount
	else
	x(2,i)  = 0.d0
	mapnod(i) = i
	end if
	enddo
	numnod = kount

	kount = numelm
	do i = 1,numelm
	kount = kount + 1
		if (kount.gt.maxelm) then
		write(llog,'(//'' ***error*** detected by subroutine xz_reflect_mesh:'', &
                             /'' number of element '',i5,'' exceeds maximum '',i5)') kount,maxelm
		stop
		endif
	ix(1,kount) = mapnod(ix(5,i))
	ix(2,kount) = mapnod(ix(6,i))
	ix(3,kount) = mapnod(ix(7,i))
	ix(4,kount) = mapnod(ix(8,i))
	ix(5,kount) = mapnod(ix(1,i))
	ix(6,kount) = mapnod(ix(2,i))
	ix(7,kount) = mapnod(ix(3,i))
	ix(8,kount) = mapnod(ix(4,i))
	enddo
	numelm = kount

	deallocate(mapnod)

end subroutine xz_reflect_brick_mesh
!===========================================================================
!	                       xy_reflect_brick_mesh 
!===========================================================================
subroutine xy_reflect_brick_mesh(x,				&
                                 ix,			&
							     maxnod,		&
                                 maxelm,		&
                                 numnod,		&
                                 numelm,		&
						         tol,			&
						         llog)
	implicit none

	integer		maxnod
	integer		maxelm
	integer		numnod
	integer		numelm
	real*8		tol
	integer		llog

	real*8		x(3,maxnod)
	integer		ix(8,maxelm)

	integer		i
	integer		kount

	integer, allocatable :: mapnod(:) 

	allocate(mapnod(maxnod))

	kount = numnod
	do i = 1, numnod
	if(dabs(x(3,i)).gt.tol*1.d-2) then
	kount = kount + 1
		if (kount.gt.maxnod) then
		write(llog,'(//'' ***error*** detected by subroutine yz_reflect_mesh:'', &
                             /'' number of nodes '',i5,'' exceeds maximum '',i5)') kount,maxnod
		stop
		endif
	x(1,kount)  = x(1,i) 
	x(2,kount)  = x(2,i) 
	x(3,kount)  =-x(3,i)
	mapnod(i)  = kount
	else
	x(3,i)  = 0.d0
	mapnod(i) = i
	end if
	enddo
	numnod = kount

	kount = numelm
	do i = 1,numelm
	kount = kount + 1
		if (kount.gt.maxelm) then
		write(llog,'(//'' ***error*** detected by subroutine yz_reflect_mesh:'', &
                             /'' number of element '',i5,'' exceeds maximum '',i5)') kount,maxelm
		stop
		endif
	ix(1,kount) = mapnod(ix(2,i))
	ix(2,kount) = mapnod(ix(1,i))
	ix(3,kount) = mapnod(ix(4,i))
	ix(4,kount) = mapnod(ix(3,i))
	ix(5,kount) = mapnod(ix(6,i))
	ix(6,kount) = mapnod(ix(5,i))
	ix(7,kount) = mapnod(ix(8,i))
	ix(8,kount) = mapnod(ix(7,i))
	enddo
	numelm = kount

	deallocate(mapnod)

end subroutine xy_reflect_brick_mesh
!=======================================================================
!                           point_element_multi
!=======================================================================
subroutine point_element_multi(xp,				                &
						 associated_element,                &
						 associated_element_surface,   &
                         numsurfs)

	use fem_data

    implicit none
    
        real*8			xp(3)
	integer			a_element
	integer                 a_element_surface
        integer         associated_element(4)
        integer         associated_element_surface(4)
        integer         numsurfs

	integer		n
	integer         m
	integer         nps
        integer         i 

        logical         help

        if(  dabs(xp(1)-0.5d0).lt.(1.d-10).and.  &
             dabs(xp(2)-0.d0) .lt.(1.d-10).and.   &
             dabs(xp(3)-0.d0) .lt.(1.d-10) ) then 
           help = .true. 
        endif

        help = .false. 

        if(help) then 
!           write(*,*) 'input xp'
!           read(*,*) xp(1),xp(2),xp(3)
           write(*,*) 
           write(*,*) 
           write(*,*)'xp=', xp
        endif

        numsurfs = 0 

	loop_element:   do n = 1, fem_number_of_element

           a_element         = n
           a_element_surface = 0

           if(help) write(*,*) 
           if(help) write(*,*) '###element =', n 

           loop_surface:   do m = 1, 6

              if(help) then 
                 write(*,*) '  #surface = ', m 
              endif

              call point_to_surface(xp,                                         &
                   fem_node_coordinate(:,fem_element_surface_node(1,m,n)),	&
                   fem_node_coordinate(:,fem_element_surface_node(2,m,n)),	&
                   fem_node_coordinate(:,fem_element_surface_node(3,m,n)),	&
                   fem_node_coordinate(:,fem_element_surface_node(4,m,n)),	&
                   nps)

              if(nps.eq.-1) then
                 a_element = 0 
                 if(help) write(*,*) '###outside of the element, continue to next element' 
                 exit loop_surface
              else if(nps.eq.0) then
                 if(fem_element_surface_neighbor(m,n).gt.0) then 
                    if(help) then 
                       write(*,*) '  #point on an internal surface, continue to next surface'
                    endif
                 else
                    a_element_surface = m 
                    numsurfs = numsurfs + 1
                    if(numsurfs.gt.4) then 
                       write(*,*) 'num_surfs > 4!'
                       stop 
                    endif
                    associated_element(numsurfs) = n
                    associated_element_surface(numsurfs) = m
                    if(help) then 
                       write(*,*) '  #VALID surface found, continue to next surface'
                    endif
                 endif
              else if(nps.eq.1) then
                 if(help) write(*,*) '  #out of surface but inside the box, next surface'
              endif

           enddo loop_surface

!
! outcomes after checking all surfaces of element n:
! 1) point outside of the element
!    a_element = 0, do not change numsurfs 
! 2) point on an internal surface, need exit similarly to 3)
!    a_element =n, a_element_surface = 0 numsurfs = 0 
! 3) point inside the element
!    a_element = n, a_element_surface = 0, numsurfs = 0, need exit loop_element
! 4) point on box surface
!    numsurfs > 0 
! 
           
           if(a_element.eq.n.and.a_element_surface.eq.0) then
              if(help) then 
                 write(*,*) '###node inside element or on internal surface'
              endif
              exit loop_element
           endif

         enddo loop_element

         if(a_element.eq.0.and.numsurfs.eq.0) then 
            if(help) write(*,*) '####node outside of the box####'
            numsurfs = -1
         endif


         if(help) then 
            write(*,*) 'numsurfs = ', numsurfs 
         endif

end subroutine point_element_multi
!=======================================================================
!                           point_element
!=======================================================================
subroutine point_element(xp,				                &
						 associated_element,                &
						 associated_element_surface)
	use fem_data

    implicit none
    
	real*8			xp(3)
	integer			associated_element
	integer         associated_element_surface

	integer			n
	integer         m
	integer         nps



	loop_element:   do n = 1, fem_number_of_element
           associated_element         = n
           associated_element_surface = 0
           loop_surface:   do m = 1, 6
              call point_to_surface(xp,                                         &
                   fem_node_coordinate(1,fem_element_surface_node(1,m,n)),	&
                   fem_node_coordinate(1,fem_element_surface_node(2,m,n)),	&
                   fem_node_coordinate(1,fem_element_surface_node(3,m,n)),	&
                   fem_node_coordinate(1,fem_element_surface_node(4,m,n)),      & 
                   nps)


              if(nps.eq.-1) then
                 associated_element = 0
                 exit loop_surface
              else if(nps.eq.0) then
                 associated_element_surface = m
              endif
           enddo loop_surface
           if(associated_element.eq.n) then
              exit loop_element
           endif
        enddo loop_element

end subroutine point_element
!=======================================================================
!                           point_to_surface
!=======================================================================
subroutine point_to_surface(xp,				&
							x1,				&
							x2,				&
							x3,				&
                                                        x4,                             &
							nps)
							
	implicit none 
	
	real*8			xp(3)
	real*8			x1(3)
	real*8			x2(3)
	real*8			x3(3)
        real*8                  x4(3)
	integer			nps

	real*8			vol,vol1,vol2

        integer                 i

        real*8                  xs(3,4)
	real*8          in_surface_element        

! plot out data to see in tecplot

!        write(*,*) xp
!        write(*,*) x1
!        write(*,*) x2
!        write(*,*) x3
!        write(*,*) x4


	vol1 =  (xp(1)-x1(1))*((x2(2)-x1(2))*(x3(3)-x1(3))-(x2(3)-x1(3))*(x3(2)-x1(2)))   &
	     - (xp(2)-x1(2))*((x2(1)-x1(1))*(x3(3)-x1(3))-(x2(3)-x1(3))*(x3(1)-x1(1)))   &
	     + (xp(3)-x1(3))*((x2(1)-x1(1))*(x3(2)-x1(2))-(x2(2)-x1(2))*(x3(1)-x1(1)))

	vol2 =  (xp(1)-x1(1))*((x2(2)-x1(2))*(x4(3)-x1(3))-(x2(3)-x1(3))*(x4(2)-x1(2)))   &
	     - (xp(2)-x1(2))*((x2(1)-x1(1))*(x4(3)-x1(3))-(x2(3)-x1(3))*(x4(1)-x1(1)))   &
	     + (xp(3)-x1(3))*((x2(1)-x1(1))*(x4(2)-x1(2))-(x2(2)-x1(2))*(x4(1)-x1(1)))

        vol = max(dabs(vol1),dabs(vol2))

!        write(*,*) 'vol1, vol2=', vol1, vol2 

        if(dabs(vol).eq.dabs(vol1)) vol = vol1
        if(dabs(vol).eq.dabs(vol2)) vol = vol2 

!	vol  =  (xp(1)-x1(1))*((x2(2)-x1(2))*(x3(3)-x1(3))-(x2(3)-x1(3))*(x3(2)-x1(2)))   &
!	     - (xp(2)-x1(2))*((x2(1)-x1(1))*(x3(3)-x1(3))-(x2(3)-x1(3))*(x3(1)-x1(1)))   &
!	     + (xp(3)-x1(3))*((x2(1)-x1(1))*(x3(2)-x1(2))-(x2(2)-x1(2))*(x3(1)-x1(1)))

        if(dabs(vol).lt.1.d-2.and.dabs(vol).gt.1.d-5) write(*,*) 'vol=', vol 

	if(vol.gt.1.d-5) then
           nps = 1
	else if(vol.lt.-1.d-5) then
           nps =-1
	else

           nps = -1 

           xs(:,1) = x1(:)
           xs(:,2) = x2(:)
           xs(:,3) = x3(:)
           xs(:,4) = x4(:)

           if(in_surface_element(xs,xp).lt.-1.d-10 .and. &
              in_surface_element(xs,xp).gt.-1.d-2) then
              write(*,*) 'in Point_to_surface: insurface=', in_surface_element(xs,xp)
           endif

           if(in_surface_element(xs,xp).gt.1.d-10 .or. &
                abs(in_surface_element(xs,xp)).le.1.d-10 ) then 
              nps = 0
           endif
           

!           call PointInTrapezoid(xp,x1,x2,x3,x4,nps)
	endif


end subroutine point_to_surface

!=======================================================================
!                       PointInTrapezoid                               =
!=======================================================================
subroutine PointInTrapezoid(p, a,b,c,d,nps)
        real*8 p(3), a(3),b(3),c(3),d(3)
        integer nps 
        logical IS1, IS2, IS3, IS4

        call SameSide(p,c,a,b,IS1)
        call SameSide(p,d,b,c,IS2)
        call SameSide(p,a,c,d,IS3)
        call SameSide(p,b,d,a,IS4)

        if(IS1.and.IS2.and.IS3.and.IS4) then 
           nps = 0 
        else
           nps = -1
        endif
     
end subroutine PointInTrapezoid
subroutine SameSide(p1,p2,a,b,IS)
        real*8 p1(3), p2(3),a(3),b(3)
        real*8 cp1(3), cp2(3)
        real*8 x
        logical IS
        
        call  cross_product(cp1, b-a, p1-a)
        call  cross_product(cp2, b-a, p2-a)

        x = dot_product(cp1, cp2)

        if(dabs(x).lt.1.d-7.and.dabs(x).gt.0.d0) write(*,*) 'x = ', x

        if (x.ge.0.d0) then 
           IS = .TRUE. 
        else
           IS = .FALSE. 
        endif
end subroutine SameSide
!=======================================================================
!                    fem_segment_surface_intersection
!=======================================================================
subroutine fem_segment_surface_intersection(xseg1,                      &
                                            xseg2,                      &
                                            xint,                       &
                                            associated_element_surface, &
                                            associated_element)
!///
!   This subroutine determines the intersection of a segment and domain surface
!   Node 2 of the element is assumed to be outside of the domian
!
!   input:
!	xseg(3,2)					    coordinates of two end nodes of the segment
!   
!   output:
!	xint(3)					        coordinates of the intersection
!	associated_element_surface		the element surface the intersection is associated with
!	associated_element		        the elelment the intersection is associated with.
!///
	use fem_data

	implicit none

	real(8)		xseg(3,2), xseg1(3), xseg2(3)
	real(8)     xint(3)
	integer		associated_element_surface
	integer		associated_element

	integer		n
	integer     i
	integer     surface
	integer     element
	integer     nls
	real(8)		xs(3,4)

        real(8)     xseg1p(3),xseg2p(3),xintp(3)

	associated_element_surface = 0
	associated_element         = 0

! obtain the coordinates in the FEM frame first

        xseg1p(:) = 0.d0
        xseg2p(:) = 0.d0
        xintp(:) = 0.d0
        xint(:) = 0.d0

        do i = 1, 3 
           do n = 1, 3
              xseg1p(i) = xseg1p(i) + fem_ageom(i,n)*xseg1(n)
              xseg2p(i) = xseg2p(i) + fem_ageom(i,n)*xseg2(n)
           enddo
        enddo

        xseg(:,1) = xseg1p(:)
        xseg(:,2) = xseg2p(:)

	loop_element:  do n = 1, fem_number_of_boundary_surface
	               surface = fem_boundary_surface_association(1,n)
	               element = fem_boundary_surface_association(2,n)
	               do i = 1, 4
	               xs(:,i) = fem_node_coordinate(:,fem_element_surface_node(i,surface,element))
	               enddo
	               call line_surface_intersection(xseg,         &
	                                              xs,			&
	                                              xintp,			&
	                                              nls)
                   if(nls.ne.0) then
                   associated_element_surface  = surface
                   associated_element          = element
                   exit loop_element
                   endif
                   enddo loop_element

! rotate to put xintp back to the DD3d frame

                   do i = 1, 3 
                      do n = 1, 3 
                         xint(i) = xint(i) + fem_ageom(n,i)*xintp(n)
                      enddo
                   enddo

end subroutine fem_segment_surface_intersection
!=======================================================================
!                          line_surface_intersection
!=======================================================================
subroutine line_surface_intersection(xl,			    &
                                     xs,				&
                                     xm,				&
                                     nls)
                                         
	implicit none

	real*8			xl(3,2)
	real*8			xs(3,4)
	real*8			xm(3)
	integer			nls

    integer         i
    real*8          u(3)
    real*8          v(3)
	real*8			a(3)
	real*8          c
	real*8          r
	real*8          k(3)
	real*8          adotk
	real*8          t
	real*8          vector_length
	real*8          in_surface_element


!///
!   set up no intersection
!///
    nls= 0
!///
!   get surface equation
!///
    u = xs(:,3) - xs(:,1)
    v = xs(:,4) - xs(:,2)
    call cross_product(a,u,v)
    c = dot_product(a,xs(:,1))
!///
!  get line equation
!///
    u = xl(:,2)-xl(:,1)
    r = vector_length(u)
    k = u/r
!///
!   get intersection point
!///
    adotk = dot_product(a,k)
    
    if(abs(adotk).lt.1.d-5) write(*,*) 'adotk=', adotk

    if(abs(adotk).gt.0.d0) then
    t = (c-dot_product(a,xl(:,1)))/dot_product(a,k)
!///
!   check if the intersection is on the segment
!///
!
! t is between 0 and r
!
    if(abs(t).lt.1.d-5) write(*,*) 't=', t

    if(t.gt.0.d0.and.t.lt.r) then
    xm = xl(:,1) + k*t
!///
!   check if the intersection is in the surface domain
!///
    if(in_surface_element(xs,xm).gt.1.d-10 .or. &
    abs(in_surface_element(xs,xm)).le.1.d-10) then
       write(*,*) 'insurface=', in_surface_element(xs,xm)
    endif

    if(in_surface_element(xs,xm).gt.1.d-10 .or. &
    abs(in_surface_element(xs,xm)).le.1.d-10 ) then 
    nls = 1
    endif
    endif
    endif

end subroutine line_surface_intersection
!=======================================================================
!                           vector_length
!=======================================================================
function vector_length(a)
						 
    real*8          vector_length
    real*8          a(3)

    vector_length = sqrt(a(1)**2+a(2)**2+a(3)**2)

end function vector_length
!=======================================================================
!                           cross_product
!=======================================================================
subroutine cross_product(c,				&
						 a,				&
						 b)
						 
	real*8			c(3)
	real*8			a(3)
	real*8			b(3)

    c(1) = a(2)*b(3)-a(3)*b(2)
    c(2) = a(3)*b(1)-a(1)*b(3)
    c(3) = a(1)*b(2)-a(2)*b(1)

end subroutine cross_product
!=======================================================================
!                           in_surface_element
!=======================================================================
function in_surface_element(xs,		        &
                            xm)
						 
	real*8          in_surface_element
	real*8			xs(3,4)
	real*8			xm(3)

    real*8          a(3)
    real*8          b(3)
    real*8          c(3)
    real*8          d(3)
    real*8          ab(3)
    real*8          bc(3)
    real*8          cd(3)
    real*8          da(3)

    a = xs(:,1) - xm
    b = xs(:,2) - xm
    c = xs(:,3) - xm
    d = xs(:,4) - xm
    call cross_product(ab,a,b)
    call cross_product(bc,b,c)
    call cross_product(cd,c,d)
    call cross_product(da,d,a)
    in_surface_element = min(dot_product(ab,cd),dot_product(bc,da))

end function in_surface_element
!=======================================================================
!                           triangle_area
!=======================================================================
function triangle_area(x1,				&
                       x2,				&
                       x3)
						 
	real*8          triangle_area
	real*8          x1(3)
	real*8			x2(3)
	real*8			x3(3)

    real*8          a(3)
    real*8          b(3)
    real*8          c(3)

    real*8          vector_length 

    a    = x2 - x1
    b    = x3 - x1
    call cross_product(c,a,b)
    triangle_area = 0.5d0*vector_length(c)

end function triangle_area




