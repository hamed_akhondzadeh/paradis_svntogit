/**************************************************************************
 *
 *      Module:  FEM_Force.c -- Contains the functions used to add FEM
 *               related remote forces.
 *
 *      Includes functions:
 *               ComputeFEMSegSigbRem()
 *               ComputeFEM1SegSigbRem()
 *               InitSegSigbRem()
 *
 *************************************************************************/
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#include "Home.h"
#include "Util.h"

#include "FEM.h"


#ifdef _FEM
/*-------------------------------------------------------------------------
 *
 *      Function:     InitSegSigbRem
 *      Description:  Zero out the sigbRem value for all segments.  This
 *                    should only be needed when computing the FEM image
 *                    stress with the FastMultipole(FMM) code enabled
 *
 *-----------------------------------------------------------------------*/
void InitSegSigbRem(Home_t *home, int reqType)
{
        int  i, j;
        Node_t *node;

/*
 *      Initialize sigbRem for all segments attached to native
 *      nodes.
 */
        for (i = 0; i < home->newNodeKeyPtr; i++) {

            node = home->nodeKeys[i];
            if (node == (Node_t *)NULL) continue;

            for (j = 0; j < node->numNbrs; j++) {
                node->sigbRem[3*j  ] = 0.0;
                node->sigbRem[3*j+1] = 0.0;
                node->sigbRem[3*j+2] = 0.0;
            }
        }

/*
 *      Initialize sigbRem for all segments attached to ghost
 *      nodes.
 */
        node = home->ghostNodeQ;

        while (node != (Node_t *)NULL) {

            for (j = 0; j < node->numNbrs; j++) {
                node->sigbRem[3*j  ] = 0.0;
                node->sigbRem[3*j+1] = 0.0;
                node->sigbRem[3*j+2] = 0.0;
            }

            node = node->next;
        }

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:       ComputeFEM1SegSigbRem
 *      Description:    
 *
 *      Arguments:
 *              node    pointer to first nodal endpoint of the segment
 *              nbr     pointer to second nodal endpoint of the segment
 *              armID1  index in <node>'s segment list of the arm 
 *                      terminating at <nbr>
 *              armID2  index in <nbr>'s segment list of the arm 
 *                      terminating at <node>
 *
 *------------------------------------------------------------------------*/
void ComputeFEM1SegSigbRem(Home_t *home, Node_t *node, Node_t *nbr,
                           int armID1, int armID2)
{
        int     i, j;
        real8   bx, by, bz;
        real8   dx, dy, dz;
        real8   x1, y1, z1;
        real8   sigb1, sigb2, sigb3;
        real8   r[3], sig[6], totRemSig[3][3];
        real8   sigSH[6]; 
        Param_t *param;

        param = home->param;

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                totRemSig[i][j] = 0.0;
            }
        }

        x1 = node->x; 
        y1 = node->y; 
        z1 = node->z;
        
/*
 *      Get the midpoint of the segment
 */
        dx = nbr->x - x1; 
        dy = nbr->y - y1; 
        dz = nbr->z - z1;
         
        ZImage(param, &dx, &dy, &dz);
        
        r[0] = x1 + dx*0.5;
        r[1] = y1 + dy*0.5;
        r[2] = z1 + dz*0.5;
            
/*
 *      Add FEM and Yoffe image stress
 */

	sig[0]=0.0;sig[1]=0.0;sig[2]=0.0;
        sig[3]=0.0;sig[4]=0.0;sig[5]=0.0;

#ifdef _FEMIMGSTRESS

        FEM_Point_Stress(r, sig); 

#endif 

        totRemSig[0][0] = sig[0];
        totRemSig[1][1] = sig[1];
        totRemSig[2][2] = sig[2];
        totRemSig[0][1] = sig[3];
        totRemSig[1][2] = sig[4];
        totRemSig[2][0] = sig[5];
        totRemSig[1][0] = totRemSig[0][1];
        totRemSig[2][1] = totRemSig[1][2];
        totRemSig[0][2] = totRemSig[2][0];

        AllYoffeStress(home, r[0], r[1], r[2], sigSH);

        totRemSig[0][0] += sigSH[0];
        totRemSig[1][1] += sigSH[3];
        totRemSig[2][2] += sigSH[5];
        totRemSig[0][1] += sigSH[1];
        totRemSig[1][2] += sigSH[4];
        totRemSig[2][0] += sigSH[2];
        totRemSig[1][0] = totRemSig[0][1];
        totRemSig[2][1] = totRemSig[1][2];
        totRemSig[0][2] = totRemSig[2][0];

	/* #endif turn this on when calling no YF */ 
        
/*
 *      Calculate the segment's sig dot b and store it in both nodes
 */
        bx = node->burgX[armID1];
        by = node->burgY[armID1];
        bz = node->burgZ[armID1];
        
        sigb1 = totRemSig[0][0]*bx +
                totRemSig[0][1]*by +
                totRemSig[0][2]*bz;
        sigb2 = totRemSig[1][0]*bx +
                totRemSig[1][1]*by +
                totRemSig[1][2]*bz;
        sigb3 = totRemSig[2][0]*bx +
                totRemSig[2][1]*by +
                totRemSig[2][2]*bz;

        node->sigbRem[3*armID1  ] += sigb1;
        node->sigbRem[3*armID1+1] += sigb2;
        node->sigbRem[3*armID1+2] += sigb3;
        
        nbr->sigbRem[3*armID2  ] += sigb1;
        nbr->sigbRem[3*armID2+1] += sigb2;
        nbr->sigbRem[3*armID2+2] += sigb3;

        return;
}


void ComputeFEMSegSigbRem(Home_t *home, int reqType)
{
        int     inode, armID1, armID2, mm, kk;
        real8   bx1, by1, bz1, sigb1, sigb2, sigb3;
        real8   x1, y1, z1, dx, dy, dz;
        real8   r[3], sig[6], totRemSig[3][3];
        real8   sigSH[6]; 
        Node_t  *node, *nbr;
        Param_t *param;
         
        param = home->param;
        
/*
 *      Initialize totRemSig to zero, Meijie, 11/25/03
 */
        for (mm=0;mm<3;mm++) {
            for (kk=0;kk<3;kk++) {
                totRemSig[mm][kk] =0;
            }
        }
        
/*
 *      Loop though all native nodes
 */
        for (inode = 0; inode < home->newNodeKeyPtr; inode++) {
        
            node = home->nodeKeys[inode];
            if (!node) continue;
        
            x1 = node->x; 
            y1 = node->y; 
            z1 = node->z;
        
/*
 *          Loop thru the node's arms
 */
            for (armID1 = 0; armID1 < node->numNbrs; armID1++) {

                nbr = GetNodeFromTag(home, node->nbrTag[armID1]);
                armID2 = GetArmID(home, nbr, node);

                if (armID2 < 0) {
                    Fatal("ComputeFEMSegSigbRem: Inconsistent linkage between "
                          "nodes (%d,%d) and (%d,%d)",
                          node->myTag.domainID, node->myTag.index,
                          nbr->myTag.domainID, nbr->myTag.index);
                }

/*
 *              if nbr is a ghost, always calc sigb. Otherwise, just
 *              calc if tag(node) < tag(nbr), then copy sigb to
 *              redundant arm
 */
                if ((nbr->myTag.domainID == home->myDomain) && 
                    OrderNodes(node, nbr) > 0) {
                    continue;
                }
        
                ComputeFEM1SegSigbRem(home, node, nbr, armID1, armID2);
            }
        }

        return;
}
#endif  /* ifdef _FEM */
