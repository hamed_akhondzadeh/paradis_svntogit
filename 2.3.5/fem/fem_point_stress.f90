!======================================================================= 
!                         fem_point_stress
! Calculate FEM (image) stress at a given point
!======================================================================= 
        subroutine fem_point_stress(xp,img_stress)
	use fem_data 
	use tecplot_lib 
 
	implicit none

        real(8) xp(3)
        real(8) img_stress(6)
        integer ne, i, ng 
        integer nes

!c use nes to find out the surface # of the element ne, then 
!c use the fem_data to find out the global surface # and its surface normal 
!c vector
!c
        call point_element(xp,ne,nes)

        if(ne.eq.0) then 
           write(*,*) 'coordinates= ', xp 
           stop 'Point is out of the box!' 
        endif

        if(fem_number_of_gauss_point.eq.8) then 
           call nearest_gauss_point(xp,ne,ng)
!c     write(*,*) 'point xp=', xp 
!c     write(*,*) 'element # ne=', ne
!c     write(*,*) 'nearest gauss point ng=', ng 
           do i=1, 6
              img_stress(i)=fem_gauss_stress(i,ng,ne) 
           enddo 
        else
           do i=1, 6
              img_stress(i)=fem_element_stress(i,1,ne)
           enddo
        endif

      end subroutine fem_point_stress
