#############################################################################
#
#    makefile: controls the build of paradisfem 
#
############################################################################

#
#       Include files containing the system specific macro definitions
#       as well as the settings and flags that are not system dependent.
#

include ../makefile.sys
include ../makefile.setup

SRCDIR = ../src
INCDIR = ../include
BINDIR = ../bin

#
#       Set flags to enable the FEM portions of the ParaDiS modules
#
DEFS += -D_FEM
DEFS += -D_FEMIMGSTRESS

#
#	paradisfem uses various source modules from the parallel
#       code located in the paradis source directory.  Since these
#       modules are compiled in the paradis directory with a different
#       set of preprocessor definitions than are needed here, we
#       need to create links in this directory back to the source
#       modules and create separate object modules for these sources.
#

EXTERN_C_SRCS = AddNode.c             \
                CellCharge.c          \
                CheckArms.c           \
                Collision.c           \
                CommSendGhosts.c      \
                CommSendSecondaryGhosts.c \
                CommSendMirrorNodes.c \
                CommSendRemesh.c      \
                CommSendSegments.c    \
                CommSendVelocity.c    \
                CorrectionTable.c     \
                CrossSlipBCC.c        \
                CrossSlipFCC.c        \
                Decomp.c              \
                deWitInteraction.c    \
                DeltaPlasticStrain.c  \
                DLBfreeOld.c          \
                FccCrossSlip.c        \
                FixRemesh.c           \
                ForwardEulerIntegrator.c \
                FMComm.c              \
                FMSigma2.c            \
                FMSupport.c           \
                FreeInitArrays.c      \
                GenerateOutput.c      \
                GetDensityDelta.c     \
                GetNativeNodes.c      \
                GetNewGhostNode.c     \
                GetNewNativeNode.c    \
                Gnuplot.c             \
                Heap.c                \
                InitCellDomains.c     \
                InitCellNatives.c     \
                InitCellNeighbors.c   \
                InitHome.c            \
                Initialize.c          \
                InitRemoteDomains.c   \
                InitSendDomains.c     \
                InputSanity.c         \
                LoadCurve.c           \
                LocalSegForces.c      \
                MemCheck.c            \
                Meminfo.c             \
                Migrate.c             \
                MobilityLaw_BCC_glide.c   \
                MobilityLaw_Relax.c   \
                MobilityLaw_BCC_0.c   \
                MobilityLaw_BCC_0b.c  \
                MobilityLaw_FCC_0.c   \
                MobilityLaw_FCC_2.c   \
                NodeForce.c           \
                NodeVelocity.c        \
                ParadisFinish.c       \
                ParadisInit.c         \
                ParadisStep.c         \
                Param.c               \
                Parse.c               \
                PartialForces.c       \
                Plot.c                \
                QueueOps.c            \
                RBDecomp.c            \
                ReadBinaryRestart.c   \
                ReadRestart.c         \
                RemapInitialTags.c    \
                Remesh.c              \
                RemeshRule_2.c        \
                RemeshRule_3.c        \
                RemoteSegForces.c     \
                RemoveNode.c          \
                ResetNodeArmForce.c   \
                RSDecomp.c            \
                SortNativeNodes.c     \
                SortNodesForCollision.c \
                Tecplot.c             \
                Timer.c               \
                Topology.c            \
                TrapezoidIntegrator.c \
                Util.c                \
                WriteArms.c           \
                WriteAtomEye.c           \
                WriteBinaryRestart.c    \
                WriteDensFlux.c       \
                WriteDensityField.c   \
                WriteForceTimes.c     \
                WriteFragments.c      \
                WritePoleFig.c        \
                WritePovray.c         \
                WriteProp.c           \
                WriteRestart.c        \
                WriteTSB.c            \
                WriteVelocity.c
                

EXTERN_CPP_SRCS = DisplayC.C  \
                  display.C 

EXTERN_SRCS = $(EXTERN_C_SRCS) $(EXTERN_CPP_SRCS)
EXTERN_OBJS = $(EXTERN_C_SRCS:.c=.o) $(EXTERN_CPP_SRCS:.C=.o)

#
#	Define the exectutable, source and object modules for paradisfem 
#

PARADISFEM     = paradisfem
PARADISFEM_BIN = $(BINDIR)/$(PARADISFEM)

PARADISFEM_C_SRCS = AdjustNodePosition.c  \
                    AdjustSurfaceNodeVel.c\
                    AllImageStress.c      \
                    AllSegmentStress.c    \
                    AllYoffeStress.c      \
                    FEM_Force.c           \
                    FEM_Main.c            \
                    FEM_Util.c            \
                    SegmentStress.c       \
                    yoffe_corr.c

PARADISFEM_F_SRCS = fem_data.f90           \
                    fem_lib.f90            \
                    tecplot_lib.f90        \
                    fem_mesh.f90           \
                    fem_mesh_brick.f90     \
                    fem_mesh_cylinder.f90  \
                    fem_mesh_void.f90      \
	            fem_mesh_octahedron.f90 \
                    fem_procedure.f90      \
                    fem_dd_link.f90        \
                    stress_on_boundary.f90 \
                    fem_point_stress.f90   \
                    fem_init.f90           \
                    fem_set_step.f90       \
                    yoffe_all.f90          \
                    node_on_surface.f90

PARADISFEM_SRCS = $(PARADISFEM_C_SRCS) $(PARADISFEM_F_SRCS) \
                  $(EXTERN_C_SRCS) $(EXTERN_CPP_SRCS)

PARADISFEM_OBJS = $(PARADISFEM_C_SRCS:.c=.o)   \
                  $(PARADISFEM_F_SRCS:.f90=.o) \
                  $(EXTERN_C_SRCS:.c=.o)       \
                  $(EXTERN_CPP_SRCS:.C=.o)

# add system dependent include and library directories here (if needed)
#
PARADISFEM_INCS.linux =
PARADISFEMTF_LIBS.linux =

PARADISFEM_INCS.wcr = 
PARADISFEM_LIBS.wcr =

PARADISFEM_INCS.vip =
PARADISFEM_LIBS.vip =

PARADISFEM_INCS.su-ahpcrc =
PARADISFEM_LIBS.su-ahpcrc =

PARADISFEM_INCS.cygwin =
PARADISFEM_LIBS.cygwin =

PARADISFEM_INCS.mc-cc =
PARADISFEM_LIBS.mc-cc =

PARADISFEM_INCS.mac =
PARADISFEM_LIBS.mac =

PARADISFEM_INCS = -I Include $(PARADISFEM_INCS.$(SYS))
PARADISFEM_LIBS = $(PARADISFEM_LIBS.$(SYS))


###########################################################################
#
#	Define rules for converting the various types of source files
#       to object modules.
#
###########################################################################

.SUFFIXES : .C .f90

.c.o:	makefile ../makefile.sys ../makefile.setup
	$(CC) $(OPT) $(CCFLAG) $(PARADISFEM_INCS) $(INCS) -c $<

.C.o:	makefile ../makefile.sys ../makefile.setup
	$(CPP) $(OPT) $(CPPFLAG) $(INCS) -c $<

.f90.o:	makefile ../makefile.sys ../makefile.setup
	$(F90) $(OPT) $(F90_OPTS) -c $<

###########################################################################
#
#	Define all targets and dependencies below
#
###########################################################################

all:		$(PARADISFEM) 

clean:
		rm -f *.o $(EXTERN_SRCS) $(PARADISFEM_BIN) *.mod

depend:		 *.c $(SRCDIR)/*.c $(INCDIR)/*.h makefile
		makedepend -Y$(INCDIR) *.c  -fmakefile.dep

$(BINDIR):
		mkdir $(BINDIR)

$(EXTERN_SRCS): $(SRCDIR)/$@
		- @ ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1


$(PARADISFEM):	$(BINDIR) $(PARADISFEM_BIN)

$(PARADISFEM_BIN): $(PARADISFEM_SRCS) $(PARADISFEM_OBJS) $(HEADERS)
		$(CPP) $(OPT) $(PARADISFEM_OBJS) -o $@ $(LIB) $(F90_LIB)


include ./makefile.dep
