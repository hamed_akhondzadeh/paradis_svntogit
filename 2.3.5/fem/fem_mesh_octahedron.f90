!=======================================================================
!                          fem_mesh_data_brick
!=======================================================================
subroutine fem_mesh_data_octahedron(base_diagonal_half,			&
							   grid_base,			&
							   grid_vertical) 
!///
!   This subroutine generates a uniform structured three dimensional mesh
!   of a brick domain.
!///
	use fem_data
	use tecplot_lib

	implicit none

	real(8)		base_diagonal_half
        integer         grid_base
        integer         grid_vertical
        real(8)         ax
	real(8)		ay
	real(8)		az
	integer		nx
	integer		ny
	integer		nz

        real(8)         xco,yco,zco,sumco,tmp
	
	integer		i
	integer		j
	integer		k
	integer		m
	integer		n

        real(8)         ageom(3,3)

        nx = grid_base
        ny = grid_base
        nz = grid_vertical

        ax = 2.d0*base_diagonal_half
        ay = ax
        az = ax 

        do i = 1, 3 
           do j = 1, 3
              fem_ageom(i,j) = ageom(j,i)
           enddo
        enddo
        call norm(fem_ageom(1,:))
        call norm(fem_ageom(2,:))
        call norm(fem_ageom(3,:))

!///
!   calculate number of nodes and elements
!///
	fem_number_of_node    = nx*ny*nz
	fem_number_of_element = (nx-1)*(ny-1)*(nz-1)
!///
!   allocate finite element arrays
!///
    call fem_mesh_allocation
!///
!   initialize finite element arrays
!///
    call fem_mesh_initialization
!///
!   allocate finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_allocation
!///
!   initialize finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_initialization

	do k = 1, nz
           do j = 1, ny
              do i = 1, nx
                 xco = -0.5d0*ax+float(i-1)*ax/float(nx-1)
                 yco = -0.5d0*ay+float(j-1)*ay/float(ny-1)
                 zco = -0.5d0*az+float(k-1)*az/float(nz-1)

                 sumco = abs(xco)+abs(yco)+abs(zco)
                 if(sumco.ne.0.d0) then 
                    tmp = max(abs(xco),abs(yco),abs(zco))/sumco
                    xco = xco*tmp
                    yco = yco*tmp
                    zco = zco*tmp 
                 endif

                 n   = (k-1)*ny*nx+(j-1)*nx+i
!///
!   compute node coordinates
!///
                 fem_node_coordinate(1,n) = xco
                 fem_node_coordinate(2,n) = yco
                 fem_node_coordinate(3,n) = zco

              enddo
           enddo
	enddo

!///
!   assign node boundary condition
!   1 -- displacement need to be solved
!   0 -- displacement fixed to be 0
!  -1 -- displacement prescribed 
!///
!    fem_node_boundary_condition = 0
!///
!   node with image stress
!   1 -- yes
!   0 -- no 
!///
!   fem_node_img_id(n) = 0
!///
!   node with applied stress
!   1 -- yes
!   0 -- no 
!///
!	fem_node_app_id(n) = 0
!///

!///
!   generate element properties
!///
	do k = 1, nz-1
	do j = 1, ny-1
	do i = 1, nx-1
!///
!	connectivity, i.e., relation of local nodes and global nodes
!///
	n = (k-1)*(ny-1)*(nx-1)+(j-1)*(nx-1)+i
	m = (k-1)*ny*nx+(j-1)*nx+i
	fem_element_node(1,n) = m
	fem_element_node(2,n) = m + ny*nx
	fem_element_node(3,n) = m + ny*nx+1
	fem_element_node(4,n) = m + 1
	fem_element_node(5,n) = m + nx
	fem_element_node(6,n) = m + nx + ny*nx
	fem_element_node(7,n) = m + nx + ny*nx + 1
	fem_element_node(8,n) = m + nx + 1
	enddo 
	enddo
	enddo 

!///
!   get mesh data for fem and dd 
!///
    call fem_dd_mesh_data

!///
!   Output mesh data for tecplot (optional)
!///
	call tecplot_fem_mesh(fem_node_coordinate,				            &
						  1.d0,		                            &
						  fem_element_node,	                    &
						  'BRICK',				    &
						  'element_mesh_octahedron.dat',		    &
						  fem_ltmp)

	call tecplot_fem_mesh(fem_node_coordinate,				            &
						  1.d0,					    &
						  fem_element_surface_element_node,	    &
						  'QUADRILATERAL',		            &
						  'element_surface_mesh_octahedron.dat',	    &
						  fem_ltmp)
						  
	call tecplot_fem_mesh(fem_node_coordinate,				                &
						  1.d0,					        &
						  fem_boundary_surface_element_node,            &
						  'QUADRILATERAL',			        &
						  'element_boundary_surface_mesh_octahedron.dat',    &
						  fem_ltmp)

end subroutine fem_mesh_data_octahedron

