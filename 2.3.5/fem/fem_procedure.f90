!=======================================================================
!                         fem_elastic_8n_3d_dir
!=======================================================================
subroutine fem_elastic_8n_3d_dir(numnod,					&
								 numelm,					&	
							     ie,						&
							     x,							&
							     u,							&
							     f,							&
							     id_bdc,					&
							     c,							&
							     young,						&
							     poisson)

	use fem_lib

	implicit none

	integer		numnod
	integer		numelm
	integer		ie(8,numelm)
	real*8		x(3,numnod)
	real*8		u(3,numnod)
	real*8		f(3,numnod)
	integer		id_bdc(3,numnod)
	real*8		c(3,3,3,3)
	real*8		young
	real*8		poisson


	integer		numeqs
	integer		numsys
	integer,	allocatable ::	id_eqs(:,:)
	integer,	allocatable ::	kdia(:)
	real*8,		allocatable ::	sysmat(:)
	real*8,		allocatable ::	sysfor(:)
	real*8,		allocatable ::	sysvar(:)

	allocate(id_eqs(3,numnod))

	call system_profile(id_bdc,			&
						id_eqs,			&
						numeqs)

	allocate(kdia(numeqs))

	call fem_system_diaposition(numnod,		&
								numelm,		&
								numeqs,		&
								ie,			&
								id_eqs,		&
								kdia)

	numsys = kdia(numeqs)
	write(6,'('' numnod= '', i10)') numnod
	write(6,'('' numelm= '', i10)') numelm
	write(6,'('' numeqs= '', i10)') numeqs
	write(6,'('' numsys= '', i10)') numsys
	allocate(sysmat(numsys))
	allocate(sysfor(numeqs))
	allocate(sysvar(numeqs))

	call fem_system_elastic_8n_3d(numnod,			&
								  numelm,			&
								  numeqs,			&
								  numsys,			&
								  ie,				&
								  id_eqs,			&
								  sysmat,			&
								  kdia,				&
								  x,				&
								  c,				&
								  young,			&
								  poisson)

	call fem_force_elastic_8n_3d(numnod,			&
								 numelm,			&
								 numeqs,			&
								 ie,				&
								 id_eqs,			&
								 sysfor,			&
								 x,				&
								 f)
!///
!	assign system term force term that will be replace with dislocation solution
!///
	sysvar = sysfor
!///
!	apply displacement contraint condition
!///
	call displacement_constaint_condition(sysmat,		&
										  sysvar,		&
										  u,			&
										  id_bdc,		&
										  id_eqs,		&
										  kdia,			&
										  numnod,		&
										  numsys,		&
										  numeqs)

!///
!   solve displacement solution
!///
        write(*,*) 'now build sysmat' 
	call sparin(sysmat,kdia)
        write(*,*) 'done build sysmat'

	call spabac(sysmat,sysvar,kdia)
        write(*,*) 'done obtain sysvar'

	call fem_update_node_variable(u,				&
								  sysvar,			&
								  id_eqs,			&
								  numnod,			&
								  3,				&
								  numeqs)
        write(*,*) 'done obtain u' 

end subroutine fem_elastic_8n_3d_dir
!========================================================================
!                         fem_system_profile
!========================================================================
subroutine fem_system_profile(numnod,			&
							  id_bdc,			&
							  numeqs,			&
							  id_eqs)
	implicit none

	integer		numnod
	integer		id_bdc(3,numnod)
	integer		numeqs
	integer		id_eqs(3,numnod)

	integer		kount
	integer		i
	integer		j

	kount = 0
    do i = 1,numnod
    do j = 1,3
    if(id_bdc(j,i).ne.0) then
    kount = kount + 1
    id_eqs(j,i) = kount
	else
    id_eqs(j,i) = 0
    endif
    enddo
    enddo

    numeqs = kount

end	subroutine fem_system_profile
!=========================================================================
!                          fem_system_diaposition
!=========================================================================
subroutine fem_system_diaposition(numnod,		&
								  numelm,		&
								  numeqs,		&
								  ie,			&
								  id_eqs,		&
								  kdia)
	implicit none

	integer		numnod
	integer		numelm
	integer		numeqs
	integer		ie(8,numelm)
	integer		id_eqs(3,numnod)
	integer		kdia(numeqs)

	integer		n
	integer		i
	integer		j
	integer		ia
	integer		ja
	integer		ki
	integer		kj
	integer		iw
	integer		iwt

	kdia = 0

	do n = 1, numelm
		do i  = 1,3
		do ia = 1,8
			iw = 1
			ki = id_eqs(i,ie(ia,n))
			if(ki.ne.0) then
				do j  = 1,3
				do ja = 1,8
					kj = id_eqs(j,ie(ja,n))
					if(kj.ne.0) then
					iwt = ki-kj+1
					if(iwt>iw) iw = iwt
					endif
				enddo
				enddo 
				if(iw>kdia(ki)) then
				kdia(ki) = iw
				endif
			endif
		enddo
		enddo
	enddo

	kdia(1) = 1
	do i = 2, numeqs
	kdia(i) = kdia(i)+kdia(i-1)
	enddo

end subroutine fem_system_diaposition
!========================================================================
!                        fem_system_elastic_8n_3d 
!========================================================================
subroutine fem_system_elastic_8n_3d(numnod,			&
									numelm,			&
									numeqs,			&
									numsys,			&
									ie,				&
									id_eqs,			&
									sysmat,			&
									kdia,			&
									x,				&
								    c,				&
								    young,			&
								    poisson)
	use fem_lib
	
	implicit none

	integer		numnod
	integer		numelm
	integer		numeqs
	integer		numsys
	integer		ie(8,numelm)
	integer		id_eqs(3,numnod)
	real*8		sysmat(numsys)
	integer		kdia(numeqs)
	real*8		x(3,numnod)
	real*8		c(3,3,3,3)
	real*8		young
	real*8		poisson
	
    real*8		elmmat(24,24)
    real*8		xe(3,8)
	integer		g(24)
	integer		n
	integer		i
	integer		j

	sysmat = 0.d0

    do n = 1, numelm

	elmmat = 0.d0

!///
!	Get element coordinates
!///
    do i = 1,3
    do j = 1,8
    xe(i,j) = x(i,ie(j,n))
    enddo
    enddo

    do i = 1,3
    do j = 1,8
    g(3*(j-1)+i) = id_eqs(i,ie(j,n))
    enddo
    enddo
!///
!	Element matrix
!///
	call fem_element_matrix_iso_elastic_8n_3d(elmmat,			&
											  xe,				&
											  young,			&
											  poisson)
!///
!	Assembly of element matrices into skyline global matrix
!///
	call fsparv(sysmat,			&
			    elmmat,			&
				g,				&
				kdia)
	enddo

end subroutine fem_system_elastic_8n_3d
!==========================================================================
!                      fem_element_matrix_iso_elastic_8n_3d
!==========================================================================
subroutine fem_element_matrix_iso_elastic_8n_3d(elmmat,			&
					                            xe,				&
						                        young,			&
											    poisson)
	use fem_lib

	implicit none

	real*8		elmmat(24,24)
	real*8		xe(3,8)
	real*8		young
	real*8		poisson

	integer						i
	integer,	parameter ::	nqd = 8
	integer,	parameter ::	ndim = 3
	integer,	parameter ::	nod  = 8
	integer,	parameter ::	nodof = 3
	integer,	parameter ::	ndof = nod*nodof
	integer,	parameter ::	nst	 = 6
	real*8						dee(nst,nst)
	real*8						coord(nod,ndim)
	real*8						points(nqd,ndim)
	real*8						weights(nqd)
	real*8						der(ndim,nod)
	real*8						jac(ndim,ndim)
	real*8						deriv(ndim,nod)
	real*8						bee(nst,ndof)
	real*8						det

	call deemat(dee,young,poisson)
	call sample('hexahedron',points,weights)
	coord = transpose(xe)

    integration:  do i = 1, nqd
                  call shape_der(der,points,i)
				  jac=matmul(der,coord)
                  det= determinant(jac)
				  call invert(jac)
                  deriv = matmul(jac,der)
				  call beemat(bee,deriv)
                  elmmat= elmmat + matmul(matmul(transpose(bee),dee),bee)*det*weights(i)
    end do integration

end subroutine fem_element_matrix_iso_elastic_8n_3d
!==========================================================================
!                      fem_element_force_elastic_8n_3d
!==========================================================================
subroutine fem_element_force_elastic_8n_3d(elmfor,			&
					                       xe,				&
						                   fe)
	use fem_lib

	implicit none

	real*8		elmfor(24)
	real*8		xe(3,8)
	real*8		fe(3,8)

	integer						k
	integer						i
	integer						ia
	integer,	parameter ::	nqd = 8
	integer,	parameter ::	ndim = 3
	integer,	parameter ::	nod  = 8
	integer,	parameter ::	nodof = 3
	integer,	parameter ::	ndof = nod*nodof
	real*8						coord(nod,ndim)
	real*8						points(nqd,ndim)
	real*8						weights(nqd)
	real*8						der(ndim,nod)
	real*8						jac(ndim,ndim)
	real*8						fun(nod)
	real*8						fq(ndim)
	real*8						deriv(ndim,nod)
	real*8						det

	call sample('hexahedron',points,weights)
	coord = transpose(xe)

    integration:  do k = 1, nqd
                  call shape_der(der,points,k)
				  jac=matmul(der,coord)
                  det= determinant(jac)
                  call shape_fun(fun,points,k)
				  fq = matmul(fe,fun)
				  do  i = 1, 3
				  do ia = 1, 8
                  elmfor(3*(ia-1)+i) = elmfor(3*(ia-1)+i) + fq(i)*fun(ia)*det*weights(k)
				  enddo
				  enddo
    end do integration

end subroutine fem_element_force_elastic_8n_3d
!==========================================================================
!                         fem_add_matrix_elastic_8n_3d
!==========================================================================
subroutine fem_add_matrix_elastic_8n_3d(sysmat,					&
                                        kdia,					&
						                elmmat,					&
										ie,						&
                                        id_eqs,					&
									    numnod,					&
									    numelm,					&
						                numsys,					&
									    numeqs,					&
										n)

	implicit none

	integer		numnod
	integer		numelm
	integer		numsys
	integer		numeqs
	integer		n
	real*8		sysmat(numsys)
	integer		kdia(numeqs)
	real*8		elmmat(24,24)
	integer		ie(8,numelm)
	integer		id_eqs(3,numnod)

	integer		i
	integer		ia
	integer		ki
	integer		j
	integer		ja
	integer		kj
	integer		iw
	integer		isys

	do i  = 1,3
	do ia = 1,8
	ki = id_eqs(i,ie(ia,n))
	if(ki.ne.0) then
	do j  = 1,3
	do ja = 1,8
	kj = id_eqs(j,ie(ja,n))
	if(kj.ne.0) then
	iw = ki-kj
	if(iw>=0) then
	isys = kdia(ki)-iw
	sysmat(isys) = sysmat(isys) + elmmat(3*(ia-1)+i,3*(ja-1)+j)
	endif
	endif
	enddo
	enddo
	endif
	enddo
	enddo

end subroutine fem_add_matrix_elastic_8n_3d
!========================================================================
!                        fem_force_elastic_8n_3d 
!========================================================================
subroutine fem_force_elastic_8n_3d(numnod,			&
								   numelm,			&
								   numeqs,			&
								   ie,				&
								   id_eqs,			&
								   sysfor,			&
								   x,				&
								   f)
	use fem_lib
	
	implicit none

	integer		numnod
	integer		numelm
	integer		numeqs
	integer		ie(8,numelm)
	integer		id_eqs(3,numnod)
	real*8		sysfor(numeqs)
	real*8		x(3,numnod)
	real*8		f(3,numnod)
	
    real*8		elmfor(24)
    real*8		xe(3,8)
	real*8		fe(3,8)
	integer		g(24)
	integer		n
	integer		i
	integer		j

	sysfor = 0.d0

    do n = 1, numelm

	elmfor = 0.d0

!///
!	Get element coordinates
!///
    do i = 1,3
    do j = 1,8
    xe(i,j) = x(i,ie(j,n))
	fe(i,j) = f(i,ie(j,n))
    enddo
    enddo

    do i = 1,3
    do j = 1,8
    g(3*(j-1)+i) = id_eqs(i,ie(j,n))
    enddo
    enddo
!///
!	Element force
!///
!	call fem_element_force_elastic_8n_3d(elmfor,			&
!										 xe,				&
!										 fe)
	do i = 1, 3
	do j = 1, 8
	elmfor(3*(j-1)+i) = fe(i,j)
	enddo
	enddo 
!///
!	Assembly of element forces into global force vector
!///
	do i = 1, 24
	if(g(i).ne.0) then
!	sysfor(g(i)) = sysfor(g(i)) + elmfor(i)
	sysfor(g(i)) = elmfor(i)
	endif
	enddo

	enddo

end subroutine fem_force_elastic_8n_3d
!==========================================================================
!                          fem_update_node_variable
!==========================================================================
subroutine fem_update_node_variable(node_variable,		&
									system_variable,	&
									id_eqs,				&
									numnod,				&
									noddof,				&
									numeqs)
    implicit none

	integer			numnod
	integer			noddof
	integer			numeqs
	integer			id_eqs(noddof,numnod)
	real*8			node_variable(noddof,numnod)
	real*8			system_variable(numeqs)

	integer			n
	integer			i

    do n = 1,numnod
    do i = 1,noddof
    if(id_eqs(i,n).ne.0) then
    node_variable(i,n) = system_variable(id_eqs(i,n))
    else
    node_variable(i,n) = 0.d0
    endif
    enddo
    enddo

end subroutine fem_update_node_variable
!=======================================================================
!                              fem_error_message
!=======================================================================
subroutine error_message(subroutine_name, message,llog)

	implicit none

	character*(*)	subroutine_name
	character*(*)	message
!	real*8			llog
        integer         llog

	write(llog,'('' *** error dectected in subroutine '',a40,'':'',/a40)')		&
		subroutine_name,message

	stop

end subroutine error_message
!=======================================================================
!                          fem_elastic_8n_3d_pcg
!=======================================================================
subroutine fem_elastic_8n_3d_pcg(numnod,					&
								 numelm,					&	
								 ie,						&
								 xco,						&
								 disp,						&
								 force,						&
								 id_bdc,					&
								 c,							&
								 young,						&
								 poisson)

	use fem_lib

	implicit none

	integer		numnod
	integer		numelm
	integer		ie(8,numelm)
	real*8		xco(3,numnod)
	real*8		disp(3,numnod)
	real*8		force(3,numnod)
	integer		id_bdc(3,numnod)
	real*8		c(3,3,3,3)
	real*8		young
	real*8		poisson

	integer		numeqs
	integer,	allocatable ::	id_eqs(:,:)
	real*8,		allocatable ::	sysfor(:)
	real*8,		allocatable ::	sysvar(:)

	integer						i
	integer						j
	integer						m
	integer						n
	integer						iters
	integer,	parameter ::	nqd = 8
	integer,	parameter ::	ndim = 3
	integer,	parameter ::	nod  = 8
	integer,	parameter ::	nodof = 3
	integer,	parameter ::	ndof = nod*nodof
	integer,	parameter ::	nst	 = 6
	integer						g(ndof)						
	real*8						dee(nst,nst)
	real*8						coord(nod,ndim)
	real*8						points(nqd,ndim)
	real*8						weights(nqd)
	real*8						der(ndim,nod)
	real*8						jac(ndim,ndim)
	real*8						deriv(ndim,nod)
	real*8						bee(nst,ndof)
	real*8						det
	real*8						elmmat(ndof,ndof)

	real*8,		allocatable ::	p(:)
	real*8,		allocatable ::	r(:)
	real*8,		allocatable ::	x(:)
	real*8,		allocatable ::	xnew(:)
	real*8,		allocatable ::	u(:)
	real*8,		allocatable ::	diag_precon(:)
	real*8,		allocatable ::	d(:)
	real*8						pmul(ndof)
	real*8						utemp(ndof)

	real*8						up
	real*8						alpha
	real*8						beta

	real*8 ::					tol = 1.d-10
	real*8						error
	integer						limit
	logical						converged

	allocate(id_eqs(3,numnod))

	call fem_system_profile(numnod,			&
							id_bdc,			&
							numeqs,			&
							id_eqs)

	write(6,'('' numnod= '', i10)') numnod
	write(6,'('' numelm= '', i10)') numelm
	write(6,'('' numeqs= '', i10)') numeqs
!///
!   Set up arrays for pcg iteration
!///
	allocate(sysfor(numeqs))
	allocate(sysvar(numeqs))
	allocate(p(0:numeqs))
	allocate(r(0:numeqs))
	allocate(x(0:numeqs))
	allocate(xnew(0:numeqs))
	allocate(u(0:numeqs))
	allocate(diag_precon(0:numeqs))
	allocate(d(0:numeqs))

	p = 0.d0
	r = 0.d0
	x = 0.d0
	xnew = 0.d0
	diag_precon = 0.d0
	limit = numeqs
!///
!   obtain the common element matrix
!///
	call deemat(dee,young,poisson)
	call sample('hexahedron',points,weights)

	do n = 1, numelm
    do i = 1,3
    do j = 1,8
    coord(j,i) = xco(i,ie(j,n))
    enddo
    enddo
    
	elmmat = 0.d0
	do i = 1, nqd
	call shape_der (der,points,i)
	jac = matmul(der,coord)
    det = determinant(jac)
	call invert(jac)
    deriv = matmul(jac,der)
	call beemat (bee,deriv) 
    elmmat = elmmat + matmul(matmul(transpose(bee),dee),bee)*det*weights(i)   
    enddo
!///
!   precondition vector
!///
 	do i = 1,3
    do j = 1,8
    g(3*(j-1)+i) = id_eqs(i,ie(j,n))
    enddo
    enddo
    do m = 1, ndof
	diag_precon(g(m)) = diag_precon(g(m)) + elmmat(m,m)
	end do 
    end do

	call fem_force_elastic_8n_3d(numnod,			&
								 numelm,			&
								 numeqs,			&
								 ie,				&
								 id_eqs,			&
								 sysfor,			&
								 xco,				&
								 force)

!--------------------invert the preconditioner and get starting r--------------
	do i = 1, numeqs
	r(i) = sysfor(i)
	enddo 
	r(0) = 0.d0
	diag_precon(1:numeqs) = 1.d0/diag_precon(1:numeqs)
	diag_precon(0) = 0.d0
	d = diag_precon*r
	p = d
!--------------------preconditioned c. g. iterations---------------------------
    iters = 0
    pcg_iteration: do 
				   iters = iters + 1
				   u = 0.d0
				   do n = 1, numelm
				   do i = 1,3
				   do j = 1,8
				   g(3*(j-1)+i) = id_eqs(i,ie(j,n))
				   enddo
				   enddo
				   pmul = p(g)

	elmmat = 0.d0
    do i = 1,3
    do j = 1,8
    coord(j,i) = xco(i,ie(j,n))
    enddo
    enddo
	do i = 1, nqd
	call shape_der (der,points,i)
	jac = matmul(der,coord)
    det = determinant(jac)
	call invert(jac)
    deriv = matmul(jac,der)
	call beemat (bee,deriv) 
    elmmat = elmmat + matmul(matmul(transpose(bee),dee),bee)*det*weights(i)   
    enddo

				   utemp = matmul(elmmat,pmul)
				   u(g) = u(g)+ utemp 
				   end do
!--------------------------pcg equation solution-------------------------------
				   up = dot_product(r,d)
				   alpha = up/dot_product(p,u)
				   xnew = x + p*alpha
				   r = r - u*alpha
				   d = diag_precon*r
				   beta = dot_product(r,d)/up
				   p = d + p*beta
				   error = maxval(abs(xnew-x))/maxval(abs(xnew))
				   converged = (error < tol )
				   x = xnew
				   write(6,'('' pcg iteration = '',i8, '' error = '',e15.5)') iters, error
				   write(111,'(i6,e15.5)') iters, error
				   if(converged .or. iters==limit) exit
	end do pcg_iteration

	do i = 1, numeqs
	sysvar(i) = x(i)
	enddo

	call fem_update_node_variable(disp,				&
								  sysvar,			&
								  id_eqs,			&
								  numnod,			&
								  3,				&
								  numeqs)

end subroutine fem_elastic_8n_3d_pcg
!=======================================================================
!                           fem_elastic_stress8
!=======================================================================
subroutine fem_iso_elastic_stress8(numnod,					&
							       numelm,					&	
							       ie,						&
							       xco,						&
							       disp,					&
							       stress,					&
								   xqd,						&
							       young,					&
							       poisson)

	use fem_lib

	implicit none

	integer		numnod
	integer		numelm
	integer		ie(8,numelm)
	real*8		xco(3,numnod)
	real*8		disp(3,numnod)
	real*8		stress(6,8,numelm)
	real*8		xqd(3,8,numelm)
	real*8		young
	real*8		poisson

	integer						i
	integer						j
	integer						m
	integer						n
	integer,	parameter ::	nqd = 8
	integer,	parameter ::	ndim = 3
	integer,	parameter ::	nod = 8
	integer,	parameter ::	nodof = 3
	integer,	parameter ::	ndof = nod*nodof
	integer,	parameter ::	nst	= 6
!	integer						g(ndof)						
	real*8						dee(nst,nst)
	real*8						coord(nod,ndim)
	real*8						points(nqd,ndim)
	real*8						weights(nqd)
	real*8						fun(nod)
	real*8						der(ndim,nod)
	real*8						jac(ndim,ndim)
	real*8						deriv(ndim,nod)
	real*8						bee(nst,ndof)
	real*8						det
	real*8						elmdisvec(ndof)
	real*8						sigma(nst)
	real*8						xtmp(3)

!///
!   obtain the common element matrix
!///
	call deemat(dee,young,poisson)

	call sample('hexahedron',points,weights)

	
	do n = 1, numelm

    do i = 1, ndim
    do j = 1, nod
    coord(j,i) = xco(i,ie(j,n))
    enddo
    enddo

	do i = 1, nqd
	call shape_fun(fun,points,i)
	xtmp = matmul(fun,coord)
	do j = 1, 3
	xqd(j,i,n) = xtmp(j)
	enddo
	enddo

    do i = 1, ndim
    do j = 1, nod
    elmdisvec(3*(j-1)+i) = disp(i,ie(j,n))
    enddo
    enddo
    
	do i = 1, nqd
	call shape_der (der,points,i)
	jac = matmul(der,coord)
    det = determinant(jac)
	call invert(jac)
    deriv = matmul(jac,der)
	call beemat (bee,deriv) 
    sigma = matmul(dee, matmul(bee,elmdisvec))
	do m = 1, nst
	stress(m,i,n) = sigma(m)
	enddo
!	stress(:,i,n) = sigma
    end do
	enddo

end subroutine fem_iso_elastic_stress8
!=======================================================================
!                           fem_elastic_stress1
!=======================================================================
subroutine fem_iso_elastic_stress1(numnod,					&
							       numelm,					&	
							       ie,						&
							       xco,						&
							       disp,					&
							       stress,					&
							       young,					&
							       poisson)

	use fem_lib

	implicit none

	integer		numnod
	integer		numelm
	integer		ie(8,numelm)
	real*8		xco(3,numnod)
	real*8		disp(3,numnod)
	real*8		stress(6,1,numelm)
	real*8		young
	real*8		poisson

	integer						i
	integer						j
	integer						m
	integer						n
	integer,	parameter ::	nqd = 1
	integer,	parameter ::	ndim = 3
	integer,	parameter ::	nod = 8
	integer,	parameter ::	nodof = 3
	integer,	parameter ::	ndof = nod*nodof
	integer,	parameter ::	nst	= 6
	integer						g(ndof)						
	real*8						dee(nst,nst)
	real*8						coord(nod,ndim)
	real*8						points(nqd,ndim)
	real*8						weights(nqd)
	real*8						der(ndim,nod)
	real*8						jac(ndim,ndim)
	real*8						deriv(ndim,nod)
	real*8						bee(nst,ndof)
	real*8						det
	real*8						elmdisvec(ndof)
	real*8						sigma(nst)

!///
!   obtain the common element matrix
!///
	call deemat(dee,young,poisson)

	call sample('hexahedron',points,weights)

	do n = 1, numelm

    do i = 1, ndim
    do j = 1, nod
    coord(j,i) = xco(i,ie(j,n))
    enddo
    enddo

    do i = 1, ndim
    do j = 1, nod
    elmdisvec(3*(j-1)+i) = disp(i,ie(j,n))
    enddo
    enddo
    
	do i = 1, nqd
	call shape_der (der,points,i)
	jac = matmul(der,coord)
    det = determinant(jac)
	call invert(jac)
    deriv = matmul(jac,der)
	call beemat (bee,deriv) 
    sigma = matmul(dee, matmul(bee,elmdisvec))
!	do m = 1, nst
!	stress(m,i,n) = sigma(m)
!	enddo
	stress(:,i,n) = sigma
    end do
	enddo

end subroutine fem_iso_elastic_stress1
!=======================================================================
!                           displacement_constaint_condition
!=======================================================================
subroutine displacement_constaint_condition(sysmat,		&
											sysvar,		&
											u,			&
											id_bdc,		&
											id_eqs,		&
											kdia,		&
											numnod,		&
											numsys,		&
											numeqs)

	implicit none

	integer			numnod
	integer			numeqs
	integer			numsys
	integer			id_bdc(3,numnod)
	integer			id_eqs(3,numnod)
	integer			kdia(numeqs)
	real*8			sysmat(numsys)
	real*8			sysvar(numeqs)
	real*8			u(3,numnod)

	integer			n
	integer			i
	real*8			tmp


	tmp = 1.d20*maxval(sysmat)

	do n = 1, numnod
	do i = 1, 3
	if(id_bdc(i,n).eq.-1) then
	sysmat(kdia(id_eqs(i,n))) = tmp
	sysvar(id_eqs(i,n)) = tmp*u(i,n)
	endif
	enddo
	enddo

end subroutine displacement_constaint_condition
