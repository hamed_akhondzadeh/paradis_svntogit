!=======================================================================
!                          fem_mesh_data_cylinder
!=======================================================================
subroutine fem_mesh_data_cylinder(radius,			&
								  height,			&
								  nr,				&
								  nh,                           &
                                                                  ageom)
!///
!   This subroutine generates a structured three dimensional mesh
!   of a cylinder domain.
!   input
!   radius      cylinder radius
!   height      cyliner height
!   nr          number of node in diameter
!   nh          number of nodes in height
!   ageom       the rotation matrix of FEM in dd3d coordinate system
!///

	use fem_data
	use tecplot_lib

	implicit none

	real(8)		radius
	real(8)		height

	integer		nr
	integer		nh

	real(8)		ax
	real(8)		ay
	real(8)		az
	integer		nx
	integer		ny
	integer		nz

	real(8)		ax1
	real(8)		ay1
	real(8)		az1
	real(8)		axi
	real(8)		ayi
	real(8)		azi

	real(8)		xco
	real(8)		yco
	real(8)		zco
	real(8)		rco
	real(8)		tmp

        real(8)         ageom(3,3)
	
	integer		i
	integer		j
	integer		k
	integer		m
	integer		n

        real(8)         tmp_coord(3)

        real*8  tor

        tor=1.d-5

        do i = 1, 3 
           do j = 1, 3
              fem_ageom(i,j) = ageom(j,i)
           enddo
        enddo

        call norm(fem_ageom(1,:))
        call norm(fem_ageom(2,:))
        call norm(fem_ageom(3,:))

!        write(*,*) fem_ageom(1,1), fem_ageom(1,2), fem_ageom(1,3)
!        write(*,*) fem_ageom(2,1), fem_ageom(2,2), fem_ageom(2,3)
!        write(*,*) fem_ageom(3,1), fem_ageom(3,2), fem_ageom(3,3)
!        stop


!///
!   calculate number of nodes and elements
!///
	nx = nr
	ny = nr
	nz = nh

	fem_number_of_node    = nx*ny*nz
	fem_number_of_element = (nx-1)*(ny-1)*(nz-1)
!///
!   allocate finite element arrays
!///
    call fem_mesh_allocation
!///
!   initialize finite element arrays
!///
    call fem_mesh_initialization
!///
!   allocate finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_allocation
!///
!   initialize finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_initialization
!///
!   generate node properties
!///

	ax = radius*2.d0
	ay = radius*2.d0
	az = height

	do k = 1, nz
           zco = -0.5d0*az+float(k-1)*az/float(nz-1)
           do j = 1, ny
              do i = 1, nx
                 xco = -0.5d0*ax+float(i-1)*ax/float(nx-1)
                 yco = -0.5d0*ay+float(j-1)*ay/float(ny-1)
                 rco = sqrt(xco*xco+yco*yco)
                 if(rco.ne.0.d0)then
                    tmp = max(abs(xco), abs(yco))
                    xco = xco*tmp/rco
                    yco = yco*tmp/rco
                 endif
                 n   = (k-1)*ny*nx+(j-1)*nx+i
!///
!   compute node coordinates
!///
                 fem_node_coordinate(1,n) = xco
                 fem_node_coordinate(2,n) = yco
                 fem_node_coordinate(3,n) = zco

              enddo
           enddo
	enddo

!///
!   assign node displacement boundary condition
!   1 -- displacement need to be solved
!   0 -- displacement fixed to be 0
!  -1 -- displacement prescribed 
!///

!///////////////////////////////////////////////////////////////
! choice #5 all surfaces are traction free,fix 8 corners
! Question: fix which corners???
! temporarily: fix both top and bottom surface. 
!//////////////////////////////////////////////////////////////

        if(fem_BC_type.eq.5) then
           do n = 1, fem_number_of_node

              if(dabs(fem_node_coordinate(3,n)+height/2.d0).lt.tor.or. & 
                 dabs(fem_node_coordinate(3,n)-height/2.d0).lt.tor ) then 
                 fem_node_boundary_condition(1,n) = 0
                 fem_node_boundary_condition(2,n) = 0
                 fem_node_boundary_condition(3,n) = 0
              endif

              rco = dsqrt(fem_node_coordinate(1,n)**2+fem_node_coordinate(2,n)**2)
              if(dabs(rco-radius).le.tor.and. &
                 fem_node_boundary_condition(1,n).eq.1.and.&
                 fem_node_boundary_condition(2,n).eq.1.and.&
                 fem_node_boundary_condition(3,n).eq.1) then 
                 fem_node_img_id(n) = 1
              endif
           enddo
        endif
!///
!   generate element properties
!///
	do k = 1, nz-1
	do j = 1, ny-1
	do i = 1, nx-1
!///
!	connectivity, i.e., relation between the local nodes and the global nodes
!///
	n = (k-1)*(ny-1)*(nx-1)+(j-1)*(nx-1)+i
	m = (k-1)*ny*nx+(j-1)*nx+i
	fem_element_node(1,n) = m
	fem_element_node(2,n) = m + ny*nx
	fem_element_node(3,n) = m + ny*nx+1
	fem_element_node(4,n) = m + 1
	fem_element_node(5,n) = m + nx
	fem_element_node(6,n) = m + nx + ny*nx
	fem_element_node(7,n) = m + nx + ny*nx + 1
	fem_element_node(8,n) = m + nx + 1
	enddo 
	enddo
	enddo 
!///
!   get mesh data for fem and dd 
!///
    call fem_dd_mesh_data
!///
!   Output mesh data for tecplot (optional)
!///

!    do n = 1, fem_number_of_node
!       do i = 1, 3

!          tmp_coord(i) = fem_ageom(1,i)*fem_node_coordinate(1,n) &
!               +fem_ageom(2,i)*fem_node_coordinate(2,n) &
!               +fem_ageom(3,i)*fem_node_coordinate(3,n)
!       enddo
       
!       fem_node_coordinate(:,n) = tmp_coord(:)
!    enddo


	call tecplot_fem_mesh(fem_node_coordinate,					&
						  1.d0,					&
						  fem_element_node,			&
						  'BRICK',				&
						  'cylinder_element_mesh.dat',		&
						  fem_ltmp)
        write(*,*) 'cylinder_element_mesh.dat printed' 

	call tecplot_fem_mesh(fem_node_coordinate,				                &
						  1.d0,					        &
						  fem_boundary_surface_element_node,            &
						  'QUADRILATERAL',			        &
						  'element_boundary_surface_mesh_cylinder.dat',    &
						  fem_ltmp)


end subroutine fem_mesh_data_cylinder
