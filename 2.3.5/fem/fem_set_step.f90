!======================================================================= 
!                         fem_set_step
! set fem_istep
!======================================================================= 
        subroutine fem_set_step(istep)
	use fem_data 
	use tecplot_lib 
 
	implicit none

        integer istep
        
	fem_istep = istep
        
        end subroutine fem_set_step

