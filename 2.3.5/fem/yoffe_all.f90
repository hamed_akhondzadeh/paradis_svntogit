! Subroutines in this file are extracted from the version:
! FILM/SOURCE/SOURCE_May24_04.tar. 
! 6/18/2004 
!==========================================================================
! This subroutine calculates the Yoffe's image stress numerically. 
! Currently, sh_r0=1, sh_rc=1.d-3 are chosen and tested for a case
! when alpha=pi/10, dr=sh_rc/2 and for b=bs. The worst error bar is 
! in the order of 1/10000. Testing is done by comparing analytical 
! solution from numerical method.
!==========================================================================
        subroutine sh_image_stress_num(rr,rsurf,rmid,surf_norm,isign, &
                                       xburgers,xmu,xnu,sigma2)
 
	use fem_data 
	use tecplot_lib 
 
	implicit none 

        integer         i, j, ii 
        real(8)         rsurf(3),rmid(3),rr(3)
        real(8)         v(3),vp(3),xmu,xnu
        real(8)         bs,be,bx
	real(8)		x,y,z,eta,zeta,etap,zetap,r
        real(8)         bp

	real(8)		sigma_tot(6),sigma_inf(6)
	real(8)		sigma1(6),sigma2(6)
        real(8)         sigma(6)

        real(8)         r_factor,ra(3),rb(3),rc(3)
        real(8)         dr, dra, drb 
        real(8)         beta 
        real(8)         rp(3)

        real(8)         line_vector(3),a(3,3)
        real(8)         alpha, line_vectorp(3)
        real(8)         xburgers(3),xburgersp(3)

        real(8)         surf_norm(3)
        real(8)         line_sense(3)
        integer         isign 

!M sh_r0: radius of the reference hemisphere; sh_rc: cutoff distance from dislocation line
        real(8)         sh_rc, sh_r0 

        logical       help, irot 
        real(8)         tor 
        real(8)         alpha_old

        real(8)         sum

        help=.false.
!       help=.true. 

        tor=1.d-10 

        call norm(surf_norm)
        call norm(line_sense)

!M first decide if the plane containing the dislocation line needs to be rotated so
! that 1) the disl. lies in the yz plane; 2) dislocation lies in the negative z area
        do i=1, 3
           line_vector(i)=rsurf(i)-rmid(i)
           v(i)=rr(i)-rsurf(i)
        enddo
        call norm(line_vector)
!
!  not clear how to define line_sense in the nodal description. 
!
        line_sense(:)=line_vector(:)

        if(help) write(*,*) 'v=', v


!        v(1)=50.d0
!        v(2)=150.d0
!        v(3)=1.d0

        sum=0.d0
        do i=1, 3
           sum = sum + surf_norm(i)*line_vector(i)
        enddo
        if(dabs(sum).le.tor) then 
           stop 'Dislocation lies at the surface!' 
        elseif(sum.lt.-tor) then 
           stop 'Dislocation in the wrong half-space!' 
        endif

        if(help) then 
           write(*,*) 'isign=', isign 
           write(*,*) 'rsurf=', rsurf
           write(*,*) 'rmid=', rmid 
        endif

        call rot_plane(line_vector,surf_norm,irot,a)
!        write(*,*) 'irot=', irot
!        write(*,*) 'a=', a

       if(help) then
           write(*,*) 'irot=', irot
           write(*,*) 'a=', a
        endif

	if(irot) then 
	   call rotate_vector(xburgers,xburgersp,a,1)
	   call rotate_vector(v,vp,a,1)	 
	   call rotate_vector(line_vector,line_vectorp,a,1)
	else
	   do i=1, 3
	      xburgersp(i)=xburgers(i)
	      vp(i)=v(i)
              line_vectorp(i)=line_vector(i)
	   enddo 
	endif 

        alpha=acos(line_vectorp(3))
        if(alpha.lt.-tor) stop 'why is alpha negative?!'

        alpha_old=acos(line_vectorp(3))
        if(dabs(alpha_old-alpha).gt.tor) then
           write(*,*) 'alpha, alpha_old=', alpha, alpha_old
           write(*,*) '!!!!!!!A HA!!!!!!!!!!!!!!!'
           read(*,'()')
        endif

        bx=xburgersp(1)
        bs=xburgersp(3)*cos(alpha)-xburgersp(2)*sin(alpha)
        be=xburgersp(2)*cos(alpha)+xburgersp(3)*sin(alpha) 

        sh_rc=1.d-3
        sh_r0=1.d0

       if(help) then
          write(*,*) '****Enter yoffe_image_num.F**** '
       endif 

       do i=1,3 
          rp(i)=vp(i)
       enddo 

        x=rp(1)
        y=rp(2)
        z=rp(3)
        r=dsqrt(x*x+y*y+z*z)

        if(help) then 
           write(*,*) 'rp=', rp 
           write(*,*) 'r=', r
        endif

        if(r.lt.tor) then 
           write(*,*) 'x, y, z=', x, y, z
           stop 'r<1.d-10' 
        endif 

        r_factor=sh_r0/r
 
        if(help) write(*,*) 'r_factor=', r_factor

        do i=1, 3
           rp(i)=rp(i)*r_factor 
        enddo 
        x=rp(1)
        y=rp(2)
        z=rp(3)

        etap =-y*cos(alpha)-z*sin(alpha)
        zetap=-z*cos(alpha)+y*sin(alpha)

        dr=dsqrt(etap**2+x**2)

        if(help) then 
           write(*,*) 'dr, sh_rc=', dr, sh_rc 
        endif 

        do i=1, 6
           sigma(i) =0.d0
           sigma1(i)=0.d0
           sigma2(i)=0.d0
           sigma_tot(i)=0.d0
           sigma_inf(i)=0.d0
        enddo 

        if((dr-sh_rc).gt.tor) then
            call sh_total_stress  (vp,bs,be,bx,alpha,xmu,xnu,sigma_tot)
            call InfSeg_Rot_Stress(vp,bs,be,bx,alpha,xmu,xnu,sigma_inf)
            
            do i=1, 6
               sigma(i)=sigma_tot(i)-sigma_inf(i)
            enddo 

            if(help) then
               write(*,*) 'vp=', vp 
               write(*,*) 'dr > sh_rc: Tot-Inf:'
               write(*,*) sigma
!               pause 
            endif 

           else
!M center_of_circle ordinate in the x-etap-zetap frame
            rc(1)=0.d0
            rc(2)=0.d0
            rc(3)=zetap
            
            if(dabs(x).lt.tor.and.dabs(etap).lt.tor) then
               ra(1)=sh_rc
               ra(2)=0.d0
             else
               ra(1)=x*sh_rc/dr
               ra(2)=etap*sh_rc/dr
            endif 

            ra(3)=dsqrt(sh_r0**2-sh_rc**2)

            if(help) write(*,*) 'ra=', ra

            rb(1)=-ra(1)
            rb(2)=-ra(2)
            rb(3)=ra(3)

            if(help) write(*,*) 'rb=', rb

!M     x=beta*ra(1)+(1-beta)*rb(1)
!M or  etap=beta*ra(2)+(1-beta)*rb(2)

            if(dabs(ra(1)-rb(1)).gt.tor) then 
               beta=(x-rb(1))/(ra(1)-rb(1))
              else
               beta=(etap-rb(2))/(ra(2)-rb(2))
            endif 

            if(help) then 
               write(*,*) 'beta=', beta
!               pause 'check beta' 
            endif 

!M transform the coordinates of A and B back to x-y-z frame 
            rp(1)=ra(1)
            rp(2)=-cos(alpha)*ra(2)+sin(alpha)*ra(3)
            rp(3)=-sin(alpha)*ra(2)-cos(alpha)*ra(3)
            
            call sh_total_stress(rp,bs,be,bx,alpha,xmu,xnu,sigma_tot)
            call InfSeg_Rot_Stress(rp,bs,be,bx,alpha,xmu,xnu,sigma_inf)
            do i=1, 6
               sigma1(i)=sigma_tot(i)-sigma_inf(i)
            enddo 

            rp(1)=rb(1)
            rp(2)=-cos(alpha)*rb(2)+sin(alpha)*rb(3)
            rp(3)=-sin(alpha)*rb(2)-cos(alpha)*rb(3)
            
            call sh_total_stress(rp,bs,be,bx,alpha,xmu,xnu,sigma_tot)
            call InfSeg_Rot_Stress(rp,bs,be,bx,alpha,xmu,xnu,sigma_inf)
            do i=1, 6
               sigma2(i)=sigma_tot(i)-sigma_inf(i)
            enddo 

            do i=1, 6
               sigma(i)=(1.d0-beta)*sigma2(i)+beta*sigma1(i)
               sigma(i)=sigma(i)*r_factor 
            enddo 

            if(help) then 
               write(*,*) 'interpolated stress='
               write(*,*) sigma
               if((dr-sh_rc).lt.tor) write(*,*) 'dr < sh_rc'
!               pause 
            endif 
            
        endif 

        if(irot) then 
           call rotate_matrix(sigma2,sigma,a,2)
        else
           do j=1, 6
              sigma2(j)=sigma(j)
           enddo
        endif         

        do j=1, 6
           sigma2(j)=sigma2(j)*isign
        enddo 

        if(help) then 
           write(*,*) 'sh_image=', sigma2
!           pause 
        endif

       end subroutine sh_image_stress_num 


!==================================================================
! This subroutine constructs the rotation matrix between the 
! local frame & the global frame:
! Global frame: the original x,y,z coordinates systems
! Local frame: z' along <001> orientation,  
!              y'z' chosen to contain the dislocation line 
!===================================================================
      subroutine rot_plane(line_vector,surf_norm,irot,amatr)
      implicit none

      real*8 line_vector(3)
      real*8 azp(3),axp(3),ayp(3)
      real*8 tor
      real*8 amatr(3,3)
      logical irot
      integer i, j
      real*8 surf_norm(3)
      real*8 sum 
      logical help

      help=.false.
!      help=.true. 

      tor=1.d-10

      irot=.true.

      do i=1, 3
         do j=1, 3
            amatr(i,j)=0.d0
         enddo 
      enddo 

      sum=0.d0
      do i=1, 3
         sum=sum+line_vector(i)*surf_norm(i)
      enddo
      if(sum.lt.-tor) stop 'why sum< 0?' 

      if(help) then
         write(*,*) 'surf_norm=', surf_norm 
         write(*,*) 'line_vector=', line_vector
      endif

      azp(:)=surf_norm(:)

!
!M situation where surf_norm along <001> 
!
      if(dabs(surf_norm(3)-1.d0).lt.tor) then
         
         if(dabs(sum-1.d0).lt.tor) then 
            irot=.false.
            if(help) write(*,*) 'disl. lies along <001>' 
            return
         endif

         call rvector(azp,line_vector,axp)
         call norm(axp)          

         if(dabs(axp(1)-1.d0).lt.tor) then 
            irot=.false.
            if(help) write(*,*) 'disl. lies in yz plane: no rotation' 
            return
         endif

      else
!
!M situation when surf_norm.ne.001 (note: 001 is the special case; all others are equal)
!
         if(dabs(sum-1.d0).lt.tor) then 
            axp(1)=1.d0
            axp(2)=0.d0
            axp(3)=0.d0
 !           if(help) write(*,*) 'disl. lies along surf_norm & surf_norm is NOT 001' 
         else
            call rvector(azp,line_vector,axp)
            call norm(axp)          
         endif

      endif
!
!M build rotation matrix between local frame (Yoffe setup,primed) and global frame 
!M from local -> global: aij=cos(xi',xj); from global to local: aij'=cos(xi,xj')=transpose of aij
!
      call rvector(azp,axp,ayp)
      call norm(ayp)
      call RotMatrix(axp,ayp,azp,amatr)

      if(help) then 
         write(*,*) 'axp=', axp
         write(*,*) 'ayp=', ayp
         write(*,*) 'azp=', azp
      endif
      end

      subroutine rotate_vector(a,b,rot,iopt)
      implicit none
      real*8 a(3),b(3),rot(3,3)
      integer i,j,ii,jj,iopt

! Similar to rot2 (for matrix), this is for vector
! ROT=matrix from frame 1 (crystal or local) --> 2 (macro or global)
! rotates global (a)--> local(b) if iopt=1
!         local (b)--> global (a) if iopt=2
!M frame1=crystal; frame2=macro
!        write(*,*) 'ROT'
!        do i=1,3
!        write(*,*) (rot(i,j),j=1,3)
!        enddo

        go to (10,20),iopt

 10     continue
        do i=1,3
          b(i)=0.d0
          do j=1, 3
             b(i)=b(i)+rot(i,j)*a(j)
          enddo
        enddo

        return

20      continue
        do j=1,3
          a(j)=0.d0
          do i=1, 3
             a(j)=a(j)+b(i)*rot(i,j)
          enddo
        enddo

        return

        end 

      subroutine rotate_matrix(sig,sigp,rot,iopt)
      implicit none
      real*8 sig(6),sigp(6)
      real*8 a(3,3),b(3,3),rot(3,3)
      integer i,j,ii,jj,iopt

! ROT=matrix from frame 1 (crystal or local) --> 2 (macro or global)
! rotates global (a) --> local(b) if iopt=1
!         local (b)--> global(a) if iopt=2
!M frame1=crystal; frame2=macro
!        write(*,*) 'ROT'
!        do i=1,3
!        write(*,*) (rot(i,j),j=1,3)
!        enddo

        go to (10,20),iopt

10      continue

        a(1,1)=sig(1)
        a(1,2)=sig(2)
        a(1,3)=sig(3)
        a(2,2)=sig(4)
        a(2,3)=sig(5)
        a(3,3)=sig(6)

        a(2,1)=a(1,2)
        a(3,1)=a(1,3)
        a(3,2)=a(2,3)

        do i=1,3
           do j=1,3
              b(i,j)=0.d0
              do ii=1,3
                 do jj=1,3
                    b(i,j)=b(i,j)+rot(i,ii)*rot(j,jj)*a(ii,jj)
                 enddo
              enddo
           enddo
        enddo

        sigp(1)=b(1,1)
        sigp(2)=b(1,2)
        sigp(3)=b(1,3)
        sigp(4)=b(2,2)
        sigp(5)=b(2,3)
        sigp(6)=b(3,3)

        return

 20     continue
        b(1,1)=sigp(1)
        b(1,2)=sigp(2)
        b(1,3)=sigp(3)
        b(2,2)=sigp(4)
        b(2,3)=sigp(5)
        b(3,3)=sigp(6)

        b(2,1)=b(1,2)
        b(3,1)=b(1,3)
        b(3,2)=b(2,3)
        
        do i=1,3
           do j=1,3
              a(i,j)=0.d0
              do ii=1,3
                 do jj=1,3
                    a(i,j)=a(i,j)+rot(ii,i)*rot(jj,j)*b(ii,jj)
                 enddo
              enddo
           enddo
        enddo

        sig(1)=a(1,1)
        sig(2)=a(1,2)
        sig(3)=a(1,3)
        sig(4)=a(2,2)
        sig(5)=a(2,3)
        sig(6)=a(3,3)

        return

        end 

!==========================================================================
! This subroutine implements the Yoffe's stress fields for an semi-infinite
! dislocation intersecting with a semi-infinite free surface with arbitrary
! Burgers vector and with an arbitrary angle to the free surface. 
! The results implemented are from Ref. 1, which used Yoffe's ideas in 
! Ref. 2 & 3
! 1 Phil. Mag., 44, 657-665 (1981), S. J. Shaibani and P. M. Hazzledine
! 2 Phil. Mag., 5, 161 (1960); 
! 3 Phil. Mag., 6, 1147 (1961). 
! Anoter ref. gives correction to one misprint in Ref. 1: 
! conference paper, ICSMA'82 (ICSMA6), editor: R.C.Gifkins, 
! NY, Pergamon Press (1983), p45. P.M.Hazzledine and S. J. Shaibani 
! The results for alpha=0 is programmed based on Ref. by K. Honda:
! Jap. J. Appl. Phys., 18, 215 (1979) (image stress only). 
! The alpha=0 solution is also programmed directly from SH solution.
!==========================================================================
        subroutine sh_total_stress(rp,bs,be,bx,alpha,xmu,xnu,sigma_sh)
 
	use fem_data 
	use tecplot_lib 
 
	implicit none 

        integer         i, j, ii 
        real(8)         rp(3),alpha,xmu,xnu
        real(8)         bs,be,bx
	real(8)		x,y,z,eta,zeta,etap,zetap,r
        real(8)         a,b,bp
        real(8)         k, one_minus_2nu,two_alpha
        real(8)         c, c1, k1, k2   
        real(8)         s
	real(8)		sigma_bs(6)
        real(8)         qe,de
	real(8)		sigma_be(6)
        real(8)         qx,dx
        real(8)         sigma_bx(6)
        real(8)         sigma_sh(6)
        real(8)         term_m,term2_m,m(6),pbx,pby,pbz

        real(8)         d_factor, xy2  

        real(8)         pi,zero,one,two,half,three,four,tor 

        real(8)         tm1, tm2, tm3, tm4, tm5, tm6,tm7,tm8

        logical       help

        help=.false.
!      help=.true. 

       if(help) then
          write(*,*) 'in yoffe.F: '
          write(*,*) 'bx,bs,be=',bx,bs,be
       endif 

        pi=3.1415927d0 
        zero=0.d0 
        tor=1.d-10 
        one=1.d0
        two=2.d0
        three=3.d0
        four=4.d0
        half=0.5d0 


        do i=1, 6
           sigma_bs(i)=zero
           sigma_be(i)=zero 
           sigma_bx(i)=zero
           sigma_sh(i)=zero 
        enddo 

        two_alpha=two*alpha 
        one_minus_2nu=one-two*xnu 

        x=rp(1)
        y=rp(2)
        z=rp(3)
        r=dsqrt(x*x+y*y+z*z)
        a=r-z

!        if(r.lt.10.d0) write(*,*) 'yoffe: r<10', r 

!#ifdef debug 
!cM An independent set of results given by Honda plus the 
!cM solutions for a screw or an edge in an infinite medium.
!
!        if(alpha.lt.tor) then
!           write(*,*) 'alpha=zero=', alpha 

!           xy2=x**2+y**2 

!           if(dabs(bs).gt.tor) then

!              write(*,*) 'b=bs=', bs

!            d_factor=xmu*bs/two/pi
!           sigma_bs(1)=d_factor*(-two*x*y/r/a**2)                    
!           sigma_bs(4)=d_factor*( two*x*y/r/a**2)
!            sigma_bs(6)=zero
!M superimpose image solution with regular solution 
!            sigma_bs(5)=d_factor*x/xy2
!            sigma_bs(5)=d_factor*(-x/r/a)
!     &                 +d_factor*x/xy2
!            sigma_bs(3)=d_factor*(-y/xy2)
!            sigma_bs(3)=d_factor*y/r/a
!     &                 +d_factor*(-y/xy2)
!            sigma_bs(2)=d_factor*(x**2-y**2)/r/a**2
!           endif
           
!           if(dabs(be).gt.tor) then
!
!              write(*,*) 'b=be=', be
!
!            d_factor=xnu*xmu*be/two/pi/(one-xnu)
!            sigma_be(1)=d_factor*(two*x*(z*x**2*(three*r-z)
!     &                    -r**2*a*(r+two*z))/r**3/a**3 
!     &                    +one_minus_2nu*x*(r*a-two*y**2)/r/a**3)
!     &                 +d_factor/xnu*x*(x**2-y**2)/xy2**2                   
!            sigma_be(4)=d_factor*(two*x*(z*y**2*(three*r-z)
!     &                    -r**3*a)/r**3/a**3 
!     &                   +one_minus_2nu*x*(three*r*a-two*x**2)/r/a**3)
!     &                 +d_factor/xnu*x*(three*y**2+x**2)/xy2**2     
!            sigma_be(6)=d_factor* two*x*(z*a-r**2)/r**3/a
!     &                 +d_factor*two*x/xy2 
!            sigma_be(5)=d_factor*(-two*x*y*z*(two*r-z)/r**3/a**2)
!            sigma_be(3)=d_factor*two*z*(r**2*a-x**2*(two*r-z))
!     &                     /r**3/a**2
!            sigma_be(2)=d_factor*(two*y*z*(x**2*(3*r-z)-r**2*a)
!     &                     /r**3/a**3+one_minus_2nu*y*(two*x**2
!     &                     -r*a)/r/a**3)
!     &                 +d_factor/xnu*y*(x**2-y**2)/xy2**2 
!           endif

!           if(dabs(bx).gt.tor) then

!            if(help) write(*,*) 'b=bx=', bx

!            d_factor=xnu*xmu*bx/two/pi/(one-xnu) 
!            sigma_bx(1)=d_factor*(two*y*(r**3*a-x**2*z*(three*r-z))
!     &                    /r**3/a**3+one_minus_2nu*y*(two*y**2
!     &                    -three*r*a)/r/a**3)
!     &                 +d_factor/xnu*(-y*(three*x**2+y**2)/xy2**2)
!            sigma_bx(4)=d_factor*(two*y*(r**2*a*(r+two*z)
!     &                    -y**2*z*(three*r-z))/r**3/a**3
!     &                    +one_minus_2nu*y*(two*x**2-r*a)
!     &                    /r/a**3)
!     &                 +d_factor/xnu*y*(x**2-y**2)/xy2**2
!            sigma_bx(6)=d_factor*two*y*(r**2-z*a)/r**3/a
!     &                 +d_factor*two*(-y/xy2)
!            sigma_bx(5)=d_factor*two*z*(y**2*(two*r-z)-r**2*a)
!     &                    /r**3/a**2
!            sigma_bx(3)=d_factor*two*x*y*z*(two*r-z)/r**3/a**2
!            sigma_bx(2)=d_factor*(two*x*z*(r**2*a-y**2*(three*r-z))
!     &                    /r**3/a**3+one_minus_2nu*x*(r*a
!     &                    -two*y**2)/r/a**3)
!     &                 +d_factor/xnu*x*(x**2-y**2)/xy2**2 


!           endif 

!         do i=1, 6
!          sigma_sh(i)=sigma_bs(i)+sigma_be(i)+sigma_bx(i)
!         enddo 
         
!         return 
!        endif 
!#endif 

        eta =y*cos(alpha)-z*sin(alpha)
        zeta=z*cos(alpha)+y*sin(alpha)
        etap =-y*cos(alpha)-z*sin(alpha)
        zetap=-z*cos(alpha)+y*sin(alpha)

        b=r-zeta
        bp=r-zetap

        if( (dabs(x).lt.tor.and.dabs(y).lt.tor).or. & 
            (dabs(a).lt.tor).or.                    &
            (dabs(b).lt.tor).or.(dabs(bp).lt.tor) ) then
           write(*,*) 'x, y=', x, y
           write(*,*) 'a=', a 
           write(*,*) 'b, bp=', b, bp 
           write(*,*) 'ENCOUNTER STRESS SINGULARITY POINT,NOT CALCULATED'
           read(*,'()') 
           RETURN
        endif 

        pbx=x/r
        pby=y/r-sin(alpha)
        pbz=z/r-cos(alpha)

        k=four*(one-xnu)*one_minus_2nu
        if(alpha.lt.tor) then 
           c=zero
           c1=one_minus_2nu/r*(-one/a+z/a**2-two*y**2/a**3)
           k1=-k*y/a**2
           k2=k*(y/a**2+y/r/a)
          else
           c=two*one_minus_2nu/tan(alpha)        &
            *(cos(alpha)/r/a+y*sin(alpha)/r/a/a-one/r/b)
           c1=c/tan(alpha)
           k1=k/tan(alpha)**2*sin(alpha)*(one/a-one/b)
           k2=k/tan(alpha)*(-one/a+cos(alpha)**2/b+y*sin(alpha)/r/b)
        endif 

!M Stress fields for a screw dislocation bs
        if(dabs(bs).lt.tor) goto 11 

        if(help) write(*,*) 'b=bs=', bs 

        s=2.d0*pi/xmu/bs

        term_m=-x*sin(two_alpha)*(r+b)/r**3/b**2

        m(1)=term_m*x*x
        m(2)=term_m*x*y
        m(3)=term_m*x*z
        m(4)=term_m*y*y
        m(5)=term_m*y*z
        m(6)=term_m*z*z

        sigma_bs(1)=m(1)+two*(one-xnu)*x*sin(two_alpha)/r/b  & 
                  -two*x*eta*cos(alpha)**2/r/b**2+c*x
        sigma_bs(4)=m(4)-half*x*sin(two_alpha)*              &
                   (one/r/bp+one/r/b+two/b**2)               &
                   +two*x*y*cos(alpha)*(one+sin(alpha)**2)/r/b**2-c*x
        sigma_bs(6)=m(6)+half*x*sin(two_alpha)               &
                  *(one/r/bp-one/r/b+two*z*cos(alpha)/r/b**2)
        sigma_bs(5)=m(5)+half*x*cos(two_alpha)*(one/r/bp-one/r/b) &
                    + two*x*z*sin(alpha)**2*cos(alpha)/r/b**2
        sigma_bs(3)=m(3)+half*cos(alpha)*(etap/r/bp+eta/r/b)      &
                   + z*sin(two_alpha)/r/b
        sigma_bs(2)=m(2)-half*sin(alpha)*(etap/r/bp-eta/r/b)           &
                  +cos(alpha)/r*(two*x*x/b/b-two*z*cos(alpha)/b-one)   &
                  +y*c+two*one_minus_2nu*cos(alpha)*(one/b-one/a)

        do i=1, 6
           sigma_bs(i)=sigma_bs(i)/s
        enddo 

        if(help) write(*,*) sigma_bs

!M Stress fields for an edge dislocation be
 11     continue

        if(dabs(be).lt.tor) goto 22 

        if(help) write(*,*) 'b=be=', be 

        qe=one/bp-one/b+two*z*cos(alpha)/b/b
        de=four*pi*(one-xnu)/xmu/be

        term_m=x*qe/r**3+four*(one-xnu)*x*cos(alpha)**2*(r+b)/r**3/b**2
        term2_m=x*(one/bp**2-one/b**2+four*z*cos(alpha)/b**3)

        m(1)=term_m*x*x+term2_m*pbx*pbx
        m(2)=term_m*x*y+term2_m*pbx*pby
        m(3)=term_m*x*z+term2_m*pbx*pbz
        m(4)=term_m*y*y+term2_m*pby*pby
        m(5)=term_m*y*z+term2_m*pby*pbz
        m(6)=term_m*z*z+term2_m*pbz*pbz

        sigma_be(1)=m(1)-x*qe/r-four*x*cos(alpha)**2/b**2        & 
                  -k*cos(alpha)**2*x/r/b-two*(one-xnu)*c1*x

        sigma_be(4)=m(4)+x*qe/r*(one-two*(one-xnu)*sin(alpha)**2)    &
                  +four*x*cos(alpha)**2*(two*(one-xnu)/r/b-one/b**2) &
                  +two*(one-xnu)*c1*x

        sigma_be(6)=m(6)+x*qe/r*(one-two*(one-xnu)*cos(alpha)**2)    &
                  +four*x*z*cos(alpha)*(one/r/bp**2-one/r/b**2)

        sigma_be(5)=m(5)+x*qe/r*(one-xnu)*sin(two_alpha)+two*x*cos(alpha) &
                  *(y-r*sin(alpha))*(one/r/bp**2-one/r/b**2)              &
!M this term is added as a correction in a later conference paper         
                  -four*(one-xnu)*x*z*cos(alpha)*sin(two_alpha)           &
                  /r/b**2 

        sigma_be(3)=m(3)-qe/r*(z+r*cos(alpha))+two*x**2*cos(alpha)        &
                   *(one/r/bp**2-one/r/b**2)-four*z*cos(alpha)**2         &
                  *((one-xnu)/r/b-one/b**2)

        sigma_be(2)=m(2)-qe/r*(y-r*sin(alpha))-four*(one-xnu)*y           &
                   *cos(alpha)**2/r/b+k1                                  &
                  -two*(one-xnu)*y*c1 

        do i=1,6
           sigma_be(i)=sigma_be(i)/de
        enddo 


        if(help) write(*,*) sigma_be

!M Stress fields for a dislocation with bx
 22     continue

        if(dabs(bx).lt.tor) goto 33 

        if(help) write(*,*) 'b=bx=', bx 

        qx=etap/bp-eta/b-two*z*eta*cos(alpha)/b**2
        dx=four*pi*(one-xnu)/xmu/bx

        term_m=qx/r**3-two*one_minus_2nu*cos(alpha)*             &
              (y*(r+b)/r**3/b**2-sin(alpha)/r/b**2)
        term2_m=etap/bp**2-eta/b**2-four*z*eta*cos(alpha)/b**3

        m(1)=term_m*x*x+term2_m*pbx*pbx
        m(2)=term_m*x*y+term2_m*pbx*pby
        m(3)=term_m*x*z+term2_m*pbx*pbz
        m(4)=term_m*y*y+term2_m*pby*pby
        m(5)=term_m*y*z+term2_m*pby*pbz
        m(6)=term_m*z*z+term2_m*pbz*pbz
        
        sigma_bx(1)=m(1)+qx/r+two*eta*cos(alpha)*(z+r*cos(alpha))  & 
                  /r/b**2-two*cos(alpha)*(two*one_minus_2nu*y/r/b  &
                  -(one-four*xnu)*sin(alpha)/b)                    &
                  +k2                                              &
                  +two*(one-xnu)*y*c1/cos(alpha)                   

        sigma_bx(4)=m(4)-qx/r*(one-two*xnu*sin(alpha)**2)+two*eta          &
                  *cos(alpha)**2*(z*cos(alpha)+r)/r/b**2+sin(two_alpha)    &
                  *((two*xnu*y*sin(alpha)-z*cos(alpha))/r/b-four*xnu/b)    &
                  +k1/cos(alpha)                                           &
                  -two*(one-xnu)*y*c1/cos(alpha)

        sigma_bx(6)=m(6)-qx/r*(one-two*xnu*cos(alpha)**2)+four*z*etap      &
                  *cos(alpha)/r/bp**2-z**2*sin(two_alpha)/r/b**2+two*y      &
                  *cos(alpha)*(cos(alpha)**2/b**2-(one-two*xnu              &
                  *cos(alpha)**2)/r/b)


        sigma_bx(5)=m(5)+two*cos(alpha)*(y-r*sin(alpha))*(etap/r/bp**2     &
                  -z*sin(alpha)*(one+two*xnu)/r/b**2)+two*xnu*cos(alpha)   &
                  *(z+r*cos(alpha))*(one/r/bp-one/r/b)

        sigma_bx(3)=m(3)+two*x*etap*cos(alpha)/r/bp**2+x*sin(alpha)       &
                  *(one/r/bp-one/r/b-two*z*cos(alpha)/r/b**2)             

        sigma_bx(2)=m(2)+x*cos(alpha)*(one/r/bp+(three-four*xnu)/r/b      &
                  +two*z*cos(alpha)/r/b**2)-k*cos(alpha)*x/r/b             &
                  -two*(one-xnu)*c1*x/cos(alpha)


       do i=1, 6 
         sigma_bx(i)=sigma_bx(i)/dx 
       enddo 

        if(help) write(*,*) sigma_bx

 33    continue

       do i=1, 6
          sigma_sh(i)=sigma_bs(i)+sigma_be(i)+sigma_bx(i)
       enddo 

       end subroutine sh_total_stress


! Stress calculation for an infinite long segment 
!*************************************************************
! STRESS DUE TO THE IS-TH SEGMENT AT POINT R
! UPPER HALF ONLY IN ORDER OF 11,12,13,22,23,33
!*************************************************************
	SUBROUTINE InfSeg_Rot_Stress(vp,bs,be,bx1,the_alpha,XMU,XNU,SIGMA)
	implicit none

	INTEGER I, icase 
	real*8 bs,be,bx1
	real*8 xburgp(3),xburgpp(3)
	real*8 bx,by,bz
	REAL*8 XMU,XNU
	REAL*8 R(3),vp(3)
	REAL*8 SIGMA(6),sigma2(6)
	real*8 x,y, xy2,tor 
	real*8 sigma_bx(6),sigma_by(6),sigma_bz(6)

	logical help 

	logical irot
	real*8  the_alpha 
	real*8  bxp(3),byp(3),bzp(3),b(3,3)

!	help=.true.
	help=.false.

	tor=1.d-10 

	irot=.false.
	if(the_alpha.gt.tor) irot=.true.

	xburgp(1)=bx1
	xburgp(2)=be*cos(the_alpha)-bs*sin(the_alpha)
	xburgp(3)=bs*cos(the_alpha)+be*sin(the_alpha)	

	if(help) then 
	   write(*,*) 'vp=', vp
	   write(*,*) 'bs,be,bx=', bs,be,bx1
	   write(*,*) 'xburgp=', xburgp(1),xburgp(2),xburgp(3)
	   write(*,*) 'alpha=', the_alpha
	   write(*,*) 'irot=', irot 
!	   if(irot) pause 
	endif 

	do i=1,6
           SIGMA(i)=0.d0
	   sigma2(i)=0.d0
	   sigma_bx(i)=0.d0
	   sigma_by(i)=0.d0 
	   sigma_bz(i)=0.d0 
	enddo 

!M build rotation matrix between local frame (Inf.screw setup, primed) and global frame(Yoffe's setup)
!M from local -> global: bij=cos(xi',xj); from global to local: bij'=cos(xi,xj')
            if(irot) then 
               bzp(1)=0.d0
               bzp(2)=-sin(the_alpha)
               bzp(3)=cos(the_alpha)
               bxp(1)=1.d0
               bxp(2)=0.d0
               bxp(3)=0.d0
               call rvector(bzp,bxp,byp)
               call RotMatrix(bxp,byp,bzp,b)
            endif 

	    if(irot) then 
               call rotate_vector(xburgp,xburgpp,b,1)
	       bx=xburgpp(1)
	       by=xburgpp(2)
	       bz=xburgpp(3)
	       call rotate_vector(vp,r,b,1) 
	      else
	          bx=xburgp(1)
		  by=xburgp(2)
		  bz=xburgp(3)
		  do i=1,3 
		     r(i)=vp(i)
		  enddo 
	    endif 

	   if(help.and.irot) then 
            write(*,*) 'for InfStress rotation'
            write(*,*) 'bxp=', bxp
            write(*,*) 'byp=', byp
            write(*,*) 'bzp=', bzp
            write(*,*) 'b='
	    write(*,*) 'bx,by,bz=',bx, by, bz
	    write(*,*) 'r=', r 
!	    pause 
	   endif

	   x=r(1)
	   y=r(2)
	   xy2=x**2+y**2

	   if(dabs(xy2).lt.tor) then
	      write(*,*) 'x, y, xy2=', x, y, xy2
!	      pause 'zero stress returned'  
	      return
	   endif 

!#ifdef debug 
!	   write(*,*) 'DEBUG ONLY: '
!	      sigma(1)=0.d0
!	      sigma(2)=0.d0
!	      sigma(3)=0.d0 
!	      sigma(4)=0.d0 
!	      sigma(5)=0.d0
!	      sigma(6)=10.d0*exp(-(y/20.d0-0.d0)**2)
!             sigma(6)=10.d0*exp(-(y/10.d0-0.d0)**2)
!	      sigma(6)=y
!	      sigma(6)=100.d0*x/xy2

!	      return
!#endif 

	if(dabs(bz).gt.tor) call   InfScrew(x,y,xy2,xmu,xnu,bz,sigma_bz)
	if(dabs(bx).gt.tor) call InfEdge_bx(x,y,xy2,xmu,xnu,bx,sigma_bx)
	if(dabs(by).gt.tor) call InfEdge_by(x,y,xy2,xmu,xnu,by,sigma_by)

	do i=1, 6
	   sigma2(i)=sigma_bx(i)+sigma_by(i)+sigma_bz(i)
	enddo 

	if(irot) then
	   call rotate_matrix(sigma,sigma2,b,2)
	  else
	     do i=1, 6
		sigma(i)=sigma2(i)
	     enddo 
	endif 

	return 

        END


	subroutine InfScrew(x,y,xy2,xmu,xnu,burgs,sigma)
	implicit none
	real*8 x, y, xy2, burgs, sigma(6),xmu,xnu
	real*8 pi 

	pi=3.1415927d0 

!M these few lines calc. the stress solution of a screw dislocation in an 
!  infinite medium 

	   sigma(1)= 0.d0
	   sigma(2)= 0.d0
	   sigma(3)=-xmu*burgs/2.d0/pi*y/xy2
	   sigma(4)= 0.d0
	   sigma(5)= xmu*burgs/2.d0/pi*x/xy2 
	   sigma(6)= 0.d0 
	  
	   return
	   end

	   subroutine InfEdge_bx(x,y,xy2,xmu,xnu,burgs,sigma)
	   implicit none
	   real*8 x, y, xy2, burgs, sigma(6),xmu,xnu
	   real*8 pi, d_factor 

	   pi=3.1415927d0 

!M these few lines calc. the stress solution of an edge dislocation in an 
!  infinite medium: b=bx 

	   d_factor=xnu*xmu*burgs/2.d0/pi/(1.d0-xnu)
	   sigma(1)=d_factor/xnu*(-y*(3.d0*x**2+y**2)/xy2**2)
	   sigma(2)=d_factor/xnu*x*(x**2-y**2)/xy2**2 
	   sigma(3)=0.d0
	   sigma(4)=d_factor/xnu*y*(x**2-y**2)/xy2**2
	   sigma(5)=0.d0 
	   sigma(6)=d_factor*2.d0*(-y/xy2) 
	  
	   return
	   end

	   subroutine InfEdge_by(x,y,xy2,xmu,xnu,burgs,sigma)
	   implicit none
	   real*8 x, y, xy2, burgs, sigma(6),xmu,xnu
	   real*8 pi, d_factor 

	   pi=3.1415927d0 

!M these few lines calc. the stress solution of an edge dislocation in an 
!  infinite medium: b=by
	   d_factor=xnu*xmu*burgs/2.d0/pi/(1.d0-xnu)
	   sigma(1)=d_factor/xnu*x*(x**2-y**2)/xy2**2                   
	   sigma(2)=d_factor/xnu*y*(x**2-y**2)/xy2**2 
	   sigma(3)=0.d0 
	   sigma(4)=d_factor/xnu*x*(3.d0*y**2+x**2)/xy2**2     
	   sigma(5)=0.d0  
	   sigma(6)=d_factor*2.d0*x/xy2 
	  
	   return
	   end

!******************************************************  
	subroutine norm(a)
	implicit none
	real*8 a(3),an(3),sum
	integer i
	real*8 tor

	tor=1.d-10

	sum=0.d0
	do i=1, 3
	   sum=sum+a(i)**2
	enddo 
	sum=dsqrt(sum)

	if(sum.lt.tor) return 

	do i=1, 3
	   an(i)=a(i)/sum
	enddo

	do i=1, 3
	   a(i)=an(i)
	enddo 

	return
	end
!******************************************************  
	subroutine rvector(a,b,c)
	implicit none
	real*8 a(3),b(3),c(3)
	c(1)=a(2)*b(3)-a(3)*b(2)
	c(2)=a(3)*b(1)-a(1)*b(3)
	c(3)=a(1)*b(2)-a(2)*b(1)
	return
	end

      subroutine RotMatrix(xp,yp,zp,a)
      implicit none  
      real*8 xp(3),yp(3),zp(3),a(3,3)
      integer i,j

!      aij=cos(xi',xj)

      do i=1, 3
         a(1,i)=xp(i)
         a(2,i)=yp(i)
         a(3,i)=zp(i)
      enddo 

      return 
      
      end 

