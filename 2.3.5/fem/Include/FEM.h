/***************************************************************************
 *
 *      FEM.h  interface between ParaDiS & FEM code of Meijie Tang &
 *             Guanshui Xu
 *
 *      Updated: Meijie Tang, Mar18, 2004
 *               Gregg Hommes 06/06/2008
 *
 **************************************************************************/

#ifndef _FEM_H
#define _FEM_H

#include <Home.h>

extern struct _home *home0;

/* the C names are dummy names not used. The purpose is to keep track 
 * which fortran subroutines are called by the C 
 */ 

#define FEM_Mesh_Data_Brick fem_mesh_data_brick_
#define FEM_Mesh_Data_Cylinder fem_mesh_data_cylinder_
#define FEM_Mesh_Data_Void fem_mesh_data_void_
#define FEM_Mesh_Data_Octahedron fem_mesh_data_octahedron_

#define FEM_Boundary_Node_Force_Term fem_boundary_node_force_term_
#define FEM_Analysis fem_analysis_
#define FEM_Point_Stress fem_point_stress_

#define FEM_Node_On_Surface node_on_surface_ 
#define FEM_Segment_Surface_Intersection fem_segment_surface_intersection_


#define FEM_Set_Step fem_set_step_
#define DD_Stress_on_FEM_Boundary stress_on_boundary_
#define ParadisStress paradisstress_

/*
 *      Prototype the FEM C functions
 */
void AdjustNodePosition(Home_t *home, int logLostDislocations);
void AdjustSurfaceNodeVel(Home_t *home, Node_t *node);
void AllImageStress(Home_t *home, real8 xm, real8 ym, real8 zm,
        real8 totStress[3][3], real8 totStressImg[6]);
void AllSegmentStress(Home_t *home, real8 xm, real8 ym, real8 zm,
        real8 totStress[][3]);
void AllYoffeStress(Home_t *home, real8 xm, real8 ym, real8 zm,
        real8 totStressSH[6]);
void ComputeFEMSegSigbRem(Home_t *home, int reqType);
void ComputeFEM1SegSigbRem(Home_t *home, Node_t *node, Node_t *nbr,
        int armID1, int armID2);
void FEM_Init();
void FEM_Step();
void FEM_NodePropInit(); 
void InitSegSigbRem(Home_t *home, int reqType);
void ParadisStress(real8 r[3], real8 sig_seg[6], real8 sig_node_img[6],
        real8 sig_node_yf[6]);
void YoffeSetPositionFlag(int flag);

/*
 *      Now prototypes for the FEM fortran functions
 */
void fem_analysis_();
void fem_boundary_node_force_term_();
void fem_init_(real8 *shr, real8 *youngs, real8 *pois, int *dirmax,
       int *BC_type, int *geometry_type, int *numsurfs_in,
       real8 *x1, real8 *x2, real8 *y1, real8 *y2, real8 *z1, real8 *z2);
void fem_mesh_data_brick_(real8 *fem_ax, real8 *fem_ay, real8 *fem_az,
       int *fem_nx, int *fem_ny, int *fem_nz, real8 *z1, real8 *y1,
       real8 *x1, real8 fem_ageom[3][3]);
void fem_mesh_data_octahedron_(real8 *fem_base_diagonal_half, int *fem_grid_base, 
                               int *fem_grid_vertical); 
void fem_mesh_data_cylinder_(real8 *fem_radius, real8 *fem_height,
       int *fem_nr, int *fem_nh, real8 fem_ageom[3][3]);
void fem_mesh_data_void_in_cube_adaptive_(real8 *fem_void_radius,
       real8 *fem_cube_edge_length, int *fem_numelm_arc);
void fem_point_stress_(real8 r[3], real8 sig[6]);
void fem_segment_surface_intersection_(real8 xseg1[3], real8 xseg2[3],
       real8 xinter[3], int *element_surface, int *element);
void fem_set_step_(int *step);
void node_on_surface_(int *num_surfs, int node_number[4][2],
       real8 surf_norm[4][3], real8 *x, real8 *y, real8 *z, int *rot);
void stress_on_boundary_(real8 sig_app[6]);
/* corrected function */
int sh_image_stress_num_corr(double* rr, double* rsurf, double* rmid, double* surf_norm,
                              int* isign, double* burgers, double* mu, double* nu, double* sigma);


#endif /* _FEM_H */

