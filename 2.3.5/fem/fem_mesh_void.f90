!=======================================================================
!                   fem_mesh_data_void_in_cube_adaptive
!=======================================================================
subroutine fem_mesh_data_void_in_cube_adaptive(void_radius,			&
											   cube_edge_length,	&
											   numelm_arc)
!///
!   This subroutine generates an adaptive three dimensional mesh of a cube 
!   that contains a void.
!///

	use fem_data
	use tecplot_lib

	implicit none

	real(8)		void_radius
	real(8)		cube_edge_length
	integer		numelm_arc

	integer							maxnod
	integer							maxelm
	
	integer							number_of_node_tmp
	integer							number_of_element_tmp
	integer,	allocatable	::		element_node_tmp(:,:)
	real*8,		allocatable	::		node_coordinate_tmp(:,:)

	integer							number_of_node_sub
	integer							number_of_element_sub
	integer,	allocatable	::		element_node_sub(:,:)
	real*8,		allocatable	::		node_coordinate_sub(:,:)

	real(8)		trap_a
	real(8)		trap_b
	real(8)		trap_l
	real(8)		elmsize
	real(8)		elminc
	integer		numelm_rad
	real(8)		tmp
	integer		itmp
	
	integer		i
	integer		j
	integer		k
	integer		m
	integer		n
!///
!   check input parameters
!///
	if (void_radius.gt.cube_edge_length) then
		call error_message('fem_mesh_data_void_in_cube' , 'void is bigger than cube', 6)
	endif

	if (numelm_arc.lt.1) then
		call error_message('fem_mesh_data_void_in_cube' , 'mesh size may be too coarse', 6)
	endif

	trap_a = void_radius/numelm_arc
	trap_b = cube_edge_length/numelm_arc
	trap_l  = cube_edge_length - void_radius
	call trapezoidal_distribution(trap_a,				&
								  trap_b,				&
								  trap_l,				&
								  elmsize,				&
								  elminc,				&
								  numelm_rad)
!///
!   allocate temporary node arrays
!///
	number_of_node_tmp    = 0
	number_of_element_tmp = 0

	number_of_node_sub    = (numelm_arc+1)*(numelm_arc+1)*(numelm_rad+1)
	number_of_element_sub = numelm_arc*numelm_arc*numelm_rad
	allocate(node_coordinate_sub(3,number_of_node_sub))
	allocate(element_node_sub(8,number_of_element_sub))

	maxnod  = 24*number_of_node_sub
	maxelm  = 24*number_of_element_sub

	allocate(element_node_tmp(8,maxelm))
	allocate(node_coordinate_tmp(3,maxnod))

	call one_block(element_node_sub,				&
				   node_coordinate_sub,				&
				   number_of_node_sub,				&
				   number_of_element_sub,			&
				   void_radius,						&
				   cube_edge_length,				&
				   elmsize,							&
				   elminc,							&
				   numelm_arc,						&
				   numelm_rad)

	call tecplot_fem_mesh(node_coordinate_sub,				&
						  1.d0,								&
						  element_node_sub,					&
						  'BRICK',							&
						  'block_1.dat',					&
						  fem_ltmp)


	call joint_hex_mesh(node_coordinate_tmp,		&
						node_coordinate_sub,		&
						element_node_tmp,			&
						element_node_sub,			&
						number_of_node_tmp,			&
						number_of_node_sub,			&
						number_of_element_tmp,		&
						number_of_element_sub,		&
						maxnod,			&
						maxelm,		&
						1.d-2*elmsize)

	do i = 1, number_of_node_sub
	tmp = node_coordinate_sub(1,i)
	node_coordinate_sub(1,i) = node_coordinate_sub(3,i)
	node_coordinate_sub(3,i) = tmp
	enddo

	do i = 1, number_of_element_sub
	itmp = element_node_sub(2, i)
	element_node_sub(2, i) = element_node_sub(4, i)
	element_node_sub(4, i) = itmp
	itmp = element_node_sub(6, i)
	element_node_sub(6, i) = element_node_sub(8, i)
	element_node_sub(8, i) = itmp
	enddo

	call tecplot_fem_mesh(node_coordinate_sub,				&
						  1.d0,								&
						  element_node_sub,					&
						  'BRICK',							&
						  'block_2.dat',					&
						  fem_ltmp)


	call joint_hex_mesh(node_coordinate_tmp,		&
						node_coordinate_sub,		&
						element_node_tmp,			&
						element_node_sub,			&
						number_of_node_tmp,			&
						number_of_node_sub,			&
						number_of_element_tmp,		&
						number_of_element_sub,		&
						maxnod,						&
						maxelm,						&
						1.d-2*elmsize)

	do i = 1, number_of_node_sub
	tmp = node_coordinate_sub(1,i)
	node_coordinate_sub(1,i) = node_coordinate_sub(2,i)
	node_coordinate_sub(2,i) = tmp
	enddo

	do i = 1, number_of_element_sub
	itmp = element_node_sub(2, i)
	element_node_sub(2, i) = element_node_sub(4, i)
	element_node_sub(4, i) = itmp
	itmp = element_node_sub(6, i)
	element_node_sub(6, i) = element_node_sub(8, i)
	element_node_sub(8, i) = itmp
	enddo

	call tecplot_fem_mesh(node_coordinate_sub,				&
						  1.d0,								&
						  element_node_sub,					&
						  'BRICK',							&
						  'block_3.dat',					&
						  fem_ltmp)

	call joint_hex_mesh(node_coordinate_tmp,		&
						node_coordinate_sub,		&
						element_node_tmp,			&
						element_node_sub,			&
						number_of_node_tmp,			&
						number_of_node_sub,			&
						number_of_element_tmp,		&
						number_of_element_sub,		&
						maxnod,						&
						maxelm,						&
						1.d-2*elmsize)

	call yz_reflect_brick_mesh(node_coordinate_tmp,				&
                               element_node_tmp,				&
							   maxnod,							&
                               maxelm,							&
                               number_of_node_tmp,				&
                               number_of_element_tmp,			&
						       1.d-2*elmsize,						&
						       6)

	call xz_reflect_brick_mesh(node_coordinate_tmp,				&
                               element_node_tmp,				&
							   maxnod,							&
                               maxelm,							&
                               number_of_node_tmp,				&
                               number_of_element_tmp,			&
						       1.d-2*elmsize,						&
						       6)

	call xy_reflect_brick_mesh(node_coordinate_tmp,				&
                               element_node_tmp,				&
							   maxnod,							&
                               maxelm,							&
                               number_of_node_tmp,				&
                               number_of_element_tmp,			&
						       1.d-2*elmsize,						&
						       6)

	fem_number_of_node    = number_of_node_tmp
	fem_number_of_element = number_of_element_tmp
!///
!   allocate finite element arrays
!///
    call fem_mesh_allocation
!///
!   initialize finite element arrays
!///
    call fem_mesh_initialization
!///
!   allocate finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_allocation
!///
!   initialize finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_initialization
!///
!	assign node coordinates and element connectivity
!///
	do n = 1, fem_number_of_node
	do i = 1, 3
	fem_node_coordinate(i,n) = node_coordinate_tmp(i,n)
	enddo
	enddo
	
	do n = 1, fem_number_of_element
	do i = 1, 8
	fem_element_node(i,n) = element_node_tmp(i,n)
	enddo
	enddo

	deallocate(element_node_tmp)
	deallocate(node_coordinate_tmp)

	call tecplot_fem_mesh(fem_node_coordinate,				        &
						  1.d0,								        &
						  fem_element_node,					        &
						  'BRICK',							        &
						  'element_mesh_void_adaptive_full.dat',	&
						  fem_ltmp)

	do i = 1, fem_number_of_node
	if(abs(fem_node_coordinate(3,i)).lt.1.d-2*elmsize) then
	fem_node_boundary_condition(3,i) = 0
	endif
	if(abs(fem_node_coordinate(2,i)).lt.1.d-2*elmsize) then
	fem_node_boundary_condition(2,i) = 0
	endif
	if(abs(fem_node_coordinate(1,i)).lt.1.d-2*elmsize) then
	fem_node_boundary_condition(1,i) = 0
	endif

	if(abs(fem_node_coordinate(3,i)-cube_edge_length).lt.1.d-2*elmsize) then
	fem_node_app_id(i) = 1
	endif

	enddo
!///
!   get mesh data for fem and dd 
!///
    call fem_dd_mesh_data
end subroutine fem_mesh_data_void_in_cube_adaptive
!=======================================================================
!                          fem_mesh_data_void_in_cube
!=======================================================================
subroutine fem_mesh_data_void_in_cube(void_radius,				&
									  cube_edge_length,			&
									  unit_size)
!///
!   This subroutine generates a three dimensional mesh of a cube that contains
!   a void.
!///

	use fem_data
	use tecplot_lib

	implicit none

	real(8)		void_radius
	real(8)		cube_edge_length
	real(8)		unit_size
	
	integer							tmp_number_of_node
	integer							tmp_number_of_element
	integer,	allocatable	::		tmp_element_node(:,:)
	real*8,		allocatable	::		tmp_node_coordinate(:,:)

	real*8		evoid
	real*8		ecube
	integer		nvoid
	integer		ncube
	
	integer		i
	integer		j
	integer		k
	integer		m
	integer		n
	integer		maxnod
	integer		maxelm
!///
!   check input parameters
!///
	if (void_radius.gt.0.75d0*cube_edge_length) then
		call error_message('fem_mesh_data_void_in_cube' , 'void is too big', 6)
	endif

	if (void_radius/unit_size.lt.3) then
		call error_message('fem_mesh_data_void_in_cube' , 'mesh size may be too coarse', 6)
	endif

	nvoid = void_radius/unit_size
	ncube = cube_edge_length/unit_size

	evoid = void_radius/float(nvoid)
	ecube = (cube_edge_length-void_radius)/float(ncube-nvoid)
!///
!   calculate temporaray number of nodes and elements
!///
	tmp_number_of_node    = (ncube+1)**3
	tmp_number_of_element = ncube**3
!///
!   allocate node arrays
!///
	allocate(tmp_element_node(8,tmp_number_of_element))
	allocate(tmp_node_coordinate(3,tmp_number_of_node))
!///
!   generate node coordinates
!///
	do k = 1, ncube + 1
	do j = 1, ncube + 1
	do i = 1, ncube + 1
	n = (k-1)*(ncube+1)*(ncube+1)+(j-1)*(ncube+1)+i
!///
!   compute node coordinate
!///
	if(k.le.(nvoid+1)) then
	tmp_node_coordinate(3,n) = evoid*float(k-1)
	else
	tmp_node_coordinate(3,n) = evoid*float(nvoid)+ecube*float(k-nvoid-1)
	endif

	if(j.le.(nvoid+1)) then
	tmp_node_coordinate(2,n) = evoid*float(j-1)
	else
	tmp_node_coordinate(2,n) = evoid*float(nvoid)+ecube*float(j-nvoid-1)
	endif

	if(i.le.(nvoid+1)) then
	tmp_node_coordinate(1,n) = evoid*float(i-1)
	else
	tmp_node_coordinate(1,n) = evoid*float(nvoid)+ecube*float(i-nvoid-1)
	endif

	enddo
	enddo
	enddo
!///
!   generate element connectivity
!///
	do k = 1, ncube
	do j = 1, ncube
	do i = 1, ncube
!///
!	connectivity, i.e., relation of local nodes and global nodes
!///
	n = (k-1)*ncube*ncube+(j-1)*ncube+i
	m = (k-1)*(ncube+1)*(ncube+1)+(j-1)*(ncube+1)+i
	tmp_element_node(1,n) = m
	tmp_element_node(2,n) = m + (ncube+1)*(ncube+1)
	tmp_element_node(3,n) = m + (ncube+1)*(ncube+1) + 1
	tmp_element_node(4,n) = m + 1
	tmp_element_node(5,n) = m + (ncube+1)
	tmp_element_node(6,n) = m + (ncube+1) + (ncube+1)*(ncube+1)
	tmp_element_node(7,n) = m + (ncube+1) + (ncube+1)*(ncube+1) + 1
	tmp_element_node(8,n) = m + (ncube+1) + 1
	enddo
	enddo
	enddo
!///
!   remove_void_elements
!///
	call remove_void_element(tmp_number_of_node,			&
							 tmp_number_of_element,			&
							 tmp_element_node,				&
							 tmp_node_coordinate,			&
							 void_radius)
!///
!   allocate finite element arrays
!///
	fem_number_of_node    = tmp_number_of_node
	fem_number_of_element = tmp_number_of_element
	allocate(fem_element_node(8,fem_number_of_element))
	allocate(fem_node_boundary_condition(3,fem_number_of_node))
	allocate(fem_node_coordinate(3,fem_number_of_node))
	allocate(fem_node_displacement(3,fem_number_of_node))
	allocate(fem_node_force(3,fem_number_of_node))
	allocate(fem_element_stress(6,1,fem_number_of_element))
	allocate(fem_gauss_stress(6,8,fem_number_of_element))
	allocate(fem_node_stress(6,fem_number_of_node))
!///
!   assign element and node array
!///
	do n = 1, fem_number_of_node
	do i = 1, 3
	fem_node_coordinate(i,n) = tmp_node_coordinate(i,n)
	enddo
	enddo
	
	do n = 1, fem_number_of_element
	do i = 1, 8
	fem_element_node(i,n) = tmp_element_node(i,n)
	enddo
	enddo
!///
!   transform the cubic void to spherical void
!///
	call transform_void_from_cube_to_sphere(void_radius,		&
	                                        cube_edge_length)
!///
!
!///
	call tecplot_fem_mesh(fem_node_coordinate,				&
						  1.d0,								&
						  fem_element_node,					&
						  'BRICK',							&
						  'elment_mesh_void_eighth.dat',	&
						  fem_ltmp)

	deallocate(tmp_element_node)
	deallocate(tmp_node_coordinate)
	maxnod = 8*fem_number_of_node
	maxelm = 8*fem_number_of_element
	allocate(tmp_element_node(8,maxelm))
	allocate(tmp_node_coordinate(3,maxnod))

	tmp_number_of_node    = fem_number_of_node
	tmp_number_of_element = fem_number_of_element

	do n = 1, tmp_number_of_node
	do i = 1, 3
	tmp_node_coordinate(i,n) = fem_node_coordinate(i,n)
	enddo
	enddo
	
	do n = 1, fem_number_of_element
	do i = 1, 8
	tmp_element_node(i,n) = fem_element_node(i,n)
	enddo
	enddo

	call yz_reflect_brick_mesh(tmp_node_coordinate,				&
                               tmp_element_node,				&
							   maxnod,							&
                               maxelm,							&
                               tmp_number_of_node,				&
                               tmp_number_of_element,			&
						       unit_size,						&
						       6)

	call xz_reflect_brick_mesh(tmp_node_coordinate,				&
                               tmp_element_node,				&
							   maxnod,							&
                               maxelm,							&
                               tmp_number_of_node,				&
                               tmp_number_of_element,			&
						       unit_size,						&
						       6)



	call xy_reflect_brick_mesh(tmp_node_coordinate,				&
                               tmp_element_node,				&
							   maxnod,							&
                               maxelm,							&
                               tmp_number_of_node,				&
                               tmp_number_of_element,			&
						       unit_size,						&
						       6)

	deallocate(fem_element_node)
	deallocate(fem_node_boundary_condition)
	deallocate(fem_node_coordinate)
	deallocate(fem_node_displacement)
	deallocate(fem_node_force)
	deallocate(fem_element_stress)
	deallocate(fem_gauss_stress)
	deallocate(fem_node_stress)

	fem_number_of_node    = tmp_number_of_node
	fem_number_of_element = tmp_number_of_element
	allocate(fem_element_node(8,fem_number_of_element))
	allocate(fem_node_boundary_condition(3,fem_number_of_node))
	allocate(fem_node_coordinate(3,fem_number_of_node))
	allocate(fem_node_displacement(3,fem_number_of_node))
	allocate(fem_node_force(3,fem_number_of_node))
	allocate(fem_element_stress(6,1,fem_number_of_element))
	allocate(fem_gauss_stress(6,8,fem_number_of_element))
	allocate(fem_node_stress(6,fem_number_of_node))

	do n = 1, fem_number_of_node
	do i = 1, 3
	fem_node_coordinate(i,n) = tmp_node_coordinate(i,n)
	enddo
	enddo
	
	do n = 1, fem_number_of_element
	do i = 1, 8
	fem_element_node(i,n) = tmp_element_node(i,n)
	enddo
	enddo

	deallocate(tmp_element_node)
	deallocate(tmp_node_coordinate)

	call tecplot_fem_mesh(fem_node_coordinate,				&
						  1.d0,								&
						  fem_element_node,					&
						  'BRICK',							&
						  'element_mesh_void_full.dat',				&
						  fem_ltmp)


contains
!!=======================================================================
!!                         remove_void_element
!!=======================================================================
		subroutine remove_void_element(numnod,			&
									   numelm,			&
									   ix,				&
									   x,				&
									   void_radius)
		implicit none

		integer		numnod
		integer		numelm
		integer		ix(8,numelm)
		real*8		x(3,numnod)
		real*8		void_radius

		integer		numnodsub
		integer		numelmsub
		integer		n
		integer		i

		real*8,		allocatable	::	xsub(:,:)
		integer,	allocatable	::	ixsub(:,:)
		integer,	allocatable	::	mapnod(:)
		integer,	allocatable	::	mapelm(:)

		real*8		xc(3)


		numelmsub = 0
		do n = 1, numelm
		do i = 1,3
		xc(i) = (   x(i,ix(1,n))			&
		          + x(i,ix(2,n))			&
			      + x(i,ix(3,n))			&
				  + x(i,ix(4,n))			&
				  + x(i,ix(5,n))			&
			      + x(i,ix(6,n))			&
		          + x(i,ix(7,n))			&
		          + x(i,ix(8,n)))/8.d0
		enddo
		if(xc(1).gt.void_radius.or.			&
		   xc(2).gt.void_radius.or.			&
		   xc(3).gt.void_radius) then
		numelmsub = numelmsub + 1
		endif
		enddo

		numnodsub = 0
		do n = 1, numnod
		if(x(1,n).ge.0.999d0*void_radius.or.			&
		   x(2,n).ge.0.999d0*void_radius.or.			&
		   x(3,n).ge.0.999d0*void_radius) then
		numnodsub = numnodsub + 1
		endif
		enddo 

		allocate(xsub(3,numnodsub))
		allocate(ixsub(8,numelmsub))
		allocate(mapnod(numnod))

		numnodsub = 0
		do n = 1, numnod
		if(x(1,n).ge.0.999d0*void_radius.or.			&
		   x(2,n).ge.0.999d0*void_radius.or.			&
		   x(3,n).ge.0.999d0*void_radius) then
		numnodsub = numnodsub + 1
		mapnod(n) = numnodsub
		do i = 1, 3
		xsub(i,numnodsub) = x(i,n)
		enddo
		endif
		enddo 

		numelmsub = 0
		do n = 1, numelm
		do i = 1,3
		xc(i) = (   x(i,ix(1,n))			&
		          + x(i,ix(2,n))			&
		          + x(i,ix(3,n))			&
		          + x(i,ix(4,n))			&
		          + x(i,ix(5,n))			&
		          + x(i,ix(6,n))			&
		          + x(i,ix(7,n))			&
		          + x(i,ix(8,n)))/8.d0
		enddo
		if(xc(1).gt.void_radius.or.			&
		   xc(2).gt.void_radius.or.			&
		   xc(3).gt.void_radius) then
		numelmsub = numelmsub + 1
		do i = 1, 8
		ixsub(i,numelmsub) = mapnod(ix(i,n))
		enddo
		endif
		enddo

		numnod = numnodsub
		numelm = numelmsub

		do n = 1, numnod
		do i = 1, 3
		x(i,n) = xsub(i,n)
		enddo
		enddo

		do n = 1, numelm
		do i = 1, 8
		ix(i,n) = ixsub(i,n)
		enddo
		enddo

		deallocate(xsub)
		deallocate(ixsub)
		deallocate(mapnod)

	end	subroutine remove_void_element
end subroutine fem_mesh_data_void_in_cube
!=======================================================================
!                     transform_void_from_cube_to sphere
!=======================================================================
subroutine transform_void_from_cube_to_sphere(void_radius,		&
	                                          cube_edge_length)
	use fem_data

	implicit none

	real*8		void_radius
	real*8		cube_edge_length
	real*8		rco
	
	integer     n
!///
!   assign boundary condition
!///
	fem_node_boundary_condition = 1
	fem_node_displacement       = 0.d0

	do n = 1, fem_number_of_node

	    if(fem_node_coordinate(1,n).lt.1.001*void_radius.and.		&
		fem_node_coordinate(2,n).lt.1.001*void_radius.and.		&
		fem_node_coordinate(3,n).lt.1.001*void_radius) then
		fem_node_boundary_condition(1,n) =-1
		fem_node_boundary_condition(2,n) =-1
		fem_node_boundary_condition(3,n) =-1
		rco = sqrt(fem_node_coordinate(1,n)**2+fem_node_coordinate(2,n)**2+fem_node_coordinate(3,n)**2)
	    fem_node_displacement(1,n) = (void_radius-rco)*fem_node_coordinate(1,n)/rco
		fem_node_displacement(2,n) = (void_radius-rco)*fem_node_coordinate(2,n)/rco
		fem_node_displacement(3,n) = (void_radius-rco)*fem_node_coordinate(3,n)/rco
		endif

		if(fem_node_coordinate(1,n).lt.0.001*void_radius) then
	    fem_node_boundary_condition(1,n) = 0
		endif
			
		if(fem_node_coordinate(2,n).lt.0.001*void_radius) then
	    fem_node_boundary_condition(2,n) = 0
		endif

		if(fem_node_coordinate(3,n).lt.0.001*void_radius) then
		fem_node_boundary_condition(3,n) = 0
		endif

		if(fem_node_coordinate(1,n).gt.0.999*cube_edge_length) then
		fem_node_boundary_condition(1,n) = 0
		fem_node_boundary_condition(2,n) = 0
		fem_node_boundary_condition(3,n) = 0
		endif
			
		if(fem_node_coordinate(2,n).gt.0.999*cube_edge_length) then
		fem_node_boundary_condition(1,n) = 0
		fem_node_boundary_condition(2,n) = 0
		fem_node_boundary_condition(3,n) = 0
		endif

		if(fem_node_coordinate(3,n).gt.0.999*cube_edge_length) then
		fem_node_boundary_condition(1,n) = 0
		fem_node_boundary_condition(2,n) = 0
		fem_node_boundary_condition(3,n) = 0
		endif

	enddo
!///
!   assign node force
!///
	fem_node_force = 0.d0
!///
!   compute nodal displacement using finite element analysis 
!///
	call fem_elastic_8n_3d_pcg(fem_number_of_node,					&
							   fem_number_of_element,				&	
						       fem_element_node,					&
							   fem_node_coordinate,					&
							   fem_node_displacement,				&
							   fem_node_force,						&
							   fem_node_boundary_condition,			&
							   fem_elastic_constant,				&
							   fem_young_modulus,					&
							   fem_poisson_ratio)
!///
!   update nodal coordinates
!///
	fem_node_coordinate = fem_node_coordinate + fem_node_displacement

end subroutine transform_void_from_cube_to_sphere
