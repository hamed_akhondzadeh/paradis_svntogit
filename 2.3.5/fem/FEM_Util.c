/***************************************************************************
 *
 *  Module      : FEMUtil.c
 *  Description : Connection with FEM (F90) functions  
 *  Updated     : Meijie Tang, Mar18, 2004
 *
 **************************************************************************/

#include "FEM.h"
#include "Node.h"
#include "InData.h"
#include "Home.h"
#include "QueueOps.h"
#include <math.h> 

/***************************************************************************
 *
 *      Function:    ParadisStress
 *      Description: 
 *
 *      NOTE: Only sig_node_img is used to solve for the FEM image stress; 
 *      sig_node_yf and sig_seg are for debugging purpose only 
 *
 **************************************************************************/
void ParadisStress(double r[3], double sig_seg[6], double sig_node_img[6],
                   double sig_node_yf[6])
{
        double stress[3][3];
        double stress_img[6]; 

/*
 *      AllSegmentStress is not directly used by DDFEM. The disl-disl.
 *      interactions are handled by the standard ParaDiS part of the code
 */

        AllSegmentStress(home0, r[0], r[1], r[2], stress); 

        sig_seg[0] = stress[0][0];
        sig_seg[1] = stress[1][1];
        sig_seg[2] = stress[2][2];
        sig_seg[3] = stress[0][1];
        sig_seg[4] = stress[1][2];
        sig_seg[5] = stress[2][0];

        AllImageStress(home0, r[0], r[1], r[2],
                       stress, stress_img);

        sig_node_img[0] = stress_img[0] + stress[0][0];
        sig_node_img[1] = stress_img[3] + stress[1][1];
        sig_node_img[2] = stress_img[5] + stress[2][2];
        sig_node_img[3] = stress_img[1] + stress[0][1];
        sig_node_img[4] = stress_img[4] + stress[1][2];
        sig_node_img[5] = stress_img[2] + stress[2][0];

/*
 *      Test Yoffe image stress at fem nodes, below is only used for debugging
 */
#if 0
        AllYoffeStress(home0, r[0], r[1], r[2], stress_img);

        sig_node_yf[0] = stress_img[0];
        sig_node_yf[1] = stress_img[3];
        sig_node_yf[2] = stress_img[5];
        sig_node_yf[3] = stress_img[1];
        sig_node_yf[4] = stress_img[4];
        sig_node_yf[5] = stress_img[2];
#endif

        return;
}


void FEM_Init()
{
        real8 fem_ax, fem_ay, fem_az;
        real8 youngs, pois, shr;
        int dirmax;
        int fem_nx, fem_ny, fem_nz;
	int geometry_type; 
	int BC_type; 
	real8 x1,x2,y1,y2,z1,z2;
	int numsurfs_in; 

	real8 fem_radius,fem_height; 
	int fem_nr, fem_nh; 

	real8 fem_void_radius,fem_cube_edge_length; 
	int fem_numelm_arc; 

	real8 fem_base_diagonal_half;
	int fem_grid_base, fem_grid_vertical;

        real8 fem_ageom[3][3],xp[3],yp[3],zp[3];

/*
 *      ageom matrix defines the coordinate rotation defined by the FEM
 *      box in the ParaDiS box
 */
        zp[0] = home0->param->fem_ageom_z[0];
        zp[1] = home0->param->fem_ageom_z[1];
        zp[2] = home0->param->fem_ageom_z[2];

        Normalize(&zp[0], &zp[1], &zp[2]);

        xp[0] = home0->param->fem_ageom_x[0];
        xp[1] = home0->param->fem_ageom_x[1];
        xp[2] = home0->param->fem_ageom_x[2];

        Normalize(&xp[0], &xp[1], &xp[2]);

        NormalizedCrossVector(zp, xp, yp);

        fem_ageom[0][0] = xp[0];
        fem_ageom[0][1] = xp[1];
        fem_ageom[0][2] = xp[2];

        fem_ageom[1][0] = yp[0];
        fem_ageom[1][1] = yp[1];
        fem_ageom[1][2] = yp[2];

        fem_ageom[2][0] = zp[0];
        fem_ageom[2][1] = zp[1];
        fem_ageom[2][2] = zp[2];

        shr = home0->param->shearModulus;
        pois = home0->param->pois;
	youngs = 2.0 * shr * (1.0 + pois);

#if 0
	printf("shr= %f, pois = %f, youngs = %f \n", shr, pois, youngs);
#endif

        dirmax = home0->param->dirmax;

/*
 *      BC_type = 1: only one free surface at z=zBoundMax
 *      BC_type = 4: top and bottom along z are free surfaces
 *      BC_type = 5: all six surfaces are free
 */
	BC_type = home0->param->BC_type; 
	geometry_type = home0->param->mesh_type; 

/*
 *      Input for number of free surface
 */ 
        if (BC_type==1)  numsurfs_in = 1;
        if (BC_type==4)  numsurfs_in = 2;
        if (BC_type==5)  numsurfs_in = 6;

        fem_init_(&shr, &youngs, &pois, &dirmax, &BC_type, &geometry_type,
                  &numsurfs_in, &x1, &x2, &y1, &y2, &z1, &z2);

#ifdef _FEM

/*
 *      This needs to be put into repo eventually. May 10, 2006
 *          geometry_type = 1: rectangular
 *          geometry_type = 2: cylinder
 *          geometry_type = 3: void
 *          geometry_type = 4: octahedron
 *          geometry_type = 5: Y-shape 
 */ 
	if (geometry_type == 1) { 

            x1 = home0->param->xBoundMin;
            x2 = home0->param->xBoundMax;
            y1 = home0->param->yBoundMin;
            y2 = home0->param->yBoundMax;
            z1 = home0->param->zBoundMin;
            z2 = home0->param->zBoundMax;

            fem_ax= x2 - x1;
            fem_ay= y2 - y1;
            fem_az= z2 - z1;

            fem_nx = home0->param->fem_nx;
            fem_ny = home0->param->fem_ny;
            fem_nz = home0->param->fem_nz;

/*
 *         Meijie Tang: make sure to shift fem coordinates to be 
 *         consistent with the dd box
 */ 
/*
            printf("FEM_Mesh_Data_Brick next \n");
 */ 
            fem_mesh_data_brick_(&fem_ax, &fem_ay, &fem_az, &fem_nx,
                                 &fem_ny, &fem_nz, &z1, &y1, &x1, fem_ageom);
	}

       	if (geometry_type == 2) {
	  
            fem_radius = home0->param->fem_radius;
            fem_height = home0->param->fem_height;
            fem_nr = home0->param->fem_nr;
            fem_nh = home0->param->fem_nh;

            fem_mesh_data_cylinder_(&fem_radius, &fem_height,
                                    &fem_nr, &fem_nh, fem_ageom); 
	}

	if (geometry_type == 3) {
            fem_void_radius = home0->param->fem_void_radius; 
            fem_cube_edge_length = home0->param->fem_cube_edge_length; 
            fem_numelm_arc =home0->param->fem_numelm_arc; 
            fem_mesh_data_void_in_cube_adaptive_(&fem_void_radius,
                                                 &fem_cube_edge_length,
                                                 &fem_numelm_arc); 
        }

	if (geometry_type == 4) {
            fem_base_diagonal_half = home0->param->fem_base_diagonal_half; 
	    fem_grid_base = home0->param->fem_grid_base; 
	    fem_grid_vertical = home0->param->fem_grid_vertical; 
            fem_mesh_data_octahedron_(&fem_base_diagonal_half, &fem_grid_base, &fem_grid_vertical); 
        }

#endif
        return;
}


void FEM_Step()
{
        double sig_app[6];
        double xp[3], img_stress[6];
        int i;

/*
 *      Should use value from ParaDiS
 */
        sig_app[0] = home0->param->appliedStress[0];
        sig_app[1] = home0->param->appliedStress[1];
        sig_app[2] = home0->param->appliedStress[2];
        sig_app[4] = home0->param->appliedStress[3];
        sig_app[5] = home0->param->appliedStress[4];
        sig_app[3] = home0->param->appliedStress[5];

/*
 *      For the moment, sig_app is not used through the FEM routines. 
 *      Instead, the forces due to applied loading is calculated in 
 *      ExtPKForce inside NodeForce.c and directly applied to each 
 *      dislocation node
 */ 
        stress_on_boundary_(sig_app);
        printf("FEM Image BC calculated\n");

        fem_boundary_node_force_term_();
        printf("FEM node force calculated\n");

        fem_analysis_();
        printf("FEM solved\n");

        return;
}
