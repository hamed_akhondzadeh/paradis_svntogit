!=======================================================================
!                        fem_boundary_node_force_term
! Notation for stress components: 
! 1=xx, 2=yy, 3=zz, 4=xy, 5=yz, 6=xz            M. Tang, 9/12/2005 
!=======================================================================
subroutine fem_boundary_node_force_term
	use fem_data
	use fem_lib
!///
!   This subroutine is used to calculate the node "force term", which
!   is required to calculate the node displacemnt. 
!///	
	implicit none

	integer,	parameter ::	nqd  = 4
	integer,	parameter ::	ndim = 2
	integer,	parameter ::	nod  = 4

	real*8		points(nqd,2)
	real*8		weights(nqd)
	real*8		xs(3,nod)
	real*8		coord(nod,ndim)
	real*8		xvec(3,3)
	real*8		fn(3,nod)
	real*8		fq(3)
	real*8		fun(nod)
	real*8		der(ndim,nod)
	real*8		jac(ndim,ndim)
	real*8		det

	integer		ne
	integer		ns
	integer		n
	integer		i
	integer		j
	integer		k

!///
!   initialize node force
!///
	fem_node_force = 0.d0
!///
!   obtain standard gauss point and weight 
!///
	call sample('quadrilateral',points,weights)

!///
!   run surface of element loop to obtain the node 'force term'
!///
	do ne = 1, fem_number_of_element
	do ns = 1, 6
!///
! MT 6/6/2003
! Initialize 
!///
        fn = 0.d0 

!///
!   compute image stress on the surface with image stress
!///
	if(fem_element_surface_img_id(ns,ne).eq.1.or.		&
	   fem_element_surface_app_id(ns,ne).eq.1) then
!///
!   coordinates of surface nodes
!///
	do n  = 1, nod
	xs(1,n) = fem_node_coordinate(1,fem_element_surface_node(n,ns,ne))
	xs(2,n) = fem_node_coordinate(2,fem_element_surface_node(n,ns,ne))
	xs(3,n) = fem_node_coordinate(3,fem_element_surface_node(n,ns,ne))
	enddo
!///
!   surface coordinate system
!///
	call surface_coordinate_system(xvec,			&
								   xs(1,1),			&
								   xs(1,4),			&
								   xs(1,3))
!///
!   node image stress traction
!///
	if(fem_element_surface_img_id(ns,ne).eq.1) then
	do n = 1, nod
	fn(1,n) = fem_node_img_stress(1,fem_element_surface_node(n,ns,ne))*xvec(1,3)		&
			 +fem_node_img_stress(4,fem_element_surface_node(n,ns,ne))*xvec(2,3)		&
			 +fem_node_img_stress(6,fem_element_surface_node(n,ns,ne))*xvec(3,3)
	fn(2,n) = fem_node_img_stress(4,fem_element_surface_node(n,ns,ne))*xvec(1,3)		&
			 +fem_node_img_stress(2,fem_element_surface_node(n,ns,ne))*xvec(2,3)		&
			 +fem_node_img_stress(5,fem_element_surface_node(n,ns,ne))*xvec(3,3)		
	fn(3,n) = fem_node_img_stress(6,fem_element_surface_node(n,ns,ne))*xvec(1,3)		&
			 +fem_node_img_stress(5,fem_element_surface_node(n,ns,ne))*xvec(2,3)		&
			 +fem_node_img_stress(3,fem_element_surface_node(n,ns,ne))*xvec(3,3)		
	enddo
        
	endif
!///
!   add node applied stress traction
!///
	if(fem_element_surface_app_id(ns,ne).eq.1) then
	do n = 1, nod
	fn(1,n) = fn(1,n)																	&
			 +fem_node_app_stress(1,fem_element_surface_node(n,ns,ne))*xvec(1,3)		&
			 +fem_node_app_stress(4,fem_element_surface_node(n,ns,ne))*xvec(2,3)		&
			 +fem_node_app_stress(6,fem_element_surface_node(n,ns,ne))*xvec(3,3)
	fn(2,n) = fn(2,n)																	&
			 +fem_node_app_stress(4,fem_element_surface_node(n,ns,ne))*xvec(1,3)		&
			 +fem_node_app_stress(2,fem_element_surface_node(n,ns,ne))*xvec(2,3)		&
			 +fem_node_app_stress(5,fem_element_surface_node(n,ns,ne))*xvec(3,3)		
	fn(3,n) = fn(3,n) 																	&
			 +fem_node_app_stress(6,fem_element_surface_node(n,ns,ne))*xvec(1,3)		&
			 +fem_node_app_stress(5,fem_element_surface_node(n,ns,ne))*xvec(2,3)		&
			 +fem_node_app_stress(3,fem_element_surface_node(n,ns,ne))*xvec(3,3)	
	enddo
	endif
!///
!   coordinates of surface nodes in the surface coodinate system
!///
	do i = 1, 2
	do n = 1, nod
    coord(n,i) = 0.
    do j = 1, 3
    coord(n,i) = coord(n,i) + xvec(j,i)*xs(j,n)
    enddo
    enddo
    enddo
!///
!   integrate to obtain the surface node 'force term'
!///
	do k = 1, nqd
	call shape_fun(fun,points,k)
	call shape_der(der,points,k)
	jac  = matmul(der,coord)
	det  = determinant(jac)

!cM note: fn is the traction (force per unit area) 
!         fq is the total traction 
	fq   = matmul(fn,fun)
	do i = 1, 3
	do n = 1, nod
	fem_node_force(i,fem_element_surface_node(n,ns,ne)) =		&
		fem_node_force(i,fem_element_surface_node(n,ns,ne)) + det*weights(k)*fq(i)*fun(n)
	enddo
	enddo
	enddo
	endif
	enddo
	enddo

!	write(fem_llog,'('' n force'',i5,3e15.5)') (n,(fem_node_force(i,n),i=1,3),n=1,fem_number_of_node)

end subroutine fem_boundary_node_force_term
!=======================================================================
!                                 fem_analysis
!=======================================================================
subroutine fem_analysis
	use fem_data
        use tecplot_lib 

        logical help 

        help = .true. 

        fem_number_of_gauss_point = 1
!        fem_number_of_gauss_point = 8 


!///
!   This subroutine computes displacement and stress using finite element analysis
!///
!///
!   solve node displacement using the finite element analysis
!///
	if(fem_number_of_element.le.fem_dir_numelm_max) then
	call fem_elastic_8n_3d_dir(fem_number_of_node,					&
							   fem_number_of_element,				&	
						       fem_element_node,					&
							   fem_node_coordinate,					&
							   fem_node_displacement,				&
							   fem_node_force,						&
							   fem_node_boundary_condition,			&
							   fem_elastic_constant,				&
							   fem_young_modulus,					&
							   fem_poisson_ratio,                                   &
                                                           fem_ltmp,                                            & 
                                                           fem_istep)
	else
	call fem_elastic_8n_3d_pcg(fem_number_of_node,					&
							   fem_number_of_element,				&	
						       fem_element_node,			&
							   fem_node_coordinate,					&
							   fem_node_displacement,				&
							   fem_node_force,						&
							   fem_node_boundary_condition,			&
							   fem_elastic_constant,				&
							   fem_young_modulus,					&
							   fem_poisson_ratio)
	endif
!///
!   calculate stress based on displacment
!///
        if(fem_number_of_gauss_point.eq.1) then 
           call fem_iso_elastic_stress1(fem_number_of_node,				&
                fem_number_of_element,				&
                fem_element_node,					&
                fem_node_coordinate,				&
                fem_node_displacement,				&
                fem_element_stress,				&
                fem_young_modulus,					&
                fem_poisson_ratio)
!///
!   obtain node stress based on the average of element stress
!///
           call stress_element_to_node1(fem_number_of_node,			&
								fem_number_of_element,		&
								fem_element_node,			&
								fem_element_stress,			&
								fem_node_stress)

        endif

        if(fem_number_of_gauss_point.eq.8) then 
           call fem_iso_elastic_stress8(fem_number_of_node,				&
								 fem_number_of_element,				&
		 						 fem_element_node,					&
								 fem_node_coordinate,				&
								 fem_node_displacement,				&
								 fem_gauss_stress,				&
                                                                 fem_gauss_coordinate,                          &
								 fem_young_modulus,					&
								 fem_poisson_ratio)
!///
!   obtain node stress based on the average of element stress
!///
           call stress_element_to_node8(fem_number_of_node,			&
								fem_number_of_element,		&
								fem_element_node,			&
								fem_gauss_stress,			&
                                                                fem_gauss_coordinate,                   &
								fem_node_stress)
        endif

        if(help) then 
           call fem_tecplot
           write(*,*) 'fem_tecplot is called' 
!           pause 
        endif

end subroutine fem_analysis

!=======================================================================
!                           nearest_gauss_point
! Find the nearest gauss point within the same element. 
!=======================================================================
subroutine nearest_gauss_point(xp, ne, ng)

	use fem_data

	real*8			xp(3)
	integer			ne, ng 
	integer			i, j 
        real*8                  dr, drmin

        drmin = 0.d0
        do i = 1, 8
           dr = 0.d0
           do j = 1, 3
              dr = dr + (xp(j)-fem_gauss_coordinate(j,i,ne))**2
           enddo
           dr = dsqrt(dr)
           if(i.eq.1) then
              drmin = dr
              ng = i 
           else
              if(dr.lt.drmin) then 
                 drmin = dr
                 ng = i
              endif
           endif
        enddo
        
        if(ng.eq.0) stop 'Nearest Gauss Point not Found!' 

        return 

end subroutine nearest_gauss_point
!=======================================================================
!                               fem_tecplot
!=======================================================================
subroutine fem_tecplot

	use fem_data
	use tecplot_lib

	implicit none

	real*8		length_scale
	real*8		displacement_scale
	real*8		stress_scale

        integer         n, i 

        logical         plot_node_seg_stress,plot_node_ana_stress,plot_node_app_stress,&
                        plot_node_fem_stress,plot_node_img_stress,plot_node_disp, &
                        plot_node_anafem_stress,plot_node_tot_stress,plot_node_anaseg_stress,&
                        plot_node_femseg_stress

        logical         help

        help = .true. 

        if(help) then 
           plot_node_anaseg_stress=.true.
           plot_node_femseg_stress=.true.
           plot_node_anafem_stress=.true.
           plot_node_seg_stress=.true.
           plot_node_img_stress=.true.
           plot_node_ana_stress=.true.
           plot_node_app_stress=.true.
           plot_node_disp=.true. 
           plot_node_fem_stress=.true.
           plot_node_tot_stress=.true.
        else
           plot_node_anaseg_stress=.false.
           plot_node_femseg_stress=.false.
           plot_node_anafem_stress=.false.
           plot_node_seg_stress=.false.
           plot_node_img_stress=.false.
           plot_node_ana_stress=.false.
           plot_node_app_stress=.false.
           plot_node_disp=.false.
           plot_node_fem_stress=.false.
           plot_node_tot_stress=.false.
        endif
          

!cM 
!        fem_ltmp                = 61 
	length_scale		= 1.d0
	displacement_scale	= 1.d0
	stress_scale		= 1.d0
	

        if(plot_node_disp) then 
           call tecplot_fem_field(fem_node_coordinate,					&
						   length_scale,						&
						   fem_node_displacement,				&
						   displacement_scale,					&
						   fem_element_node,					&
						   fem_istep,							&
						   'displacement',						&
						   'BRICK',								&
						   'femfld_displacement.dat',			&
						   fem_ltmp)
          write(*,*) 'femfld_displacement plotted' 	
        endif

        if(plot_node_fem_stress) then 
           call tecplot_fem_field(fem_node_coordinate,					&
						   length_scale,						&
						   fem_node_stress,						&
						   stress_scale,						&
						   fem_element_node,					&
						   fem_istep,							&
						   'stress',							&
						   'BRICK',								&
						   'femfld_FEMstress.dat',					&
						   fem_ltmp)
          write(*,*) 'femfld_FEMstress plotted' 	
        endif

        if(plot_node_anafem_stress) then 
           call tecplot_fem_field(fem_node_coordinate,					&
						   length_scale,						&
						   fem_node_stress+fem_node_ana_stress,						&
						   stress_scale,						&
						   fem_element_node,					&
						   fem_istep,							&
						   'stress',							&
						   'BRICK',								&
						   'femfld_YF+FEMstress.dat',					&
						   fem_ltmp)	
          write(*,*) 'femfld_YF+FEMstress plotted' 
        endif


        if(plot_node_femseg_stress) then 
           call tecplot_fem_field(fem_node_coordinate,					&
						   length_scale,						&
						   fem_node_seg_stress+fem_node_stress,			&
						   stress_scale,						&
						   fem_element_node,					&
						   fem_istep,							&
						   'stress',							&
						   'BRICK',								&
						   'femfld_INF+FEMstress.dat',					&
						   fem_ltmp)
          write(*,*) 'femfld_INF+FEMstress plotted' 	
        endif

        if(plot_node_tot_stress) then 
           call tecplot_fem_field(fem_node_coordinate,					&
						   length_scale,						&
						   fem_node_stress+fem_node_ana_stress+fem_node_seg_stress,		&
						   stress_scale,						&
						   fem_element_node,					&
						   fem_istep,							&
						   'stress',							&
						   'BRICK',								&
						   'femfld_total_stress.dat',					&
						   fem_ltmp)	
          write(*,*) 'femfld_total_stress plotted' 
        endif

        if(plot_node_seg_stress) then
           call tecplot_fem_field(fem_node_coordinate,	&		
               1.d0,					&
               fem_node_seg_stress,			&	
               1.d0,			                &
               fem_element_node,			&	
               fem_istep,				&		
               'stress',				&		
               'BRICK',					&		
               'femfld_INFstress.dat',		&		
               61)	
           
           write(*,*) 'femfld_INFstress plotted' 
        endif
           
        if(plot_node_ana_stress) then
           call tecplot_fem_field(fem_node_coordinate,	&		
               1.d0,					&
               fem_node_ana_stress,			&	
               1.d0,			                &
               fem_element_node,			&	
               fem_istep,				&		
               'stress',				&		
               'BRICK',					&		
               'femfld_YFstress.dat',		&		
               61)	      
           write(*,*) 'femfld_YFstress plotted'  
        endif

        if(plot_node_anaseg_stress) then
           call tecplot_fem_field(fem_node_coordinate,	&		
               1.d0,					&
               fem_node_ana_stress+fem_node_seg_stress,	&			
               1.d0,		                        &
               fem_element_node,			&	
               fem_istep,				&		
               'stress',				&		
               'BRICK',					&		
               'femfld_INF+YFstress.dat',	&			
               61)	      
           write(*,*) 'femfld_INF+YFstress plotted'  
        endif
           
        if(plot_node_img_stress) then
           call tecplot_fem_field(fem_node_coordinate,	&	
               1.d0,					&
               fem_node_img_stress,			&	
               1.d0,			                &
               fem_element_node,			&	
               fem_istep,				&		
               'stress',				&		
               'BRICK',					&		
               'femfld_BCstress.dat',		&		
               fem_ltmp)	
           
           write(*,*) 'femfld_BCstress plotted' 
        endif
           
        if(plot_node_app_stress) then
           call tecplot_fem_field(fem_node_coordinate,	&	
               1.d0,					&
               fem_node_app_stress,			&	
               1.d0,		                        &
               fem_element_node,			&	
               fem_istep,				&		
               'stress',				&		
               'BRICK',					&		
               'femfld_node_app_stress.dat',		&		
               fem_ltmp)	
           
           write(*,*) 'femfld_node_app_stress plotted' 
        endif

        write(*,*) '#############CHECK femfld data#############'

end subroutine fem_tecplot

!=======================================================================
!                         stress_element_to_node1
!=======================================================================
subroutine stress_element_to_node1(numnod,					&
								  numelm,					&	
								  ie,						&
								  stress_element,			&
								  stress_node)
!///
!   This subroutine calculates the value of the stress associated with 
!   nodes from the value of the stress associated with elements.
!   Note that it uses a very simple average scheme which probably
!   works accurately only for a uniform mesh.
!///
	implicit none

	integer			numnod
	integer			numelm
	integer			ie(8,numelm)
	real*8			stress_element(6,1,numelm)
	real*8			stress_node(6,numnod)

	integer			n
	integer			i
	integer			j
	integer			numelm_adj(numnod)			


	stress_node = 0.d0
	numelm_adj  = 0

	do n = 1, numelm
           do i = 1, 8
!              if(ie(i,n).eq.23811) write(*,*) '23811 node shares elm#:',n
              numelm_adj(ie(i,n)) = numelm_adj(ie(i,n))+1
              do j = 1, 6
                 stress_node(j,ie(i,n)) = stress_node(j,ie(i,n)) + stress_element(j,1,n)
              enddo
           enddo
	enddo

	do i = 1, numnod
! numelm_adj(i)=1, or 2, or 4, or 8 
	do j = 1, 6
	stress_node(j,i) = stress_node(j,i)/float(numelm_adj(i))
	enddo
	enddo

end subroutine stress_element_to_node1

!=======================================================================
!                         stress_element_to_node8
!=======================================================================
subroutine stress_element_to_node8(numnod,					&
								  numelm,					&	
								  ie,						&
								  gauss_stress,			&
                                                                  gauss_coordinate,                      &
								  stress_node)
!///
!   This subroutine calculates the value of the stress associated with 
!   nodes from the value of the stress associated with Gauss points inside elements.
!   The node stress value is taken using the nearest Gauss point stress value within each element. 
!///
        use fem_data

	implicit none

	integer			numnod
	integer			numelm
	integer			ie(8,numelm)
	real*8			gauss_stress(6,8,numelm)
	real*8			gauss_coordinate(3,8,numelm)
	real*8			stress_node(6,numnod)
        real*8                  xp(3)

	integer			n
	integer			i
	integer			j
	integer			numelm_adj(numnod)			
        integer                 ng 

	stress_node = 0.d0
	numelm_adj  = 0

	do n = 1, numelm
           do i = 1, 8
              numelm_adj(ie(i,n)) = numelm_adj(ie(i,n))+1
              do j = 1, 3
                 xp(j) = fem_node_coordinate(j,ie(i,n))
              enddo
              call nearest_gauss_point(xp,n,ng)
              do j = 1, 6
                 stress_node(j,ie(i,n)) = stress_node(j,ie(i,n)) + gauss_stress(j,ng,n)
              enddo
           enddo
	enddo

	do i = 1, numnod
! numelm_adj(i)=1, or 2, or 4, or 8 
	do j = 1, 6
	stress_node(j,i) = stress_node(j,i)/float(numelm_adj(i))
	enddo
	enddo

end subroutine stress_element_to_node8

