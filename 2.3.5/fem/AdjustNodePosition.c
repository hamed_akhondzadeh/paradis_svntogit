/***************************************************************************
 *   
 *      Module:         AdjustNodePosition.c
 *      Description:    Contains functions needed to adjust the node 
 *                      positions if they move out of 
 *                      the simulation box with free surfaces. 
 *                     
 *      Version 4:      Meijie Tang and Gregg Hommes, May 5, 2006      
 * 
 * 
 *      Note: The fem routines are to be used to find
 *          (1) if the node is outside of the simulation box, for this
 *              only DD data needed is the position of the node itself;
 *          (2) if the node is outside, for each neighboring segment,
 *              find out the intersection with the surface. For this,
 *              both nodes' positions are needed. if the intersection
 *              point is outside of the segment, means this segment is
 *              outside of the box; otherwise, find the intersection point 
 *              at the surface and give this data back to DD. Also, give
 *              other information associated with the surface node
 *              (elemenet # and its normal vector etc)  
 *
 *      Included functions:
 *          AdjustNodePosition()
 *               
 **************************************************************************/
#include "Home.h"
#include "Node.h"
#include "Util.h"
#include "Comm.h"
#include "FEM.h"

void AdjustNodePosition(Home_t *home, int logLostDislocations)
{
        int     i, j, nc, splitStatus; 
        int     globalOp; 
        int     armCount, armList, arm12, arm21;
        int     node_number[4][2]; 
        int     node_number1[4][2];
        int     node_number2[4][2];
        int     num_surfs, num_surfs1, num_surfs2, rot = 1;  
        int     inter_number[2];
        real8   x, y, z; 
        real8   x1, y1, z1; 
        real8   x2, y2, z2; 
        real8   dx, dy, dz, delLength;
        real8   surf_norm[4][3];
        real8   surf_norm1[4][3];
        real8   surf_norm2[4][3]; 
        real8   xseg1[3],xseg2[3], xinter[3], xinter_old[3];
        real8   pos1[3], pos2[3], vel1[3], vel2[3];
        Param_t *param; 
        Node_t  *node, *nbr; 
        Node_t  *splitNode1, *splitNode2;
        Node_t  *tmpNode1, *tmpNode2;
          
        param = home->param;
        globalOp = 1; 

/*
 *      Initialize the list on which we'll queue all topological
 *      operations performed by this domain that will need to be
 *      sent to the remote domains.
 */
        ClearOpList(home);
        
        for (i = 0; i < home->newNodeKeyPtr; i++) {
            
            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }
        
            x = node->x;
            y = node->y; 
            z = node->z; 

            node_on_surface_(&num_surfs, node_number, surf_norm,
                             &x, &y, &z, &rot); 

            if (num_surfs > 1) {
                Fatal("AdjustNodePosiiton: A dislocation node belongs "
                      "to more than 1 surface!");
            } 

            node->fem_Surface[0] = node_number[0][0];
            node->fem_Surface[1] = node_number[0][1];
            node->fem_Surface_Norm[0] = surf_norm[0][0];
            node->fem_Surface_Norm[1] = surf_norm[0][1];
            node->fem_Surface_Norm[2] = surf_norm[0][2];

            if (num_surfs > 0) {
                node->constraint = 1;
            }
/*
 *          Loop through all arms connected to this node.  Note: the loop
 *          control variables <j> and <nc> must decremented on loop
 *          iterations during which an arm is removed or we could end
 *          up skipping arms.
 */
            nc = node->numNbrs;

            for (j = 0; j < nc; j++) {
              
                nbr = GetNeighborNode(home, node, j);
        
/*
 *              Skip segments not owned by the current node
 */
                if (OrderNodes(node, nbr) >= 0) {
                    continue;
                }

                x1 = nbr->x; 
                y1 = nbr->y; 
                z1 = nbr->z;   
        
                node_on_surface_(&num_surfs1, node_number1, surf_norm1,
                                 &x1, &y1, &z1, &rot);
    
                if (num_surfs1 > 1) {
                    Fatal ("AdjustNodePosition: A dislocation node1 belongs "
                           "to more than 1 surface!");
                }

                nbr->fem_Surface[0] = node_number1[0][0];
                nbr->fem_Surface[1] = node_number1[0][1];
                nbr->fem_Surface_Norm[0] = surf_norm1[0][0];
                nbr->fem_Surface_Norm[1] = surf_norm1[0][1];
                nbr->fem_Surface_Norm[2] = surf_norm1[0][2];

                if (num_surfs1 > 0) {
                    nbr->constraint = 1;
                }

/*
 *              If both endpoints of the segment are within the
 *              box, or 1 node is within the box and the other is
 *              on the surface, there's no need to do anything.
 */
                if (((num_surfs == 0) && (num_surfs1 == 0)) ||
                    ((num_surfs == 0) && (num_surfs1 > 0 )) ||
                    ((num_surfs > 0)  && (num_surfs1 == 0))) {
                    continue;
                }

/*
 *              If both endpoints of the segment are on the surface, 
 *              but on different FEM elements, need do nothing
 */
                if ( (num_surfs > 0)  && (num_surfs1 > 0)  &&
                     (node_number[0][0] != node_number1[0][0]) ) { 
#if 0
                    Fatal("Segment touches 2 different fem elements surfaces"); 
#endif
                    printf("Segment touches 2 different fem elements "
                           "surfaces in AdjustNodePosition");

                    printf("node coordinate = %f %f %f\n", node->x, node->y, node->z);
                    printf("nbr coordinate = %f %f %f\n", nbr->x, nbr->y, nbr->z);
                    continue;
                }

/*
 *              If any of the following circumstances are true, we
 *              can simply cut the connection between the two nodes
 *              and continue:
 *                  - If both nodes are on the same surface (and the same element)
 *                  - If one node is on the surface and the
 *                    other is outside the box
 *                  - If both nodes are outside the box.
 */
                if ((num_surfs > 0 && num_surfs1 > 0)  || 
                    (num_surfs > 0 && num_surfs1 < 0 ) ||
                    (num_surfs < 0 && num_surfs1 > 0 ) || 
                    (num_surfs < 0 && num_surfs1 < 0)) {

/*
 *                  If we need to, track the density of dislocations that
 *                  are lost as they move out of the problem space.
 */
                    if (logLostDislocations) {
                        dx = node->x - nbr->x;
                        dy = node->y - nbr->y;
                        dz = node->z - nbr->z;

                        ZImage(param, &dx, &dy, &dz);

                        delLength = sqrt(dx*dx + dy*dy + dz*dz);

                        param->fem_delSegLength += delLength;
                    }

                    ChangeArmBurg(home, node, &node->nbrTag[j], 0, 0, 0,
                                  0, 0, 0, globalOp, DEL_SEG_NONE);
                    ChangeArmBurg(home, nbr, &node->myTag, 0, 0, 0,
                                  0, 0, 0, globalOp, DEL_SEG_NONE);     

/*
 *                  Mark the nodes so that forces will be recalculated
 *                  later for whichever of the nodes remains, plus
 *                  adjust the loop control variables to account
 *                  for the deletion of an arm.
 */ 
                    node->flags |= NODE_RESET_FORCES;
                    nbr->flags  |= NODE_RESET_FORCES;

                    nc--;
                    j--;

                    continue; 
                } 
        
/*
 *              If we've reached this point, the segment consists of one
 *              node inside the box and the other outside so find the
 *              location at which the segment intersects the surface
 *              and split the current node creating a new node on the
 *              surface.
 *
 *              We probably don't need to worry about passing in correct
 *              velocity values for the two nodes since force and velocity will
 *              be recalculated later.
 */

/*  set the node outside the box to be xseg2 */ 


/*		printf("num_surfs,  num_surfs1 = %d %d\n", num_surfs, num_surfs1);*/

	        if(num_surfs < 0 ) { 
		  xseg2[0] = node->x;
		  xseg2[1] = node->y;
		  xseg2[2] = node->z; 

		  xseg1[0] = x1;
		  xseg1[1] = y1;
		  xseg1[2] = z1;
		}

		if(num_surfs1 < 0) {
		  xseg1[0] = node->x;
		  xseg1[1] = node->y;
		  xseg1[2] = node->z; 

		  xseg2[0] = x1;
		  xseg2[1] = y1;
		  xseg2[2] = z1;
		}

#if 0
                xseg1[0] = x1-10;
                xseg1[0] = y1;
                xseg1[0] = z1;
#endif


                fem_segment_surface_intersection_(xseg1,xseg2, xinter,
                                                  &inter_number[0],
                                                  &inter_number[1]);

                if (inter_number[0] == 0) {
                    Fatal("There has to be an intersection point!" ); 
                }

                armCount = 1;
                armList = j;

                pos1[0] = node->x;
                pos1[1] = node->y;
                pos1[2] = node->z;

                pos2[0] = xinter[0];
                pos2[1] = xinter[1];
                pos2[2] = xinter[2];

                vel1[0] = node->vX;
                vel1[1] = node->vY;
                vel1[2] = node->vZ;

                vel2[0] = 0.0;
                vel2[1] = 0.0;
                vel2[2] = 0.0;

                splitStatus = SplitNode(home, OPCLASS_REMESH, node,
                                        pos1, pos2, vel1, vel2, armCount,
                                        &armList, globalOp, &splitNode1,
                                        &splitNode2);

                if (splitStatus != SPLIT_SUCCESS) continue;

/*
 *              In this case, SplitNode() *should* be returning the
 *              newly created node in <splitNode2> leaving the original
 *              node in its original location, but just to be safe,
 *              we'll add a couple checks to handle things in the
 *              event that the original node gets shifted and the
 *              new node placed where node was originally located.
 *              Yes, a bit of a mess, but SplitNode() can do that.
 */

                if (node == splitNode1) {
 /*
 *                  Node was left at its original position.  If it was inside
 *                  the box, we'll cut the connection between the new node
 *                  (splitNode2) and the neighbor node, otherwise we cut
 *                  the connection between node and the new node.
 */
                    if (num_surfs == 0 ) {
                        tmpNode1 = nbr;
                        tmpNode2 = splitNode2;
                    } else {
                        tmpNode1 = node;
                        tmpNode2 = splitNode2;
                    }
                } else {
/*
 *                  Node was repositioned to the surface and the new node
 *                  (splitNode1) created at node's original position.
 *                  If node was originally inside the box, we need to
 *                  cut the connection between node and nbr, otherwise
 *                  we cut the connection between node and spliNode1 (the
 *                  new node).
 */
                    if (num_surfs == 0) {
                        tmpNode1 = splitNode2;
                        tmpNode2 = nbr;
                    } else { /* node was outside the box */
                        tmpNode1 = splitNode1;
                        tmpNode2 = splitNode2;
                    }

/*
 *                  Point <node> to the node at the original position
 *                  before continuing.
 */
                    node = splitNode1;
                }

/*
 *              If we need to, track the density of dislocations that
 *              are lost as they move out of the problem space.
 */
                if (logLostDislocations) {

                    dx = tmpNode1->x - tmpNode2->x;
                    dy = tmpNode1->y - tmpNode2->y;
                    dz = tmpNode1->z - tmpNode2->z;

                    ZImage(param, &dx, &dy, &dz);

                    delLength = sqrt(dx*dx + dy*dy + dz*dz);

                    param->fem_delSegLength += delLength;
                }

/*
 *              Find the indices of the arms connecting the two
 *              nodes and break the connection in both directions.
 */
                arm12 = GetArmID(home, tmpNode1, tmpNode2);
                arm21 = GetArmID(home, tmpNode2, tmpNode1);

                ChangeArmBurg(home, tmpNode1, &tmpNode1->nbrTag[arm12],
                              0, 0, 0, 0, 0, 0, globalOp, DEL_SEG_NONE);
                ChangeArmBurg(home, tmpNode2, &tmpNode2->nbrTag[arm21],
                              0, 0, 0, 0, 0, 0, globalOp, DEL_SEG_NONE);

/*
 *              Add node->fem_Surface and node->fem_Surface_Norm for SplitNode2
 */
                x2 = splitNode2->x; 
                y2 = splitNode2->y; 
                z2 = splitNode2->z; 

                node_on_surface_(&num_surfs2, node_number2, surf_norm2,
                                 &x2, &y2, &z2, &rot);

                if (num_surfs2 > 1) {
                    Fatal("AdjustNodePosition: A dislocation node2 belongs "
                          "to more than 1 surface!");
                }

                if (num_surfs2 < 1) {
		  Fatal("AdjustNodePosition: SplitNode2 has to be on surface !"); 
                }

                splitNode2->fem_Surface[0]=node_number2[0][0]; 
                splitNode2->fem_Surface[1]=node_number2[0][1]; 
                splitNode2->fem_Surface_Norm[0]=surf_norm2[0][0]; 
                splitNode2->fem_Surface_Norm[1]=surf_norm2[0][1]; 
                splitNode2->fem_Surface_Norm[2]=surf_norm2[0][2]; 

                if (num_surfs2 > 0) {
                    splitNode2->constraint = 1;
                }

/*
 *              After mark the nodes through 
 *              node->flags etc so that forces will be recalculated
 *              later for whichever of the nodes remains, plus
 *              adjust the loop control variables to account
 *              for the deletion of an arm.
 */ 
                nbr->flags        |= NODE_RESET_FORCES;
                splitNode1->flags |= NODE_RESET_FORCES;
                splitNode2->flags |= NODE_RESET_FORCES;

                nc--;
                j--;
            }
        }
 
/*
 *      Send the list of topological operations (if any) performed
 *      by this domain out to the remote domains, and locally process
 *      and operations performed by remote domains, then remove any
 *      nodes that may have ended up as orphans.
 */
        CommSendRemesh(home);
        FixRemesh(home);
        RemoveOrphanedNodes(home);
        
/*
 *      Last thing that needs to be done is to re-evaluate force
 *      and velocity on any local nodes that were marked above
 *      and distribute those new values to the remote domains.
 */
        ReevaluateForces(home);
        CommSendVelocity(home);

        return;    
}
