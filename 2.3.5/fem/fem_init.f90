!======================================================================= 
!                         fem_init
! Initialize FEM
!======================================================================= 
        subroutine fem_init(shear,youngs, poisson, dirsolmax,BC_type,&
                            geometry_type,                           &     
                            ax1,ax2,ay1,ay2,az1,az2)
	use fem_data 
	use tecplot_lib 
 
	implicit none

        real(8) shear, youngs, poisson
        integer dirsolmax
        integer BC_type, mesh_type 
        real(8) ax1,ax2,ay1,ay2,az1,az2
        integer geometry_type 
        
        fem_ltmp = 61

! always assume homogenous mesh 

        fem_mesh_type = 1

	fem_young_modulus=youngs
	fem_shear_modulus=shear
	fem_poisson_ratio=poisson
	fem_dir_numelm_max=dirsolmax
        fem_BC_type = BC_type 
!        fem_mesh_type = mesh_type 
        fem_geometry_type = geometry_type 

        end subroutine fem_init

