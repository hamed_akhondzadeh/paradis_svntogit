!c Use point_element in fem_mesh.f90 to find out if the node is out of 
!c the box, if yes, surf_id=-1; if it's inside, but not on free surface
!c surf_id = 0; if inside and on free surface, surf_id=1 
!c if yes, which surface and its surface normal vector. 1/24/2006 c
!c
!c Potential problem: for a corner position, it finds surf_id=6, -x surface.
!c or when a node is on an edge, which surface does it belong to? 
!c need to be solved. 10/5/2005 

!======================================================================= 
!                         node_on_surface
! This subroutine decides if the given point is on the surface of the 
! simulation box. If yes, return the surf_id number where
! the point resides, as well as its outward surface normal vector 
! 1/24/2006
! Temporatily: surf_id is not labeled in FEM codes, instead node_number
! keeps track the information: node_number(1)= fem element #; 
! node_number(2)= fem element surface #. 
!======================================================================= 
subroutine node_on_surface(num_surfs,node_number,surf_norm,x,y,z,rot)

	use fem_data 
	use tecplot_lib 
 
	implicit none
 
        real(8)         x,y,z,r(3)
        integer         i, j 
        integer         node_number(2,4) 
        real(8)         surf_norm(3,4)
	integer         ne(4), nes(4) 
        integer         num_surfs, num_surfs_tmp
        
        real(8)         surf_norm_tmp(3)
        integer         node_number_tmp(2)

        logical         SurfNorm
        integer         rot

        num_surfs = 0 

        do i = 1, 4
           node_number(1,i) = 0
           node_number(2,i) = 0 

           surf_norm(1,i)=0
           surf_norm(2,i)=0
           surf_norm(3,i)=0
        enddo


!        write(*,*) 'input x, y, z'
!       read(*,*) x, y, z

        r(1) = x
        r(2) = y
        r(3) = z

        if(rot.eq.1) then 
! calc. the coordinates in the FEM frame

           r(:) = 0.d0
           do i = 1, 3
              r(i) = r(i) + fem_ageom(i,1)*x+fem_ageom(i,2)*y+fem_ageom(i,3)*z
           enddo
        endif

        call point_element_multi(r,ne,nes,num_surfs_tmp)

! num_surfs_tmp = 1: on a box surface
! num_surfs_tmp = 0: inside the box
! num_surfs_tmp < 0: outside the box
! num_surfs_tmp > 1: on an edge or corner on the box surface 

!        write(*,*) 'num_surfs_tmp=', num_surfs_tmp

        if(num_surfs_tmp.eq.-1) then 
           num_surfs = -1
           return 
        endif

        if(num_surfs_tmp.eq.0) then 
           num_surfs = 0 
!           write(*,*) 'point inside the box or on internal surface'
           return 
        endif

        num_surfs = 1
        surf_norm(1,1) = fem_element_surface_norm(1,nes(1),ne(1))
        surf_norm(2,1) = fem_element_surface_norm(2,nes(1),ne(1))
        surf_norm(3,1) = fem_element_surface_norm(3,nes(1),ne(1))

        node_number(1,1) = ne(1)
        node_number(2,1) = nes(1)

!
! if num_surfs_tmp > 1, the point belongs to different element/surface combination, need sort out to find
! the distinctive surf_norms. 
!
        if(num_surfs_tmp.gt.1) then 

           do i = 2, num_surfs_tmp
              surf_norm_tmp(1) = fem_element_surface_norm(1,nes(i),ne(i))
              surf_norm_tmp(2) = fem_element_surface_norm(2,nes(i),ne(i))
              surf_norm_tmp(3) = fem_element_surface_norm(3,nes(i),ne(i))

              SurfNorm = .FALSE. 

              j_loop: do j = 1, i-1 
                 if(surf_norm(1,j).eq.surf_norm_tmp(1).and. &
                    surf_norm(2,j).eq.surf_norm_tmp(2).and. &
                    surf_norm(3,j).eq.surf_norm_tmp(3)) then 
                    SurfNorm = .TRUE. 
                    exit j_loop
                 endif
              enddo j_loop 

              if(.not.SurfNorm) then 

                 num_surfs = num_surfs + 1

                 surf_norm(1,num_surfs) = surf_norm_tmp(1)
                 surf_norm(2,num_surfs) = surf_norm_tmp(2)
                 surf_norm(3,num_surfs) = surf_norm_tmp(3)
                    
                 node_number(1,num_surfs) = ne(i)
                 node_number(2,num_surfs) = nes(i)
                    
              endif
                            
           enddo

        endif

!        write(*,*) 'num_surfs=', num_surfs
!        do i = 1, num_surfs
!           write(*,*) 'i=', i
!           write(*,*) 'node_number=', node_number(1,i), node_number(2,i)
!           write(*,*) 'surf_norm=', surf_norm(1,i),surf_norm(2,i),surf_norm(3,i)
!        enddo


end subroutine node_on_surface  

