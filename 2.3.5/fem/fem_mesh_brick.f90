!=======================================================================
!                          fem_mesh_data_brick
!=======================================================================
subroutine fem_mesh_data_brick(ax,			&
							   ay,			&
							   az,			&
							   nx,			&
							   ny,			&
							   nz,                  &
                                                           zshift,              &
                                                           yshift,              &
                                                           xshift,              &
                                                           ageom)
!///
!   This subroutine generates a uniform structured three dimensional mesh
!   of a brick domain.
!///
	use fem_data
	use tecplot_lib

	implicit none

	real(8)		ax
	real(8)		ay
	real(8)		az
	integer		nx
	integer		ny
	integer		nz
	
	integer		i
	integer		j
	integer		k
	integer		m
	integer		n

        real(8)         zshift
        real(8)         yshift
        real(8)         xshift

        real(8)         ageom(3,3)

        do i = 1, 3 
           do j = 1, 3
              fem_ageom(i,j) = ageom(j,i)
           enddo
        enddo
        call norm(fem_ageom(1,:))
        call norm(fem_ageom(2,:))
        call norm(fem_ageom(3,:))

!///
!   calculate number of nodes and elements
!///
	fem_number_of_node    = nx*ny*nz
	fem_number_of_element = (nx-1)*(ny-1)*(nz-1)
!///
!   allocate finite element arrays
!///
    call fem_mesh_allocation
!///
!   initialize finite element arrays
!///
    call fem_mesh_initialization
!///
!   allocate finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_allocation
!///
!   initialize finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_initialization

	do k = 1, nz
	do j = 1, ny
	do i = 1, nx
	n   = (k-1)*ny*nx+(j-1)*nx+i
	fem_node_coordinate(1,n) = -0.5d0*ax+float(i-1)*ax/float(nx-1)
	fem_node_coordinate(2,n) = -0.5d0*ay+float(j-1)*ay/float(ny-1)
	fem_node_coordinate(3,n) = -0.5d0*az+float(k-1)*az/float(nz-1)
	enddo
	enddo
	enddo

!///
!   assign node boundary condition
!   1 -- displacement need to be solved
!   0 -- displacement fixed to be 0
!  -1 -- displacement prescribed 
!///
!    fem_node_boundary_condition = 0
!///
!   node with image stress
!   1 -- yes
!   0 -- no 
!///
!   fem_node_img_id(n) = 0
!///
!   node with applied stress
!   1 -- yes
!   0 -- no 
!///
!	fem_node_app_id(n) = 0
!///

!///
!   generate element properties
!///
	do k = 1, nz-1
	do j = 1, ny-1
	do i = 1, nx-1
!///
!	connectivity, i.e., relation of local nodes and global nodes
!///
	n = (k-1)*(ny-1)*(nx-1)+(j-1)*(nx-1)+i
	m = (k-1)*ny*nx+(j-1)*nx+i
	fem_element_node(1,n) = m
	fem_element_node(2,n) = m + ny*nx
	fem_element_node(3,n) = m + ny*nx+1
	fem_element_node(4,n) = m + 1
	fem_element_node(5,n) = m + nx
	fem_element_node(6,n) = m + nx + ny*nx
	fem_element_node(7,n) = m + nx + ny*nx + 1
	fem_element_node(8,n) = m + nx + 1
	enddo 
	enddo
	enddo 

!MT
        call assign_mesh_boundary_condition(ax,ay,az)
!
! For DDFEM, shift coordinates so that z between [zshift,zshift+az], x between [xshift,xshift+ax],
! y [yshift,yshift+ay]
!

!        write(*,*) 'zshift=', zshift
!        write(*,*) 'yshift=', yshift
!        write(*,*) 'xshift=', xshift
!        do n = 1, fem_number_of_node
!           fem_node_coordinate(3,n) = fem_node_coordinate(3,n) + zshift
!           fem_node_coordinate(2,n) = fem_node_coordinate(2,n) + yshift
!           fem_node_coordinate(1,n) = fem_node_coordinate(1,n) + xshift
!        enddo

!///
!   get mesh data for fem and dd 
!///
    call fem_dd_mesh_data

!///
!   Output mesh data for tecplot (optional)
!///
	call tecplot_fem_mesh(fem_node_coordinate,				            &
						  1.d0,		                            &
						  fem_element_node,	                    &
						  'BRICK',				    &
						  'element_mesh_brick.dat',		    &
						  fem_ltmp)

!	call tecplot_fem_mesh(fem_node_coordinate,				            &
!						  1.d0,					    &
!						  fem_element_surface_element_node,	    &
!						  'QUADRILATERAL',		            &
!						  'element_surface_mesh_brick.dat',	    &
!						  fem_ltmp)
						  
!	call tecplot_fem_mesh(fem_node_coordinate,				                &
!						  1.d0,					        &
!						  fem_boundary_surface_element_node,            &
!						  'QUADRILATERAL',			        &
!						  'element_boundary_surface_mesh_brick.dat',    &
!						  fem_ltmp)

end subroutine fem_mesh_data_brick

!MT
!==========================================================================
!            assign mesh boundary condition
!            originally inside subroutine fem_mesh.f90
! Note that the boundaries of the simulation box goes from (-ax/2,-ay/2,
! -az/2) to (ax/2,ay/2,az/2)
!=========================================================================
subroutine assign_mesh_boundary_condition(ax,ay,az)
        use fem_data
        implicit none

        integer n
        real*8  ax, ay, az
        real*8  tor

        tor=1.d-5
!///
! fem_BC_type: 1--top surface traction free, fix all 5 other surfaces;
! fem_BC_type: 2--top surface traction free, bottom surface fixed displacement,rest free surface
! fem_BC_type: 3--top surface traction free, fix 4 side surfaces, bottom surface applied by Yoffe
! fem_BC_type: 4--top and bottom surface traction free, fixed disl. on 4 sides surfaces
! fem_BC_type: 5--all 6 surfaces are traction free, fix 8 corners
!///
!        write(*,*) 'fem_BC_type=', fem_BC_type

!///
!   assign node boundary condition
!   1 -- displacement need to be solved (default initial value)
!   0 -- displacement fixed to be 0
!  -1 -- displacement prescribed
!///

!///
!  mimic semi-infinite half-body with free surface at top
!///
!//////////////////////////////////////////////////////////////
! choice #1 fix all surfaces and corners except the top surface (exclude the 4 corners)
!//////////////////////////////////////////////////////////////

       if(fem_BC_type.eq.1) then

        do n = 1, fem_number_of_node
        if(dabs(fem_node_coordinate(3,n)+az/2.d0).lt.tor) then
           fem_node_boundary_condition(1,n) = 0
           fem_node_boundary_condition(2,n) = 0
           fem_node_boundary_condition(3,n) = 0
         endif
         if(dabs(fem_node_coordinate(1,n)+ax/2.d0).lt.tor   .or. &
              dabs(fem_node_coordinate(1,n)-ax/2.d0).lt.tor ) then
            fem_node_boundary_condition(1,n) = 0
            fem_node_boundary_condition(2,n) = 0
            fem_node_boundary_condition(3,n) = 0
         endif

         if(dabs(fem_node_coordinate(2,n)+ay/2.d0).lt.tor   .or. &
            dabs(fem_node_coordinate(2,n)-ay/2.d0).lt.tor ) then
           fem_node_boundary_condition(1,n) = 0
           fem_node_boundary_condition(2,n) = 0
           fem_node_boundary_condition(3,n) = 0
         endif

!   node with image stress
!   1 -- yes
!   0 -- no
!

         if(dabs(fem_node_coordinate(3,n)-az/2.d0).lt.tor .and. &
            fem_node_boundary_condition(1,n).eq.1 ) then
           fem_node_img_id(n) = 1
         endif

!   node with applied stress
!   1 -- yes
!   0 -- no
!///
!         if(dabs(fem_node_coordinate(3,n)-az/2.d0).lt.tor) then
!            fem_node_app_id(n)=1
!         endif

       enddo

      endif

!//////////////////////////////////////////////////////////////
! choice #2 fix the bottom surface + apply traction free condition on all the
! rest 5 surfaces.
!//////////////////////////////////////////////////////////////

       if(fem_BC_type.eq.2) then
        do n = 1, fem_number_of_node
         if(dabs(fem_node_coordinate(3,n)+az/2.d0).lt.tor) then
           fem_node_boundary_condition(1,n) = 0
           fem_node_boundary_condition(2,n) = 0
           fem_node_boundary_condition(3,n) = 0
         endif

!   node with image stress
!   1 -- yes
!   0 -- no
!
         if( (dabs(fem_node_coordinate(1,n)+ax/2.d0).lt.tor   .or. &
              dabs(fem_node_coordinate(1,n)-ax/2.d0).lt.tor.or. &
              dabs(fem_node_coordinate(2,n)+ay/2.d0).lt.tor   .or. &
              dabs(fem_node_coordinate(2,n)-ay/2.d0).lt.tor.or. &
              dabs(fem_node_coordinate(3,n)-az/2.d0).lt.tor).and. &
              fem_node_boundary_condition(1,n).eq.1 ) then
                fem_node_img_id(n) = 1
         endif

!   node with applied stress
!   1 -- yes
!   0 -- no
!///
!         if(dabs(fem_node_coordinate(3,n)-az/2.d0).lt.tor) then
!            fem_node_app_id(n)=1
!         endif

        enddo

       endif

!//////////////////////////////////////////////////////////////
! choice #3: apply traction free on top surface, and analytical solution bottom
! surrface and fixed displacements on the rest surfaces.
!//////////////////////////////////////////////////////////////

       if(fem_BC_type.eq.3) then
        do n = 1, fem_number_of_node

!   node with image stress
!   1 -- yes
!   0 -- no

         if(dabs(fem_node_coordinate(3,n)-az/2.d0).lt.tor) then
                fem_node_img_id(n) = 1
         endif

!   apply force on bottom surface
!   1 -- applied
!   0 -- no

         if(dabs(fem_node_coordinate(3,n)+az/2.d0).lt.tor) then
            fem_node_app_id(n) = 1
         endif

!   fixed displacement on 4 side surfaces
!   1 -- displacement to be solved
!   0 -- fixed displacement
!///
         if( (dabs(fem_node_coordinate(1,n)+ax/2.d0).lt.tor   .or. &
              dabs(fem_node_coordinate(1,n)-ax/2.d0).lt.tor.or. &
              dabs(fem_node_coordinate(2,n)+ay/2.d0).lt.tor   .or. &
              dabs(fem_node_coordinate(2,n)-ay/2.d0).lt.tor).and.&
              fem_node_img_id(n).eq.0.and.&
              fem_node_app_id(n).eq.0 ) then

                fem_node_boundary_condition(1,n)=0
                fem_node_boundary_condition(2,n)=0
                fem_node_boundary_condition(3,n)=0
         endif

        enddo

       endif

!//////////////////////////////////////////////////////////////
! choice #4: top and bottom surface traction free, side surfaces with zero displ.
!//////////////////////////////////////////////////////////////

       if(fem_BC_type.eq.4) then
        do n = 1, fem_number_of_node

! fixed displacement (=0) at 4 side surfaces

         if(dabs(fem_node_coordinate(1,n)+ax/2.d0).lt.tor   .or. &
            dabs(fem_node_coordinate(1,n)-ax/2.d0).lt.tor ) then
           fem_node_boundary_condition(1,n) = 0
           fem_node_boundary_condition(2,n) = 0
           fem_node_boundary_condition(3,n) = 0
         endif

         if(dabs(fem_node_coordinate(2,n)+ay/2.d0).lt.tor   .or. &
            dabs(fem_node_coordinate(2,n)-ay/2.d0).lt.tor ) then
           fem_node_boundary_condition(1,n) = 0
           fem_node_boundary_condition(2,n) = 0
           fem_node_boundary_condition(3,n) = 0
         endif

!   node with image stress
!   1 -- yes
!   0 -- no
!
         if( (dabs(fem_node_coordinate(3,n)+az/2.d0).lt.tor    .or. &
              dabs(fem_node_coordinate(3,n)-az/2.d0).lt.tor).and. &
              fem_node_boundary_condition(1,n).eq.1 ) then
                fem_node_img_id(n) = 1
         endif

!   node with applied stress
!   1 -- yes
!   0 -- no
!///

        enddo

       endif

!///////////////////////////////////////////////////////////////
! choice #5 all 6 surfaces are traction free,fix 8 corners
!//////////////////////////////////////////////////////////////
       if(fem_BC_type.eq.5) then
          do n = 1, fem_number_of_node
             if(dabs(fem_node_coordinate(3,n)+az/2.d0).lt.tor.or. &
                  dabs(fem_node_coordinate(3,n)-az/2.d0).lt.tor.or. &
                  dabs(fem_node_coordinate(2,n)+ay/2.d0).lt.tor.or. &
                  dabs(fem_node_coordinate(2,n)-ay/2.d0).lt.tor.or. &
                  dabs(fem_node_coordinate(1,n)+ax/2.d0).lt.tor.or. &
                  dabs(fem_node_coordinate(1,n)-ax/2.d0).lt.tor     ) then
                fem_node_img_id(n) = 1
             endif

             if( (dabs(fem_node_coordinate(3,n)+az/2.d0).lt.tor.or.      &
                  dabs(fem_node_coordinate(3,n)-az/2.d0).lt.tor).and. &
                 (dabs(fem_node_coordinate(2,n)+ay/2.d0).lt.tor.or.      &
                  dabs(fem_node_coordinate(2,n)-ay/2.d0).lt.tor).and. &
                 (dabs(fem_node_coordinate(1,n)+ax/2.d0).lt.tor.or.      &
                  dabs(fem_node_coordinate(1,n)-ax/2.d0).lt.tor) ) then
                fem_node_boundary_condition(1,n) = 0
                fem_node_boundary_condition(2,n) = 0
                fem_node_boundary_condition(3,n) = 0
                fem_node_img_id(n) = 0


             endif
          enddo
       endif

end subroutine assign_mesh_boundary_condition






