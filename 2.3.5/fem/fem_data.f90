module fem_data
!///
!   temporary channel to write out data
!///
	integer							fem_ltmp
	integer							fem_llog
!///
!   maximum number of element for direct method
!///
	integer							fem_dir_numelm_max
!///
!   Materials data
!///
	real*8		fem_young_modulus
	real*8		fem_shear_modulus
	real*8		fem_poisson_ratio
	real*8		fem_elastic_constant(3,3,3,3)
!///
!   finite element mesh data
!///
	integer						fem_number_of_node
	integer						fem_number_of_element
	integer,	allocatable	::		fem_element_node(:,:)
	integer,	allocatable	::		fem_node_boundary_condition(:,:)
	real*8,		allocatable	::		fem_node_coordinate(:,:)
	real*8,		allocatable	::		fem_node_displacement(:,:)
	real*8,		allocatable	::		fem_node_force(:,:)
	real*8,		allocatable	::		fem_element_stress(:,:,:)
	real*8,		allocatable	::		fem_gauss_stress(:,:,:)
	real*8,		allocatable	::		fem_gauss_coordinate(:,:,:)
	real*8,		allocatable	::		fem_node_stress(:,:)
!///
!   finite element and dislocation dynaqmics interface data
!///
	integer						fem_number_of_element_surface
	integer						fem_number_of_boundary_surface
	integer,	allocatable	::		fem_element_surface_node(:,:,:)
	integer,	allocatable	::		fem_boundary_surface_association(:,:)
	integer,	allocatable	::		fem_element_surface_neighbor(:,:)
	integer,	allocatable	::		fem_element_surface_element_node(:,:)
	integer,	allocatable	::		fem_boundary_surface_element_node(:,:)
	integer,	allocatable	::		fem_element_surface_img_id(:,:)
	integer,	allocatable	::		fem_element_surface_app_id(:,:)
	integer,	allocatable	::		fem_node_img_id(:)
	integer,	allocatable	::		fem_node_app_id(:)
	real*8,         allocatable     ::              fem_element_surface_norm(:,:,:)
	real*8,		allocatable	::		fem_node_app_stress(:,:)
	real*8,		allocatable	::		fem_node_img_stress(:,:)
	real*8,		allocatable	::		fem_node_seg_stress(:,:)
	real*8,		allocatable	::		fem_node_ana_stress(:,:)
!///
!   dislocation segments
!///
	integer						fem_number_of_segment_node
	integer						fem_number_of_segment_element
	integer,	allocatable	::		fem_segment_element_node(:,:)
	real*8,	        allocatable	::		fem_segment_node_coordinate(:,:)
!///
!   Historical data
!///
	integer							fem_istep

!cM
!///
! define fem mesh type: fem_mesh_type=1, homogenous mesh
!                       fem_mesh_type=2, in-homogeneous mesh
!///
        integer         fem_mesh_type 
        
!///
! define simulation box geometry type: fem_geometry_type=1, rectangular
!                                      fem_geometry_bype=2, cylinder
!                                      fem_geometry_bype=3, void
!                                      fem_geometry_bype=4, octahedron
!                                      fem_geometry_bype=5, Y-shape    
!///
        integer         fem_geometry_type

!///
! make fem_BC_type available: fem_BC_type = 1, only one free surface at z=zBoundMax
!                             fem_BC_type = 4, top and bottom along z are free surface
!                             fem_BC_type = 5, all surfaces are free 
! 
!///
        integer         fem_BC_type 

!///
! specify number of Gaussian points used
!///
        integer         fem_number_of_gauss_point

!///
! specify the rotation matrix of FEM in the dd3d coordinate system 
!///
        real*8 fem_ageom(3,3)

end module fem_data
