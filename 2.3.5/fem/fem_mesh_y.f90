!=======================================================================
!                          fem_mesh_data_brick
!=======================================================================
subroutine fem_mesh_data_y(side,			&
						   thickness,	    &
						   length,          &
						   ns,              &
						   nt,              &
						   nl)
!   This subroutine generates a structured three dimensional mesh
!   of a Y-shaped structure.
!///

	use fem_data
	use tecplot_lib

	implicit none

	real(8)		side
	real(8)		thickness
	real(8)     length

	integer		ns
	integer		nt
	integer     nl

	real(8)		ax
	real(8)		ay
	real(8)		az
	integer		nx
	integer		ny
	integer		nz

    real(8)     alpha
	real(8)		q(2,2)
	real(8)		xco(2)

	integer		i
	integer		j
	integer		k
	integer		m
	integer		n
	
	integer							maxnod
	integer							maxelm
	
	integer							number_of_node_tmp
	integer							number_of_element_tmp
	integer,	allocatable	::		element_node_tmp(:,:)
	real*8,		allocatable	::		node_coordinate_tmp(:,:)

	integer							number_of_node_sub
	integer							number_of_element_sub
	integer,	allocatable	::		element_node_sub(:,:)
	real*8,		allocatable	::		node_coordinate_sub(:,:)


!///
!   allocate temporary node arrays
!///
	number_of_node_tmp    = 0
	number_of_element_tmp = 0

	nx = nt
	ny = (ns/2)*2+1
	nz = nl

	number_of_node_sub    = nx*ny*nz
	number_of_element_sub = (nx-1)*(ny-1)*(nz-1)

	allocate(node_coordinate_sub(3,number_of_node_sub))
	allocate(element_node_sub(8,number_of_element_sub))

	maxnod  = 24*number_of_node_sub
	maxelm  = 24*number_of_element_sub
	
	allocate(element_node_tmp(8,maxelm))
	allocate(node_coordinate_tmp(3,maxnod))
!///
!   generate mesh for one arm
!///
	ax = thickness
	ay = side
	az = length

	do k = 1, nz
	do j = 1, ny
	do i = 1, nx
	n = (k-1)*ny*nx+(j-1)*nx+i
!///
!   compute node coordinate
!///
	node_coordinate_sub(1,n) = -0.5d0*ax + float(i-1)*ax/float(nx-1)
	node_coordinate_sub(2,n) = -0.5d0*ay + float(j-1)*ay/float(ny-1)
	node_coordinate_sub(3,n) =  abs(node_coordinate_sub(2,n))*sqrt(3.d0)/3.d0 +            &
	                            float(k-1)*(az+(ay-abs(node_coordinate_sub(2,n)))*sqrt(3.d0)/3.d0)/float(nz-1)
    enddo
    enddo
    enddo 

!///
!   generate element properties
!///
	do k = 1, nz-1
	do j = 1, ny-1
	do i = 1, nx-1
!///
!	connectivity, i.e., relation of local nodes and global nodes
!///
	n = (k-1)*(ny-1)*(nx-1)+(j-1)*(nx-1)+i
	m = (k-1)*ny*nx+(j-1)*nx+i
	element_node_sub(1,n) = m
	element_node_sub(2,n) = m + ny*nx
	element_node_sub(3,n) = m + ny*nx+1
	element_node_sub(4,n) = m + 1
	element_node_sub(5,n) = m + nx
	element_node_sub(6,n) = m + nx + ny*nx
	element_node_sub(7,n) = m + nx + ny*nx + 1
	element_node_sub(8,n) = m + nx + 1
    enddo
    enddo
    enddo 

	call tecplot_fem_mesh(node_coordinate_sub,				&
						  1.d0,								&
						  element_node_sub,					&
						  'BRICK',							&
						  'Y_1.dat',					&
						  fem_ltmp)

	call joint_hex_mesh(node_coordinate_tmp,		&
						node_coordinate_sub,		&
						element_node_tmp,			&
						element_node_sub,			&
						number_of_node_tmp,			&
						number_of_node_sub,			&
						number_of_element_tmp,		&
						number_of_element_sub,		&
						maxnod,			            &
						maxelm,		                &
						1.d-3*ax)
!///
!   get arm 2 by clockwise rotating arm 1 120 degress
!///
    alpha  = 8.d0*atan(1.d0)/3.d0
    q(1,1) = cos(alpha)
    q(1,2) =-sin(alpha)
    q(2,1) = sin(alpha)
    q(2,2) = cos(alpha)
    
    do n = 1, number_of_node_sub
    xco(1) = node_coordinate_sub(2,n)
    xco(2) = node_coordinate_sub(3,n)
	node_coordinate_sub(2,n) = xco(1)*q(1,1) + xco(2)*q(2,1) 
	node_coordinate_sub(3,n) = xco(1)*q(1,2) + xco(2)*q(2,2)
	enddo

	call tecplot_fem_mesh(node_coordinate_sub,				&
						  1.d0,								&
						  element_node_sub,					&
						  'BRICK',							&
						  'Y_2.dat',					&
						  fem_ltmp)

	call joint_hex_mesh(node_coordinate_tmp,		&
						node_coordinate_sub,		&
						element_node_tmp,			&
						element_node_sub,			&
						number_of_node_tmp,			&
						number_of_node_sub,			&
						number_of_element_tmp,		&
						number_of_element_sub,		&
						maxnod,			            &
						maxelm,		                &
						1.d-3*ax)
!///
!   get arm 3 by clockwise rotating arm 3 120 degress
!///
    alpha  = 8.d0*atan(1.d0)/3.d0
    q(1,1) = cos(alpha)
    q(1,2) =-sin(alpha)
    q(2,1) = sin(alpha)
    q(2,2) = cos(alpha)
    
    do n = 1, number_of_node_sub
    xco(1) = node_coordinate_sub(2,n)
    xco(2) = node_coordinate_sub(3,n)
	node_coordinate_sub(2,n) = xco(1)*q(1,1) + xco(2)*q(2,1) 
	node_coordinate_sub(3,n) = xco(1)*q(1,2) + xco(2)*q(2,2)
	enddo

	call tecplot_fem_mesh(node_coordinate_sub,				&
						  1.d0,								&
						  element_node_sub,					&
						  'BRICK',							&
						  'Y_3.dat',					&
						  fem_ltmp)

	call joint_hex_mesh(node_coordinate_tmp,		&
						node_coordinate_sub,		&
						element_node_tmp,			&
						element_node_sub,			&
						number_of_node_tmp,			&
						number_of_node_sub,			&
						number_of_element_tmp,		&
						number_of_element_sub,		&
						maxnod,			            &
						maxelm,		                &
						1.d-3*ax)
!///
!   assign temporary mesh data
!///						
	fem_number_of_node    = number_of_node_tmp
	fem_number_of_element = number_of_element_tmp
!///
!   allocate finite element arrays
!///
    call fem_mesh_allocation
!///
!   initialize finite element arrays
!///
    call fem_mesh_initialization
!///
!   allocate finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_allocation
!///
!   initialize finite element and dislocation dynamics interface arrays
!///
    call fem_dd_mesh_initialization
!///
!	assign node coordinates and element connectivity
!///
	do n = 1, fem_number_of_node
	do i = 1, 3
	fem_node_coordinate(i,n) = node_coordinate_tmp(i,n)
	enddo
	enddo
	
	do n = 1, fem_number_of_element
	do i = 1, 8
	fem_element_node(i,n) = element_node_tmp(i,n)
	enddo
	enddo
!///
!   deallocate temporary data array
!///
	deallocate(element_node_tmp)
	deallocate(node_coordinate_tmp)
	deallocate(element_node_sub)
	deallocate(node_coordinate_sub)
!///
!   get mesh data for fem and dd 
!///
    call fem_dd_mesh_data
!///
!   Output mesh data for tecplot (optional)
!///
	call tecplot_fem_mesh(fem_node_coordinate,					&
						  1.d0,									&
						  fem_element_node,						&
						  'BRICK',								&
						  'element_mesh_Y.dat',			        &
						  fem_ltmp)

end subroutine fem_mesh_data_y




