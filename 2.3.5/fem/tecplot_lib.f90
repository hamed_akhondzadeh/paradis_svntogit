module tecplot_lib
contains
!==========================================================================
!                              fem_mesh
!==========================================================================
subroutine tecplot_fem_mesh(x,				&
							scale,			&
							ie,				&
							elementtype,	&
							filename,		&
							ltmp)	
        use fem_data 
	implicit none
	
	real(8),		intent(in) ::		x(:,:)
	real(8),		intent(in) ::		scale
	integer,		intent(in) ::		ie(:,:)
	character*(*),	intent(in) ::		elementtype
	character*(*),	intent(in) ::		filename
	integer,		intent(in) ::		ltmp
	 
	integer		numnod
	integer		numelm
	integer		ndim
	integer		nnod
	integer		i
	integer		j

	numnod = ubound(x,dim=2)
	ndim   = ubound(x,dim=1)
	numelm = ubound(ie,dim=2)
	nnod   = ubound(ie,dim=1)

	open(unit=ltmp,file=trim(filename),status='unknown')
    write(ltmp,'('' TITLE = "Mesh" '')')

	select case(ndim)
		case(2)
			write(ltmp,'('' VARIABLES = X Y'')')
		case(3)
			write(ltmp,'('' VARIABLES = X Y Z '')')
			case default
			print*,' *** error in fem_mesh: unexpected dimension'
	end select

	select case(nnod)
		case(3)
		    write(ltmp,'('' ZONE T = "ZONE"'','', N = '',i8, '', E = '',i8, '', F = FEPOINT'', &
                             '', ET = '',a15)') numnod, numelm, elementtype
		case(4)
		    write(ltmp,'('' ZONE T = "ZONE"'','', N = '',i8, '', E = '',i8, '', F = FEPOINT'', &
                             '', ET = '',a15)') numnod, numelm, elementtype
		case(6)
		    write(ltmp,'('' ZONE T = "ZONE"'','', N = '',i8, '', E = '',i8, '', F = FEPOINT'', &
                             '', ET = '',a15)') numnod, 4*numelm, elementtype
		case(8)
		    write(ltmp,'('' ZONE T = "ZONE"'','', N = '',i8, '', E = '',i8, '', F = FEPOINT'', &
                             '', ET = '',a15)') numnod, 4*numelm, elementtype
		case default
			print*,' *** error in fem_mesh: unexpected element type'
	end select

	select case(ndim)
		case(2)
			write(ltmp,'(1x,2e15.5)') ((x(j,i)/scale,j=1,ndim),i=1,numnod)
		case(3)
			write(ltmp,'(1x,3e15.5)') ((x(j,i)/scale,j=1,ndim),i=1,numnod)
			case default
			print*,' *** error in fem_mesh: unexpected dimension'
	end select

	select case(nnod)
		case(3)
			write(ltmp,'(1x,3i10)') ((ie(j,i),j=1,nnod),i=1,numelm)
		case(4)
			write(ltmp,'(1x,4i10)') ((ie(j,i),j=1,nnod),i=1,numelm)
		case(6)
			do i = 1,numelm
			write(ltmp,'(1x,4i10)')ie(1,i),ie(4,i),ie(6,i)
			write(ltmp,'(1x,4i10)')ie(4,i),ie(2,i),ie(5,i)
			write(ltmp,'(1x,4i10)')ie(6,i),ie(5,i),ie(3,i)
			write(ltmp,'(1x,4i10)')ie(4,i),ie(5,i),ie(6,i)
			enddo
		case(8)
			write(ltmp,'(1x,8i10)') ((ie(j,i),j=1,nnod),i=1,numelm)
		case default
			print*,' *** error in fem_mesh: unexpected element type'
	end select

	close(unit=ltmp)

end subroutine tecplot_fem_mesh
!==========================================================================
!                            tecplot_fem_field
!==========================================================================
subroutine tecplot_fem_field(x,							&
							 length_scale,				&
							 var,						&
							 var_scale,					&
							 ie,						&
							 istep,						&
							 varname,					&
							 elementtype,				&
							 filename,					&
							 ltmp)	
	
        use fem_data 
	implicit none

	real(8),		intent(in) ::		x(:,:)
	real(8),		intent(in) ::		length_scale
	real(8),		intent(in) ::		var(:,:)
	real(8),		intent(in) ::		var_scale
	integer,		intent(in) ::		ie(:,:)
	integer,		intent(in) ::		istep
	character*(*),	intent(in) ::		varname
	character*(*),	intent(in) ::		elementtype
	character*(*),	intent(in) ::		filename
	integer,		intent(in) ::		ltmp
	 
	integer		numnod
	integer		numelm
	integer		ndim
	integer		nnod
	integer		nvar

	integer		i
	integer		j

!///
!   get dimension
!///
	numnod = ubound(x,2)
	ndim   = ubound(x,1)
	numelm = ubound(ie,2)
	nnod   = ubound(ie,1)
	nvar   = ubound(var,1)
!///
!	data for Tecplot.
!///
    if(istep.eq.1) then
	open(ltmp,file=filename,status='unknown')
    write(ltmp,'('' TITLE = " '',a10, '' field data" '')') varname
	
	if(ndim.eq.2.and.nvar.eq.1) then	
    write(ltmp,'('' VARIABLES = '','' "X ",'','' "Y ",'','' "VAR",'')')
	else if(ndim.eq.2.and.nvar.eq.2) then
    write(ltmp,'('' VARIABLES = '','' "X ",'','' "Y ",'','' "VAR_1",'','' "VAR_2",'')')
	else if(ndim.eq.2.and.nvar.eq.3) then
    write(ltmp,'('' VARIABLES = '','' "X ",'','' "Y ",'','' "VAR_1",'','' "VAR_2",'','' "VAR_3",'')')
	else if(ndim.eq.2.and.nvar.eq.6) then
    write(ltmp,'('' VARIABLES = '','' "X ",'','' "Y ",'','' "VAR_1",'','' "VAR_2",'','' "VAR_3",'', &
           '' "VAR_4",'','' "VAR_5",'','' "VAR_6",'')')
	else if(ndim.eq.3.and.nvar.eq.3) then
    write(ltmp,'('' VARIABLES = '','' "X ",'','' "Y ",'','' "Z ",'','' "VAR_1",'','' "VAR_2",'','' "VAR_3",'')')
	else if(ndim.eq.3.and.nvar.eq.6) then
    write(ltmp,'('' VARIABLES = '','' "X ",'','' "Y ",'','' "Z ",'','' "VAR_1",'','' "VAR_2",'', &
            '' "VAR_3",'','' "VAR_4",'','' "VAR_5",'','' "VAR_6",'')')
	else
	print*,' *** error in tecplot_fem_field: expansion is needed'
	endif
    endif

	open(ltmp,file=filename,position='append')
	if(nnod.ne.6) then
    write(ltmp,'('' ZONE T = "ZONE'',i5,''",'','' N ='',i8,'','','' E ='',i8,'','','' F = FEPOINT,'', &
                '' ET ='',a15)') istep, numnod, numelm, elementtype
	else
    write(ltmp,'('' ZONE T = "ZONE'',i5,''",'','' N ='',i8,'','','' E ='',i8,'','','' F = FEPOINT,'', &
                '' ET ='',a15)') istep,	numnod, 4*numelm, elementtype
	endif

	if(ndim.eq.3.and.nvar.eq.3) then
    write(ltmp,'(1x,3e12.4,3e12.4)') ((x(j,i)/length_scale,j=1,ndim),(var(j,i)/var_scale,j=1,nvar),i=1,numnod)
	else if(ndim.eq.3.and.nvar.eq.6) then
    write(ltmp,'(1x,3e12.4,6e12.4)') ((x(j,i)/length_scale,j=1,ndim),(var(j,i)/var_scale,j=1,nvar),i=1,numnod)
	else if(ndim.eq.2.and.nvar.eq.6) then
    write(ltmp,'(1x,2e12.4,6e12.4)') ((x(j,i)/length_scale,j=1,ndim),(var(j,i)/var_scale,j=1,nvar),i=1,numnod)
	else if(ndim.eq.2.and.nvar.eq.3) then
    write(ltmp,'(1x,2e12.4,3e12.4)') ((x(j,i)/length_scale,j=1,ndim),(var(j,i)/var_scale,j=1,nvar),i=1,numnod)
	else
	print*,' *** error in tecplot_fem_field: expansion is needed'
	endif


	if(nnod.eq.3) then
    write(ltmp,'(1x,3i10)') ((ie(j,i),j=1,nnod),i=1,numelm)
        else if(nnod.eq.4) then 
    write(ltmp,'(1x,4i10)') ((ie(j,i),j=1,nnod),i=1,numelm)
	else if(nnod.eq.8) then
    write(ltmp,'(1x,8i10)') ((ie(j,i),j=1,nnod),i=1,numelm)
    endif

	close(unit=ltmp)

end subroutine tecplot_fem_field
!==========================================================================
!                     fem_field_plot_8n_3d
!==========================================================================
subroutine fem_field_plot_8n_3d(x,							&
								length_scale,				&
								u,							&
								displacement_scale,			&
								sig,						&
								stress_scale,				&
								ie,							&
								numnod,						&
								numelm,						&
								istep,						&
								filename,					&
								ltmp)	
	
	implicit none

	integer				numnod
	integer				numelm
	real*8				length_scale
	real*8				displacement_scale
	real*8				stress_scale
	real*8				x(3,numnod)
	real*8				u(3,numnod)
	real*8				sig(6,numnod)
	integer				ie(8,numelm)
	integer				istep
	character*(*)		filename
	integer				ltmp

	integer		i
	integer		j
!///
!	data for Tecplot.
!///
    if(istep.eq.1) then
	open(ltmp,file=filename,status='unknown')
    write(ltmp,'('' TITLE = " Displacement Field data" '')')
    write(ltmp,'('' VARIABLES = '','' "X ",'','' "Y ",'','' "Z ",'','' "U_1",'','' "U_2",'','' "U_3",'', &
                 '' "SIG_11",'','' "SIG_22",'','' "SIG_33",'','' "SIG_32",'','' "SIG_13",'','' "SIG_12",'')')
    endif
	open(ltmp,file=filename,position='append')
    write(ltmp,'('' ZONE T = "ZONE'',i5,''",'','' N ='',i8,'','','' E ='',i8,'','','' F = FEPOINT,'', &
                 '' ET = BRICK '')') istep,numnod,numelm

    do i = 1,numnod
    write(ltmp,'(1x,3e12.4,3e12.4,6e12.4)') x(1,i)/length_scale,				&
										   x(2,i)/length_scale,				&
                                           x(3,i)/length_scale,				&
                                           u(1,i)/displacement_scale,		&
                                           u(2,i)/displacement_scale,		&
                                           u(3,i)/displacement_scale,		&
                                           sig(1,i)/stress_scale,			&
                                           sig(2,i)/stress_scale,			&
                                           sig(3,i)/stress_scale,			&
                                           sig(4,i)/stress_scale,			&
                                           sig(5,i)/stress_scale,			&
                                           sig(6,i)/stress_scale
    enddo

    do i = 1,numelm
    write(ltmp,'(1x,8i10)') (ie(j,i),j=1,8)
    enddo

	close(unit=ltmp)

end subroutine fem_field_plot_8n_3d
!==========================================================================
!                         llnl_graphics_node_data
!==========================================================================
subroutine llnl_graphics_node_data(x,				&
				    			   scale,			&
								   filename,		&
								   ltmp)	
	implicit none
	
	real(8),		intent(in) ::		x(:,:)
	real(8),		intent(in) ::		scale
	character*(*),	intent(in) ::		filename
	integer,		intent(in) ::		ltmp
	 
	integer		numnod
	integer		ndim
	integer		i
	integer		j

	numnod = ubound(x,dim=2)
	ndim   = ubound(x,dim=1)

	open(unit=ltmp,file=trim(filename),status='unknown')
    write(ltmp,'(i15)') numnod
	write(ltmp,'(1x,3e15.5)') ((x(j,i)/scale,j=1,ndim),i=1,numnod)

	close(unit=ltmp)

end subroutine llnl_graphics_node_data
!==========================================================================
!                     llnl_graphics_connection_data
!==========================================================================
subroutine llnl_graphics_connection_data(ie,			&
										 elementtype,	&
										 filename,		&
										 ltmp)	
	implicit none
	
	integer,		intent(in) ::		ie(:,:)
	character*(*),	intent(in) ::		elementtype
	character*(*),	intent(in) ::		filename
	integer,		intent(in) ::		ltmp
	 
	integer			numelm
	integer			nnod
	integer			numseg
	integer,		allocatable ::		segment_color(:)
	integer		i
	integer		j

	numelm = ubound(ie,dim=2)
	nnod   = ubound(ie,dim=1)

	open(unit=ltmp,file=trim(filename),status='unknown')
!///
!   get total number of segments
!///
!	numseg = 12*numelm		!for covenience, I overplot the segments of each element
!    write(ltmp,'(i15)') numseg
!///
!   assign color code to each segment
!///
!	allocate(segment_color(numseg))
!	do i = 1, numseg
!	segment_color(i) = 1	!for covenience, same color is assigned
!	enddo

	select case(nnod)
		case(8)
!			do i = 1, numelm
!			write(ltmp,'(2i10, i15)') ie(1,i), ie(4,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(4,i), ie(8,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(8,i), ie(5,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(5,i), ie(1,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(2,i), ie(3,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(3,i), ie(7,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(7,i), ie(6,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(6,i), ie(2,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(1,i), ie(2,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(4,i), ie(3,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(8,i), ie(7,i), segment_color(i)
!			write(ltmp,'(2i10, i15)') ie(5,i), ie(6,i), segment_color(i)
!			enddo
		case default
			print*,' *** error in llnl_graphics_connection_data: unexpected element type'
	end select

	close(unit=ltmp)

!	deallocate(segment_color)

end subroutine llnl_graphics_connection_data

end module tecplot_lib
