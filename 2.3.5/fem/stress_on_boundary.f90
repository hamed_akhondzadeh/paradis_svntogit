!======================================================================= 
!                         stress_on_boundary
! Calculate the stresses due to all segments at surface nodes (sig_seg) 
! and the image stress boundary condition for FEM solver (sig_node_img). 
! sig_app is passed in from dd using FEM notation for stress. 
! (sigxx,sigxy,sigxz, sigyy,sigyz,sigzz)
! M. Tang 6/18/2004 
!======================================================================= 
        subroutine stress_on_boundary(sig_app)

 
	use fem_data 
	use tecplot_lib 
 
	implicit none
        external ParadisStress
 
        real(8)         sig_seg(6)
        real(8)         sig_node_img(6), sig_node_yf(6)
        real(8)         sig_app(6) 
        real(8)         r(3) 

	integer		is 
        integer         i, j 

!c
!c surface_id specifies the surface where the node is. if node is not on any surface, 
!c surface_id = 0 
!c 
        integer        surface_id 

        logical         help


        help = .true. 

         fem_node_seg_stress=0.d0
         fem_node_img_stress=0.d0
         fem_node_ana_stress=0.d0
         
         do i = 1, fem_number_of_node
            do j = 1, 6
               fem_node_app_stress(j,i)=sig_app(j)
            enddo
         enddo



        do i=1, fem_number_of_node
!c
!c for now, surface_id is not treated and set to 0 all the time. 
!c 6/18/2004 
!c
           surface_id = 0 

           do j=1, 3
              r(j)=fem_node_coordinate(j,i)
           enddo
!c# 
!c#  calc. the stress due to ALL dd segments at each fem node
!c#

!C (!!!This is where F90 needs to call C routines)

        do j=1, 6
           sig_seg(j)=0.d0
        enddo 

        if(fem_node_img_id(i).eq.1) then 
           call ParadisStress(r,sig_seg,sig_node_img,sig_node_yf)
        endif

!c#ifdef debug
      do j = 1, 6
         fem_node_ana_stress(j,i) = sig_node_yf(j)
      enddo

!c#endif 
!c# 
!c# pay attention to notation, if defined different in your dd code, re-define them
!c# NOTE: sig_seg(1)=sigma_11, sig_seg(2)=sigma22,sig_seg(3)=sigma33,
!c#       sig_seg(4)=sigma_12, sig_seg(5)=sigma23,sig_seg(6)=sigma13
!c#
           do j=1, 6
              fem_node_seg_stress(j,i)= sig_seg(j) 
              if(fem_node_img_id(i).eq.1) then 
                 fem_node_img_stress(j,i)= -sig_node_img(j)
              endif
           enddo 
        enddo 

!c# also add applied stress tensor values to certain boundary nodes. Ignore the details
!c# for now. 

!c       do i=1, fem_number_of_node
!c          if(fem_node_app_id(i).eq.1) then
!c            do j=1, 6 
!c             fem_node_app_stress(j,i)=sig_app(j)
!c            enddo  
!c          endif  
!c       enddo 

      end subroutine stress_on_boundary  

