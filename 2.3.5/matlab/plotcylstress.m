N = 10;
stress = load('../outputs/test_screw/stress.out');
x = reshape(stress(:,1),N,N);
y = reshape(stress(:,2),N,N);
sigxz = reshape(stress(:,8),N,N);
sigyz = reshape(stress(:,7),N,N);


r = sqrt(x.*x + y.*y);
sigrz = (y.*sigxz - x.*sigyz) ./ r;

contour(sigrz, 30);

axis('equal');


