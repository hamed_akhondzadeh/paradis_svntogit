/*****************************************************************************
 *
 *      Module:         Cylinder_Remesh_3.c
 *      Description:    This module contains functions to handle node
 *                      collision with the cylindrical surface
 *                      
 *      Included functions:
 *              cylproject(x,y,z,x2,y2,z2,Radius,*dx,*dy,*dz)
 *              Cylinder_Remesh(home)
 *
 *      Chris Weinberger & Wei Cai
 *
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "Home.h"
#include "Util.h"
#include "CYL.h"
#include "QueueOps.h"



/*-------------------------------------------------------------------------
 *
 *      Function:    cylpoint - BCC
 *      Description: Projects a pont along the dislocation line onto the
 *                   cylinder surface
 *
 *------------------------------------------------------------------------*/
void cylproject(double x,double y,double z,double x2,double y2,double z2,double Radius,
              double *dx,double *dy,double *dz)
{
      double vx, vy, vz, v2x, v2y, v2z;
      double t, A, B, C;

      vx = x - x2; vy = y - y2; vz = z - z2;
      A  = vx*vx + vy*vy;
      B  = 2*(x2*vx + y2*vy);
      C  = x2*x2 + y2*y2 - Radius*Radius;

      /* t is the solution of A*t^2 + B*t + C = 0 */
      t = ( -B+sqrt(B*B-4*A*C) ) / (2*A);

      *dx = x2 + t*vx - x;
      *dy = y2 + t*vy - y;
      *dz = z2 + t*vz - z;
}



/*-------------------------------------------------------------------------
 *
 *      Function:    Cylinder_Remesh_3
 *      Description: Handle node collision with cylindrical surface
 *
 *------------------------------------------------------------------------*/
void Cylinder_Remesh_3(Home_t *home)
{
    int i, j, k, constra, constrb, ninlinksa, ninlinksb, status, mergeStatus, mergeDone;
  int armCount, splitStatus, globalOp, armID, *armList;
  double x, y, z, ri, Radius, Delta, nx, ny, nz, dx, dy, dz, bx, by, bz, dr;
  double tmp, nodePos[3], newPos[3], nodeVel[3], newVel[3], vec[3];
  real8   f0seg1[3], f0seg2[3], f1seg1[3], f1seg2[3];
  Node_t *node, *nodea, *nodeb, *nodek, *splitNode1, *splitNode2, *mergedNode;
  Node_t *nbrNode;
  Param_t *param;
  double moveratio;
  Tag_t   oldTag1, oldTag2;
  

  /* Node constraint :
   * 7 = fixed / no Burgers vector conservation
   * 6 = on cylinder surface - one arm + virtual segment
   */
  

   param = home->param;
   Radius = cylinder0->radius;


  /* Go through all nodes, if the nodes flag is 6, and project this node
     to the surface
   */
   for (i = 0; i < home->newNodeKeyPtr; i++) {

        node = home->nodeKeys[i];
        if (node == (Node_t *)NULL) continue;
        if (node->constraint != 6) continue;
//        printf("Projecting Node to the Surface\n");

        x = node->x; y = node->y; z = node->z;
        nx = node->nx[0]; ny = node->ny[0]; nz = node->nz[0];
        ri = sqrt(x*x+y*y);

      /* find the neighboring node of this node */
        for (j = 0; j < node->numNbrs; j++) {
           nbrNode = GetNeighborNode(home, node, j);
           if (nbrNode->constraint == 0) {
              cylproject(x,y,z,nbrNode->x,nbrNode->y,nbrNode->z,Radius,&dx,&dy,&dz);
              break;
           }
        }
      
        /* debug */
        ri = sqrt(dx*dx+dy*dy+dz*dz);
        if (ri > Radius)
        {
            printf("[%e,%e,%e]=cylpoint(%e,%e,%e,%e,%e,%e,%e)\n",dx,dy,dz,x,y,z,nx,ny,nz,Radius);
	    printf("ri=%f\n",ri);
            exit(-1);
        }
     
        node->x += dx;
        node->y += dy;
        node->z += dz;
        node->constraint = 6;
   }
   
 
  /* Go through all nodes, if node gets close enough to the surface,
   * move it to the surface and change flag to 6
   */
   for (i = 0; i < home->newNodeKeyPtr; i++) {

        node = home->nodeKeys[i];
        if (node == (Node_t *)NULL) continue;

        x = node->x; y = node->y; z = node->z;
        nx = node->nx[0]; ny = node->ny[0]; nz = node->nz[0];
        ri = sqrt(x*x+y*y);

        if ( (ri >= Radius) && (node->constraint == 0) ) {
//            printf("Found Node Outside Domain: Remesh!\n");
            node->constraint=6;

           /* find the neighbor nodes of this node */
               for (j = 0; j < node->numNbrs; j++) {
                  nbrNode = GetNeighborNode(home, node, j);
                  if (nbrNode->constraint == 0) {
                     cylproject(x,y,z,nbrNode->x,nbrNode->y,nbrNode->z,
				Radius,&dx,&dy,&dz);
                     globalOp = (nbrNode->myTag.domainID != node->myTag.domainID);
                     oldTag1 = node->myTag;
                     oldTag2 = nbrNode->myTag;
                     armID= GetArmID(home, node, nbrNode);
                     armList = &armID;
                     armCount = 1;
                     vec[0] = nbrNode->x-x;
                     vec[1] = nbrNode->y-y;
                     vec[2] = nbrNode->z-z;
                     nodePos[0] = x;
                     nodePos[1] = y;
                     nodePos[2] = z;
                     nodeVel[0] = node->vX;
                     nodeVel[1] = node->vY;
                     nodeVel[2] = node->vZ;
                     newVel[0] = node->vX;
                     newVel[1] = node->vY;
                     newVel[2] = node->vZ;
                     newPos[0] = x + dx;
                     newPos[1] = y + dy;
                     newPos[2] = z + dz;       
                     splitStatus = SplitNode(home, OPCLASS_REMESH,
                                    node, nodePos, newPos,
                                    nodeVel, newVel,
                                    armCount, armList,
                                    globalOp, &splitNode1,
                                    &splitNode2);

                     if (splitStatus == SPLIT_SUCCESS) {

                         EstRefinementForces(home, node, nbrNode, newPos, vec,
                                             f0seg1, f1seg1, f0seg2, f1seg2);
/*
 *              The force estimates above are good enough for the
 *              remainder of this timestep, but mark the force and
 *              velocity data for some nodes as obsolete so that
 *              more accurate forces will be recalculated either at
 *              the end of this timestep, or the beginning of the next.
 */
                         MarkNodeForceObsolete(home, splitNode1);
                         MarkNodeForceObsolete(home, splitNode2);
                         MarkNodeForceObsolete(home, nbrNode);

/*
 *              Reset nodal forces on all nodes involved
 *              in the split.
 */
                         ResetSegForces(home, splitNode1, &splitNode2->myTag,
                               f0seg1[X], f0seg1[Y], f0seg1[Z], 1);

                         ResetSegForces(home, splitNode2, &splitNode1->myTag,
                               f1seg1[X], f1seg1[Y], f1seg1[Z], 1);

                         ResetSegForces(home, splitNode2, &nbrNode->myTag,
                               f0seg2[X], f0seg2[Y], f0seg2[Z], 1);

                         ResetSegForces(home, nbrNode, &splitNode2->myTag,
                               f1seg2[X], f1seg2[Y], f1seg2[Z], 1);

                         (void)EvaluateMobility(home, splitNode1);
                         (void)EvaluateMobility(home, splitNode2);
                         (void)EvaluateMobility(home, nbrNode);
//                         printf("New Node Pos = (%e,%e,%e)\n",newPos[0],newPos[1],newPos[2]);
//                         printf("Old Node Pos = (%e,%e,%e)\n",nodePos[0],nodePos[1],nodePos[2]);
//                         printf("Splitnode2 Pos = (%e,%e,%e)\n",splitNode2->x,splitNode2->y,splitNode2->z);
                         splitNode2->constraint=6;
                      }
                  }
               }
        }
   }







   
   /* Remove any link between two surface nodes (flag == 6)
    */
   for (i = 0; i < home->newNodeKeyPtr; i++) {

        nodea = home->nodeKeys[i];
        if (nodea == (Node_t *)NULL) continue;

        constra = nodea->constraint;
        for (j = 0; j < nodea->numNbrs; j++) {

            nodeb = GetNeighborNode(home, nodea, j); 
            constrb = nodeb->constraint;

            if(OrderNodes(nodea,nodeb)>0) continue;

            nx = nodea->nx[j]; ny = nodea->ny[j]; nz = nodea->nz[j];
            bx = nodea->burgX[j]; by = nodea->burgY[j]; bz = nodea->burgZ[j];

            //printf("j = %d constra = %d constrb = %d\n",j,constra,constrb);

            if ( (constra == 6) && (constrb == 6) ) {
              /* Count the number of links inside the cylinder */ 
              ninlinksa = ninlinksb = 0;

              for (k=0; k<nodea->numNbrs; k++) {
                 nodek = GetNeighborNode(home,nodea, k);
                 if (nodek->constraint == 0)
                      ninlinksa ++;
              }

              for (k=0; k<nodeb->numNbrs; k++) {
                 nodek = GetNeighborNode(home,nodeb, k);
                 if (nodek->constraint == 0)
                      ninlinksb ++;
              }
              //printf("ninlinksa = %d  ninlinksb = %d\n",ninlinksa,ninlinksb);
              //printf("constra = %d  constrab = %d\n",constra,constrb);

              /* Now apply remesh rules on two connected nodes on the surface */             
              //if ( (ninlinksa == (nodea->numNbrs-1)) && (ninlinksb == (nodeb->numNbrs-1)) ) {
               /* nodes a and b only have links inside, remove the link between a and b
                */
                  globalOp = 1;
                  armCount = 1;
                  armID = j;
                  armList = &armID;
                  ChangeArmBurg(home, nodea, &nodeb->myTag, 0, 0, 0, 0, 0, 0,
#ifdef _STACKINGFAULT
                                      0.0, 0.0, 0.0,
#endif
                                globalOp, DEL_SEG_HALF);
                  ChangeArmBurg(home, nodeb, &nodea->myTag, 0, 0, 0, 0, 0, 0,
#ifdef _STACKINGFAULT
                                      0.0, 0.0, 0.0,
#endif
                                globalOp, DEL_SEG_HALF);
            }
        }
   }

   /* Remove any nodes with zero neighbors
    */
   for (i = 0; i < home->newNodeKeyPtr; i++) {

        nodea = home->nodeKeys[i];
        if (nodea == (Node_t *)NULL) continue;

        if (nodea->numNbrs == 0)
            RemoveNode(home, nodea, globalOp);
   }
   
}
