/*****************************************************************************
 *
 *      Module:         Cylinder_Remesh_2.c
 *      Description:    This module contains functions to handle node
 *                      collision with the cylindrical surface
 *                      
 *      Included functions:
 *              cylpoint(x,y,z,nx,ny,nz,Radius)
 *              Cylinder_Remesh()
 *
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "Home.h"
#include "Util.h"
#include "CYL.h"
#include "QueueOps.h"
//#include "DD3dProto.h"

/*-------------------------------------------------------------------------
 *
 *      Function:    cylpoint - FCC
 *      Description: Returns the point on the cylinder "relatively" close to the
 *                   point (x,y,z), subject to the glide constraint (nx,ny,nz)
 *                   Radius is the cylinder radius.  Cylinder is along z axis.
 *
 *------------------------------------------------------------------------*/
void cylpoint(double x,double y,double z,double nx,double ny,double nz,double Radius,
              double *dx,double *dy,double *dz)
{
      double vx, vy, vz, v2x, v2y, v2z;
      double r, x1, x2, y1, y2, d11, d12, d21, d22, d, alpha, alpha1, alpha2, tmp;

      
      tmp = sqrt(nx*nx+ny*ny+nz*nz);
      nx = nx / tmp;
      ny = ny / tmp;
      nz = nz / tmp;
   
      r = sqrt(x*x+y*y);
      x1 = x*Radius/r;
      x2 = -x1;
      y1 = y*Radius/r;
      y2 = -y1;

      d11 = sqrt( (x-x1)*(x-x1) + (y-y1)*(y-y1) );
      d12 = sqrt( (x-x1)*(x-x1) + (y-y2)*(y-y2) );
      d21 = sqrt( (x-x2)*(x-x2) + (y-y1)*(y-y1) );
      d22 = sqrt( (x-x2)*(x-x2) + (y-y2)*(y-y2) );

      d = d11; 
      if(d>d12) d=d12;
      if(d>d21) d=d21;
      if(d>d22) d=d22;

      if ( d11 == d ) {
          vx = x1-x; vy = y1-y; vz=0;
      } else if (d12 == d) {
          vx = x1-x; vy = y2-y; vz=0;
      } else if (d21 == d) {
          vx = x2-x; vy = y1-y; vz=0;
      } else if (d22 == d) {
          vx = x2-x; vy = y2-y; vz=0;
      } else {
          fprintf(stderr,"error in cylinder_remesh: point location failed\n");
      }


      

      /* Project this direction into the glide plane */
      if ((vx*vx+vy*vy+vz*vz)==0) {
         v2x = vx; v2y = vy; v2z = vz;
         alpha = 0;
      } else {
         tmp = vx*nx + vy*ny + vz*nz;
         v2x = vx - tmp*nx; v2y = vy - tmp*ny; v2z = vz - tmp*nz; 
         alpha1 = (-(v2x*x+v2y*y) + sqrt( (v2x*x+v2y*y)*(v2x*x+v2y*y) - 
                                         (v2x*v2x+v2y*v2y)*(x*x+y*y-Radius*Radius) ))
                  / (v2x*v2x+v2y*v2y);
         alpha2 = (-(v2x*x+v2y*y) - sqrt( (v2x*x+v2y*y)*(v2x*x+v2y*y) -
                                         (v2x*v2x+v2y*v2y)*(x*x+y*y-Radius*Radius) ))
                  / (v2x*v2x+v2y*v2y);
         if ( fabs(alpha1)<fabs(alpha2) ) 
             alpha = alpha1;
         else
             alpha = alpha2;
      }


      

      vx = v2x*alpha; vy = v2y*alpha; vz = v2z*alpha;

      *dx = vx; *dy = vy; *dz = vz;
}

/*-------------------------------------------------------------------------
 *
 *      Function:    cylpoint - BCC
 *      Description: Returns the point on the cylinder "relatively" close to the
 *                   point (x,y,z), subject to the constraint that the point
 *                   stays along the line defined by (x,y,z) and (x2,y2,z2)
 *                   Radius is the cylinder radius.  Cylinder is along z axis.
 *
 *------------------------------------------------------------------------*/
void cylpoint_line(double x,double y,double z,double x2,double y2,double z2,double Radius,
              double *dx,double *dy,double *dz)
{
      double vx, vy, vz, v2x, v2y, v2z;
      double t, A, B, C;

      vx = x - x2; vy = y - y2; vz = z - z2;
      A  = vx*vx + vy*vy;
      B  = 2*(x2*vx + y2*vy);
      C  = x2*x2 + y2*y2 - Radius*Radius;

      /* t is the solution of A*t^2 + B*t + C = 0 */
      t = ( -B+sqrt(B*B-4*A*C) ) / (2*A);

      *dx = x2 + t*vx - x;
      *dy = y2 + t*vy - y;
      *dz = z2 + t*vz - z;
}



/*-------------------------------------------------------------------------
 *
 *      Function:    Cylinder_Remesh_2
 *      Description: Handle node collision with cylindrical surface
 *
 *------------------------------------------------------------------------*/
void Cylinder_Remesh_2(Home_t *home)
{
  int i, j, k, constra, constrb, ninlinksa, ninlinksb, status;
  int armCount, splitStatus, globalOp, armID, *armList;
  double x, y, z, ri, Radius, Delta, nx, ny, nz, dx, dy, dz, bx, by, bz, dr;
  double tmp, nodePos[3], newPos[3], nodeVel[3], newVel[3];
  Node_t *node, *nodea, *nodeb, *nodek, *splitNode1, *splitNode2, *mergedNode;
  Node_t *nbrNode;
  Param_t *param;
  double moveratio;
  

  /* Node constraint :
   * 7 = fixed / no Burgers vector conservation
   * 6 = fixed / on cylinder surface
   * 5 = fixed / outside cylinder
   */
  

   param = home->param;
   Radius = cylinder0->radius;
//   Delta = 0.05; /* hard coded number, move to input file later */
   Delta = 0.1*param->minSeg; /* hard coded number, move to input file later */
   moveratio = 0.8;

  /* Go through all nodes, if the nodes flag is 6, and project this node
     to the surface
   */
   for (i = 0; i < home->newNodeKeyPtr; i++) 
     {
       node = home->nodeKeys[i];
       if (node == (Node_t *)NULL) continue;
       if (node->constraint != 6) continue;

       x = node->x; y = node->y; z = node->z;
       nx = node->nx[0]; ny = node->ny[0]; nz = node->nz[0];
       ri = sqrt(x*x+y*y);

      /* old projection on glide plane */
        cylpoint(x,y,z,nx,ny,nz,Radius,&dx,&dy,&dz);
#if 1
      /* find the neighboring node of this node */
        for (j = 0; j < node->numNbrs; j++) {
           nbrNode = GetNeighborNode(home, node, j);
           if (nbrNode->constraint == 0) {
              cylpoint_line(x,y,z,nbrNode->x,nbrNode->y,nbrNode->z,Radius,&dx,&dy,&dz);
              break;
           }
        }
#endif
      
        /* debug */
        ri = sqrt(dx*dx+dy*dy+dz*dz);
        if (ri > Radius)
        {
            printf("[%e,%e,%e]=cylpoint(%e,%e,%e,%e,%e,%e,%e)\n",dx,dy,dz,x,y,z,nx,ny,nz,Radius);
            exit(-1);
        }
     
        node->x += dx;
        node->y += dy;
        node->z += dz;
        node->constraint = 6;
   }


   
 
  /* Go through all nodes, if node gets close enough to the surface,
   * move it to the surface and change flag to 6
   */
   for (i = 0; i < home->newNodeKeyPtr; i++) {

        node = home->nodeKeys[i];
        if (node == (Node_t *)NULL) continue;

        x = node->x; y = node->y; z = node->z;
        nx = node->nx[0]; ny = node->ny[0]; nz = node->nz[0];
        ri = sqrt(x*x+y*y);
        if ( (Radius - ri <= Delta*Radius) && (node->constraint == 0) ) {
              cylpoint(x,y,z,nx,ny,nz,Radius,&dx,&dy,&dz);
#if 1
           /* find the neighboring node of this node */
               for (j = 0; j < node->numNbrs; j++) {
                  nbrNode = GetNeighborNode(home, node, j);
                  if (nbrNode->constraint == 0) {
                     cylpoint_line(x,y,z,nbrNode->x,nbrNode->y,nbrNode->z,
				   Radius,&dx,&dy,&dz);
                     break;
                  }
               }
#endif
              /* debug */
              ri = sqrt(dx*dx+dy*dy+dz*dz);
              if (ri > Radius)
              {
                  printf("[%e,%e,%e]=cylpoint(%e,%e,%e,%e,%e,%e,%e)\n",dx,dy,dz,x,y,z,nx,ny,nz,Radius);
                  exit(-1);
              }
              node->x += dx;
              node->y += dy;
              node->z += dz;
              node->constraint = 6;
              printf("******************************************************\n");
              printf("node (%d,%d) put on surface dr = (%f,%f,%f) r = (%f,%f,%f)\n",
                     node->myTag.domainID,node->myTag.index,dx,dy,dz,
                     node->x,node->y,node->z);
              printf("******************************************************\n");
        }
   }

   /* Remove any link between two surface nodes (flag == 6)
    */
   for (i = 0; i < home->newNodeKeyPtr; i++) {

        nodea = home->nodeKeys[i];
        if (nodea == (Node_t *)NULL) continue;

        constra = nodea->constraint;
        for (j = 0; j < nodea->numNbrs; j++) {

            nodeb = GetNeighborNode(home, nodea, j); 
            constrb = nodeb->constraint;

            if(OrderNodes(nodea,nodeb)>0) continue;

            nx = nodea->nx[j]; ny = nodea->ny[j]; nz = nodea->nz[j];
            bx = nodea->burgX[j]; by = nodea->burgY[j]; bz = nodea->burgZ[j];

            //printf("j = %d constra = %d constrb = %d\n",j,constra,constrb);

            if ( (constra == 6) && (constrb == 6) ) {
              /* Count the number of links inside the cylinder */ 

              /* Now apply remesh rules on two connected nodes on the surface */             
                  globalOp = 1;
                  ChangeArmBurg(home, nodea, &nodeb->myTag, 0, 0, 0, 0, 0, 0,
#ifdef _STACKINGFAULT
                                      0.0, 0.0, 0.0,
#endif
                                globalOp, DEL_SEG_HALF);
                  ChangeArmBurg(home, nodeb, &nodea->myTag, 0, 0, 0, 0, 0, 0,
#ifdef _STACKINGFAULT
                                      0.0, 0.0, 0.0,
#endif
                                globalOp, DEL_SEG_HALF);
            }
        }
   }

   /* Remove any nodes with zero neighbors
    */
   for (i = 0; i < home->newNodeKeyPtr; i++) {

        nodea = home->nodeKeys[i];
        if (nodea == (Node_t *)NULL) continue;

        if (nodea->numNbrs == 0)
            RemoveNode(home, nodea, globalOp);
   }
   
}
