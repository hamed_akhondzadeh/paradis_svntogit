/*-------------------------------------------------------------------------
 *
 *      Function:     ParadisStep
 *      Description:  This function controls everything needed for a
 *                    single step of a ParaDiS simulation including
 *                    force calculations, ghost cell communications,
 *                    node migration, dynamic load balance, output
 *                    generation, etc.
 *
 *-----------------------------------------------------------------------*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "Home.h"
#include "Util.h"
#include "DisplayC.h"
#include "Comm.h"
#include "Mobility.h"
#include "Decomp.h"
#include "QueueOps.h"

#if defined _FEM | defined _FEMIMGSTRESS
#include "FEM.h"
#endif

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef CYL
#include "CYL.h"
#endif

/*
 *      By default, there are no runtime checks to see if all of
 *      the dislocations have annihilated themselves.  To enable
 *      a check with an abort if it happens, simply define the
 *      DEBUG_CHECK_FOR_ZERO_SEG value below to 1 rather than zero.
 */
#define DEBUG_CHECK_FOR_ZERO_SEG 0

void  LoadCurve(Home_t *home, real8 deltaStress[3][3]);


#if 0
/*
 * This is just a debug routine to dump each domains forces to a file
 */
static void DumpForces(Home_t *home, int sequence)
{
        int    i, j;
        char   fname[128];
        FILE   *fp;
        Node_t *node;

        sprintf(fname, "nodeforce.%d.%06d.%d", home->myDomain, home->cycle+1, sequence);
        fp = fopen(fname,"w");

        for (i = 0; i < home->newNodeKeyPtr; i++) {

            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }

/*
            fprintf(fp,"  %.15e %.15e %.15e  (%d,%d) position\n",
                    node->x, node->y, node->z,
                    node->myTag.domainID, node->myTag.index);
*/

            fprintf(fp,"  %.15e %.15e %.15e  (%d,%d) force\n",
                    node->fX, node->fY, node->fZ,
                    node->myTag.domainID, node->myTag.index);

        }

#if 0
        node = home->ghostNodeQ;
        while (node) {

            fprintf(fp,"  %.15e %.15e %.15e  (%d,%d) position\n",
                    node->x, node->y, node->z,
                    node->myTag.domainID, node->myTag.index);

            fprintf(fp,"  %.15e %.15e %.15e  (%d,%d) force\n",
                    node->fX, node->fY, node->fZ,
                    node->myTag.domainID, node->myTag.index);

            node = node->next;
        }
#endif

        fclose(fp);

        return;
}
#endif


#if 0
/*
 *      Just another debug function... This one will check the length
 *      of every segment owned by the current domain and print out an
 *      error message any time it finds a segment more than 10% longer
 *      than the maxSeg length.
 */
void CheckSegLengths(Home_t *home, char *msg)
{
        int    i, j;
        real8  xLen, yLen, zLen, totLen;
        Node_t *node1, *node2;

        for (i = 0; i < home->newNodeKeyPtr; i++) {

            if ((node1 = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }

            for (j = 0; j < node1->numNbrs; j++) {

                node2 = GetNeighborNode(home, node1, j);

                if (node2 == (Node_t *)NULL) {
                    printf("%s:  Segment (%d,%d)--(%d,%d) Cannot get neighbor\n",
                           msg, node1->myTag.domainID, node1->myTag.index,
                           node1->nbrTag[j].domainID, node1->nbrTag[j].index);
                    fflush(NULL);
                    continue;
                }

                xLen = node2->x - node1->x;
                yLen = node2->y - node1->y;
                zLen = node2->z - node1->z;

                ZImage(home->param, &xLen, &yLen, &zLen);

                totLen = sqrt(xLen*xLen + yLen*yLen + zLen*zLen);

                if (totLen > (home->param->maxSeg * 1.1)) {
                    printf("%s:  Segment (%d,%d)--(%d,%d) length = %lf\n",
                           msg, node1->myTag.domainID, node1->myTag.index,
                           node2->myTag.domainID, node2->myTag.index, totLen);
                    fflush(NULL);
                }
            }
        }

        return;
}
#endif


/*
 *      For debugging only.  If DEBUG_STEP is not defined, all
 *      calls to Synchronize() will be replaced with an empty
 *      block of code, but if it is defined, the calls will
 *      be replaced with a call to syncronize the code and log
 *      a message.
 */
#ifdef DEBUG_STEP
#define Synchronize(a,b) _Synchronize((a),(b))
#else
#define Synchronize(a,b) {}
#endif

/*
 *      Explicitly synchronize parallel execution via an MPI
 *      barrier and print a message when all tasks have reached
 *      the barrier.  For debug only.
 */
void _Synchronize(Home_t *home, char *msg)
{

#ifdef PARALLEL
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        if (home->myDomain == 0) {
            printf(" *** %s: All tasks synchronized\n", msg);
            fflush(NULL);
        }
}

#if DEBUG_CHECK_FOR_ZERO_SEG
/*
 *      Just a sanity check to kill the simulation if we
 *      end up in a situation with no more dislocations in the
 *      problem.  This is really only for debugging and testing.
 */
void CheckForEmptySimulation(Home_t *home)
{
        int    i, localNodeCnt, globalNodeCnt;
        Node_t *node;

        localNodeCnt = 0;
        globalNodeCnt = 0;

        for (i = 0; i < home->newNodeKeyPtr; i++) {
            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            } else {
                localNodeCnt++;
            }
        }

#ifdef PARALLEL
        MPI_Reduce(&localNodeCnt, &globalNodeCnt, 1, MPI_INT, MPI_SUM,
                    0, MPI_COMM_WORLD);
#else
        globalNodeCnt = localNodeCnt;
#endif
        if ((home->myDomain == 0) && (globalNodeCnt == 0)) {
            Fatal("All dislocations in simulation have been "
                  "annihilated.\nSimulation terminating NOW!");
        }
}
#endif


#ifdef NAN_CHECK
/*
 *      Just a sanity check to determine if the position or velocity
 *      of any node contains NaNs or infinite values.  If any are
 *      found, the code will abort... hopefully this check is done
 *      *before* the bad data is written to a restart file.
 */
void CheckForNANS(Home_t *home)
{
        int    i;
        Node_t *node;

        for (i = 0; i < home->newNodeKeyPtr; i++) {

            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }

            if (isnan(node->x) ||
                isnan(node->y) ||
                isnan(node->z)) {
                PrintNode(node);
                Fatal("Node position contains NaNs.  Aborting!");
            }
             
            if (isinf(node->x) ||
                isinf(node->y) ||
                isinf(node->z)) {
                PrintNode(node);
                Fatal("Node position contains NaNs.  Aborting!");
            }
             
            if (isinf(node->vX) ||
                isinf(node->vY) ||
                isinf(node->vZ)) {
                PrintNode(node);
                Fatal("Node velocity contains infinites.  Aborting!");
            }
             
            if (isinf(node->vX) ||
                isinf(node->vY) ||
                isinf(node->vZ)) {
                PrintNode(node);
                Fatal("Node velocity contains infinites.  Aborting!");
            }
             
        }

        return;
}
#endif  /* ifdef NAN_CHECK */


/*-------------------------------------------------------------------------
 *
 *      Function:    ApplyDeltaStress
 *      Description: Increment the force/vel for all native nodes based
 *                   on the change in applied stress (deltaStress)
 *                   during a timestep.  Since the applied stress plays
 *                   no impact on the segment/segment interactions this
 *                   is much less expensive than recomputing all the n^2
 *                   segment interactions.
 *
 *------------------------------------------------------------------------*/
static void ApplyDeltaStress(Home_t *home, real8 deltaStress[3][3])
{
        int     i, j, nbrArm, nbrIsLocal;
        real8   x1, y1, z1, x2, y2, z2;
        real8   bx1, by1, bz1, dx, dy, dz;
        real8   f1[3], f2[3];
        Node_t  *node, *nbr;
        Param_t *param;
        
        param = home->param;

/*
 *      Loop over all native nodes
 */        
        for (i = 0; i < home->newNodeKeyPtr; i++) {

            node = home->nodeKeys[i];
            if (!node) continue;
        
            x1=node->x;
            y1=node->y;
            z1=node->z;
        
/*
 *          For each node, recalculate forces for all the node's arms
 *          that are either owned by this node, or terminate non-locally.
 */
            for (j = 0; j < node->numNbrs; j++) {

                nbr = GetNeighborNode(home, node, j);
                nbrIsLocal = (nbr->myTag.domainID == home->myDomain);

                if (nbrIsLocal) {
                    if (OrderNodes(node, nbr) >= 0) continue;
                    nbrArm = GetArmID(home, nbr, node);
                }
        
                bx1 = node->burgX[j];
                by1 = node->burgY[j];
                bz1 = node->burgZ[j];
        
                dx=nbr->x-x1;
                dy=nbr->y-y1;
                dz=nbr->z-z1;
        
                ZImage(param, &dx, &dy, &dz) ;
        
                x2=x1+dx;
                y2=y1+dy;
                z2=z1+dz;
        
                ExtPKForce(deltaStress, bx1, by1, bz1, x1, y1, z1,
                        x2, y2, z2, f1, f2);
        
                AddtoNodeForce(node,f1);
                AddtoArmForce(node, j, f1);
        
                if (nbrIsLocal) {
                    AddtoNodeForce(nbr, f2);
                    AddtoArmForce(nbr, nbrArm, f2);
                }
            }

            (void)EvaluateMobility(home, node);
        }

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    ReevaluateForces
 *      Description: Look for any local nodes whose force/velocity data
 *                   have been flagged as obsolete.  For all such nodes
 *                   recompute the forces and velocities.
 *
 *------------------------------------------------------------------------*/
#ifdef _CYL
void ReevaluateForces(Home_t *home, Cylinder_t *cylinder)
#else
void ReevaluateForces(Home_t *home)
#endif
{
        int     i;
        Node_t  *node;
        Param_t *param;

        param = home->param;

        for (i = 0; i < home->newNodeKeyPtr; i++) {
            node = home->nodeKeys[i];
            if (node == (Node_t *)NULL) continue;
            if (node->flags & NODE_RESET_FORCES) {

#ifdef _CYL
	        SetOneNodeForce(home, cylinder, node);
#else
                SetOneNodeForce(home, node);
#endif
                EvaluateMobility(home, node);

                node->flags &= (~NODE_RESET_FORCES);
            }
        }

        return;
}

#ifdef _CYL
void ParadisStep(Home_t *home, Cylinder_t *cylinder)
#else
void ParadisStep(Home_t *home)
#endif
{
        int        maxMem, localForcesOK;
        int        i, j;
        real8      maxDeltaT;
        real8      deltaStress[3][3];
        Param_t    *param;
        Node_t     *node;
        double     time1, time2;
        static int cycleEnd = 0;
        static int firstTime = 1;
	FMLayer_t *layer;

        param = home->param;

/*
 *      If this step is an initial load-balance-only step just
 *      perform the minimal work needed to estimate per-process
 *      load, shift boundaries, and migrate nodes among processors.
 */
        if (param->numDLBCycles > 0) {
/*
 *          Note:  When param->numDLBCycles > 0, NodeForce() assumes
 *          the cycle is a DLB-only cycle and only counts the number
 *          of force calcs that would be done without actually calculating
 *          any forces.
 */
#ifdef _CYL
	    NodeForce(home,cylinder);
#else
            NodeForce(home);
#endif
            Rebalance(home, DLB_USE_FORCECALC_COUNT);

/*
 *          Any time the boundaries are changed, we need to migrate
 *          nodes to their new owning domains and go through all the
 *          ghost node communication stuff.
 */
            Migrate(home);
            RecycleGhostNodes(home);
            SortNativeNodes(home);
            CommSendGhosts(home);

            home->cycleForceCalcCount = 0;

            return;
        }

/*
 *      First real (i.e. non-DLB-only) cycle, calculate the final cycle number
 */
        if (cycleEnd == 0) {
           cycleEnd = home->param->cycleStart + home->param->maxstep;
        }


#ifndef NO_XWINDOW
        while (WinIsPaused()) {
            sleep(1);
        }
#endif

/*
 *      Calculate the net charge tensor for each cell (includes global comm)
 */
        CellCharge(home);

/*
 *      Calculate new force and velocity data for all nodes or a selected
 *      subset and distribute the new data out to neighboring domains.
 *      The first cycle we'll recalculate all forces and velocities
 *      We do this to get an initial estimate of forces on the first cycle,
 *      After that, we only need to recompute values for nodes that were
 *      involved in topological changes the previous step.
 */
        if (firstTime) {
#ifdef _CYL
	    NodeForce(home, cylinder);
#else
            NodeForce(home);
#endif
            CalcNodeVelocities(home, 1);
            CommSendVelocity(home);
            firstTime = 0;
        } else {
#ifdef _CYL
	  PartialForces(home, cylinder);
#else
            PartialForces(home);
#endif
            CalcNodeVelocities(home, 0);
            CommSendVelocity(home);
        }

/*
 *      Invoke the selected time step integration method.  The
 *      selected method will calculate the time step as well as
 *      move nodes to their correct locations and communicate 
 *      the new nodal force/velocity data to neighboring domains.
 */

        if (strcmp(param->timestepIntegrator, "forward-euler") == 0) {
#ifdef _CYL
	    ForwardEulerIntegrator(home,cylinder);
#else
            ForwardEulerIntegrator(home);
#endif
        } else if (strcmp(param->timestepIntegrator, "trapezoid") == 0) {
#ifdef _CYL
            TrapezoidIntegrator(home,cylinder);
#else
            TrapezoidIntegrator(home);
#endif
        } else {
/*
 *          Used to be specified as 'backard-euler', so if integration
 *          method is unrecognized, use trapezoid as default
 */
#ifdef _CYL
	  TrapezoidIntegrator(home,cylinder);
#else
	  TrapezoidIntegrator(home);
#endif
        }
/*
 *      Increment the per-burgers vector density gain/loss with
 *      changes for this cycle.  This must be done immediately
 *      after timestep integration!
 *
 *      Note: This is currently only applicable to BCC simulations.
 */
        GetDensityDelta(home);

/*
 *      Calculate the new plastic strain.
 */
        DeltaPlasticStrain(home);

/*
 *      This subroutine controls (discrete) cross-slip event to
 *      determine the glide plane for next time step. Only relevant
 *      for FCC system. 
 */
#if 0 /* obsolete, replaced by CrossslipFCC, see below */
        if ((param->mobilityType == MOB_FCC_2) &&
             (param->mob_cross_slip && param->mob_compute_escaig)) {
            CrossSlip(home);
        }
#endif

/*
 *      Before doing topological changes, set flags indicating any
 *      nodes exempt from topological changes.  These flags are used
 *      in both splitting multi-arm nodes and collisions, so this
 *      function should be invoked before either of those items are done.
 */
        InitTopologyExemptions(home);

/*
 *      Now do all the topological changes from segment interactions
 *      (collisions, multinode splitting)...  Clear the list of local
 *      operations that will be sent to the remote domains for processsing,
 *      then split any multi-arm nodes that need splitting, cross slip
 *      nodes (as needed/allowed), handle all local collisions, then
 *      send remote nodes the list of ops needed to keep their data in sync.
 */
        ClearOpList(home);
        SortNodesForCollision(home);


#ifdef _CYL
	SplitMultiNodes(home,cylinder);
#else
        SplitMultiNodes(home);
#endif


/*
 *      Call the BCC cross-slip function if needed.
 *      FIX ME!  This should eventually be replaced with a generic
 *      cross-slip function that will call an appropriate cross-slip
 *      mechanism regardless of whether it's BCC, FCC, etc...
 */
        if (param->mobilityType == MOB_BCC_GLIDE) {
            if (param->enableCrossSlip) {
                CrossSlipBCC(home);
            }
        }

	

/*
 *      Call the FCC cross-slip function if needed.
 */
        if ( (param->mobilityType == MOB_FCC_0) || (param->mobilityType == MOB_FCC_2) ) {
            if (param->enableCrossSlip) {
                CrossSlipFCC(home);
            }
        }

        HandleCollisions(home);
 
#ifdef PARALLEL
#ifdef SYNC_TIMERS
        TimerStart(home, POST_COLLISION_BARRIER);
        MPI_Barrier(MPI_COMM_WORLD);
        TimerStop(home, POST_COLLISION_BARRIER);
#endif
#endif

        TimerStart(home, COL_SEND_REMESH);
        CommSendRemesh(home);
        TimerStop(home, COL_SEND_REMESH);

        TimerStart(home, COL_FIX_REMESH);
        FixRemesh(home);
        TimerStop(home, COL_FIX_REMESH);

#ifdef _FEM
	AdjustNodePosition(home, 1); 
#endif 

/*
 *      Under certain circumstances, parallel topological changes can
 *      create double links between nodes; links which can not be detected
 *      until after FixRemesh() is called... so, a quick check has to be
 *      done to clean up these potential double-links here, or they will
 *      cause problems later on.  Should only have to check nodes local
 *      to this domain.
 */
        for (i = 0; i < home->newNodeKeyPtr; i++) {
            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
            (void)RemoveDoubleLinks(home, node, 0);
            node->flags &= ~NODE_CHK_DBL_LINK;
        }

/*
 *      If memory debugging is enabled, run a consistency check on all
 *      allocated memory blocks looking for corruption in any of the
 *      block headers or trailers.
 */
#ifdef DEBUG_MEM
        ParadisMemCheck();
#endif
       
/*
 *      Invoke mesh coarsen/refine
 */

#ifdef _CYL
        Remesh(home,cylinder);
#else
        Remesh(home);
#endif

/*
 *      Define load curve and adjust nodal force/velocities to account
 *      for any change in applied stress this cycle.
 */
        LoadCurve(home, deltaStress);

#if 1
/*
 *      This is only needed when we do force calcs for only
 *      a subset of the nodes at the beginning of the timestep.
 */
        ApplyDeltaStress(home, deltaStress);
#endif

/*
 *      If necessary, use the current load data to generate a new
 *      domain decomposition to rebalance the workload among the
 *      processors.
 */
        Rebalance(home, DLB_USE_WALLCLK_TIME);

/*
 *      Send any nodes that have moved beyond the domain's
 *      boundaries to the domain the node now belongs to.
 */
        Migrate(home);

/*
 *      Recycle all the ghost nodes: move them back to the free Queue
 */
        RecycleGhostNodes(home);

/*
 *      Sort the native nodes and into their proper
 *      subcells.   Note: this sort and the following ghost cell
 *      communications have been moved to the end of the ParadisStep
 *      loop because the output generation functions (called at the
 *      end of this subroutine) depend on the ghost cell data being
 *      accurate, but the Dynamic load balance and migration process
 *      above cleared the ghost cell data.
 */
        SortNativeNodes(home);

/*
 *      Communicate ghost cells to/from neighbor domains
 */
        CommSendGhosts(home);

#ifdef NAN_CHECK
/*
 *      For debug only:  Abort if any of the nodes have position or
 *      velocity values that are NaNs or infinites.  Be sure to do this
 *      before we write the restart files so we don't get bad data
 *      in the restart.
 */
        CheckForNANS(home);
#endif

/*
 *      The call to GenerateOutput will update the time and cycle counters,
 *      determine if any output needs to be generated at this stage, and
 *      call the appropriate I/O functions if necessary.
 */
        GenerateOutput(home, STAGE_CYCLE);

/*
 *      If memory debugging is enabled, run a consistency check on all
 *      allocated memory blocks looking for corruption in any of the
 *      block headers or trailers.
 */
#ifdef DEBUG_MEM
        ParadisMemCheck();
#endif

        CheckMemUsage(home, "ParadisStep-complete");

/*
 *      Zero out the count of force calculations done this cycle
 *      so the load-balancing in the next step is based on accurate
 *      values.
 */
        home->cycleForceCalcCount = 0;

/*
 *      For Debugging and testing only...
 */
#if DEBUG_CHECK_FOR_ZERO_SEG
        CheckForEmptySimulation(home);
#endif

        return;
}
