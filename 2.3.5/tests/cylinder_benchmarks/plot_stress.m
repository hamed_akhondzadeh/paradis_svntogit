% plot stress field on a regular mesh computed by ParaDiS


data1 = load('InfStress00_SpectralMethod1.out'); 
data2 = load('InfStressPBC_SpectralMethod1.out');

ND = length(data1);
N = sqrt(ND);  M = N;
ind_shift = [floor(N/2)+2:N,1:floor(N/2)+1];
x = reshape(data1(:,1),N,M);
y = reshape(data1(:,2),N,M);
z = reshape(data1(:,3),N,M);

for i=1:6,
    stress1 = reshape(data1(:,i+3),N,M);
    stress2 = reshape(data2(:,i+3),N,M);
    ind_plot=floor(N/2)+0;
    figure(3); subplot(2,3,i);
    %plot([1:M],stress1(ind_plot,:),'b-',[1:M],fftshift(stress2(ind_plot,:)),'r--');
    plot([1:M],stress1(ind_plot,:),'b-',[1:M],stress2(ind_plot,ind_shift),'r--');
    xlim([1 M]);
end

if (1)
for i=1:6,
    stress1 = reshape(data1(:,i+3),N,M);
    stress2 = reshape(data2(:,i+3),N,M);
    figure(1); subplot(2,3,i); mesh(stress1); xlim([0 M]); ylim([0 N]);
    figure(2); subplot(2,3,i); mesh(stress2(:,ind_shift)); xlim([0 M]); ylim([0 N]);
end
end