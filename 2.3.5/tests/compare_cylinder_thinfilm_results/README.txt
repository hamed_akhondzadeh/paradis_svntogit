In this directory, the .txt files correspond to different options in the image stress method for the TF.

force_NoImg_NoYoff_No_VS.txt is the force output when the spectral method is off, Yoffe method is off and virtual segments are off.
force_NoImg_NoYoff_Yes_VS.txt is the force output when the spectral method is off, Yoffe method is off and virtual segments are on.
force_NoImg_YesYoff_Yes_VS.txt is the force output when the spectral method is off, Yoffe method is on and virtual segments are on.
force_YesImg_YesYoff_Yes_VS.txt  is the force output when the spectral method is on, Yoffe method is on and virtual segments are on.

Sylvie Aubry
Thu Jun 18 2009
