/***************************************************************************
 *
 *  Function    : Main
 *  Description : main routine for ParaDiS simulation
 *
 *  To do: Remove hard coded paths in CompareNodalForces
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "Home.h"
#include "Init.h"
#include "Param.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef FPES_ON
#include <fpcontrol.h>
#endif

#define norm2(x,y,z) (x*x+y*y+z*z)
#define norm(x,y,z) ( pow(x*x+y*y+z*z,0.5) )

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

#define NUMCOLUMNS    9
#define LOADBLOCKSIZE 10000
#define MAXPATHLENGTH 256
#define MAXGROUPNUM   5
#define MAXLINELEN    1000

#define REFDATADIR    "ref_results"
#define USRDATADIR    "test_results/properties"

#define FULLDATAFILE  "%s/NodeForce_FULL.out"
#define GROUPDATAFILE "%s/NodeForce_GROUP%d.out"

#define TEST1_tol 1e-7
#define TEST2_tol 1e-7
#define TEST3_tol 1e-7

#define MIN_V_mag 1e-2
#define MIN_F_mag 1e-5

/**********************************************************
 * Function: ReadData
 * Description: Reads Serial (Refrence) and Parallel(test or user) nodal info
 * Format of the table should be
 * [Node.xCoor Node.yCoor Node.zCoor Node.Fx Node.Fy Node.Fz Node.Vx Node.Vy Node.Vz]
 ***********************************************************/
void ReadData(const char *TablePath, double ***CoorFVel, int *nRows){
    int i,j, EndOfFile, MaxSize;
    FILE    *Table;
    char line[MAXLINELEN];

    Table=fopen(TablePath,"r");

    if (Table == NULL) {
        fprintf(stderr,"Cannot open file %s\n", TablePath);
    }
    *CoorFVel=(double **)malloc(LOADBLOCKSIZE*sizeof(double *));
    
    EndOfFile = 0;
    i = 0;
    MaxSize = LOADBLOCKSIZE;
    while (!EndOfFile) {
        (*CoorFVel)[i]=(double *)malloc(NUMCOLUMNS*sizeof(double));

        for (j=0; j<NUMCOLUMNS-1; j++){
            if (fscanf(Table,"%lf",&((*CoorFVel)[i][j])) == -1){
        	EndOfFile = 1;
        	break;
            }
        }
		
        /* skip the rest of line */
        if (fscanf(Table,"%lf\n",&((*CoorFVel)[i][NUMCOLUMNS-1])) == -1){
        	EndOfFile = 1;
        }

        i++;
        if (i>=MaxSize-1){
            MaxSize+=LOADBLOCKSIZE;
            *CoorFVel= (double **)realloc(*CoorFVel,(MaxSize)*sizeof(double **));
        }
		
    }
    printf("ReadData: i = %d\n", i);
    *nRows=i-1;	
    printf("First row = %e %e %e %e %e %e %e %e %e \n",(*CoorFVel)[0][0],
			(*CoorFVel)[0][1],(*CoorFVel)[0][2],(*CoorFVel)[0][3],(*CoorFVel)[0][4],
			(*CoorFVel)[0][5],(*CoorFVel)[0][6],(*CoorFVel)[0][7],(*CoorFVel)[0][8]);	
    printf("Last  row = %e %e %e %e %e %e %e %e %e\n\n",(*CoorFVel)[*nRows-1][0],
			(*CoorFVel)[*nRows-1][1],(*CoorFVel)[*nRows-1][2],(*CoorFVel)[*nRows-1][3],
			(*CoorFVel)[*nRows-1][4],(*CoorFVel)[*nRows-1][5],(*CoorFVel)[*nRows-1][6],
			(*CoorFVel)[*nRows-1][7],(*CoorFVel)[*nRows-1][8]);	
			
    fclose(Table);
}



/**********************************************************
 * Function: CompareNodalForces
 * Description: Compares the value of nodal forces in the subcell 
 * case with the original simulation and returns the 
 * value of maximum difference.
 ***********************************************************/
int CompareNodalForces(){
	
	double ForceMag, V_Mag;
	char path[MAXPATHLENGTH];
	double **Forces_full_Ref, **Forces_full_user;
	int nNodes_full_Ref, nNodes_full_user;
	double ***Forces_GROUP_Ref, ***Forces_GROUP_user;
	int *nNodes_GROUP_Ref, *nNodes_GROUP_user;
	int i,j;
	real8 Diff[3];

	printf("Full_Usr_Data:\n");
	sprintf(path, FULLDATAFILE, USRDATADIR);				
	ReadData(path, &Forces_full_user, &nNodes_full_user);
		
	printf("Full_Ref_Data:\n");
	sprintf(path, FULLDATAFILE, REFDATADIR);				
	ReadData(path, &Forces_full_Ref, &nNodes_full_Ref);
				

	
    /* It is also possible to save nRows from first call to ReadData and 
	 * use that value for future calls to this
     * function. For now,this was not implemented.
     */

	Forces_GROUP_Ref=(double ***)malloc(MAXGROUPNUM*sizeof(double **));
	nNodes_GROUP_Ref = (int *)malloc(MAXGROUPNUM*sizeof(int ));
	
	Forces_GROUP_user=(double ***)malloc(MAXGROUPNUM*sizeof(double **));
	nNodes_GROUP_user = (int *)malloc(MAXGROUPNUM*sizeof(int ));
	
	for (j=0; j<MAXGROUPNUM; j++){	
	    printf("Group%d_Ref_Data:\n",j);
	    sprintf(path, GROUPDATAFILE, REFDATADIR, j);				
	    ReadData(path, Forces_GROUP_Ref + j, nNodes_GROUP_Ref + j);

	    printf("Group%d_Usr_Data:\n",j);
	    sprintf(path, GROUPDATAFILE, USRDATADIR, j);				
	    ReadData(path, Forces_GROUP_user + j, nNodes_GROUP_user + j);
	}
		
	//Sanity Check: if the number of nodes in all of the tables are the same.
	 
	if ( ( nNodes_full_Ref      != nNodes_full_user)  ||    
		 ( nNodes_GROUP_user[0] != nNodes_full_user ) ||
		 ( nNodes_GROUP_user[1] != nNodes_full_user ) ||
		 ( nNodes_GROUP_user[2] != nNodes_full_user ) || 
		 ( nNodes_GROUP_user[3] != nNodes_full_user ) ||
		 ( nNodes_GROUP_user[4] != nNodes_full_user ) ) {
			printf("Sanity check"RED" FAILED" RESET "\n"); 
			printf("Number of nodes is different than the refrence table!!!\n"
				"Check if your data file is the same as the reference file\n\n");
	}
		 
	int MapParToSer[nNodes_full_Ref];
	for (i=0; i<nNodes_full_user; i++){
		for (j=0; j<nNodes_full_Ref; j++){
			Diff[0]=Forces_full_Ref[j][0]-Forces_full_user[i][0];
			Diff[1]=Forces_full_Ref[j][1]-Forces_full_user[i][1];
			Diff[2]=Forces_full_Ref[j][2]-Forces_full_user[i][2];
			if ( norm2(Diff[0],Diff[1],Diff[2])<1e-10 ){
				MapParToSer[i]=j;
				break;
			}
		}
	}		 
		 
		 
	//test1: check if the sum of the subgroup forces, is equal to the full
	printf("\n");
    printf("*****************************************************************\n");
    printf("Test1 checks if the sum of subgroup forces equal to total forces.\n");
    printf("*****************************************************************\n");

	double MaxErr_F=0.0;
	int MaxErr_F_nodeID_user; //this is actually row number not nodeID
	double Err_F, MaxErr_F_rel;
	double MaxErr_F_Vec[3];
	
	int test1Violation_count=0;
	
	for (i=0; i<nNodes_full_user; i++){
		double ForceDiff[3];
		for (j=0; j<3; j++){
			ForceDiff[j] = 
			Forces_GROUP_user[0][i][j+3]+Forces_GROUP_user[1][i][j+3]+
			Forces_GROUP_user[2][i][j+3]+Forces_GROUP_user[3][i][j+3]+
			Forces_GROUP_user[4][i][j+3]-Forces_full_user[i][j+3];
		}
		
		Err_F=norm2(ForceDiff[0],ForceDiff[1],ForceDiff[2]);
		if ( Err_F > MaxErr_F){
			MaxErr_F = norm2(ForceDiff[0],ForceDiff[1],ForceDiff[2]);
			MaxErr_F_Vec[0]= ForceDiff[0];
			MaxErr_F_Vec[1]= ForceDiff[1];
			MaxErr_F_Vec[2]= ForceDiff[2];
			
			MaxErr_F_nodeID_user=i;
		}
		
		if ( sqrt(Err_F)>1e9){
			test1Violation_count++;
			
			// printf("MaxDiff between " BLU "forces " RESET "obtained from Full and groups is %e \n"
			// "This Difference is associated with row user=%d ref=%d\n",sqrt(MaxErr_F),
								// i, MapParToSer[i]);
								
			// printf("Forces_GROUP1_user: %e %e %e %e %e %e \n\n",Forces_GROUP_user[1][i][0],
			// Forces_GROUP_user[1][i][1],Forces_GROUP_user[1][i][2],
			// Forces_GROUP_user[1][i][3],Forces_GROUP_user[1][i][4],
			// Forces_GROUP_user[1][i][5]);
		
		}
	}
	MaxErr_F_rel= MaxErr_F / norm2(Forces_full_user[ MaxErr_F_nodeID_user ][3],
							Forces_full_user[ MaxErr_F_nodeID_user ][4],
							Forces_full_user[ MaxErr_F_nodeID_user ][5]);

	if 	(MaxErr_F_rel < TEST1_tol){
		printf("test1"GRN" PASSED" RESET"\n");
	} else {
		printf("test1Violation_count=%d\n",test1Violation_count);
		printf("test1"RED" FAILED" RESET "\n");
		printf("MaxDiff between " BLU "forces " RESET "obtained from Full and groups is %e \n"
		 "This Difference is associated with row user=%d ref=%d\n",sqrt(MaxErr_F),
								MaxErr_F_nodeID_user, MapParToSer[MaxErr_F_nodeID_user]);
								
		printf("Forces_full_user  : %e %e %e %e %e %e \n",Forces_full_user[MaxErr_F_nodeID_user][0],
		Forces_full_user[MaxErr_F_nodeID_user][1],Forces_full_user[MaxErr_F_nodeID_user][2],
		Forces_full_user[MaxErr_F_nodeID_user][3],Forces_full_user[MaxErr_F_nodeID_user][4],
		Forces_full_user[MaxErr_F_nodeID_user][5]);
		
		printf("Forces_GROUP0_user: %e %e %e %e %e %e \n",Forces_GROUP_user[0][MaxErr_F_nodeID_user][0],
		Forces_GROUP_user[0][MaxErr_F_nodeID_user][1],Forces_GROUP_user[0][MaxErr_F_nodeID_user][2],
		Forces_GROUP_user[0][MaxErr_F_nodeID_user][3],Forces_GROUP_user[0][MaxErr_F_nodeID_user][4],
		Forces_GROUP_user[0][MaxErr_F_nodeID_user][5]);
		
		printf("Forces_GROUP1_user: %e %e %e %e %e %e \n",Forces_GROUP_user[1][MaxErr_F_nodeID_user][0],
		Forces_GROUP_user[1][MaxErr_F_nodeID_user][1],Forces_GROUP_user[1][MaxErr_F_nodeID_user][2],
		Forces_GROUP_user[1][MaxErr_F_nodeID_user][3],Forces_GROUP_user[1][MaxErr_F_nodeID_user][4],
		Forces_GROUP_user[1][MaxErr_F_nodeID_user][5]);
		
		printf("Forces_GROUP2_user: %e %e %e %e %e %e \n",Forces_GROUP_user[2][MaxErr_F_nodeID_user][0],
		Forces_GROUP_user[2][MaxErr_F_nodeID_user][1],Forces_GROUP_user[2][MaxErr_F_nodeID_user][2],
		Forces_GROUP_user[2][MaxErr_F_nodeID_user][3],Forces_GROUP_user[2][MaxErr_F_nodeID_user][4],
		Forces_GROUP_user[2][MaxErr_F_nodeID_user][5]);
		
		printf("Forces_GROUP3_user: %e %e %e %e %e %e \n",Forces_GROUP_user[3][MaxErr_F_nodeID_user][0],
		Forces_GROUP_user[3][MaxErr_F_nodeID_user][1],Forces_GROUP_user[3][MaxErr_F_nodeID_user][2],
		Forces_GROUP_user[3][MaxErr_F_nodeID_user][3],Forces_GROUP_user[3][MaxErr_F_nodeID_user][4],
		Forces_GROUP_user[3][MaxErr_F_nodeID_user][5]);
		
		printf("Forces_GROUP4_user: %e %e %e %e %e %e \n",Forces_GROUP_user[4][MaxErr_F_nodeID_user][0],
		Forces_GROUP_user[4][MaxErr_F_nodeID_user][1],Forces_GROUP_user[4][MaxErr_F_nodeID_user][2],
		Forces_GROUP_user[4][MaxErr_F_nodeID_user][3],Forces_GROUP_user[4][MaxErr_F_nodeID_user][4],
		Forces_GROUP_user[4][MaxErr_F_nodeID_user][5]);	
		
		printf("ForceDiffMax= %e %e %e \n\n",MaxErr_F_Vec[0],MaxErr_F_Vec[1],MaxErr_F_Vec[2] );	
		
		printf("~~~~~~~~~~ CHECK IF sendSubGroup EQUALS 1  ~~~~~~~~~ \n\n");		
	}
	
	
	//test2: compare the forces & velocities obtained from LocalSegForce(full) with the reference values.
	printf("\n");
        printf("***********************************************************************\n");
        printf("Test2 compares the FULL forces and velocities against reference values.\n");
        printf("***********************************************************************\n");
	double Err_V = 0.0;
	   	   Err_F = 0.0;
		   MaxErr_F=0.0;
	double MaxErr_V=0.0;
	double MaxErr_V_Vec[3];
		   MaxErr_F_Vec[0]=0.0; MaxErr_F_Vec[1]=0.0; MaxErr_F_Vec[2]=0.0; 
		   MaxErr_F_nodeID_user=0;
	int	   MaxErr_V_nodeID_user=0;
	int    MaxErr_F_nodeID_Ref=0;
	int	   MaxErr_V_nodeID_Ref=0;
		   MaxErr_F_rel=0.0 ;
	double MaxErr_V_rel=0.0	; 
	int RefNode;
	double Err_Coor;
	
	for (i=0; i<nNodes_full_user; i++){
		RefNode=MapParToSer[i];
		Err_Coor=norm2(Forces_full_user[i][0]-Forces_full_Ref[RefNode][0] , 
					   Forces_full_user[i][1]-Forces_full_Ref[RefNode][1] ,
					   Forces_full_user[i][2]-Forces_full_Ref[RefNode][2] );
		if 	(Err_Coor > 1e-10){
			printf("Node mapping was not done correctly\n");
			return;	
		}
			
		
		Err_F=norm2(Forces_full_user[i][3]-Forces_full_Ref[RefNode][3] , 
					Forces_full_user[i][4]-Forces_full_Ref[RefNode][4] ,
					Forces_full_user[i][5]-Forces_full_Ref[RefNode][5] );
		Err_V=norm2(Forces_full_user[i][6]-Forces_full_Ref[RefNode][6] , 
					Forces_full_user[i][7]-Forces_full_Ref[RefNode][7] ,
					Forces_full_user[i][8]-Forces_full_Ref[RefNode][8] );	
					
		if ( Err_F > MaxErr_F){
			MaxErr_F= Err_F;
			MaxErr_F_Vec[0]= Forces_full_user[i][3]-Forces_full_Ref[RefNode][3] ;
			MaxErr_F_Vec[1]= Forces_full_user[i][4]-Forces_full_Ref[RefNode][4];
			MaxErr_F_Vec[2]= Forces_full_user[i][5]-Forces_full_Ref[RefNode][5];
			MaxErr_F_nodeID_user=i;
			MaxErr_F_nodeID_Ref=RefNode;
		}
		
		if ( Err_V > MaxErr_V){
			MaxErr_V= Err_V;
			MaxErr_V_Vec[0]= Forces_full_user[i][6]-Forces_full_Ref[RefNode][6] ;
			MaxErr_V_Vec[1]= Forces_full_user[i][7]-Forces_full_Ref[RefNode][7];
			MaxErr_V_Vec[2]= Forces_full_user[i][8]-Forces_full_Ref[RefNode][8];
			MaxErr_V_nodeID_user=i;
			MaxErr_V_nodeID_Ref=RefNode;
		}
	}
	
	ForceMag  = norm(Forces_full_Ref[MaxErr_F_nodeID_Ref][3],
				      Forces_full_Ref[MaxErr_F_nodeID_Ref][4],
				      Forces_full_Ref[MaxErr_F_nodeID_Ref][5]);
	if (fabs(ForceMag)>MIN_F_mag)	{			  
		MaxErr_F_rel=sqrt( MaxErr_F/ ForceMag )*100;
	} else {
		MaxErr_F_rel= 0.0;
	}
			
	V_Mag = norm(Forces_full_Ref[MaxErr_V_nodeID_Ref][6],
				  Forces_full_Ref[MaxErr_V_nodeID_Ref][7],
				  Forces_full_Ref[MaxErr_V_nodeID_Ref][8]);
	if (fabs(V_Mag) >MIN_V_mag ){			  
		MaxErr_V_rel=sqrt( MaxErr_V/ V_Mag)*100;
	} else {
		MaxErr_V_rel= 0.0 ;
	}	
	
	if (MaxErr_F_rel < TEST2_tol && MaxErr_V_rel < TEST2_tol ){
		printf("test2"GRN" PASSED" RESET"\n");
	} else {
		printf("test2"RED" FAILED" RESET "\n");
		if (MaxErr_F_rel > TEST2_tol){
			printf("MaxErr between " BLU"forces "RESET "obtained from LocalSegForces(Full) of user"
			" W.R.T Ref values are %g%%\n"
			"This Difference is associated with row_user =%d row_ref = %d\n",MaxErr_F_rel,MaxErr_F_nodeID_user,MaxErr_F_nodeID_Ref);
			printf("Forces_full_user: %e %e %e \n",Forces_full_user[MaxErr_F_nodeID_user][3],
			Forces_full_user[MaxErr_F_nodeID_user][4],Forces_full_user[MaxErr_F_nodeID_user][5]);
			printf("Forces_full_Ref: %e %e %e \n",Forces_full_Ref[MaxErr_F_nodeID_Ref][3],
			Forces_full_Ref[MaxErr_F_nodeID_Ref][4],Forces_full_Ref[MaxErr_F_nodeID_Ref][5]);
			printf("ForceDiffMax= %e %e %e \n\n",MaxErr_F_Vec[0],MaxErr_F_Vec[1],MaxErr_F_Vec[2] );			
		}
		if (MaxErr_V_rel > TEST2_tol){				
			printf("MaxErr between " YEL"velocities "RESET"obtained from LocalSegForces(Full) of user"
			" W.R.T Ref values are %g%% \n"
			"This Difference is associated with row_user =%d row_ref =%d\n",MaxErr_V_rel,MaxErr_V_nodeID_user,MaxErr_V_nodeID_Ref);
			printf("Velocity_full_user: %e %e %e \n",Forces_full_user[MaxErr_V_nodeID_user][6],
			Forces_full_user[MaxErr_V_nodeID_user][7],Forces_full_user[MaxErr_V_nodeID_user][8]);
			printf("Velocity_full_Ref: %e %e %e \n",Forces_full_Ref[MaxErr_V_nodeID_Ref][6],
			Forces_full_Ref[MaxErr_V_nodeID_Ref][7],Forces_full_Ref[MaxErr_V_nodeID_Ref][8]);
			printf("VelocityDiffMax= %e %e %e \n\n",MaxErr_V_Vec[0],MaxErr_V_Vec[1],MaxErr_V_Vec[2] );
		}
	}
	
	
	
	//test3: check the forces & velocities obtained from LocalSegForce(GROUP) with the refrence values.
	printf("\n");
        printf("***********************************************************************\n");
        printf("Test3 compares the GROUP forces and velocities against reference values.\n");
        printf("***********************************************************************\n");
	for (j=0; j<MAXGROUPNUM; j++){	
		Err_V = 0.0;
	   	Err_F = 0.0;
		MaxErr_F=0.0;
	    MaxErr_V=0.0;
		//double MaxErr_F_Vec[3], MaxErr_V_Vec[3];
		MaxErr_F_Vec[0]=0.0; MaxErr_F_Vec[1]=0.0; MaxErr_F_Vec[2]=0.0; 
		MaxErr_V_Vec[0]=0.0; MaxErr_V_Vec[1]=0.0; MaxErr_V_Vec[2]=0.0; 
		// MaxErr_F_nodeID=0;
		// MaxErr_V_nodeID=0;
	
		for (i=0; i<nNodes_full_user; i++){
			
			RefNode=MapParToSer[i];
			Err_Coor=norm2(Forces_GROUP_user[j][i][0]-Forces_GROUP_Ref[j][RefNode][0] , 
					   Forces_GROUP_user[j][i][1]-Forces_GROUP_Ref[j][RefNode][1] ,
					   Forces_GROUP_user[j][i][2]-Forces_GROUP_Ref[j][RefNode][2] );
			if 	(Err_Coor > 1e-10){
				printf("Node mapping was not done correctly\n");
				return;	
			}
		
		
			Err_F=norm2(Forces_GROUP_user[j][i][3]-Forces_GROUP_Ref[j][RefNode][3] , 
		    			Forces_GROUP_user[j][i][4]-Forces_GROUP_Ref[j][RefNode][4] ,
					    Forces_GROUP_user[j][i][5]-Forces_GROUP_Ref[j][RefNode][5] );
			Err_V=norm2(Forces_GROUP_user[j][i][6]-Forces_GROUP_Ref[j][RefNode][6] , 
				      	Forces_GROUP_user[j][i][7]-Forces_GROUP_Ref[j][RefNode][7] ,
					    Forces_GROUP_user[j][i][8]-Forces_GROUP_Ref[j][RefNode][8] );	
					
			if ( Err_F > MaxErr_F){
				MaxErr_F= Err_F;
				MaxErr_F_Vec[0]= Forces_GROUP_user[j][i][3]-Forces_GROUP_Ref[j][RefNode][3] ;
				MaxErr_F_Vec[1]= Forces_GROUP_user[j][i][4]-Forces_GROUP_Ref[j][RefNode][4];
				MaxErr_F_Vec[2]= Forces_GROUP_user[j][i][5]-Forces_GROUP_Ref[j][RefNode][5];
				MaxErr_F_nodeID_user=i;
				MaxErr_F_nodeID_Ref=RefNode;
			}
		
			if ( Err_V > MaxErr_V){
				MaxErr_V= Err_V;
				MaxErr_V_Vec[0]= Forces_GROUP_user[j][i][6]-Forces_GROUP_Ref[j][RefNode][6] ;
				MaxErr_V_Vec[1]= Forces_GROUP_user[j][i][7]-Forces_GROUP_Ref[j][RefNode][7];
				MaxErr_V_Vec[2]= Forces_GROUP_user[j][i][8]-Forces_GROUP_Ref[j][RefNode][8];
				MaxErr_V_nodeID_user=i;
				MaxErr_V_nodeID_Ref=RefNode;
			}
		}
		ForceMag = norm(Forces_GROUP_Ref[j][MaxErr_F_nodeID_Ref][3],
						 Forces_GROUP_Ref[j][MaxErr_F_nodeID_Ref][4],
						 Forces_GROUP_Ref[j][MaxErr_F_nodeID_Ref][5]) ;
		if ( fabs(ForceMag) > MIN_F_mag ) {				 
			MaxErr_F_rel=sqrt( MaxErr_F/ForceMag)*100;
		} else {
			MaxErr_F_rel=0.0;
		}
		
		V_Mag = norm(Forces_GROUP_Ref[j][MaxErr_V_nodeID_Ref][6],
					  Forces_GROUP_Ref[j][MaxErr_V_nodeID_Ref][7],
					  Forces_GROUP_Ref[j][MaxErr_V_nodeID_Ref][8]);
		if (fabs(V_Mag) > MIN_V_mag){			  
			MaxErr_V_rel=sqrt( MaxErr_V/ V_Mag)*100;
		} else {
			MaxErr_V_rel= 0.0 ;
		}		
				
		if (MaxErr_F_rel < TEST3_tol && MaxErr_V_rel < TEST3_tol ){
			printf("test3 for GROUP%d"GRN" PASSED" RESET"\n",j);
		} else {
			printf("test3  for GROUP%d"RED" FAILED" RESET "\n",j);	
			if (MaxErr_F_rel > TEST3_tol){
				printf("MaxErr between " BLU "forces " RESET "obtained from LocalSegForces(GROUP%d) of user"
				" W.R.T Ref values are %g%%\n"
		           	"This Difference is associated with row_user =%d row_ref =%d\n",j,MaxErr_F_rel,MaxErr_F_nodeID_user,MaxErr_F_nodeID_Ref);
		           	
				printf("Forces_full_user: %e %e %e \n",Forces_GROUP_user[j][MaxErr_F_nodeID_user][3],
				Forces_GROUP_user[j][MaxErr_F_nodeID_user][4],Forces_GROUP_user[j][MaxErr_F_nodeID_user][5]);
				printf("Forces_full_Ref: %e %e %e \n",Forces_GROUP_Ref[j][MaxErr_F_nodeID_Ref][3],
				Forces_GROUP_Ref[j][MaxErr_F_nodeID_Ref][4],Forces_GROUP_Ref[j][MaxErr_F_nodeID_Ref][5]);
				printf("ForceDiffMax= %e %e %e \n\n",MaxErr_F_Vec[0],MaxErr_F_Vec[1],MaxErr_F_Vec[2] );			
			}
			if (MaxErr_V_rel > TEST3_tol){							
				printf("MaxErr between " YEL"velocities "RESET" obtained from LocalSegForces(GROUP%d) of user"
				" W.R.T Ref values are %g%% \n"
		           	"This Difference is associated with row_user =%d row_ref =%d\n",j,MaxErr_V_rel,MaxErr_V_nodeID_user,MaxErr_V_nodeID_Ref);
				printf("Velocity_full_user: %e %e %e \n",Forces_GROUP_user[j][MaxErr_V_nodeID_user][6],
				Forces_GROUP_user[j][MaxErr_V_nodeID_user][7],Forces_GROUP_user[j][MaxErr_V_nodeID_user][8]);
				printf("Velocity_full_Ref: %e %e %e \n",Forces_GROUP_Ref[j][MaxErr_V_nodeID_Ref][6],
				Forces_GROUP_Ref[j][MaxErr_V_nodeID_Ref][7],Forces_GROUP_Ref[j][MaxErr_V_nodeID_Ref][8]);
				printf("VelocityDiffMax= %e %e %e \n\n",MaxErr_V_Vec[0],MaxErr_V_Vec[1],MaxErr_V_Vec[2] );	
			}	
		}			
	}
	
	
}



/* calculate node force and velocities and write to file */
int CalNodalForces (int argc, char *argv[])
{
        int     cycleEnd, memSize, initialDLBCycles;
        time_t  tp;
        Home_t  *home;
        Param_t *param;
		int        i,j;
        int        doAll = 1;
     	char fileName[MAXPATHLENGTH];
		int numprocs, procid;	
		
#ifdef PARALLEL
		printf("PARALLEL is defined\n");
#endif		

        Meminfo(&memSize);
        ParadisInit(argc, argv, &home);
		numprocs = home->numDomains;
		procid = home->myDomain;
		printf("procid=%d\n",home->myDomain);
        home->cycle      = home->param->cycleStart;

        param            = home->param;
        cycleEnd         = param->cycleStart + param->maxstep;
        initialDLBCycles = param->numDLBCycles;

        if (param->forceCutOff == 0) CellCharge(home);
        
        SegSegListMaker(home, FULL);
        FILE    *fp;
        Node_t *node_tmp;

#ifdef _IMPLICIT        
        for (j=GROUP0; j<=GROUP4; j++){
        	FlagSubcycleNodes(home, j);
        	NodeForce(home, j);
        
        	CalcNodeVelocities(home, 1, doAll);

#ifdef PARALLEL
			//MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
			//MPI_Comm_rank(MPI_COMM_WORLD, &procid); 	
			int rank = 0;
			while (rank < numprocs) {
			   if (procid == rank) {
					snprintf(fileName, sizeof(fileName), GROUPDATAFILE, DIR_PROPERTIES,j-GROUP0);
					if (rank==0){
						fp = fopen(fileName, "w");
					} else	{
						fp = fopen(fileName, "a");	
					}
					
					for (i = 0 ; i < home->newNodeKeyPtr ; i++) {
						if ( (node_tmp = home->nodeKeys[i]) == (Node_t *)NULL) continue;
							fprintf(fp, "%e %e %e %e %e %e %e %e %e  \n",
								node_tmp->x , node_tmp->y , node_tmp->z ,
								node_tmp->fX, node_tmp->fY, node_tmp->fZ,
								node_tmp->vX, node_tmp->vY, node_tmp->vZ);
						//		node_tmp->myTag.domainID,node_tmp->myTag.index);
					}
					fclose(fp);   
			   }
			   rank++;
			   MPI_Barrier (MPI_COMM_WORLD);
			}
			
#else
	printf("this section is only for Parallel runs\n");
	return;
#endif			
	
        }
#endif
        
        NodeForce(home, FULL);
        CalcNodeVelocities(home, 1, doAll);
         
#ifdef PARALLEL
			int rank = 0;
			while (rank < numprocs) {
			   if (procid == rank) {
					snprintf(fileName, sizeof(fileName), FULLDATAFILE, DIR_PROPERTIES);
					if (rank==0){
						fp = fopen(fileName, "w");
					} else	{
						fp = fopen(fileName, "a");	
					}
					for (i = 0 ; i < home->newNodeKeyPtr ; i++) {
						if ( (node_tmp = home->nodeKeys[i]) == (Node_t *)NULL) continue;
							fprintf(fp, "%e %e %e %e %e %e %e %e %e \n",  
								node_tmp->x , node_tmp->y , node_tmp->z ,
								node_tmp->fX, node_tmp->fY,node_tmp->fZ ,
								node_tmp->vX, node_tmp->vY,node_tmp->vZ );
					}
					fclose(fp); 
			   }
			   rank++;
			   MPI_Barrier (MPI_COMM_WORLD);
			}
#else
	printf("this section is only for Parallel runs\n");
	return;
#endif		

        /* ParadisFinish(home); */
		
		
}

/* Main program starts here */
int main (int argc, char *argv[])
{
		//MPI_Init(&argc, &argv);
        /* calculate node force and velocities and write to file */
        CalNodalForces(argc, argv);

        chdir("..");

        /* Compare forces and velocities against values stored in tables */
		
#ifdef PARALLEL
		int numprocs, procid;	
		MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
		MPI_Comm_rank(MPI_COMM_WORLD, &procid); 
		if (procid==0){
			CompareNodalForces();
		}
		MPI_Barrier (MPI_COMM_WORLD);
		MPI_Finalize();
#else
	//	 CompareNodalForces();
#endif

        return(0);
}
