from ctypes import *
import numpy as np
import time, string, sys, getopt
import subprocess, os, re, shutil
import commands
from Home_imp import *
from glob import *

def init_paradis():
  global workdir, paradis_ctrlfile, paradis_datafile
  global paradis_result_dir, maxstep,  work_dir_length
  global shearModulus, pois
  global ParadisLibraryPath
  
  workdir = os.getcwd() 
  work_dir_length = len(workdir)

  taskname = sys.argv[1]
#  taskname = 'frank_read_src'
  paradis_ctrlfile =  workdir + '/'+taskname + '.ctrl'
  paradis_datafile =  workdir + '/'+taskname + '.data'

  ParadisLibraryPath = workdir+"/../../../lib/libparadisimp.so"

  if not os.path.exists(paradis_ctrlfile) or not os.path.exists(paradis_datafile):    
    print "Paradis input files missing."
    exit(0)

# Export paradis functions
def paradis_bookkeeping():
  global paradislib, home, abqinit, paradisinit, paradisstep, abqstep

  argc = 2
  myargv = ctypes.c_char_p * argc
  argv = myargv("paradis",paradis_ctrlfile)
  paradislib = cdll.LoadLibrary(ParadisLibraryPath)
  home = POINTER(Home_t)()
  paradisinit = paradislib.ParadisInit
  paradisstep = paradislib.ParadisStep

  paradisinit.argtypes = [ ctypes.c_int,  POINTER(ctypes.c_char_p), POINTER(POINTER(Home_t))] 
  paradisinit(argc,byref(argv),byref(home))

  p_memSize = POINTER(c_int)
  p_param = home.contents.param
  home.contents.cycle = p_param.contents.cycleStart
  cycleEnd = p_param.contents.cycleStart + p_param.contents.maxstep
  initialDLBCycles = p_param.contents.numDLBCycles

'''
!!! Main Program Starts Here !!!

'''
def main(argv):
  init_paradis()
  paradis_bookkeeping()
  maxstep = 200000
  for tstep in range(0,maxstep):
    
    print 'tstep = {0}/{1}'.format(tstep, maxstep)
    t0 = time.time()  
    home.contents.cycle = tstep
    paradisstep(home)
    s2=POINTER(POINTER(c_double))
    r =POINTER(c_double)
    t1 = time.time()


if __name__ == "__main__":
   main(sys.argv[1:])
