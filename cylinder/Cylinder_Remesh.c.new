/*****************************************************************************
 *
 *      Module:         Cylinder_Remesh.c
 *      Description:    This module contains functions to handle node
 *                      collision with the cylinder surface
 *                      
 *****************************************************************************/


/***************************************************************************** 
Algorithm
*********

- Remove all segments with both nodes flagged 6. It can happen that
during remesh, a node has been deleted that leads to two nodes 
connected with flag 6.

- Flag all nodes that are outside the CYL to 6.

- Split segments with a node inside (flag 0) and a node outside the CYL 
(flag 6). We need to be careful here to remove the neighbors and not
the node since splitting a node affect its pointer.

- Remove segments with both nodes flagged 6.

When the CYL Remesh routine is finished, nodes that are flagged 6 are
on the surface and have only one arm. However some remesh rules of
ParaDiS such as SplitMultiNode, MergeNode and HandleCollisions can
affect these properties. Some nodes with flag 6 can have multiarms or
move inside or outside the CYL.

Before starting the algorithm, we need to ensure that 
- all segments with both nodes at flag 6 have been removed 
- that nodes with flag 6 are outside or on the surface of the CYL.
So we call routines that
- Check whether a node flagged 6 is still on the surface and has one
arm.

Problems
********

(1) A node flagged 6 inside the CYL with multiple arms. If this happens and
the Burgers vector at that node is not conserved, we need to
abort. This happens in parallel.  Need to check how this can happen.

(2) In parallel, the split can fail because the segment does not
belong to the processor.


Solutions
*********

(1) Modifications of Remesh rules of ParaDiS

In RemeshRule2.c:
We don't remove a node with flag 6 in MeshCoarsen. 

In Collisions.c:
When two nodes A and B are going to collide, if node A has a  flag 6. 
The final collision point is the node A. That means that this node won't
move inside the CYL during a collision.

This corrects the problem of having a node inside with flag 6.

(2) If the loop is done over the node to be removed then there is no problems anymore because
it belongs to the processors calling SplitNode. We stil have to be careful with pointers so we
use a while routine.
 

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "Home.h"
#include "Util.h"
#include "CYL.h"
#include "QueueOps.h"

#define DEBUGMODE 1
#define iprint 0

#ifdef _CYLINDER
/*-------------------------------------------------------------------------
 *
 *      Function:    Cylinder_Remesh
 *      Description: Handle node collision with cylinder surface
 *
 *------------------------------------------------------------------------*/
void Cylinder_Remesh(Home_t *home,Cylinder_t *cylinder)
{
  int i, j, k, splitstatus, isDomOwnsSeg=0;
  int globalOp;
  double radius,r;
  Node_t *nodea, *nodeb;
  Param_t *param;
  int icall;
  
  int thisDomain;
  thisDomain = home->myDomain;

  param = home->param;
  
  /* Numerical errors lead the projected point to be
   * just a little bit bigger than the radius 
   */
  radius = cylinder->radius;

  // During remesh, a node can been deleted that leads to two nodes 
  // connected and with flag 6.
  // Remove those nodes.
  RemoveSurfaceSegments(home);

#if DEBUGMODE
  icall = 0;
  //CheckSurfaceNodes(home, cylinder, icall);
  icall = 1;
#endif

  for (i = 0; i < home->newNodeKeyPtr; i++) 
    {
      nodea = home->nodeKeys[i];
      if (nodea == (Node_t *)NULL) continue;
      if (nodea->constraint == PINNED_NODE) continue;
      if (nodea->constraint == SURFACE_NODE) continue;
      if (nodea->constraint == CYLINDER_SURFACE_NODE) continue;
      /* if a node is outside cylinder, flag it to a surface node (6) */ 
      if (sqrt(nodea->x*nodea->x + nodea->y*nodea->y) >= radius)  
	nodea->constraint = CYLINDER_SURFACE_NODE;
    }
  

  /* The only remaining case is a surface nodea (6) 
   * outside the film with a neibhoring normal nodeb (0) 
   * inside the film 
   * nodea  belongs to the current proc. but nodea
   * may belong to another processor.
   * Once SplitNode has been used, nodea pointers have changed.
   * We need to start from the begining of the loop again.
   */

  int dirty = 1;
  
  while (dirty) 
    {
      dirty = 0;
      
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;
	  if (nodea->constraint == PINNED_NODE) continue;
	  if (nodea->constraint != SURFACE_NODE) continue;
	  if (nodea->constraint != CYLINDER_SURFACE_NODE) continue;
	  
	  r = sqrt(nodea->x*nodea->x + nodea->y*nodea->y);

	  if ((nodea->constraint == CYLINDER_SURFACE_NODE||nodea->constraint == SURFACE_NODE) && r > radius + 1e-5)
	    {
	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j);
		  if (nodeb->constraint == UNCONSTRAINED) 
		    {
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodea->myTag))
			{		      
			  if (iprint == 1) 
			    {
			      printf("Domain %d Splitting nodes (%d,%d)-(%d,%d)\n",thisDomain,
				     nodea->myTag.domainID, nodea->myTag.index,
				     nodeb->myTag.domainID, nodeb->myTag.index);
			    }
			  // Nodea is outside. Nodeb is inside
			  // Find the node in between nodea and nodeb and split the segment
			  splitstatus = Split(home,nodea,nodeb,radius);
			  dirty = 1;
			}
		    }
		  if (dirty) break;
		} // for j
	      if (dirty) break;
	    }
	}// for i
    }//while


  dirty = 1;
  
  while (dirty) 
    {
      dirty = 0;
      
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;
	  if (nodea->constraint == PINNED_NODE) continue;
	  if (nodea->constraint != UNCONSTRAINED) continue;
	  
	  if (nodea->constraint == UNCONSTRAINED)
	    {
	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j);
		  r = sqrt(nodeb->x*nodeb->x + nodeb->y*nodeb->y);
		  if ((nodeb->constraint == CYLINDER_SURFACE_NODE||nodeb->constraint == SURFACE_NODE) && r > radius + 1e-5) 
		    {
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag))
			{
			  if (iprint == 1)
			    {
			      printf("Domain %d Splitting nodes (%d,%d)-(%d,%d)\n",thisDomain,
				     nodea->myTag.domainID, nodea->myTag.index,
				     nodeb->myTag.domainID, nodeb->myTag.index);
			    }
			  // Nodea is outside. Nodeb is inside
			  // Find the node in between nodea and nodeb and split the segment
			  splitstatus = Split(home,nodeb,nodea,radius);
			  dirty = 1;
			}
		    }
		  if (dirty) break;
		} // for j
	      if (dirty) break;
	    }
	}// for i
    }//while




  /* The only remaining case is to remove any arms between surface nodes (6) */
  RemoveSurfaceSegments(home);

#if DEBUGMODE
  CheckSurfaceNodes(home, cylinder,icall);
#endif
   
}


/*-------------------------------------------------------------------------
 *
 *      Function:    CheckSurfaceNodes
 *      Description: Check there should be no segments connection surface nodes
 *                   A surface node should only have one arm
 *
 *------------------------------------------------------------------------*/
void CheckSurfaceNodes(Home_t *home,Cylinder_t *cylinder, int icall)
{
  int i, j, k;
  Node_t *nodea, *nodeb, *nodec;
  real8 radius = cylinder->radius,r;
  real8 eps = 1.e-1;

  int thisDomain;
  thisDomain = home->myDomain;

  for (i = 0; i < home->newNodeKeyPtr; i++) 
    {
      nodea = home->nodeKeys[i];
      if (nodea == (Node_t *)NULL) continue;

      r = sqrt(nodea->x*nodea->x + nodea->y*nodea->y);
      if((nodea->constraint == CYLINDER_SURFACE_NODE||nodea->constraint == SURFACE_NODE)&& fabs(r-radius)> eps) 
	{
	  Fatal("Domain %d Node a (%d,%d) is flagged 6 but inside r=%f",
		thisDomain, nodea->myTag.domainID, nodea->myTag.index,r);
	}
      

      if (nodea->constraint != SURFACE_NODE) continue;
      if (nodea->constraint != CYLINDER_SURFACE_NODE) continue;
      
      for (j = 0; j < nodea->numNbrs; j++) 
	{
	  nodeb = GetNeighborNode(home, nodea, j); 
	  if(OrderNodes(nodea,nodeb)>0) continue;
	  
	  if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag))
	    {
	      r = sqrt(nodea->x*nodea->x + nodea->y*nodea->y);
	      if (r> radius + 1e-5)
		{
		  PrintNodesandNeighbors("Before Fatal",home);
		  Fatal("Domain %d Node a (%d,%d) is flagged 6 but is not on the surface! nbNeigh=%d r=%.15e\n",
			icall,thisDomain, nodea->myTag.domainID, nodea->myTag.index,nodea->numNbrs,r);
		}
	    }
	}
    }
}


/*-------------------------------------------------------------------------
 *
 *      Function:    RemoveSurfaceSegments
 *      Description: remove segments connecting surface nodes
 *
 *------------------------------------------------------------------------*/
void RemoveSurfaceSegments(Home_t *home)
{
  int i, j, k, constra, constrb;
  int globalOp;
  real8 rA,rB;
  Node_t *nodea, *nodeb, *nodec;
  
  int thisDomain;
  thisDomain = home->myDomain;

  int dirty = 1;

  while (dirty) 
  {
     dirty = 0;
     for (i = 0; i < home->newNodeKeyPtr; i++) 
     {
       nodea = home->nodeKeys[i];
       if (nodea == (Node_t *)NULL) continue;
       
       constra = nodea->constraint;
       if (constra!= SURFACE_NODE) continue;

       for (j = 0; j < nodea->numNbrs; j++) 
       {
	   nodeb = GetNeighborNode(home, nodea, j); 
	   constrb = nodeb->constraint;
	   
	   if(OrderNodes(nodea,nodeb)>0) continue;
           if (constrb!= SURFACE_NODE) continue;

	   rA = sqrt(nodea->x*nodea->x + nodea->y*nodea->y);
	   rB = sqrt(nodeb->x*nodeb->x + nodeb->y*nodeb->y);

	   if (iprint) printf("Domain %d -- Found a segment on the surface to be deleted:\n",
		  home->myDomain);
	   //printf("(%d,%d)-(%d,%d) rA=%f rB=%f\n",
	   //  nodea->myTag.domainID, nodea->myTag.index,
	   //  nodeb->myTag.domainID, nodeb->myTag.index,rA,rB);

           /* inform neighboring domains (CPUs) of this change */
           globalOp = 1;
	       
	   if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag))
           {
	     if (iprint) printf("Domain %d -- Remove arms (%d,%d)-(%d,%d)\n",
		    home->myDomain,nodea->myTag.domainID, nodea->myTag.index,
		    nodeb->myTag.domainID, nodeb->myTag.index);
	     
	     ChangeArmBurg(home, nodea, &nodeb->myTag, 0, 0, 0, 0, 0, 0,
#ifdef _STACKINGFAULT
			   0.0, 0.0, 0.0,
#endif
			   globalOp, DEL_SEG_HALF);
	     ChangeArmBurg(home, nodeb, &nodea->myTag, 0, 0, 0, 0, 0, 0,
#ifdef _STACKINGFAULT
			   0.0, 0.0, 0.0,
#endif
			   globalOp, DEL_SEG_HALF);
	     
	     if (nodeb->numNbrs == 0) 
	       {
		 if (nodeb->myTag.domainID == thisDomain)
		   {
		     if (iprint) printf("Removing node b (%d,%d)\n",nodeb->myTag.domainID, nodeb->myTag.index);
		     RemoveNode(home, nodeb, globalOp);
		   }
	       }
	     dirty = 1;
	   }
           if (dirty) break;
       }// for (j)
       if (dirty) break;
     }// for (i)
  }//while
  
   /* 
    * Remove any nodes with zero neighbors
    */
  for (i = 0; i < home->newNodeKeyPtr; i++) 
     {
       globalOp = 1;
       nodea = home->nodeKeys[i];
       if (nodea == (Node_t *)NULL) continue;
       
       if (nodea->numNbrs == 0 && nodea->myTag.domainID == thisDomain)
	 {
	   if (iprint) printf("Removing node a (%d,%d)\n",nodea->myTag.domainID, nodea->myTag.index);
	   RemoveNode(home, nodea, globalOp);
	 }
     }
  
}

#undef iprint
#endif
