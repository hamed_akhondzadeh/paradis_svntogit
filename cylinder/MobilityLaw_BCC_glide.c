/**************************************************************************
 *
 *      Module:       Mobility_BCC_glide
 *      Description:  Contains functions for calculating mobility of nodes
 *                    in BCC metals.  Based on MobilityLaw_BCC_0b.c
 *                    This is really only slightly modified from
 *                    mobility BCC_0b by first obtaining the velocity assuming 
 *                    climbing exists, then project out the climb component 
 *                    from the velocity. 
 *
 *      Authors:      Meijie Tang         Nov. 1, 2007
 *
 *      Includes functions:
 *
 *            Mobility_BCC_glide()
 *                
 *      Modification (iryu/2011.1.31)
 *               : Surface node should be on the surface. 
 *                 From BCC_0 modlibitylaw, the velocity normal to the 
 *                 surface set to be zero
 *
 *      Modification (iryu/2011.2.15)
 *               : Surface node should be on the surface. 
 *                 From FCC_0 modlibitylaw, surface node is adjusted
 *
 *      Modification (iryu/2011.2.23)
 *               : Make version 3 to make surface node stay on the surface.
 *                 (if surface node, call FCC_0 with out line constraint)
 *
 *      Modification (iryu/2011.2.23)
 *               : Surface cross slip (version1)
 *                 (change the slip plane of the surface node only)
 *                 It is only based on the magnitude of the force 
 *                 projected on two slip planes.
 *
 *      Modification (iryu/2011.2.23)
 *                 !!!!Need to check more. 
 *               : Make the mobility of the surface node faster than inside node
 *                 If the radius of the node is larger than 0.9R, adjust the mobility
 *
 *      Modification (iryu/2011.3.11)
 *               : Surface cross slip (version2) 
 *                 (change the slip plane of the surface node only)
 *                 It is determined by the contribution both of the image stress and of the P-K force.
 *
 *      Modification (iryu/2011.8.02)
 *               : Surface cross slip (version3) 
 *                 For {110}<110> slip system
 *                 To do that, need to know image force
 *                 Here image force on the surface node is specified here. 
 *                 We assume that surface cross slip occurs under 100MPa compression
 *                 Based on the total force projected on the each slip plane, choose slip plane. 
 *
 *      Modification (iryu/2011.8.05)
 *               : Surface cross slip (version4) 
 *                 For {110}<110> slip system
 *                 To do that, need to know image force
 *                 Here image force on the surface node is specified here. 
 *                 We assume that surface cross slip occurs under 100MPa compression
 *                 Because force along the line direction does not have physical meaning, total force is projected along tangent directions. 
 *                 Based on the projected force, choose slip plane.  
 *
 *                  
 ***************************************************************************/
#include <stdio.h>
#include <math.h>
#include <sys/param.h>
#include "Home.h"
#include "Util.h"
#include "Mobility.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef _FEM
#include "FEM.h"
#endif

#define DEBUG_PRINT 0

/**************************************************************************
 *
 *      Function:     Mobility_BCC_glide
 *      Description:  This function calculates the velocity for a single
 *                    specified node.
 *
 *      Returns: 0 on success
 *               1 if velocity could not be determiend
 * 
 *************************************************************************/
int Mobility_BCC_glide(Home_t *home, Node_t *node)
{
        int     i, nbrs;
        int     numNonZeroLenSegs = 0;
        real8   bx, by, bz;
        real8   dx, dy, dz;
        real8   mx, my, mz;
        real8   nx, ny, nz; 
        real8   temp;
        real8   mag, mag2, halfMag, invMag;
        real8   invbMag2, bMag2, costheta, costheta2;
        real8   Bline, Bscrew, Bedge, Bglide, Bclimb, Beclimb;
        real8   Bscrew2, Beclimb2;
        real8   invBscrew2, invBedge2;
        real8   BlmBsc, BclmBsc, BglmBsc, BlmBecl;
        real8   eps = 1.0e-12, eps1 = 1.0e-2, eps2 = 0.95;
        real8   burgCryst[3];
        real8   nForce[3], nVel[3];
        real8   Btotal[3][3] = {{0.0, 0.0, 0.0},
                                {0.0, 0.0, 0.0},
                                {0.0, 0.0, 0.0}};
        real8   invBtotal[3][3];

        real8   normX[3],normY[3],normZ[3];
        int     norms; 
        real8   tor=1.e-5; 

        Node_t  *nbrNode, *nbr, *nbr0, *nbr1;
        Param_t *param;



/*
 *      If the node is a "fixed" node, we cannot move it, so just zero
 *      out the velocity and return.
 */
        if (node->constraint == PINNED_NODE) {
            node->vX = 0.0;
            node->vY = 0.0;
            node->vZ = 0.0;
            return(0);
        }

/*
 *      If any of the arms of the node has a burgers vector that
 *      has explicitly been set to be sessile (via control file
 *      inputs), the node may not be moved.
 */
        if (NodeHasSessileBurg(home, node)) {
            node->vX = 0.0;
            node->vY = 0.0;
            node->vZ = 0.0;
            return(0);
        }
        param = home->param;

        Bscrew     = 1.0 / param->MobScrew;
        Bedge      = 1.0 / param->MobEdge;

        /* Beclimb    = 1.0 / param->MobClimb; */ 
        Beclimb    = 1.0e10; 
        

        Bscrew2    = Bscrew * Bscrew;
        Beclimb2   = Beclimb * Beclimb;

        Bline      = 1.0e-2 * MIN(Bscrew, Bedge);
        BlmBsc     = Bline - Bscrew;
        BlmBecl    = Bline - Beclimb; 

        invBscrew2 = 1.0 / (Bscrew*Bscrew);
        invBedge2  = 1.0 / (Bedge*Bedge);

        nbrs = node->numNbrs;

/*
 *      find out the independent glide planes the node's neighbor segments have 
 */
        norms = 0; 

        normX[0] = node->nx[0];
        normY[0] = node->ny[0];
        normZ[0] = node->nz[0]; 

        Normalize(&normX[0],&normY[0],&normZ[0]);

/*
 *      Modified per Tom Arsenlis: Once you find a norm, project the other
 *      normals to be perpendicular
 */
        if(nbrs > 1) { 
            i=1;
            while((norms<2)&&(i<nbrs)){
                nx = node->nx[i];
                ny = node->ny[i];
                nz = node->nz[i];

                Normalize(&nx,&ny,&nz); 

                if ( norms==0) {
                    temp=fabs(normX[0]*nx+normY[0]*ny+normZ[0]*nz);
                    if (fabs(1.0e0-temp)>tor ) {
                        norms = 1;
                        nx-=temp*normX[0];
                        ny-=temp*normY[0];
                        nz-=temp*normZ[0];
                        Normalize(&nx,&ny,&nz);
                        normX[1]=nx;
                        normY[1]=ny; 
                        normZ[1]=nz;
                        normX[2]=normY[0]*normZ[1]-normZ[0]*normY[1];
                        normY[2]=normZ[0]*normX[1]-normX[0]*normZ[1];
                        normZ[2]=normX[0]*normY[1]-normY[0]*normX[1];
                    }
                } else {/* norms==1*/
                    temp=normX[2]*nx+normY[2]*ny+normZ[2]*nz;
                    if (fabs(temp)>tor ) {
                        norms = 2;
                    }
                }

                i++;
            } /* end while */
        } /* end if (nbrs > 1) */   

        node->vX = 0.0e0;
        node->vY = 0.0e0;
        node->vZ = 0.0e0;

        if (norms<2){
/*
 *          Loop over all arms of the node, adding each arm's contribution
 *          to the drag matrix.
 */
            for (i = 0; i < nbrs; i++) {

                bx = node->burgX[i];
                by = node->burgY[i];
                bz = node->burgZ[i];

                bMag2 = (bx*bx + by*by + bz*bz);
                invbMag2 = 1.0 / bMag2;

/*
 *              Calculate the length of the arm and its tangent line direction
 */
                nbrNode = GetNeighborNode(home, node, i);

                if (nbrNode == (Node_t *)NULL) {
                    printf("WARNING: Neighbor not found at %s line %d\n",
                           __FILE__, __LINE__);
                    continue;
                }

                dx = nbrNode->x - node->x;
                dy = nbrNode->y - node->y;
                dz = nbrNode->z - node->z;

                ZImage(param, &dx, &dy, &dz);

                mag2    = dx*dx + dy*dy + dz*dz;
/*
 *              If the segment is zero length (which can happen when
 *              the mobility function is being called from SplitMultiNodes())
 *              just skip the segment.
 */
                if (mag2 < eps) {
                    continue;
                }

                numNonZeroLenSegs++;

                mag     = sqrt(mag2);
                halfMag = mag/2.0;
                invMag  = 1.0 / mag;

                dx *= invMag;
                dy *= invMag;
                dz *= invMag;

/*
 *              Calculate how close to screw the arm is
 */
                costheta = (dx*bx + dy*by + dz*bz);
                costheta2 = (costheta*costheta) * invbMag2;

/*
 *              [0 0 1] arms don't move as readily as other arms, so must be
 *              handled specially.
 *
 *              If needed, rotate a copy of the burgers vector from the
 *              laboratory frame to the crystal frame.
 */
                if (param->useLabFrame) {
                    real8 bTmp[3] = {bx, by, bz};
                    Matrix33Vector3Multiply(home->rotMatrixInverse, bTmp,
                                            burgCryst);
                } else {
                    burgCryst[X] = bx;
                    burgCryst[Y] = by;
                    burgCryst[Z] = bz;
                }

                if (fabs(burgCryst[X]*burgCryst[Y]*burgCryst[Z]) < eps) {
                    if (nbrs == 2) {
                        Btotal[0][0] += halfMag * Beclimb;
                        Btotal[1][1] += halfMag * Beclimb;
                        Btotal[2][2] += halfMag * Beclimb;
                    } else {
                        Btotal[0][0] += halfMag * (dx*dx * BlmBecl + Beclimb);
                        Btotal[0][1] += halfMag * (dx*dy * BlmBecl);
                        Btotal[0][2] += halfMag * (dx*dz * BlmBecl);
                        Btotal[1][1] += halfMag * (dy*dy * BlmBecl + Beclimb);
                        Btotal[1][2] += halfMag * (dy*dz * BlmBecl);
                        Btotal[2][2] += halfMag * (dz*dz * BlmBecl + Beclimb);
                    }
                } else  {
/*
 *                  Arm is not [0 0 1], so build the drag matrix assuming the
 *                  dislocation is screw type
 */
                    Btotal[0][0] += halfMag * (dx*dx * BlmBsc + Bscrew);
                    Btotal[0][1] += halfMag * (dx*dy * BlmBsc);
                    Btotal[0][2] += halfMag * (dx*dz * BlmBsc);
                    Btotal[1][1] += halfMag * (dy*dy * BlmBsc + Bscrew);
                    Btotal[1][2] += halfMag * (dy*dz * BlmBsc);
                    Btotal[2][2] += halfMag * (dz*dz * BlmBsc + Bscrew);

/*
 *                  Now correct the drag matrix for dislocations that are
 *                  not screw
 */
                    if ((1.0 - costheta2) > eps) {


                        nx = node->nx[i];
                        ny = node->ny[i];
                        nz = node->nz[i];

                        xvector(nx, ny, nz, dx, dy, dz, &mx, &my, &mz);

                        Bglide = sqrt(invBedge2 + (invBscrew2-invBedge2) *
                                      costheta2);
                        Bglide = 1.0 / Bglide;
                        Bclimb = sqrt(Beclimb2 + (Bscrew2 - Beclimb2) *
                                      costheta2);

#ifdef NAN_CHECK
                        if (isnan(Bglide) != 0) {
                            Fatal("Mobility_BCC_glide: Bglide = NaN\n"
                                "  Bglide = sqrt(invBedge2 + "
                                "(invBscrew2-invBedge2)*costheta2)\n"
                                "  where invBedge2 = %lf, invBscrew2 = %lf, "
                                "costheta2 = %lf", invBedge2, invBscrew2,
                                costheta2);
                        }

                        if (isnan(Bclimb) != 0) {
                            Fatal("Mobility_BCC_glide: Bclimb = NaN\n"
                                "  Bclimb = sqrt(Beclimb2 + "
                                "(Bscrew2-Beclimb2)*costheta2)\n"
                                "  where Beclimb2 = %lf, Bscrew2 = %lf, "
                                "costheta2 = %lf", Beclimb2, Bscrew2,
                                costheta2);
                        }
#endif
                        BclmBsc = Bclimb - Bscrew;
                        BglmBsc = Bglide - Bscrew;


                        Btotal[0][0] += halfMag * (nx*nx * BclmBsc +
                                        mx*mx * BglmBsc);
                        Btotal[0][1] += halfMag * (nx*ny * BclmBsc +
                                        mx*my * BglmBsc);
                        Btotal[0][2] += halfMag * (nx*nz * BclmBsc +
                                        mx*mz * BglmBsc);
                        Btotal[1][1] += halfMag * (ny*ny * BclmBsc +
                                        my*my * BglmBsc);
                        Btotal[1][2] += halfMag * (ny*nz * BclmBsc +
                                        my*mz * BglmBsc);
                        Btotal[2][2] += halfMag * (nz*nz * BclmBsc +
                                        mz*mz * BglmBsc);
                    }
                }  /* End non-[0 0 1] arm */
            }  /* End loop over arms */

            Btotal[1][0] = Btotal[0][1];
            Btotal[2][0] = Btotal[0][2];
            Btotal[2][1] = Btotal[1][2];

/*
 *          It's possible this function was called for a node which only
 *          had zero length segments (during SplitSurfaceNodes() for example).
 *          If that is the case, just set the velocity to zero and return;
 */
            if (numNonZeroLenSegs == 0) {
                node->vX = 0.0;
                node->vY = 0.0;
                node->vZ = 0.0;
                return(0);
            }
/*
 *          At this point we should check if the matrix is invertable and
 *          if it isn't, find the eigen values and eigen vectors of the drag
 *          matrix, then invert the drag matrix keeping zero eigen values
 *          as zero.
 *
 *          FIX ME!  For now, we're assuming the matrix is invertable.
 */
        
            nForce[0] = node->fX;
            nForce[1] = node->fY;
            nForce[2] = node->fZ;

            if (Matrix33Invert(Btotal, invBtotal) == 0) {
				printf("At node : %d \n", node->myTag.index);
                Fatal("Mobility_BCC_glide: Cannot invert 3X3 matrix!");
            }
            Matrix33Vector3Multiply(invBtotal, nForce, nVel);

/*
 *          orthogonolize the velocity with all indepdent glide
 *          normal vectors. 
 */ 
            for (i = 0; i <= norms; i++) {
                nx = normX[i];
                ny = normY[i];
                nz = normZ[i]; 
                temp = nVel[0]*nx + nVel[1]*ny + nVel[2]*nz;
                nVel[0] -= temp*nx;
                nVel[1] -= temp*ny;
                nVel[2] -= temp*nz;
            }

            node->vX = nVel[0];
            node->vY = nVel[1];
            node->vZ = nVel[2];
        }

#ifdef _FEM
/*
 *      The velocity of any surface node along the negative surface
 *      normal direction should be zero to prevent the node moving into
 *      the box.  Make a call to adjsut the velocity accordingly.
 *
 *      Note: If the node moves outside the box, it's allowed here, then
 *      position is adjusted in AdjustNodePosition().
 */
        AdjustSurfaceNodeVel(home, node);
#endif

#ifdef _CYLINDER

/*(iryu) Cylinder surface nodes need to be on the their own slip plane & on the cylinder surface */
/*(version1 )*************************************/

#if 0 
/*
 *      remove velocity along surface normal CRW
 *      (may not be correct, need to be modified later, 2009/08/05)
 *
 *      (iryu/2011.01.31)
 *
 */
 	if (node->constraint == CYLINDER_SURFACE_NODE)
    {
		real8 nx,ny,nz,nr;	
		real8 ndotv;
		/* project out the component along surface normal */
		nx = node->x; ny = node->y; nz = 0;
		nr = sqrt(nx*nx+ny*ny+nz*nz);
			if (nr==0) fprintf(stderr,"Surface node cannot have nr = %e\n",nr);
				nx /= nr; ny /= nr; nz /= nr;
				ndotv = node->vX*nx + node->vY*ny + node->vZ*nz;
				node->vX -= ndotv*nx;
				node->vY -= ndotv*ny;
				node->vZ -= ndotv*nz;
	}		

#endif 

/*(version2 )*************************************/
/* copy from FCC_0                               */

#if 0 

    /* put surface node velocity on surface 
     * by adding velocity along line direction 
     */
    if (node->constraint == CYLINDER_SURFACE_NODE)
    {
        /* compute the surface normal of cylinder */
        real8 nx,ny,nz,nr;	
	real8 lr,lx,ly,lz;
	real8 VelxNode, VelyNode, VelzNode, Veldotlcr;
        nx = node->x; ny = node->y; nz = 0;
        nr = sqrt(nx*nx+ny*ny+nz*nz);
        if (nr==0) fprintf(stderr,"Surface node cannot have nr = %e\n",nr);
        nx/=nr; ny/=nr; nz/=nr;

        if (node->numNbrs == 1)
        {   
           /* compute line direction */
           nbr=GetNeighborNode(home,node, 0); 
           dx=nbr->x - node->x;
    	   dy=nbr->y - node->y;
           dz=nbr->z - node->z;

           ZImage (param, &dx, &dy, &dz) ;
           lr=sqrt(dx*dx+dy*dy+dz*dz);
           lx = dx/lr; ly = dy/lr; lz = dz/lr;

           real8 vl, ldotn, eps;
           eps = 0.05;  /* adjust this number to avoid surface node shooting... */
           ldotn = lx*nx + ly*ny + lz*nz;
/* Calcualte VelxNode by projecting the veloicity to line direction */
/* (iryu/2011.2.15)                                                 */ 
         VelxNode = lx*node->vX;
         VelyNode = ly*node->vY;
         VelzNode = lz*node->vZ;
/********************************************************************/
		   /* compute velocity along line direction */
           if (fabs(ldotn) > eps)
           {   
              vl = (VelxNode*nx + VelyNode*ny + VelzNode*nz) / ldotn;
              node->vX = VelxNode - vl * lx; 
              node->vY = VelyNode - vl * ly; 
              node->vZ = VelzNode - vl * lz; 
           }   
           else
           {
              vl = (VelxNode*nx + VelyNode*ny + VelzNode*nz);
              node->vX = VelxNode * ldotn - vl * lx;
              node->vY = VelyNode * ldotn - vl * ly;
              node->vZ = VelzNode * ldotn - vl * lz;
           }
        }
        else
        {
              node->vX = 0.0;
              node->vY = 0.0;
              node->vZ = 0.0;
        }

        /* we should check here whether vdotn is zero now 
        vmag = sqrt(node->vX*node->vX + node->vY*node->vY + node->vZ*node->vZ);
        if (fabs(node->vZ) > (vmag*1e-4)) 
        {
            printf("surface node vX = %e vY = %e vZ = %e vmag = %e nc = %d\n",
                    node->vX,node->vY,node->vZ,vmag,nc);
            for (i=0;i<nc;i++) printf("norm[%d] = (%e, %e, %e)\n",i,normX[i],normY[i],normZ[i]);
            Fatal("MobilityLaw_FCC_0: surface node vZ non-zero!");
        }
        */
    }

#endif    
/*********  End of version2*************************************/


/* version3  */
// Copy from FCC_0 all. 
// Remove redundant parts

#if 1
    if (node->constraint == CYLINDER_SURFACE_NODE)
    {

    real8 VelxNode, VelyNode, VelzNode, Veldotlcr;
    int i, j, cst, nc, nconstraint, nlc;
    real8 normX[100], normY[100], normZ[100], normx[100], normy[100], normz[100];
    real8 lineX[100], lineY[100], lineZ[100];
    real8 burgX, burgY, burgZ, a, b;
    real8 dx, dy, dz, lx, ly, lz, lr, LtimesB, Lx, Ly, Lz;
    real8 lcx, lcy, lcz, normdotlc;
    Node_t *nbr, *nbr2;
    real8 MobScrew, MobEdge, Mob;
    real8 bx, by, bz, br, dangle;


    Lx=param->Lx;
    Ly=param->Ly;
    Lz=param->Lz;

    MobScrew = param->MobScrew;
    MobEdge  = param->MobEdge;
    
    cst = node->constraint;    // cst: the number of glide constraints
    nc = node->numNbrs ;       // nc : the number of neighbor nodes

    
    /* copy glide plane constraints and determine line constraints */
    for(i=0;i<nc;i++)
    {
        normX[i] = normx[i] = node->nx[i];
        normY[i] = normy[i] = node->ny[i];
        normZ[i] = normz[i] = node->nz[i];

	    lineX[i] = lineY[i] = lineZ[i] = 0;

    }
    
    normX[nc] = node->x; normY[nc] = node->y; normZ[nc] = 0.0;
	real8 tmp = sqrt((normX[nc])*(normX[nc]) + (normY[nc])*(normY[nc]));
    normX[nc] /=tmp;
    normY[nc] /=tmp;
	nc ++;

    /* normalize glide plane normal vectors and lc line vectors*/
    for(i=0;i<nc;i++)
    {
        a=sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);
	b=sqrt(lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i]);

        if(a>0)
        {
            normX[i]/=a;
            normY[i]/=a;
            normZ[i]/=a;

            normx[i]/=a;
            normx[i]/=a;
            normx[i]/=a;
        }
        if(b>0)
        {
            lineX[i]/=b;
            lineY[i]/=b;
            lineZ[i]/=b;
        }
    }


    /* Find independent glide constraints */ 
    nconstraint = nc;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
        }
        #define FFACTOR_ORTH 0.05
        if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH)
        {
            normX[i] = normY[i] = normZ[i] = 0;
            nconstraint--;
        }
    }

    /* Find independent line constraints */
    nlc = 0;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(lineX+i,lineY+i,lineZ+i,lineX[j],lineY[j],lineZ[j]);
        }
        if((lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i])<FFACTOR_ORTH)
        {
            lineX[i] = lineY[i] = lineZ[i] = 0;
        }
        else
        {
            nlc ++;
        }
    }

    /* find total dislocation length times drag coefficent (LtimesB)*/
    LtimesB=0;
    for(j=0;j<node->numNbrs;j++)
    {
        nbr=GetNeighborNode(home,node,j);
        dx=nbr->x - node->x;
        dy=nbr->y - node->y;
        dz=nbr->z - node->z;
        ZImage (param, &dx, &dy, &dz) ;
        lr=sqrt(dx*dx+dy*dy+dz*dz);
        
        if (lr==0)
        { /* zero arm segment can happen after node split 
           * it is OK to have plane normal vector == 0
           * Skip (do nothing)
           */
        }
        else 
        {
           if((node->nx[j]==0)&&(node->ny[j]==0)&&(node->nz[j]==0))
           {
              printf("Mobility_FCC_0: glide plane norm = 0 for segment with nonzero length lr = %e!\n",lr);
           }

           lx=dx/lr; ly=dy/lr; lz=dz/lr;

           bx = node->burgX[j];
           by = node->burgY[j];
           bz = node->burgZ[j];
           br = sqrt(bx*bx+by*by+bz*bz);
           bx/=br; by/=br; bz/=br; /* unit vector along Burgers vector */

           dangle = fabs(bx*lx+by*ly+bz*lz);
           Mob=MobEdge+(MobScrew-MobEdge)*dangle;

           LtimesB+=(lr/Mob);
	}
    }
    LtimesB/=2;


    /* Velocity is simply proportional to total force per unit length */
    VelxNode = node->fX/LtimesB;
    VelyNode = node->fY/LtimesB;
    VelzNode = node->fZ/LtimesB;
    

    /* Orthogonalize with glide plane constraints */
    for(i=0;i<nc;i++)
    {
        if((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0))
        {
	     Orthogonalize(&VelxNode,&VelyNode,&VelzNode,
                         normX[i],normY[i],normZ[i]);
        }
    }


    /* Any dislocation with glide plane not {111} type can only move along its length
     * This rule includes LC junction which is on {100} plane
     */

    node->vX = VelxNode;
    node->vY = VelyNode;
    node->vZ = VelzNode;

    /* put surface node velocity on surface 
     * by adding velocity along line direction 
     */ 
 
    /* compute the surface normal of cylinder */	
        real8 nx,ny,nz,nr;
        nx = node->x; ny = node->y; nz = 0;
        nr = sqrt(nx*nx+ny*ny+nz*nz);
        if (nr==0) fprintf(stderr,"Surface node cannot have nr = %e\n",nr);
        nx/=nr; ny/=nr; nz/=nr;

        if (node->numNbrs == 1)
        {
           /* compute line direction */
           nbr=GetNeighborNode(home,node, 0);
           dx=nbr->x - node->x;
           dy=nbr->y - node->y;
           dz=nbr->z - node->z;
           ZImage (param, &dx, &dy, &dz) ;
           lr=sqrt(dx*dx+dy*dy+dz*dz);
           lx = dx/lr; ly = dy/lr; lz = dz/lr;

           real8 vl, ldotn, eps;
           eps = 0.05;  /* adjust this number to avoid surface node shooting... */
           ldotn = lx*nx + ly*ny + lz*nz;

           /* compute velocity along line direction */
           if (fabs(ldotn) > eps)
           {
              vl = (VelxNode*nx + VelyNode*ny + VelzNode*nz) / ldotn;
              node->vX = VelxNode - vl * lx;
              node->vY = VelyNode - vl * ly;
              node->vZ = VelzNode - vl * lz;
           }
           else
           {
              vl = (VelxNode*nx + VelyNode*ny + VelzNode*nz);
              node->vX = VelxNode * ldotn - vl * lx;
              node->vY = VelyNode * ldotn - vl * ly;
              node->vZ = VelzNode * ldotn - vl * lz;
           }
        }
        else
        {
              node->vX = 0.0;
              node->vY = 0.0;
              node->vZ = 0.0;
        }

        /* we should check here whether vdotn is zero now 
        vmag = sqrt(node->vX*node->vX + node->vY*node->vY + node->vZ*node->vZ);
        if (fabs(node->vZ) > (vmag*1e-4)) 
        {
            printf("surface node vX = %e vY = %e vZ = %e vmag = %e nc = %d\n",
                    node->vX,node->vY,node->vZ,vmag,nc);
            for (i=0;i<nc;i++) printf("norm[%d] = (%e, %e, %e)\n",i,normX[i],normY[i],normZ[i]);
            Fatal("MobilityLaw_FCC_0: surface node vZ non-zero!");
        }
        */

	}
	
#endif 

/*********  End of version3 *************************************/


/* (iryu) Change node with different slip planes */

#if 0 

/*
 *
 *    (iryu/2011.02.13)
 *    To make the mobility of the node which has two arms on different slip planes 1/10 of mobility of inner node. 
 *
 */ 
	if(node->numNbrs == 2){
		if(node->constraint == UNCONSTRAINED){
/***	nbr1=GetNeighborNode(home,node,0);                              ***/
/***	nbr2=GetNeighborNode(home,node,1);                              ***/
      	  real8 scale_two_arm_node;
     	  int nodetag;
/*     	  real8 nodetag;*/
     	  real8 tolerance;
     	  tolerance=0.1e-01;
          Node_t  *nbr1, *nbr2;
     	  real8 nx1,nx2,ny1,ny2,nz1,nz2;
     	  scale_two_arm_node=0.1e0;                   
          nodetag = node->myTag.index;
     	  nx1= node->nx[0];
    	  nx2= node->nx[1];
     	  ny1= node->ny[0];
     	  ny2= node->ny[1];
     	  nz1= node->nz[0];
     	  nz2= node->nz[1];
 			if( fabs(nx1-nx2)>tolerance || fabs(ny1-ny2)>tolerance   || fabs(nz1-nz2)>tolerance ){
/***     For the test                                                                                  ***/
/***	 printf("Two arm mobility is %e times smaller than that in inner nodes\n",scale_two_arm_node); ***/
//       printf("Node %d have two arms which have different slip planes\n",nodetag); 
			node->vX*=scale_two_arm_node;
			node->vY*=scale_two_arm_node;
			node->vZ*=scale_two_arm_node;
			}
		}
	}
		
#endif 


/* (iryu) Surface cross slip */

#if 0

/*
 *
 *    (iryu/2011.02.22)
 *   < Surface cross slip >
 *    Version 1 : Based on the resolved force on the surface node
 *    Conditions: 1. dislocation should be similar to screw. (line sence vector cross burger vector>0.9)
 *                2. Calculate force on the surface node.(PK_F)
 *                3. Project the force on two glide planes.(PK_F_n1,PK_F_n2)
 *                4. Calculate the norm of the projected forces.
 *                5. Select the slip plane on which projected force is larger  
 *
 *    (iryu/2011.04.10)
 *   < Surface cross slip >
 *                - modify the code 
 *                  when slip plane is changed, surface node moves along the intesection line.   
 */
if(node->constraint == CYLINDER_SURFACE_NODE)
{
		Node_t  *nbr, *nbr1, *nbr2;
		nbr=GetNeighborNode(home,node,0);                //Neighbor of surface node 
		nbr1=GetNeighborNode(home,nbr,0);                //1st Neighbor of nearest neighbor node 
		nbr2=GetNeighborNode(home,nbr,1);                //2nd Neighbor of nearest neighbor node
	if ((node->numNbrs == 1) && (nbr->numNbrs == 2) )
	{
		real8 tolerance;			tolerance = 0.95;	
		real8 screw_character;							// Value which say how surface segment is similar to screw character
		real8 x1,y1,z1;									// Surface node coordinates 
		real8 x2,y2,z2;									// Surface neighbor node coordinates 
		real8 dx,dy,dz;								 
		real8 bx,by,bz;									// Burgers vector of surface node 
		real8 sizeB; 	real8 sizeD;
		real8 n1X,n1Y,n1Z;								// slip plane1 : n1 [ 0  -1  1]
		real8 n2X,n2Y,n2Z;								// slip plane2 : n2 [-1   0  1]
		real8 radius;
		radius = param->cyl_radius;
		n1X=0.0e0;			n1Y=-1.0e0/sqrt(2.0e0);		n1Z = 1.0e0/sqrt(2.0e0);	
		n2X=-1.0e0/sqrt(2.0e0);	n2Y=0.0e0;				n2Z = 1.0e0/sqrt(2.0e0);
		real8 fX,fY,fZ;									// Force on the node
		fX=node->fX; 	fY=node->fY;		fZ=node->fZ;		
		real8 fXn1,fYn1,fZn1;							// Force on the node projected to plane1. 
		real8 fXn2,fYn2,fZn2;							// Force on the node projected on plane2
		real8 normFn1;									// norm of Fn1
		real8 normFn2;									// norm of Fn2
		real8 t1,t2,treal;										// To move surface nodes along intesection line of two planes
		real8 alpha;
 		x1=node->x; 	y1=node->y; 	z1=node->z; 
 		x2=nbr->x; 		y2=nbr->y; 		z2=nbr->z; 
		dx=x1-x2;		dy=y1-y2;		dz=z1-z2;
		sizeD = sqrt(dx*dx + dy*dy + dz*dz);
	    dx/=sizeD;		dy/=sizeD;		dz/=sizeD;				// Check if the dX is unit vector. 
		bx = node->burgX[0];
		by = node->burgY[0];
		bz = node->burgZ[0];
		sizeB = sqrt(bx*bx + by*by + bz*bz);
	    bx/=sizeB;		by/=sizeB;		bz/=sizeB;				// Check if the burger vector is unit vector. 
		screw_character=fabs(dx*bx+dy*by+dz*bz);
		if(screw_character > tolerance)
		{ // if surface segment has screw character. 
	      // Move dislocation at the surface such that it is located along the intersection of two planes.
		  //
			alpha = sqrt((by*by)*(radius-x2)*(radius+x2)-2.0e0*(bx*by*x2*y2)+(bx*bx)*(radius-y2)*(radius+y2));
			t1=-((bx*x2+by*y2)+alpha)/(bx*bx+by*by);
			t2=-((bx*x2+by*y2)-alpha)/(bx*bx+by*by);
//			if (fabs(t1)>fabs(t2))	
//			{
//				 treal=t2;
//			}
//			else
//			{
//				treal=t1;
//			}
//			node->x=x2+treal*bx;
//			node->y=y2+treal*by;
//			node->z=z2+treal*bz;
		  // Calculate forces on two planes
			fXn1 = fX-(fX*n1X+fY*n1Y+fZ*n1Z)*n1X;
			fYn1 = fY-(fX*n1X+fY*n1Y+fZ*n1Z)*n1Y;
			fZn1 = fZ-(fX*n1X+fY*n1Y+fZ*n1Z)*n1Z;
			normFn1 = sqrt(fXn1*fXn1+fYn1*fYn1+fZn1*fZn1);
			fXn2 = fX-(fX*n2X+fY*n2Y+fZ*n2Z)*n2X;
			fYn2 = fY-(fX*n2X+fY*n2Y+fZ*n2Z)*n2Y;
			fZn2 = fZ-(fX*n2X+fY*n2Y+fZ*n2Z)*n2Z;
			normFn2 = sqrt(fXn2*fXn2+fYn2*fYn2+fZn2*fZn2);
			if (normFn1 > normFn2)
			{	// if norm(Fn1) > norm (Fn2), then select n1 as a slip plane for the surface node
				node->nx[0]=n1X;		node->ny[0]=n1Y;		node->nz[0]=n1Z;
				if (nbr1->constraint == CYLINDER_SURFACE_NODE)
				{	// nbr1  is the surface node. 
					nbr->nx[0]=node->nx[0];		nbr->ny[0]=node->ny[0];		nbr->nz[0]=node->nz[0];
				}
				else // nbr2 is the surface node.
				{
					nbr->nx[1]=node->nx[0];		nbr->ny[1]=node->ny[0];		nbr->nz[1]=node->nz[0];
				}
			
			}
			else
			{	// if norm (Fn1)<norm(Fn2), then select n2 as a slip plane for the surface node. 
				node->nx[0]=n2X;		node->ny[0]=n2Y;		node->nz[0]=n2Z;
				if (nbr1->constraint == CYLINDER_SURFACE_NODE)
				{	// nbr1  is the surface node. 
					nbr->nx[0]=node->nx[0];		nbr->ny[0]=node->ny[0];		nbr->nz[0]=node->nz[0];
				}
				else // nbr2 is the surface node.
				{
					nbr->nx[1]=node->nx[0];		nbr->ny[1]=node->ny[0];		nbr->nz[1]=node->nz[0];
				}
			}
		}
	}
}			
#endif 

#if 0
/*
 *    (iryu/2011.05.11)
 *   < Surface cross slip >
 *    Version 2 : Consider the effect of image stress 
 *                Two plane version (0  1 -1) (1  0 -1)
 *    Conditions: 1. dislocation should be similar to screw. (line sence vector cross burger vector>0.9)
 *                2. Check the coordinate of the neighbor node
 *                3. Take the slip plane on which dislocation can move in the direction of length shrinkage.  
 *    Algorithm:
 *                1. Compute tangent vectors(t1,t2) with respect to n1, n2.
 *                   - t1,t2 should be on the slip plane and the tangent plane to the cylinder surface passing through the surface node. 
 *                   - t1,t2 should point to the direction along which dislocation length shrinks. 
 *                2. Compute force vector due to the applied stress and seg-seg force. 
 *                3. if dot(tan1, Fn1)>0 && dot(tan2,Fn2)<0), choose n1 as a slip plane
 *                   if dot(tan1, Fn1)<0 && dot(tan2,Fn2)>0), choose n2 as a slip plane
 */
if(node->constraint == CYLINDER_SURFACE_NODE)
{
		Node_t  *nbr, *nbr1, *nbr2;
		nbr=GetNeighborNode(home,node,0);                //Neighbor of surface node 
		nbr1=GetNeighborNode(home,nbr,0);                //1st Neighbor of nearest neighbor node 
		nbr2=GetNeighborNode(home,nbr,1);                //2nd Neighbor of nearest neighbor node
	if ((node->numNbrs == 1) && (nbr->numNbrs == 2) )
	{
		real8 tolerance;			tolerance = 0.95;	
		real8 screw_character;							
		real8 x1,y1,z1;									// Surface node coordinates 
		real8 x2,y2,z2;									// Surface neighbor node coordinates 
		real8 tan1x,tan1y,tan1z;						// tangent vector1 with respect to slip plane1
		real8 tan2x,tan2y,tan2z;						// tangent vector2 with respect to slip plane2
		real8 dx,dy,dz;								 
		real8 bx,by,bz;									// Burgers vector of surface node 
		real8 sizeB, sizeD;
		real8 sizeT1, sizeT2;  
		real8 n1X,n1Y,n1Z;								// slip plane1 : n1 [ 0  -1  1]
		real8 n2X,n2Y,n2Z;								// slip plane2 : n2 [-1   0  1]
		real8 radius;		radius = param->cyl_radius;
		n1X=0.0e0;			n1Y=-1.0e0/sqrt(2.0e0);		n1Z = 1.0e0/sqrt(2.0e0);	
		n2X=-1.0e0/sqrt(2.0e0);	n2Y=0.0e0;				n2Z = 1.0e0/sqrt(2.0e0);
		real8 fX,fY,fZ;									// Force on the node
		fX=node->fX; 	fY=node->fY;		fZ=node->fZ;		
		real8 fXn1,fYn1,fZn1;							// Force on the node projected to plane1. 
		real8 fXn2,fYn2,fZn2;							// Force on the node projected on plane2
		real8 normFn1;									// norm of Fn1
		real8 normFn2;									// norm of Fn2
		real8 t1,t2,treal;						// To move surface nodes along intesection line of two planes
		real8 alpha;
 		x1=node->x; 	y1=node->y; 	z1=node->z; 
 		x2=nbr->x; 		y2=nbr->y; 		z2=nbr->z; 
		dx=x1-x2;		dy=y1-y2;		dz=z1-z2;
		sizeD = sqrt(dx*dx + dy*dy + dz*dz);
	    dx/=sizeD;		dy/=sizeD;		dz/=sizeD;				// Check if the dX is unit vector. 
		bx = node->burgX[0];
		by = node->burgY[0];
		bz = node->burgZ[0];
		sizeB = sqrt(bx*bx + by*by + bz*bz);
	    bx/=sizeB;		by/=sizeB;		bz/=sizeB;				// Check if the burger vector is unit vector. 
		screw_character=fabs(dx*bx+dy*by+dz*bz);
		if(screw_character > tolerance)
		{ // if surface segment has screw character. 
	      // Move dislocation at the surface such that it is located along the intersection of two planes.
			alpha = sqrt((by*by)*(radius-x2)*(radius+x2)-2.0e0*(bx*by*x2*y2)+(bx*bx)*(radius-y2)*(radius+y2));
			t1=-((bx*x2+by*y2)+alpha)/(bx*bx+by*by);
			t2=-((bx*x2+by*y2)-alpha)/(bx*bx+by*by);
		  // Calculate forces on two planes
			fXn1 = fX-(fX*n1X+fY*n1Y+fZ*n1Z)*n1X;
			fYn1 = fY-(fX*n1X+fY*n1Y+fZ*n1Z)*n1Y;
			fZn1 = fZ-(fX*n1X+fY*n1Y+fZ*n1Z)*n1Z;
			normFn1 = sqrt(fXn1*fXn1+fYn1*fYn1+fZn1*fZn1);
			fXn2 = fX-(fX*n2X+fY*n2Y+fZ*n2Z)*n2X;
			fYn2 = fY-(fX*n2X+fY*n2Y+fZ*n2Z)*n2Y;
			fZn2 = fZ-(fX*n2X+fY*n2Y+fZ*n2Z)*n2Z;
			normFn2 = sqrt(fXn2*fXn2+fYn2*fYn2+fZn2*fZn2);
		// Compute tan1, tan2
			tan1x = -y1/sqrt(2);
//			tan1y =  x1/sqrt(2);
			tan1y =  0e0;
			tan1z =  x1/sqrt(2);
			if (z2>z1 && tan1z<0)
			{	tan1x=-tan1x;	tan1y=-tan1y;	tan1z=-tan1z;	}		
			if (z2<z1 && tan1z>0)
			{	tan1x=-tan1x;	tan1y=-tan1y;	tan1z=-tan1z;	}		
//			tan2x = -y1/sqrt(2);
			tan2x = 0e0;
			tan2y =  x1/sqrt(2);
			tan2z = -y1/sqrt(2);
			if ((z2>z1) && (tan2z<0.0e0))
			{	tan2x=-tan2x;	tan2y=-tan2y;	tan2z=-tan2z;	}		
			if ((z2<z1) && (tan2z>0.0e0))
			{	tan2x=-tan2x;	tan2y=-tan2y;	tan2z=-tan2z;	}		
			sizeT1 = sqrt(tan1x*tan1x + tan1y*tan1y + tan1z*tan1z);
		    tan1x/=sizeT1;		tan1y/=sizeT1;		tan1z/=sizeT1;
			sizeT2 = sqrt(tan2x*tan2x + tan2y*tan2y + tan2z*tan2z);
		    tan2x/=sizeT2;		tan2y/=sizeT2;		tan2z/=sizeT2;
		// Surface cross slip occurs when
 		//	1. if dot(tan1, Fn1)>0 && dot(tan2,Fn2)<0), choose n1 as a slip plane
 		//	2. if dot(tan1, Fn1)<0 && dot(tan2,Fn2)>0), choose n2 as a slip plane
			real8 dotTan1Fn1; 			//dot(tan1,Fn1)
   			dotTan1Fn1= tan1x*fXn1 + tan1y*fYn1 + tan1z*fZn1;
			real8 dotTan2Fn2;			//dot(tan2,Fn2)
   			dotTan2Fn2= tan2x*fXn2 + tan2y*fYn2 + tan2z*fZn2;
			if (dotTan1Fn1>0 && dotTan2Fn2<0)
			{	
				node->nx[0]=n1X;		node->ny[0]=n1Y;		node->nz[0]=n1Z;
				if (nbr1->constraint == CYLINDER_SURFACE_NODE)
				{	// nbr1  is the surface node. 
					nbr->nx[0]=node->nx[0];		nbr->ny[0]=node->ny[0];		nbr->nz[0]=node->nz[0];
				}
				else // nbr2 is the surface node.
				{
					nbr->nx[1]=node->nx[0];		nbr->ny[1]=node->ny[0];		nbr->nz[1]=node->nz[0];
				}
			}
			else if (dotTan1Fn1<0 && dotTan2Fn2>0)
			{
				node->nx[0]=n2X;		node->ny[0]=n2Y;		node->nz[0]=n2Z;
				if (nbr1->constraint == CYLINDER_SURFACE_NODE)
				{	// nbr1  is the surface node. 
					nbr->nx[0]=node->nx[0];		nbr->ny[0]=node->ny[0];		nbr->nz[0]=node->nz[0];
				}
				else // nbr2 is the surface node.
				{
					nbr->nx[1]=node->nx[0];		nbr->ny[1]=node->ny[0];		nbr->nz[1]=node->nz[0];
				}
			}
		}
	}
}	
#endif 



#if 0

/*
 *    (iryu/2011.08.10)
 *   < Surface cross slip >
 *    Version 2-2 : Consider the effect of image stress 
 *                  Two plane version (0  1 -1) (1  0 -1)
 *    Algorithm:
 *                1. Compute tangent vectors(t1,t2) with respect to n1, n2.
 *                   - t1,t2 are the tangent vectors along the trace of ellipse passing through the surface node. 
 *                   - t1,t2 points to the direction in which dislocation shrinks 
 *                2. Get P-K force
 *                3. if dot(t1, P-K force>0), choose n1 as a slip plane
 *                   if dot(t2, P-K force<0), choose n2 as a slip plane 
 */
if(node->constraint == CYLINDER_SURFACE_NODE)
{
		Node_t  *nbr, *nbr1, *nbr2;
		nbr=GetNeighborNode(home,node,0);                //Neighbor of surface node 
	if ((node->numNbrs == 1) && (nbr->numNbrs == 2) )
	{
		nbr1=GetNeighborNode(home,nbr,0);                //1st Neighbor of nearest neighbor node 
		nbr2=GetNeighborNode(home,nbr,1);                //2nd Neighbor of nearest neighbor node
		real8 tolerance;			tolerance = 0.95;	
		real8 screw_character;							// Value which say how surface segment is similar to screw character
		real8 x1,y1,z1;									// Surface node coordinates 
		real8 x2,y2,z2;									// Surface neighbor node coordinates 
		real8 dx,dy,dz;								 
		real8 bx,by,bz;									// Burgers vector of surface node 
		real8 normB; 	real8 normD;
		real8 radius,R;		radius = param->cyl_radius;
		real8 PKfx,PKfy,PKfz;									// Force on the node
		real8 Imgfx,Imgfy,Imgfz;						// Image force on the node
		real8 fx,fy,fz;									// Force on the node
		real8 t1,t2,treal;										// To move surface nodes along intesection line of two planes
		real8 alpha, alpha1, alpha2;
		real8 dotT1, dotT2 ; 		
		real8 dotB1, dotB2, dotB3, dotB4;
		real8 TX1, TX2, TY1, TY2, TZ1, TZ2;
		real8 normT1, normT2;
		real8 NX1, NX2, NY1, NY2, NZ1, NZ2;
 		x1=node->x; 	y1=node->y; 	z1=node->z; 
 		x2=nbr->x; 		y2=nbr->y; 		z2=nbr->z; 
		dx=x1-x2;		dy=y1-y2;		dz=z1-z2;		//Line sense vector of the surface segment(pointing outwards)
		normD = sqrt(dx*dx + dy*dy + dz*dz);
	    dx/=normD;	dy/=normD;	dz/=normD;	//Line sense vector of the surface segment(normalized)
		bx = node->burgX[0];	by = node->burgY[0];	bz = node->burgZ[0];	//Burgers vector of the surface segment
		normB = sqrt(bx*bx + by*by + bz*bz);
		bx/=normB;	by/=normB;	bz/=normB;	//Burgers vector of the surface segment	(normalized)
		screw_character=fabs(dx*bx + dy*by + dz*bz);		//Parameter to show if surface character is screw(if 1=screw)

		//Check
//		printf("Surface node (%d,%d)\n", node->myTag.domainID, node->myTag.index);
//		printf("Neighbor node (%d,%d)\n", nbr->myTag.domainID, nbr->myTag.index);
		printf("node(x,y,z)=(%e,%e, %e)\n", node->x,node->y,node->z);
		printf("nbr(x,y,z)=(%e,%e, %e)\n", nbr->x,nbr->y,nbr->z);
//		printf("dx : %e, dy : %e, dz : %e \n",  dx,dy,dz);
//		printf("bx : %e, by : %e, bz : %e \n",  bx,by,bz);
//		printf("screw_character : %e\n",screw_character);

		if(screw_character > tolerance)
		{ // if surface segment has screw character. 
	      // Move dislocation at the surface such that it is located along the intersection of two planes.
			printf("Surface node %d  have screw character! \n", node->myTag.index);
			R= radius; 
			alpha1 =-(bx*x2 + by*y2 + sqrt(R*R*bx*bx + R*R*by*by - bx*bx*y2*y2 + 2.*bx*by*x2*y2 - by*by*x2*x2))/(bx*bx + by*by);
			alpha2 =-(bx*x2 + by*y2 - sqrt(R*R*bx*bx + R*R*by*by - bx*bx*y2*y2 + 2.*bx*by*x2*y2 - by*by*x2*x2))/(bx*bx + by*by);


			printf("alpha1: %e, alpha2 : %e\n",alpha1, alpha2);
			if (fabs(alpha1)>=fabs(alpha2)) 
			{// Take smaller alpha, since it means closer point to nbr
				printf("alpha = alpha2, becuase alpha2 is smaller");
				alpha=alpha2;
			}
			else 
			{
				printf("alpha = alpha1,because alpha1 is smaller");
				alpha=alpha1;
			}
			node->x=x2+alpha*bx;
			node->y=y2+alpha*by;
	  		node->z=z2+alpha*bz;
			x1=node->x; 	y1=node->y; 	z1=node->z; 

		//Check if the projection is right
//		printf("Surface node (%d,%d)\n", node->myTag.domainID, node->myTag.index);
		printf("node(x,y,z)=(%e,%e, %e)\n", x1,y1,z1);
		printf("r^2 = %e, x^2+y^2 = %e\n",  radius*radius , x1*x1+y1*y1);
		printf("difference = %e\n",   radius*radius- (x1*x1+y1*y1));



			dotB1= fabs(bx*(1.0e0/sqrt(3.0)) + by*(1.0e0/sqrt(3.0)) + bz*(1.0e0/sqrt(3.0))); 
			dotB2= fabs(bx*(1.0e0/sqrt(3.0)) + by*(1.0e0/sqrt(3.0)) + bz*(-1.0e0/sqrt(3.0))); 
			dotB3= fabs(bx*(-1.0e0/sqrt(3.0)) + by*(1.0e0/sqrt(3.0)) + bz*(1.0e0/sqrt(3.0))); 
			dotB4= fabs(bx*(1.0e0/sqrt(3.0)) + by*(-1.0e0/sqrt(3.0)) + bz*(1.0e0/sqrt(3.0))); 
            printf("dotB1= %e, dotB2= %e, dotB3= %e, dotB4= %e\n", dotB1,dotB2,dotB3,dotB4);

			// Select 2 slip planes which contain Burgers vector.
			if (dotB1 >= tolerance)
			{// if b=[1 1 1]/sqrt(3)
				printf("B1=(1 1 1) or (-1 -1 -1) \n");
				NX1 = 1.0e0/sqrt(2.0e0);    NY1 = 0.0e0; 				NZ1 =-1.0e0/sqrt(2.0e0);
				NX2 = 0.0e0;				NY2 = 1.0e0/sqrt(2.0e0);	NZ2 =-1.0e0/sqrt(2.0e0);
				printf("N1 =(%e,%e,%e)\n", NX1,NY1,NZ1);
				printf("N2 =(%e,%e,%e)\n", NX2,NY2,NZ2);
			}		
			else if (dotB2 >= tolerance)
			{// if b=[1 1 -1]/sqrt(3)
				printf("B2=(1 1 -1) or (-1 -1 1)  \n");
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 = 0.0e0;				NZ1 = 1.0e0/sqrt(2.0e0);
				NX2 = 0.0e0;			    NY2 = 1.0e0/sqrt(2.0e0);	NZ2 = 1.0e0/sqrt(2.0e0);
				printf("N1 =(%e,%e,%e)\n", NX1,NY1,NZ1);
				printf("N2 =(%e,%e,%e)\n", NX2,NY2,NZ2);
			}		
			else if (dotB3 >= tolerance)
			{// if b=[-1 1 1]/sqrt(3)
				printf("B3=(-1 1 1) or (1 -1 -1)  \n");
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 = 0.0e0;				NZ1 = 1.0e0/sqrt(2.0e0);
				NX2 = 0.0e0;			    NY2 = 1.0e0/sqrt(2.0e0);	NZ2 =-1.0e0/sqrt(2.0e0);
				printf("N1 =(%e,%e,%e)\n", NX1,NY1,NZ1);
				printf("N2 =(%e,%e,%e)\n", NX2,NY2,NZ2);
			}		
			else if (dotB4 >= tolerance)
			{// if b=[1 -1 1]/sqrt(3)
				printf("B4=(1 -1 1) or (-1 1 -1)  \n");
				NX1 = 1.0e0/sqrt(2.0e0); 	NY1 = 0.0e0;				NZ1 =-1.0e0/sqrt(2.0e0);
				NX2 = 0.0e0;			    NY2 = 1.0e0/sqrt(2.0e0);	NZ2 = 1.0e0/sqrt(2.0e0);
				printf("N1 =(%e,%e,%e)\n", NX1,NY1,NZ1);
				printf("N2 =(%e,%e,%e)\n", NX2,NY2,NZ2);
			}		
			else
			{
				Fatal("B should be one of them in BCC glide plane!!");
//				printf("Warning : B should be one of them in BCC glide plane! \n");
					
			}
			// Compute tangent vector for each slip plane
			TX1 = -y1-(-y1*NX1+x1*NY1)*NX1;		TY1 = x1-(-y1*NX1+x1*NY1)*NY1;		TZ1 = -(-y1*NX1+x1*NY1)*NZ1;			
			TX2 = -y1-(-y1*NX2+x1*NY2)*NX2;		TY2 = x1-(-y1*NX2+x1*NY2)*NY2;		TZ2 = -(-y1*NX2+x1*NY2)*NZ2;			
			normT1 = sqrt(TX1*TX1 + TY1*TY1 + TZ1*TZ1);			//Normalize them. 
			normT2 = sqrt(TX2*TX2 + TY2*TY2 + TZ2*TZ2);
			TX1 /=normT1;	TY1 /=normT1;	TZ1 /=normT1;	   
			TX2 /=normT2;	TY2 /=normT2;	TZ2 /=normT2;	   
			if ((z2>z1) && (TZ1<0.0e0))
			{	TX1=-TX1;	TY1=-TY1;	TZ1=-TZ1;	}		
			if ((z2<z1) && (TZ1>0.0e0))
			{	TX1=-TX1;	TY1=-TY1;	TZ1=-TZ1;	}		
			if ((z2>z1) && (TZ2<0.0e0))
			{	TX2=-TX2;	TY2=-TY2;	TZ2=-TZ2;	}		
			if ((z2<z1) && (TZ2>0.0e0))
			{	TX2=-TX2;	TY2=-TY2;	TZ2=-TZ2;	}		
/*
			if (z1>=z2) 
			{
				if (TZ1>=0e0)	
				{
					TX1=-TX1;	TY1=-TY1;	TZ1=-TZ1;
				}
				if (TZ2>=0e0)	
				{
					TX2=-TX2;	TY2=-TY2;	TZ2=-TZ2;
				}
			}		
			else 
			{
				if (TZ1<0e0)
				{
					TX1=-TX1;	TY1=-TY1;	TZ1=-TZ1;
				}
				if (TZ2<0e0)
				{
					TX2=-TX2;	TY2=-TY2;	TZ2=-TZ2;
				}
			}	
*/	
		// Surface cross slip occurs when
 		//	1. if dot(T1,P-K force) >= dot(T2,P-K force), choose n1 as a slip plane
 		//	2. else if dot(T1,P-K force) < dot(T2,P-K force), choose n2 as a slip plane
			// Calculate P-K force
			PKfx=node->fX; 	PKfy=node->fY;		PKfz=node->fZ;		
			Imgfx=0.0e0;	Imgfy=0.0e0;		Imgfz=1.0e10;
			// Calculate the total force = Image force + P-K force
			fx=Imgfx+PKfx;	fy=Imgfy+PKfy;	fz=Imgfz+PKfz; 

   			dotT1= TX1*fx + TY1*fy + TZ1*fz; 
   			dotT2= TX2*fx + TY2*fy + TZ2*fz; 

			if (dotT1 >= dotT2 )
			{	
				node->nx[0]=NX1;		node->ny[0]=NY1;		node->nz[0]=NZ1;
				if (nbr1->constraint == CYLINDER_SURFACE_NODE)
				{	// nbr1  is the surface node. 
					nbr->nx[0]=node->nx[0];		nbr->ny[0]=node->ny[0];		nbr->nz[0]=node->nz[0];
				}
				else // nbr2 is the surface node.
				{
					nbr->nx[1]=node->nx[0];		nbr->ny[1]=node->ny[0];		nbr->nz[1]=node->nz[0];
				}
			}
			else if (dotT1 < dotT2)
			{
				node->nx[0]=NX2;		node->ny[0]=NY2;		node->nz[0]=NZ2;
				if (nbr1->constraint == CYLINDER_SURFACE_NODE)
				{	// nbr1  is the surface node. 
					nbr->nx[0]=node->nx[0];		nbr->ny[0]=node->ny[0];		nbr->nz[0]=node->nz[0];
				}
				else // nbr2 is the surface node.
				{
					nbr->nx[1]=node->nx[0];		nbr->ny[1]=node->ny[0];		nbr->nz[1]=node->nz[0];
				}
			}
		}
	}
}
			
#endif 

#if 0
/*
 *    (iryu/2011.08.02)
 *   < Surface cross slip >
 *    Version 3 : 3 slip planes 
 *    Conditions: 1. dislocation should be similar to screw. (line sence vector cross burger vector>0.9)
 *                2. Seaching surface node(node)
 *                3. Check the coordinate of the neighbor node(nbr)
 *                4. Take the slip plane on which amount of the force is the maximum
 *    Algorithm:
 *                1. Compute the total force
 *                   F(total) = F(image) + F(loading)
 *                   - F(image) makes dislocation shorter in length. 
 *                     if (node->z >= nbr->z)   F image : -Z direction
 *                     if (node->z <  nbr->z)   F image : +Z direction
 *                2. Compute projected forces to each planes.(Fn1,Fn2,Fn3) 
 *                3. if ((fabs(Fn1)>= fabs(Fn2))&& (fabs(Fn1)>= fabs(Fn3))) : choose n1 as a slip plane
 *              else if ((fabs(Fn2) > fabs(Fn1))&& (fabs(Fn2)>= fabs(Fn3))) : choose n2 as a slip plane
 *              else if ((fabs(Fn3) > fabs(Fn1))&& (fabs(Fn3)> fabs(Fn2)))  : choose n3 as a slip plane
 */
	if(node->constraint == CYLINDER_SURFACE_NODE)
	{
		real8 radius;		radius = param->cyl_radius; // cylinder radius
		real8 R;
		Node_t  *nbr, *nbr1, *nbr2, *nbr3, *nbr4;
		real8 tolerance1;			tolerance1 = 0.95;	
		real8 tolerance2;			tolerance2 = 0.01;	
		real8 tolerance3;			tolerance3 = 0.01;	
		real8 screw_character;							// Value which say how surface segment is similar to screw character
		real8 x1,y1,z1;									// Surface node coordinates(node) 
		real8 x2,y2,z2;									// Surface neighbor node coordinates (nbr)
		real8 bx,by,bz;									// Burgers vector of surface segment	
		real8 dx,dy,dz;									// Line sense vector for surface segment(outward)	
		real8 normB, normD;
		int surfarm;									// Surface arm id of nbr 
//______Slip planes____________________________________		
		real8 nx1,ny1,nz1;								// slip plane1 : n1 [ 1  1  0]
		real8 nx2,ny2,nz2;								// slip plane2 : n2 [ 1 -1  0]
		real8 nx3,ny3,nz3;								// slip plane3 : n3 [ 1  0  1]
		real8 nx4,ny4,nz4;								// slip plane4 : n4 [ 1  0 -1]
		real8 nx5,ny5,nz5;								// slip plane5 : n5 [ 0  1  1]
		real8 nx6,ny6,nz6;								// slip plane6 : n6 [ 0  1 -1]
		nx1 = 1.0e0/sqrt(2.0e0);	ny1 = 1.0e0/sqrt(2.0e0);	nz1 = 0.0e0;	
		nx2 = 1.0e0/sqrt(2.0e0);	ny2 =-1.0e0/sqrt(2.0e0);	nz2 = 0.0e0;	
		nx3 = 1.0e0/sqrt(2.0e0);	ny3 = 0.0e0;				nz3 = 1.0e0/sqrt(2.0e0);
		nx4 = 1.0e0/sqrt(2.0e0);	ny4 = 0.0e0;				nz4 =-1.0e0/sqrt(2.0e0);
		nx5 = 0.0e0; 				ny5 = 1.0e0/sqrt(2.0e0);	nz5 = 1.0e0/sqrt(2.0e0);	
		nx6 = 0.0e0; 				ny6 = 1.0e0/sqrt(2.0e0);	nz6 =-1.0e0/sqrt(2.0e0);	
		real8 alpha, alpha1, alpha2;					// scaling factor
		real8 NX1,NY1,NZ1;								// slip plane1 which contains burgers vector
		real8 NX2,NY2,NZ2;								// slip plane2 which contains burgers vector 
		real8 NX3,NY3,NZ3;								// slip plane3 which contains burgers vector 
//______Force _________________________________________		
		real8 PKfx,PKfy,PKfz;							// P-K force on the node
		real8 Imgfx,Imgfy,Imgfz;						// Image force 
		real8 fx,fy,fz;									// Total force = Image force + P-K force 
		real8 fxn1,fyn1,fzn1;							// Force on the node projected to plane1
		real8 fxn2,fyn2,fzn2;							// Force on the node projected on plane2
		real8 fxn3,fyn3,fzn3;							// Force on the node projected on plane3
		real8 normFn1;									// norm of Fn1
		real8 normFn2;									// norm of Fn2
		real8 normFn3;									// norm of Fn3
//////////////////////////////////////////////////////////
		nbr=GetNeighborNode(home,node,0);                //Neighbor of surface node 
 		x1=node->x; 	y1=node->y; 	z1=node->z; 
 		x2=nbr->x; 		y2=nbr->y; 		z2=nbr->z; 
		dx=x1-x2;		dy=y1-y2;		dz=z1-z2;		//Line sense vector of the surface segment(pointing outwards)
		normD = sqrt(dx*dx + dy*dy + dz*dz);
	    dx=dx/normD;	dy=dy/normD;	dz=dx/normD;	//Line sense vector of the surface segment(normalized)
		bx = node->burgX[0];	by = node->burgY[0];	bz = node->burgZ[0];	//Burgers vector of the surface segment
		normB = sqrt(bx*bx + by*by + bz*bz);
	    bx=bx/normB;	by=by/normB;	bz=bz/normB;	//Burgers vector of the surface segment	(normalized)
		screw_character=fabs(dx*bx+dy*by+dz*bz);		//Parameter to show if surface character is screw(if 1=screw)
		if(screw_character > tolerance1)
		{ // if surface segment has screw character. 
	      // Move dislocation at the surface such that it is along the burgers vectors
			R= radius; 
			alpha1 =-(bx*x2 + by*y2 + sqrt(R*R*bx*bx + R*R*by*by - bx*bx*y2*y2 + 2.*bx*by*x2*y2 - by*by*x2*x2))/(bx*bx + by*by);
			alpha2 =-(bx*x2 + by*y2 - sqrt(R*R*bx*bx + R*R*by*by - bx*bx*y2*y2 + 2.*bx*by*x2*y2 - by*by*x2*x2))/(bx*bx + by*by);
			if (fabs(alpha1)>=fabs(alpha2)) 
			{// Take smaller alpha, since it means closer point to nbr
				alpha=alpha2;
			}
			else 
			{
				alpha=alpha1;
			}
//			node->x=x2+alpha*bx;
//			node->y=y2+alpha*by;
//			node->z=z2+alpha*bz;
//			x1=node->x; 	y1=node->y; 	z1=node->z; 
			// Calculate image forces
			Imgfx=0.0;			Imgfy=0.0;			
			Imgfz=0.0e0;						///// Need to specify. !!!!!!!!!!!!!!!!!!!!!!!!!!!!
			if (z2>=z1)
			{
				Imgfz*=1.0;						// Upward
			}
			else if (z2<z1)
			{	
				Imgfz*=-1.0;					//Downward
			}
			// Calculate P-K force
			PKfx=node->fX;	PKfy=node->fY;	PKfz=node->fZ;	

			// Calculate the total force = Image force + P-K force
			fx=Imgfx+PKfx;	fy=Imgfy+PKfy;	fz=Imgfz+PKfz; 

			// Select 3 slip planes which contain Burgers vector.
			if (fabs(bx-(1.0e0/sqrt(3)))+fabs(by-(1.0e0/sqrt(3)))+fabs(bz-(1.0e0/sqrt(3)))<=tolerance2 
			|| (fabs(bx+(1.0e0/sqrt(3)))+fabs(by+(1.0e0/sqrt(3)))+fabs(bz+(1.0e0/sqrt(3)))<=tolerance2))
			{// if b=[1 1 1]/sqrt(3)
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 =-1.0e0/sqrt(2.0e0);	NZ1 = 0.0e0 ;
				NX2 = 0.0e0;				NY2 = 1.0e0/sqrt(2.0e0);	NZ2 =-1.0e0/sqrt(2.0e0);
				NX3 = 1.0e0/sqrt(2.0e0);    NY3 = 0.0e0; 				NZ3 =-1.0e0/sqrt(2.0e0);
			}		
			else if (fabs(bx-(1.0e0/sqrt(3)))+fabs(by-(1.0e0/sqrt(3)))+fabs(bz+(1.0e0/sqrt(3)))<=tolerance2 
			||      (fabs(bx+(1.0e0/sqrt(3)))+fabs(by+(1.0e0/sqrt(3)))+fabs(bz-(1.0e0/sqrt(3)))<=tolerance2))
			{// if b=[1 1 -1]/sqrt(3)
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 =-1.0e0/sqrt(2.0e0);	NZ1 = 0.0e0 ;
				NX2 = 1.0e0/sqrt(2.0e0);	NY2 = 0.0e0;				NZ2 = 1.0e0/sqrt(2.0e0);
				NX3 = 0.0e0;			    NY3 = 1.0e0/sqrt(2.0e0);	NZ3 = 1.0e0/sqrt(2.0e0);
			}		
			else if (fabs(bx+(1.0e0/sqrt(3)))+fabs(by-(1.0e0/sqrt(3)))+fabs(bz-(1.0e0/sqrt(3)))<=tolerance2 
			||      (fabs(bx-(1.0e0/sqrt(3)))+fabs(by+(1.0e0/sqrt(3)))+fabs(bz+(1.0e0/sqrt(3)))<=tolerance2))
			{// if b=[-1 1 1]/sqrt(3)
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 = 1.0e0/sqrt(2.0e0);	NZ1 = 0.0e0 ;
				NX2 = 1.0e0/sqrt(2.0e0);	NY2 = 0.0e0;				NZ2 = 1.0e0/sqrt(2.0e0);
				NX3 = 0.0e0;			    NY3 = 1.0e0/sqrt(2.0e0);	NZ3 =-1.0e0/sqrt(2.0e0);
			}		
			else if (fabs(bx-(1.0e0/sqrt(3)))+fabs(by+(1.0e0/sqrt(3)))+fabs(bz-(1.0e0/sqrt(3)))<=tolerance2 
			||      (fabs(bx+(1.0e0/sqrt(3)))+fabs(by-(1.0e0/sqrt(3)))+fabs(bz+(1.0e0/sqrt(3)))<=tolerance2))
			{// if b=[1 -1 1]/sqrt(3)
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 = 1.0e0/sqrt(2.0e0);	NZ1 = 0.0e0 ;
				NX2 = 1.0e0/sqrt(2.0e0); 	NY2 = 0.0e0;				NZ2 =-1.0e0/sqrt(2.0e0);
				NX3 = 0.0e0;			    NY3 = 1.0e0/sqrt(2.0e0);	NZ3 = 1.0e0/sqrt(2.0e0);
			}		

			// Calculate the projected force on slip planes
			fxn1 = fx-(fx*NX1+fy*NY1+fz*NZ1)*NX1;
			fyn1 = fy-(fx*NX1+fy*NY1+fz*NZ1)*NY1;
			fzn1 = fz-(fx*NX1+fy*NY1+fz*NZ1)*NZ1;

			fxn2 = fx-(fx*NX2+fy*NY2+fz*NZ2)*NX2;
			fyn2 = fy-(fx*NX2+fy*NY2+fz*NZ2)*NY2;
			fzn2 = fz-(fx*NX2+fy*NY2+fz*NZ2)*NZ2;

			fxn3 = fx-(fx*NX3+fy*NY3+fz*NZ3)*NX3;
			fyn3 = fy-(fx*NX3+fy*NY3+fz*NZ3)*NY3;
			fzn3 = fz-(fx*NX3+fy*NY3+fz*NZ3)*NZ3;

			normFn1=sqrt(fxn1*fxn1+fyn1*fyn1+fzn1*fzn1); 
			normFn2=sqrt(fxn2*fxn2+fyn2*fyn2+fzn2*fzn2); 
			normFn3=sqrt(fxn3*fxn3+fyn3*fyn3+fzn3*fzn3); 

			// Find arm #(surfarm) of nbr which is the link to the surface node
			if (nbr->numNbrs == 2)
			{
				nbr1=GetNeighborNode(home,nbr,0);           //1st neighbor of nbr 
				nbr2=GetNeighborNode(home,nbr,1);           //2nd neighbor of nbr 
				if (fabs((nbr1->x)-(node->x))+fabs((nbr1->y)-(node->y))+fabs((nbr1->z)-(node->z))< tolerance3)
				{	//nbr1 = node(surface node)
					surfarm =0;
				}
				else if (fabs((nbr2->x)-(node->x))+fabs((nbr2->y)-(node->y))+fabs((nbr2->z)-(node->z))< tolerance3) 
				{	//nbr2 = node(surface node)
					surfarm =1;
				}
			}		
			else if (nbr->numNbrs == 3)
			{
				nbr1=GetNeighborNode(home,nbr,0);           //1st neighbor of nbr 
				nbr2=GetNeighborNode(home,nbr,1);           //2nd neighbor of nbr 
				nbr3=GetNeighborNode(home,nbr,2);           //3rd neighbor of nbr 
				if (fabs((nbr1->x)-(node->x))+fabs((nbr1->y)-(node->y))+fabs((nbr1->z)-(node->z))< tolerance3)
				{	//nbr1 = node(surface node)
					surfarm =0;
				}
				else if (fabs((nbr2->x)-(node->x))+fabs((nbr2->y)-(node->y))+fabs((nbr2->z)-(node->z))< tolerance3) 
				{	//nbr2 = node(surface node)
					surfarm =1;
				}
				else if (fabs((nbr3->x)-(node->x))+fabs((nbr3->y)-(node->y))+fabs((nbr3->z)-(node->z))< tolerance3) 
				{	//nbr3 = node(surface node)
					surfarm =2;
				}
			}
			else if (nbr->numNbrs == 4)
			{
				nbr1=GetNeighborNode(home,nbr,0);           //1st neighbor of nbr 
				nbr2=GetNeighborNode(home,nbr,1);           //2nd neighbor of nbr 
				nbr3=GetNeighborNode(home,nbr,2);           //3rd neighbor of nbr 
				nbr4=GetNeighborNode(home,nbr,3);           //4rd neighbor of nbr 
				if (fabs((nbr1->x)-(node->x))+fabs((nbr1->y)-(node->y))+fabs((nbr1->z)-(node->z))< tolerance3)
				{	//nbr1 = node(surface node)
					surfarm =0;
				}
				else if (fabs((nbr2->x)-(node->x))+fabs((nbr2->y)-(node->y))+fabs((nbr2->z)-(node->z))< tolerance3) 
				{	//nbr2 = node(surface node)
					surfarm =1;
				}
				else if (fabs((nbr3->x)-(node->x))+fabs((nbr3->y)-(node->y))+fabs((nbr3->z)-(node->z))< tolerance3) 
				{	//nbr3 = node(surface node)
					surfarm =2;
				}
				else if (fabs((nbr4->x)-(node->x))+fabs((nbr4->y)-(node->y))+fabs((nbr4->z)-(node->z))< tolerance3) 
				{	//nbr4 = node(surface node)
					surfarm =3;
				}
			}

			// Choose slip plane based on the magnitude of the force projected on them.
			if ((normFn1>= normFn2) && (normFn1>= normFn3) )
			{	// slip plane = n1
				node->nx[0]=NX1;		node->ny[0]=NY1;		node->nz[0]=NZ1;
			}		
			else if ((normFn2 > normFn1) && (normFn2>= normFn3) )
			{	// slip plane = n2	
				node->nx[0]=NX2;		node->ny[0]=NY2;		node->nz[0]=NZ2;
			}		
			else if ((normFn3 > normFn1) && (normFn3 > normFn2) )
			{	// slip plane = n3	
				node->nx[0]=NX3;		node->ny[0]=NY3;		node->nz[0]=NZ3;
			}
			nbr->nx[surfarm]=node->nx[0];		
			nbr->ny[surfarm]=node->ny[0];
			nbr->nz[surfarm]=node->nz[0];
		}
	}
			
#endif 



#if 1
/*
 *    (iryu/2011.08.05)
 *   < Surface cross slip >
 *    Version 4 : 3 slip planes 
 *    Conditions: 1. dislocation should be similar to screw. (line sence vector cross burger vector>0.9)
 *                2. Seaching surface node(node)
 *                3. Check the coordinate of the neighbor node(nbr)
 *                4. Compute tangent vector for each slip plane
 *                5. Compute force projected along the tangent vectors.
 *                6. Choose slip plane based on the amount of projected force.
 *    Algorithm:
 *                1. Compute the total force
 *                   F(total) = F(image) + F(loading)
 *                   - F(image) makes dislocation shorter in length. 
 *                     if (node->z >= nbr->z)   F image : -Z direction
 *                     if (node->z <  nbr->z)   F image : +Z direction
 *		  2. Compute tangent vector for 3 slip planes(tn1, tn2, tn3)
 *                3. Compute projected forces along tangent vector .(Ft1,Ft2,Ft3) 
 *                3. if ((fabs(Ft1)>= fabs(Ft2))&& (fabs(Ft1)>= fabs(Ft3))) : choose n1 as a slip plane
 *              else if ((fabs(Ft2) > fabs(Ft1))&& (fabs(Ft2)>= fabs(Ftn3))): choose n2 as a slip plane
 *              else if ((fabs(Ft3) > fabs(Ft1))&& (fabs(Ft3)> fabs(Ft2)))  : choose n3 as a slip plane
 */
if(node->constraint == CYLINDER_SURFACE_NODE)
{
	int NOFLIP = 0;		//If Burgers vector is not typical type of BCC, flip does not occur(NOFLIP=1)

	if (DEBUG_PRINT  == 1) 
	{	printf("p---Start of Surface Cross slip---q \n");	}
	Node_t  *nbr, *nbr1, *nbr2;
	nbr=GetNeighborNode(home,node,0);                //Neighbor of surface node
	if (DEBUG_PRINT  == 1)
	{ 
		printf("Surface node (%d,%d)\n", node->myTag.domainID, node->myTag.index);
		printf("Neighbor node (%d,%d)\n", nbr->myTag.domainID, nbr->myTag.index);
		printf("Neighbor has %d arms \n", nbr->numNbrs);
	}
	 
	if ((node->numNbrs == 1) && (nbr->numNbrs == 2) )
	{	
		nbr1=GetNeighborNode(home,nbr,0);                //1st Neighbor of nearest neighbor node 
		nbr2=GetNeighborNode(home,nbr,1);                //2nd Neighbor of nearest neighbor node
		real8 radius;		radius = param->cyl_radius; // cylinder radius
		real8 R;
		real8 tolerance;			tolerance = 0.90;	
//		real8 tolerance2;			tolerance2 = 0.01;	
//		real8 tolerance3;			tolerance3 = 0.01;	
		real8 screw_character;				
		real8 x1,y1,z1;							// Surface node coordinates(node) 
		real8 x2,y2,z2;							// Surface neighbor node coordinates (nbr)
		real8 bx,by,bz;							// Burgers vector of surface segment	
		real8 dx,dy,dz;							// Line sense vector for surface segment(outward)	
		real8 normB, normD;
		int surfarm;							// Surface arm id of nbr 
//______Slip planes____________________________________		
		real8 alpha, alpha1, alpha2;					// scaling factor
		real8 dotB1, dotB2, dotB3, dotB4;
		real8 NX1,NY1,NZ1;						// slip plane1 which contains burgers vector
		real8 NX2,NY2,NZ2;						// slip plane2 which contains burgers vector 
		real8 NX3,NY3,NZ3;						// slip plane3 which contains burgers vector
		real8 TX1, TY1,TZ1;						// Tangent vector for N1
		real8 TX2, TY2,TZ2;						// Tangent vector for N2
		real8 TX3, TY3,TZ3;						// Tangent vector for N3 
		real8 normT1, normT2, normT3;					// norm of Ti
//______Force _________________________________________		
		real8 PKfx,PKfy,PKfz;						// P-K force on the node
		real8 Imgfx,Imgfy,Imgfz;					// Image force 
		real8 fx,fy,fz;							// Total force = Image force + P-K force 
		real8 fxT1,fyT1,fzT1;						// Force along T1
		real8 fxT2,fyT2,fzT2;						// Force along T2
		real8 fxT3,fyT3,fzT3;						// Force along T3 
		real8 normFT1;							// norm of FT1
		real8 normFT2;							// norm of FT2
		real8 normFT3;							// norm of FT3
//////////////////////////////////////////////////////////
 		x1=node->x; 	y1=node->y; 	z1=node->z; 
 		x2=nbr->x; 		y2=nbr->y; 		z2=nbr->z; 
		dx=x1-x2;		dy=y1-y2;		dz=z1-z2;		//Line sense vector
		normD = sqrt(dx*dx + dy*dy + dz*dz);
	        dx=dx/normD;	dy=dy/normD;	dz=dz/normD;	//Line sense vector (normalized)
		bx = node->burgX[0];	by = node->burgY[0];	bz = node->burgZ[0];
		normB = sqrt(bx*bx + by*by + bz*bz);
	        bx=bx/normB;	by=by/normB;	bz=bz/normB;	//Burgers vector of the surface segment	(normalized)
		screw_character=fabs(dx*bx + dy*by + dz*bz);

		if (DEBUG_PRINT  == 1) 
		{
//		printf("Surface node (%d,%d)\n", node->myTag.domainID, node->myTag.index);
//		printf("Neighbor node (%d,%d)\n", nbr->myTag.domainID, nbr->myTag.index);
//		printf("node(x,y,z)=(%e,%e, %e)\n", node->x,node->y,node->z);
//		printf("nbr(x,y,z)=(%e,%e, %e)\n", nbr->x,nbr->y,nbr->z);
//		printf("dx : %e, dy : %e, dz : %e \n",  dx,dy,dz);
//		printf("bx : %e, by : %e, bz : %e \n",  bx,by,bz);
//		printf("screw_character : %e\n",screw_character);
		} 
		if(screw_character >=  tolerance )
		{ // if surface segment has screw character. 
	      // Move dislocation at the surface such that it is along the burgers vectors
			if (DEBUG_PRINT  == 1) 
			{
				printf("Surface node %d  have screw character! \n", node->myTag.index); 
			}
			R= radius; 
			alpha1 =-(bx*x2 + by*y2 + sqrt(R*R*bx*bx + R*R*by*by - bx*bx*y2*y2 + 2.*bx*by*x2*y2 - by*by*x2*x2))/(bx*bx + by*by);
			alpha2 =-(bx*x2 + by*y2 - sqrt(R*R*bx*bx + R*R*by*by - bx*bx*y2*y2 + 2.*bx*by*x2*y2 - by*by*x2*x2))/(bx*bx + by*by);

			if (fabs(alpha1)>=fabs(alpha2)) 
			{// Take smaller alpha, since it means closer point to nbr
				alpha=alpha2;
			}
			else 
			{
				alpha=alpha1;
			}
/*//iryu (Here is a problem!!!!)
			node->x = x2+alpha*bx;
			node->y = y2+alpha*by;
			node->z = z2+alpha*bz;
      (Here is a problem!!!!) 
*/
			x1=node->x; 	y1=node->y; 	z1=node->z; 
			// Calculate image forces
//#ifdef _CYLIMGSTRESS
//			Imgfx=0.0;			Imgfy=0.0;		Imgfz=0.0;
//#else
			///// Need to specify.(absolute value)!!!!!!!!!!!!!!!!!!!!!!!!
			Imgfx=0.0;			Imgfy=0.0;		Imgfz=1.0e11;
			if (z2>=z1)
			{
				Imgfz*=1.0;						// Upward
			}
			else if (z2<z1)
			{	
				Imgfz*=-1.0;					//Downward
			}
//#endif
			// Calculate P-K force
			PKfx=node->fX;	PKfy=node->fY;	PKfz=node->fZ;	

			// Calculate the total force = Image force + P-K force
			fx=Imgfx+PKfx;	fy=Imgfy+PKfy;	fz=Imgfz+PKfz;
 
			dotB1= fabs(bx*(1.0e0/sqrt(3.0)) + by*(1.0e0/sqrt(3.0)) + bz*(1.0e0/sqrt(3.0)));
			dotB2= fabs(bx*(1.0e0/sqrt(3.0)) + by*(1.0e0/sqrt(3.0)) + bz*(-1.0e0/sqrt(3.0))); 
			dotB3= fabs(bx*(-1.0e0/sqrt(3.0)) + by*(1.0e0/sqrt(3.0)) + bz*(1.0e0/sqrt(3.0))); 
			dotB4= fabs(bx*(1.0e0/sqrt(3.0)) + by*(-1.0e0/sqrt(3.0)) + bz*(1.0e0/sqrt(3.0))); 
			if (DEBUG_PRINT  == 1) 
			{	
				printf("Total force Fx = %e Fy = %e Fz = %e \n",  fx,fy,fz);
				printf("P-K force Fx = %e Fy = %e Fz = %e \n",  PKfx,PKfy,PKfz);
            			printf("dotB1= %e, dotB2= %e, dotB3= %e, dotB4= %e\n", dotB1,dotB2,dotB3,dotB4);
			}
			// Select 3 slip planes which contain Burgers vector.
//
			if (dotB1 >= tolerance)
			{// if b=[1 1 1]/sqrt(3)
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 =-1.0e0/sqrt(2.0e0);	NZ1 = 0.0e0 ;
				NX2 = 0.0e0;				NY2 = 1.0e0/sqrt(2.0e0);	NZ2 =-1.0e0/sqrt(2.0e0);
				NX3 = 1.0e0/sqrt(2.0e0);    NY3 = 0.0e0; 				NZ3 =-1.0e0/sqrt(2.0e0);
				if (DEBUG_PRINT  == 1) 
				{	
					printf("B1=(1 1 1) or (-1 -1 -1) \n");		
					printf("N1 =(%e,%e,%e)\n", NX1,NY1,NZ1);
					printf("N2 =(%e,%e,%e)\n", NX2,NY2,NZ2);
					printf("N3 =(%e,%e,%e)\n", NX3,NY3,NZ3);
				}
			}		
			else if (dotB2 >= tolerance)
			{// if b=[1 1 -1]/sqrt(3)
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 =-1.0e0/sqrt(2.0e0);	NZ1 = 0.0e0 ;
				NX2 = 1.0e0/sqrt(2.0e0);	NY2 = 0.0e0;				NZ2 = 1.0e0/sqrt(2.0e0);
				NX3 = 0.0e0;			    NY3 = 1.0e0/sqrt(2.0e0);	NZ3 = 1.0e0/sqrt(2.0e0);
				if (DEBUG_PRINT  == 1) 
				{	
					printf("B2=(1 1 -1) or (-1 -1 1)  \n");
					printf("N1 =(%e,%e,%e)\n", NX1,NY1,NZ1);
					printf("N2 =(%e,%e,%e)\n", NX2,NY2,NZ2);
					printf("N3 =(%e,%e,%e)\n", NX3,NY3,NZ3);
				}
			}		
			else if (dotB3 >= tolerance)
			{// if b=[-1 1 1]/sqrt(3)
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 = 1.0e0/sqrt(2.0e0);	NZ1 = 0.0e0 ;
				NX2 = 1.0e0/sqrt(2.0e0);	NY2 = 0.0e0;				NZ2 = 1.0e0/sqrt(2.0e0);
				NX3 = 0.0e0;			    NY3 = 1.0e0/sqrt(2.0e0);	NZ3 =-1.0e0/sqrt(2.0e0);
				if (DEBUG_PRINT  == 1) 
				{	
					printf("B3=(-1 1 1) or (1 -1 -1)  \n");
					printf("N1 =(%e,%e,%e)\n", NX1,NY1,NZ1);
					printf("N2 =(%e,%e,%e)\n", NX2,NY2,NZ2);
					printf("N3 =(%e,%e,%e)\n", NX3,NY3,NZ3);
				}
			}		
			else if (dotB4 >= tolerance)
			{// if b=[1 -1 1]/sqrt(3)
				NX1 = 1.0e0/sqrt(2.0e0);	NY1 = 1.0e0/sqrt(2.0e0);	NZ1 = 0.0e0 ;
				NX2 = 1.0e0/sqrt(2.0e0); 	NY2 = 0.0e0;				NZ2 =-1.0e0/sqrt(2.0e0);
				NX3 = 0.0e0;			    NY3 = 1.0e0/sqrt(2.0e0);	NZ3 = 1.0e0/sqrt(2.0e0);
				if (DEBUG_PRINT  == 1) 
				{	
					printf("B4=(1 -1 1) or (-1 1 -1)  \n");
					printf("N1 =(%e,%e,%e)\n", NX1,NY1,NZ1);
					printf("N2 =(%e,%e,%e)\n", NX2,NY2,NZ2);
					printf("N3 =(%e,%e,%e)\n", NX3,NY3,NZ3);
				}
			}		
			else
			{
				printf("B should be one of them in BCC glide plane!!\n");
				NOFLIP = 1;
//				Fatal("B should be one of them in BCC glide plane!!");
			}
			if (NOFLIP == 0)
			{	
				// Compute tangent vector for each slip plane
				TX1 = -y1-(-y1*NX1+x1*NY1)*NX1;	TY1 = x1-(-y1*NX1+x1*NY1)*NY1;	TZ1 = -(-y1*NX1+x1*NY1)*NZ1;			
				TX2 = -y1-(-y1*NX2+x1*NY2)*NX2;	TY2 = x1-(-y1*NX2+x1*NY2)*NY2;	TZ2 = -(-y1*NX2+x1*NY2)*NZ2;			
				TX3 = -y1-(-y1*NX3+x1*NY3)*NX3;	TY3 = x1-(-y1*NX3+x1*NY3)*NY3;	TZ3 = -(-y1*NX3+x1*NY3)*NZ3;			
				normT1 = sqrt(TX1*TX1 + TY1*TY1 + TZ1*TZ1);			//Normalize them. 
				normT2 = sqrt(TX2*TX2 + TY2*TY2 + TZ2*TZ2);
				normT3 = sqrt(TX3*TX3 + TY3*TY3 + TZ3*TZ3);
				TX1 /=normT1;	TY1 /=normT1;	TZ1 /=normT1;	   
				TX2 /=normT2;	TY2 /=normT2;	TZ2 /=normT2;	   
				TX3 /=normT3;	TY3 /=normT3;	TZ3 /=normT3;	   

				// Calculate the projected force along tangent vector(F dot Ti)
				normFT1=fabs(fx*TX1 + fy*TY1 + fz*TZ1); 
				normFT2=fabs(fx*TX2 + fy*TY2 + fz*TZ2);  
				normFT3=fabs(fx*TX3 + fy*TY3 + fz*TZ3);  
				if (DEBUG_PRINT  == 1) 
				{	
					//printf("normFT1 :  %e\n",normFT1);
					//printf("normFT2 :  %e\n",normFT2);
					//printf("normFT3 :  %e\n",normFT3);
				}	
/* Not necessary becausee we only consider the case where nbr has only two arms(one surface arm, one inside arm)
				// Find arm #(surfarm) of nbr which is the link to the surface node
				if (nbr->numNbrs == 2)
				{
					nbr1=GetNeighborNode(home,nbr,0);           //1st neighbor of nbr 
					nbr2=GetNeighborNode(home,nbr,1);           //2nd neighbor of nbr 
					if (fabs((nbr1->x)-(node->x))+fabs((nbr1->y)-(node->y))+fabs((nbr1->z)-(node->z))< 1.0-tolerance)
					{	//nbr1 = node(surface node)
						surfarm =0;
					}
					else if (fabs((nbr2->x)-(node->x))+fabs((nbr2->y)-(node->y))+fabs((nbr2->z)-(node->z))<1.0-lerance) 
					{	//nbr2 = node(surface node)
						surfarm =1;
					}
				}		
				else if (nbr->numNbrs == 3)
				{
					nbr1=GetNeighborNode(home,nbr,0);           //1st neighbor of nbr 
					nbr2=GetNeighborNode(home,nbr,1);           //2nd neighbor of nbr 
					nbr3=GetNeighborNode(home,nbr,2);           //3rd neighbor of nbr 
					if (fabs((nbr1->x)-(node->x))+fabs((nbr1->y)-(node->y))+fabs((nbr1->z)-(node->z))< 1.0-tolerance)
					{	//nbr1 = node(surface node)
						surfarm =0;
					}
					else if (fabs((nbr2->x)-(node->x))+fabs((nbr2->y)-(node->y))+fabs((nbr2->z)-(node->z))< 1.0-tolerance) 
					{	//nbr2 = node(surface node)
						surfarm =1;
					}
					else if (fabs((nbr3->x)-(node->x))+fabs((nbr3->y)-(node->y))+fabs((nbr3->z)-(node->z))< 1.0-tolerance) 
					{	//nbr3 = node(surface node)
						surfarm =2;
					}
				}
				else if (nbr->numNbrs == 4)
				{
					nbr1=GetNeighborNode(home,nbr,0);           //1st neighbor of nbr 
					nbr2=GetNeighborNode(home,nbr,1);           //2nd neighbor of nbr 
					nbr3=GetNeighborNode(home,nbr,2);           //3rd neighbor of nbr 
					nbr4=GetNeighborNode(home,nbr,3);           //4rd neighbor of nbr 
					if (fabs((nbr1->x)-(node->x))+fabs((nbr1->y)-(node->y))+fabs((nbr1->z)-(node->z))< 1.0-tolerance)
					{	//nbr1 = node(surface node)
						surfarm =0;
					}
					else if (fabs((nbr2->x)-(node->x))+fabs((nbr2->y)-(node->y))+fabs((nbr2->z)-(node->z))< 1.0-tolerance) 
					{	//nbr2 = node(surface node)
						surfarm =1;
					}
					else if (fabs((nbr3->x)-(node->x))+fabs((nbr3->y)-(node->y))+fabs((nbr3->z)-(node->z))< 1.0-tolerance) 
					{	//nbr3 = node(surface node)
						surfarm =2;
					}
					else if (fabs((nbr4->x)-(node->x))+fabs((nbr4->y)-(node->y))+fabs((nbr4->z)-(node->z))< 1.0-tolerance) 
					{	//nbr4 = node(surface node)
						surfarm =3;
					}
				}
*/   
				// Choose slip plane based on the magnitude of the force projected on them.
				if ((normFT1>= normFT2) && (normFT1>= normFT3) )
				{	// slip plane = n1
					if (DEBUG_PRINT  == 1) 
					{	
						printf("Slip plane : N1 (%e, %e, %e) \n",NX1,NY1,NZ1);
					}
					node->nx[0]=NX1;	node->ny[0]=NY1;	node->nz[0]=NZ1;
				}		
				else if ((normFT2 > normFT1) && (normFT2>= normFT3) )
				{	// slip plane = n2	
					if (DEBUG_PRINT  == 1) 
					{	
						printf("Slip plane : N2 (%e, %e, %e) \n",NX2,NY2,NZ2);
					}
					node->nx[0]=NX2;	node->ny[0]=NY2;	node->nz[0]=NZ2;
				}		
				else if ((normFT3 > normFT1) && (normFT3 > normFT2) )
				{	// slip plane = n3	
					if (DEBUG_PRINT  == 1) 
					{	
						printf("Slip plane : N3 (%e, %e, %e) \n",NX2,NY2,NZ2);
					}
					node->nx[0]=NX3;	node->ny[0]=NY3;	node->nz[0]=NZ3;
				}
				// Change the slip plane of the neighbor node(nbr)
				if (nbr1->constraint == CYLINDER_SURFACE_NODE)
				{   // nbr1  is the surface node. 
                			nbr->nx[0]=node->nx[0];     nbr->ny[0]=node->ny[0];     nbr->nz[0]=node->nz[0];
				}    
				else // nbr2 is the surface node.
				{    
					nbr->nx[1]=node->nx[0];     nbr->ny[1]=node->ny[0];     nbr->nz[1]=node->nz[0];
				}   
			}
// 
		}
	}
	if (DEBUG_PRINT  == 1) 
	{	printf("b---End of Surface Cross slip---d \n");	
	}
}
			
#endif 





#if 0 
/*
 *
 *      (iryu/2011.02.02)
 *      To make the mobility of surface nodes n times faster than mobility of inner node. 
 *     
 *      (iryu/2011.02.22)
 *      Modifiction: In previous case, the mobility of the surface node only is adjusted. 
 *                   Now, mobility of inner nodes are modified according to coordinates. 
 *      Condition :  1. inner nodes can move faster only in burgers vector direction. 
 *                   2. if the radius of inner nodes in the resion ]0.9R,R[ the mobilities are adjusted 
 *                     
 *      Caution!! : this part needs to be check. 
 *                  Some prolbem happens : some cusps happen inside, but if two cusps meet, it make weird nodes
 *                  It make the time step really small(10^-13)
 */  

	/*	scale = param->MobSurface;                   */
		real8 scale, scale2, radius, distance ;
		scale = 2e+01;                                                                
		radius = param->cyl_radius;

    if (node->constraint == CYLINDER_SURFACE_NODE)
    {
		/***	 printf("The mobility of the surface node is %e times smaller than that in inner nodes\n",scale); */
		node->vX *= scale;	
		node->vY *= scale;	
		node->vZ *= scale;
	}	
	else if(node->constraint == UNCONSTRAINED)
	{	//Mobility of inner nodes are scaled according to their coordinates. 
		distance = sqrt((node->x*node->x)+(node->y*node->y));
		if (distance > 0.8*radius)
		{
		//	For test
		//	printf(" radius : %e \n",radius);
		scale2= 1.0+((scale-1.0)/(0.1*radius))*(distance-0.9*radius);
		node->vX *= scale2;	
		node->vY *= scale2;	
		node->vZ *= scale2;
		}
	}	
	
#endif


#if 0 
/*
 *
 *      (iryu/2011.05.12)
 *      To save computational cost, remove surface debris(dislocation segment) under following conditions. 
 *      Condition :  1. Dislocation segents consist of only 3~4 segament.  
 *                   2. Force on the inner nodes should points outwards.
 *                   3. Alternatively, it would be possible to assume that surface debris should go out due to the image effect. 
 *                      In this case, we don't need to consider force direction. 
 *      Method1 : Specify node inner node flag to 6 (Due to the remesh rule, these segement can be removed) 
 *              ->  Does not work!!!!(Segmentation error!)
 *      Method2 : Multiply mobility for inner node by 10^5. (to make this segment move faster)               
 */  

	if(node->constraint == CYLINDER_SURFACE_NODE)
	{
		Node_t *nbr1, *nbr2, *nbr3;
		real8 toleranceNode; toleranceNode = 0.01e0; 
		real8 fX1,fY1,fX2,fY2;								// Force of the 1st neighbor node(nbr1) 
		real8 normF, normF1, normF2;
		real8 x1,y1, x2,y2;
		real8 normXY,normXY1, normXY2;;
		real8 criterion,criterion1,criterion2;
		real8 seglength;
		nbr1=GetNeighborNode(home,node,0);                //Neighbor of surface node 
		seglength = sqrt(((node->x-nbr1->x))*((node->x-nbr1->x))+
                         ((node->y-nbr1->y))*((node->y-nbr1->y))+
						 ((node->z-nbr1->z))*((node->y-nbr1->y)));

		if ((nbr1->numNbrs == 2)&&(seglength <= 2.0e0*(home->param->minSeg) ))
		{
			nbr2=GetNeighborNode(home,nbr1,0);                //2nd Neighbor of nearest neighbor node
			if (fabs((nbr2->x)-(node->x)) <=toleranceNode 
		     && fabs((nbr2->y)-(node->y)) <=toleranceNode
	    	 && fabs((nbr2->z)-(node->z)) <=toleranceNode)
			{
				nbr2=GetNeighborNode(home,nbr1,1);            
			}
			if ((nbr2->numNbrs == 1)&& (nbr2->constraint == CYLINDER_SURFACE_NODE))
			{	// Case 1 :  node(surface) -- nbr1 -- nbr2(surface)	
				fX1=nbr1->fX;		fY1=nbr1->fY;			
				normF = sqrt(fX1*fX1+fY1*fY1);
				fX1/=normF;			fY1/=normF;
				x1=nbr1->x;		y1=nbr1->y;					//outward vector
				normXY = sqrt(x1*x1+y1*y1);
				x1/=normXY;			y1/=normXY;
	            criterion = x1*fX1+y1*fY1;  //dot product (coordinate, force) 
				if (criterion >= 0.0e0 )
				{   // if force on inner node points outwards
					nbr1->x = node->x;
					nbr1->y = node->y;
					nbr1->z = node->z;
					nbr1->constraint = CYLINDER_SURFACE_NODE;        //Method1
	//				nbr1->fX += x1*1e10;		//Add huge force in radial direction
	//				nbr1->fY += y1*1e10;      	//Add huge force in radial direction
				}
			}
			else if (nbr2->numNbrs == 2)
			{ // Case 2 :    node(surface) -- nbr1 -- nbr2 -- nbr3(surface)		
				nbr3=GetNeighborNode(home,nbr2,0);                //3rd Neighbor of nearest neighbor node
				if (fabs((nbr3->x)-(nbr1->x)) <=toleranceNode 
			     && fabs((nbr3->y)-(nbr1->y)) <=toleranceNode
		    	 && fabs((nbr3->z)-(nbr1->z)) <=toleranceNode)
				{
					nbr3=GetNeighborNode(home,nbr2,1);            
				}
				if ((nbr3->numNbrs == 1)&& (nbr3->constraint == CYLINDER_SURFACE_NODE))
				{	
					fX1=nbr1->fX;		fY1=nbr1->fY;		
					fX2=nbr2->fX;		fY2=nbr2->fY;		
					normF1 = sqrt(fX1*fX1+fY1*fY1);
					normF2 = sqrt(fX2*fX2+fY2*fY2);
					x1=nbr1->x;		y1=nbr1->y;	
					x2=nbr2->x;		y2=nbr2->y;
					normXY1 = sqrt(x1*x1+y1*y1);
					normXY2 = sqrt(x2*x2+y2*y2);
					x1/=normXY1;			y1/=normXY1;
					x2/=normXY2;			y2/=normXY2;
					criterion1 = x1*fX1+y1*fY1;
					criterion2 = x2*fX2+y2*fY2;
					if ((criterion1 >=0.0e0) && (criterion2 >= 0.0e0))
					{   // force1 and force2 point outwards
						nbr1->x = node->x;
						nbr1->y = node->y;
						nbr1->z = node->z;
						nbr1->constraint= CYLINDER_SURFACE_NODE;        // Attach nbr1 to node(surface node)
		//				nbr2->x = nbr3->x;
		//				nbr2->y = nbr3->y;
		//				nbr2->z = nbr3->z;
		//				nbr2->constraint= CYLINDER_SURFACE_NODE;        // Attach nbr2 to nbr3(surface node)
						//	nbr1->constraint=CYLINDER_SURFACE_NODE;								// Method1 does not work
						//	nbr2->constraint=CYLINDER_SURFACE_NODE;								// Method1 does not work
						//	nbr1->fX += x1*1e10;		//Add huge force in radial direction
						//	nbr1->fY += y1*1e10;      	//Add huge force in radial direction
						//	nbr2->fX += x2*1e10;		//Add huge force in radial direction
						//	nbr2->fY += y2*1e10;      	//Add huge force in radial direction
					}
				}	
			}	
		}// if (nbr1->numNbrs == 2) 
	}
		
#endif 

#if 0 
/*
 *
 *      (iryu/2011.05.13) _ version1
 *      To get realistic stress-strain curve, need to consider Pierls barrier. 
 *      Because code only consider athermal movement of dislocation. 
 *      So, here I multiply factor which can replicate the Pierls energy barrier.  
 *      Factor :  
 *                     -20* Tf
 *                exp(----------)
 *                       T       
 *                 - T  : resolved shear stress
 *                 - Tf : threshold shere stress below which dislocation do not move. 
 *                 - 20 : arbitrary constant which control the shape 
 *           [Caution]    
 *                   - Tf need to be changed with respect to the characteristic of the dislocation segment. 
 *                    
 */
		real8 Threshold;
		real8 scaleX,scaleY, scaleZ;
		Threshold = 1e+00;                              // need to be adjusted !!!!                                 
		real8 fX,fY,fZ;									// Force on the node
		fX=node->fX; 	fY=node->fY;		fZ=node->fZ;		
		scaleX = exp(-1.0*Threshold/fX);
		scaleY = exp(-1.0*Threshold/fY);
		scaleZ = exp(-1.0*Threshold/fZ);

		node->vX *= scaleX;	
		node->vY *= scaleY;	
		node->vZ *= scaleZ;
	
#endif

#if 0 
/*
 *
 *      (iryu/2011.05.13) _ version2
 *      To avoid segmentation fault, 
 *       if fX<threshold , vX=0;
 *       if fY<threshold , vY=0;
 *       if fZ<threshold , vZ=0;
 *           [Caution]    
 *                   - Tf need to be changed with respect to the characteristic of the dislocation segment. 
 *                    
 */
	if(node->constraint != CYLINDER_SURFACE_NODE)
	{
		real8 Threshold;
		Threshold = 1e+04;                              // need to be adjusted !!!!                                 
		real8 scaleX,scaleY, scaleZ;
		real8 fX,fY,fZ;									// Force on the node
		fX=node->fX; 	fY=node->fY;		fZ=node->fZ;		
		scaleX = exp(-10.0*Threshold/fX);
		scaleY = exp(-10.0*Threshold/fY);
		scaleZ = exp(-10.0*Threshold/fZ);
		if (fX<Threshold)
		{	node->vX = 0.0;	}
		else
		{	node->vX *= scaleX;	}
		if (fY<Threshold)
		{	node->vY = 0.0;	}
		else
		{	node->vY *= scaleY;	}
		if (fZ<Threshold)
		{	node->vZ = 0.0;	}
		else
		{	node->vZ *= scaleZ;	}
	}	

#endif


#if 0 
/*
 *      (iryu/2011.02.13)
 *      To flip the slip plane of the surface node if one dislocation has only one slip plane. 
 *      ------Algorithm 
 *           1. Find the surface node. 
 *           2. Find neighbor node
 *               - check if this node have two arms 
 *                  Yes-go step 3
 *                  No - check if this node is on the surface
 *                       Yes - Flip the slip plane
 *                       No - stop 
 *           3. Check if two arms have same slip planes.
 *               Yes- Repeat step 2
 *               No - stop  
 */ 
	if(node->constraint == CYLINDER_SURFACE_NODE){
		Node_t  *nbr_pre, *nbr0, *nbr1, *nbr2;
		//		Node_t  *nbr_11, *nbr12, *nbr21, *nbr22;        // Neighbor of nbr1: nbr11,nbr12, Neighbor of nbr2: nbr21,nbr22
		nbr_pre=node;
		nbr0=GetNeighborNode(home,node,0);                //Neighbor of node of interest
		real8 tnx,tny;
		int nodetag;   
        nodetag = node->myTag.index;
		real8 x0,y0,z0;   //coordinate of nbr0
		real8 x1,y1,z1;   //surface node1
		real8 x2,y2,z2;   //surface node2
		real8 distance1, distance2; //distance between nbr0 and node1,2
		real8 t1,t2; 
		real8 critical;
		real8 radius;
		radius = param->cyl_radius;

		//	printf("Checking node %d which is on the surface\n",nodetag); 
			while(nbr0->numNbrs == 2){
				//printf("nbr_pre [%d] --x:%e, y:%e, z:%e  \n",nbr_pre->myTag.index, nbr_pre->x,nbr_pre->y,nbr_pre->z); 
		        //printf("nbr0 [%d] --:x:%e, y:%e, z:%e  \n",nbr0->myTag.index, nbr0->x,nbr0->y,nbr0->z); 
				//printf("It has two arms \n");
				nbr1=GetNeighborNode(home,nbr0,0);   //1st neighbor of nbr0       
				nbr2=GetNeighborNode(home,nbr0,1);   //2nd neighbor of nbr0     
				
				//	nbr11=GetNeighborNode(home,nbr1,0);   //1st neighbor of nbr1       
				//	nbr21=GetNeighborNode(home,nbr2,0);   //1st neighbor of nbr2       
				
				//printf("nbr2 [%d] --:x:%e, y:%e, z:%e  \n",nbr2->myTag.index, nbr2->x,nbr2->y,nbr2->z); 
  				if (nbr0->nx[0]==nbr0->nx[1] &&	nbr0->ny[0]==nbr0->ny[1] &&	nbr0->nz[0]==nbr0->nz[1])
				{   // Check if the two arms of nbr0 are on same plane
					//printf(" These arms are on the same plane\n"); 
					if( (nbr1->x==nbr_pre->x)  && (nbr1->y==nbr_pre->y) && (nbr1->z==nbr_pre->z) )
					{   //	Check if nbr1=nbr_pre then, nbr0=nbr2	
							if (nbr2->constraint != CYLINDER_SURFACE_NODE)
							{
								//	printf("Since nbr2 is not on the surface, let's searchnig forward!\n"); 
								//	printf("nbr1 is equal to nbr_pre. Now, nbr2 is the forwarding node.\n");
								nbr_pre=nbr0; 
								nbr0=nbr2;
							}
							else
							{	
								critical = sqrt((node->x-nbr2->x)*(node->x-nbr2->x)
												+(node->y-nbr2->y)*(node->y-nbr2->y)
												+(node->z-nbr2->z)*(node->z-nbr2->z));
								if (critical>radius*0.5)
								{
										// Flip the slip plane of the node of interest
		        						printf("Whole straight dislocation is on one plane. Then let's flip the slip plane\n"); 
										tnx=nbr0->nx[1];
										tny=nbr0->ny[1];
										nbr0->nx[1]=tny;
										nbr0->ny[1]=tnx;
										tnx=nbr2->nx[0];
										tny=nbr2->ny[0];
										nbr2->nx[0]=tny;
										nbr2->ny[0]=tnx;
										// Need to adjust the coordinate of the surface node which should be on the new slip plane.
										x0=nbr0->x;		y0=nbr0->y;		z0=nbr0->z;
										t1=(-(x0+y0)+sqrt(2.0*radius*radius-(x0-y0)*(x0-y0)))/2.0;	
										t2=(-(x0+y0)-sqrt(2.0*radius*radius-(x0-y0)*(x0-y0)))/2.0;	
										x1=x0+t1;	y1=y0+t1;	z1=z0+t1;
										x2=x0+t2;	y2=y0+t2;	z2=z0+t2;
										distance1=sqrt(3.0*t1*t1);
										distance2=sqrt(3.0*t2*t2);
										if (distance1>distance2)
										{//node 2 is the surface node. 
											nbr2->x=x2;		nbr2->y=y2;		nbr2->z=z2;
										}	
										else
										{ //node 1 is the surface node. 
											nbr2->x=x1;		nbr2->y=y1;		nbr2->z=z1;
										}
		        						//	printf("Out of the while loop!\n"); 
										break;
								}
							}  
					}     
					else if((nbr2->x==nbr_pre->x)  && (nbr2->y==nbr_pre->y) && (nbr2->z==nbr_pre->z) )
					{   //	Check if nbr2=nbr_pre then, nbr0=nbr1	
							if (nbr1->constraint != CYLINDER_SURFACE_NODE)
							{
							//printf("nbr2 is equal to nbr_pre. Now, nbr1 is the forwarding node.\n"); 
							//printf("Since nbr1 is not on the surface, let's searchnig forward!\n"); 
								nbr_pre=nbr0; 
								nbr0=nbr1;
							}
							else
							{
								critical = sqrt((node->x-nbr2->x)*(node->x-nbr2->x)
												+(node->y-nbr2->y)*(node->y-nbr2->y)
												+(node->z-nbr2->z)*(node->z-nbr2->z));
								if (critical>radius*0.5)
								{
										// Flip the slip plane of the node of interest
				        				printf("Whole straight dislocation is on one plane. Then let's flip the slip plane\n"); 
										tnx=nbr0->nx[0];
										tny=nbr0->ny[0];
										nbr0->nx[0]=tny;
										nbr0->ny[0]=tnx;
										tnx=nbr1->nx[0];
										tny=nbr1->ny[0];
										nbr1->nx[0]=tny;
										nbr1->ny[0]=tnx;
										// Need to adjust the coordinate of the surface node which should be on the new slip plane.
										x0=nbr0->x;		y0=nbr0->y;		z0=nbr0->z;
										t1=(-(x0+y0)+sqrt(2.0*radius*radius-(x0-y0)*(x0-y0)))/2.0;	
										t2=(-(x0+y0)-sqrt(2.0*radius*radius-(x0-y0)*(x0-y0)))/2.0;	
										x1=x0+t1;		y1=y0+t1;		z1=z0+t1;
										x2=x0+t2;		y2=y0+t2;		z2=z0+t2;
										distance1=sqrt(3.0*t1*t1);
										distance2=sqrt(3.0*t2*t2);
										if (distance1>distance2)
										{//node 2 is the surface node. 
											nbr1->x=x2;		nbr1->y=y2;			nbr1->z=z2;
										}	
										else
										{ //node 1 is the surface node. 
											nbr1->x=x1;		nbr1->y=y1;			nbr1->z=z1;
										}
		        						//	printf("Out of the while loop!\n"); 
										break;
								}
							} 
					} 
				} 
				else 
				{	
		        	//	printf("They are on different planes. I'll do nothing!!\n"); 
		        	//	printf("Out of the while loop!\n"); 
					break;
				}
			} // End of while loop 
	} // End of if(node->constraint == CYLINDER_SURFACE_NODE)
#endif



#endif     /*ifdef cylinder*/
	if (DEBUG_PRINT  == 1) 
	{	
		//printf("***** End of the moblity BCC glide*****\n"); 
	}

        return(0);
}
