#include "Home.h"


#ifdef _ABAQUS

#include "ABQ.h"

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void Write_Node_Force(Home_t *home,char *format)
{
		FILE *fp;
		char name[15];
		int i;
		Node_t *node;

		if (home->myDomain == 0) printf("\nWriting out forces after %s\n",format);

		sprintf(name, "force%d.out",home->myDomain);
		fp = fopen(name,"a");

		for (i=0;i<home->newNodeKeyPtr;i++) {
			node = home->nodeKeys[i];
			if (!node) continue;
			fprintf(fp,"%.15e  %.15e  %.15e  %.15e  %.15e  %.15e \n",node->x,node->y,node->z,
						node->fX,node->fY,node->fZ);
		}

		fclose(fp);
		//xiaohan		exit(0);
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void PrintStress(Home_t *home,Abaqus_t *abaqus)
{
/***************************************************************************
 *		This program computes the stress at a point x in the domain
 *		Sylvie Aubry, Apr 25 2008
 *
 **************************************************************************/

		FILE *fpInf,*fpImg,*fpTot;
		char format[15];
		int i,j,k,l,m,ii,jj;
		int npoint;
		real8 r[3], stress[3][3],s1[3][3],s1loc[3][3],s2[3][3],s[3][3],Ys[3][3];
	  
		real8 diff, x, y;
		npoint = 300;

		diff = home->param->Lx/(1.0*npoint);
	  
		if (home->myDomain == 0) 
			printf(" \n\n WRITING STRESS TO FILE %d points \n\n",npoint);

		sprintf(format, "InfStress%d.out",home->myDomain);
		fpInf = fopen(format,"w");

		sprintf(format, "ImgStress%d.out",home->myDomain);
		fpImg = fopen(format,"w");

		sprintf(format, "TotStress%d.out",home->myDomain);
		fpTot = fopen(format,"w");

		for (i=0; i<npoint; i++) {
			x = home->param->minSideX + diff*i;
			r[0] = x;
			r[1] = 1.0;
			r[2] = 0; //home->param->minSideZ; /* Top surface */
		//	r[2] = -500.0;
			
		//	if (x<-4900 || x>4900) continue;

			/* infinite medium stress */
			Init3x3(s1loc);
			AllSegmentStress(home,abaqus,r[0],r[1],r[2],s1loc);

#ifdef PARALLEL
			MPI_Allreduce(s1loc, s1, 9, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
			for (ii = 0; ii < 3; ii++) {
				for (jj = 0; jj < 3; jj++) {
					s1[ii][jj] = s1loc[ii][jj];
				}
			}
#endif
			/* image stress */	
			Init3x3(s2);
#ifdef _ABQIMGSTRESS
		//	DispStress(abaqus,r,s2);
			ABQ_calc_stress(home, abaqus, r, s2);
#endif


#ifndef _NOYOFFESTRESS
			/* Yoffe stress */
			Init3x3(Ys);
			AllYoffeStress(home, abaqus, r[0], r[1], r[2], Ys);
			for (ii = 0; ii < 3; ii++) {
				for (jj = 0; jj < 3; jj++) {
					s2[ii][jj] += Ys[ii][jj];
				}
			}
#endif

			/* total stress */  
			for (ii = 0; ii < 3; ii++) {
				for (jj = 0; jj < 3; jj++) {
					s[ii][jj] = s1[ii][jj] + s2[ii][jj];
				}
			}

			/* Print stresses */ 
			fprintf(fpInf,"%.15e %.15e %.15e      %.15e %.15e %.15e %.15e %.15e %.15e\n",
			r[0],r[1],r[2], s1[0][0],s1[1][1],s1[2][2],s1[1][2],s1[2][0],s1[0][1]);

			fprintf(fpImg,"%.15e %.15e %.15e      %.15e %.15e %.15e %.15e %.15e %.15e\n",
			r[0],r[1],r[2], s2[0][0],s2[1][1],s2[2][2],s2[1][2],s2[2][0],s2[0][1]);

		//	fprintf(fpTot,"%.15e %.15e %.15e      %.15e %.15e %.15e %.15e %.15e %.15e\n",
		//	r[0],r[1],r[2], s[0][0],s[1][1],s[2][2],s[1][2],s[2][0],s[0][1]);
			
			fprintf(fpTot,"%.8e %.8e %.8e  %.8e %.8e %.8e %.8e %.8e %.8e  %.8e %.8e %.8e %.8e %.8e %.8e  %.8e %.8e %.8e %.8e %.8e %.8e\n",
			r[0],r[1],r[2], s2[0][0],s2[1][1],s2[2][2],s2[1][2],s2[2][0],s2[0][1],
							s1[0][0],s1[1][1],s1[2][2],s1[1][2],s1[2][0],s1[0][1],
							s[0][0],s[1][1],s[2][2],s[1][2],s[2][0],s[0][1]);
		}

		fclose(fpImg);
		fclose(fpInf);
		fclose(fpTot);

		//xiaohan		exit(0);
		return;
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void PrintTractions(Home_t *home,Abaqus_t *abaqus)
{
	FILE *fpInf;
	char format[50];
	int i,j,k,l,m,ii,jj;
	int nx, ny;
	real8 r[3], s[3][3],s1loc[3][3];
	double** GridPts[3];
  
#ifndef _ABQIMGSTRESS
	real8 difX, difY, x, y;
	nx = 100;
	ny = 100;

	difX = home->param->Lx/(1.0*nx);
	difY = home->param->Ly/(1.0*ny);

	for (i=0;i<3;i++) 
		GridPts[i] = (double **)malloc(sizeof(double*)*nx);

	for (i=0;i<3;i++) {
		for (k=0;k<nx;k++) {
			GridPts[i][k] = (double *)malloc(sizeof(double)*ny);
		}
	}

	for (i=0; i<nx; i++) {
		x = home->param->minSideX + difX*i;
		for (j=0; j<ny; j++) {
			y = home->param->minSideY + difY*j;
			GridPts[0][i][j] = x;
			GridPts[1][i][j] = y;
			GridPts[2][i][j] = abaqus->t;
		}
	}
#else
	nx = abaqus->nx;
	ny = abaqus->ny;

	for (i=0;i<3;i++) 
		GridPts[i] = (double **)malloc(sizeof(double*)*nx);

	for (i=0;i<3;i++) {
		for (k=0;k<nx;k++) {
			GridPts[i][k] = (double *)malloc(sizeof(double)*ny);
		}
	}

	for (k=0;k<3;k++) {
		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
			//	GridPts[k][i][j] = abaqus->Grid[k][i][j];
			}
		}
	}
#endif
  
	real8 *tx, *ty, *tz, *tx_tot, *ty_tot, *tz_tot;

	// Allocate tx, ty, tz
	tx = malloc(sizeof(double)*nx*ny*2);
	ty = malloc(sizeof(double)*nx*ny*2);
	tz = malloc(sizeof(double)*nx*ny*2);

	tx_tot = malloc(sizeof(double)*nx*ny*2);
	ty_tot = malloc(sizeof(double)*nx*ny*2);
	tz_tot = malloc(sizeof(double)*nx*ny*2);

	if (home->myDomain == 0) 
		printf(" \n\n WRITING  Tractions TO FILE %d x %d points \n\n",nx, ny);

	sprintf(format, "Tractions%d.out",home->myDomain);
	fpInf = fopen(format,"w");

	// Bottom Surface
	for (i=0; i<nx; i++) {
		if (home->myDomain == 0)  printf("Bottom doing i=%d\n",i);
		for (j=0; j<ny; j++) {
			r[0] =  GridPts[0][i][j];
			r[1] =  GridPts[1][i][j];
			r[2] = -GridPts[2][i][j]; 

			/* infinite medium stress */
			Init3x3(s1loc);
			AllSegmentStress(home,abaqus,r[0],r[1],r[2],s1loc);


#ifdef PARALLEL
			MPI_Allreduce(s1loc, s, 9, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
			for (ii = 0; ii < 3; ii++) {
				for (jj = 0; jj < 3; jj++) {
					s[ii][jj] = s1loc[ii][jj];
				}
			}
#endif

			tx[j+i*ny] = s[0][2];
			ty[j+i*ny] = s[1][2];
			tz[j+i*ny] = s[2][2];
		}
	}


	//Top surface
	for (i=0; i<nx; i++) {
		if (home->myDomain == 0)  printf("Top doing i=%d\n",i);
		for (j=0; j<ny; j++) {
			r[0] = GridPts[0][i][j];
			r[1] = GridPts[1][i][j];
			r[2] = GridPts[2][i][j]; 

			/* infinite medium stress */
			Init3x3(s1loc);
			AllSegmentStress(home,abaqus,r[0],r[1],r[2],s1loc);

#ifdef PARALLEL
			MPI_Allreduce(s1loc, s, 9, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
			for (ii = 0; ii < 3; ii++) {
				for (jj = 0; jj < 3; jj++) {
					s[ii][jj] = s1loc[ii][jj];
				}
			}
#endif

			tx[j+i*ny+nx*ny] = -s[0][2];
			ty[j+i*ny+nx*ny] = -s[1][2];
			tz[j+i*ny+nx*ny] = -s[2][2];
		}
	}
/*
	for (i=0; i<nx; i++) {
		for (j=0; j<ny; j++) {
			fprintf(fpInf,"%.15e %.15e %.15e      %.15e %.15e %.15e\n",
			        abaqus->Grid[0][i][j],abaqus->Grid[1][i][j],-abaqus->Grid[2][i][j],
			        tx[j+i*ny], ty[j+i*ny], tz[j+i*ny]);
			fprintf(fpInf,"%.15e %.15e %.15e      %.15e %.15e %.15e\n",
			        abaqus->Grid[0][i][j],abaqus->Grid[1][i][j],abaqus->Grid[2][i][j],
			        tx[j+i*ny+nx*ny], ty[j+i*ny+nx*ny], tz[j+i*ny+nx*ny]);
		}
	}*/

	fclose(fpInf);


	for (i=0;i<3;i++) {
		for (k=0;k<nx;k++) {
			free(GridPts[i][k]);
		}
	}

	for (i=0;i<3;i++) free(GridPts[i]);

	//xiaohan	exit(0);
	return;
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void Write_sigbRem(Home_t *home,char *format)
{
	FILE *fp;
	char name[40];
	int i,armID12,ti;
	Node_t *node, *nbr;

	if (home->myDomain == 0) printf("\nWriting out sigbRem after %s\n",format);

	sprintf(name, "sig%dbRem.out",home->myDomain);
	fp = fopen(name,"w");


	for (i=0;i<home->newNodeKeyPtr;i++) {
		node = home->nodeKeys[i];
		if (!node) continue;

		for (ti = 0; ti < node->numNbrs; ti++) {
			nbr = GetNeighborNode(home, node, ti);
			armID12 = GetArmID(home, node, nbr);

			fprintf(fp,"%.15e  %.15e  %.15e  %.15e  %.15e  %.15e \n",node->x,node->y,node->z,
			node->sigbRem[armID12*3],node->sigbRem[armID12*3+1],
			node->sigbRem[armID12*3+2]);
		}
	}

	fclose(fp);

	//xiaohan	exit(0);
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/
 
void PrintdSegImgStress(Home_t *home, Abaqus_t *abaqus)
{
	FILE *fp;
	char format[15];
	int i,j,k,l,m;
	real8 r[3], s1[3][3];

	int nx = abaqus->nx;
	int ny = abaqus->ny;

	if (home->myDomain == 0) 
		printf(" \n\n WRITING dSegImgStress TO FILE %d x %d points \n\n ",nx, ny);

	sprintf(format, "dSegImgStress%d.out",home->myDomain);
	fp = fopen(format,"w");

	for (i=0; i<nx; i++) {
		for (j=0; j<ny; j++) {
		//	r[0] = abaqus->Grid[0][i][j];
		//	r[1] = abaqus->Grid[1][i][j];
		//	r[2] = abaqus->Grid[2][i][j]; /* Top surface */
			//r[2] =-abaqus->Grid[2][i][j]; /* Bottom surface */

			dSegImgStress(home, s1, 
			              0.0, 0.0, 0.0, /*  r0 */
			              0.0, 1.0, 0.0, /*  dl */
			              0.0, 0.0, 1.0, /*  burg */
			              r[0], r[1], r[2],
			              0 /* pbc */ );

			fprintf(fp,   "%.15e %.15e %.15e      %.15e %.15e %.15e %.15e %.15e %.15e\n",
			      r[0],r[1],r[2], s1[0][0],s1[1][1],s1[2][2],s1[1][2],s1[2][0],s1[0][1]);
		}
	}

	fclose(fp);
	//xiaohan	exit(0);
	return;
}




void PrintForce(Home_t *home,Abaqus_t* abaqus)
{
	FILE *fp;
	char format[15];
	int i, j, k;
	int nx, ny;
	double px[3];
	double sdis[3][3], simg[3][3];
	Param_t *param;
	
	param = home->param;
	
	printf("Print image stress\n");
	
	nx = abaqus->nx;
	ny = abaqus->ny;
	
	double maxs33 = 0.0;
	double maxerr = 0.0;
	double abserr = 0.0;
	
	sprintf(format, "stress_top_%d.dat", nx);
	fp = fopen(format,"w");
	
	px[0] = 0.0;
	px[1] = 0.5 * (param->minSideY + param->maxSideY);
	px[2] = param->minSideZ + param->Lz;
	
	for (i = 0; i < nx; i++) {
		
		px[0] = param->minSideX + (param->Lx / (nx-1)) * i;
		
		Init3x3(sdis);
		AllSegmentStress(home, abaqus, px[0], px[1], px[2], sdis);
		
		Init3x3(simg);
		DispStress(abaqus, px, simg);
		
		maxs33 = MAX(fabs(sdis[2][2]),maxs33);
		maxerr = MAX(fabs(sdis[2][2]+simg[2][2]),maxerr);
		abserr += fabs(sdis[2][2]+simg[2][2]);
		
		fprintf(fp, "%e %e %e %e %e %e %e %e %e %e %e %e\n", px[0], px[1], px[2], 
		sdis[2][2], simg[2][2], sdis[2][2] + simg[2][2],  // s33
		sdis[0][2], simg[0][2], sdis[0][2] + simg[0][2],  // s13
		sdis[1][2], simg[1][2], sdis[1][2] + simg[1][2]); // s23
		
	}
	
	fclose(fp);
	
	
	printf("Print image force\n");
	
	Node_t  *node1, *node2;
	int i1, i2, nc;
	double xA, yA, zA;
	double xB, yB, zB;
	double bZ;
	double tx, ty, tz, magt, sign;
	double sbx, sby, sbz;
	double a, R, d, x, y;
	double b, b2, MU, NU;
	double fz_anl, fz_img;
	
	// Coordinate of the first node
	for (i1 = 0; i1 < home->newNodeKeyPtr; i1++) {
		node1 = home->nodeKeys[i1];
		if (!node1) continue;
		else break;
	}
	
	a = param->maxSideZ - node1->z;
	x = 0.5 * (param->minSideX + param->maxSideX);
	y = 0.5 * (param->minSideY + param->maxSideY);
	x = x - node1->x;
	y = y - node1->y;
	R = sqrt(x*x+y*y);
	d = a/R;
	
	printf("a   relerr   maxerr   abserr\n");
	printf("%e %e %e %e\n",a,maxerr/maxs33,maxerr,abserr/nx);
	
	printf("--a=%f, R=%f, d=%f\n", a, R, d);
	
	b = param->burgMag;
	b2 = b*b;
	MU = param->shearModulus;
	NU = param->pois;
	
	// Analytical solution
	if (d - 0.5 < 1.e-4) fz_anl = 4.6451;
	else if (d - 1.0 < 1.e-4) fz_anl = 2.0881;
	else if (d - 4.0 < 1.e-4) fz_anl = 0.0393;
	else fz_anl = 1.0;
	fz_anl = 1.0/8.0/M_PI*b2*MU/(R*b)/(1.0-NU)*fz_anl;


	// Loop over all segments to average force
	int nseg = 0;
	fz_img = 0.0;
	double fz_seg[home->newNodeKeyPtr];
	for (i1 = 0; i1 < home->newNodeKeyPtr; i1++) {
		node1 = home->nodeKeys[i1];
		if (!node1) continue;
		
		nc = node1->numNbrs;
		
		xB = node1->x;
		yB = node1->y;
		zB = node1->z;

		for (i2 = 0; i2 < nc; i2++) {
			node2 = GetNeighborNode(home, node1, i2);
			if (!node2) continue;

			if (OrderNodes(node2, node1) != 1) continue;

			xA = node2->x;
			yA = node2->y;
			zA = node2->z;
			
			bZ = node1->burgZ[i2];
			if (bZ < 0.0) sign = -1.0;
			else sign = 1.0;
			
			tx = xA - xB;
			ty = yA - yB;
			tz = zA - zB;
			magt = sqrt(tx*tx+ty*ty+tz*tz);
			tx = tx/magt;
			ty = ty/magt;
			tz = tz/magt;
			
			// Middle position
			px[0] = 0.5*(xA + xB);
			px[1] = 0.5*(yA + yB);
			px[2] = 0.5*(zA + zB);
			
			Init3x3(simg);
			DispStress(abaqus, px, simg);
			
			//output: Sxx,Syy,Szz,Sxy,Syz,Szx
			sbx = sign*b*simg[0][2];
			sby = sign*b*simg[1][2];
			sbz = sign*b*simg[2][2];
			
			fz_seg[nseg] = sbx*ty - sby*tx;
			fz_img += fz_seg[nseg];
			nseg++;
			
			//printf("----seg: %d, Image force: %e\n", nseg, fz_seg[nseg-1]);
			
		}

	}
	
	// Average
	fz_img = fz_img / nseg;
	double dev = 0.0;
	for (i = 0; i < nseg; i++) {
		dev += (fz_seg[i]-fz_img)*(fz_seg[i]-fz_img);
	}
	dev = sqrt(dev/nseg);
	
	printf("--Analytical force: %e, Image force: %e, std-dev: %e\n", fz_anl, fz_img, dev);
	
	// Open output file
	char filename[100];
	FILE *pFile;
	
	sprintf(filename, "force_err_%d.dat", nx);
	pFile = fopen(filename, "w");
	fprintf(pFile, "N  Analytical_force  Image_force  std_dev  Rel_error\n");
	fprintf(pFile, "%d %e %e %e %e\n", nx, fz_anl, fz_img, dev, fabs(fz_anl-fz_img)/fz_anl);
	
	fprintf(pFile,"a   relerr   maxerr   abserr\n");
	fprintf(pFile,"%e %e %e %e\n",a,maxerr/maxs33,maxerr,abserr/nx);
	
	fclose(pFile);
	
	return;
}

#endif
