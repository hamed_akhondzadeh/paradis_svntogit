/***************************************************************************
 *
 *  Module      : abq_creat_mesh.c
 *  Description : Creates the FEM mesh including the FEM nodes and elements,
 *                the elements connectivity, quadratures on the free surface
 *                (for nodal force calculation due to image stress) and the 
 *                required data for PBC implementation
 * 
 **************************************************************************/


#include "Home.h"
#include <math.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef _ABAQUS

#include "ABQ.h"
 
void ABQ_Create_Mesh_Quad(Param_t *param, Abaqus_t *abaqus) 
{
	int    iNode , iElem , nx , ny , nz    , iXmaxYmin , i , j , k , m , n;
	int    xFix  , yFix  , zFix  , nodS[8] , iXmaxYmax;
	int    iQuaX , iQuaY , nQxyz , iQuad   , iXminYmin;
	int    iXmin , iXmax , iYmin , iYmax   , iXminYmax;
	real8  ABQLx , x , dX , n1x , n3x , xi [6] , fact  , iXi ;
	real8  ABQLy , y , dY , n1y , n3y , eta[6] , fact1 , iEta;
	real8  ABQLz , z , dZ , n1z , n3z , wei[6] , fact2 ;

/*
 *	Initialization
 */
	nx = abaqus->nx;
	ny = abaqus->ny;
	nz = abaqus->nz;
	int  nodeIndex[2*nx+1][2*ny+1][2*nz+1];
	
	for (i = 0 ; i <= 2*nx ; i++) {
		for (j = 0 ; j <= 2*ny ; j++) {
			for (k = 0 ; k <= 2*nz ; k++) {
				nodeIndex[i][j][k] = -1;
			}
		}
	}

	ABQLx = abaqus->ABQLx;
	ABQLy = abaqus->ABQLy;
	ABQLz = abaqus->ABQLz;

	dX = ABQLx/(2.0*nx);
	dY = ABQLy/(2.0*ny);
	dZ = ABQLz/(2.0*nz);
	
	xFix = 0;
	yFix = 0;
	zFix = 0;
	
/*
 *	Create the FEM nodes and assign their initial coordinates
 */
	iNode = 0;
	iXmin = 0;  iXminYmin = 0;
	iYmin = 0;  iXminYmax = 0;
	iXmax = 0;  iXmaxYmin = 0;
	iYmax = 0;  iXmaxYmax = 0;

	for (k = 0 ; k <= 2*nz ; k++) {
		if (strcmp(param->tf_or_hs, "tf") == 0) z = -abaqus->t       + dZ*k;
		if (strcmp(param->tf_or_hs, "hs") == 0) z =  param->minSideZ + dZ*k;
		
		if (k == 0 ) zFix = 1;
		else         zFix = 0;

		for (j = 0 ; j <= 2*ny ; j++) {
			if (k%2 == 1 && j%2 == 1) continue;
			y = param->minSideY + dY*j;
		
			if (j == 0 || j == 2*ny) yFix = 1;
			else                     yFix = 0;
			
			for (i = 0 ; i <= 2*nx ; i++) {
				if ((j%2 == 1 || k%2 == 1) && (i%2 == 1)) continue;
				x = param->minSideX + dX*i;
		
				if (i == 0 || i == 2*nx) xFix = 1;
				else                     xFix = 0;

				abaqus->FEMnodes[iNode].x = x;
				abaqus->FEMnodes[iNode].y = y;
				abaqus->FEMnodes[iNode].z = z;
				
				abaqus->FEMnodes[iNode].xFix = xFix;
				abaqus->FEMnodes[iNode].yFix = yFix;
				abaqus->FEMnodes[iNode].zFix = zFix;
				
				nodeIndex[i][j][k] = iNode;
				
/*
 *				Generating the arrays required for PBC implementation in Abaqus
 *				k = 0 is excluded below because its nodes are defined as fixed BC
 */
				if (abaqus->pbcXY && k != 0) {
					if (i == 0) {
						if (j == 0) {
							abaqus->nSetXminYmin[iXminYmin] = iNode;
							iXminYmin++;
						} else if (j == 2*ny) {
							abaqus->nSetXminYmax[iXminYmax] = iNode;
							iXminYmax++;
						} else {
							abaqus->nSetXmin[iXmin] = iNode;
							iXmin++;
						}
					} else if (i == 2*nx) {
						if (j == 0) {
							abaqus->nSetXmaxYmin[iXmaxYmin] = iNode;
							iXmaxYmin++;
						} else if (j == 2*ny) {
							abaqus->nSetXmaxYmax[iXmaxYmax] = iNode;
							iXmaxYmax++;
						} else {
							abaqus->nSetXmax[iXmax] = iNode;
							iXmax++;
						}
					} else if (j == 0) {
						abaqus->nSetYmin[iYmin] = iNode;
						iYmin++;
					} else if (j == 2*ny) {
						abaqus->nSetYmax[iYmax] = iNode;
						iYmax++;
					}
				}
				
				iNode++;
			}
		}
	}
	
/*
 *	Check whether node generation was completed successfully
 */
	if (iNode != abaqus->nNodes) {
		Fatal ("Error in FEM mesh generation. abaqus->nNodes=%d , #of created nodes=%d\n",
		       abaqus->nNodes , iNode);
	}
	
	if (abaqus->pbcXY) {
		if (iXmin != abaqus->nNsurX || iXmax != abaqus->nNsurX) {
			Fatal ("Error in FEM mesh generation. abaqus->nNsurX=%d , #of Xmin nodes=%d, Xmax nodes=%d\n",
				   abaqus->nNsurX , iXmin , iXmax);
		}
		
		if (iYmin != abaqus->nNsurY || iYmax != abaqus->nNsurY) {
			Fatal ("Error in FEM mesh generation. abaqus->nNsurY=%d , #of Ymin nodes=%d, Ymax nodes=%d\n",
				   abaqus->nNsurY , iYmin , iYmax);
		}
		
		if (iXminYmin != abaqus->nNedge || iXmaxYmin != abaqus->nNedge ||
		    iXminYmax != abaqus->nNedge || iXmaxYmax != abaqus->nNedge) {
			printf("Fatal: Error in FEM mesh generation.\n");
			printf("       abaqus->nNedge=%d\n" , abaqus->nNedge);
			printf("       #of XminYmin nodes=%d, XmaxYmin nodes=%d\n", iXminYmin , iXmaxYmin);
			printf("       #of XminYmax nodes=%d, XmaxYmax nodes=%d\n", iXminYmax , iXmaxYmax);
			exit(0);
		}
		
		for (i = 0 ; i < abaqus->nNsurX ; i++) {
			j = abaqus->nSetXmin[i];
			k = abaqus->nSetXmax[i];
			
			if (abaqus->FEMnodes[j].y != abaqus->FEMnodes[k].y) 
				Fatal("i=%d , node[%d].y=%f , node[%d].y=%f\n" , i,j , abaqus->FEMnodes[j].y , k , abaqus->FEMnodes[k].y);
			if (abaqus->FEMnodes[j].z != abaqus->FEMnodes[k].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,j , abaqus->FEMnodes[j].z , k , abaqus->FEMnodes[k].z);
		}
		
		for (i = 0 ; i < abaqus->nNsurY ; i++) {
			j = abaqus->nSetYmin[i];
			k = abaqus->nSetYmax[i];
			
			if (abaqus->FEMnodes[j].x != abaqus->FEMnodes[k].x) 
				Fatal("i=%d , node[%d].x=%f , node[%d].x=%f\n" , i,j , abaqus->FEMnodes[j].x , k , abaqus->FEMnodes[k].x);
			if (abaqus->FEMnodes[j].z != abaqus->FEMnodes[k].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,j , abaqus->FEMnodes[j].z , k , abaqus->FEMnodes[k].z);
		}
		
		for (i = 0 ; i < abaqus->nNedge ; i++) {
			j = abaqus->nSetXminYmin[i];
			k = abaqus->nSetXmaxYmin[i];
			m = abaqus->nSetXminYmax[i];
			n = abaqus->nSetXmaxYmax[i];
			
			if (abaqus->FEMnodes[j].z != abaqus->FEMnodes[k].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,j , abaqus->FEMnodes[j].z , k , abaqus->FEMnodes[k].z);
			if (abaqus->FEMnodes[j].y != abaqus->FEMnodes[k].y) 
				Fatal("i=%d , node[%d].y=%f , node[%d].y=%f\n" , i,j , abaqus->FEMnodes[j].y , k , abaqus->FEMnodes[k].y);
			
			if (abaqus->FEMnodes[j].z != abaqus->FEMnodes[m].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,j , abaqus->FEMnodes[j].z , m , abaqus->FEMnodes[m].z);
			if (abaqus->FEMnodes[j].x != abaqus->FEMnodes[m].x) 
				Fatal("i=%d , node[%d].x=%f , node[%d].x=%f\n" , i,j , abaqus->FEMnodes[j].x , m , abaqus->FEMnodes[m].x);
			
			if (abaqus->FEMnodes[k].z != abaqus->FEMnodes[n].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,k , abaqus->FEMnodes[k].z , n , abaqus->FEMnodes[n].z);
			if (abaqus->FEMnodes[k].x != abaqus->FEMnodes[n].x) 
				Fatal("i=%d , node[%d].x=%f , node[%d].x=%f\n" , i,k , abaqus->FEMnodes[k].x , n , abaqus->FEMnodes[n].x);
			
			if (abaqus->FEMnodes[m].z != abaqus->FEMnodes[n].z)
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,m , abaqus->FEMnodes[m].z , n , abaqus->FEMnodes[n].z);
			if (abaqus->FEMnodes[m].y != abaqus->FEMnodes[n].y) 
				Fatal("i=%d , node[%d].y=%f , node[%d].y=%f\n" , i,m , abaqus->FEMnodes[m].y , n , abaqus->FEMnodes[n].y);
		}
	}

/*
 *	Create the FEM elements and assign their nodal connectivity
 */	
	iElem = 0;
	for (k = 1 ; k < 2*nz ; k += 2 ) {
		for (j = 1 ; j < 2*ny ; j += 2 ) {
			for (i = 1 ; i < 2*nx ; i += 2 ) {
				abaqus->FEMelems[iElem].node[ 0] = nodeIndex[i-1][j-1][k-1];
				abaqus->FEMelems[iElem].node[ 1] = nodeIndex[i+1][j-1][k-1];
				abaqus->FEMelems[iElem].node[ 2] = nodeIndex[i+1][j+1][k-1];
				abaqus->FEMelems[iElem].node[ 3] = nodeIndex[i-1][j+1][k-1];
				abaqus->FEMelems[iElem].node[ 4] = nodeIndex[i-1][j-1][k+1];
				abaqus->FEMelems[iElem].node[ 5] = nodeIndex[i+1][j-1][k+1];
				abaqus->FEMelems[iElem].node[ 6] = nodeIndex[i+1][j+1][k+1];
				abaqus->FEMelems[iElem].node[ 7] = nodeIndex[i-1][j+1][k+1];
				
				abaqus->FEMelems[iElem].node[ 8] = nodeIndex[i  ][j-1][k-1];
				abaqus->FEMelems[iElem].node[ 9] = nodeIndex[i+1][j  ][k-1];
				abaqus->FEMelems[iElem].node[10] = nodeIndex[i  ][j+1][k-1];
				abaqus->FEMelems[iElem].node[11] = nodeIndex[i-1][j  ][k-1];
				abaqus->FEMelems[iElem].node[12] = nodeIndex[i  ][j-1][k+1];
				abaqus->FEMelems[iElem].node[13] = nodeIndex[i+1][j  ][k+1];
				abaqus->FEMelems[iElem].node[14] = nodeIndex[i  ][j+1][k+1];
				abaqus->FEMelems[iElem].node[15] = nodeIndex[i-1][j  ][k+1];
				
				abaqus->FEMelems[iElem].node[16] = nodeIndex[i-1][j-1][k  ];
				abaqus->FEMelems[iElem].node[17] = nodeIndex[i+1][j-1][k  ];
				abaqus->FEMelems[iElem].node[18] = nodeIndex[i+1][j+1][k  ];
				abaqus->FEMelems[iElem].node[19] = nodeIndex[i-1][j+1][k  ];
				
				iElem++;
			}
		}
	}
	
	if (iElem != abaqus->nElems) {
		printf("Error in FEM mesh generation. abaqus->nElems=%d , #of created elems=%d\n",
			abaqus->nElems , iElem);
	}
	
/*
 *	Create the quadrature points on the free surface
 *	First, do some initialization
 */
	iQuad = 0;
	k     = 2*nz-1;
	nQxyz = abaqus->nQuadSurface;
	
	if (nQxyz == 2) {
		
		fact = sqrt(1.0/3.0);
		xi [0] = -fact ;   xi [1] = fact;
		eta[0] = -fact ;   eta[1] = fact;
		wei[0] =  1.0  ;   wei[1] =  1.0;
	} else if (nQxyz == 3) {
		
		fact = sqrt(3.0/5.0);
		xi [0] = -fact ;   xi [1] = 0.0   ;   xi [2] = fact;
		eta[0] = -fact ;   eta[1] = 0.0   ;   eta[2] = fact;
		wei[0] = 5.0/9 ;   wei[1] = 8.0/9 ;   wei[2] = 5.0/9;
	} else if (nQxyz == 4) {
		
		fact  = sqrt(6.0/5.0);
		fact1 = sqrt(3.0/7.0 - 2.0*fact/7.0);
		fact2 = sqrt(3.0/7.0 + 2.0*fact/7.0);
		
		xi [0] = -fact2 ;   xi [1] = -fact1 ;   xi [2] = fact1 ;   xi [3] = fact2;
		eta[0] = -fact2 ;   eta[1] = -fact1 ;   eta[2] = fact1 ;   eta[3] = fact2;
		wei[0] = (18.0-sqrt(30.0))/36.0     ;   wei[3] = wei[0];
		wei[1] = (18.0+sqrt(30.0))/36.0     ;   wei[2] = wei[1];
	} else if (nQxyz == 5) {
		
		fact  = sqrt(10.0/7.0);
		fact1 = sqrt(5.0 - 2.0*fact) / 3.0;
		fact2 = sqrt(5.0 + 2.0*fact) / 3.0;
		
		xi [0] = -fact2 ;   xi [1] = -fact1 ;   xi [2] = 0.0 ;   xi [3] = fact1 ;   xi [4] = fact2;
		eta[0] = -fact2 ;   eta[1] = -fact1 ;   eta[2] = 0.0 ;   eta[3] = fact1 ;   eta[4] = fact2;
		wei[0] = (322.0 - 13.0*sqrt(70.0)) / 900.0   ;   wei[4] = wei[0];
		wei[1] = (322.0 + 13.0*sqrt(70.0)) / 900.0   ;   wei[3] = wei[1];
		wei[2] = (128.0 / 225.0);
	} else if (nQxyz == 6) {
		
		fact  = 0.2386191860831969086305017;
		fact1 = 0.6612093864662645136613996;
		fact2 = 0.9324695142031520278123016;
		
		xi [0] = -fact2 ;   xi [1] = -fact1 ;   xi [2] = -fact ;   xi [3] = fact ;   xi [4] = fact1 ;   xi [5] = fact2;
		eta[0] = -fact2 ;   eta[1] = -fact1 ;   eta[2] = -fact ;   eta[3] = fact ;   eta[4] = fact1 ;   eta[5] = fact2;
		wei[0] = 0.1713244923791703450402961   ;   wei[5] = wei[0];
		wei[1] = 0.3607615730481386075698335   ;   wei[4] = wei[1];
		wei[2] = 0.4679139345726910473898703   ;   wei[3] = wei[2];
	}
	
/*
 *	Now, loop over all surface elements and make the quadratures
 *	needed for the numerical integration of surface traction
 */
	for (j = 1 ; j < 2*ny ; j += 2 ) {
		for (i = 1 ; i < 2*nx ; i += 2 ) {
			
/*
 *			Find the node numbers corresponding to this element
 *			which are at the surface and save them in nodS array.
 *			Also save the minimum and maximum coordinates in each
 *			direction.
 */
			nodS[0] = nodeIndex[i-1][j-1][k+1];
			nodS[1] = nodeIndex[i+1][j-1][k+1];
			nodS[2] = nodeIndex[i+1][j+1][k+1];
			nodS[3] = nodeIndex[i-1][j+1][k+1];
			nodS[4] = nodeIndex[i  ][j-1][k+1];
			nodS[5] = nodeIndex[i+1][j  ][k+1];
			nodS[6] = nodeIndex[i  ][j+1][k+1];
			nodS[7] = nodeIndex[i-1][j  ][k+1];
					
			n1x = abaqus->FEMnodes[nodS[0]].x;
			n1y = abaqus->FEMnodes[nodS[0]].y;
			n1z = abaqus->FEMnodes[nodS[0]].z;
			n3x = abaqus->FEMnodes[nodS[2]].x;
			n3y = abaqus->FEMnodes[nodS[2]].y;
			n3z = abaqus->FEMnodes[nodS[2]].z;
			
/*
 *			Now, generate the quadratures, calculate their 
 *			coordinates, integration weights and corresponding
 *			shape function values.
 */
			for (iQuaY = 0 ; iQuaY < nQxyz ; iQuaY++) {
				for (iQuaX = 0 ; iQuaX < nQxyz ; iQuaX++) {
					iXi  = xi [iQuaX];
					iEta = eta[iQuaY];
					
					for (n = 0 ; n < 8 ; n++) {
						abaqus->FEMquads[iQuad].node[n] = nodS[n];
					}
					
					x = n1x + 0.5 * (n3x-n1x) * (1.0+iXi );
					y = n1y + 0.5 * (n3y-n1y) * (1.0+iEta);
					abaqus->FEMquads[iQuad].x       = x  ;
					abaqus->FEMquads[iQuad].y       = y  ;
					abaqus->FEMquads[iQuad].z       = n1z;
					abaqus->FEMquads[iQuad].weight  = wei[iQuaX] * wei[iQuaY];
					
					abaqus->FEMquads[iQuad].shFn[0] =-0.25*(1.0-iXi)*(1.0-iEta)*(1.0+iXi+iEta);
					abaqus->FEMquads[iQuad].shFn[1] =-0.25*(1.0+iXi)*(1.0-iEta)*(1.0-iXi+iEta);
					abaqus->FEMquads[iQuad].shFn[2] =-0.25*(1.0+iXi)*(1.0+iEta)*(1.0-iXi-iEta);
					abaqus->FEMquads[iQuad].shFn[3] =-0.25*(1.0-iXi)*(1.0+iEta)*(1.0+iXi-iEta);
					abaqus->FEMquads[iQuad].shFn[4] = 0.50*(1.0-iXi*iXi)*(1.0-iEta     );
					abaqus->FEMquads[iQuad].shFn[5] = 0.50*(1.0+iXi    )*(1.0-iEta*iEta);
					abaqus->FEMquads[iQuad].shFn[6] = 0.50*(1.0-iXi*iXi)*(1.0+iEta     );
					abaqus->FEMquads[iQuad].shFn[7] = 0.50*(1.0-iXi    )*(1.0-iEta*iEta);
					iQuad++;
				}
			}
		}
	}
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/
 
void ABQ_Create_Mesh_Linr(Param_t *param, Abaqus_t *abaqus) 
{
	int    iNode , iElem , nx , ny , nz    , iXmaxYmin , i , j , k , m , n;
	int    xFix  , yFix  , zFix  , nodS[4] , iXmaxYmax;
	int    iQuaX , iQuaY , nQxyz , iQuad   , iXminYmin;
	int    iXmin , iXmax , iYmin , iYmax   , iXminYmax;
	real8  ABQLx , x , dX , n1x , n3x , xi [6] , fact  , iXi ;
	real8  ABQLy , y , dY , n1y , n3y , eta[6] , fact1 , iEta;
	real8  ABQLz , z , dZ , n1z , n3z , wei[6] , fact2 ;

/*
 *	Initialization
 */
	nx = abaqus->nx;
	ny = abaqus->ny;
	nz = abaqus->nz;
	int  nodeIndex[nx+1][ny+1][nz+1];
	
	for (i = 0 ; i <= nx ; i++) {
		for (j = 0 ; j <= ny ; j++) {
			for (k = 0 ; k <= nz ; k++) {
				nodeIndex[i][j][k] = -1;
			}
		}
	}

	ABQLx = abaqus->ABQLx;
	ABQLy = abaqus->ABQLy;
	ABQLz = abaqus->ABQLz;

	dX = ABQLx/(1.0*nx);
	dY = ABQLy/(1.0*ny);
	dZ = ABQLz/(1.0*nz);
	
/*
 *	Create the FEM nodes and assign their initial coordinates
 */
	iNode = 0;
	iXmin = 0;  iXminYmin = 0;
	iYmin = 0;  iXminYmax = 0;
	iXmax = 0;  iXmaxYmin = 0;
	iYmax = 0;  iXmaxYmax = 0;

	for (k = 0 ; k <= nz ; k++) {
		if (strcmp(param->tf_or_hs, "tf") == 0) z = -abaqus->t       + dZ*k;
		if (strcmp(param->tf_or_hs, "hs") == 0) z =  param->minSideZ + dZ*k;
		
		if (k == 0 ) zFix = 1;
		else         zFix = 0;

		for (j = 0 ; j <= ny ; j++) {
			y = param->minSideY + dY*j;
		
			if (j == 0 || j == ny) yFix = 1;
			else                   yFix = 0;
			
			for (i = 0 ; i <= nx ; i++) {
				x = param->minSideX + dX*i;
		
				if (i == 0 || i == nx) xFix = 1;
				else                   xFix = 0;

				abaqus->FEMnodes[iNode].x = x;
				abaqus->FEMnodes[iNode].y = y;
				abaqus->FEMnodes[iNode].z = z;
				
				abaqus->FEMnodes[iNode].xFix = xFix;
				abaqus->FEMnodes[iNode].yFix = yFix;
				abaqus->FEMnodes[iNode].zFix = zFix;
				
				nodeIndex[i][j][k] = iNode;
				
/*
 *				Generating the arrays required for PBC implementation in Abaqus
 *				k = 0 is excluded below because its nodes are defined as fixed BC
 */
				if (abaqus->pbcXY && k != 0) {
					if (i == 0) {
						if (j == 0) {
							abaqus->nSetXminYmin[iXminYmin] = iNode;
							iXminYmin++;
						} else if (j == ny) {
							abaqus->nSetXminYmax[iXminYmax] = iNode;
							iXminYmax++;
						} else {
							abaqus->nSetXmin[iXmin] = iNode;
							iXmin++;
						}
					} else if (i == nx) {
						if (j == 0) {
							abaqus->nSetXmaxYmin[iXmaxYmin] = iNode;
							iXmaxYmin++;
						} else if (j == ny) {
							abaqus->nSetXmaxYmax[iXmaxYmax] = iNode;
							iXmaxYmax++;
						} else {
							abaqus->nSetXmax[iXmax] = iNode;
							iXmax++;
						}
					} else if (j == 0) {
						abaqus->nSetYmin[iYmin] = iNode;
						iYmin++;
					} else if (j == ny) {
						abaqus->nSetYmax[iYmax] = iNode;
						iYmax++;
					}
				}
				
				iNode++;
			}
		}
	}
	
/*
 *	Check whether node generation was completed successfully
 */
	if (iNode != abaqus->nNodes) {
		Fatal ("Error in FEM mesh generation. abaqus->nNodes=%d , #of created nodes=%d\n",
		       abaqus->nNodes , iNode);
	}
	
	if (abaqus->pbcXY) {
		if (iXmin != abaqus->nNsurX || iXmax != abaqus->nNsurX) {
			Fatal ("Error in FEM mesh generation. abaqus->nNsurX=%d , #of Xmin nodes=%d, Xmax nodes=%d\n",
				   abaqus->nNsurX , iXmin , iXmax);
		}
		
		if (iYmin != abaqus->nNsurY || iYmax != abaqus->nNsurY) {
			Fatal ("Error in FEM mesh generation. abaqus->nNsurY=%d , #of Ymin nodes=%d, Ymax nodes=%d\n",
				   abaqus->nNsurY , iYmin , iYmax);
		}
		
		if (iXminYmin != abaqus->nNedge || iXmaxYmin != abaqus->nNedge ||
		    iXminYmax != abaqus->nNedge || iXmaxYmax != abaqus->nNedge) {
			printf("Fatal: Error in FEM mesh generation.\n");
			printf("       abaqus->nNedge=%d\n" , abaqus->nNedge);
			printf("       #of XminYmin nodes=%d, XmaxYmin nodes=%d\n", iXminYmin , iXmaxYmin);
			printf("       #of XminYmax nodes=%d, XmaxYmax nodes=%d\n", iXminYmax , iXmaxYmax);
			exit(0);
		}
		
		for (i = 0 ; i < abaqus->nNsurX ; i++) {
			j = abaqus->nSetXmin[i];
			k = abaqus->nSetXmax[i];
			
			if (abaqus->FEMnodes[j].y != abaqus->FEMnodes[k].y) 
				Fatal("i=%d , node[%d].y=%f , node[%d].y=%f\n" , i,j , abaqus->FEMnodes[j].y , k , abaqus->FEMnodes[k].y);
			if (abaqus->FEMnodes[j].z != abaqus->FEMnodes[k].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,j , abaqus->FEMnodes[j].z , k , abaqus->FEMnodes[k].z);
		}
		
		for (i = 0 ; i < abaqus->nNsurY ; i++) {
			j = abaqus->nSetYmin[i];
			k = abaqus->nSetYmax[i];
			
			if (abaqus->FEMnodes[j].x != abaqus->FEMnodes[k].x) 
				Fatal("i=%d , node[%d].x=%f , node[%d].x=%f\n" , i,j , abaqus->FEMnodes[j].x , k , abaqus->FEMnodes[k].x);
			if (abaqus->FEMnodes[j].z != abaqus->FEMnodes[k].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,j , abaqus->FEMnodes[j].z , k , abaqus->FEMnodes[k].z);
		}
		
		for (i = 0 ; i < abaqus->nNedge ; i++) {
			j = abaqus->nSetXminYmin[i];
			k = abaqus->nSetXmaxYmin[i];
			m = abaqus->nSetXminYmax[i];
			n = abaqus->nSetXmaxYmax[i];
			
			if (abaqus->FEMnodes[j].z != abaqus->FEMnodes[k].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,j , abaqus->FEMnodes[j].z , k , abaqus->FEMnodes[k].z);
			if (abaqus->FEMnodes[j].y != abaqus->FEMnodes[k].y) 
				Fatal("i=%d , node[%d].y=%f , node[%d].y=%f\n" , i,j , abaqus->FEMnodes[j].y , k , abaqus->FEMnodes[k].y);
			
			if (abaqus->FEMnodes[j].z != abaqus->FEMnodes[m].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,j , abaqus->FEMnodes[j].z , m , abaqus->FEMnodes[m].z);
			if (abaqus->FEMnodes[j].x != abaqus->FEMnodes[m].x) 
				Fatal("i=%d , node[%d].x=%f , node[%d].x=%f\n" , i,j , abaqus->FEMnodes[j].x , m , abaqus->FEMnodes[m].x);
			
			if (abaqus->FEMnodes[k].z != abaqus->FEMnodes[n].z) 
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,k , abaqus->FEMnodes[k].z , n , abaqus->FEMnodes[n].z);
			if (abaqus->FEMnodes[k].x != abaqus->FEMnodes[n].x) 
				Fatal("i=%d , node[%d].x=%f , node[%d].x=%f\n" , i,k , abaqus->FEMnodes[k].x , n , abaqus->FEMnodes[n].x);
			
			if (abaqus->FEMnodes[m].z != abaqus->FEMnodes[n].z)
				Fatal("i=%d , node[%d].z=%f , node[%d].z=%f\n" , i,m , abaqus->FEMnodes[m].z , n , abaqus->FEMnodes[n].z);
			if (abaqus->FEMnodes[m].y != abaqus->FEMnodes[n].y) 
				Fatal("i=%d , node[%d].y=%f , node[%d].y=%f\n" , i,m , abaqus->FEMnodes[m].y , n , abaqus->FEMnodes[n].y);
		}
	}

/*
 *	Create the FEM elements and assign their nodal connectivity
 */	
	iElem = 0;
	for (k = 0 ; k < nz ; k++ ) {
		for (j = 0 ; j < ny ; j++ ) {
			for (i = 0 ; i < nx ; i++ ) {
				abaqus->FEMelems[iElem].node[ 0] = nodeIndex[i  ][j  ][k  ];
				abaqus->FEMelems[iElem].node[ 1] = nodeIndex[i+1][j  ][k  ];
				abaqus->FEMelems[iElem].node[ 2] = nodeIndex[i+1][j+1][k  ];
				abaqus->FEMelems[iElem].node[ 3] = nodeIndex[i  ][j+1][k  ];
				abaqus->FEMelems[iElem].node[ 4] = nodeIndex[i  ][j  ][k+1];
				abaqus->FEMelems[iElem].node[ 5] = nodeIndex[i+1][j  ][k+1];
				abaqus->FEMelems[iElem].node[ 6] = nodeIndex[i+1][j+1][k+1];
				abaqus->FEMelems[iElem].node[ 7] = nodeIndex[i  ][j+1][k+1];
				
				iElem++;
			}
		}
	}
	
	if (iElem != abaqus->nElems) {
		printf("Error in FEM mesh generation. abaqus->nElems=%d , #of created elems=%d\n",
			abaqus->nElems , iElem);
	}
	
/*
 *	Create the quadrature points on the free surface
 *	First, do some initialization
 */
	iQuad = 0;
	k     = nz-1;
	nQxyz = abaqus->nQuadSurface;
	
	if (nQxyz == 2) {
		
		fact = sqrt(1.0/3.0);
		xi [0] = -fact ;   xi [1] = fact;
		eta[0] = -fact ;   eta[1] = fact;
		wei[0] =  1.0  ;   wei[1] =  1.0;
	} else if (nQxyz == 3) {
		
		fact = sqrt(3.0/5.0);
		xi [0] = -fact ;   xi [1] = 0.0   ;   xi [2] = fact;
		eta[0] = -fact ;   eta[1] = 0.0   ;   eta[2] = fact;
		wei[0] = 5.0/9 ;   wei[1] = 8.0/9 ;   wei[2] = 5.0/9;
	} else if (nQxyz == 4) {
		
		fact  = sqrt(6.0/5.0);
		fact1 = sqrt(3.0/7.0 - 2.0*fact/7.0);
		fact2 = sqrt(3.0/7.0 + 2.0*fact/7.0);
		
		xi [0] = -fact2 ;   xi [1] = -fact1 ;   xi [2] = fact1 ;   xi [3] = fact2;
		eta[0] = -fact2 ;   eta[1] = -fact1 ;   eta[2] = fact1 ;   eta[3] = fact2;
		wei[0] = (18.0-sqrt(30.0))/36.0     ;   wei[3] = wei[0];
		wei[1] = (18.0+sqrt(30.0))/36.0     ;   wei[2] = wei[1];
	} else if (nQxyz == 5) {
		
		fact  = sqrt(10.0/7.0);
		fact1 = sqrt(5.0 - 2.0*fact) / 3.0;
		fact2 = sqrt(5.0 + 2.0*fact) / 3.0;
		
		xi [0] = -fact2 ;   xi [1] = -fact1 ;   xi [2] = 0.0 ;   xi [3] = fact1 ;   xi [4] = fact2;
		eta[0] = -fact2 ;   eta[1] = -fact1 ;   eta[2] = 0.0 ;   eta[3] = fact1 ;   eta[4] = fact2;
		wei[0] = (322.0 - 13.0*sqrt(70.0)) / 900.0   ;   wei[4] = wei[0];
		wei[1] = (322.0 + 13.0*sqrt(70.0)) / 900.0   ;   wei[3] = wei[1];
		wei[2] = (128.0 / 225.0);
	} else if (nQxyz == 6) {
		
		fact  = 0.2386191860831969086305017;
		fact1 = 0.6612093864662645136613996;
		fact2 = 0.9324695142031520278123016;
		
		xi [0] = -fact2 ;   xi [1] = -fact1 ;   xi [2] = -fact ;   xi [3] = fact ;   xi [4] = fact1 ;   xi [5] = fact2;
		eta[0] = -fact2 ;   eta[1] = -fact1 ;   eta[2] = -fact ;   eta[3] = fact ;   eta[4] = fact1 ;   eta[5] = fact2;
		wei[0] = 0.1713244923791703450402961   ;   wei[5] = wei[0];
		wei[1] = 0.3607615730481386075698335   ;   wei[4] = wei[1];
		wei[2] = 0.4679139345726910473898703   ;   wei[3] = wei[2];
	}
	
/*
 *	Now, loop over all surface elements and make the quadratures
 *	needed for the numerical integration of surface traction
 */
	for (j = 0 ; j < ny ; j++ ) {
		for (i = 0 ; i < nx ; i++ ) {
			
/*
 *			Find the node numbers corresponding to this element
 *			which are at the surface and save them in nodS array.
 *			Also save the minimum and maximum coordinates in each
 *			direction.
 */
			nodS[0] = nodeIndex[i  ][j  ][k+1];
			nodS[1] = nodeIndex[i+1][j  ][k+1];
			nodS[2] = nodeIndex[i+1][j+1][k+1];
			nodS[3] = nodeIndex[i  ][j+1][k+1];
					
			n1x = abaqus->FEMnodes[nodS[0]].x;
			n1y = abaqus->FEMnodes[nodS[0]].y;
			n1z = abaqus->FEMnodes[nodS[0]].z;
			n3x = abaqus->FEMnodes[nodS[2]].x;
			n3y = abaqus->FEMnodes[nodS[2]].y;
			n3z = abaqus->FEMnodes[nodS[2]].z;
			
/*
 *			Now, generate the quadratures, calculate their 
 *			coordinates, integration weights and corresponding
 *			shape function values.
 */
			for (iQuaY = 0 ; iQuaY < nQxyz ; iQuaY++) {
				for (iQuaX = 0 ; iQuaX < nQxyz ; iQuaX++) {
					iXi  = xi [iQuaX];
					iEta = eta[iQuaY];
					
					for (n = 0 ; n < 4 ; n++) {
						abaqus->FEMquads[iQuad].node[n] = nodS[n];
					}
					
					x = n1x + 0.5 * (n3x-n1x) * (1.0+iXi );
					y = n1y + 0.5 * (n3y-n1y) * (1.0+iEta);
					abaqus->FEMquads[iQuad].x       = x  ;
					abaqus->FEMquads[iQuad].y       = y  ;
					abaqus->FEMquads[iQuad].z       = n1z;
					abaqus->FEMquads[iQuad].weight  = wei[iQuaX] * wei[iQuaY];
					
					abaqus->FEMquads[iQuad].shFn[0] = 0.25*(1.0-iXi)*(1.0-iEta);
					abaqus->FEMquads[iQuad].shFn[1] = 0.25*(1.0+iXi)*(1.0-iEta);
					abaqus->FEMquads[iQuad].shFn[2] = 0.25*(1.0+iXi)*(1.0+iEta);
					abaqus->FEMquads[iQuad].shFn[3] = 0.25*(1.0-iXi)*(1.0+iEta);
					iQuad++;
				}
			}
		}
	}
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_Create_Mesh(Param_t *param, Abaqus_t *abaqus)
{
/*
 *	Skip this part if tetrahedral elements are being used
 *	because for these element types the mesh should be already
 *	read and stored before
 */
	if (strcmp(param->abq_elemType, "tetrahedral") == 0) {
		return;
	}
	
/*
 *	Create the mesh for hexahedral elements
 */
	if (strcmp(param->abq_elemOrder, "linear" ) == 0) {
		ABQ_Create_Mesh_Linr(param, abaqus);
		
	} else if (strcmp(param->abq_elemOrder, "quadratic") == 0) {
		ABQ_Create_Mesh_Quad(param, abaqus);
		
	} else  {
		Fatal("abq_elemOrder should be defined as either linear or quadratic\n");
		
	}
}

#endif
