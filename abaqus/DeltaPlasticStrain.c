/***************************************************************************
 *   
 *      Module:     DeltaPlasticStrain.
 *
 *      Description:  This contains a simple generic dispatch function that
 *                    will invoke the version of DeltaPlasticStrain*() 
 *                    appropriate to the type of material being simulated
 *
 ***************************************************************************/
#include "Home.h"
#include "Mobility.h"

#ifdef _ABAQUS
void DeltaPlasticStrain(Home_t *home, Abaqus_t *abaqus)
#else
void DeltaPlasticStrain(Home_t *home)
#endif
{

        switch(home->param->materialType) {

            case MAT_TYPE_BCC:
#ifdef _ABAQUS
                DeltaPlasticStrain_BCC(home, abaqus);
#else
		DeltaPlasticStrain_BCC(home);
#endif
                break;

            case MAT_TYPE_FCC:
#ifdef _ABAQUS
                DeltaPlasticStrain_FCC(home, abaqus);
#else
		DeltaPlasticStrain_FCC(home);
#endif
                break;
        }

        return;
}
