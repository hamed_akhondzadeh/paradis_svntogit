
#include "Home.h"
#include "Node.h"
#include "Util.h"
#include "Comm.h"

void PrintSurfaceNodes(Home_t *home);

/*-------------------------------------------------------------------------
 *
 *      Function:     GetSProcessRegion
 *                    Return index of SProcess region and position of
 *                    the coordinate (inside, outside, on the surface)
 *
 *-----------------------------------------------------------------------*/
void GetSProcessRegion(Home_t *home, double x, double y, double z, int *region, int *pos)
{
	Param_t *param;
	double xmin, ymin, zmin;
	double xmax, ymax, zmax;
	double eps;
	
	param = home->param;
	
	eps = 1.e-5;
	
	// Only one region as of now
	*region = 0;
	
	// Global bounding box
	xmin = param->minSideX;
	xmax = param->maxSideX;
	ymin = param->minSideY;
	ymax = param->maxSideY;
	zmin = param->minSideZ;
	zmax = param->maxSideZ;
	
	// Check with bounding box
	// Outside the volume
	if ((param->xBoundType == Free && (xmin-x > eps || x-xmax > eps)) ||
	    (param->yBoundType == Free && (ymin-y > eps || y-ymax > eps)) ||
	    (param->zBoundType == Free && (zmin-z > eps || z-zmax > eps))) {
		*pos = POS_OUTSIDE;
	}
	
	// On the boundary
	else if ((param->xBoundType == Free) &&
	    (fabs(x-xmin) < eps || fabs(x-xmax) < eps)) {
		*pos = POS_SURFACE;
	}
	else if ((param->yBoundType == Free) &&
	    (fabs(y-ymin) < eps || fabs(y-ymax) < eps)) {
		*pos = POS_SURFACE;
	}
	else if ((param->zBoundType == Free) &&
	    (fabs(z-zmin) < eps || fabs(z-zmax) < eps)) {
		*pos = POS_SURFACE;
	}
	else {
		*pos = POS_INSIDE;
	}
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     GetNodeBoundaryPosition
 *      Description:  Returns: 
 * 					  * POS_INSIDE if node lies inside of the simulation domain
 * 					  * POS_SURFACE if on the surface boundary
 * 					  * POS_OUTSIDE if outside the simulation domain
 *
 *-----------------------------------------------------------------------*/
int GetNodeBoundaryPosition(Home_t *home, Node_t *node)
{
	double x, y, z;
	double eps;
	int region, pos;
	
	x = node->x;
	y = node->y;
	z = node->z;
	
	GetSProcessRegion(home, x, y, z, &region, &pos);
	
	return pos;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     SurfaceNodeIntersection
 *      Description:  Compute the intersection point between the segment
 *                    node1->node2 and the simulation domain boundary
 * 					  (node1 is inside and node2 is outside)
 *
 *-----------------------------------------------------------------------*/
int SurfaceNodeIntersection(Home_t *home, Node_t *node1, Node_t *node2, 
                            double pos[3], double *s, int *dir, double norm[3])
{
	Param_t *param;
	int i;
	double x1[3], x2[3];
	double p[3], n[3];
	double t[3], w[3];
	double D, N, st, stmp;
	
	param = home->param;
	
	x1[0] = node1->x;
	x1[1] = node1->y;
	x1[2] = node1->z;

	x2[0] = node2->x;
	x2[1] = node2->y;
	x2[2] = node2->z;
	
	t[0] = x2[0] - x1[0];
	t[1] = x2[1] - x1[1];
	t[2] = x2[2] - x1[2];
	
	int inter = 0;
	st = 0.0;
	
	// Test intersection for all facets of a cube
	// Select closest intersection to the inside node
	for (i = 0; i < 3; i++) {
		
		// Point on the surface
		p[0] = 0.0;
		p[1] = 0.0;
		p[2] = 0.0;
		
		// Normal to the surface
		n[0] = 0.0;
		n[1] = 0.0;
		n[2] = 0.0;
		
		if (i == 0) {
			if (x1[0] < 0.5*(param->minSideX + param->maxSideX)) {
				p[0] = param->minSideX;
				n[0] = -1.0;
			} else {
				p[0] = param->maxSideX;
				n[0] = 1.0;
			}
		}
		else if (i == 1) {
			if (x1[1] < 0.5*(param->minSideY + param->maxSideY)) {
				p[1] = param->minSideY;
				n[1] = -1.0;
			} else { 
				p[1] = param->maxSideY;
				n[1] = 1.0;
			}
		}
		else if (i == 2) {
			if (x1[2] < 0.5*(param->minSideZ + param->maxSideZ)) {
				p[2] = param->minSideZ;
				n[2] = -1.0;
			} else { 
				p[2] = param->maxSideZ;
				n[2] = 1.0;
			}
		}
		
		w[0] = x1[0] - p[0];
		w[1] = x1[1] - p[1];
		w[2] = x1[2] - p[2];
		
		D =  n[0]*t[0] + n[1]*t[1] + n[2]*t[2];
		N = -n[0]*w[0] - n[1]*w[1] - n[2]*w[2];
		
		// Segment is parallel to the plane
		if (fabs(D) < 1.e-5) {
			if (N == 0.0) continue; // Segment lies in plane
			else continue; // No intersection
		}
		
		// Compute intersection parameter
		stmp = N / D;
		if (stmp < 0.0 || stmp > 1.0) continue;
		
		// Compute intersection point
		if (inter == 0 || (inter == 1 && stmp < st)) {
			inter = 1;
			st = stmp;
			pos[0] = x1[0] + st * t[0];
			pos[1] = x1[1] + st * t[1];
			pos[2] = x1[2] + st * t[2];
			*dir = i;
			norm[0] = n[0];
			norm[1] = n[1];
			norm[2] = n[2];
		}
		
	}
	
	*s = st;
	return inter;
}

/*-------------------------------------------------------------------------
 *
 *      Function:    AdjustNodePosition
 *      Description: Handle nodes that are outside of the simulation domain
 *
 *------------------------------------------------------------------------*/
void AdjustNodePosition(Home_t *home, Abaqus_t* abaqus)
{
        int     i, j, nc, splitStatus; 
        int     globalOp; 
        int     armCount, armList, arm12, arm21;
        double   x, y, z;  
        double   pos1[3], pos2[3], vel1[3], vel2[3];
        double   x1[3], x2[3];
        Param_t *param; 
        Node_t  *node, *nbr; 
        Node_t  *splitNode1, *splitNode2;
        Node_t  *tmpNode1, *tmpNode2;
          
        param = home->param;
        globalOp = 1; 
        
        int bt1, bt2, bt3;

        
        for (i = 0; i < home->newNodeKeyPtr; i++) {
            
            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
                continue;
            }
            
            bt1 = GetNodeBoundaryPosition(home, node);
            if (bt1 == POS_SURFACE) node->constraint = SURFACE_NODE;
            
/*
 *          Loop through all arms connected to this node.  Note: the loop
 *          control variables <j> and <nc> must decremented on loop
 *          iterations during which an arm is removed or we could end
 *          up skipping arms.
 */
            nc = node->numNbrs;

            for (j = 0; j < nc; j++) {
              
                nbr = GetNeighborNode(home, node, j);
                if (nbr == (Node_t *)NULL) continue;
/*
 *              Skip segments not owned by the current node
 */
                if (OrderNodes(node, nbr) >= 0) continue; 
                
                bt2 = GetNodeBoundaryPosition(home, nbr);
				if (bt2 == POS_SURFACE) nbr->constraint = SURFACE_NODE;

/*
 *              If both endpoints of the segment are within the
 *              box, or 1 node is within the box and the other is
 *              on the surface, there's no need to do anything.
 */           
                if ((bt1 == POS_INSIDE  && bt2 == POS_INSIDE)  ||
                    (bt1 == POS_INSIDE  && bt2 == POS_SURFACE) ||
                    (bt1 == POS_SURFACE && bt2 == POS_INSIDE)) {
					continue;
				}
/*
 *              If any of the following circumstances are true, we
 *              can simply cut the connection between the two nodes
 *              and continue:
 *                  - If both nodes are on the same surface (and the same element)
 *                  - If one node is on the surface and the
 *                    other is outside the box
 *                  - If both nodes are outside the box.
 */		
				if ((bt1 == POS_SURFACE && bt2 == POS_SURFACE) ||
				    (bt1 == POS_SURFACE && bt2 == POS_OUTSIDE) ||
				    (bt1 == POS_OUTSIDE && bt2 == POS_SURFACE) ||
				    (bt1 == POS_OUTSIDE && bt2 == POS_OUTSIDE)) {

                    ChangeArmBurg(home, node, &node->nbrTag[j], 0, 0, 0,
                                  0, 0, 0, globalOp, DEL_SEG_NONE);
                    ChangeArmBurg(home, nbr, &node->myTag, 0, 0, 0,
                                  0, 0, 0, globalOp, DEL_SEG_NONE);     

                    nc--;
                    j--;

                    continue;
                }
        
/*
 *              If we've reached this point, the segment consists of one
 *              node inside the box and the other outside so find the
 *              location at which the segment intersects the surface
 *              and split the current node creating a new node on the
 *              surface.
 *
 *              We probably don't need to worry about passing in correct
 *              velocity values for the two nodes since force and velocity will
 *              be recalculated later.
 */
                /*
                printf("node1: %d, coord=%f %f %f, pos=%d\n", 
                node->myTag.index, node->x, node->y, node->z, bt1);
				printf("node2: %d, coord=%f %f %f, pos=%d\n", 
                nbr->myTag.index, nbr->x, nbr->y, nbr->z, bt2);
                */
                
                int inter, dir;
                double s, dl, n1n2;
                double norm[3], dv[3], n1[3], n2[3];
                
                if (bt1 == POS_INSIDE) {
					tmpNode1 = node;
					tmpNode2 = nbr;
				} else {
					tmpNode1 = nbr;
					tmpNode2 = node;
				}
					
				x1[0] = tmpNode1->x;	x2[0] = tmpNode2->x;
				x1[1] = tmpNode1->y;	x2[1] = tmpNode2->y;
				x1[2] = tmpNode1->z;	x2[2] = tmpNode2->z;
					
				// USE s for velocity here?
				inter = SurfaceNodeIntersection(home, tmpNode1, tmpNode2, pos2, &s, &dir, norm); 
                
                if (inter != 1) {
					printf("node1: %f %f %f, pos=%d\n", x1[0], x1[1], x1[2], bt1);
					printf("node2: %f %f %f, pos=%d\n", x2[0], x2[1], x2[2], bt2);
                    Fatal("There has to be an intersection point!");
                }

                armCount = 1;
                armList = j;
                
/*
 *              At this stage, tmpNode1 points to the node inside the box
 *              and tmpNode2 points to the node outside the box.
 *              To avoid ending up with very small segments, first try
 *              to see if tmpNode1 is close to the surface. If so, just
 *              move the node to the surface instead of splitting it.
 *              To do this, first check the distance between the node and
 *              the surface, make sure the domain owns the node, and verify
 *              that the node is a discretization node belonging to a single
 *              dislocation line.
 */
            
                dv[0] = pos2[0] - x1[0];
				dv[1] = pos2[1] - x1[1];
				dv[2] = pos2[2] - x1[2];
				dl = sqrt(dv[0]*dv[0]+dv[1]*dv[1]+dv[2]*dv[2]);
                
                if (dl < 0.25*param->minSeg && tmpNode1->numNbrs == 2 &&
                    DomainOwnsSeg(home, OPCLASS_REMESH, home->myDomain, &tmpNode1->myTag)) {
					
					//printf("--dl=%f < minSeg=%f\n", dl, 0.25*param->minSeg);					

/*
 *              	Check that both arms of the node belongs to the same
 *                  plane such that tmpNode1 can be moved safely to the
 *                  surface without inducing glide violations.
 */									
					n1[0] = tmpNode1->nx[0];
					n1[1] = tmpNode1->ny[0];
					n1[2] = tmpNode1->nz[0];
					NormalizeVec(n1);
					
					n2[0] = tmpNode1->nx[1];
					n2[1] = tmpNode1->ny[1];
					n2[2] = tmpNode1->nz[1];
					NormalizeVec(n2);
	
					n1n2 = n1[0]*n2[0] + n1[1]*n2[1] + n1[2]*n2[2];
					if (fabs(fabs(n1n2)-1.0) < 1.0e-5) {
					
						tmpNode1->x = pos2[0];
						tmpNode1->y = pos2[1];
						tmpNode1->z = pos2[2];
						
						tmpNode1->constraint = SURFACE_NODE;
						tmpNode1->constraint_Dir = dir;
						tmpNode1->constraint_Normal[0] = norm[0];
						tmpNode1->constraint_Normal[1] = norm[1];
						tmpNode1->constraint_Normal[2] = norm[2];
					
/*
 *              		Find the indices of the arms connecting the two
 *              		nodes and break the connection in both directions.
 */
						arm12 = GetArmID(home, tmpNode1, tmpNode2);
						arm21 = GetArmID(home, tmpNode2, tmpNode1);

						ChangeArmBurg(home, tmpNode1, &tmpNode1->nbrTag[arm12],
									  0, 0, 0, 0, 0, 0, globalOp, DEL_SEG_NONE);
						ChangeArmBurg(home, tmpNode2, &tmpNode2->nbrTag[arm21],
									  0, 0, 0, 0, 0, 0, globalOp, DEL_SEG_NONE);
									  
						//printf("---move node %d to surf\n", tmpNode1->myTag.index);
						
						nc--;
						j--;
							
						continue;
					}
				}

/*
 *              In the following we will try to split the node owned by
 *              the domain into a node remaining at the same position
 *              and a node positioned at the surface.
 */

                pos1[0] = node->x;
                pos1[1] = node->y;
                pos1[2] = node->z;

                vel1[0] = node->vX;
                vel1[1] = node->vY;
                vel1[2] = node->vZ;

                vel2[0] = 0.0;
                vel2[1] = 0.0;
                vel2[2] = 0.0;

                splitStatus = SplitNode(home, OPCLASS_REMESH, node,
                                        pos1, pos2, vel1, vel2, armCount,
                                        &armList, globalOp, &splitNode1,
                                        &splitNode2, 0);
                                      

                if (splitStatus != SPLIT_SUCCESS) {
					Fatal("AdjustNodePosition: SplitNode failed!");
					continue;
				}

/*
 *              In this case, SplitNode() *should* be returning the
 *              newly created node in <splitNode2> leaving the original
 *              node in its original location, but just to be safe,
 *              we'll add a couple checks to handle things in the
 *              event that the original node gets shifted and the
 *              new node placed where node was originally located.
 *              Yes, a bit of a mess, but SplitNode() can do that.
 */
                Node_t *verifNode;
                
                if (node == splitNode1) {
 /*
 *                  Node was left at its original position.  If it was inside
 *                  the box, we'll cut the connection between the new node
 *                  (splitNode2) and the neighbor node, otherwise we cut
 *                  the connection between node and the new node.
 */
                    if (bt1 == POS_INSIDE) {
                        tmpNode1 = nbr;
                        tmpNode2 = splitNode2;
                        verifNode = node;
                    } else {
                        tmpNode1 = node;
                        tmpNode2 = splitNode2;
                        verifNode = nbr;
                    }
                } 
                else {
/*
 *                  Node was repositioned to the surface and the new node
 *                  (splitNode1) created at node's original position.
 *                  If node was originally inside the box, we need to
 *                  cut the connection between node and nbr, otherwise
 *                  we cut the connection between node and spliNode1 (the
 *                  new node).
 */
					if (bt1 == POS_INSIDE) {
                        tmpNode1 = splitNode2;
                        tmpNode2 = nbr;
                        verifNode = splitNode1;
                    } else { /* node was outside the box */
                        tmpNode1 = splitNode1;
                        tmpNode2 = splitNode2;
                        verifNode = nbr;
                    }
/*
 *                  Point <node> to the node at the original position
 *                  before continuing.
 */
                    node = splitNode1;
                }

/*
 *              Find the indices of the arms connecting the two
 *              nodes and break the connection in both directions.
 */
                arm12 = GetArmID(home, tmpNode1, tmpNode2);
                arm21 = GetArmID(home, tmpNode2, tmpNode1);

                ChangeArmBurg(home, tmpNode1, &tmpNode1->nbrTag[arm12],
                              0, 0, 0, 0, 0, 0, globalOp, DEL_SEG_NONE);
                ChangeArmBurg(home, tmpNode2, &tmpNode2->nbrTag[arm21],
                              0, 0, 0, 0, 0, 0, globalOp, DEL_SEG_NONE);


                // Check splitNode2 is on the surface
                bt3 = GetNodeBoundaryPosition(home, splitNode2);
                
				if (bt3 == POS_SURFACE) {
					splitNode2->constraint = SURFACE_NODE;
					splitNode2->constraint_Dir = dir;
					splitNode2->constraint_Normal[0] = norm[0];
					splitNode2->constraint_Normal[1] = norm[1];
					splitNode2->constraint_Normal[2] = norm[2];
					//printf("node1: %f %f %f\n", x1[0], x1[1], x1[2]);
					//printf("node2: %f %f %f\n", x2[0], x2[1], x2[2]);
					//printf("nodes1: %f %f %f\n", verifNode->x, verifNode->y, verifNode->z);
					//printf("nodes2: %f %f %f\n", splitNode2->x, splitNode2->y, splitNode2->z);
					//printf("---\n");
				} else {
					printf("x-bounds: %f %f\n", param->minSideX, param->maxSideX);
					printf("y-bounds: %f %f\n", param->minSideY, param->maxSideY);
					printf("z-bounds: %f %f\n", param->minSideZ, param->maxSideZ);
					printf("node1: %f %f %f\n", x1[0], x1[1], x1[2]);
					printf("node2: %f %f %f\n", x2[0], x2[1], x2[2]);
					printf("nodei: %f %f %f\n", splitNode2->x, splitNode2->y, splitNode2->z);
					printf("sdist: %e\n", s);
					Fatal("AdjustNodePosition: SplitNode2 has to be on surface!, pos=%d", bt3); 
				}

                nc--;
                j--;
            }
        }
        
        //printf("End AdjustNodePosition\n");
		//PrintSurfaceNodes(home);

        return;     
}

void PrintSurfaceNodes(Home_t *home)
{
		int        i, j, bt;
		Node_t     *node, *nbr;
		double dx, dy, dz, dl;

		
/*
 *      Loop through all the local nodes and look for segments that
 *      intersect a surface.
 */
		for (i = 0; i < home->newNodeKeyPtr; i++) {

			if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
			
			bt = GetNodeBoundaryPosition(home, node);
			
			if (bt == POS_OUTSIDE && node->numNbrs != 0) {
				printf("--Outside node - id=%d, pos=%f,%f,%f\n", 
				node->myTag.index, node->x, node->y, node->z);
				/*
				for (j = 0; j < node->numNbrs; j++) {
					nbr = GetNeighborNode(home, node, j);
					if (nbr == (Node_t *)NULL) continue;
					if (OrderNodes(node, nbr) >= 0) continue;
					printf("---arm%d - id=%d\n", j, nbr->myTag.index);
				}
				*/
			}

			for (j = 0; j < node->numNbrs; j++) {
				
				nbr = GetNeighborNode(home, node, j);
				if (nbr == (Node_t *)NULL) continue;
				if (OrderNodes(node, nbr) >= 0) continue;
				
				dx = nbr->x - node->x;
				dy = nbr->y - node->y;
				dz = nbr->z - node->z;
                dl = sqrt(dx*dx+dy*dy+dz*dz);
                
                if (dl < 0.25*home->param->minSeg) {
					printf("--Segment %d-%d of length=%f < minSeg=%f\n", 
					node->myTag.index, nbr->myTag.index, dl, 0.25*home->param->minSeg); 
				}
                
			}

			if (node->constraint != SURFACE_NODE) continue;
			
			/*printf("--Surface node - id=%d, pos=%f,%f,%f, v=%e,%e,%e, f=%e,%e,%e\n", 
			node->myTag.index, node->x, node->y, node->z, node->vX, node->vY, node->vZ, 
			node->fX, node->fY, node->fZ);*/
			
			printf("--Surface node - id=%d, pos=%f,%f,%f\n", 
			node->myTag.index, node->x, node->y, node->z);
		}
}
