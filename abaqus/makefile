############################################################################
#
#    makefile: controls the build of various ParaDiS support utilities
#
#    Builds the following utilities:
#
#        paradisgen     --  problem generator
#        paradisrepart  --  creates a new problem decomposition with a new
#                           domain geometry from an previous nodal data file
#        paradisconvert --
#        ctablegen      --
#
############################################################################
include ../makefile.sys
include ../makefile.setup

DEFS += -D_ABAQUS
#DEFS += -D_ABQ_TEST1
#DEFS += -D_ABQ_TEST2
DEFS += -D_NOYOFFESTRESS
#DEFS += -D_NOVIRTUALSEG
DEFS += -D_CONTINUOUS_STRESSFIELD
DEFS += -D_ABQIMGSTRESS
# we need the following line to turn off multi node splitting for partials
#DEFS += -DMULTI_NODE_SPLIT_FREQ=100000

## The following flags for data output

DEFS += -D_PRINTSTRESS
# DEFS += -D_WRITENODEFORCE

SRCDIR = ../src
INCDIR = ../include
BINDIR = ../bin

#
#	The utilities use various source modules from the parallel
#       code located in the parent directory.  Maintain a list of
#	these files.
#
#	These modules are compiled in the parent directory with a
#	different set of preprocessor definitions than are needed
#	here, so we need to create links in this directory back to
#	the source modlues and create separate object modules for
#	these sources.
#

EXTERN_C_SRCS = CellCharge.c             \
      Collision.c              \
      CommSendGhosts.c         \
      CommSendGhostPlanes.c    \
      CommSendMirrorNodes.c    \
      CommSendRemesh.c         \
      CommSendSecondaryGhosts.c \
      CommSendSegments.c       \
      CommSendVelocity.c       \
      CorrectionTable.c        \
      ParadisFinish.c          \
      DebugFunctions.c         \
      Decomp.c                 \
      DisableUnneededParams.c  \
      DLBfreeOld.c             \
      deWitInteraction.c       \
      FindPreciseGlidePlane.c  \
      FixRemesh.c              \
      FMComm.c                 \
      FMSigma2.c               \
      FMSupport.c              \
      FreeInitArrays.c         \
      GenerateOutput.c         \
      GetDensityDelta.c        \
      GetNewNativeNode.c       \
      GetNewGhostNode.c        \
      Gnuplot.c                \
      Heap.c                   \
      InitCellDomains.c        \
      InitCellNatives.c        \
      InitCellNeighbors.c      \
      InitHome.c               \
      InitRemoteDomains.c      \
      InitSendDomains.c        \
      LoadCurve.c              \
      Matrix.c                 \
      Meminfo.c                \
      MemCheck.c               \
      Migrate.c                \
      MobilityLaw_FCC_0b.c     \
      MobilityLaw_FCC_climb.c  \
      NodeVelocity.c           \
      OsmoticForce.c           \
      ParadisThread.c          \
      Parse.c                  \
      PickScrewGlidePlane.c    \
      QueueOps.c               \
      ReadRestart.c	       \
      ReadBinaryRestart.c      \
      RBDecomp.c               \
      RSDecomp.c               \
      RemapInitialTags.c       \
      RemeshRule_3.c           \
      RemoteSegForces.c        \
      RemoveNode.c             \
      ResetGlidePlanes.c       \
      SortNativeNodes.c        \
      SortNodesForCollision.c  \
      SplitSurfaceNodes.c      \
      Tecplot.c                \
      Timer.c                  \
      WriteArms.c              \
      WriteAtomEye.c           \
      WriteBinaryRestart.c     \
      WriteDensFlux.c          \
      WriteDensityField.c      \
      WriteForce.c             \
      WriteFragments.c         \
      WritePoleFig.c           \
      WritePovray.c            \
      WriteProp.c              \
      WriteRestart.c           \
      WriteVelocity.c          \
      WriteVisit.c   

EXTERN_CPP_SRCS = DisplayC.C       \
                  display.C 

EXTERN_SRCS = $(EXTERN_C_SRCS) $(EXTERN_CPP_SRCS)
EXTERN_OBJS = $(EXTERN_C_SRCS:.c=.o) $(EXTERN_CPP_SRCS:.C=.o)

#
#       Define the sources in the partial/ directory
#

# Files that need to be merged with partial/ directory
#                   Initialize.c          (1 block)
#                   LocalSegForces.c      (3 blocks) 
#                   NodeForce.c           (6 blocks) 
#                   Param.c               (1 block) 
#                   Topology.c            (RemoveDoubleLinks, SplitNode, MergeNode) 

#
#	Define the exectutable, source and object modules for
#	the problem generator.
#

PARADISABQ     = paradisabq
LIBPARADIS     = libparadis
PARADISABQ_DLL = $(BINDIR)/libparadis.so
PARADISABQ_BIN = $(BINDIR)/$(PARADISABQ)

PARADISABQ_C_SRCS = ABQ_Main.c                \
                    abq_creat_mesh.c          \
                    abq_calc_stress.c         \
                    ABQ_Util.c                \
	            SP_AdjustNodePosition.c   \
                    Abaqus_Remesh.c           \
                    abaqus.c                  \
                    AllSegmentStress.c        \
                    AllYoffeStress.c          \
                    CrossSlipFCC.c            \
                    CrossSlipBCC.c            \
                    PredictiveCollision.c     \
                    ProximityCollision.c      \
                    Compute.c                 \
                    CrossSlip.c               \
                    DeltaPlasticStrain.c      \
                    DeltaPlasticStrain_BCC.c  \
                    DeltaPlasticStrain_FCC.c  \
                    DispStress.c              \
                    ForwardEulerIntegrator.c  \
                    Initialize.c              \
                    InputSanity.c             \
                    LocalSegForces.c          \
                    MobilityLaw_BCC_0.c       \
                    MobilityLaw_FCC_0.c       \
                    MobilityLaw_BCC_0b.c      \
                    MobilityLaw_BCC_glide.c   \
                    MobilityLaw_Relax.c       \
                    NodeForce.c               \
                    ParadisInit.c            \
                    ParadisStep.c             \
                    Param.c                   \
                    Plot.c                    \
                    PrintStress.c             \
                    RemeshRule_2.c            \
                    Remesh.c                  \
                    SegmentStress.c           \
                    SemiInfiniteSegSegForce.c \
                    TrapezoidIntegrator.c     \
                    Topology.c                \
                    Util.c		              \
                    Yoffe.c                   \
                    Yoffe_corr.c             

PARADISABQ_INCS.linux = -I/usr/include
PARADISABQ_LIBS.linux = -L/usr/lib/ -lm

PARADISABQ_INCS.mc2 = -I${HOME}/usr/include
PARADISABQ_LIBS.mc2 = -L${HOME}/usr/lib/ -lm

PARADISABQ_INCS = -I Include $(PARADISABQ_INCS.$(SYS))
PARADISABQ_LIBS = $(PARADISABQ_LIBS.$(SYS))

PARADISABQ_SRCS = $(PARADISABQ_C_SRCS) $(PARADISABQ_CPP_SRCS)
PARADISABQ_OBJS = $(PARADISABQ_C_SRCS:.c=.o) $(PARADISABQ_CPP_SRCS:.C=.o)



###########################################################################
#
#	Define a rule for converting .c files to object modules.
#	All modules are compile serially in this directory
#
###########################################################################

.c.o:		makefile ../makefile.sys ../makefile.setup
		$(CC) $(OPT) $(CCFLAG) $(PARADISABQ_INCS) $(INCS) -c $<

.C.o:		makefile ../makefile.sys ../makefile.setup
		$(CPP) $(OPT) $(CPPFLAG) $(INCS) -c $<


###########################################################################
#
#	Define all targets and dependencies below
#
###########################################################################

all:		$(EXTERN_OBJS) $(PARADISABQ) 

clean:
		rm -f *.o $(EXTERN_SRCS) $(PARADISABQ_BIN) test_YoffeInfStress

depend:		 *.c $(SRCDIR)/*.c $(INCDIR)/*.h makefile
		makedepend -Y$(INCDIR) *.c  -fmakefile.dep

#
#	Create any necessary links in the current directory to source
#	modules located in the SRCDIR directory
#

$(EXTERN_SRCS): $(SRCDIR)/$@
		- @ ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1

# For vip
#$(EXTERN_SRCS): $(SRCDIR)/$@
#                ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1


$(PARADISABQ):	$(PARADISABQ_BIN)
$(PARADISABQ_BIN): $(PARADISABQ_SRCS) $(PARADISABQ_OBJS) $(EXTERN_OBJS) $(HEADERS)
	echo $(PARADISABQ_OBJS)
		$(CPP) $(OPT) $(PARADISABQ_OBJS) $(EXTERN_OBJS) -o $@  $(LIB) $(PARADISABQ_LIBS)

$(LIBPARADIS): $(PARADISABQ_DLL)
$(PARADISABQ_DLL): $(PARADISABQ_SRCS) $(PARADISABQ_OBJS) $(EXTERN_OBJS) $(HEADERS)
	echo $(PARADISABQ_OBJS)
		$(CPP) -shared $(OPT) $(PARADISABQ_OBJS) $(EXTERN_OBJS) -o  $@  $(LIB) $(PARADISABQ_LIBS)

test_YoffeInfStress: test_YoffeInfStress.o AllYoffeStress.o Yoffe_corr.o Yoffe.o Util.o Heap.o QueueOps.o
	$(CC) -o $@ $^ -lm

