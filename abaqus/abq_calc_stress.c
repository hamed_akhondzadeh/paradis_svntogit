/***************************************************************************
 *
 *  Module      : abq_calc_stress.c
 *  Description : Calculates stress at any given point of the FEM domain
 * 
 **************************************************************************/


#include "Home.h"
#include <math.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef _ABAQUS

#include "ABQ.h"

double  invert33 (double A[3][3] , double Ainv[3][3])
{
	double   detA;
	int      i;
	
	detA = A[0][0] * (A[1][1]*A[2][2] - A[1][2]*A[2][1])
	     - A[0][1] * (A[1][0]*A[2][2] - A[1][2]*A[2][0])
	     + A[0][2] * (A[1][0]*A[2][1] - A[1][1]*A[2][0]) ;
	
	if (fabs(detA) < 1e-10 )
		Fatal("Zero determinant while inverting a 3x3 matrix\n");
	
	Ainv[0][0] = (A[1][1]*A[2][2] - A[1][2]*A[2][1]) / detA;
	Ainv[0][1] =-(A[0][1]*A[2][2] - A[0][2]*A[2][1]) / detA;
	Ainv[0][2] = (A[0][1]*A[1][2] - A[0][2]*A[1][1]) / detA;
		
	Ainv[1][0] =-(A[1][0]*A[2][2] - A[1][2]*A[2][0]) / detA;
	Ainv[1][1] = (A[0][0]*A[2][2] - A[0][2]*A[2][0]) / detA;
	Ainv[1][2] =-(A[0][0]*A[1][2] - A[0][2]*A[1][0]) / detA;
		
	Ainv[2][0] = (A[1][0]*A[2][1] - A[1][1]*A[2][0]) / detA;
	Ainv[2][1] =-(A[0][0]*A[2][1] - A[0][1]*A[2][0]) / detA;
	Ainv[2][2] = (A[0][0]*A[1][1] - A[0][1]*A[1][0]) / detA;
	
	return(detA);
}

/************************************************************
*
*  input:
*    A - pointer to array of 4x4 doubles (source matrix)
*  output:
*    Ainv - pointer to array of 16 doubles (invert matrix)
*
*	Reference:
*	ftp://download.intel.com/design/PentiumIII/sml/24504301.pdf
*
*************************************************************/
void invert44(double A[4][4], double Ainv[4][4], double *detA)
{
	int    i, j, k;
	double mat[16]; // Source matrix in array form
	double dst[16]; // Inverted matrix in array form
	double tmp[12]; // temp array for pairs 
	double src[16]; // array of transpose source matrix 
	
/*
 *	Convert the source matrix to an array form
 */
	k = 0;
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			mat[k] = A[i][j];
			k++;
		}
	}

/*
 *	transpose matrix
 */
	for (i = 0; i < 4; i++) {
		src[i     ] = mat[i*4    ];
		src[i + 4 ] = mat[i*4 + 1];
		src[i + 8 ] = mat[i*4 + 2];
		src[i + 12] = mat[i*4 + 3];
	}

/*
 *	calculate pairs for first 8 elements (cofactors)
 */
	tmp[0 ] = src[10] * src[15];
	tmp[1 ] = src[11] * src[14];
	tmp[2 ] = src[9 ] * src[15];
	tmp[3 ] = src[11] * src[13];
	tmp[4 ] = src[9 ] * src[14];
	tmp[5 ] = src[10] * src[13];
	tmp[6 ] = src[8 ] * src[15];
	tmp[7 ] = src[11] * src[12];
	tmp[8 ] = src[8 ] * src[14];
	tmp[9 ] = src[10] * src[12];
	tmp[10] = src[8 ] * src[13];
	tmp[11] = src[9 ] * src[12];
	
/*
 *	Calculate first 8 elements (cofactors)
 */
	dst[0]  = tmp[0]*src[5] + tmp[3]*src[6] + tmp[4 ]*src[7];
	dst[0] -= tmp[1]*src[5] + tmp[2]*src[6] + tmp[5 ]*src[7];
	dst[1]  = tmp[1]*src[4] + tmp[6]*src[6] + tmp[9 ]*src[7];
	dst[1] -= tmp[0]*src[4] + tmp[7]*src[6] + tmp[8 ]*src[7];
	dst[2]  = tmp[2]*src[4] + tmp[7]*src[5] + tmp[10]*src[7];
	dst[2] -= tmp[3]*src[4] + tmp[6]*src[5] + tmp[11]*src[7];
	dst[3]  = tmp[5]*src[4] + tmp[8]*src[5] + tmp[11]*src[6];
	dst[3] -= tmp[4]*src[4] + tmp[9]*src[5] + tmp[10]*src[6];
	dst[4]  = tmp[1]*src[1] + tmp[2]*src[2] + tmp[5 ]*src[3];
	dst[4] -= tmp[0]*src[1] + tmp[3]*src[2] + tmp[4 ]*src[3];
	dst[5]  = tmp[0]*src[0] + tmp[7]*src[2] + tmp[8 ]*src[3];
	dst[5] -= tmp[1]*src[0] + tmp[6]*src[2] + tmp[9 ]*src[3];
	dst[6]  = tmp[3]*src[0] + tmp[6]*src[1] + tmp[11]*src[3];
	dst[6] -= tmp[2]*src[0] + tmp[7]*src[1] + tmp[10]*src[3];
	dst[7]  = tmp[4]*src[0] + tmp[9]*src[1] + tmp[10]*src[2];
	dst[7] -= tmp[5]*src[0] + tmp[8]*src[1] + tmp[11]*src[2];
	
/*
 *	Calculate pairs for second 8 elements (cofactors)
 */
	tmp[0 ] = src[2]*src[7];
	tmp[1 ] = src[3]*src[6];
	tmp[2 ] = src[1]*src[7];
	tmp[3 ] = src[3]*src[5];
	tmp[4 ] = src[1]*src[6];
	tmp[5 ] = src[2]*src[5];
	tmp[6 ] = src[0]*src[7];
	tmp[7 ] = src[3]*src[4];
	tmp[8 ] = src[0]*src[6];
	tmp[9 ] = src[2]*src[4];
	tmp[10] = src[0]*src[5];
	tmp[11] = src[1]*src[4];

/*
 *	Calculate second 8 elements (cofactors)
 */
	dst[8 ]  = tmp[0 ]*src[13] + tmp[3 ]*src[14] + tmp[4 ]*src[15];
	dst[8 ] -= tmp[1 ]*src[13] + tmp[2 ]*src[14] + tmp[5 ]*src[15];
	dst[9 ]  = tmp[1 ]*src[12] + tmp[6 ]*src[14] + tmp[9 ]*src[15];
	dst[9 ] -= tmp[0 ]*src[12] + tmp[7 ]*src[14] + tmp[8 ]*src[15];
	dst[10]  = tmp[2 ]*src[12] + tmp[7 ]*src[13] + tmp[10]*src[15];
	dst[10] -= tmp[3 ]*src[12] + tmp[6 ]*src[13] + tmp[11]*src[15];
	dst[11]  = tmp[5 ]*src[12] + tmp[8 ]*src[13] + tmp[11]*src[14];
	dst[11] -= tmp[4 ]*src[12] + tmp[9 ]*src[13] + tmp[10]*src[14];
	dst[12]  = tmp[2 ]*src[10] + tmp[5 ]*src[11] + tmp[1 ]*src[9 ];
	dst[12] -= tmp[4 ]*src[11] + tmp[0 ]*src[9 ] + tmp[3 ]*src[10];
	dst[13]  = tmp[8 ]*src[11] + tmp[0 ]*src[8 ] + tmp[7 ]*src[10];
	dst[13] -= tmp[6 ]*src[10] + tmp[9 ]*src[11] + tmp[1 ]*src[8 ];
	dst[14]  = tmp[6 ]*src[9 ] + tmp[11]*src[11] + tmp[3 ]*src[8 ];
	dst[14] -= tmp[10]*src[11] + tmp[2 ]*src[8 ] + tmp[7 ]*src[9 ];
	dst[15]  = tmp[10]*src[10] + tmp[4 ]*src[8 ] + tmp[9 ]*src[9 ];
	dst[15] -= tmp[8 ]*src[9 ] + tmp[11]*src[10] + tmp[5 ]*src[8 ];
	
/*
 *	Calculate determinant
 */
	*detA=src[0]*dst[0]+src[1]*dst[1]+src[2]*dst[2]+src[3]*dst[3];
	
	if (fabs(*detA) < 1e-10 )
		Fatal("Zero determinant while inverting a 4x4 matrix\n");
	
/*
 *	Calculate matrix inverse
 */
	k = 0;
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			Ainv[i][j] = dst[k] / *detA;
			k++;
		}
	}
	
	return;
}

/******************************************************************
 ******************************************************************
 ******************************************************************/

double  calc_dN_dXi (double xi  , double Ixi  , int a ,
                     double eta , double Ieta , int b ,
                     double zi  , double Izi  , int c , int nodeNum)
{
	double  v1 , v2 , v3;
	v1 = xi  * Ixi ;
	v2 = eta * Ieta;
	v3 = zi  * Izi ;
	
	if (nodeNum < 8) {
		return( Ixi/8.0 * (1.0+v2) * (1.0+v3) * (2.0*v1+v2+v3-1.0));
	} else {
		if (b ) v2 *= eta;
		if (c ) v3 *= zi ;
		if (!a) v1  = Ixi;
		
		return( v1 * (a+1) * (1.0+v2) * (1.0+v3) / 4.0 );
	}
}

/******************************************************************
 ******************************************************************
 ******************************************************************/

double  calc_dN_dEta (double xi  , double Ixi  , int a ,
                      double eta , double Ieta , int b ,
                      double zi  , double Izi  , int c , int nodeNum)
{
	double  v1 , v2 , v3;
	v1 = xi  * Ixi ;
	v2 = eta * Ieta;
	v3 = zi  * Izi ;
	
	if (nodeNum < 8) {
		return( Ieta/8.0 * (1.0+v1) * (1.0+v3) * (v1+2.0*v2+v3-1.0));
	} else {
		if (a ) v1 *= xi  ;
		if (c ) v3 *= zi  ;
		if (!b) v2  = Ieta;
		
		return( v2 * (b+1) * (1.0+v1) * (1.0+v3) / 4.0 );
	}
}

/******************************************************************
 ******************************************************************
 ******************************************************************/

double  calc_dN_dZi (double xi  , double Ixi  , int a ,
                     double eta , double Ieta , int b ,
                     double zi  , double Izi  , int c , int nodeNum)
{
	double  v1 , v2 , v3;
	v1 = xi  * Ixi ;
	v2 = eta * Ieta;
	v3 = zi  * Izi ;
	
	if (nodeNum < 8) {
		return( Izi/8.0 * (1.0+v1) * (1.0+v2) * (v1+v2+2.0*v3-1.0));
	} else {
		if (a ) v1 *= xi ;
		if (b ) v2 *= eta;
		if (!c) v3  = Izi;
		
		return( v3 * (c+1) * (1.0+v1) * (1.0+v2) / 4.0 );
	}
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void static getStressFromDispTetrahed(Abaqus_t *abaqus, 
                                      FEMnode_t FEMnode[abaqus->nodePelem], 
                                      double x , double y , double z ,
                                      double stress[3][3])
{
	int     i , j , k;
	double  dN_dx[abaqus->nodePelem], A     [4][4], xvec [4], L[4];
	double  dN_dy[abaqus->nodePelem], Ainv  [4][4], stra1[6], detA;
	double  dN_dz[abaqus->nodePelem], strain[3][3], stra2[6];
	
/*
 *	First find the natural coordinates of this point in this element
 */
	xvec[0] = 1.0;
	xvec[1] = x  ;
	xvec[2] = y  ;
	xvec[3] = z  ;
	
	for (i = 0; i < 4; i++) {
		L[i]    = 0.0;
		A[0][i] = 1.0;
		A[1][i] = FEMnode[i].x;
		A[2][i] = FEMnode[i].y;
		A[3][i] = FEMnode[i].z;
	}
	
	invert44(A , Ainv , &detA);
	
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			L[i] += Ainv[i][j] * xvec[j];
		}
	}
	
	if (fabs(L[0]+L[1]+L[2]+L[3]-1.0) > 1e-9)
		Fatal("Error in calculating the natural coordinates in tetrahedral elements\n");
	
/*
 *	Calculating the derivatives of the shape functions with
 *	respect to x,y,z
 */
	if (abaqus->nodePelem == 4) {
		for (i = 0 ; i < 4 ; i++) {
			dN_dx[i] = Ainv[i][1];
			dN_dy[i] = Ainv[i][2];
			dN_dz[i] = Ainv[i][3];
		}
	} else {
		for (i = 0 ; i < 4 ; i++) {
			dN_dx[i] = Ainv[i][1] * (4.0*L[i] - 1.0);
			dN_dy[i] = Ainv[i][2] * (4.0*L[i] - 1.0);
			dN_dz[i] = Ainv[i][3] * (4.0*L[i] - 1.0);
		}
		
		for (i = 4 ; i < 10 ; i++) {
			if      (i < 7) j = i - 4;
			else            j = i - 7;
			
			if      (i < 6) k = j + 1;
			else if (i== 6) k = 0;
			else            k = 3;
			
			dN_dx[i] = 4.0 * (Ainv[j][1]*L[k] + Ainv[k][1]*L[j]);
			dN_dy[i] = 4.0 * (Ainv[j][2]*L[k] + Ainv[k][2]*L[j]);
			dN_dz[i] = 4.0 * (Ainv[j][3]*L[k] + Ainv[k][3]*L[j]);
		}
	}
	
/*
 *	Calculating the strain
 */
	for (i=0 ; i<3; i++) {
		for (j=0 ; j<3 ; j++) {
			strain[i][j] = 0.0;
		}
	}
		
	for (j=0 ; j<abaqus->nodePelem ; j++) {
		strain[0][0] += dN_dx[j] * FEMnode[j].Ux;
		strain[0][1] += dN_dy[j] * FEMnode[j].Ux;
		strain[0][2] += dN_dz[j] * FEMnode[j].Ux;
		strain[1][0] += dN_dx[j] * FEMnode[j].Uy;
		strain[1][1] += dN_dy[j] * FEMnode[j].Uy;
		strain[1][2] += dN_dz[j] * FEMnode[j].Uy;
		strain[2][0] += dN_dx[j] * FEMnode[j].Uz;
		strain[2][1] += dN_dy[j] * FEMnode[j].Uz;
		strain[2][2] += dN_dz[j] * FEMnode[j].Uz;
	}
	
	strain[0][1] += strain[1][0];   //strain[1][0] = strain[0][1];
	strain[0][2] += strain[2][0];   //strain[2][0] = strain[0][2];
	strain[1][2] += strain[2][1];   //strain[2][1] = strain[1][2];
	
/*
 *	Calculate the stress
 */
	stra1[0] = strain[0][0];
	stra1[1] = strain[1][1];
	stra1[2] = strain[2][2];
	stra1[3] = strain[0][1];
	stra1[4] = strain[0][2];
	stra1[5] = strain[1][2];
	
	for (i = 0 ; i < 6 ; i++) {
		stra2[i] = 0.0;
		
		for (j = 0 ; j < 6 ; j++) 
			stra2[i] += abaqus->Cij[i][j] * stra1[j];
	}

	stress[0][0] = stra2[0];
	stress[1][1] = stra2[1];
	stress[2][2] = stra2[2];
	stress[0][1] = stra2[3];  stress[1][0] = stra2[3];
	stress[0][2] = stra2[4];  stress[2][0] = stra2[4];
	stress[1][2] = stra2[5];  stress[2][1] = stra2[5];
}


/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void static getStressFromDispHexahed(Abaqus_t *abaqus, 
                                     FEMnode_t FEMnode[abaqus->nodePelem], 
                                     double xi , double eta , double zi ,
                                     double stress[3][3])
{
	int     i , j ;
	double  v1, v2, v3;
	double  dN_dXi [abaqus->nodePelem] , dN_dx [abaqus->nodePelem];  
	double  dN_dEta[abaqus->nodePelem] , dN_dy [abaqus->nodePelem];
	double  dN_dZi [abaqus->nodePelem] , dN_dz [abaqus->nodePelem];
	double  jacobi[3][3] = {0.0} , jInver[3][3] , detm;
	double  strain[3][3] , stra1[6] , stra2[6];
	
/*
 *	Calculating the derivatives of the shape functions with
 *	respect to the iso-parametric coefficients
 */
	if (abaqus->nodePelem == 8) {
		// component:        0    1    2    3    4    5    6    7
		double  Ixi [8] = {-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0};
		double  Ieta[8] = {-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0};
		double  Izi [8] = {-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0};
		
		for (j=0 ; j<8 ; j++) {
			v1 = 1.0 + xi  * Ixi [j];
			v2 = 1.0 + eta * Ieta[j];
			v3 = 1.0 + zi  * Izi [j];
			dN_dXi [j] = Ixi [j]/8.0 * v2*v3;
			dN_dEta[j] = Ieta[j]/8.0 * v1*v3;
			dN_dZi [j] = Izi [j]/8.0 * v1*v2;
		}
	} else {
		// component:         0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19
		double  Ixi [20] = {-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0,-1.0,-1.0,-1.0, 1.0,-1.0,-1.0,-1.0, 1.0, 1.0,-1.0};
		double  Ieta[20] = {-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0,-1.0,-1.0,-1.0, 1.0,-1.0,-1.0,-1.0, 1.0, 1.0};
		double  Izi [20] = {-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0,-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0,-1.0,-1.0,-1.0,-1.0};
		int      a  [20] = {  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 ,  0 ,  0 ,  0 ,  0 };
		int      b  [20] = {  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 ,  0 ,  0 ,  0 };
		int      c  [20] = {  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  1 ,  1 ,  1 ,  1 };
		
		for (j=0 ; j<20 ; j++) {
			dN_dXi [j] = calc_dN_dXi  (xi , Ixi [j], a[j], 
									   eta, Ieta[j], b[j],
									   zi , Izi [j], c[j], j);
			
			dN_dEta[j] = calc_dN_dEta (xi , Ixi [j], a[j], 
									   eta, Ieta[j], b[j],
									   zi , Izi [j], c[j], j);
			
			dN_dZi [j] = calc_dN_dZi  (xi , Ixi [j], a[j], 
									   eta, Ieta[j], b[j],
									   zi , Izi [j], c[j], j);
		}
	}
	
/*
 *	Calculating the Jacobian
 */
	for (j=0 ; j<abaqus->nodePelem ; j++) {
		jacobi[0][0] += dN_dXi [j] * FEMnode[j].x;
		jacobi[0][1] += dN_dXi [j] * FEMnode[j].y;
		jacobi[0][2] += dN_dXi [j] * FEMnode[j].z;
		jacobi[1][0] += dN_dEta[j] * FEMnode[j].x;
		jacobi[1][1] += dN_dEta[j] * FEMnode[j].y;
		jacobi[1][2] += dN_dEta[j] * FEMnode[j].z;
		jacobi[2][0] += dN_dZi [j] * FEMnode[j].x;
		jacobi[2][1] += dN_dZi [j] * FEMnode[j].y;
		jacobi[2][2] += dN_dZi [j] * FEMnode[j].z;
	}
	
/*	printf("\n%f %f %f\n" , xi ,eta , zi);
	printf("%f %f %f\n" , jacobi[0][0] , jacobi[0][1] , jacobi[0][2]);
	printf("%f %f %f\n" , jacobi[1][0] , jacobi[1][1] , jacobi[1][2]);
	printf("%f %f %f\n" , jacobi[2][0] , jacobi[2][1] , jacobi[2][2]);*/
	
	detm = invert33(jacobi , jInver);

/*
 *	Calculating the derivatives of the shape functions with
 *	respect to the x, y, z
 */
	for (j=0 ; j<abaqus->nodePelem ; j++) {
		dN_dx[j] = jInver[0][0] * dN_dXi[j] + jInver[0][1] * dN_dEta[j] + jInver[0][2] * dN_dZi[j];
		dN_dy[j] = jInver[1][0] * dN_dXi[j] + jInver[1][1] * dN_dEta[j] + jInver[1][2] * dN_dZi[j];
		dN_dz[j] = jInver[2][0] * dN_dXi[j] + jInver[2][1] * dN_dEta[j] + jInver[2][2] * dN_dZi[j];
	}
	
/*
 *	Calculating the strain
 */
	for (i=0 ; i<3; i++) {
		for (j=0 ; j<3 ; j++) {
			strain[i][j] = 0.0;
		}
	}
		
	for (j=0 ; j<abaqus->nodePelem ; j++) {
		strain[0][0] += dN_dx[j] * FEMnode[j].Ux;
		strain[0][1] += dN_dy[j] * FEMnode[j].Ux;
		strain[0][2] += dN_dz[j] * FEMnode[j].Ux;
		strain[1][0] += dN_dx[j] * FEMnode[j].Uy;
		strain[1][1] += dN_dy[j] * FEMnode[j].Uy;
		strain[1][2] += dN_dz[j] * FEMnode[j].Uy;
		strain[2][0] += dN_dx[j] * FEMnode[j].Uz;
		strain[2][1] += dN_dy[j] * FEMnode[j].Uz;
		strain[2][2] += dN_dz[j] * FEMnode[j].Uz;
	}
	
	strain[0][1] += strain[1][0];   //strain[1][0] = strain[0][1];
	strain[0][2] += strain[2][0];   //strain[2][0] = strain[0][2];
	strain[1][2] += strain[2][1];   //strain[2][1] = strain[1][2];
	
/*
 *	Calculate the stress
 */
	stra1[0] = strain[0][0];
	stra1[1] = strain[1][1];
	stra1[2] = strain[2][2];
	stra1[3] = strain[0][1];
	stra1[4] = strain[0][2];
	stra1[5] = strain[1][2];
	
	for (i = 0 ; i < 6 ; i++) {
		stra2[i] = 0.0;
		
		for (j = 0 ; j < 6 ; j++) 
			stra2[i] += abaqus->Cij[i][j] * stra1[j];
	}

	stress[0][0] = stra2[0];
	stress[1][1] = stra2[1];
	stress[2][2] = stra2[2];
	stress[0][1] = stra2[3];  stress[1][0] = stra2[3];
	stress[0][2] = stra2[4];  stress[2][0] = stra2[4];
	stress[1][2] = stra2[5];  stress[2][1] = stra2[5];
}


/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void static getStressFromNodeStressTetrahed(
                    Abaqus_t *abaqus, 
                    FEMnode_t FEMnode[abaqus->nodePelem], 
                    double x , double y , double z ,
                    double stress[3][3])
{
	int     i , j ,k;
	double  A   [4][4], xvec[4], Ni [abaqus->nodePelem];
	double  Ainv[4][4], L   [4], detA;
	
/*
 *	First find the natural coordinates of this point in this element
 */
	xvec[0] = 1.0;
	xvec[1] = x  ;
	xvec[2] = y  ;
	xvec[3] = z  ;
	
	for (i = 0; i < 4; i++) {
		L[i]    = 0.0;
		A[0][i] = 1.0;
		A[1][i] = FEMnode[i].x;
		A[2][i] = FEMnode[i].y;
		A[3][i] = FEMnode[i].z;
	}
	
	invert44(A , Ainv , &detA);
	
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			L[i] += Ainv[i][j] * xvec[j];
		}
	}
	
	if (fabs(L[0]+L[1]+L[2]+L[3]-1.0) > 1e-9)
		Fatal("Error in calculating the natural coordinates in tetrahedral elements\n");
	
/*
 *	Calculating shape functions values at this point
 */
	if (abaqus->nodePelem == 4) {
		for (i = 0 ; i < 4 ; i++) {
			Ni[i] = L[i];
		}
	} else {
		for (i = 0 ; i < 4 ; i++) {
			Ni[i] = L[i] * (2.0*L[i] - 1.0);
		}
		
		for (i = 4 ; i < 10 ; i++) {
			if      (i < 7) j = i - 4;
			else            j = i - 7;
			
			if      (i < 6) k = j + 1;
			else if (i== 6) k = 0;
			else            k = 3;
			
			Ni[i] = 4.0*L[j]*L[k];
		}
	}
	
/*
 *	Calculating the stress
 */
	for (i=0 ; i<3; i++) {
		for (j=0 ; j<3 ; j++) {
			stress[i][j] = 0.0;
		}
	}
		
	for (i=0 ; i<abaqus->nodePelem ; i++) {
		for (j=0 ; j<3 ; j++) {
			for (k=0 ; k<3 ; k++) {
				stress[j][k] += Ni[i] * FEMnode[i].stress[j][k];
			}
		}
	}
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void getStressFromNodeStress(Abaqus_t *abaqus, 
                             FEMnode_t FEMnode[abaqus->nodePelem] , 
                             double xi , double eta , double zi ,
					         double stress[3][3])
{
	int     i  , j  , k  ;
	double  v1 , v2 , v3 , N_i[abaqus->nodePelem];
	
/*
 *	Calculating the values of the shape functions for the 
 *	point defined by (xi, eta, zi)
 */
	if (abaqus->nodePelem == 8) {
		// component:         0    1    2    3    4    5    6    7
		double  Ixi [8] = {-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0};
		double  Ieta[8] = {-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0};
		double  Izi [8] = {-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0};
		
/*
 *		Calculating shape function values
 */
		for (i=0 ; i<8 ; i++) {
			v1 = xi  * Ixi [i];
			v2 = eta * Ieta[i];
			v3 = zi  * Izi [i];
			
			N_i [i] = (1.0+v1) * (1.0+v2) * (1.0+v3) / 8.0;
		}
	} else {
		// component:         0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19
		double  Ixi [20] = {-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0,-1.0,-1.0,-1.0, 1.0,-1.0,-1.0,-1.0, 1.0, 1.0,-1.0};
		double  Ieta[20] = {-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0,-1.0,-1.0,-1.0, 1.0,-1.0,-1.0,-1.0, 1.0, 1.0};
		double  Izi [20] = {-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0,-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0,-1.0,-1.0,-1.0,-1.0};
		int      a  [20] = {  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 ,  0 ,  0 ,  0 ,  0 };
		int      b  [20] = {  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 ,  0 ,  0 ,  0 };
		int      c  [20] = {  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  0 ,  1 ,  1 ,  1 ,  1 };
		
/*
 *		Calculating shape function values
 */
		for (i=0 ; i<8 ; i++) {
			v1 = xi  * Ixi [i];
			v2 = eta * Ieta[i];
			v3 = zi  * Izi [i];
			
			N_i [i] = (1.0+v1) * (1.0+v2) * (1.0+v3) * (v1+v2+v3-2.0) / 8.0;
		}
		
		for (i=8 ; i<20 ; i++) {
			v1 = xi  * Ixi [i];
			v2 = eta * Ieta[i];
			v3 = zi  * Izi [i];
			
			if (a[i]) v1 *= xi ;
			if (b[i]) v2 *= eta;
			if (c[i]) v3 *= zi ;
				
			N_i [i] = (1.0+v1) * (1.0+v2) * (1.0+v3) / 4.0;
		}
	}
	
/*
 *	Calculating the stress
 */
	for (i=0 ; i<3; i++) {
		for (j=0 ; j<3 ; j++) {
			stress[i][j] = 0.0;
		}
	}
		
	for (i=0 ; i<abaqus->nodePelem ; i++) {
		for (j=0 ; j<3 ; j++) {
			for (k=0 ; k<3 ; k++) {
				stress[j][k] += N_i[i] * FEMnode[i].stress[j][k];
			}
		}
	}
	
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

int mathSign (double x)
{
	if      (x < 0) return -1;
	else if (x > 0) return  1;
	else            return  0;
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

static void sameSide(double X1[3], double X2[3], double X3[3], 
                     double X4[3], double p [3], int *side)
{
    int     i;
	double  v21[3] , v41[3] , dotV4 , normal[3];
	double  v31[3] , vP1[3] , dotP  ;
	
	for (i = 0 ; i < 3 ; i++) {
		v21[i] = X2[i] - X1[i];
		v31[i] = X3[i] - X1[i];
		v41[i] = X4[i] - X1[i];
		vP1[i] = p [i] - X1[i];
	}
	cross(v21, v31 , normal);
    dotV4 = DotProduct(normal, v41);
    dotP  = DotProduct(normal, vP1);
	
	if (dotP == 0.0) *side = 0;
	else if (mathSign(dotV4) == mathSign(dotP)) *side = 1;
	else *side = -1;
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/
 
void ABQ_calc_stress(Home_t *home   , Abaqus_t *abaqus, 
                     double  pos[3] , double    stress[3][3])
{
	int         iElmX , iElmY , iElmZ , iElm , iNode , i, j, inElm;
	int         side1 , side2 , side3 , side4;
	double      dX    , dY    , dZ    , X1[3], X2[3] ;
	double      Xi    , Eta   , Zi    , X3[3], X4[3] ;
	FEMnode_t   FEMnode[abaqus->nodePelem];
	
	if (strcmp(home->param->abq_elemType, "tetrahedral") == 0) {
		inElm = 0;
		
		for (i = 0; i < abaqus->nElems; i++) {
			for (j = 0 ; j < abaqus->nodePelem ; j++) {
				iNode = abaqus->FEMelems[i].node[j];
				FEMnode[j] = abaqus->FEMnodes[iNode];
			}
			
			X1[0] = FEMnode[0].x; X1[1] = FEMnode[0].y;
			X1[2] = FEMnode[0].z;
			X2[0] = FEMnode[1].x; X2[1] = FEMnode[1].y;
			X2[2] = FEMnode[1].z;
			X3[0] = FEMnode[2].x; X3[1] = FEMnode[2].y;
			X3[2] = FEMnode[2].z;
			X4[0] = FEMnode[3].x; X4[1] = FEMnode[3].y;
			X4[2] = FEMnode[3].z;
		

			//printf("pos = [%f,%f,%f], X=[%f,%f,%f], X2=[%f,%f,%f], X3[%f,%f,%f], X4=[%f,%f,%f]\n", pos[0],pos[1],pos[2], X1[0],X1[1],X1[2], X2[0],X2[1],X2[2], X3[0],X3[1],X3[2], X4[0],X4[1],X4[2]);	
			sameSide(X1, X2, X3, X4, pos, &side1);
			sameSide(X1, X2, X4, X3, pos, &side2);
			sameSide(X1, X3, X4, X2, pos, &side3);
			sameSide(X2, X3, X4, X1, pos, &side4);
			
			if (side1 >= 0 && side2 >= 0 && side3 >= 0 && side4 >= 0){
				inElm = 1;
				break;
			}
		}
		//Xiaohan allows node outside the cube
		if (inElm == 0) {
		  //Fatal("Point was not found inside any element\n");
		  printf("Point was not found inside any element. pos = [%f,%f,%f]\n",pos[0],pos[1],pos[2]);
		  printf("But this is possible when dislocation exits free surface\n");
		  return;
		} else {
#ifdef _CONTINUOUS_STRESSFIELD
	//	getStressFromNodeStressTetrahed(abaqus, FEMnode, pos[0],
	//	                          pos[1], pos[2] , stress);
		getStressFromDispTetrahed(abaqus, FEMnode, pos[0],
		                          pos[1], pos[2] , stress);
#else
		getStressFromDispTetrahed(abaqus, FEMnode, pos[0],
		                          pos[1], pos[2] , stress);
#endif
		}
		return;
	}
	
/*
 *	Finding the element in which the point pos resides in
 */
	dX = abaqus->ABQLx / (1.0*abaqus->nx);
	dY = abaqus->ABQLy / (1.0*abaqus->ny);
	dZ = abaqus->ABQLz / (1.0*abaqus->nz);
	
	iElmX = (int) ((pos[0] - home->param->minSideX) / dX );
	iElmY = (int) ((pos[1] - home->param->minSideY) / dY );
	iElmZ = (int) ((pos[2] - home->param->minSideZ) / dZ );
	
	if (iElmX == abaqus->nx) iElmX--;
	if (iElmY == abaqus->ny) iElmY--;
	if (iElmZ == abaqus->nz) iElmZ--;
		
	iElm = iElmX + (iElmY + iElmZ * abaqus->ny) * abaqus->nx;
	
/*
 *	Pack the FEM nodes of the corresponding element and convert 
 *	the position of the point pos to the iso-parametric system 
 */
	for (i = 0 ; i < abaqus->nodePelem ; i++) {
		iNode = abaqus->FEMelems[iElm].node[i];
		FEMnode[i] = abaqus->FEMnodes[iNode];
	}
	
	Xi  = 2.0 * (pos[0] - FEMnode[0].x) / dX - 1.0;
	Eta = 2.0 * (pos[1] - FEMnode[0].y) / dY - 1.0;
	Zi  = 2.0 * (pos[2] - FEMnode[0].z) / dZ - 1.0;
	
/*
 *	Calculate the stress at the point pos given the FEM 
 *	nodal displacements or stresses
 */
#ifdef _CONTINUOUS_STRESSFIELD
	getStressFromNodeStress(abaqus, FEMnode , Xi , Eta , Zi , stress);
	//getStressFromDispHexahed(abaqus, FEMnode , Xi , Eta , Zi , stress);
#else
	getStressFromDispHexahed(abaqus, FEMnode , Xi , Eta , Zi , stress);
#endif
	
/*	printf("%d %d %d | %d | %f %f %f\n" , iElmX , iElmY , iElmZ , iElm , Xi , Eta , Zi);
	printf("%9.2e %9.2e %9.2e | %9.2e %9.2e %9.2e\n" , 
	        strain[0][0] , strain[1][1] , strain[2][2] , strain[0][1] , strain[0][2] , strain[1][2]);
	
	exit(1);*/
	
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_calc_node_stress(Home_t *home , Abaqus_t *abaqus)
{
#ifdef _CONTINUOUS_STRESSFIELD	
	int        *node2elmCount  ;
	int         i,j,k,l, iNode ;
	double      stress[3][3] , dX , Xi ;
	double      dY , Eta , dZ , Zi , x, y, z;
	FEMnode_t   FEMnode[abaqus->nodePelem]  ;
	
/*
 *	Zeroing
 */
	node2elmCount = (int *)malloc(sizeof(int)*abaqus->nNodes);
	
	for (i = 0 ; i < abaqus->nNodes ; i++) {
		node2elmCount[i] = 0;
		
		for (j = 0 ; j < 3 ; j++) {
			for (k = 0 ; k < 3 ; k++) {
				abaqus->FEMnodes[i].stress[j][k] = 0.0;
			}
		}
	}
	
/*
 *	Looping over elements
 */
	if (strcmp(home->param->abq_elemType, "hexahedral") == 0) {
		for (i = 0 ; i < abaqus->nElems ; i++) {
		
/*
 *			Pack the node numbers of this element and find the side lengths
 *			along X, Y and Z. Also count the number of times a node is 
 *			connected to elements
 */
			for (j = 0 ; j < abaqus->nodePelem ; j++) {
				iNode = abaqus->FEMelems[i].node[j];
				FEMnode[j] = abaqus->FEMnodes[iNode];
				
				node2elmCount[iNode]++;
			}
			
			dX = fabs(FEMnode[6].x - FEMnode[0].x);
			dY = fabs(FEMnode[6].y - FEMnode[0].y);
			dZ = fabs(FEMnode[6].z - FEMnode[0].z);
		
/*
 *			Loop over the nodes of this element and calculate the stress at 
 *			this node given the element nodal displacements
 */
			for (j = 0 ; j < abaqus->nodePelem ; j++) {
				iNode = abaqus->FEMelems[i].node[j];
				
				Xi  = 2.0 * (FEMnode[j].x - FEMnode[0].x) / dX - 1.0;
				Eta = 2.0 * (FEMnode[j].y - FEMnode[0].y) / dY - 1.0;
				Zi  = 2.0 * (FEMnode[j].z - FEMnode[0].z) / dZ - 1.0;
				
				getStressFromDispHexahed(abaqus, FEMnode , Xi , Eta , Zi , stress);
				
				for (k = 0 ; k < 3 ; k++) {
					for (l = 0 ; l < 3 ; l++) {
						abaqus->FEMnodes[iNode].stress[k][l] += stress[k][l];
					}
				}
			}
		}
	} else {
		for (i = 0 ; i < abaqus->nElems ; i++) {
		
/*
 *			Pack the node numbers of this element and find the side lengths
 *			along X, Y and Z. Also count the number of times a node is 
 *			connected to elements
 */
			for (j = 0 ; j < abaqus->nodePelem ; j++) {
				iNode = abaqus->FEMelems[i].node[j];
				FEMnode[j] = abaqus->FEMnodes[iNode];
				
				node2elmCount[iNode]++;
			}
		
/*
 *			Loop over the nodes of this element and calculate the stress at 
 *			this node given the element nodal displacements
 */
			for (j = 0 ; j < abaqus->nodePelem ; j++) {
				iNode = abaqus->FEMelems[i].node[j];
				
				x = FEMnode[j].x;
				y = FEMnode[j].y;
				z = FEMnode[j].z;
				getStressFromDispTetrahed(abaqus , FEMnode , 
				                          x, y, z, stress );
				
				for (k = 0 ; k < 3 ; k++) {
					for (l = 0 ; l < 3 ; l++) {
						abaqus->FEMnodes[iNode].stress[k][l] += stress[k][l];
					}
				}
			}
		}
	}
	
	for (i = 0 ; i < abaqus->nNodes ; i++) {
		for (j = 0 ; j < 3 ; j++) {
			for (k = 0 ; k < 3 ; k++) {
				abaqus->FEMnodes[i].stress[j][k] /= node2elmCount[i];
				
	//			stress[j][k] = abaqus->FEMnodes[i].stress[j][k];
			}
		}
		
	//	printf("%i %9.2e %9.2e %9.2e | %9.2e %9.2e %9.2e\n" , i,
	//        stress[0][0] , stress[1][1] , stress[2][2] , stress[0][1] , stress[0][2] , stress[1][2]);
	
	//exit(1);
	}
	
	free(node2elmCount);
#endif
}

#endif
