/***************************************************************************
 *
 *  Module      : abaqus.c
 *  Description : Calculates stress at free surfaces of the abaqus 
 * 
 **************************************************************************/


#include "Home.h"
#include <math.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef _ABAQUS

#include "ABQ.h"

/***************************************************************************
 *
 *  Module      : ABQ_Mesh_allocations_tet
 *  Description : Allocate the FEM mesh related arrays for tetrahedral 
 *                elements
 *
 ***************************************************************************/

void ABQ_Mesh_allocations_tet(Home_t *home, Abaqus_t *abaqus) 
{	
	int         i , n1, n4, n7, n10  , xFix, ListSize, nElemOnSurf;
	int         j , n2, n5, n8, k    , yFix, nNsurf;
	int         n , n3, n6, n9, nQuad, zFix, succes;
	double      x , x0, x1, minX, maxX, facX, fact1, L[6][3];
	double      y , y0, y1, minY, maxY, facY, fact2, wei [6];
	double      z , z0, z1, minZ, maxZ, facZ, fact3, epsi   ;
	char       *line = NULL , str[30];
	FILE       *fp;
	Param_t    *param;
	size_t      len = 0;
	ssize_t     lineLen;

	param = home->param;
	
/*
 *	Initializing.
 *	Open the abq2paraInput.inp file and read it till get to the 
 *	nodal information
 */
	epsi  = 1e-8;
	nQuad = abaqus->nQuadSurface;
	fp = fopen("abq2paraInput.inp", "r");
	if (fp == NULL) {
    	printf("\nError in opening file abq2paraInput.inp. This file is\n");
		printf("       required when tetrahedral elements are used.\n");
		Fatal("Error ocured in ABQ_Mesh_allocations_tet function");
    }
	
	if      (abaqus->nodePelem == 10) nNsurf = 6;
	else if (abaqus->nodePelem == 4 ) nNsurf = 3;
	else Fatal("Number of FEM nodes per element should be 10 or 4 for tetrahedral elements\n");
	
	succes = 0;
	while ((lineLen = getline(&line, &len, fp)) != -1) {
		printf("%s",line);
		succes++;
		if (succes==30) exit(0);
		if ( strcmp("*Node\n",line) == 0) {
			succes=1;
			break;
		}
	}
	
	if (succes == 0) Fatal("Error in reading abq2paraInput.inp file\n");
	
/*
 *	Read the nodal position and store them into the FEMnode data structure
 */
	i = 0;
	ListSize = 0;
	
	while (1) {
		if (i >= ListSize - 1) {
			ListSize += 10000;
			abaqus->FEMnodes = (FEMnode_t *) realloc(abaqus->FEMnodes, sizeof(FEMnode_t)*ListSize);
		}
		
		if (fscanf(fp , "%i, %lf, %lf, %lf\n" , &j , &x , &y , &z) !=4) break;
		abaqus->FEMnodes[i].x = x;
		abaqus->FEMnodes[i].y = y;
		abaqus->FEMnodes[i].z = z;
		i++;
	}
	
	if (abaqus->FEMnodes == NULL) Fatal("Not enough memory to allocate FEMnodes array (1)\n");
	abaqus->nNodes   = i;
	abaqus->FEMnodes = (FEMnode_t *) realloc(abaqus->FEMnodes, sizeof(FEMnode_t)*i);
	
/*
 *	Read the element connectivity and store them into the FEMelem data structure
 */
	i = 0;
	ListSize = 0;
	getline(&line, &len, fp);
	
	if (abaqus->nodePelem == 10) {
		
		if (strcmp("*Element, type=C3D10\n" , line) != 0)
			Fatal("Missing element information in the Abaqus abq2paraInput file\n");
		
		while (1) {
			if (i >= ListSize - 1) {
				ListSize += 10000;
				abaqus->FEMelems = (FEMelem_t *) realloc(abaqus->FEMelems, sizeof(FEMelem_t)*ListSize);
			}
			
			if (fscanf(fp , "%i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i\n" , 
			                 &j , &n1, &n2, &n3, &n4 , &n5,
			                 &n6, &n7, &n8, &n9, &n10) !=11) break;
			
			abaqus->FEMelems[i].node = (int *) malloc(sizeof(int) * 10);
			abaqus->FEMelems[i].node[0] = n1 -1;
			abaqus->FEMelems[i].node[1] = n2 -1;
			abaqus->FEMelems[i].node[2] = n3 -1;
			abaqus->FEMelems[i].node[3] = n4 -1;
			abaqus->FEMelems[i].node[4] = n5 -1;
			abaqus->FEMelems[i].node[5] = n6 -1;
			abaqus->FEMelems[i].node[6] = n7 -1;
			abaqus->FEMelems[i].node[7] = n8 -1;
			abaqus->FEMelems[i].node[8] = n9 -1;
			abaqus->FEMelems[i].node[9] = n10-1;
			i++;
		}
		
	} else {
		
		if (strcmp("*Element, type=C3D4\n"  , line) != 0)
			Fatal("Missing element information in the Abaqus abq2paraInput file\n");
		
		while (1) {
			if (i >= ListSize - 1) {
				ListSize += 10000;
				abaqus->FEMelems = (FEMelem_t *) realloc(abaqus->FEMelems, sizeof(FEMelem_t)*ListSize);
			}
			
			if (fscanf(fp , "%i, %i, %i, %i, %i\n" , &j , &n1, &n2, &n3, &n4) !=5) break;
			
			abaqus->FEMelems[i].node = (int *) malloc(sizeof(int) * 4);
			abaqus->FEMelems[i].node[0] = n1-1;
			abaqus->FEMelems[i].node[1] = n2-1;
			abaqus->FEMelems[i].node[2] = n3-1;
			abaqus->FEMelems[i].node[3] = n4-1;
			i++;
		}
	}
	
	if (abaqus->FEMelems == NULL) Fatal("Not enough memory to allocate FEMelems array (2)\n");
	abaqus->nElems   = i;
	abaqus->FEMelems = (FEMelem_t *) realloc(abaqus->FEMelems, sizeof(FEMelem_t)*i);
	
	printf("nNodes = %d\n" , abaqus->nNodes);
	printf("nElems = %d\n" , abaqus->nElems);
	
/*
 *	Close the abq2paraInput.inp file
 */
	if (fclose(fp) != 0) Fatal("error closing file abq2paraInput file\n");
	
/*
 *	Scale the FEM coordinates to match with ParaDiS
 *	Note: minX, maxX, etc. are the coordinate upper and lower bounds given in abq2paraInput
 *	      x0, x1, etc.  are the limits of the ParaDiS input files
 */
	x0 = param->minSideX;   x1 = param->maxSideX;
	y0 = param->minSideY;   y1 = param->maxSideY;
	if (strcmp(param->tf_or_hs, "hs") == 0) {
		z0 = param->minSideZ;
		z1 = 0.0;
	} else if (strcmp(param->tf_or_hs, "tf") == 0) {
		z0 = -abaqus->t;
		z1 = abaqus->t;
	}
	
	minX = 1e9 ;  minY = 1e9 ;  minZ = 1e9 ;
	maxX =-1e9 ;  maxY =-1e9 ;  maxZ =-1e9 ;
	
	for (i = 0 ; i < abaqus->nNodes ; i++) {
		minX = MIN(minX , abaqus->FEMnodes[i].x);
		maxX = MAX(maxX , abaqus->FEMnodes[i].x);
		minY = MIN(minY , abaqus->FEMnodes[i].y);
		maxY = MAX(maxY , abaqus->FEMnodes[i].y);
		minZ = MIN(minZ , abaqus->FEMnodes[i].z);
		maxZ = MAX(maxZ , abaqus->FEMnodes[i].z);
	}
	
	facX = (x1-x0) / (maxX-minX);
	facY = (y1-y0) / (maxY-minY);
	facZ = (z1-z0) / (maxZ-minZ);
	
	for (i = 0 ; i < abaqus->nNodes ; i++) {
		abaqus->FEMnodes[i].x = x0 + facX * (abaqus->FEMnodes[i].x-minX);
		abaqus->FEMnodes[i].y = y0 + facY * (abaqus->FEMnodes[i].y-minY);
		abaqus->FEMnodes[i].z = z0 + facZ * (abaqus->FEMnodes[i].z-minZ);
	}
	
/*
 *	Loop over elements and identify surface elements and the nodes 
 *	which are on the free surface
 */
	nElemOnSurf = 0;
	
	for (i = 0 ; i < abaqus->nElems ; i++) {
		k = 0;
		
		for (j = 0 ; j < abaqus->nodePelem ; j++) {
			n = abaqus->FEMelems[i].node[j];
			z = abaqus->FEMnodes[n].z;
			
			if ((fabs(z-z0) < epsi && strcmp(param->tf_or_hs, "tf") == 0) ||
				(fabs(z-z1) < epsi )) {
				
				if (k == 0)
				abaqus->FEMelems[i].surfNodes = (int *)malloc(sizeof(int) * nNsurf);
				abaqus->FEMelems[i].surfNodes[k] = n;
				k++;
			}
		}
		
		abaqus->FEMelems[i].onSurf = k;
		if (k == nNsurf ) nElemOnSurf++;
		if (k >  nNsurf ) Fatal("Error in finding the surface elements\n");
		
/*
 *		For quadratic tetrahedral elements the order of the nodes on the  
 *		surfaces (which share a node with the the 4th node of the tetrahedron)
 *		are not the same as order of the nodes of a quadratic triangular element.
 *		So, here we change the node ordering.
 */
	/*	if (nNsurf == 6 && k == 6 && 
			abaqus->FEMelems[i].surfNodes[2] == abaqus->FEMelems[i].node[3]) {
			
			n = abaqus->FEMelems[i].surfNodes[5];
			abaqus->FEMelems[i].surfNodes[5] = abaqus->FEMelems[i].surfNodes[4];
			abaqus->FEMelems[i].surfNodes[4] = n;
		}*/
	}
	
/*
 *	Assign the displacement boundary condition to nodes
 */
	for (i = 0 ; i < abaqus->nNodes ; i++) {
		x = abaqus->FEMnodes[i].x;
		y = abaqus->FEMnodes[i].y;
		z = abaqus->FEMnodes[i].z;
		
		xFix = 0;
		yFix = 0;
		zFix = 0;
		
		if (fabs(x-x0) < epsi || fabs(x-x1) < epsi) xFix = 1;
		if (fabs(y-y0) < epsi || fabs(y-y1) < epsi) yFix = 1;
		if (fabs(z-z0) < epsi && strcmp(param->tf_or_hs, "hs") == 0) 
			zFix = 1;
                //xiaohan adds this
                if (fabs(z-z0) < epsi && strcmp(param->tf_or_hs, "tf") == 0)
	                zFix = 1;
		
		abaqus->FEMnodes[i].xFix = xFix;
		abaqus->FEMnodes[i].yFix = yFix;
		abaqus->FEMnodes[i].zFix = zFix;
	}
	
/*
 *	Allocate the free surface quadrature point arrays
 */
	abaqus->nQuads   = nElemOnSurf*nQuad;
	abaqus->FEMquads = (FEMquad_t *)malloc(sizeof(FEMquad_t) * abaqus->nQuads);
	
	if (abaqus->FEMquads == NULL)
		Fatal("Not enough memory to allocate FEMquads arrays (1)\n");
	
	for (i = 0 ; i < abaqus->nQuads ; i++) {
		abaqus->FEMquads[i].node = (int *)malloc(nNsurf * sizeof(int   ));
		abaqus->FEMquads[i].shFn = (double *)malloc(nNsurf * sizeof(double));
	
		if (abaqus->FEMquads[i].node == NULL || abaqus->FEMquads[i].shFn == NULL )
			Fatal("Not enough memory to allocate FEMquads arrays (2)\n");
	}
	
/*
 *	Create the quadrature points on the free surface.
 *	These quadrature points are used for nodal image force calculation.
 *	First, do some initialization
 */	
	if (nQuad == 1) {
		L[0][0] = 1.0/3.0;
		L[0][1] = 1.0/3.0;
		L[0][2] = 1.0/3.0;
		wei [0] = 1.0;
		
	} else if (nQuad == 3) {
		fact1 = 1.0/6.0;
		fact2 = 2.0/3.0;
		fact3 = 1.0/3.0;
		L[0][0] = fact2;   L[1][0] = fact1;   L[2][0] = fact1;
		L[0][1] = fact1;   L[1][1] = fact2;   L[2][1] = fact1;
		L[0][2] = fact1;   L[1][2] = fact1;   L[2][2] = fact2;
		wei [0] = fact3;   wei [1] = fact3;   wei [2] = fact3;
		
	} else if (nQuad == 4) {
		fact1 = 1.0 / 3.0;
		fact2 =-9.0 /16.0;
		fact3 = 25.0/48.0;
		L[0][0] = fact1;   L[1][0] = 0.6  ;   L[2][0] = 0.2  ;   L[3][0] = 0.2  ;
		L[0][1] = fact1;   L[1][1] = 0.2  ;   L[2][1] = 0.6  ;   L[3][1] = 0.2  ;
		L[0][2] = fact1;   L[1][2] = 0.2  ;   L[2][2] = 0.2  ;   L[3][2] = 0.6  ;
		wei [0] = fact2;   wei [1] = fact3;   wei [2] = fact3;   wei [3] = fact3;
		
	} else if (nQuad == 6) {
		fact1 = 0.816847572980459;
		fact2 = 0.091576213509771;
		fact3 = 0.109951743655322;
		L[0][0] = fact1;   L[1][0] = fact2;   L[2][0] = fact2;
		L[0][1] = fact2;   L[1][1] = fact1;   L[2][1] = fact2;
		L[0][2] = fact2;   L[1][2] = fact2;   L[2][2] = fact1;
		wei [0] = fact3;   wei [1] = fact3;   wei [2] = fact3;
		
		fact1 = 0.108103018168070;
		fact2 = 0.445948490915965;
		fact3 = 0.223381589678011;
		L[3][0] = fact1;   L[4][0] = fact2;   L[5][0] = fact2;
		L[3][1] = fact2;   L[4][1] = fact1;   L[5][1] = fact2;
		L[3][2] = fact2;   L[4][2] = fact2;   L[5][2] = fact1;
		wei [3] = fact3;   wei [4] = fact3;   wei [5] = fact3;
	} else {
		Fatal("nQuadSurface should be either 1,3,4 or 6 when tetrahedral elements are being used\n");
	}
	
/*
 *	Now loop over surface elements and make the surface quadratures.
 */
	int iQuad = 0;
	
	for (i = 0 ; i < abaqus->nElems ; i++) {
		if (abaqus->FEMelems[i].onSurf != nNsurf) continue;
		
		for (j = 0 ; j < nQuad ; j++) {
			x = 0.0;
			y = 0.0;
			z = 0.0;
			
			for (k = 0 ; k < nNsurf ; k++) {
				n = abaqus->FEMelems[i].surfNodes[k];
				abaqus->FEMquads[iQuad].node[k] = n;
				
				if (k < 3) {					
					x += abaqus->FEMnodes[n].x * L[j][k];
					y += abaqus->FEMnodes[n].y * L[j][k];
					z += abaqus->FEMnodes[n].z * L[j][k];
				}
			}
			
			abaqus->FEMquads[iQuad].x      = x;
			abaqus->FEMquads[iQuad].y      = y;
			abaqus->FEMquads[iQuad].z      = z;
			abaqus->FEMquads[iQuad].weight = wei[j];
			
			if (nNsurf == 3) {
				abaqus->FEMquads[iQuad].shFn[0] = L[j][0];
				abaqus->FEMquads[iQuad].shFn[1] = L[j][1];
				abaqus->FEMquads[iQuad].shFn[2] = L[j][2];
			} else {
				abaqus->FEMquads[iQuad].shFn[0] = L[j][0]*(2.0*L[j][0]-1.0);
				abaqus->FEMquads[iQuad].shFn[1] = L[j][1]*(2.0*L[j][1]-1.0);
				abaqus->FEMquads[iQuad].shFn[2] = L[j][2]*(2.0*L[j][2]-1.0);
				abaqus->FEMquads[iQuad].shFn[3] = L[j][0]*L[j][1]*4.0;
				abaqus->FEMquads[iQuad].shFn[4] = L[j][1]*L[j][2]*4.0;
				abaqus->FEMquads[iQuad].shFn[5] = L[j][2]*L[j][0]*4.0;
			}
			iQuad++;
		}
	}
	
/*
 *	Allocating the arrays for making PBC in Abaqus
 */
	abaqus->nNsurX = 0;
	abaqus->nNsurY = 0;
	abaqus->nNedge = 0;
	
	if (abaqus->pbcXY) {
		for (i = 0 ; i < abaqus->nNodes ; i++) {
			x = abaqus->FEMnodes[i].x;
			y = abaqus->FEMnodes[i].y;
			z = abaqus->FEMnodes[i].z;
			
			if (fabs(x-x0) < epsi || fabs(x-x1) < epsi ) {
				if (fabs(y-y0) < epsi || fabs(y-y1) < epsi ) {
					abaqus->nNedge++;
				} else {
					abaqus->nNsurX++;
				}
			} else if (fabs(y-y0) < epsi || fabs(y-y1) < epsi ) {
				abaqus->nNsurY++;
			}
		}
		
		abaqus->nNsurX /= 2;
		abaqus->nNsurY /= 2;
		abaqus->nNedge /= 4;
	
		abaqus->nSetXmin     = (int *) malloc(sizeof(int) * abaqus->nNsurX);
		abaqus->nSetXmax     = (int *) malloc(sizeof(int) * abaqus->nNsurX);
		abaqus->nSetYmin     = (int *) malloc(sizeof(int) * abaqus->nNsurY);
		abaqus->nSetYmax     = (int *) malloc(sizeof(int) * abaqus->nNsurY);
		abaqus->nSetXminYmin = (int *) malloc(sizeof(int) * abaqus->nNedge);
		abaqus->nSetXminYmax = (int *) malloc(sizeof(int) * abaqus->nNedge);
		abaqus->nSetXmaxYmax = (int *) malloc(sizeof(int) * abaqus->nNedge);
		abaqus->nSetXmaxYmin = (int *) malloc(sizeof(int) * abaqus->nNedge);
	
		if (abaqus->nSetXmin     == NULL ||  abaqus->nSetXmax     == NULL ||
			abaqus->nSetYmin     == NULL ||  abaqus->nSetYmax     == NULL ||
			abaqus->nSetXminYmin == NULL ||  abaqus->nSetXminYmax == NULL ||
			abaqus->nSetXmaxYmin == NULL ||  abaqus->nSetXmaxYmax == NULL ) {
			Fatal("Not enough memory to allocate FEM related arrays (4)\n");
		}
	}

	if (home->myDomain == 0) printf("All FEM arrays allocated\n\n");
}

/***************************************************************************
 *
 *  Module      : ABQ_Mesh_allocations_hex
 *  Description : Allocate the FEM mesh related arrays for hexahedral 
 *                elements
 *
 ***************************************************************************/

void ABQ_Mesh_allocations_hex(Home_t *home, Abaqus_t *abaqus) 
{	
	int    i;
	int NXMAX = abaqus->nx;
	int NYMAX = abaqus->ny;
	int NZMAX = abaqus->nz;
	
/*
 *	Allocating the FEM nodes and elements
 */
	int  nQuad = abaqus->nQuadSurface;
	int  nNsurf;
	
	if (abaqus->nodePelem == 20) {
		abaqus->nNodes = 4*(NXMAX*NYMAX*NZMAX) + 3*(NXMAX*NYMAX + NXMAX*NZMAX + NYMAX*NZMAX)
					   + 2*(NXMAX+NYMAX+NZMAX) + 1;
		nNsurf = 8;
	} else if (abaqus->nodePelem == 8) {
		abaqus->nNodes = (NXMAX+1) * (NYMAX+1) * (NZMAX+1);
		nNsurf = 4;
	} else {
		Fatal("Number of FEM nodes per element should be 8 or 20\n");
	}
	
//	abaqus->nQuads = 2*(NXMAX*NYMAX+NYMAX*NZMAX+NXMAX*NZMAX)*(nQuad*nQuad);
	abaqus->nQuads   = NXMAX*NYMAX*(nQuad*nQuad);
	abaqus->nElems   = NXMAX*NYMAX*NZMAX;
	
	abaqus->FEMelems = (FEMelem_t *) malloc(sizeof(FEMelem_t) * abaqus->nElems);
	abaqus->FEMnodes = (FEMnode_t *) malloc(sizeof(FEMnode_t) * abaqus->nNodes);
	abaqus->FEMquads = (FEMquad_t *) malloc(sizeof(FEMquad_t) * abaqus->nQuads);
	
	if (abaqus->FEMnodes == NULL  ||  abaqus->FEMelems == NULL  ||  
	    abaqus->FEMquads == NULL  ) {
		Fatal("Not enough memory to allocate FEM related arrays (1)\n");
	}
	
	for (i = 0 ; i < abaqus->nElems ; i++) {
		abaqus->FEMelems[i].node = (int *) malloc(sizeof(int) * abaqus->nodePelem);
	
		if (abaqus->FEMelems[i].node == NULL )
			Fatal("Not enough memory to allocate FEM related arrays (2)\n");
	}
	
	for (i = 0 ; i < abaqus->nQuads ; i++) {
		abaqus->FEMquads[i].node = (int    *) malloc(nNsurf * sizeof(int   ));
		abaqus->FEMquads[i].shFn = (double *) malloc(nNsurf * sizeof(double));
	
		if (abaqus->FEMquads[i].node == NULL || abaqus->FEMquads[i].shFn == NULL )
			Fatal("Not enough memory to allocate FEM related arrays (3)\n");
	}
	
/*
 *	Allocating the arrays for making PBC in abaqus
 */
	if (abaqus->pbcXY) {
		if (abaqus->nodePelem == 20) {
			abaqus->nNsurX = 3*NXMAX*NZMAX + 2*NZMAX - 4*NZMAX;              //  #nodes an the surface excluding BC nodes
			abaqus->nNsurY = 3*NYMAX*NZMAX + 2*NZMAX - 4*NZMAX;
		//	abaqus->nNsurX = 3*NXMAX*NZMAX + 2*(NXMAX+NZMAX) + 1 - 4*NZMAX;  //  Considering all nodes on the surface
		//	abaqus->nNsurY = 3*NYMAX*NZMAX + 2*(NYMAX+NZMAX) + 1 - 4*NZMAX;
			abaqus->nNedge = 2*NZMAX;                                        //  #nodes on the edges parallel to z axis
		} else if (abaqus->nodePelem == 8) {
			abaqus->nNsurX = NXMAX*NZMAX + NZMAX - 2*NZMAX;                  //  #nodes an the surface excluding BC nodes
			abaqus->nNsurY = NYMAX*NZMAX + NZMAX - 2*NZMAX;
			abaqus->nNedge = NZMAX;                                          //  #nodes on the edges parallel to z axis
		}
	
		abaqus->nSetXmin     = (int *) malloc(sizeof(int) * abaqus->nNsurX);
		abaqus->nSetXmax     = (int *) malloc(sizeof(int) * abaqus->nNsurX);
		abaqus->nSetYmin     = (int *) malloc(sizeof(int) * abaqus->nNsurY);
		abaqus->nSetYmax     = (int *) malloc(sizeof(int) * abaqus->nNsurY);
		abaqus->nSetXminYmin = (int *) malloc(sizeof(int) * abaqus->nNedge);
		abaqus->nSetXminYmax = (int *) malloc(sizeof(int) * abaqus->nNedge);
		abaqus->nSetXmaxYmax = (int *) malloc(sizeof(int) * abaqus->nNedge);
		abaqus->nSetXmaxYmin = (int *) malloc(sizeof(int) * abaqus->nNedge);
	
		if (abaqus->nSetXmin     == NULL ||  abaqus->nSetXmax     == NULL ||
			abaqus->nSetYmin     == NULL ||  abaqus->nSetYmax     == NULL ||
			abaqus->nSetXminYmin == NULL ||  abaqus->nSetXminYmax == NULL ||
			abaqus->nSetXmaxYmin == NULL ||  abaqus->nSetXmaxYmax == NULL ) {
			Fatal("Not enough memory to allocate FEM related arrays (4)\n");
		}
	}

	if (home->myDomain == 0) 
		printf("All arrays allocated NXMAX = %d, NYMAX= %d, NZMAX= %d\n\n\n",NXMAX,NYMAX,NZMAX);
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_allocations(Home_t *home, Abaqus_t *abaqus)
{

	int NXMAX = abaqus->nx;
	int NYMAX = abaqus->ny;
	int NZMAX = abaqus->nz;

	int i,j,k;

	abaqus->kx = (double *)malloc(sizeof(double)*NXMAX);
	if (abaqus->kx == NULL) {
		printf("Not enough memory to allocate kx\n");
		exit(0);
	}

	abaqus->ky = (double *)malloc(sizeof(double)*NYMAX);
	if (abaqus->ky == NULL) {
		printf("Not enough memory to allocate ky\n");
		exit(0);
	}  

	abaqus->Txp = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
	if (abaqus->Txp == NULL) {
		printf("Not enough memory to allocate Tx\n");
		exit(0);
	}

	abaqus->Typ = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
	if (abaqus->Typ == NULL) {
		printf("Not enough memory to allocate Ty\n");
		exit(0);
	}

	abaqus->Tzp = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
	if (abaqus->Tzp == NULL) {
		printf("Not enough memory to allocate Tz\n");
		exit(0);
	}

	abaqus->Txm = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
	if (abaqus->Txm == NULL) {
		printf("Not enough memory to allocate Tx\n");
		exit(0);
	}
	
	abaqus->Tym = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
	if (abaqus->Tym == NULL) {
		printf("Not enough memory to allocate Ty\n");
		exit(0);
	}
	
	abaqus->Tzm = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
	if (abaqus->Tzm == NULL) {
		printf("Not enough memory to allocate Tz\n");
		exit(0);
	}

	abaqus->A = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
	if (abaqus->A == NULL) {
		printf("Not enough memory to allocate A\n");
		exit(0);
	}

	abaqus->B = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
	if (abaqus->B == NULL) {
		printf("Not enough memory to allocate B\n");
		exit(0);
	}

	abaqus->C = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
	if (abaqus->C == NULL) {
		printf("Not enough memory to allocate C\n");
		exit(0);
	}  

	abaqus->E = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
	if (abaqus->E == NULL) {
		printf("Not enough memory to allocate E\n");
		exit(0);
	}

	abaqus->F = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
	if (abaqus->F == NULL) {
		printf("Not enough memory to allocate F\n");
		exit(0);
	}

	abaqus->G = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
	if (abaqus->G == NULL) {
		printf("Not enough memory to allocate G\n");
		exit(0);
	}


	for (i=0;i<NXMAX;i++) {
		abaqus->A[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
		abaqus->B[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
		abaqus->C[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);

		abaqus->E[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
		abaqus->F[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
		abaqus->G[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);

		if (abaqus->A[i] == NULL || abaqus->B[i] == NULL ||  
		    abaqus->C[i] == NULL || abaqus->E[i] == NULL || 
		    abaqus->F[i] == NULL || abaqus->G[i] == NULL ) {
			printf("Not enough memory to allocate ABCEFG\n");
			exit(0);
		} 
	}

	for (i=0;i<3;i++) {
		for (j=0;j<3;j++) {
			abaqus->MsInv[i][j] = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);      
			abaqus->MaInv[i][j] = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);      
			abaqus->Stress[i][j] = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);

			if (abaqus->MsInv[i][j] == NULL || 
			    abaqus->MaInv[i][j] == NULL || 
			    abaqus->Stress[i][j] == NULL) {
				printf("Not enough memory to allocate MsInv or MaInv or Stress array\n");
				exit(0);
			}
		}
	}

	for (i=0;i<3;i++) {
		for (j=0;j<3;j++) {
			for (k=0;k<NXMAX;k++) {
				abaqus->MsInv[i][j][k] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
				abaqus->MaInv[i][j][k] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
				abaqus->Stress[i][j][k] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);

				if (abaqus->MsInv[i][j][k] == NULL || 
					abaqus->MaInv[i][j][k] == NULL ||
					abaqus->Stress[i][j][k] == NULL) {
					printf("Not enough memory to allocate MsInv or MaInv or Stress array\n");
					exit(0);
				}  
			}
		}
	}
	
	if (strcmp(home->param->abq_elemType, "tetrahedral") == 0) {
		ABQ_Mesh_allocations_tet(home, abaqus);
	} else {
		ABQ_Mesh_allocations_hex(home, abaqus);
	}

}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_Create_Matrices(Abaqus_t *abaqus)
{
	//Minvmatrix(abaqus);
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_calc_nodeF(Home_t *home, Abaqus_t *abaqus)
{
	int     nNsurf , n , i , j;
	double  shFn , x[4] , vec1[3] , tracX , burg2;
	double  weig , y[4] , vec2[3] , tracY , coeff;
	double  area , z[4] , vec3[3] , tracZ;
	
/*
 *	Initialization and Zeroing the external forces
 */
	for (i = 0 ; i < abaqus->nNodes ; i++) {
		abaqus->FEMnodes[i].Fx = 0.0;
		abaqus->FEMnodes[i].Fy = 0.0;
		abaqus->FEMnodes[i].Fz = 0.0;
	}
	
	burg2 = home->param->burgMag * home->param->burgMag;
	//xiaohan force abq scale
	//burg2 = 1;

	if      (abaqus->nodePelem == 20) nNsurf = 8;  // Hexahedral quadratic elements
	else if (abaqus->nodePelem == 8 ) nNsurf = 4;  // Hexahedral linear elements
	else if (abaqus->nodePelem == 10) nNsurf = 6;  // Tetrahedral quadratic elements
	else if (abaqus->nodePelem == 4 ) nNsurf = 3;  // Tetrahedral linear elements
	
/*
 *	Looping over surface quadratures and adding their contribution 
 *	to the nodal forces
 */
	for (i = 0 ; i < abaqus->nQuads ; i++) {
		
/*
 *		First calculate the area of the surface element
 *		corresponding to this quadrature
 */
		if (strcmp(home->param->abq_elemType, "hexahedral") == 0) {
			for (j = 0 ; j < 4 ; j++) {
				n    = abaqus->FEMquads[i].node[j];
				x[j] = abaqus->FEMnodes[n].x;
				y[j] = abaqus->FEMnodes[n].y;
				z[j] = abaqus->FEMnodes[n].z;
			}
			
			vec1[0] = x[2] - x[0];
			vec1[1] = y[2] - y[0];
			vec1[2] = z[2] - z[0];
			
			vec2[0] = x[3] - x[1];
			vec2[1] = y[3] - y[1];
			vec2[2] = z[3] - z[1];
			
			coeff = 0.25;
		} else {
			for (j = 0 ; j < 3 ; j++) {
				n    = abaqus->FEMquads[i].node[j];
				x[j] = abaqus->FEMnodes[n].x;
				y[j] = abaqus->FEMnodes[n].y;
				z[j] = abaqus->FEMnodes[n].z;
			}
			
			vec1[0] = x[2] - x[0];
			vec1[1] = y[2] - y[0];
			vec1[2] = z[2] - z[0];
			
			vec2[0] = x[2] - x[1];
			vec2[1] = y[2] - y[1];
			vec2[2] = z[2] - z[1];
			
			coeff = 1.0;
		}
			
		cross(vec1 , vec2 , vec3);
		area  = 0.5 * sqrt(vec3[0]*vec3[0] + vec3[1]*vec3[1] + vec3[2]*vec3[2]);
		area *= burg2;
		
/*
 *		Now loop over all nodes surrounding this quadrature and add
 *		its contribution to the nodes
 */
		weig  =  coeff * area * abaqus->FEMquads[i].weight;
		tracX = -abaqus->FEMquads[i].Tx;
		tracY = -abaqus->FEMquads[i].Ty;
		tracZ = -abaqus->FEMquads[i].Tz;
		
	//	tracX = 0.0;
	//	tracY = 0.0;
	//	tracZ = -2.0e-5;
		
		for (j = 0 ; j < nNsurf ; j++) {
			n    = abaqus->FEMquads[i].node[j];
			shFn = abaqus->FEMquads[i].shFn[j];
			
			abaqus->FEMnodes[n].Fx += weig * tracX * shFn;
			abaqus->FEMnodes[n].Fy += weig * tracY * shFn;
			abaqus->FEMnodes[n].Fz += weig * tracZ * shFn;
		}
	}
	
/*	real8   sum=0.0;
	for (i = 0 ; i < abaqus->nNodes ; i++) {
		if (abs(abaqus->FEMnodes[i].Fz) > 1e-6) {
			sum+=abaqus->FEMnodes[i].Fz;
			printf("%d %f %f %f %e\n" , i , abaqus->FEMnodes[i].x ,
			                                abaqus->FEMnodes[i].y ,
											abaqus->FEMnodes[i].z ,
											abaqus->FEMnodes[i].Fz);
		}
	}
	printf("\nsum = %e\n" , sum);
	printf("abaqus->nQuadSurface=%d\n" , abaqus->nQuadSurface);
	exit(0);*/
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_writeInput(Home_t *home, Abaqus_t *abaqus)
{
		int      i , j , count;
		real8    b ;
		FILE    *fp;
		Param_t *param;
	
		param = home->param;
	
/*
 *		First delete all files in the ABQ directory.
 *		Then open a new file for writing.
 */
		if (mkdir("ABQ", S_IRWXU) != 0) {
			if (errno == EEXIST)
				system("rm -r ABQ/*");
			else
				Fatal("Open error %d on directory ABQ\n", errno);
		}
		
		fp = fopen("ABQ/abqJob.inp", "w");
		b  = param->burgMag;
		//xiaohan force abq scale
		//b = 1;
	
/*
 *		Writing the required headers
 */
		fprintf(fp , "*Heading\n");
		fprintf(fp , "Abaqus input file written by ParaDiS\n");
		fprintf(fp , "** Job name: Job-1 Model name: Model-1\n");
		fprintf(fp , "*Preprint, echo=NO, model=NO, history=NO, contact=NO\n");
		fprintf(fp , "**\n");
		fprintf(fp , "** PARTS\n");
		fprintf(fp , "**\n");
	//	fprintf(fp , "*Part, name=Part-1\n");
		
/*
 *		Writing the nodes and their positions
 */
		fprintf(fp , "*Node, Nset=NAll\n");
		for (i = 0 ; i < abaqus->nNodes ; i++) {
			fprintf(fp , "%7d,%20.12e,%20.12e,%20.12e\n" , i+1 , b*abaqus->FEMnodes[i].x ,
			                           b*abaqus->FEMnodes[i].y , b*abaqus->FEMnodes[i].z);
		}
		
/*
 *		Writing the elements and their connectivity
 */
		if      (abaqus->nodePelem == 20) 
			fprintf(fp , "*Element, type=C3D20R, Elset=ElAll\n");  // Hexahedral quadratic elements
		else if (abaqus->nodePelem == 8 ) 
			fprintf(fp , "*Element, type=C3D8, Elset=ElAll\n");    // Hexahedral linear elements
		else if (abaqus->nodePelem == 10) 
			fprintf(fp , "*Element, type=C3D10, Elset=ElAll\n");   // Tetrahedral quadratic elements
		else if (abaqus->nodePelem == 4 ) 
			fprintf(fp , "*Element, type=C3D4, Elset=ElAll\n");    // Tetrahedral linear elements
		
		for (i = 0 ; i < abaqus->nElems ; i++) {
			fprintf(fp , "%7d" , i+1);
			
			for (j = 0 ; j < abaqus->nodePelem ; j++) {
				if (j == 14)
					fprintf(fp , ",%7d,\n" , abaqus->FEMelems[i].node[j] + 1);
				else if (j == 15)
					fprintf(fp ,  "%7d"    , abaqus->FEMelems[i].node[j] + 1);
				else
					fprintf(fp , ",%7d"    , abaqus->FEMelems[i].node[j] + 1);
				
			}
			fprintf(fp , "\n");
		}
		
/*
 *		Creating the node sets for applying PBC in X and Y directions
 *		First make the sets for the surface nodes (not on the edges)
 */
		if (abaqus->pbcXY) {
			fprintf(fp , "*Nset, nset=Set-Xmin\n");
			count = 0;
			for (i = 0 ; i < abaqus->nNsurX ; i++) {
				count++;
				if (count%16 == 0) fprintf(fp , "%7d\n" , abaqus->nSetXmin[i]+1);
				else               fprintf(fp , "%7d,"  , abaqus->nSetXmin[i]+1);
			}
			if (count%16 != 0) fprintf(fp , "\n" );
			
			fprintf(fp , "*Nset, nset=Set-Xmax\n");
			count = 0;
			for (i = 0 ; i < abaqus->nNsurX ; i++) {
				count++;
				if (count%16 == 0) fprintf(fp , "%7d\n" , abaqus->nSetXmax[i]+1);
				else               fprintf(fp , "%7d,"  , abaqus->nSetXmax[i]+1);
			}
			if (count%16 != 0) fprintf(fp , "\n" );

			fprintf(fp , "*Nset, nset=Set-Ymin\n");
			count = 0;
			for (i = 0 ; i < abaqus->nNsurY ; i++) {
				count++;
				if (count%16 == 0) fprintf(fp , "%7d\n" , abaqus->nSetYmin[i]+1);
				else               fprintf(fp , "%7d,"  , abaqus->nSetYmin[i]+1);
			}
			if (count%16 != 0) fprintf(fp , "\n" );
			
			fprintf(fp , "*Nset, nset=Set-Ymax\n");
			count = 0;
			for (i = 0 ; i < abaqus->nNsurY ; i++) {
				count++;
				if (count%16 == 0) fprintf(fp , "%7d\n" , abaqus->nSetYmax[i]+1);
				else               fprintf(fp , "%7d,"  , abaqus->nSetYmax[i]+1);
			}
			if (count%16 != 0) fprintf(fp , "\n" );
			
/*
 *			Now make the sets for nodes on the edges
 */
			fprintf(fp , "*Nset, nset=Set-XminYmin\n");
			count = 0;
			for (i = 0 ; i < abaqus->nNedge ; i++) {
				count++;
				if (count%16 == 0) fprintf(fp , "%7d\n" , abaqus->nSetXminYmin[i]+1);
				else               fprintf(fp , "%7d,"  , abaqus->nSetXminYmin[i]+1);
			}
			if (count%16 != 0) fprintf(fp , "\n" );
			
			fprintf(fp , "*Nset, nset=Set-XminYmax\n");
			count = 0;
			for (i = 0 ; i < abaqus->nNedge ; i++) {
				count++;
				if (count%16 == 0) fprintf(fp , "%7d\n" , abaqus->nSetXminYmax[i]+1);
				else               fprintf(fp , "%7d,"  , abaqus->nSetXminYmax[i]+1);
			}
			if (count%16 != 0) fprintf(fp , "\n" );
			
			fprintf(fp , "*Nset, nset=Set-XmaxYmin\n");
			count = 0;
			for (i = 0 ; i < abaqus->nNedge ; i++) {
				count++;
				if (count%16 == 0) fprintf(fp , "%7d\n" , abaqus->nSetXmaxYmin[i]+1);
				else               fprintf(fp , "%7d,"  , abaqus->nSetXmaxYmin[i]+1);
			}
			if (count%16 != 0) fprintf(fp , "\n" );
			
			fprintf(fp , "*Nset, nset=Set-XmaxYmax\n");
			count = 0;
			for (i = 0 ; i < abaqus->nNedge ; i++) {
				count++;
				if (count%16 == 0) fprintf(fp , "%7d\n" , abaqus->nSetXmaxYmax[i]+1);
				else               fprintf(fp , "%7d,"  , abaqus->nSetXmaxYmax[i]+1);
			}
			if (count%16 != 0) fprintf(fp , "\n" );
		}
		
/*
 *		Assigning all nodes and elements to the "ElAll" set.
 *		Assigning section to the part.
 */
		fprintf(fp , "**\n");
		fprintf(fp , "** Section: Section-1\n");
		fprintf(fp , "*Solid Section, elset=ElAll, material=Material-1\n");
		fprintf(fp , ",\n");
		fprintf(fp , "**\n");
		
/*
 *		Defining PBC constraints using equation keyword
 */
		if (abaqus->pbcXY) {

/*
 *			Surface nodes
 */
			fprintf(fp , "**\n");
			fprintf(fp , "** PBC Constraints\n");
			fprintf(fp , "**\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-Xmin, 1, 1.0, Set-Xmax, 1, -1.0\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-Xmin, 2, 1.0, Set-Xmax, 2, -1.0\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-Xmin, 3, 1.0, Set-Xmax, 3, -1.0\n");
			
			fprintf(fp , "**\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-Ymin, 1, 1.0, Set-Ymax, 1, -1.0\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-Ymin, 2, 1.0, Set-Ymax, 2, -1.0\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-Ymin, 3, 1.0, Set-Ymax, 3, -1.0\n");
			
/*
 *			Edge nodes
 */			
			fprintf(fp , "**\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-XminYmin, 1, 1.0, Set-XmaxYmin, 1, -1.0\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-XminYmin, 3, 1.0, Set-XmaxYmin, 3, -1.0\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-XminYmax, 1, 1.0, Set-XmaxYmax, 1, -1.0\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-XminYmax, 3, 1.0, Set-XmaxYmax, 3, -1.0\n");
			
			fprintf(fp , "**\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-XminYmin, 2, 1.0, Set-XminYmax, 2, -1.0\n");
		//	fprintf(fp , "*Equation\n");
		//	fprintf(fp , "2\n");
		//	fprintf(fp , "Set-XminYmin, 3, 1.0, Set-XminYmax, 3, -1.0\n");
			fprintf(fp , "*Equation\n");
			fprintf(fp , "2\n");
			fprintf(fp , "Set-XmaxYmin, 2, 1.0, Set-XmaxYmax, 2, -1.0\n");
		//	fprintf(fp , "*Equation\n");
		//	fprintf(fp , "2\n");
		//	fprintf(fp , "Set-XmaxYmin, 3, 1.0, Set-XmaxYmax, 3, -1.0\n");
		}
		
/*
 *		Defining the material properties
 */
		fprintf(fp , "**\n");
		fprintf(fp , "** MATERIALS\n");
		fprintf(fp , "**\n");
		fprintf(fp , "*Material, name=Material-1\n");
		fprintf(fp , "*Elastic\n");
		fprintf(fp , " %e, %f\n" , param->shearModulus , param->pois);
		fprintf(fp , "**\n");
		fprintf(fp , "** ----------------------------------------------------------------\n");
		
/*
 *		Defining the loading step
 */		
		fprintf(fp , "**\n");
		fprintf(fp , "** STEP: LoadStep\n");
		fprintf(fp , "**\n");
		fprintf(fp , "*Step, name=LoadStep\n");
		fprintf(fp , "*Static\n");
		fprintf(fp , "1.0, 1.0, 1e-5, 1.0\n");
		fprintf(fp , "**\n");
		
/*
 *		Defining the boundary condition
 */		
		fprintf(fp , "** BOUNDARY CONDITIONS\n");
		fprintf(fp , "**\n");
		fprintf(fp , "*Boundary\n");
		abaqus->nFixed = 0;
		for (i = 0 ; i < abaqus->nNodes ; i++) {
			if (abaqus->pbcXY) {
				if (abaqus->FEMnodes[i].zFix == 1) {
					fprintf(fp , "%d, ENCASTRE\n" , i+1);
					abaqus->nFixed++;
				}
			} else {
				if (abaqus->FEMnodes[i].xFix) fprintf(fp , "%d, 1\n" , i+1);
				if (abaqus->FEMnodes[i].yFix) fprintf(fp , "%d, 2\n" , i+1);
				if (abaqus->FEMnodes[i].zFix) fprintf(fp , "%d, 3\n" , i+1);
			}
		}
		
/*
 *		Defining the loading
 */		
		fprintf(fp , "**\n");
		fprintf(fp , "** LOADS\n");
		fprintf(fp , "**\n");
		fprintf(fp , "*Cload\n");
		for (i = 0 ; i < abaqus->nNodes ; i++) {
			if (fabs(abaqus->FEMnodes[i].Fx) > 1e-9)
				fprintf(fp , "%d, 1, %e\n"  , i+1 , abaqus->FEMnodes[i].Fx);
			
			if (fabs(abaqus->FEMnodes[i].Fy) > 1e-9)
				fprintf(fp , "%d, 2, %e\n"  , i+1 , abaqus->FEMnodes[i].Fy);
			
			if (fabs(abaqus->FEMnodes[i].Fz) > 1e-9)
				fprintf(fp , "%d, 3, %e\n"  , i+1 , abaqus->FEMnodes[i].Fz);
		}
		
/*
 *		Defining the field and history outputs from Abaqus
 */		
		fprintf(fp , "**\n");
		fprintf(fp , "** OUTPUT REQUESTS\n");
		fprintf(fp , "**\n");
		fprintf(fp , "*Restart, write, frequency=0\n");
		fprintf(fp , "**\n");
		fprintf(fp , "** FIELD OUTPUT: F-Output-1\n");
		fprintf(fp , "**\n");
		fprintf(fp , "*Output, field\n");
		fprintf(fp , "*Node Output\n");
		fprintf(fp , "CF, RF, U\n");
		fprintf(fp , "*Element Output, directions=YES\n");
		fprintf(fp , "E, S\n");
		fprintf(fp , "*Output, history, frequency=0\n");
		
/*
 *		Defining the desired ASCI format outputs from Abaqus
 */
		fprintf(fp , "**\n");
		fprintf(fp , "**\n");
		fprintf(fp , "*Node Print\n");
		fprintf(fp , "U\n");
		fprintf(fp , "**\n");
	//	fprintf(fp , "*EL PRINT\n");
	//	fprintf(fp , "COORD, S\n");
	//	fprintf(fp , "**\n");
		fprintf(fp , "*End Step\n");
		
		fclose(fp);
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void readABQresult(Home_t *home, Abaqus_t *abaqus)
{
		int         i , j , FEMnn , succes;
		char       *line = NULL , str[10];
		double      x , y , z, burg;
		FILE       *fp;
		size_t      len = 0;
		ssize_t     lineLen;

/*
 *		Zeroing and Wait for the Abaqus simulation to be done
 */
		for (i = 0 ; i < abaqus->nNodes ; i++) {
			abaqus->FEMnodes[i].Ux = 0.0;
			abaqus->FEMnodes[i].Uy = 0.0;
			abaqus->FEMnodes[i].Uz = 0.0;
		}
		
		i=0;
		while (1) {		  
		  if( access( "ABQ/abqJob.sta", F_OK ) == 0) {
		    //if( access( "ABQ/beam_tutorial2.sta", F_OK ) == 0) {
				break;
			} else {
				sleep (1);  i++;
				printf("Abaqus running time = %d s\n" , i);
			}
		}
		
/*
 *		Open the output file and start reading it until we get the
 *		part which the displacements are written
 */
	//	fp = fopen("n60x36.dat", "r");
	//	fp = fopen("n40x36.dat", "r");
		fp = fopen("ABQ/abqJob.dat", "r");
		//fp = fopen("ABQ/beam_tutorial2.dat", "r");
		printf("Abaqus output file ABQ/abqJob.dat opened successfully\n");

		succes = 0;
		while ((lineLen = getline(&line, &len, fp)) != -1) {
		//	printf("Retrieved line of length %zu:", lineLen);
		//	printf("%s", line);
			
			if ( strcmp(line , "   THE FOLLOWING TABLE IS PRINTED FOR ALL NODES\n") == 0) {
				for (j=0 ; j<4 ; j++) lineLen = getline(&line, &len, fp);
				succes = 1;
				break;
			}
		}
		
		if (succes == 0) 
			Fatal("Nodal displacements could not be read from the Abaqus output file\n");
		
/*
 *		Read the nodal displacement and store them into the FEMnode data structure
 */
		i = 0;
		while (1) {
			if (fscanf(fp , "%i %lf %lf %lf\n" , &j , &x , &y , &z) !=4) break;

			//xiaohan force ABQ scale to be 1 and scale back
			//burg = home->param->burgMag;
			//x*= burg; y*=burg; z*=burg;

			abaqus->FEMnodes[j-1].Ux = x;
			abaqus->FEMnodes[j-1].Uy = y;
			abaqus->FEMnodes[j-1].Uz = z;
			i++;
		}
		
/*
 *		Check if we have read all the data.
 *		If so, close the file and return.
 */
		fscanf(fp , "%s" , &str);
	/*	if (i != abaqus->nNodes - abaqus->nFixed) {
			printf("Number of nodes read: %d\n" , i);
			printf("Number of nodes (abaqus->nNodes): %d\n" , abaqus->nNodes);
			printf("Number of fixed nodes (abaqus->nFixed): %d\n" , abaqus->nFixed);
			Fatal("Abaqus output file was not read completely (1)\n");
		}*/
		if (strcmp("MAXIMUM" , str) != 0)
			Fatal("Abaqus output file was not read completely (2)\n");
		if (fclose(fp) != 0) Fatal("error closing file Abaqus output file\n"); 
		
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_Create_kpoints(Abaqus_t *abaqus)
{
  int i,j;

  int nx = abaqus->nx;
  int ny = abaqus->ny;
  
  real8 ABQLx = abaqus->ABQLx;
  real8 ABQLy = abaqus->ABQLy;

  int imax = nx/2 + nx % 2;
  int jmax = ny/2 + ny % 2;

  for (j=0; j<ny; j++) 
    {
      if (j < jmax)
	abaqus->ky[j]=j*2*M_PI/ABQLy;
      else
	abaqus->ky[j]=(j-ny)*2*M_PI/ABQLy;
    }      
  
  for (i=0; i<nx; i++) 
    {
      if (i < imax)
	abaqus->kx[i]=i*2*M_PI/ABQLx;
      else
	abaqus->kx[i]=(i-nx)*2*M_PI/ABQLx;
    }
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_stress_boundary(Home_t *home,Abaqus_t *abaqus)
{
/*
 *	Calculate Tractions on free surfaces from stress in the system
 *	F = sigma . n  = -T 
 *	Done every time step 
 */

	int     i , j , k , l;
	real8  *tx, x , *tx_tot , s [3][3];
	real8  *ty, y , *ty_tot , Ys[3][3];
	real8  *tz, z , *tz_tot ;
	
/*
 *	Allocate tx, ty, tz
 */
	tx     = (real8 *)malloc(sizeof(real8)*abaqus->nQuads);
	ty     = (real8 *)malloc(sizeof(real8)*abaqus->nQuads);
	tz     = (real8 *)malloc(sizeof(real8)*abaqus->nQuads);
#ifdef PARALLEL
	tx_tot = (real8 *)malloc(sizeof(real8)*abaqus->nQuads);
	ty_tot = (real8 *)malloc(sizeof(real8)*abaqus->nQuads);
	tz_tot = (real8 *)malloc(sizeof(real8)*abaqus->nQuads);
#endif

/*
 *	Calculate the traction on the top surface
 */
	for (i = 0 ; i < abaqus->nQuads ; i++) {
		x = abaqus->FEMquads[i].x;
		y = abaqus->FEMquads[i].y;
		z = abaqus->FEMquads[i].z;
		AllSegmentStress(home , abaqus , x , y , z , s);

#ifndef _NOYOFFESTRESS
		AllYoffeStress(home , abaqus , x , y , z , Ys);
		
		for (j = 0 ; j < 3 ; j++) {
			for (k = 0 ; k < 3 ; k++) {
				s[j][k] += Ys[j][k];
			}
		}
#endif

		tx[i] = s[0][2];
		ty[i] = s[1][2];
		tz[i] = s[2][2];
	}

#ifdef PARALLEL
	MPI_Allreduce(tx, tx_tot, abaqus->nQuads, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(ty, ty_tot, abaqus->nQuads, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Allreduce(tz, tz_tot, abaqus->nQuads, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	
	for (i = 0 ; i < abaqus->nQuads ; i++) {
		abaqus->FEMquads[i].Tx = tx_tot[i];
		abaqus->FEMquads[i].Ty = ty_tot[i];
		abaqus->FEMquads[i].Tz = tz_tot[i];
	}
	
	free(tx_tot);  free(ty_tot);  free(tz_tot);
#else
	for (i = 0 ; i < abaqus->nQuads ; i++) {
		abaqus->FEMquads[i].Tx = tx[i];
		abaqus->FEMquads[i].Ty = ty[i];
		abaqus->FEMquads[i].Tz = tz[i];
	}
#endif

	free(tx);  free(ty);  free(tz);
	
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

int Split(Home_t *home,Node_t *nodeout,Node_t *nodein, real8 t)
{
  // Create a node between nodeout and nodein on the abaqus surface.
  // nodeout is then placed at the surface.

  // nodeout becomes splitNode1
  // new node is SplitNode2
  // nodein untouched

  int armCount, splitStatus, globalOp, armID, *armList;
  real8 nodeVel[3], newVel[3];
  Node_t *splitNode1, *splitNode2;
  real8 xout[3],xin[3];
  real8 pos[3],vec[3];
  Param_t *param;

  param = home->param;

  xout[0] = nodeout->x;
  xout[1] = nodeout->y;
  xout[2] = nodeout->z;

  xin[0] = nodein->x;
  xin[1] = nodein->y;
  xin[2] = nodein->z;
  
  vec[0] = xin[0] - xout[0];
  vec[1] = xin[1] - xout[1];
  vec[2] = xin[2] - xout[2];

  real8 lr = sqrt(vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]);
  vec[0] /= lr;vec[1] /=lr;vec[2] /=lr;
  
  // Velocity for nodeout
  if (fabs(vec[2]) > 0.05)
    {
      nodeVel[0] = nodeout->vX - nodeout->vZ * vec[0] / vec[2];
      nodeVel[1] = nodeout->vY - nodeout->vZ * vec[1] / vec[2];
      nodeVel[2] = 0.0;
    }
  else
    {
      nodeVel[0] = nodeout->vX*vec[2] - nodeout->vZ * vec[0];
      nodeVel[1] = nodeout->vY*vec[2] - nodeout->vZ * vec[1];
      nodeVel[2] = 0.0;
    }

  newVel[0] = nodeVel[0];  //  for splitNode2
  newVel[1] = nodeVel[1];
  newVel[2] = nodeVel[2];


  // Position of the new node
  GetSurfaceNode(param,nodeout,nodein,pos,t);

  globalOp = 1;
  armCount = 1;
  armID = GetArmID(home, nodeout, nodein);
  armList = &armID;


  // splitNode1 == new node
  // splitNode2 == node out
  splitStatus = SplitNode ( home, OPCLASS_REMESH,
			    nodeout, xout, pos,
			    nodeVel, newVel,
			    armCount, armList,
			    globalOp, &splitNode1,
			    &splitNode2, 0 ); 
  
  if (splitStatus == SPLIT_SUCCESS) 
    {
      splitNode1->fX = nodeout->fX;
      splitNode1->fY = nodeout->fY;
      splitNode1->fZ = nodeout->fZ;

      splitNode2->fX = nodeout->fX;
      splitNode2->fY = nodeout->fY;
      splitNode2->fZ = nodeout->fZ;
      
            
      MarkNodeForceObsolete(home, splitNode1);
      MarkNodeForceObsolete(home, splitNode2);
      MarkNodeForceObsolete(home, nodein);

      if (nodeout->constraint == 7)
	{ splitNode1->constraint = 7; // split node on the surface
	  splitNode2->constraint = 7; // old nodeout, outside abaqus   
	}
      else
	{
	  splitNode1->constraint = 6; // split node on the surface
	  splitNode2->constraint = 6; // old nodeout, outside abaqus
	}
    }

  return splitStatus;
}

void GetSurfaceNode(Param_t *param,Node_t *nodeout,Node_t *nodein,real8 pos[3],
		    real8 t)
{
  // Find the point on the surface between node out and in.
  // out is the node to be modified

  real8 xout = nodeout->x;
  real8 yout = nodeout->y;
  real8 zout = nodeout->z;

  real8 xin = nodein->x;
  real8 yin = nodein->y;
  real8 zin = nodein->z;

  PBCPOSITION(param,xout,yout,zout,&xin,&yin,&zin);

  if ( fabs(zin-zout) < 1.e-3)
    { // already at the surface or points parallel to the surface
      pos[0] = xout;
      pos[1] = yout;
    }
  else
    {
      real8 q,u,v;
      q = (t - zout)/(zin - zout);
      u = xout + (xin - xout)*q;
      v = yout + (yin - yout)*q;
      
      pos[0] = u; 
      pos[1] = v; 
    }

      pos[2] = t;
}

/******************************************************************
 ******************************************************************
 ******************************************************************/

#endif
