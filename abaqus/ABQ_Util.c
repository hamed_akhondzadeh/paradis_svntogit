/***************************************************************************
 *
 *  Module      : ABQUtil.c
 *  Description : Connection with ABQ module functions  
 *  (Sylvie Aubry Fri Feb 22 2008)
 *
 **************************************************************************/

#include <stdio.h>
#include <math.h> 
#include "Param.h"
#include "Home.h"

#ifdef _ABAQUS

#include "ABQ.h"

/* void ABQ_Init(Home_t *home,Abaqus_t **abq_ptr) */
Abaqus_t* ABQ_Init(Home_t *home)
{
	int       i , j;
	double    coef , c11 , c12 , c44;
	Param_t  *param;
	Abaqus_t *abaqus;

	abaqus = (Abaqus_t *) calloc(1, sizeof(Abaqus_t));
	//*abq_ptr = abaqus;

	param = home->param;

	if (home->myDomain == 0) {
		/* Banner for Abaquss */
		printf("\n\n");
		printf("****************************************************\n\n");
		printf("****       MODULE : ABAQUS FOR PARADIS       ****\n\n");
		printf("****************************************************\n\n");
      
#ifdef  _NOYOFFESTRESS
		printf("No Yoffe stress correction\n");
#else
		printf("Using Yoffe stress correction\n");
#endif        
      
#ifdef  _NOVIRTUALSEG
		printf("No Virtual Segment correction\n");
#else
		printf("Using Virtual Segment correction\n");
#endif 
      
#ifdef  _ABQIMGSTRESS
		printf("Using Spectral method correction\n");
#else
		printf("No Spectral method correction\n");
#endif 
     

		if (param->fmEnabled)
			printf("Using FMM method\n");
		else {
#ifndef FULL_N2_FORCES
			printf("Using Rij tables\n");
#else
			printf("Using FULL_N2_FORCES calculations\n");
#endif
		}

		if (!param->elasticinteraction) printf("Line tension on\n");

		printf("\n");
	}

	/* Initialization */
	abaqus->mu = param->shearModulus;
	abaqus->nu = param->pois;
	abaqus->lambda = 2*abaqus->mu*abaqus->nu/(1-2*abaqus->nu);
	
	coef = abaqus->mu / (1.0 + abaqus->nu) / (1.0 - 2.0*abaqus->nu);
	c11  = coef * (1.0 - abaqus->nu);
	c44  = coef * (0.5 - abaqus->nu);
	c12  = coef * abaqus->nu;
	
	for (i = 0 ; i < 6 ; i++) {
		for (j = 0 ; j < 6 ; j++) {
			abaqus->Cij[i][j] = 0.0;
		}
	}
	
	abaqus->Cij[0][0] = c11;  abaqus->Cij[0][1] = c12;  abaqus->Cij[0][2] = c12;
	abaqus->Cij[1][0] = c12;  abaqus->Cij[1][1] = c11;  abaqus->Cij[1][2] = c12;
	abaqus->Cij[2][0] = c12;  abaqus->Cij[2][1] = c12;  abaqus->Cij[2][2] = c11;
	abaqus->Cij[3][3] = c44;  abaqus->Cij[4][4] = c44;  abaqus->Cij[5][5] = c44;

	abaqus->ncpus = param->abq_ncpus;
	abaqus->rc    = param->rc;
	abaqus->nx    = 0;
	abaqus->ny    = 0;
	abaqus->nz    = 0;
	
#ifdef  _ABQIMGSTRESS
	abaqus->nx    = param->abq_nx   ;
	abaqus->ny    = param->abq_ny   ;
	abaqus->nz    = param->abq_nz   ;
	abaqus->pbcXY = param->abq_pbcXY;
	abaqus->nQuadSurface = param->nQuadSurface;

	if(abaqus->nx == 0|| abaqus->ny == 0|| abaqus->nz == 0)
		Fatal("ABORT - nx or ny or nz is 0 in input file\n");
#endif

	abaqus->t  = param->abq_thickness / 2.0;
	abaqus->LenVirtualSeg = param->abq_VSlength;
	
	if (strcmp(param->abq_elemType, "tetrahedral") == 0) {
		
		if      (strcmp(param->abq_elemOrder, "linear"   ) == 0) abaqus->nodePelem = 4 ;
		else if (strcmp(param->abq_elemOrder, "quadratic") == 0) abaqus->nodePelem = 10;
		else   Fatal("abq_elemOrder should be defined as either linear or quadratic\n");
		
	} else if (strcmp(param->abq_elemType, "hexahedral") == 0) {
		
		if      (strcmp(param->abq_elemOrder, "linear"   ) == 0) abaqus->nodePelem = 8 ;
		else if (strcmp(param->abq_elemOrder, "quadratic") == 0) abaqus->nodePelem = 20;
		else   Fatal("abq_elemOrder should be defined as either linear or quadratic\n");
		
	} else {
		Fatal("abq_elemType should be defined as either tetrahedral or hexahedral\n");
	}		

	abaqus->Mx = 0.0;

#ifdef _BENDING
	abaqus->Mx = param->abq_Mx;
	abaqus->Bend_theta = param->abq_Bend_theta;
#endif

	if (!param->elasticinteraction) 
		param->TensionFactor = log(param->abq_thickness/param->rc)/(2.*M_PI);

	abaqus->ABQLx = param->Lx;
	abaqus->ABQLy = param->Ly;
	
	if (strcmp(param->tf_or_hs, "hs") == 0) abaqus->ABQLz =-param->minSideZ;
	if (strcmp(param->tf_or_hs, "tf") == 0) abaqus->ABQLz = abaqus->t * 2.0;

	if (home->myDomain == 0) {
		printf("Domain bounds:\n");
		printf("(Xmin=%10g,Ymin=%10g,Zmin=%10g)\n",param->minSideX,param->minSideY,param->minSideZ);
		printf("(Xmax=%10g,Ymax=%10g,Zmax=%10g)\n",param->maxSideX,param->maxSideY,param->maxSideZ);

		if (strcmp(param->tf_or_hs, "hs") == 0) printf("\nParameters of the half-space:\n");
		if (strcmp(param->tf_or_hs, "tf") == 0) printf("\nParameters of the thin-film:\n");
		printf("    Lx  = %10g\n",abaqus->ABQLx);
		printf("    Ly  = %10g\n",abaqus->ABQLy);
		printf("    Lz  = %10g\n",abaqus->ABQLz);
		printf("    t   = %10g\n",abaqus->t);
		printf("    lambda = %10g\n",abaqus->lambda);
		printf("    mu = %10g\n",abaqus->mu);
		printf("    nu = %10g\n\n",abaqus->nu);
      
#ifdef  _ABQIMGSTRESS
		printf("    nx = %5d\n"   ,abaqus->nx);
		printf("    ny = %5d\n"   ,abaqus->ny);
		printf("    nz = %5d\n"   ,abaqus->nz);
		printf("    b  = %e m\n\n",param->burgMag);
#endif
#ifndef  _NOVIRTUALSEG
		printf("    Length of virtual segments = %10g\n",abaqus->LenVirtualSeg);
#endif

    }
  
#ifdef  _ABQIMGSTRESS
	/* Allocate dynamic arrays */
	ABQ_allocations(home,abaqus);

	/* compute kx and ky */
	ABQ_Create_kpoints(abaqus);

	/* compute the matrices */
	ABQ_Create_Matrices(abaqus);
	
	/* create FEM mesh */
	ABQ_Create_Mesh(param,abaqus);
	
	if (home->myDomain == 0) {
		printf("    No. of FEM nodes           = %d\n"  ,abaqus->nNodes);
		printf("    No. of FEM elements        = %d\n"  ,abaqus->nElems);
		printf("    No. of nodes per element   = %d\n"  ,abaqus->nodePelem);
		printf("    ncpus for Abaqus           = %d\n\n",abaqus->ncpus);
	}

#endif
	return abaqus;
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/
 
//To do: combine the following two functions into one. 

void ABQ_Step(Home_t *home,Abaqus_t *abaqus)
{
		double  pos[3] , strain[3][3];
		char    abq_run[50];
	
/*
 *		Calculating Tractions from ParaDiS code T=sigma^\infty . n
 */
		ABQ_stress_boundary(home,abaqus);
  
#ifdef _ABQIMGSTRESS  
	
		if (home->myDomain == 0) {
	
/*
 *			Calculating the external forces on the FEM nodes.
 *			And writing the input file (called abqJob.inp) that 
 *			can be read by Abaqus.
 */
			ABQ_calc_nodeF(home, abaqus);
			ABQ_writeInput(home, abaqus);
		
/*
 *			Run Abaqus using the created abqJob.inp file
 */
			if (chdir("ABQ") != 0) 
				Fatal("Task %d: Unable to cd into directory ABQ", home->myDomain);
		
			sprintf(abq_run, "abaqus job=abqJob cpus=%d", abaqus->ncpus);
			printf ("%s\n", abq_run);
#if 0
			//xiaohan runs abaqus from outside  
			system(abq_run); 
#endif 
			chdir("..");
		}
		
/*
 *		Go ahead and read the FEM nodal displacement calculated by Abaqus
 */
		readABQresult(home,abaqus);
		
/*
 *		If continuous stress stress field is requested, calculate the
 *		average stresses on the nodes here
 */
#if 1
#ifdef _CONTINUOUS_STRESSFIELD
		ABQ_calc_node_stress(home, abaqus);
#endif
#endif
#endif
}



void ABQ_Step_call_abq(Home_t *home,Abaqus_t *abaqus)
{
		double  pos[3] , strain[3][3];
		char    abq_run[50];
	
/*
 *		Calculating Tractions from ParaDiS code T=sigma^\infty . n
 */
		ABQ_stress_boundary(home,abaqus);
  
#ifdef _ABQIMGSTRESS  
	
		if (home->myDomain == 0) {
	
/*
 *			Calculating the external forces on the FEM nodes.
 *			And writing the input file (called abqJob.inp) that 
 *			can be read by Abaqus.
 */
			ABQ_calc_nodeF(home, abaqus);
			ABQ_writeInput(home, abaqus);
		
/*
 *			Run Abaqus using the created abqJob.inp file
 */
			if (chdir("ABQ") != 0) 
				Fatal("Task %d: Unable to cd into directory ABQ", home->myDomain);
		
			sprintf(abq_run, "abaqus job=abqJob cpus=%d", abaqus->ncpus);
			printf ("%s\n", abq_run);
#if 1
			//xiaohan runs abaqus from outside  
			system(abq_run); 
#endif 
			chdir("..");
		}
		
/*
 *		Go ahead and read the FEM nodal displacement calculated by Abaqus
 */
		readABQresult(home,abaqus);
		
/*
 *		If continuous stress stress field is requested, calculate the
 *		average stresses on the nodes here
 */
#if 1
#ifdef _CONTINUOUS_STRESSFIELD
		ABQ_calc_node_stress(home, abaqus);
#endif
#endif
#endif
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void ABQ_Finish(Abaqus_t * abaqus)
{
	free(abaqus->kx);free(abaqus->ky);
	free(abaqus->Txp);free(abaqus->Typ);free(abaqus->Tzp);
	free(abaqus->Txm);free(abaqus->Tym);free(abaqus->Tzm);

	int NXMAX = abaqus->nx;
	int NYMAX = abaqus->ny;

	int i,j,k;
	for (i=0;i<NXMAX;i++) {
		free(abaqus->A[i]);free(abaqus->B[i]);free(abaqus->C[i]);
		free(abaqus->E[i]);free(abaqus->F[i]);free(abaqus->G[i]);
	}

	for (i=0;i<3;i++) {
		for (j=0;j<3;j++) {
			for (k=0;k<NXMAX;k++) {
				free(abaqus->MsInv[i][j][k]);
				free(abaqus->MaInv[i][j][k]);
				free(abaqus->Stress[i][j][k]);
			}
		}
	}

	for (i=0;i<3;i++) {
		for (j=0;j<3;j++) {
			free(abaqus->MsInv[i][j]);
			free(abaqus->MaInv[i][j]);
			free(abaqus->Stress[i][j]);
		}
	}

}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void SanityCheck(Home_t *home)
{
	Node_t  *node,*nbr;
	double burgSumX,burgSumY,burgSumZ;
	int i, ti, iNbr;

	for (i = 0; i < home->newNodeKeyPtr; i++) {
		node = home->nodeKeys[i];
		if (node == (Node_t *)NULL) continue;

		burgSumX = 0.0;
		burgSumY = 0.0;
		burgSumZ = 0.0;  

		int nbrs=node->numNbrs;

		if (node->constraint == 0 && nbrs < 2) {
			printf("\n\nFatal Sanity check\n");
			printf("in ABQ_Util: node (%d,%d)\n",
			node->myTag.domainID, node->myTag.index);
			printf("This node has %d neighbor and its constraint is 0\n",nbrs);
			printf("node: x=%f y=%f z=%f\n",node->x,node->y,node->z);
			
			for (ti=0;ti<nbrs;ti++) {
				nbr = GetNeighborNode(home, node, ti);
				printf("neig: x=%f y=%f z=%f\n\n\n",nbr->x,nbr->y,nbr->z);
			}
		}
      
		for (ti=0;ti<nbrs;ti++) {
			nbr = GetNeighborNode(home, node, ti);
			if (nbr == (Node_t *)NULL) continue;

			burgSumX += node->burgX[ti];
			burgSumY += node->burgY[ti];
			burgSumZ += node->burgZ[ti];
		}

		if (node->constraint == 0) {
			if ((fabs(burgSumX) > 0.0001) ||
			    (fabs(burgSumY) > 0.0001) ||
			    (fabs(burgSumZ) > 0.0001)) {

				printf("Non conservation of Burgers vector: %f %f %f\n",fabs(burgSumX),fabs(burgSumY),fabs(burgSumZ));

				printf("Error in ABQ_Util: node (%d,%d)\n",
				node->myTag.domainID, node->myTag.index);

				for (iNbr=0; iNbr < node->numNbrs; iNbr++) {
					printf("  arm[%d] burg = %e %e %e cst= %d\n",
					iNbr, node->burgX[iNbr],
					node->burgY[iNbr],
					node->burgZ[iNbr],node->constraint);
				}
	      
				Fatal("Burger's vector not conserved!");
			}	
		}
      
	}
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void Print3x3(char *format,real8 A[3][3])
{
	printf("\n %s\n", format);

	printf("%.15e %.15e %.15e\n"  ,A[0][0],A[0][1],A[0][2]);
	printf("%.15e %.15e %.15e\n"  ,A[1][0],A[1][1],A[1][2]);
	printf("%.15e %.15e %.15e\n\n",A[2][0],A[2][1],A[2][2]);
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void Print3(char *format,real8 A[3])
{
	printf("%s = ", format);
	printf("%.15e %.15e %.15e\n",A[0],A[1],A[2]);
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void Print6(char *format,real8 A[6])
{
	printf("\n %s \n", format);
	printf("%.15e %.15e %.15e \n %.15e %.15e %.15e\n\n",A[0],A[1],A[2],
	        A[3],A[4],A[5]);
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void Print3x3x3(char *format,double A[3][3][3])
{
	printf("\n %s\n", format);
	printf("%.15e %.15e %.15e\n",A[0][0][0],A[0][0][1],A[0][0][2]);
	printf("%.15e %.15e %.15e\n",A[0][1][0],A[0][1][1],A[0][1][2]);
	printf("%.15e %.15e %.15e\n",A[0][2][0],A[0][2][1],A[0][2][2]);

	printf("%.15e %.15e %.15e\n",A[1][0][0],A[1][0][1],A[1][0][2]);
	printf("%.15e %.15e %.15e\n",A[1][1][0],A[1][1][1],A[1][1][2]);
	printf("%.15e %.15e %.15e\n",A[1][2][0],A[1][2][1],A[1][2][2]);

	printf("%.15e %.15e %.15e\n",A[2][0][0],A[2][0][1],A[2][0][2]);
	printf("%.15e %.15e %.15e\n",A[2][1][0],A[2][1][1],A[2][1][2]);
	printf("%.15e %.15e %.15e\n",A[2][2][0],A[2][2][1],A[2][2][2]);

}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void Init3x3(double A[3][3])
{
	int i, j;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			A[i][j] = 0.0;
		}
	}
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void PrintNodesandNeighbors(char *format,Home_t *home)
{
	int i,j;
	Node_t *nodea, *nbr;

	for (i = 0; i < home->newNodeKeyPtr; i++) {
		nodea = home->nodeKeys[i];
		if (nodea == (Node_t *)NULL) continue;

		for (j = 0; j < nodea->numNbrs; j++) {
			nbr = GetNeighborNode(home, nodea, j);

			if (nodea->myTag.index ==  233 || nbr->myTag.index ==  233) {
				printf("\n %s\n", format);
				printf("Node (%d,%d) nx=%e,ny=%e,nz=%e\n",
				nodea->myTag.domainID, nodea->myTag.index, 
				nodea->nx[j], nodea->ny[j], nodea->nz[j]);
				printf("      Nbr (%d,%d) nx=%e,ny=%e,nz=%e\n",
				nbr->myTag.domainID, nbr->myTag.index, 
				nbr->nx[j], nbr->ny[j], nbr->nz[j]);
			}
		}
	}
}

/***************************************************************************
 ***************************************************************************
 ***************************************************************************/

void InfoNode(Home_t *home,Node_t *node)
{
	int j;
	Node_t *nbr;

	printf("node(%d,%d) cst=%d has %d neighbors\n",
	node->myTag.domainID, node->myTag.index, 
	node->constraint,node->numNbrs);

	for (j = 0; j < node->numNbrs; j++) {
		
		nbr = GetNeighborNode(home, node, j);
		printf("            nbr(%d,%d) nx=%e,ny=%e,nz=%e\n",
		nbr->myTag.domainID, nbr->myTag.index, 
		nbr->nx[j], nbr->ny[j], nbr->nz[j]);

	}
	
	printf("\n");
}


#endif


