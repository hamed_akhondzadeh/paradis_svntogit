#ifdef _ABAQUS
#ifdef _ABQIMGSTRESS

#include <stdio.h>
#include <math.h>

#include "Home.h"
#include "ABQ.h"

void ABCcoeff(Abaqus_t *abaqus)

/*
 * This program  computes A, B, C, E, F, G coeffs for linear 
 * elastic solution of a abaqus with height t
 * Sylvie Aubry Mar 3 2008
 *
 */
{
  int i,j;

#ifndef _CYGWIN
  fftw_complex *txp,*typ,*tzp,*txm,*tym,*tzm;
#else
  complex *txp,*typ,*tzp,*txm,*tym,*tzm;
#endif

  COMPLEX txS,tyS,tzS,txA,tyA,tzA;
  int nx = abaqus->nx;
  int ny = abaqus->ny;

  int nxy = nx*ny;

   txp = (COMPLEX *) malloc(sizeof(COMPLEX)*nx*ny);
   typ = (COMPLEX *) malloc(sizeof(COMPLEX)*nx*ny);
   tzp = (COMPLEX *) malloc(sizeof(COMPLEX)*nx*ny);

   txm = (COMPLEX *) malloc(sizeof(COMPLEX)*nx*ny);
   tym = (COMPLEX *) malloc(sizeof(COMPLEX)*nx*ny);
   tzm = (COMPLEX *) malloc(sizeof(COMPLEX)*nx*ny);

   fourier_transform_forward(abaqus->Txp,(fftw_complex *)txp,nx,ny);
   fourier_transform_forward(abaqus->Typ,(fftw_complex *)typ,nx,ny);
   fourier_transform_forward(abaqus->Tzp,(fftw_complex *)tzp,nx,ny);

   fourier_transform_forward(abaqus->Txm,(fftw_complex *)txm,nx,ny);
   fourier_transform_forward(abaqus->Tym,(fftw_complex *)tym,nx,ny);
   fourier_transform_forward(abaqus->Tzm,(fftw_complex *)tzm,nx,ny);
   
   for (i=0; i<nx; i++)
     for (j=0; j<ny; j++)
       {
         txS = (txp[j+i*ny] - txm[j+i*ny])/(2.0*nxy);
	 tyS = (typ[j+i*ny] - tym[j+i*ny])/(2.0*nxy);
	 tzS = (tzp[j+i*ny] - tzm[j+i*ny])/(2.0*nxy);
	 
	 txA = (txp[j+i*ny] + txm[j+i*ny])/(2.0*nxy);
	 tyA = (typ[j+i*ny] + tym[j+i*ny])/(2.0*nxy);
	 tzA = (tzp[j+i*ny] + tzm[j+i*ny])/(2.0*nxy);

	 abaqus->A[i][j] =   abaqus->MsInv[0][0][i][j]*txA + 
	                       abaqus->MsInv[0][1][i][j]*tyA + 
	                       abaqus->MsInv[0][2][i][j]*tzS;
	 abaqus->B[i][j] =   abaqus->MsInv[1][0][i][j]*txA + 
                               abaqus->MsInv[1][1][i][j]*tyA + 
		               abaqus->MsInv[1][2][i][j]*tzS;
	 abaqus->C[i][j] =   abaqus->MsInv[2][0][i][j]*txA + 
                               abaqus->MsInv[2][1][i][j]*tyA + 
		               abaqus->MsInv[2][2][i][j]*tzS;

	 abaqus->E[i][j] =   abaqus->MaInv[0][0][i][j]*txS + 
                               abaqus->MaInv[0][1][i][j]*tyS + 
		               abaqus->MaInv[0][2][i][j]*tzA;
	 abaqus->F[i][j] =   abaqus->MaInv[1][0][i][j]*txS + 
                               abaqus->MaInv[1][1][i][j]*tyS + 
		               abaqus->MaInv[1][2][i][j]*tzA;
	 abaqus->G[i][j] =   abaqus->MaInv[2][0][i][j]*txS + 
                               abaqus->MaInv[2][1][i][j]*tyS + 
		               abaqus->MaInv[2][2][i][j]*tzA;
       } 

   free(txp);
   free(typ);
   free(tzp);

   free(txm);
   free(tym);
   free(tzm);

   return;  
}

#endif
#endif

