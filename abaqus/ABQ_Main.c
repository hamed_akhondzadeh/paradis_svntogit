/***************************************************************************
 *
 *  Function    : ABQ Main
 *  Description : main routine for ParaDiS/ abaqus simulation
 *
 **************************************************************************/
#include <stdio.h>
#include <time.h>
#include "Home.h"

#ifdef _ABAQUS
#include "ABQ.h"
#endif

#include "Init.h"


#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef FPES_ON
#include <fpcontrol.h>
#endif

Home_t* ParadisInit(int argc, char *argv[]);

#ifdef _ABAQUS
#include "ABQ.h"
#include <unistd.h>
#include <stdlib.h>
void ParadisStep(Home_t *home,Abaqus_t *abaqus);
#else
void ParadisStep(Home_t *home);
#endif

void ParadisFinish(Home_t *home);

main (int argc, char *argv[])
{
		int     cycleEnd, memSize, initialDLBCycles;
		time_t  tp;
		Home_t  *home;
		Param_t *param;

#ifdef _ABAQUS
		Abaqus_t *abaqus;
#endif
/*
 *		On some systems, the getrusage() call made by Meminfo() to get
 *		the memory resident set size does not work properly.  In those
 *		cases, the function will try to return the current heap size 
 *		instead.  This initial call allows meminfo() to get a copy of
 *		the original heap pointer so subsequent calls can calculate the
 *		heap size by taking the diference of the original and current
 *		heap pointers.
 */
        Meminfo(&memSize);

/*
 *		on linux systems (e.g. MCR) if built to have floating point exceptions
 *		turned on, invoke macro to do so
 */
   
#ifdef FPES_ON
		unmask_std_fpes();
#endif

		home = ParadisInit(argc, argv);
   
#ifdef _ABAQUS    
		abaqus = ABQ_Init(home);
#endif

		home->cycle      = home->param->cycleStart;

		param            = home->param;
		cycleEnd         = param->cycleStart + param->maxstep;
		initialDLBCycles = param->numDLBCycles;

/*
 *		Perform the needed number (if any) of load-balance-only
 *		steps before doing the main processing loop.  These steps
 *		perform only the minimal amount of stuff needed to
 *		estimate per-process load, move boundaries and migrate
 *		nodes among processsors to get a good initial balance.
 */
		TimerStart(home, INITIALIZE);

		if ((home->myDomain == 0) && (initialDLBCycles != 0)) {
			time(&tp);
			printf("  +++ Beginning %d load-balancing steps at %s",
				   initialDLBCycles, asctime(localtime(&tp)));
		}

		while (param->numDLBCycles > 0) {
#ifdef _ABAQUS
			ParadisStep(home,abaqus);
#else
			ParadisStep(home);
#endif
			home->cycle++;
			param->numDLBCycles--;
		}

		if ((home->myDomain == 0) && (initialDLBCycles != 0)) {
			time(&tp);
			printf("  +++ Completed load-balancing steps at %s",
				   asctime(localtime(&tp)));
		}

		TimerStop(home, INITIALIZE);

/*
 *		Any time spent doing the initial DLB-only steps should
 *		just be attributed to initialization time, so be sure to
 *		reset the other timers before going into the main
 *		computational loop
 */
		TimerInitDLBReset(home);

/*
 *		The cycle number may have been incremented during the initial
 *		load-balance steps, so reset it to the proper starting
 *		value before entering the main processing loop.
 */
		home->cycle = home->param->cycleStart;

/*		For checking only */
#if defined _ABAQUS & defined _PRINTSTRESS
		CellCharge(home);
		SortNativeNodes(home);

#ifdef _ABQIMGSTRESS
		//ABQ_Step(home,abaqus);
		ABQ_Step_call_abq(home,abaqus);
#endif
		PrintStress(home,abaqus);		
#endif

		while (home->cycle < cycleEnd) {

#if defined _ABAQUS & defined _ABQIMGSTRESS
			SortNativeNodes(home);
			CellCharge(home); 
			//ABQ_Step(home,abaqus);
			ABQ_Step_call_abq(home,abaqus);
			CommSendGhosts(home);
#endif


#ifdef _ABAQUS
#if !defined _NOYOFFESTRESS | !defined _NOVIRTUALSEG
			BuildSurfaceSegList(home, abaqus);
#endif
			ParadisStep(home,abaqus);
#else
			ParadisStep(home);
#endif
	  
			TimerClearAll(home);
		}

		ParadisFinish(home);


#ifdef _ABAQUS
#ifdef  _ABQIMGSTRESS
		ABQ_Finish(abaqus);
		FreeCellCters();
#endif
#endif

		exit(0);
}
