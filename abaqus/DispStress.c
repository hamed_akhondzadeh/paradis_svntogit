#include "Home.h"
#include "ABQ.h"

void DispStress(Abaqus_t *abaqus,real8 r[3], real8 stress[3][3])
{
/*-------------------------------------------------------------------------
 *
 *      Function   :  DispStress
 *      Description:  This program computes the stress from the 
 *                    displacement field using elasticity theory in a 
 *                    thin film or half-space and tractions computed 
 *                    in ABQ_Util.c from AllSegmentStress.c
 *
 *      Sylvie Aubry, Wed Feb 27 2008
 *
 *------------------------------------------------------------------------*/

	int k,l,i, j, m, n;
	double kx, ky;
	double kz, kx2, ky2, kz2, kz3, kxy, kxz, kyz,kxmy;
	double lm, l2m,lpm,lmpm,l2mpm;
	double coshk, sinhk,alpha;
	double x,y,z;
	COMPLEX expk;

	int nx     = abaqus->nx;
	int ny     = abaqus->ny;
	double  t  = abaqus->t;

	double ABQLx     = abaqus->ABQLx;
	double ABQLy     = abaqus->ABQLy;
	double mu       = abaqus->mu;
	double lambda   = abaqus->lambda;

	lm  = lambda + mu;
	l2m = lm     + mu;
	lmpm= lambda/lm;
	l2mpm= l2m/lm;
	lpm = mu/lm;

/*
 *	r is the point at the mid segment between two nodes.
 *	stress is evaluated for each kx,ky in [1:nx]*[1:ny] at this point
 */
  
	for (i=0; i<3; i++) {
		for (j = 0; j < 3; j++) {
			stress[i][j] = 0.0;
		}
	}

	z = r[2];

	if (fabs(z) > t) return;

	for (j=0; j<ny; j++) {
		ky = abaqus->ky[j];
		ky2 = ky*ky;

		for (i=0; i<nx; i++) {
			kx = abaqus->kx[i];
			kx2 = kx*kx;

			kz2 = kx2+ky2;
			kxmy = kx2-ky2;

			kz  = sqrt(kz2);
			kz3 = kz2*kz;

			kxz = kx*kz;
			kyz = ky*kz;

			kxy = kx*ky;

			COMPLEX AA = abaqus->A[i][j];
			COMPLEX BB = abaqus->B[i][j];
			COMPLEX CC = abaqus->C[i][j];
			COMPLEX EE = abaqus->E[i][j];
			COMPLEX FF = abaqus->F[i][j];
			COMPLEX GG = abaqus->G[i][j];
#ifdef _CYGWIN
			COMPLEX I = complex(0.0, 1.0);
#endif
	  
			// Translated for FFT origin accuracy
			x = r[0] + ABQLx*0.5;
			y = r[1] + ABQLy*0.5;

			coshk = cosh(kz*z);
			sinhk = sinh(kz*z);
			alpha = kx*x+ky*y;
			expk  = mu*(cos(alpha)+I*sin(alpha));

			abaqus->Stress[0][0][i][j] = 2.0*expk*
			    ((I*AA*lmpm*kz + (I*EE*kx2*z+I*BB*kxy-CC*kx2))*coshk + 
				 (I*EE*lmpm*kz + (I*AA*kx2*z-I*FF*kxy-GG*kx2))*sinhk);

			abaqus->Stress[1][1][i][j] = 2.0*expk*
			    ((I*AA*lmpm*kz + (I*EE*ky2*z-I*BB*kxy-CC*ky2))*coshk +
				 (I*EE*lmpm*kz + (I*AA*ky2*z+I*FF*kxy-GG*ky2))*sinhk);

			abaqus->Stress[2][2][i][j] = -2.0*expk*
			    ((kz2*(I*EE*z - CC)-l2mpm*I*AA*kz)*coshk + 
				 (kz2*(I*AA*z - GG)-l2mpm*I*EE*kz)*sinhk);

			abaqus->Stress[2][0][i][j] = expk*
			    (((2.0*AA*kx*z-FF*ky+2.0*I*GG*kx)*kz-2.0*EE*kx*lpm)*coshk + 
			     ((2.0*EE*kx*z+BB*ky+2.0*I*CC*kx)*kz-2.0*AA*kx*lpm)*sinhk);

			abaqus->Stress[1][2][i][j] = expk*
			    (((2.0*AA*ky*z+FF*kx+2.0*I*GG*ky)*kz-2.0*EE*ky*lpm)*coshk + 
			     ((2.0*EE*ky*z-BB*kx+2.0*I*CC*ky)*kz-2.0*AA*ky*lpm)*sinhk);
	  
			abaqus->Stress[0][1][i][j] = I*expk*
			    ((2.0*EE*kxy*z-kxmy*BB+2.0*I*kxy*CC)*coshk + 
			     (2.0*AA*kxy*z+kxmy*FF+2.0*I*kxy*GG)*sinhk);
		}
	}
  
	for (k=0; k<3; k++) {
		for (l=0; l<3; l++) {
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {
#ifndef _CYGWIN	
					stress[k][l] += creal(abaqus->Stress[k][l][i][j]);
#else
					stress[k][l] += real(abaqus->Stress[k][l][i][j]);
#endif
				}
			}
		}
	}

	stress[1][0] = stress[0][1];
	stress[2][1] = stress[1][2];
	stress[0][2] = stress[2][0];

	return;
}

  
