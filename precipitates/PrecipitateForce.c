/**************************************************************************
 *
 *      Module:  PrecipitateForce -- This module contains various functions
 *               for calculating nodal forces due to precipitate misfit
 *				 stress fields.
 *
 *      Includes functions:
 *              DilatSphereStress
 *				Interp3D
 *				InterpPrecStress
 *				PrecipitateForce
 *				SetOneNodePrecForce
 *
 *
 *************************************************************************/
#ifdef _PREC

#include "Home.h"

#define MAX_NUM_POINTS 6 //max number of Gauss quadrature points used
#define MIN_NUM_POINTS 2 //min number of Gauss quadrature points used


/*-------------------------------------------------------------------------
 *
 *      Function:     DilatSphereStress
 *      Description:  Analytical stress field for a spherical inclusion
 *					  with a dilatational misfit.
 *
 *			Arguments:
 *				(x,y,z)		position of field point relative to sphere's
 * 							center
 *				dV			misfit volume
 *				MU			shear modulus
 *				NU			Poisson's ratio
 *				str			stress tensor, computed and returned to caller
 *					  
 *
 *					  Note: Rigorously, this solution should only be used
 *					  to compute nodal forces when the core radius is
 *					  zero. This is because the stress field needs to be
 *					  convolved with the core spreading function to use
 *					  the Peach-Koehler formula in the non-singular theory.
 *
 *------------------------------------------------------------------------*/

void DilatSphereStress(real8 x, real8 y, real8 z, real8 dV,
					   real8 MU, real8 NU, real8 str[3][3]) {
						   
		real8	factor, x2, y2, z2, r, invr3, invr5;

		factor = MU*dV*(1.0+NU)/(6.0*M_PI*(1.0-NU));
		x2 = x*x;
		y2 = y*y;
		z2 = z*z;
		r = sqrt(x2 + y2 + z2);
		//Protect against division by 0!
		if (r<1e-20) {
			invr3 = 0.0;
			invr5 = 0.0;
		} else {
			invr3 = 1.0/(r*r*r);
			invr5 = invr3/(r*r);
		}

		str[0][0] = factor*(invr3-3.0*x2*invr5);
		str[1][1] = factor*(invr3-3.0*y2*invr5);
		str[2][2] = factor*(invr3-3.0*z2*invr5);
		str[0][1] = -factor*(3.0*x*y*invr5);
		str[0][2] = -factor*(3.0*x*z*invr5);
		str[1][2] = -factor*(3.0*y*z*invr5);
		str[1][0] = str[0][1];
		str[2][0] = str[0][2];
		str[1][0] = str[0][1];				
				
		return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     Interp3D
 *      Description:  Linearly interpolate a function in 3D space.
 *
 *			Arguments:
 *				(xd,yd,zd)	relative position within the interval of the 
 *							query point, bounded by [0,1]
 *				(ixl,iyl,izl)	indices of the array for the point below the
 *								query point	
 *				f			3D array to interpolate
 *
 *      Returns:   interpolated value of f at (xd,yd,zd)	
 *
 *------------------------------------------------------------------------*/

real8 Interp3D(real8 xd, real8 yd, real8 zd, 
			   int ixl, int iyl, int izl, real8 ***f) {
	
		real8	f00, f10, f01, f11, f0, f1;
	
		f00 = f[ixl][iyl][izl]     * (1.0-xd) + f[ixl+1][iyl][izl]     * xd; 
		f10 = f[ixl][iyl+1][izl]   * (1.0-xd) + f[ixl+1][iyl+1][izl]   * xd; 
		f01 = f[ixl][iyl][izl+1]   * (1.0-xd) + f[ixl+1][iyl][izl+1]   * xd; 
		f11 = f[ixl][iyl+1][izl+1] * (1.0-xd) + f[ixl+1][iyl+1][izl+1] * xd; 
	
		f0 = f00*(1.0-yd) + f10*yd;
		f1 = f01*(1.0-yd) + f11*yd;
	
		return(f0*(1-zd) + f1*zd);
}
	

/*-------------------------------------------------------------------------
 *
 *      Function:     InterpPrecStress
 *      Description:  Interpolate a numerical precipitate stress field
 *
 *					  We distinguish two types of interpolation with the
 *					  interp_type parameter. If the segment is not pinned
 *					  at the precipitate surface, we use linear
 *					  interpolation. If it is, we use nearest neighbor
 *					  outside the precipitate interpolation.
 *
 *			Arguments:
 *				interp_type 0 for linear, 1 for nearest neighbor
 *				prec		pointer to precipitate structure
 *				(x,y,z)		point in space to evaluate stress
 *				str			stress tensor, computed and returned to caller
 *				
 *
 *------------------------------------------------------------------------*/

void InterpPrecStress(int interp_type, Prec_t *prec, real8 x, real8 y, real8 z,
					  real8 str[3][3]) {
		
		real8	alpha;
		real8	xt, yt, zt, dx, start;
		real8	xl, yl, zl, xd, yd, zd;
		real8	dist, distmin;
		real8	xlinvt, ylinvt, zlinvt;
		int		ixl, iyl, izl;
		int		layer, npoints; 
		int		i, j, k, imin, jmin, kmin;
		
		Precstress_t *precstress;
		
		static real8 invhalfpi = 2.0/M_PI;
		
		precstress = prec->precstress;
		
		alpha = precstress->alpha*prec->r1;
		
/*
 *		Transform the point into transformed coordinates.
 */
		xt = invhalfpi*atan(x/alpha);  
		yt = invhalfpi*atan(y/alpha);  
		zt = invhalfpi*atan(z/alpha); 
		
/*
*		Find the indices and points bounding the coordinate point.
*/ 	
		dx = 2.0/precstress->Ngrid;
		
		start = 1.0 - dx/2.0;	
		
		ixl = (int) floor((xt+start)/dx);
		iyl = (int) floor((yt+start)/dx);
		izl = (int) floor((zt+start)/dx);
		
/*
 *		interp_type = 0 means the segment is not pinned at the surface, so
 *		we can just use linear interpolaton and not worry about inaccuracies
 *		due to interpolation near the surface.
 */		
		if (interp_type == 0) {
		
/*
 *			Protect against running over the bounds of the table...
 */
			if (ixl>=precstress->Ngrid || iyl>=precstress->Ngrid || 
				izl>=precstress->Ngrid || ixl<0 || iyl<0 || izl<0) {
				Fatal("Field point requested is outside of precstress "
					  "table bounds for precipitate %d,%d "
					  "with indices: (%d,%d,%d)",prec->myTag.index,
					  prec->myTag.domainID,ixl,iyl,izl);
				
			}
		
/*
 *			Evaluate the stress field.
 */
		
			xl = ((real8) ixl)*dx-start;
			yl = ((real8) iyl)*dx-start;
			zl = ((real8) izl)*dx-start;
				
			xd = (xt - xl)/dx;
			yd = (yt - yl)/dx;
			zd = (zt - zl)/dx;
		
			str[0][0] = prec->sscale*Interp3D(xd,yd,zd,ixl,iyl,izl,precstress->sxx);
			str[1][1] = prec->sscale*Interp3D(xd,yd,zd,ixl,iyl,izl,precstress->syy);
			str[2][2] = prec->sscale*Interp3D(xd,yd,zd,ixl,iyl,izl,precstress->szz);
			str[0][1] = prec->sscale*Interp3D(xd,yd,zd,ixl,iyl,izl,precstress->sxy);
			str[0][2] = prec->sscale*Interp3D(xd,yd,zd,ixl,iyl,izl,precstress->sxz);
			str[1][2] = prec->sscale*Interp3D(xd,yd,zd,ixl,iyl,izl,precstress->syz);
			str[1][0] = str[0][1];
			str[2][0] = str[0][2];
			str[2][1] = str[1][2];

		} else {
/*
 *			interp_type = 1 means the segment is pinned at the surface.
 *			Linearly interpolating the stress field there could be highly inaccurate
 *			since it is discontinuous across the surface of the precipitate.
 *			Instead we use nearest neighbor interpolation for the nearest
 *			grid point outside the precipitate.
 */	
			layer = 0;
			distmin = -1.0;
			while (1) {
/*
 *				Loop over all grid points in this layer and store 
 *				which is closest.
 */
				npoints = 2*(1+layer);
				for (i=0; i<npoints; i++) {
					xl = ((real8) ixl+i)*dx-start;
					for (j=0; j<npoints; j++) {
						yl = ((real8) iyl+j)*dx-start;
						for (k=0; k<npoints; k++) {
							zl = ((real8) izl+k)*dx-start;
							//If the point is inside the cube, we already
							//checked it so skip it.
							if (i>0 && j>0 && k>0 &&
								i+1<npoints && j+1<npoints && k+1<npoints)
									continue;
							
							//Transform into physical coordinates
							xlinvt = alpha*tan(xl/invhalfpi);
							ylinvt = alpha*tan(yl/invhalfpi);
							zlinvt = alpha*tan(zl/invhalfpi);
							
							//If the point is inside the precipitate,
							//skip it.
							if(InsideEllipsoid(xlinvt, ylinvt, zlinvt, 0.0, 0.0, 0.0, prec,
								(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL))
									continue;
							
							//Compute the distance and compare to existing minimum
							dist = sqrt((x-xlinvt)*(x-xlinvt) 
										+ (y-ylinvt)*(y-ylinvt)
										+ (z-zlinvt)*(z-zlinvt));
							if (dist<distmin || distmin<0.0) {
								distmin = dist;
								imin = i; jmin = j; kmin = k;
								//printf("closest point in layer %i with %i %i %i and dist=%e\n",layer,imin,jmin,kmin,dist);
							}
						}
					}
				}
				
				//If we found a close point outside the precipitate, we're done!
				if (distmin>0.0) {
					break;
				}
				
				//Shift to the next layer of grid points out and keep looking
				layer++;
				ixl -= 1;
				iyl -= 1;
				izl -= 1;
			}
			
/*
 *			Protect against running over the bounds of the table...
 */
			ixl += imin;
			iyl += jmin;
			izl += kmin;
			
			if (ixl>=precstress->Ngrid || iyl>=precstress->Ngrid || 
				izl>=precstress->Ngrid || ixl<0 || iyl<0 || izl<0) {
				Fatal("Field point requested is outside of precstress "
					  "table bounds for precipitate %d,%d "
					  "with indices: (%d,%d,%d)",prec->myTag.index,
					  prec->myTag.domainID,ixl,iyl,izl);
				
   			}
			
			//Evaluate the stress state at the closest point
			str[0][0] = prec->sscale*precstress->sxx[ixl][iyl][izl];
			str[1][1] = prec->sscale*precstress->syy[ixl][iyl][izl];
			str[2][2] = prec->sscale*precstress->szz[ixl][iyl][izl];
			str[0][1] = prec->sscale*precstress->sxy[ixl][iyl][izl];
			str[0][2] = prec->sscale*precstress->sxz[ixl][iyl][izl];
			str[1][2] = prec->sscale*precstress->syz[ixl][iyl][izl];
			str[1][0] = str[0][1];
			str[2][0] = str[0][2];
			str[2][1] = str[1][2];
			
		}	
		
						   
		return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:     PrecipitateForce
 *      Description:  Compute the nodal forces on a given segment due to
 *					  all precipitates in the simulation cell.
 *
 *			Arguments:
 *				MU			Shear modulus
 *				NU			Poisson's ratio
 *				bx,by,bz	Burgers vector of segment with line direction 
 *							pointing from node1 to node2
 *				xi,yi,zi	Coordinates of nodei
 *				fi			Force vector on nodei			
 *
 *------------------------------------------------------------------------*/
	
void PrecipitateForce(Home_t *home, real8 MU, real8 NU,
					  real8 bx, real8 by, real8 bz, 
					  real8 x1, real8 y1, real8 z1, 
					  real8 x2, real8 y2, real8 z2,
					  real8 f1[3], real8 f2[3],
					  int prec1, int prec2)	{
						  
		int 	i, j, interp_type, npoints;
                real8   Lx, Ly, Lz, xp0, yp0, zp0;
                int     nx, ny, nz, Nimgx, Nimgy, Nimgz;
		real8	dx, dy, dz, len, xp, yp, zp;
		real8	dx1, dy1, dz1, dx2, dy2, dz2;
		real8	dist2, dist, xt, dxt, dgrid;
		real8	positions[MAX_NUM_POINTS], weights[MAX_NUM_POINTS];
		real8	pmidx, pmidy, pmidz, pspanx, pspany, pspanz;
		real8	xeval, yeval, zeval;
		real8	vec1[3], vec2[3];
		real8	U[3][3], tmpMat[3][3], sigmaloc[3][3]; 
		real8	sigma[3][3], sigbx, sigby, sigbz;
		real8	fLinvx, fLinvy, fLinvz;
		real8	temp, mult1, mult2;
		Prec_t	*prec;
		Precstress_t *precstress;
		
/*
 *		Initialize force vectors to zero.
 */
		f1[X] = 0.0; f1[Y] = 0.0; f1[Z] = 0.0; 
		f2[X] = 0.0; f2[Y] = 0.0; f2[Z] = 0.0;

/*
 *		Return now if we are not using precipitate misfit fields.
 */
		if (home->param->precMisfit == 0) return; 
		
/*
 *		Grab the quadratue points and coefficients for integrating 
 *		along the segment. We hard code the use of NUM_POINTS quadrature  
 *		points.
 */
//		GaussQuadCoeff(NUM_POINTS, positions, weights);

/*
 *		Precompute a few things...
 */		
		//node 2 is already PBCPOSITIONed with respect to node 1
		dx = x2-x1;
		dy = y2-y1;
		dz = z2-z1;
		
		len = sqrt(dx*dx + dy*dy + dz*dz);
		
        pmidx  = 0.5 * (x2 + x1);
        pmidy  = 0.5 * (y2 + y1);
        pmidz  = 0.5 * (z2 + z1);

        pspanx = 0.5 * dx;
        pspany = 0.5 * dy;
        pspanz = 0.5 * dz;
				
/*
 *		Loop over all precipitates and compute forces.
 */
		for (j = 0; j < home->newPrecKeyPtr; j++) {

			prec = home->precKeys[j];
			
			precstress = prec->precstress;

			if (prec == (Prec_t *)NULL) continue;

			if (prec->misfit == 0) continue;

			xp0 = prec->x; yp0 = prec->y; zp0 = prec->z;
			
			//node 2 is already PBCPOSITIONed with respect to node 1
			PBCPOSITION(home->param, x1, y1, z1, &xp0, &yp0, &zp0);

/*
 *			Choose number of images based on cut-off radius.
 */
			Lx = home->param->maxSideX-home->param->minSideX;
                        Ly = home->param->maxSideY-home->param->minSideY;
                        Lz = home->param->maxSideZ-home->param->minSideZ;

			if (home->param->xBoundType == Periodic) {
				Nimgx = (int) ceil(prec->rcut/Lx);
			} else {
                                Nimgx = 0;
			}

                        if (home->param->yBoundType == Periodic) {
                                Nimgy = (int) ceil(prec->rcut/Ly);
                        } else {
                                Nimgy = 0;
                        }

                        if (home->param->zBoundType == Periodic) {
                                Nimgz = (int) ceil(prec->rcut/Lz);
                        } else {
                                Nimgz = 0;
                        }
                        
                 for (nx = -Nimgx; nx <= Nimgx; nx++) {
                  for (ny = -Nimgy; ny <= Nimgy; ny++) {
                   for (nz = -Nimgz; nz <= Nimgz; nz++) {

			xp = xp0 + ((real8) nx)*Lx;
                        yp = yp0 + ((real8) ny)*Ly;
                        zp = zp0 + ((real8) nz)*Lz;
			
/* 
 *			Choose the number of Guass points based on the grid spacing
 *			at the closest node.
 */
			dx1 = x1-xp;
			dy1 = y1-yp;
			dz1 = z1-zp;
			
			dx2 = x2-xp;
			dy2 = y2-yp;
			dz2 = z2-zp;
			
			dist2 = MIN(dx1*dx1+dy1*dy1+dz1*dz1,dx2*dx2+dy2*dy2+dz2*dz2);
			dist = sqrt(dist2);
			
			xt = 2.0/M_PI*atan(dist/(precstress->alpha*prec->r1));  
			dxt = 2.0/precstress->Ngrid;	
			dgrid = precstress->alpha*prec->r1*tan(M_PI/2.0*(xt+dxt))-dist; 		
			
			npoints = (int) round(len/dgrid);
			npoints = MAX(MIN(npoints,MAX_NUM_POINTS),MIN_NUM_POINTS);
			
			GaussQuadCoeff(npoints, positions, weights);
						
/*
 *			Loop over the Gauss points and sum up their force
 *			contributions.
 */
	        for (i = 0; i < npoints; i++) {

	            xeval = pmidx+pspanx*positions[i]-xp;
	            yeval = pmidy+pspany*positions[i]-yp;
	            zeval = pmidz+pspanz*positions[i]-zp;
				
/*
 *				For points inside the inclusion, we set the stress state to
 *				zero. Though the stress state inside an Eshelby inclusion
 *				is actually nonzero, we assume that the influence of the
 *				internal stress state is accounted for through the
 *				precipitate cutting model (PrecipitateRelease).
 *				
 *				If both nodes are pinned on this precipitate, however, there
 *				should be a force exerted by the misfit field.
 */
				if(InsideEllipsoid(xeval, yeval, zeval, 0.0, 0.0, 0.0, prec,
					(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL)
				   && (prec1 != prec->myTag.index || prec2 != prec->myTag.index))
						continue; 
				
/*
 *				Evaluate the stress field using an analytical solution
 *				or interpolate the pretabulated field.
 */ 
				if (prec->sphere && prec->analytic_stress) {
					//Dilatational sphere case
					DilatSphereStress(xeval, yeval, zeval, prec->dV, MU, NU, sigma);
				} else {
					//Numerical stress field case					
					//Check if the point falls within the cut-off radius
					if (sqrt(xeval*xeval + yeval*yeval + zeval*zeval)>=prec->rcut) 
						continue;
						
					//Rotate the point to be in the precipitate's local coordinates
					vec1[X] = xeval; vec1[Y] = yeval; vec1[Z] = zeval;
					Matrix33Vector3Multiply(prec->UT, vec1, vec2);
					
					//Test to see if we will use linear or nearest neighbor interpolation.
					interp_type = (prec1 == prec->myTag.index && prec2 == prec->myTag.index);
					
					//Evaluate the stress tensor in local coordinates
					InterpPrecStress(interp_type, prec, vec2[X], vec2[Y], vec2[Z], sigmaloc);
					
					//Rotate the stress state into global coordinates
					Matrix33Transpose(prec->UT,U);	   	   
					Matrix33Mult33(U,sigmaloc,tmpMat);
					Matrix33Mult33(tmpMat,prec->UT,sigma);
					
				}


                sigbx = sigma[0][0]*bx +
                         sigma[0][1]*by +
                         sigma[0][2]*bz;

                sigby = sigma[1][0]*bx +
                         sigma[1][1]*by +
                         sigma[1][2]*bz;

                sigbz = sigma[2][0]*bx +
                         sigma[2][1]*by +
                         sigma[2][2]*bz;

	            fLinvx = (sigby*pspanz-sigbz*pspany);
	            fLinvy = (sigbz*pspanx-sigbx*pspanz);
	            fLinvz = (sigbx*pspany-sigby*pspanx);

	            temp = weights[i]*positions[i];
	            mult1 = weights[i]+temp;

	            f2[0] += fLinvx*mult1;
	            f2[1] += fLinvy*mult1;
	            f2[2] += fLinvz*mult1;

	            mult2 = weights[i]-temp;

	            f1[0] += fLinvx*mult2;
	            f1[1] += fLinvy*mult2;
	            f1[2] += fLinvz*mult2;    
	        }
		} //nx
		} //ny
		} //nz	
		}
	
		return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:     SetOneNodePrecForce
 *      Description:  Compute the force due to a given precipitate's misfit
 *					  field on a given node. 
 *
 *			Arguments:
 *				home	point to home structure
 *				node	pointer to node structure whose force is desired
 *				prec	pointer to precipitate structure whose misfit is
 *						the source of the force
 *				f		vector to return the force on node
 *				
 *
 *------------------------------------------------------------------------*/

void SetOneNodePrecForce(Home_t *home, Node_t *node, Prec_t *prec, real8 f[3]) {
	
	
		int		j;
		real8	x1, y1, z1, x2, y2, z2;
		real8	bx1, by1, bz1, MU, NU;
		real8	f1[3], f2[3];
		Node_t	*nbr;
		Param_t	*param;
		Prec_t 	*prec2;
	
		param = home->param;
/*
 *		If the given precipitate has no misfit, the force is zero.
 */
		if (prec->misfit==0 || home->param->precMisfit==0) {
			f[X] = 0.0;	f[Y] = 0.0;	f[Z] = 0.0;
			return;
		}
		
		int *misfit_array = malloc(sizeof(int) * home->newPrecKeyPtr);
	
/*
 *		Loop over all other precipitates and disable misfit fields for
 *		all but the given precipitate.
 */
		for (j = 0; j < home->newPrecKeyPtr; j++) {

			prec2 = home->precKeys[j];

			if (prec2 == (Prec_t *)NULL) continue;
			
			misfit_array[j] = prec2->misfit;
			
			if ((prec2->myTag.index != prec->myTag.index) && 
				(prec2->myTag.domainID != prec->myTag.domainID))
				prec2->misfit = 0;
		}	
	
	
/*
 *		Loop over all arms of the given node, compute the force on each
 *		arm, and their contributions to the nodal force vector.
 */
		f[X] = 0.0;	f[Y] = 0.0;	f[Z] = 0.0;
		
		x1 = node->x;
		y1 = node->y;
		z1 = node->z;
		
		MU = param->shearModulus;
		NU = param->pois;
		
		for (j = 0; j < node->numNbrs; j++) {
			
			nbr = GetNeighborNode(home,node,j);	
			
			x2 = nbr->x;
			y2 = nbr->y;
			z2 = nbr->z;
			
			PBCPOSITION(param,x1,y1,z2,&x2,&y2,&z2);
			
            bx1 = node->burgX[j];
            by1 = node->burgY[j];
            bz1 = node->burgZ[j];
			
			PrecipitateForce(home, MU, NU, bx1, by1, bz1, 
							 x1, y1, z1, x2, y2, z2, f1, f2,
							 -1, -1);
			
            VECTOR_ADD(f, f1);
		}
	
	
/*
 *		Reset all of the misfit flags.
 */

		for (j = 0; j < home->newPrecKeyPtr; j++) {

			prec2 = home->precKeys[j];

			if (prec2 == (Prec_t *)NULL) continue;
	
			prec2->misfit = misfit_array[j];
		}	
		
		free(misfit_array);
		
		return;	
	
}

	
#endif	
	
	
	
	
	
	
	
