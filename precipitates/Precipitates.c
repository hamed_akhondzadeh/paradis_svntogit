/*-------------------------------------------------------------------------
 *
 *      Function:     Precipitates
 *      Description:  Driver function for handling precipitate collisions
 *                    and release of nodes from precipitates.
 *
 *-----------------------------------------------------------------------*/

#ifdef _PREC

#include "Home.h"

int ChangePrecipitate(Home_t *home, Node_t *node, int precidx, int globalOp) {

        if ((node->myTag.domainID != home->myDomain) && globalOp) 
            return(PREC_FAILED);

        node->prec = precidx;
        if (precidx>=0) {
            node->constraint = PINNED_NODE;
        } else {
            node->constraint &= ~PINNED_NODE;
		}	

        if (globalOp) {
            AddOp(home, CHANGE_PREC,
                  node->myTag.domainID,
                  node->myTag.index,
                  precidx, -1,            /* used 2nd tag for precidx */
                  -1, -1,                 /* 3rd node tag unneeded */
                  0.0, 0.0, 0.0,          /* burgers vector unneeded */
                  0.0, 0.0, 0.0,          /* position not needed */
                  0.0, 0.0, 0.0);         /* plane normal not needed */
        }       

#ifdef DEBUG_PREC
        printf("Precipitate changed by domain %d to %d"
               " for node %d,%d at %e %e %e.\n",
               home->myDomain, precidx, node->myTag.domainID, 
               node->myTag.index, node->x, node->y, node->z);
#endif

        return(PREC_SUCCESS);
}

void Precipitates(Home_t *home) {
	
	    int     i;
        Node_t  *node;

/*
 *		Handle collisions between dislocation segments and precipitates.
 */		
		PrecipitateCollisions(home);

/*
 *		Remesh segments pinned at the surface of precipitates.
 */	
		PrecipitateRemesh(home);

/*
 *      Before releasing nodes, message pass the topological changes made
 *      above. This is important because some nodes may be exempted from
 *      release because they need to have their forces reset.
 */

        TimerStart(home, SEND_REMESH);
        CommSendRemesh(home);
        TimerStop(home, SEND_REMESH);

        TimerStart(home, FIX_REMESH);
        FixRemesh(home);
        TimerStop(home, FIX_REMESH);

        for (i = 0; i < home->newNodeKeyPtr; i++) {
            if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
            (void)RemoveDoubleLinks(home, node, 0);
            node->flags &= ~NODE_CHK_DBL_LINK;
        }

        RemoveOrphanedNodes(home);

        ClearOpList(home);
        InitTopologyExemptions(home);
	
/*
 *		Release nodes from being pinned at the surface of precipitates,
 *		due to cutting events or moving away from the surface.
 */	
		PrecipitateRelease(home);

//printf("Domain %i done with precipitate handling...\n",home->myDomain);

    return;
}

#endif
