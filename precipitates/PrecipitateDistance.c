/*****************************************************************************
 *
 *	Module:		PrecipitateDistance.c
 *	Description:	This module contains various functions used
 *			for determining the minimum distance or intersection points 
 *			between precipitates and dislocation segments.
 *
 *          These functions are mostly used for collision detection.
 *
 *
 *	Included functions:
 *              GetMinDistSphere
 *              GetMinDistEllipsoid
 *              InsideEllipsoid
 *				EllipsoidIntersect
 *
 *****************************************************************************/
#ifdef _PREC

#include "Home.h"

/*-------------------------------------------------------------------------
 *
 *      Function:     GetMinDistSphere
 *      Description:  Find the minimum distance between the surface of a 
 *                    sphere and a dislocation segment.
 *
 *                    x1, y1, z1 = coordinates of node 1
 *                    x2, y2, z2 = coordinates of node 2
 *                    xs, ys, zs = coordinates of sphere's center
 *                    R =  sphere radius
 *                    dist = pointer to distance output
 *                    L = pointer to relative position on segment of 
 *                        closest point
 *
 *					  Convention is that if the segment is inside the
 *					  precipitate, the minimum distance is negative. 
 *
 *-----------------------------------------------------------------------*/

void GetMinDistSphere(real8 x1, real8 y1, real8 z1, 
                      real8 x2, real8 y2, real8 z2,
                      real8 xs, real8 ys, real8 zs, 
                      real8 R, real8 *dist, real8 *L) {

    real8   xmin, ymin, zmin;
    real8   dist1, dist2;
    real8   A, B, C;

    dist1 = sqrt((x1-xs)*(x1-xs) + (y1-ys)*(y1-ys) + (z1-zs)*(z1-zs)) - R;
    dist2 = sqrt((x2-xs)*(x2-xs) + (y2-ys)*(y2-ys) + (z2-zs)*(z2-zs)) - R;

    if (dist1>0.0 && dist2>0.0) {
/*
 *      Both points outside the sphere.
 */
        *L = ((x1 - x2)*(x1 - xs) + (y1 - y2)*(y1 - ys) +
            (z1 - z2)*(z1 - zs))/((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) + 
            (z1 - z2)*(z1 - z2));
        *L = MAX(MIN(*L, 1.0), 0.0);

        xmin = (1.0-*L)*x1 + *L*x2;
        ymin = (1.0-*L)*y1 + *L*y2;
        zmin = (1.0-*L)*z1 + *L*z2;

        *dist = sqrt((xmin-xs)*(xmin-xs) + (ymin-ys)*(ymin-ys) + 
                          (zmin-zs)*(zmin-zs)) - R;
		
		//if dist<0, the segment crosses the sphere surface twice
	    if (*dist<=0.0) {
	        *dist = 0.0;
	        *L = 0.5;
	    }
    } else if (dist1<0.0 && dist2<0.0) {
/*
 *      Both points inside the sphere.
 */
        if (fabs(dist1)<fabs(dist2)) {
            *dist = dist1;
            *L = 0.0;
        } else {
            *dist = dist2;
            *L = 1.0;
        }
    } else {
/*
 *      One point inside and one point outside.
 */
        A = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2);
        B = -2.0*((x1 - x2)*(x1 - xs) + (y1 - y2)*(y1 - ys) + 
                    (z1 - z2)*(z1 - zs));
        C = (x1-xs)*(x1-xs) + (y1-ys)*(y1-ys) + (z1-zs)*(z1-zs) - R*R;
        *L = (-B+sqrt(B*B-4.0*A*C))/(2.0*A);
        if (*L<0.0 || *L>1.0) { 
            *L = (-B-sqrt(B*B-4.0*A*C))/(2.0*A);
        }
        *dist = 0.0;
    }

    return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     InsideEllipsoid
 *      Description:  Determine if the given point is inside the given
 *					  ellipsoid. Also, return the distance to the point
 *					  on the ellipsoid where the ray going from the given
 *					  point to the center crosses the ellipsoid surface, 
 *					  and the coordinates of that intersection point.
 *
 *					  Convention is that the distance is negative if the
 *					  point is inside the ellipsoid.
 *
 *                    x, y, z = coordinates of point
 *                    xp, yp, zp = coordinates of ellipsoid's center
 *                    prec = pointer to precipitate structure
 *                    dist = pointer to distance output
 *
 *-----------------------------------------------------------------------*/

int InsideEllipsoid(real8 x, real8 y, real8 z, 
					real8 xp, real8 yp, real8 zp, Prec_t *prec,
					real8 *dist, real8 *xint, real8 *yint, real8 *zint) {
						
		int		loc;
		real8	len;
		real8	s, pmc[3], LTpmc[3], eps = 1e-12;
							
		pmc[X] = x-xp;
		pmc[Y] = y-yp;
		pmc[Z] = z-zp;
			
		//Make sure the point isn't at the center
		if (DotProduct(pmc,pmc)<eps) {
			if ((dist==(real8 *)NULL)==0) *dist = MIN(MIN(prec->r1,prec->r2),prec->r3);
			
			if ((xint==(real8 *)NULL)==0) *xint = xp;
			if ((yint==(real8 *)NULL)==0) *yint = yp;
			if ((zint==(real8 *)NULL)==0) *zint = zp;
			
			return(1);
		}
		
/*
*		Check if its a sphere first.
*/
		if (prec->sphere) {			
			len = sqrt(DotProduct(pmc,pmc));
			loc = (len<=prec->r1);
			if ((dist==(real8 *)NULL)==0) *dist = len - prec->r1;
			
			if ((xint==(real8 *)NULL)==0) *xint = prec->r1/len*pmc[X]+xp;
			if ((yint==(real8 *)NULL)==0) *yint = prec->r1/len*pmc[Y]+yp;
			if ((zint==(real8 *)NULL)==0) *zint = prec->r1/len*pmc[Z]+zp;
		} else {
			Matrix33Vector3Multiply(prec->LT, pmc, LTpmc);
		
			s = 1.0/sqrt(DotProduct(LTpmc,LTpmc));

			loc = (s>=1.0);

			if ((dist==(real8 *)NULL)==0) {
				if (loc==1) { 
					*dist = -(1.0-s)*sqrt(DotProduct(pmc,pmc));
				} else {
					*dist = (1.0-s)*sqrt(DotProduct(pmc,pmc));
				}
			}
		
			if ((xint==(real8 *)NULL)==0) *xint = s*pmc[X]+xp;
			if ((yint==(real8 *)NULL)==0) *yint = s*pmc[Y]+yp;
			if ((zint==(real8 *)NULL)==0) *zint = s*pmc[Z]+zp;
		}
		
		return(loc);					
}

/*-------------------------------------------------------------------------
 *
 *      Function:     EllipsoidIntersect
 *      Description:  Determine the point along a line where it 
 *					  intersects the surface of an ellipsoid.
 *
 *                    Note that usually there are multiple solutions, and
 *                    the convention is to return the intersection point
 *                    closest to point 1 along the direction pointing from
 *                    point 1 to point 2.
 *
 *                    Returns a flag indicating whether a solution could
 *                    be found: 1 = yes, 0 = no.
 *
 *                    x1, y1, z1, x2, y2, z2 = coordinates of points
 *							defining the line segment
 *                    xp, yp, zp = coordinates of ellipsoid's center
 *                    prec = pointer to precipitate structure
 *                    L = pointer to position along the line
 *
 *-----------------------------------------------------------------------*/

int EllipsoidIntersect(real8 x1, real8 y1, real8 z1, 
    				   real8 x2, real8 y2, real8 z2, 
					   real8 xp, real8 yp, real8 zp, 
                       Prec_t *prec, real8 *L) {
	
		real8	dx12, dy12, dz12, dx1p, dy1p, dz1p;
		real8	vec1[3], vec2[3], vec12[3], vecp[3], vec1p[3];
		real8	A1[3], A2[3], Ap[3], A12[3];
		real8	A11, a, b, c, disc, sqrtdisc, a2;
        real8   L1, L2;
		real8	eps = 1e-8, eps2 = 1e-20;
		
/*
 *      Compute the coefficients of the quadratic expression.
 *		If its a sphere, the solution is simpler.
 */
		
		if (prec->sphere) {
			
            dx12 = x1-x2;
            dy12 = y1-y2;
            dz12 = z1-z2;
            dx1p = x1-xp;
            dy1p = y1-yp;
            dz1p = z1-zp;

            a = dx12*dx12 + dy12*dy12 + dz12*dz12;
            b = -2.0*(dx12*dx1p + dy12*dy1p + dz12*dz1p);
            c = dx1p*dx1p + dy1p*dy1p + dz1p*dz1p - prec->r1*prec->r1;

//            a = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2);
//            b = -2.0*((x1 - x2)*(x1 - xp) + (y1 - y2)*(y1 - yp) + 
//                        (z1 - z2)*(z1 - zp));
//            c = (x1-xp)*(x1-xp) + (y1-yp)*(y1-yp) + (z1-zp)*(z1-zp)
//                        - prec->r1*prec->r1;
			
		} else {
	
/*
 *			Assemble necessary vectors.
 */
			vec1[X] = x1;
			vec1[Y] = y1;
			vec1[Z] = z1;
	
			vec2[X] = x2;
			vec2[Y] = y2;
			vec2[Z] = z2;
	
			vec12[X] = x1 - x2;
			vec12[Y] = y1 - y2;
			vec12[Z] = z1 - z2;
	
			vecp[X] = xp;
			vecp[Y] = yp;
			vecp[Z] = zp;
	
			vec1p[X] = x1 - xp;
			vec1p[Y] = y1 - yp;
			vec1p[Z] = z1 - zp;
		
/*
 *			Precompute terms so all that's left is dot products.
 */
	
			Matrix33Vector3Multiply(prec->A, vec1, A1);
	
			Matrix33Vector3Multiply(prec->A, vec2, A2);
	
			Matrix33Vector3Multiply(prec->A, vecp, Ap);
	
			Matrix33Vector3Multiply(prec->A, vec12, A12);
	
			A11 = DotProduct(vec1,A1);
	
/*
 *			Evaluate coefficients of the quadratic equation.
 */	
			a =  A11 - 2.0*DotProduct(vec1,A2) + DotProduct(vec2,A2);
		
			b = -2.0*DotProduct(vec1p,A12);
	
			c = A11 - 2.0*DotProduct(vecp,A1) + DotProduct(vecp,Ap) - 1.0;
		}

/*
 *		Evaluate the quadratic equation and make sure we select the
 *		correct root. Also ensure the segment crosses the ellipsoid's
 *		surface. Prevent division by zero and allow for some roundoff error.
 */	
		a2 = 2.0*a;
		disc = b*b - 4.0*a*c;
		if (fabs(a2)<eps2) {
			printf("WARNING: Division by zero with precipitate"
				  " %d,%d surface in EllipsoidIntersect because a=%e.\n",
					prec->myTag.index, prec->myTag.domainID,a);
			*L = 0.5;
			return(0);
		}
		if (fabs(disc)<eps2) {
			disc = 0.0;
		}	
		if (disc<0.0) {
			printf("WARNING: Line segment does not cross precipitate"
				  " %d,%d surface in EllipsoidIntersect because disc=%e.\n",
					prec->myTag.index, prec->myTag.domainID,disc);
			*L = 0.5;
			return(0);
		}
		sqrtdisc = sqrt(disc); 
		L1 = (-b + sqrtdisc)/a2;
	    L2 = (-b - sqrtdisc)/a2;

/*
 *      Allow for some roundoff error...
 */
        if (fabs(L1) < eps) {
			L1 = 0.0;
		}
        if (fabs(L2) < eps) {
			L2 = 0.0;
		}

/*
 *      Choose the positive solution that is closest to point 1.
 */
        if (L1 < 0.0 && L2 < 0.0) {
            printf("WARNING: Line segment does not cross precipitate"
				  " %d,%d surface in EllipsoidIntersect because L1=%e and L2=%e.\n",
					prec->myTag.index, prec->myTag.domainID,L1,L2);
            *L = 0.5;
            return(0);
        } else if (L1 < 0.0) {
            *L = L2;
        } else if (L2 < 0.0) {
            *L = L1;
        } else {
            *L = MIN(L1,L2);
        }         
		
		return(1);

}


/*-------------------------------------------------------------------------
 *
 *      Function:     EllipsoidProject
 *      Description:  Project the given point onto the given ellipsoid's
 *					  surface along the given direction.
 *
 *                    We assume that we need to project onto the closest 
 *                    surface of the ellipsoid.
 *
 *                    Returns a flag indicating whether a solution could
 *                    be found: 1 = yes, 0 = no.                   
 *
 *                    x, y, z = coordinates of point
 *					  xdir, ydir, zdir = direction of projection
 *                    xp, yp, zp = coordinates of ellipsoid's center
 *                    prec = pointer to precipitate data structure
 *
 *-----------------------------------------------------------------------*/

int EllipsoidProject(real8 *x, real8 *y, real8 *z,
					  real8 xdir, real8 ydir, real8 zdir,
					  real8 xp, real8 yp, real8 zp, Prec_t *prec) {
		
		int		loc, loc2, iter, itermax, Success;
		real8	rmp[3], np[3];
		real8	shiftdir, dirsgn, dirlen, dr, x2, y2, z2, L;
/*
 *		Determine if the given point is inside the ellipsoid.
 */						  					  	
		loc = InsideEllipsoid(*x, *y, *z, xp, yp, zp, prec,
						(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL);

/*
 *		If the point is inside, the shift direction should be along the outward normal.
 *		If outside, it should be inward.
 */
		rmp[X] = *x - xp;
		rmp[Y] = *y - yp;
		rmp[Z] = *z - zp;

		if (prec->sphere) {
			VECTOR_COPY(np,rmp);
		} else {
			Matrix33Vector3Multiply(prec->A, rmp, np);
		}
		
		dirsgn = (real8) Sign(np[X]*xdir + np[Y]*ydir + np[Z]*zdir);
		if (loc==1) {
			//do nothing, the sign is correct
		} else {
			dirsgn *= -1.0;
		}
		
/*
 *		Make sure the direction vector is pointing in the direction we're 
 *		shifting, and compute the unit vector.
 */		
		dirlen = sqrt(xdir*xdir + ydir*ydir + zdir*zdir);
		xdir = dirsgn*xdir/dirlen;	
		ydir = dirsgn*ydir/dirlen;	
		zdir = dirsgn*zdir/dirlen;
		
/*
 *		Incrementally shift until we find a point with location opposite the
 *		given point. The shift length dr is set to a small fraction of the 
 *		smallest radius, trying to make sure we don't jump across the 
 *		precipitate without entering or exiting it.
 */		
/*
		dr = 0.001*MIN(MIN(prec->r1,prec->r2),prec->r3);
		loc2 = loc;
		x2 = *x; y2 = *y; z2 = *z;
		iter = 0; itermax = 10000;
		while (loc2==loc) {
			x2 += dr*xdir;
			y2 += dr*ydir;
			z2 += dr*zdir;
			loc2 = InsideEllipsoid(x2, y2, z2, xp, yp, zp, prec,
							(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL);
			iter++;
			if (iter>itermax) {
				printf("WARNING: EllipsoidProject failed to find an intersection point "
					"near (%e,%e,%e) along (%e,%e,%e) with precipitate %d.\n",*x,*y,*z,
					xdir,ydir,zdir,prec->myTag.index);
					printf("%e %e %e\n",xp,yp,zp);
				return(0);
			}		
		}
*/		
/*
 *		We now have a set of points along dir that contain the surface of the
 *		ellipsoid. All we need to do is find where their connecting line segment
 *		intersects the surface.
 */
        x2 = *x + xdir;
        y2 = *y + ydir;
        z2 = *z + zdir;

		Success = EllipsoidIntersect(*x, *y, *z, x2, y2, z2, xp, yp, zp, prec, &L);
		
        if (Success) {
		    *x = *x*(1.0-L) + L*x2;
		    *y = *y*(1.0-L) + L*y2;
		    *z = *z*(1.0-L) + L*z2;	
        }	
			
		return(Success);
  }


/*-------------------------------------------------------------------------
 *
 *      Function:     MinDistPointEllipsoid
 *      Description:  Find the minimum distance between the surface of an 
 *                    ellipsoid and a point.
 *
 *                    x, y, z = coordinates of point
 *                    xp, yp, zp = coordinates of ellipsoid's center
 *                    prec = pointer to precipitate data structure
 *                    dist = pointer to distance output
 *
 *-----------------------------------------------------------------------*/

void MinDistPointEllipsoid(real8 x, real8 y, real8 z, 
						   real8 xp, real8 yp, real8 zp,
	                       Prec_t *prec, real8 *dist) {
							   
	    int		iter, maxiter, InvOK;
	    real8	tol, err, eps = 1e-12;
		real8	s, distguess, lambda;					
		real8	cmp[3], a[3], tmpvec3[3], tmpvec4[4], tmpvec42[4]; 
		real8 	guess[3], pguess[3], dguess[3];
		real8	ymin[3], ymin2[3], rhsvec[4], mres[3];
		real8	Linv[3][3], Amat[4][4], J[4][4], Jinv[4][4];
	
/*
 *		Do some initial stuff.
 */	

		Matrix33Transpose(prec->LTinv, Linv);
		
		//Matrix33Transpose(prec->Ainv, ATinv);
		//printf("%e %e %e\n%e %e %e\n%e %e %e\n",prec->Lmult[0][0],prec->Lmult[0][1],prec->Lmult[0][2],prec->Lmult[1][0],prec->Lmult[1][1],prec->Lmult[1][2],prec->Lmult[2][0],prec->Lmult[2][1],prec->Lmult[2][2]);
		cmp[X] = xp - x;
		cmp[Y] = yp - y;
		cmp[Z] = zp - z;
		
		//Make sure the point isn't at the center
		if (DotProduct(cmp,cmp)<eps) {
			*dist = MIN(MIN(prec->r1,prec->r2),prec->r3);
			return;
		}
	
		Matrix33Vector3Multiply(Linv, cmp, a);
	
/*
 *		Compute the initial guess for the Newton iteration.
 *		This is the point of intersection with the ellipsoid's
 *		surface of the ray going from the center to the point.
 *		Also compute the separation distance for the guess, to 
 *		ensure our new solution improves upon it. 
 */		
	
		Matrix33Vector3Multiply(prec->LT, cmp, tmpvec3);
	
		s = 1.0/sqrt(DotProduct(tmpvec3,tmpvec3));
	
		guess[X] = -s*tmpvec3[X];
		guess[Y] = -s*tmpvec3[Y];
		guess[Z] = -s*tmpvec3[Z];
	
		ymin[X] = guess[X];
		ymin[Y] = guess[Y];
		ymin[Z] = guess[Z];
	
		Matrix33Vector3Multiply(prec->LTinv, guess, tmpvec3);
		pguess[X] = xp + tmpvec3[X];
		pguess[Y] = yp + tmpvec3[Y];
		pguess[Z] = zp + tmpvec3[Z];
		
		dguess[X] = pguess[X] - x;
		dguess[Y] = pguess[Y] - y;
		dguess[Z] = pguess[Z] - z;
		
		distguess = sqrt(DotProduct(dguess,dguess));
		//printf("s=%e distguess=%e \n",s,distguess);
		
/*		
 *		Use Newton's method with a Lagrange multiplier to find
 *		the point on the ellipsoid that minimizes the distance
 *		to (x,y,z).
 */
		lambda = 0.0;
		tol = 1e-9;
		iter = 1;
		maxiter = 30;
		Amat[0][0] = prec->Lmult[0][0]; Amat[0][1] = prec->Lmult[0][1]; Amat[0][2] = prec->Lmult[0][2];
		Amat[1][0] = prec->Lmult[1][0]; Amat[1][1] = prec->Lmult[1][1]; Amat[1][2] = prec->Lmult[1][2];
		Amat[2][0] = prec->Lmult[2][0]; Amat[2][1] = prec->Lmult[2][1]; Amat[2][2] = prec->Lmult[2][2];
		Amat[3][3] = 0.0;
		rhsvec[0] = a[X]; rhsvec[1] = a[Y]; rhsvec[2] = a[Z];
		rhsvec[3] = -1.0;
		                       J[0][1] = prec->Lmult[0][1]; J[0][2] = prec->Lmult[0][2];
		J[1][0] = prec->Lmult[1][0];                        J[1][2] = prec->Lmult[1][2];
		J[2][0] = prec->Lmult[2][0]; J[2][1] = prec->Lmult[2][1]; 
		J[3][3] = 0.0;
		while (1) {
			//Update the system of equations matrix
			ymin2[X] = 2.0*ymin[X]; ymin2[Y] = 2.0*ymin[Y]; ymin2[Z] = 2.0*ymin[Z];
			Amat[0][3] = -ymin2[X]; Amat[1][3] = -ymin2[Y]; Amat[2][3] = -ymin2[Z];
			Amat[3][0] = ymin[X];   Amat[3][1] = ymin[Y];   Amat[3][2] = ymin[Z];
			//printf("%e %e %e %e\n",Amat[0][0],Amat[0][1],Amat[0][2],Amat[0][3]);
			//printf("%e %e %e %e\n",Amat[1][0],Amat[1][1],Amat[1][2],Amat[1][3]);
			//printf("%e %e %e %e\n",Amat[2][0],Amat[2][1],Amat[2][2],Amat[2][3]);
			//printf("%e %e %e %e\n",Amat[3][0],Amat[3][1],Amat[3][2],Amat[3][3]);
			//printf("%e %e %e %e\n",rhsvec[0],rhsvec[1],rhsvec[2],rhsvec[3]);
			//Update the residual vector
			tmpvec4[0] = ymin[0]; tmpvec4[1] = ymin[1]; tmpvec4[2] = ymin[2];
			tmpvec4[3] = lambda;
	        MatrixMult((real8 *)Amat, 4, 4, 4, (real8 *)tmpvec4, 1, 1, (real8 *)tmpvec42, 1);
            //MatrixMult(&Amat[0][0], 4, 4, 4, &tmpvec4[0], 1, 1, &tmpvec42[0], 1);
			mres[0] = -tmpvec42[0] - rhsvec[0];
			mres[1] = -tmpvec42[1] - rhsvec[1];
			mres[2] = -tmpvec42[2] - rhsvec[2];
			mres[3] = -tmpvec42[3] - rhsvec[3];
			//printf("%e %e %e %e\n",tmpvec42[0],tmpvec42[1],tmpvec42[2],tmpvec42[3]);
			
			//Compute the error and check for convergence
			err = sqrt(mres[0]*mres[0]+mres[1]*mres[1]+mres[2]*mres[2]+mres[3]*mres[3]);
			//("iter=%d err=%e\n",iter,err);
			if (err<tol || iter>maxiter) break;
			
			//Update the Jacobian
			J[0][0] = prec->Lmult[0][0]-2.0*lambda;
			J[1][1] = prec->Lmult[1][1]-2.0*lambda;
			J[2][2] = prec->Lmult[2][2]-2.0*lambda;
			J[0][3] = -ymin2[X];  J[1][3] = -ymin2[Y];  J[2][3] = -ymin2[Z];
			J[3][0] = ymin2[X];   J[3][1] = ymin2[Y];   J[3][2] = ymin2[Z];
			
			//Solve for the update vector
			InvOK = MatrixInvert((real8 *)J, (real8 *)Jinv, 4, 4);
            //InvOK = MatrixInvert(&J[0][0], &Jinv[0][0], 4, 4);
            //InvOK = Invert4by4(J, Jinv);
			if (InvOK == 0) {
			    printf("%e %e %e %e\n",J[0][0],J[0][1],J[0][2],J[0][3]);
			    printf("%e %e %e %e\n",J[1][0],J[1][1],J[1][2],J[1][3]);
			    printf("%e %e %e %e\n",J[2][0],J[2][1],J[2][2],J[2][3]);
			    printf("%e %e %e %e\n",J[3][0],J[3][1],J[3][2],J[3][3]);
                iter = maxiter + 1;
                break;
            }
	        MatrixMult((real8 *)Jinv, 4, 4, 4, (real8 *)mres, 1, 1, (real8 *)tmpvec4, 1);
            //MatrixMult(&Jinv[0][0], 4, 4, 4, &mres[0], 1, 1, &tmpvec4[0], 1);
			
			//Update the solution and iteration counter, go onto the next iteration
			ymin[0] += tmpvec4[0];
			ymin[1] += tmpvec4[1];
			ymin[2] += tmpvec4[2];
			lambda += tmpvec4[3];
			iter++;			
		}

/*		
 *		Perform a couple of checks to make sure the solution is good. If either the
 *		maximum number of iterations was reached, or the minimum distance to the 
 *		solution point is greater than the initial guess distance, reset the solution
 *		to the initial guess. This is usually not a bad estimate, so all is not lost.
 */
		if (iter>maxiter) {
			*dist = distguess;
		} else {
			Matrix33Vector3Multiply(prec->LTinv, ymin, tmpvec3);
			tmpvec3[X] += xp-x;
			tmpvec3[Y] += yp-y;
			tmpvec3[Z] += zp-z;
			*dist = sqrt(DotProduct(tmpvec3,tmpvec3));
			if (*dist>distguess) {
				*dist = distguess;
			}
		}
		
	
		return;
	
}
	

/*-------------------------------------------------------------------------
 *
 *      Function:     GetMinDistEllipsoid
 *      Description:  Find the minimum distance between the surface of an 
 *                    ellipsoid and a dislocation segment.
 *
 *                    x1, y1, z1 = coordinates of node 1
 *                    x2, y2, z2 = coordinates of node 2
 *                    xp, yp, zp = coordinates of ellipsoid's center
 *					  prec = pointer to precipitate structure
 *                    dist = pointer to distance output
 *                    L = pointer to relative position on segment of 
 *                        closest point
 *
 *					  Convention is that if the segment is inside the
 *					  precipitate, the minimum distance is negative. 
 *
 *-----------------------------------------------------------------------*/

void GetMinDistEllipsoid(real8 x1, real8 y1, real8 z1, 
                         real8 x2, real8 y2, real8 z2,
                         real8 xp, real8 yp, real8 zp,
                         Prec_t *prec, real8 *dist, real8 *L) {
							 
		int		inside1, inside2;
		int		iter, itermax;
		real8	eps = 1e-12, dist1, dist2;
		real8	tol, a, b, c, d, dista, distb, distc, distd;
		real8	xtmp, ytmp, ztmp;
	
		if (prec->sphere) {
		    GetMinDistSphere(x1, y1, z1, 
		                     x2, y2, z2,
		                     xp, yp, zp, 
		                     prec->r1, dist, L);
		} else {
/* 
 *			Do a few simple checks to handle special cases before applying the 
 *			general approach.
 */
			
			inside1 = InsideEllipsoid(x1, y1, z1, xp, yp, zp, prec,
								(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL);
			inside2 = InsideEllipsoid(x2, y2, z2, xp, yp, zp, prec,
								(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL);					
											
/*			
 *			Is this a point?
 */
			if (((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2))<eps) {
				MinDistPointEllipsoid(x1, y1, z1, xp, yp, zp, prec, dist);
				if (inside1) {
					*dist = -*dist;
				}
				*L = 0.5;
				return;
			}
			
/*			
*			Are both points inside the ellipsoid?
*/
			if (inside1 && inside2) {
				MinDistPointEllipsoid(x1, y1, z1, xp, yp, zp, prec, &dist1);
				MinDistPointEllipsoid(x2, y2, z2, xp, yp, zp, prec, &dist2);
				if (dist1<dist2) {
					*dist = -dist1;
					*L = 0.0;
				} else {
					*dist = -dist2;
					*L = 1.0;
				}
				return;
			}
			
/*			
 *			Is just one point inside the ellipsoid?
 */
			if (inside1 || inside2) {
				EllipsoidIntersect(x1,y1,z1,x2,y2,z2,xp,yp,zp,prec,L);
				*dist = 0.0;
				return;
			}
			
/*
 *			General case: Use the golden section search method to minimize
 *			the distance along the segment.
 */
			static real8 phi = 0.618033988749895; //"golden ratio"
			//Initialize with the ends of the segment as the domain boundaries
			a = 0.0;
			b = 1.0;
			MinDistPointEllipsoid(x1, y1, z1, xp, yp, zp, prec, &dista);
			MinDistPointEllipsoid(x2, y2, z2, xp, yp, zp, prec, &distb);
			c = b+phi*(a-b);
			xtmp = (1.0-c)*x1 + c*x2;
			ytmp = (1.0-c)*y1 + c*y2;
			ztmp = (1.0-c)*z1 + c*z2;
			MinDistPointEllipsoid(xtmp, ytmp, ztmp, xp, yp, zp, prec, &distc);
			d = a+phi*(b-a);
			xtmp = (1.0-d)*x1 + d*x2;
			ytmp = (1.0-d)*y1 + d*y2;
			ztmp = (1.0-d)*z1 + d*z2;
			MinDistPointEllipsoid(xtmp, ytmp, ztmp, xp, yp, zp, prec, &distd);
			iter = 1;
			itermax = 1000;
			tol =  1e-9; //sqrt(0.01*prec->rmin);
			while (1) {
		        if (distc<distd) {
		            b = d;
		            distb = distd;
		            d = c;
		            distd = distc;
		            c = b+phi*(a-b);
					xtmp = (1.0-c)*x1 + c*x2;
					ytmp = (1.0-c)*y1 + c*y2;
					ztmp = (1.0-c)*z1 + c*z2;
					MinDistPointEllipsoid(xtmp, ytmp, ztmp, xp, yp, zp, prec, &distc);
		        } else {
		            a = c;
		            dista = distc;
		            distc = distd;
		            c = d;
		            d = a+phi*(b-a);
					xtmp = (1.0-d)*x1 + d*x2;
					ytmp = (1.0-d)*y1 + d*y2;
					ztmp = (1.0-d)*z1 + d*z2;
					MinDistPointEllipsoid(xtmp, ytmp, ztmp, xp, yp, zp, prec, &distd);
		        }
		        if (fabs(b-a)<tol*(fabs(c)+fabs(d)) || iter>itermax) {
		            *L = (a+b)/2.0;
		            *dist = (dista+distb)/2.0;
		            break;
				}
		        iter++;			
			}
			xtmp = (1.0-*L)*x1 + d*x2;
			ytmp = (1.0-*L)*y1 + d*y2;
			ztmp = (1.0-*L)*z1 + d*z2;
			if (InsideEllipsoid(xtmp, ytmp, ztmp, xp, yp, zp, prec,
				(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL))
				*dist *= -1.0;
			
		}
						 
		return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     AABBPointDistance
 *      Description:  Find the minimum distance between an axially-aligned
 *                    bounding box (AABB) and a point.
 *
 *                    This method is based on the pseudocode provided in
 *                    Geometric Tools for Computer Graphics by Schneider and
 *                    Eberly, section 10.9.4. 
 *
 *                    x, y, z = point coordinates
 *                    r1, r2,r3 = AABB dimensions (half widths)
 *                    dist2 = pointer for minimum distance squared
 *                    dist = pointer to distance output
 *                    xb, yb, zb = pointers for coordinates of point on box 
 *
 *-----------------------------------------------------------------------*/

void AABBPointDistance(real8 x, real8 y, real8 z, 
                       real8 r1, real8 r2, real8 r3,
                       real8 *dist2, real8 *xb, real8 *yb, real8 *zb) {

        int     i;
        real8   rads[3], p[3], q[3], d;

        rads[X] = r1;   
        rads[Y] = r2;    
        rads[Z] = r3;
        
        p[X] = x;       
        p[Y] = y;       
        p[Z] = z;

/*
 *      Only coordinates that extend beyond the extent of the AABB contribute to
 *      the separation distance.
 */
        *dist2 = 0.0;
        for (i=0; i<3; i++) {
            if (p[i] < -rads[i]) {
                d = p[i] + rads[i];
                *dist2 += d*d;
                q[i] = -rads[i];
            } else if (p[i] > rads[i]) {
                d = p[i] - rads[i];
                *dist2 += d*d;
                q[i] = rads[i];
            } else {
                q[i] = p[i];
            }
        }

        *xb = q[X];
        *yb = q[Y];
        *zb = q[Z];

        return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     AABBSegDistance2Zero
 *      Description:  Find the minimum distance between an axially-aligned
 *                    bounding box (AABB) and a segment in the case where
 *                    the segment is orthogonal to two of the bounding
 *                    box axes.
 *
 *                    This method is based on the pseudocode provided in
 *                    Geometric Tools for Computer Graphics by Schneider and
 *                    Eberly, section 10.9.4. 
 *
 *                    x0, y0, z0 = segment starting point
 *                    dirx,diry,dirz = vector going from point 1 to point 2
 *                    x/y/zZero = flag indicating orthogonal axes
 *                    r1, r2,r3 = AABB dimensions (half widths)
 *                    dist2 = pointer for minimum distance squared
 *                    L = pointer for normalized distance along segment
 *                    xb, yb, zb = pointers for coordinates of point on box 
 *
 *-----------------------------------------------------------------------*/

void AABBSegDistance2Zero(real8 x0, real8 y0, real8 z0,         
                          real8 dirx, real8 diry,real8 dirz,
                          int xZero, int yZero, int zZero,
                          real8 r1, real8 r2, real8 r3,
                          real8 *dist2, real8 *L, 
                          real8 *xb, real8 *yb, real8 *zb) {

        int     i, j;
        real8   rads[3], p[3], inds[2], q[3];
        real8   sgn, d, ri, pi;

        rads[X] = r1;   
        rads[Y] = r2;    
        rads[Z] = r3;

        p[X] = x0;       
        p[Y] = y0;       
        p[Z] = z0;

        *xb = 0.0;
        *yb = 0.0;
        *zb = 0.0;
        *dist2 = 0.0;

        if (yZero && zZero) {
            inds[0] = 1;
            inds[1] = 2;
            sgn = (real8) Sign(x0);
            *L = (sgn*r1-x0)/dirx;
            q[X] = sgn*r1;
            q[Y] = y0;
            q[Z] = z0;
         } else if (xZero && zZero) {
            inds[0] = 0;
            inds[1] = 2;
            sgn = (real8) Sign(y0);
            *L = (sgn*r2-y0)/diry;
            q[X] = x0;
            q[Y] = sgn*r2;
            q[Z] = z0;
         } else {
            inds[0] = 0;
            inds[1] = 1;
            sgn = (real8) Sign(z0);
            *L = (sgn*r3-z0)/dirz;
            q[X] = x0;
            q[Y] = y0;
            q[Z] = sgn*r3;
        }

        if (*L>=0.0 && *L<=1.0) {
            for (i=0; i<2; i++) {
                j = inds[i];
                ri = rads[j];
                pi = p[j];
                if (pi < -ri) {
                    d = pi+ri;
                    *dist2 += d*d;
                    q[j] = -ri;
                 } else if (pi > ri) {
                    d = pi-ri;
                    *dist2 += d*d;
                    q[j] = ri;
                }
            }
            *xb = q[X];
            *yb = q[Y];
            *zb = q[Z];
        }

        return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     AABBSegDistance1ZeroZ
 *      Description:  Find the minimum distance between an axially-aligned
 *                    bounding box (AABB) and a segment in the case where
 *                    the segment is orthogonal to the z axis.
 *
 *                    This method is based on the pseudocode provided in
 *                    Geometric Tools for Computer Graphics by Schneider and
 *                    Eberly, section 10.9.4. 
 *
 *                    x0, y0, z0 = segment starting point
 *                    dirx,diry,dirz = vector going from point 1 to point 2
 *                    x/y/zZero = flag indicating orthogonal axes
 *                    r1, r2,r3 = AABB dimensions (half widths)
 *                    dist2 = pointer for minimum distance squared
 *                    L = pointer for normalized distance along segment
 *                    xb, yb, zb = pointers for coordinates of point on box 
 *
 *-----------------------------------------------------------------------*/

void AABBSegDistance1ZeroZ(real8 x0, real8 y0, real8 z0,
                           real8 dirx, real8 diry, real8 dirz,
                           real8 r1, real8 r2, real8 r3,
                           real8 *dist2, real8 *L, 
                           real8 *xb, real8 *yb, real8 *zb) {
        
        real8   pMr[2], prod0, prod1, q[3];
        real8   tmp, d, invL2, inv;        

        pMr[X] = x0 - r1;
        pMr[Y] = y0 - r2;
        prod0 = diry*pMr[X];
        prod1 = dirx*pMr[Y];
        q[Z] = z0;

        *xb = 0.0;
        *yb = 0.0;
        *zb = 0.0;
        *dist2 = 0.0;

        if (prod0 >= prod1) {
            //line intersects "x-axis" of OBB
            //Closest point is on the + x face
            q[X] = r1;
            tmp = y0 + r2;
            d = prod0 - dirx*tmp;
            if (d >= 0.0) {
                //There is not intersecting, so compute the distance
                invL2 = 1.0/(dirx*dirx + diry*diry);
                *L = -(dirx*pMr[X] + diry*tmp) * invL2;
                if (*L>=0.0 && *L<=1.0) {
                    *dist2 += d*d*invL2;
                    q[Y] = -r2;
                    *xb = q[X];
                    *yb = q[Y];
                    *zb = q[Z];
                }
            } else {
                //Line intersects box, distance is zero.
                inv = 1/dirx;
                *L = -pMr[X]*inv;
                if (*L>=0.0 && *L<=1.0) {
                    q[Y] = y0 - prod0*inv;
                    *xb = q[X];
                    *yb = q[Y];
                    *zb = q[Z];
                }
            }
        } else {
            //line intersects "y-axis" of OBB
            //Closest point is on the + y face
            q[Y] = r2;
            tmp = x0 + r1;
            d = prod1 - diry*tmp;
            if (d >= 0.0) {
                //There is not intersecting, so compute the distance
                invL2 = 1.0/(dirx*dirx + diry*diry);
                *L = -(diry*pMr[Y] + dirx*tmp) * invL2;
                if (*L>=0.0 && *L<=1.0) {
                    *dist2 += d*d*invL2;
                    q[X] = -r1;
                    *xb = q[X];
                    *yb = q[Y];
                    *zb = q[Z];
                }
            } else {
                //Line intersects box, distance is zero.
                inv = 1.0/diry;
                *L = -pMr[Y]*inv;
                if (*L>=0.0 && *L<=1.0) {
                    q[X] = x0 - prod1*inv;
                    *xb = q[X];
                    *yb = q[Y];
                    *zb = q[Z];
                }
            }
        }

        //Now consider the z direction
        if (z0 < -r3) {
            d = z0 + r3;
            *dist2 += d*d;
            *zb = -r3;
        } else if (z0 > r3) {
            d = z0 - r3;
            *dist2 += d*d;
            *zb = r3;
        }

        return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     AABBSegDistance1Zero
 *      Description:  Find the minimum distance between an axially-aligned
 *                    bounding box (AABB) and a segment in the case where
 *                    the segment is orthogonal to one axis.
 *
 *                    This function is just a wrapper that calls
 *                    AABBSegDistance1ZeroZ according to the orthogonal
 *                    axis.
 *
 *                    This method is based on the pseudocode provided in
 *                    Geometric Tools for Computer Graphics by Schneider and
 *                    Eberly, section 10.9.4. 
 *
 *                    x0, y0, z0 = segment starting point
 *                    dirx,diry,dirz = vector going from point 1 to point 2
 *                    x/y/zZero = flag indicating orthogonal axes
 *                    r1, r2,r3 = AABB dimensions (half widths)
 *                    dist2 = pointer for minimum distance squared
 *                    L = pointer for normalized distance along segment
 *                    xb, yb, zb = pointers for coordinates of point on box 
 *
 *-----------------------------------------------------------------------*/

void AABBSegDistance1Zero(real8 x0, real8 y0, real8 z0, 
                          real8 dirx, real8 diry, real8 dirz, 
                          int xZero, int yZero, int zZero,
                          real8 r1, real8 r2, real8 r3,
                          real8 *dist2, real8 *L, 
                          real8 *xb, real8 *yb, real8 *zb) {

/*
 *      Use the function below that assumes the segment is orthogonal to the z
 *      axis, and if it is actually orthogonal to a different axis, just permute
 *      the info to rotate the coordinate system.
 */

        if (xZero) {
            AABBSegDistance1ZeroZ(y0, z0, x0, diry, dirz, dirx,
                                  r2, r3, r1, dist2, L, yb, zb, xb);
        } else if (yZero) {
            AABBSegDistance1ZeroZ(z0, x0, y0, dirz, dirx, diry,
                                  r3, r1, r2, dist2, L, zb, xb, yb);
        } else if (zZero) {
            AABBSegDistance1ZeroZ(x0, y0, z0, dirx, diry, dirz,
                                  r1, r2, r3, dist2, L, xb, yb, zb);
        } else {
            printf("WARNING: AABBSegDistance1Zero called with all nonzero direction"
                   " vector components: dir=(%e, %e, %e)\n",dirx,diry,dirz); 
        }

        return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     AABBSegDistanceFaceX
 *      Description:  Find the minimum distance between the positive x face
 *                    of an axially-aligned bounding box (AABB) and an 
 *                    arbitrary line segment. The method used ensures that
 *                    segment will either be closest to the bottom or right
 *                    edges, or the face itself.
 *
 *                    This method is based on the pseudocode provided in
 *                    Geometric Tools for Computer Graphics by Schneider and
 *                    Eberly, section 10.9.4. HOWEVER, after testing their
 *                    algorithm, it was found to be flawed and a slightly
 *                    different approach is used.
 *
 *                    x0, y0, z0 = segment starting point
 *                    dirx,diry,dirz = vector going from point 1 to point 2
 *                    r1, r2,r3 = AABB dimensions (half widths)
 *                    dist2 = pointer for minimum distance squared
 *                    L = pointer for normalized distance along segment
 *                    xb, yb, zb = pointers for coordinates of point on box 
 *
 *-----------------------------------------------------------------------*/

void AABBSegDistanceFaceX(real8 x0, real8 y0, real8 z0,
                          real8 dirx, real8 diry, real8 dirz,
                          real8 r1, real8 r2, real8 r3,
                          real8 *dist2, real8 *L, 
                          real8 *xb, real8 *yb, real8 *zb) {

        real8   pPr[3], pMr[3], Lf, yf, zf;
        real8   d, lSqr, inv;
        real8   x1, y1, z1;
        real8   tmp, dist2i, xbi, ybi, zbi, Li;
        real8   Ledge, mLedge;
/*
 *      Initialize output.
 */
        *xb = 0.0;
        *yb = 0.0;
        *zb = 0.0;
        *dist2 = 0.0;

        pPr[X] = x0 + r1;
        pPr[Y] = y0 + r2;
        pPr[Z] = z0 + r3;

        pMr[X] = x0 - r1;
        pMr[Y] = y0 - r2;
        pMr[Z] = z0 - r3;

/*
 *      Solve for the point where the line intersects the face
 */
        Lf = (r1-x0)/dirx;
        yf = y0 + Lf*diry;
        zf = z0 + Lf*dirz;

/*
 *      Test to determine when region the line falls in, and then
 *      compute the distance accordingly.
 */
        if (yf<-r2 && zf>r3) {
            //region 1
            d = dirx*pMr[X] + diry*pMr[Y] + dirz*pMr[Z];
            lSqr = dirx*dirx + diry*diry + dirz*dirz;
            *L = -d/lSqr;
            if (*L>=0.0 && *L<=1.0) {            
                *dist2 = pMr[X]*pMr[X] + pPr[Y]*pPr[Y] + pMr[Z]*pMr[Z] + d*(*L);
                *xb = r1;
                *yb = -r2;
                *zb = r3;
            }
        } else if (yf>r2 && zf<-r3) {
            //region 5
            d = dirx*pMr[X] + diry*pMr[Y] + dirz*pPr[Z];
            lSqr = dirx*dirx + diry*diry + dirz*dirz;    
            *L = -d/lSqr;
            if (*L>=0.0 && *L<=1.0) {
                *dist2 = pMr[X]*pMr[X] + pMr[Y]*pMr[Y] + pPr[Z]*pPr[Z] + d*(*L);
                *xb = r1;
                *yb = r2;
                *zb = -r3;
            }  
        } else if (yf<r2 && yf>-r2 && zf<r3 && zf>-r3) {
            //region 0 - line intersects face
            inv = 1.0/dirx;
            *L = -pMr[X]*inv;
            if (*L>=0.0 && *L<=1.0) {
                *dist2 = 0.0;
                *xb = r1;
                *yb = y0 - diry*pMr[X]*inv;
                *zb = z0 - dirz*pMr[X]*inv;
            }
        } else {
            //region 2, 3, or 4
            //Test both edges by using the minimum distance formula between two
            //segments in addition to both end points against the face
            x1 = x0+dirx; 
            y1 = y0+diry; 
            z1 = z0+dirz;

            //Start with the end points
            AABBPointDistance(x0, y0, z0, 
                              r1, r2, r3,
                              dist2, xb, yb, zb);
            *L = 0.0;
            AABBPointDistance(x1, y1, z1, 
                              r1, r2, r3,
                              &dist2i, &xbi, &ybi, &zbi);
            if (dist2i < *dist2) {
                *L = 1.0;
                *dist2 = dist2i;
                *xb = xbi;
                *yb = ybi;
                *zb = zbi;
            }
            
            //Now test against the edges (bottom and right
            //on the positive x face)
            GetMinDist2(x0, y0, z0, 0.0, 0.0, 0.0,
				        x1, y1, z1, 0.0, 0.0, 0.0,
				        r1, -r2, r3, 0.0, 0.0, 0.0,
				        r1, -r2, -r3, 0.0, 0.0, 0.0,
						&dist2i, &tmp, &Li, &Ledge);
            if (dist2i < *dist2) {
                *dist2 = dist2i;
                mLedge = 1.0-Ledge;
                *xb = mLedge*r1    + Ledge*r1;
                *yb = mLedge*(-r2) + Ledge*(-r2);
                *zb = mLedge*r3    + Ledge*(-r3);
                *L = Li;
            }
            
            
            GetMinDist2(x0, y0, z0, 0.0, 0.0, 0.0,
				        x1, y1, z1, 0.0, 0.0, 0.0,
				        r1, -r2, -r3, 0.0, 0.0, 0.0,
				        r1, r2, -r3, 0.0, 0.0, 0.0,
						&dist2i, &tmp, &Li, &Ledge);
            if (dist2i < *dist2) {
                *dist2 = dist2i;
                mLedge = 1.0-Ledge;
                *xb = mLedge*r1    + Ledge*r1;
                *yb = mLedge*(-r2) + Ledge*r2;
                *zb = mLedge*(-r3) + Ledge*(-r3);
                *L = Li;
            }         
        }

        return;

}

/*-------------------------------------------------------------------------
 *
 *      Function:     AABBSegDistanceNoZero
 *      Description:  Find the minimum distance between an axially-aligned
 *                    bounding box (AABB) and an arbitrary segment.
 *
 *                    This function is just a wrapper that calls
 *                    AABBSegDistanceFaceX.
 *
 *                    This method is based on the pseudocode provided in
 *                    Geometric Tools for Computer Graphics by Schneider and
 *                    Eberly, section 10.9.4. 
 *
 *                    x0, y0, z0 = segment starting point
 *                    dirx,diry,dirz = vector going from point 1 to point 2
 *                    r1, r2,r3 = AABB dimensions (half widths)
 *                    dist2 = pointer for minimum distance squared
 *                    L = pointer for normalized distance along segment
 *                    xb, yb, zb = pointers for coordinates of point on box 
 *
 *-----------------------------------------------------------------------*/

void AABBSegDistanceNoZero(real8 x0, real8 y0, real8 z0,
                              real8 dirx, real8 diry, real8 dirz,
                              real8 r1, real8 r2, real8 r3,
                              real8 *dist2, real8 *L, 
                              real8 *xb, real8 *yb, real8 *zb) {

        real8   pMr[3], dyEx, dxEy, dzEx, dxEz, dzEy, dyEz;

        pMr[X] = x0 - r1;
        pMr[Y] = y0 - r2;
        pMr[Z] = z0 - r3;
        dyEx = diry*pMr[X];
        dxEy = dirx*pMr[Y];

        if (dyEx >= dxEy) {
            dzEx = dirz*pMr[X];
            dxEz = dirx*pMr[Z];
            
            if (dzEx >= dxEz) {
                //line intersects + x plane
                AABBSegDistanceFaceX(x0, y0, z0,
                                     dirx, diry, dirz,
                                     r1, r2, r3,
                                     dist2, L, 
                                     xb, yb, zb);
            } else {
                //line intersects + z plane
                AABBSegDistanceFaceX(z0, y0, x0,
                                     dirz, diry, dirx,
                                     r3, r2, r1,
                                     dist2, L, 
                                     zb, yb, xb);
            }
        } else {
            dzEy = dirz*pMr[Y];
            dyEz = diry*pMr[Z];
            
            if (dzEy >= dyEz) {
                //line intersects + y plane
                AABBSegDistanceFaceX(y0, z0, x0,
                                     diry, dirz, dirx,
                                     r2, r3, r1,
                                     dist2, L, 
                                     yb, zb, xb);
            } else {
                //line intersects + z plane
                AABBSegDistanceFaceX(z0, y0, x0,
                                     dirz, diry, dirx,
                                     r3, r2, r1,
                                     dist2, L, 
                                     zb, yb, xb);
            }
        }

        return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:     OBBSegDistance
 *      Description:  Find the minimum distance between an oriented bounding
 *                    box (OBB) and a line segment.
 *
 *                    This approach is based on the pseudocode provided in
 *                    Geometric Tools for Computer Graphics by Schneider and
 *                    Eberly, section 10.9.4.                    
 *
 *                    x, y, z = point coordinates
 *                    r1, r2,r3 = AABB dimensions (half widths)
 *                    dist2 = pointer for minimum distance squared
 *					  prec = pointer to precipitate structure
 *                    dist = pointer to distance output
 *                    xb, yb, zb = pointers for coordinates of point on box 
 *
 *-----------------------------------------------------------------------*/

void OBBSegDistance(real8 x1, real8 y1, real8 z1, 
                      real8 x2, real8 y2, real8 z2,
                      real8 xp, real8 yp, real8 zp, real8 UT[3][3],
                      real8 r1, real8 r2, real8 r3,
                      real8 *dist2, real8 *L, 
                      real8 *xb, real8 *yb, real8 *zb) {


        int     xZero, yZero, zZero, nZero; 
        int     pointsSwapped, xNeg, yNeg, zNeg, nNeg;
        int     mirrorX, mirrorY, mirrorZ; 
        real8   dr1[3], dr2[3], x, y, z; 
        real8   tmp[3], tmp2[3], linedir[3], U[3][3];
        real8   lx2, ly2, lz2, len2;
        real8   eps = 1e-10;

/*
 *      Transform points into box's coordinate frame. This converts
 *      the OBB to an AABB.
 */
        tmp[X] = x1 - xp;
        tmp[Y] = y1 - yp;
        tmp[Z] = z1 - zp;
		Matrix33Vector3Multiply(UT, tmp, dr1);

        tmp[X] = x2 - xp;
        tmp[Y] = y2 - yp;
        tmp[Z] = z2 - zp;
		Matrix33Vector3Multiply(UT, tmp, dr2);

        linedir[X] = dr2[X] - dr1[X];
        linedir[Y] = dr2[Y] - dr1[Y];
        linedir[Z] = dr2[Z] - dr1[Z];

        lx2 = linedir[X]*linedir[X];
        ly2 = linedir[Y]*linedir[Y];
        lz2 = linedir[Z]*linedir[Z];

        len2 = lx2 + ly2 + lz2;

        xZero = (lx2/len2 < eps);
        yZero = (ly2/len2 < eps);
        zZero = (lz2/len2 < eps);

        nZero = xZero + yZero + zZero;

/*
 *      Many of the functions below assume that the segment points in all positive coordinate
 *      directions. To enforce this, first we swap the points if two or more
 *      components are negative. Then, if one is still negative, we mirror the
 *      segment across that coordinate axis.
 */
        xNeg = ((linedir[X] < 0.0) && !xZero);
        yNeg = ((linedir[Y] < 0.0) && !yZero);
        zNeg = ((linedir[Z] < 0.0) && !zZero);

        nNeg = xNeg + yNeg + zNeg;

        pointsSwapped = 0;
        mirrorX = 0; mirrorY = 0; mirrorZ = 0;

        if (nNeg >= 2) {
            pointsSwapped = 1;

            linedir[X] *= -1.0;
            linedir[Y] *= -1.0;
            linedir[Z] *= -1.0;
            
            VECTOR_COPY(tmp,dr1);
            VECTOR_COPY(dr1,dr2);
            VECTOR_COPY(dr2,tmp);

            if (nNeg == 2) {
                if (!xNeg) {
                   mirrorX = 1;
                   linedir[X] *= -1.0;
                   dr1[X] *= -1.0;
                   dr2[X] *= -1.0;       
                } else if (!yNeg) {
                   mirrorY = 1;
                   linedir[Y] *= -1.0;
                   dr1[Y] *= -1.0;
                   dr2[Y] *= -1.0;           
                } else {
                   mirrorZ = 1;
                   linedir[Z] *= -1.0;
                   dr1[Z] *= -1.0;
                   dr2[Z] *= -1.0;           
                }        
            }
        } else if (nNeg == 1) {
            if (xNeg) {
               mirrorX = 1;
               linedir[X] *= -1.0;
               dr1[X] *= -1.0;
               dr2[X] *= -1.0;        
            } else if (yNeg) {
               mirrorY = 1;
               linedir[Y] *= -1.0;
               dr1[Y] *= -1.0;
               dr2[Y] *= -1.0;            
            } else {
               mirrorZ = 1;
               linedir[Z] *= -1.0;
               dr1[Z] *= -1.0;
               dr2[Z] *= -1.0;             
            }
        }


        if (nZero == 3) {
            //Case 1: Line segment is a point
            AABBPointDistance(dr1[X], dr1[Y], dr1[Z], 
                              r1, r2, r3,
                              dist2, &x, &y, &z);
            *L = 0.5;
        } else if (nZero == 2) {
            //Case 2: Line segment perpendicular to two axes    
            AABBSegDistance2Zero(dr1[X], dr1[Y], dr1[Z],         
                                 linedir[X], linedir[Y], linedir[Z],
                                 xZero, yZero, zZero,
                                 r1, r2, r3,
                                 dist2, L, 
                                 &x, &y, &z);
        } else if (nZero == 1) {
            //Case 3: Line segment perpendicular to one axis
            AABBSegDistance1Zero(dr1[X], dr1[Y], dr1[Z],         
                                 linedir[X], linedir[Y], linedir[Z],
                                 xZero, yZero, zZero,
                                 r1, r2, r3,
                                 dist2, L, 
                                 &x, &y, &z);
        } else {
            //Case 4: Arbirarily oriented line segment
            AABBSegDistanceNoZero(dr1[X], dr1[Y], dr1[Z],         
                                  linedir[X], linedir[Y], linedir[Z],
                                  r1, r2, r3,
                                  dist2, L, 
                                  &x, &y, &z);
        }
/*
 *      Clamp the minimum distance point within the segment.
 */
        if (*L < 0.0) {
            *L = 0.0;
            AABBPointDistance(dr1[X], dr1[Y], dr1[Z], 
                              r1, r2, r3,
                              dist2, &x, &y, &z);
        } else if (*L > 1.0) {
            *L = 1.0;
            AABBPointDistance(dr2[X], dr2[Y], dr2[Z], 
                              r1, r2, r3,
                              dist2, &x, &y, &z);
        }

/*
 *      If we swapped the points or mirrored a coordinate axis, switch it back now.
 */
        if (pointsSwapped){
            *L = 1.0-*L; 
        }
        if (mirrorX) {
            x *= -1.0;
        } else if (mirrorY) {
            y *= -1.0;
        } else if (mirrorZ) {
            z *= -1.0;
        }

/*
 *      Convert minimum point back to global coordinates.
 */
        tmp[X] = x;
        tmp[Y] = y;
        tmp[Z] = z;

        Matrix33Transpose(UT, U);

        Matrix33Vector3Multiply(U, tmp, tmp2);

        *xb = tmp2[X] + xp;
        *yb = tmp2[Y] + yp;
        *zb = tmp2[Z] + zp;

        return;
}

#endif
