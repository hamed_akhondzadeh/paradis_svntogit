/*--------------------------------------------------------------------------
 *
 *	Prec.h	Define the struct that holds all relevant data for a single 
 *          precipitate.
 *
 *
 *------------------------------------------------------------------------*/

#ifndef _Prec_h
#define _Prec_h

#define PREC_FAILED   0
#define PREC_SUCCESS  1

/*
 *	Define the structure for storing precipitate stress field info.
 */
typedef struct {
	int		Ngrid;		//number of grid points used for NxNxN grid
	int		used;		//flag used during initialization
	real8	pois;		//Poisson's ratio  
	real8	rc;			//core radius 
	real8	r1, r2, r3;	//precipitate radii 
	real8	estar[6];	//eigenstrain tensor in Voigt notation
	real8	alpha;		//atan transformation scaling parameter
	real8	rmax;		//maximum radius of numerical grid
	//3D arrays for stress field
	real8	***sxx, ***syy, ***szz, ***syz, ***sxz, ***sxy;
} Precstress_t;

struct _prec {

    real8   x, y, z;        //position of precipitate centroid
    real8   r1, r2, r3;     //radii at precipitate axes
	real8	rmin, rmax;		//min and max radius
	real8	xdir[3], ydir[3], zdir[3]; //directions of the principal axes
	
	real8	lloop;			//segment size used for Orowan looping
		
	int		sphere;			//flag to indicate if its a sphere

	real8	UT[3][3];  		//transformation matrix

/*	
 *	The ellipsoidal geometry is specified by the A matrix. We also need
 *	its inverse, its Cholesky decomposition L, and the inverse of its 
 *	Cholesky matrix.
 */	
	real8	A[3][3], Ainv[3][3];	//A matrix and its inverse
    real8   LT[3][3], LTinv[3][3];  //transpose of L matrix and its inverse
	real8	Lmult[3][3];			//Lmult=Linv*LTinv 

/*
 *	Parameters for specifying the cutting rate or cutting force.
 */	
    real8   Eb0, tauinst;   //energy barrier and athermal cutting stress
	real8	Fcut;			//athermal cutting force

/*
 *	Misfit and misfit field data.
 */
	Precstress_t	*precstress;  //structure to store misfit field
	real8	estar[6];       //eigenstrain tensor components (Voigt notation)
	real8	dV;				//volume misfit (used with dilatational spheres)
	real8	sscale;			//scaling factor used with numerical stress field
	real8	rcut;			//cut-off radius
	int		precstress_owner; //flag to denote if precipitate is responsible for precstress memory
	int		misfit;			//flag to indicate if the precipitate has a misfit field
	int		analytic_stress;	//flag indicating if an analytic stress field is to be used
	
	int		cell2Idx;		//cell2 index owning the precipitate

	Tag_t	myTag;			//precipitates have the exact same tags as nodes

};

typedef struct _prec Prec_t;

#endif
