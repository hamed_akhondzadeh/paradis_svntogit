/**************************************************************************
 *
 *      Module:  PrecipitateRelease -- This module contains various functions
 *               for determining when to release nodes from precipitates,
 *				 either because they have cut the precipitate or because
 *				 they have reversed their direction of motion.
 *
 *      Includes functions:
 *
 *
 *************************************************************************/
 
#ifdef _PREC

#include "Home.h"

/*-------------------------------------------------------------------------
 *
 *      Function:     ReleaseNeighborNodes
 *      Description:  Release a node's neighbor node if it is within
 *					  the release distance (rel_dist) of relnode.
 *
 *					  This function is called recursively to check all
 *					  nodes along dislocation lines shared with relnode.
 *					  We assume that once we hit a node that is further
 *					  away than rel_dist along a dislocation line,
 *					  all subsequent nodes will be as well (so we can
 *					  discontinue our search).
 *
 *			Arguments:
 *				home	pointer to home structure
 *				node	pointer to node whose neighbors are to be tested
 *				relnode	pointer to node for distance check
 *				rel_dist release distance
 *
 *------------------------------------------------------------------------*/

void ReleaseNeighborNodes(Home_t *home, Node_t *node, 
						  Node_t *relnode, real8 rel_dist) {
	
		int		j;
		real8	dx, dy, dz, dist;
		Node_t	*nbr;

/*
 *		Loop over all of the node's arms and look for 
 *		neighbor nodes within rel_dist of relnode;
 */	
		for (j = 0; j < node->numNbrs; j++) {

			nbr = GetNeighborNode(home,node,j);
			
/*
 *			If the nbr is not residing on the same precipitate
 *			(or has already been released), skip it.
 */	
			if (nbr->prec != relnode->prec) continue;
			
/*
 *			If we have looped back and are checking relnode
 *			itself, skip it.
 */
			if (nbr->myTag.index==relnode->myTag.index && 
				nbr->myTag.domainID==relnode->myTag.domainID)
					continue;
			
			dx = relnode->x - nbr->x;
			dy = relnode->y - nbr->y;
			dz = relnode->z - nbr->z;

			ZImage(home->param,&dx,&dy,&dz);
		
			dist = sqrt(dx*dx + dy*dy + dz*dz);
		
/*
 *			If the nbr node is within the release distance,
 *			release it and also check all of its neighbors
 *			for release.
 */
			if (dist<rel_dist) {
                ChangePrecipitate(home, nbr, -1, 1);
				EvaluateMobility(home, nbr);
				ReleaseNeighborNodes(home,nbr,relnode,rel_dist);
			}
		}
		return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     PrecipitateRelease
 *      Description:  Loop over all nodes and release those nodes who are 
 *					  pinned at precipitates and either:
 *						1) Are moving away from the precipitate
 *						2) Satisfy the precipitate cutting condition
 *						3) Have a neighbor that is cutting the precipitate
 *
 *			Arguments:
 *				home	point to home structure
 *
 *------------------------------------------------------------------------*/

void PrecipitateRelease(Home_t *home) {
	
		int		i, j, mobError, precError;
		real8	vnode[3], vNorm2, vNorm, rmp[3], np[3], vnpDot;
		real8	xsave, ysave, zsave, shift, vtest[3];
		real8	fXsave, fYsave, fZsave;
		real8	f[3], fnode[3], Lsum, normnp, Fcut, rel_dist;
		real8	x1, y1, z1, x2, y2, z2, dx, dy, dz;
		real8	vNoise;
		Node_t 	*node, *nbr;
		Prec_t	*prec;
		Param_t	*param;
		
		param = home->param;

		real8 Fcutmax = 0.0, Fsum = 0.0;
		
		vNoise   = param->rTol/param->deltaTT;
					
/*
 *     Loop through all the nodes native to this domain looking for
 *     nodes to test for release.
 */
       for (i = 0; i < home->newNodeKeyPtr; i++) {
		   	
           	node = home->nodeKeys[i];
           	if (node == (Node_t *)NULL) continue;
		
/*
 *			Only nodes attached to precipitates can be released.
 */
			if (node->prec<0) continue;
			
/*
 *          If the node is flagged to have its force reset, then we
 *          cannot accurately compute the cutting force. Skip it for
 *          this step.
 */
            if ((node->flags & NODE_RESET_FORCES) != 0) continue;
						
if (node->prec >= home->newPrecKeyPtr) {
	printf("WARNING: Ran over the precKeys array of length %i with %i\n",home->newPrecKeyPtr,node->prec);
	continue;
} 			
			prec = home->precKeys[node->prec];
			if (prec == (Prec_t *) NULL) {
                printf("WARNING: Precipitate not found at %s line %d\n",
                       __FILE__, __LINE__);
				continue;
			} 			
			
/*
 *			Calculate the velocity if the node is released -
 *			we will need this for several calculations below.
 */
			node->constraint &= ~PINNED_NODE;
			mobError  = EvaluateMobility(home, node);
			
			vnode[X] = node->vX; 	
			vnode[Y] = node->vY; 	
			vnode[Z] = node->vZ;
			vNorm2 = DotProduct(vnode,vnode);
			vNorm = sqrt(vNorm2);
			
/* 
 *			If a neighbor node is inside the precipitate and not pinned, 
 *			then the precipitate has been cut - release this node to continue
 *			propagating the cut. Only do this test if the precipitate is 
 *			penetrable (Fcut>0).			
 */
			if (prec->Fcut >= 0.0) {
	            precError = PREC_SUCCESS;
				for (j = 0; j < node->numNbrs; j++) {

					nbr = GetNeighborNode(home,node,j);
				
					if (nbr->prec>=0) continue;	

					x2 = nbr->x;
					y2 = nbr->y;
					z2 = nbr->z;

					PBCPOSITION(param,prec->x,prec->y,prec->z,&x2,&y2,&z2);

					if(InsideEllipsoid(x2, y2, z2, prec->x, prec->y, prec->z, prec,
						(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL)) {
						//node->prec = -1;
						precError = ChangePrecipitate(home, node, -1, 1);
#ifdef DEBUG_LOG_PREC_RELEASE
						if (precError==PREC_SUCCESS) {
							printf("Node %i,%i at (%e %e %e) released from %i: interior neighbor %i,%i\n",
										node->myTag.domainID,node->myTag.index,
										node->x,node->y,node->z,
		                                prec->myTag.index,
		                                nbr->myTag.domainID,nbr->myTag.index);
							printf("%e %e %e; %e %e %e; %e %e %e\n",
									x2,y2,z2,nbr->oldx,nbr->oldy,nbr->oldz,
									nbr->olderx,nbr->oldery,nbr->olderz);
						}
#endif			
						break;	
					}			
				}
				if (node->prec<0 || precError==PREC_FAILED) continue;
			}
			
/*
 *			Reset the node as pinned and the velocity to zero.
 *			If it is to be released, we will release it below.
 */
			node->vX = 0.0;	node->vY = 0.0;	node->vZ = 0.0;
			node->constraint = PINNED_NODE;
			
			if (mobError) continue;
			
/*
 *			First we test to see if the node is trying to move away
 *			from the precipitate. We do so by comparing the outward
 *			normal of the precipitate with its velocity vector.
 */
			rmp[X] = node->x - prec->x;
			rmp[Y] = node->y - prec->y;
			rmp[Z] = node->z - prec->z;
			
			ZImage(param,&rmp[X],&rmp[Y],&rmp[Z]);
			
			if (prec->sphere) {
				VECTOR_COPY(np,rmp);
			} else {
				Matrix33Vector3Multiply(prec->A, rmp, np);
			}
		
			vnpDot = DotProduct(vnode,np);
			
			if (vnpDot>0.0 && vNorm>vNoise) {
/*
 *				The node is moving away from the precipitate. However, we also
 *				want to make sure this motion is stable. So, we move the node by
 *				rann away from the surface and make sure it doesn't reverse direction.
 */
				xsave = node->x; 	ysave = node->y; 	zsave = node->z;
				fXsave = node->fX; 	fYsave = node->fY; 	fZsave = node->fZ;
				
				//shift = param->rann/vNorm;
				shift = MAX(0.1*prec->rmin,param->rann)/vNorm;
				
				node->x += shift*vnode[X];
				node->y += shift*vnode[Y];
				node->z += shift*vnode[Z];
				
				FoldBox(param,&node->x,&node->y,&node->z);
			
				node->constraint &= ~PINNED_NODE;
                SetOneNodeForce(home, node);
				mobError  = EvaluateMobility(home, node);
			
				vtest[X] = node->vX;
				vtest[Y] = node->vY;
				vtest[Z] = node->vZ;
							
				if (DotProduct(vtest,np)>0.0 && (mobError==0) && 
							sqrt(DotProduct(vtest,vtest))>vNoise) {	
/*
 *					The node's motion away from the precipitate appears to be stable,
 *					go ahead and release it and continue checking other nodes.
 */			
					//node->constraint &= ~PINNED_NODE;
					//node->vX = vnode[X];	node->vY = vnode[Y];	node->vZ = vnode[Z];
					//node->prec = -1;
                    precError = ChangePrecipitate(home, node, -1, 1);
                    if (precError == PREC_SUCCESS) {
	                    AddOp(home, RESET_COORD,
	                        node->myTag.domainID,
	                        node->myTag.index,
	                        -1, -1,                 /* 2nd tag unneeded */
	                        -1, -1,                 /* 3rd node tag unneeded */
	                        0.0, 0.0, 0.0,          /* burgers vector unneeded */
	                        node->x, node->y, node->z,
	                        0.0, 0.0, 0.0);         /* plane normal not needed */
#ifdef DEBUG_LOG_PREC_RELEASE
						printf("Node %i,%i at (%e %e %e) released: velocity reversal\n",
								node->myTag.domainID,node->myTag.index,
								node->x,node->y,node->z);
#endif
                    }
					continue;
				}
/*
 *				The node turned back around towards the precipitate, so its motion
 *				is not stable. Leave the node attached to the precipitate
 *				and continue checking other nodes.
 */
				node->x = xsave;
				node->y = ysave;
				node->z = zsave;
				
				node->fX = fXsave;
				node->fY = fYsave;
				node->fZ = fZsave;

				node->vX = 0.0;	node->vY = 0.0;	node->vZ = 0.0;
				node->constraint = PINNED_NODE;
				
				continue;
			}
			
/*
 *			If the precipitate's cutting force is negative then we treat it as
 *			impenetrable.
 */
			if (prec->Fcut<0.0) continue;
			
/*
 *			The node is trying to cut the precipitate. We need to compute the cutting
 *			force per unit length acting on the precipitate at the node and test for
 *			a cutting event. 
 *			
 *			When we compute the cutting force, we exclude the force
 *			exerted by the precipitate it is pinned against, since the atomistic
 *			results used as inputs do not consider this stress.
 */			
			//SetOneNodePrecForce(home, node, prec, f);
			fnode[X] = node->fX;// - f[X];	
			fnode[Y] = node->fY;// - f[Y];			
			fnode[Z] = node->fZ;// - f[Z];
			
			Lsum = 0.0;
			for (j = 0; j < node->numNbrs; j++) {
			
				nbr = GetNeighborNode(home,node,j);	
			
				dx = node->x - nbr->x;
				dy = node->y - nbr->y;
				dz = node->z - nbr->z;
			
				ZImage(param,&dx,&dy,&dz);
				
				Lsum += sqrt(dx*dx + dy*dy + dz*dz);
			}
											
/*
 *			The cutting force is fnode orthgonalized with respect to both the velocity
 *			vector and the precipitate normal vector.
 */
			normnp = sqrt(DotProduct(np,np));
			Fcut = -DotProduct(fnode,vnode)*vnpDot/(vNorm2*normnp*Lsum/2.0);
Fcutmax = MAX(Fcut,Fcutmax);
Fsum += Fcut*Lsum/2.0;
			if (Fcut >= prec->Fcut) {
				printf("Fcut=%e at %e %e %e\n",Fcut,node->x,node->y,node->z);
				
/*
 *				When we release a node, we also release all nodes within some distance
 *				of the node. This helps to propagate the cutting event along the
 *				precipitate. Otherwise, releasing a node with very short segments may not lead
 *				to a global cutting event for its entire dislocation line.
 */
				//rel_dist = 0.5*MAX(MAX(prec->r1,prec->r2),prec->r3);				
				//ReleaseNeighborNodes(home,node,node,rel_dist);
                precError = ChangePrecipitate(home, node, -1, 1);
                if (precError == PREC_SUCCESS) {
				    node->vX = vnode[X];	
                    node->vY = vnode[Y];	
                    node->vZ = vnode[Z];
#ifdef DEBUG_LOG_PREC_RELEASE
				printf("Node %i,%i released from %i: cutting event\n",
						node->myTag.domainID,node->myTag.index,
						node->x,node->y,node->z,
						prec->myTag.index);
#endif
                }
			}			
		}
		//printf("Fcutmax=%e\nFsum=%e\n",Fcutmax,Fsum);
		return;
	
}

#endif
