############################################################################
#
#    makefile: controls the build of paradisprec executable
#
############################################################################
include ../makefile.sys
include ../makefile.setup

DEFS += -D_PREC
DEFS += -D_RETROCOLLISIONS

#debugging compiler flags
#use numerical routines for sphere, including stress field and distance calculation
#DEFS += -D_NUMERICALSPHERE  
#DEFS += -DDEBUG_LOG_PREC_COLLISIONS
DEFS += -DDEBUG_PREC_IMPENETRABLE
#DEFS += -DDEBUG_LOG_PREC_RELEASE
#DEFS += -DDEBUG_PREC

## The following flags for data output

#DEFS += -D_PRINTSTRESS
#DEFS += -D_WRITENODEFORCE

SRCDIR = ../src
INCDIR = ../include
BINDIR = ../bin
IMPDIR = ../implicit

PARADISPREC_INCDIR = Include
#
#	The utilities use various source modules from the parallel
#       code located in the parent directory.  Maintain a list of
#	these files.
#
#	These modules are compiled in the parent directory with a
#	different set of preprocessor definitions than are needed
#	here, so we need to create links in this directory back to
#	the source modlues and create separate object modules for
#	these sources.
#

EXTERN_C_SRCS = CellCharge.c             \
      CommSendGhostPlanes.c    \
      CommSendMirrorNodes.c    \
      CommSendRemesh.c         \
      CommSendSegments.c       \
      CommSendVelocity.c       \
      CorrectionTable.c        \
      CrossSlip.c              \
      CrossSlipFCC.c           \
      CrossSlipBCC.c           \
      DebugFunctions.c         \
      Decomp.c                 \
      DeltaPlasticStrain.c     \
      DeltaPlasticStrain_BCC.c \
      DeltaPlasticStrain_FCC.c \
      DisableUnneededParams.c  \
      DLBfreeOld.c             \
      deWitInteraction.c       \
      FindPreciseGlidePlane.c   \
      FMComm.c                 \
      FMSigma2.c               \
      FMSupport.c              \
      ForwardEulerIntegrator.c \
      FreeInitArrays.c         \
      GenerateOutput.c         \
      GetDensityDelta.c        \
      Gnuplot.c                \
      Heap.c                   \
      InitCellDomains.c        \
      InitCellNatives.c        \
      InitCellNeighbors.c      \
      InitHome.c               \
      InitRemoteDomains.c      \
      InputSanity.c            \
      LoadCurve.c              \
      Main.c                    \
      Matrix.c                 \
      Meminfo.c                \
      MemCheck.c               \
      MobilityLaw_BCC_0.c      \
      MobilityLaw_BCC_0b.c     \
      MobilityLaw_BCC_glide.c  \
      MobilityLaw_FCC_0.c      \
      MobilityLaw_FCC_0b.c     \
      MobilityLaw_FCC_climb.c  \
      MobilityLaw_Relax.c      \
      NodeVelocity.c           \
      OsmoticForce.c           \
      ParadisInit.c            \
      ParadisThread.c          \
      Parse.c                  \
      PickScrewGlidePlane.c    \
      PredictiveCollision.c     \
      ProximityCollision.c     \
      QueueOps.c               \
      ReadBinaryRestart.c      \
      ResetGlidePlanes.c       \
      RBDecomp.c               \
      RSDecomp.c               \
      RemapInitialTags.c       \
      Remesh.c                 \
      RemeshRule_3.c           \
      RemoteSegForces.c        \
      RemoveNode.c             \
      SemiInfiniteSegSegForce.c \
      SortNativeNodes.c        \
      SplitSurfaceNodes.c      \
      Tecplot.c                \
      Timer.c                  \
      TrapezoidIntegrator.c     \
      Util.c                   \
      WriteArms.c              \
      WriteAtomEye.c	       \
      WriteBinaryRestart.c     \
      WriteDensFlux.c          \
      WriteDensityField.c      \
      WriteForce.c             \
      WriteFragments.c         \
      WritePoleFig.c           \
      WritePovray.c            \
	  WriteProp.c              \
      WriteRestart.c           \
      WriteVelocity.c          \
      WriteVisit.c             

EXTERN_CPP_SRCS = DisplayC.C       \
                  display.C 

EXTERN_HEADERS = Cell.h           \
                 Constants.h      \
                 DebugFunctions.h \
                 Decomp.h         \
                 DisplayC.h       \
                 FM.h             \
                 Force.h          \
                 InData.h         \
                 Init.h           \
                 Matrix.h         \
                 MirrorDomain.h   \
                 OpList.h         \
                 ParadisGen.h     \
                 ParadisProto.h   \
                 ParadisThread.h  \
                 Parse.h          \
                 QueueOps.h       \
                 RBDecomp.h       \
                 RSDecomp.h       \
                 RemoteDomain.h   \
                 Restart.h        \
                 Tag.h            \
                 Timer.h          \
                 Topology.h       \
                 Util.h           \
                 WriteProp.h      \
                 display.h       

EXTERN_SRCS = $(EXTERN_C_SRCS) $(EXTERN_CPP_SRCS)
EXTERN_OBJS = $(EXTERN_C_SRCS:.c=.o) $(EXTERN_CPP_SRCS:.C=.o)

#
#	A few files are grabbed from the implicit directory so
#	RetroactiveCollisions can be used with segments.
#

EXTERN_IMP_SRCS = Collision.c             \
                  RetroactiveCollision.c  \
				  SortNodes.c
				  
EXTERN_IMP_OBJS = $(EXTERN_IMP_SRCS:.c=.o)

#
#       Define the sources in the precipitates/ directory
#

#
#	Define the exectutable, source and object modules for
#	the problem generator.
#


PARADISPREC     = paradisprec
PARADISPREC_BIN = $(BINDIR)/$(PARADISPREC)

PARADISPREC_C_SRCS = CommSendGhosts.c          \
                     CommSendSecondaryGhosts.c \
                     FixRemesh.c               \
                     GetNewGhostNode.c         \
                     GetNewNativeNode.c        \
                     Initialize.c              \
                     InitSendDomains.c         \
			         LocalSegForces.c          \
                     Migrate.c                 \
					 MobilityLaw_Glide.c       \
			         NodeForce.c               \
                     ParadisFinish.c           \
                     ParadisStep.c             \
                     Param.c                   \
                     Plot.c                    \
                     Precipitates.c            \
                     PrecipitateCollision.c    \
					 PrecipitateDistance.c     \
					 PrecipitateForce.c        \
					 PrecipitateRelease.c      \
					 PrecipitateRemesh.c       \
                     ReadPrecDataFile.c        \
                     ReadRestart.c             \
			         RemeshRule_2.c            \
			         SortNodesForCollision.c   \
			         SortPrecipitatesForCollision.c   \
			         Topology.c                
					               

PARADISPREC_HEADERS = $(addprefix $(PARADISPREC_INCDIR)/, $(EXTERN_HEADERS))

PARADISPREC_INCS.gcc = -I$(HOME)/usr/include
PARADISPREC_LIBS.gcc = -lm

PARADISPREC_INCS.linux = -I/usr/include
PARADISPREC_LIBS.linux = -L/usr/lib/ -lm

PARADISPREC_INCS.mc2 = -I$(HOME)/usr/include
PARADISPREC_LIBS.mc2 = -L$(HOME)/usr/lib  -lm

PARADISPREC_INCS.mac = -I/usr/local/include
PARADISPREC_LIBS.mac = -L/usr/local/lib -lm

PARADISPREC_INCS.linux.pc = -I/usr/include
PARADISPREC_LIBS.linux.pc = -L/usr/lib/ -lm


PARADISPREC_INCS.wcr = -I/opt/fftw-3.1.2/intel/include
PARADISPREC_LIBS.wcr = -L/opt/fftw-3.1.2/intel/lib/  -lm

PARADISPREC_INCS.vip = 
PARADISPREC_LIBS.vip =  -L/u/system/Power/libs/fftw-3.1.2/lib  -lm

PARADISPREC_INCS.su-ahpcrc = -I/lustre/home/mpotts/fftw/include
PARADISPREC_LIBS.su-ahpcrc = -L/lustre/home/mpotts/fftw/lib -llis -lm  

# For mjm
PARADISPREC_INCS.linux.opteron = -I/usr/cta/pet/MATH/include
PARADISPREC_LIBS.linux.opteron = -L/usr/cta/pet/MATH/lib/ -llis -lm

PARADISPREC_INCS.cygwin = 
PARADISPREC_LIBS.cygwin = -llis

PARADISPREC_INCS.mc-cc = 
PARADISPREC_LIBS.mc-cc = -llis

PARADISPREC_INCS.cray =-I/mnt/cfs/pkgs/PTOOLS/pkgs/fftw-2.1.5/include
PARADISPREC_LIBS.cray =-L/mnt/cfs/pkgs/PTOOLS/pkgs/fftw-2.1.5/lib/ -llis


PARADISPREC_INCS.Harold = -I/mnt/lustre/usrcta/pet/pkgs/fftw/3.2.2/include
PARADISPREC_LIBS.Harold = -L/mnt/lustre/usrcta/pet/pkgs/fftw/3.2.2/lib -llis


PARADISPREC_INCS = $(INCS_$(MODE).$(SYS)) $(XLIB_INCS) $(MPI_INCS) \
		  $(HDF_INCS)  $(PARADISPREC_INCS.$(SYS)) -I $(PARADISPREC_INCDIR)

PARADISPREC_LIBS = $(PARADISPREC_LIBS.$(SYS))

PARADISPREC_SRCS = $(PARADISPREC_C_SRCS) $(PARADISPREC_CPP_SRCS)
PARADISPREC_OBJS = $(PARADISPREC_C_SRCS:.c=.o) $(PARADISPREC_CPP_SRCS:.C=.o)



###########################################################################
#
#	Define a rule for converting .c files to object modules.
#	All modules are compile serially in this directory
#
###########################################################################

.c.o:		makefile ../makefile.sys ../makefile.setup
		- @ make $(PARADISPREC_HEADERS) > /dev/null 2>&1
		$(CC) $(OPT) $(CCFLAG) $(PARADISPREC_INCS) -c $<

.C.o:		makefile ../makefile.sys ../makefile.setup
		- @ make $(PARADISPREC_HEADERS) > /dev/null 2>&1
		$(CPP) $(OPT) $(CPPFLAG) $(INCS) -c $<


###########################################################################
#
#	Define all targets and dependencies below
#
###########################################################################

all:		$(EXTERN_OBJS) $(EXTERN_IMP_OBJS) $(PARADISPREC) 

clean:
		rm -f *.o $(EXTERN_SRCS) $(EXTERN_IMP_SRCS) $(PARADISPREC_BIN) test_YoffeInfStress
		rm -f $(PARADISPREC_HEADERS)

depend:		 *.c $(SRCDIR)/*.c $(IMPDIR)/*.c $(INCDIR)/*.h makefile
		makedepend -Y$(INCDIR) *.c  -fmakefile.dep

headers:
		echo $(PARADISPREC_HEADERS)

#
#	Create any necessary links in the current directory to source
#	modules located in the SRCDIR directory
#

$(EXTERN_SRCS): $(SRCDIR)/$@
		- @ ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1
		
$(EXTERN_IMP_SRCS): $(IMPDIR)/$@
		- @ ln -s  -f $(IMPDIR)/$@ ./$@ > /dev/null 2>&1		

$(PARADISPREC_HEADERS): $(INCDIR)/$(notdir $@)
		- @ ln -s  -f ../$(INCDIR)/$(notdir $@) $@ > /dev/null 2>&1

# For vip
#$(EXTERN_SRCS): $(SRCDIR)/$@
#                ln -s  -f $(SRCDIR)/$@ ./$@ > /dev/null 2>&1


$(PARADISPREC):	$(PARADISPREC_BIN)
$(PARADISPREC_BIN): $(PARADISPREC_SRCS) $(PARADISPREC_OBJS) $(EXTERN_OBJS) $(EXTERN_IMP_OBJS) $(HEADERS)
	echo $(PARADISPREC_OBJS)
		$(CPP) $(OPT) $(OPENMP_FLAG) $(PARADISPREC_OBJS) $(EXTERN_OBJS) $(EXTERN_IMP_OBJS) -o $@  $(LIB) $(PARADISPREC_LIBS)


