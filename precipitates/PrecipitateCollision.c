/*****************************************************************************
 *
 *	Module:		PrecipitateCollision.c
 *	Description:	This module contains various functions used
 *			for detecting and handling collisions between precipitates and
 *          dislocation segments.
 *
 *          The overall algorithm uses the interval halving method to detect
 *          collisions. Minimum distance calculations are done numerically
 *          using Newton's method with a Lagrange multiplier to minimize the
 *          distance to a point, and the golden section search method to 
 *          minimize the distance to a line segment.
 *
 *
 *	Included functions:
 *              PrecipitateCollisionsProx
 *				IntervalCollision (local only)
 *				DetectCollisionPrecipitate (local only)
 *				PrecipitateCollisions
 *
 *****************************************************************************/
#ifdef _PREC

#include "Home.h"

#define	 MAX_COLS 10000  //Maximum number of collisions allowed in one time step
static real8 COL_TOL = 0.001;  //tolerance for detecting collisions in fraction of time step
#define	 NO_COLLISIONS_TIME 20  //Collision time that denotes no collisions occurred

static int dbgDom;


/*-------------------------------------------------------------------------
 *
 *      Function:     PrecipitateCollisionsProx
 *      Description:  Loop over all precipitate-segment pairs in the same
 *					  or neighboring cell2s, and detect and handle 
 *					  collisions.
 *
 *					  This code uses a simple proximity algorithm for 
 *					  collision detection, and is mainly intended for
 *					  development/debugging purposes. It is not as robust
 *					  as PrecipitateCollisions.					
 *
 *-----------------------------------------------------------------------*/

void PrecipitateCollisionsProx(Home_t *home) {
        int     i, j, k, q, arm12, arm21, arm34, arm43;
        int     thisDomain, splitStatus, mergeStatus;
        int     armAB, armBA;
        int     globalOp = 1, didCollision;
        int     close2node1, close2node2, close2node3, close2node4;
        int     splitSeg1, splitSeg2;
        int     cell2Index, nbrCell2Index, nextIndex;
        int     cell2X, cell2Y, cell2Z, cx, cy, cz;
        int     localCollisionCnt, globalCollisionCnt;
        int     collisionConditionIsMet, adjustCollisionPoint;
		int 	hinge;
        real8   mindist, mindist2, dist, L, eps, half;
        real8   cTime, cPoint[3];
        real8   x1, y1, z1, vx1, vy1, vz1;
        real8   x2, y2, z2, vx2, vy2, vz2;
		real8	xp, yp, zp;
        real8   newx, newy, newz;
        real8   newvx, newvy, newvz;
        real8   pnew[3], burg1[3], burg2[3];
        real8   oldfp0[3], oldfp1[3];
        real8   f0seg1[3], f1seg1[3], f0seg2[3], f1seg2[3];
        real8   nodeVel[3], newNodeVel[3];
        real8   newPos[3], newVel[3];
        real8   vec1[3], vec2[3];
        real8   p1[3], p2[3], v1[3], v2[3];
		real8	r1, len, frac;
        Tag_t   oldTag1, oldTag2;
        Node_t  *node1, *node2, *tmpNbr;
        Node_t  *targetNode;
        Node_t  *splitNode1, *splitNode2;
        Param_t *param;
		Prec_t  *prec;

        thisDomain = home->myDomain;
        param      = home->param;

/* QUESTION!  What is reasonable for mindist2? */
		mindist = 1; //param->rann;
        mindist2 = mindist*mindist;
        eps      = 1.0e-12;
        half     = 0.5;

        localCollisionCnt = 0;
        globalCollisionCnt = 0;

#ifdef DEBUG_TOPOLOGY_DOMAIN
        dbgDom = DEBUG_TOPOLOGY_DOMAIN;
#else
        dbgDom = -1;
#endif

        //TimerStart(home, COLLISION_HANDLING);

/*
 *      Start looping through native precipiatates looking for segments to collide...
 */
        for (i = 0; i < home->newPrecKeyPtr; i++) {
			
			prec = home->precKeys[i];
            
			if (prec == (Prec_t *)NULL) continue;
			
			xp = prec->x; yp = prec->y; zp = prec->z;

/*
 *          Loop through all cell2s neighboring the prec.  Only
 *          nodes in these neighboring cell2s are candidates for
 *          collisions.
 *
 *          NOTE: All nodes are assigned cell2 membership prior to
 *          entering collision handling.  However, nodes added during
 *          node separation and collision handling are not given
 *          cell2 membership for this cycle.  In most cases this
 *          is not a problem since those nodes have usually been
 *          exempted from subsequent collisions this cycle.  There
 *          are certain circumstances, though, in which the exemptions
 *          have not been set.  If we encounter one such node that
 *          has no cell2 membership, skip it for this cycle, or bad
 *          things happen.
 */
            cell2Index = prec->cell2Idx;
            if (cell2Index < 0) {
                continue;
            }

            DecodeCell2Idx(home, cell2Index, &cell2X, &cell2Y, &cell2Z);

            for (cx = cell2X - 1; cx <= cell2X + 1; cx++) {
             for (cy = cell2Y - 1; cy <= cell2Y + 1; cy++) {
              for (cz = cell2Z - 1; cz <= cell2Z + 1; cz++) {

                nbrCell2Index = EncodeCell2Idx(home, cx, cy, cz);

/*
 *              Loop though all nodes in the neighbor cell2
 */
                nextIndex = home->cell2[nbrCell2Index];

                while (nextIndex >= 0) {

                   // if (didCollision) break;

                    node1 = home->cell2QentArray[nextIndex].node;
                    nextIndex = home->cell2QentArray[nextIndex].next;

                    if (node1 == (Node_t *)NULL) continue;
                    if (node1->flags & NO_COLLISIONS) continue;

/*
 *                  Loop over all arms of node1.  Skip any arms that
 *                  terminate at node3 (those hinge arms will be dealt
 *                  with later)
 */
                    for (arm12 = 0; arm12 < node1->numNbrs; arm12++) {

                    //    if (didCollision) break;

                        node2 = GetNodeFromTag(home, node1->nbrTag[arm12]);

                        if (node2 == (Node_t *)NULL) continue;
                        if (node2->flags & NO_COLLISIONS) continue;

                        if (CollisionNodeOrder(home, &node1->myTag,
                                               &node2->myTag) > 0) {
                            continue;
                        }

/*
 *                      Segment node1/node2 may only be used in a collision
 *                      if the segment is owned by the current domain.
 */
                        if (!DomainOwnsSeg(home, OPCLASS_COLLISION,
                                           thisDomain, &node2->myTag)) {
                            continue;
                        }

/*
 *						If both nodes are already pinned at a precipiate
 *						then they can't collide with a precipitate.
 */						
						if (node1->prec>=0 && node2->prec>=0) { 
							continue;
						} 
						//if (node1->constraint==PINNED_NODE && node2->constraint==PINNED_NODE) { 
						//	continue;
						//}
						
                        x1 = node1->x; y1 = node1->y; z1 = node1->z;
                        x2 = node2->x; y2 = node2->y; z2 = node2->z;

                        vx1 = node1->vX; vy1 = node1->vY; vz1 = node1->vZ;
                        vx2 = node2->vX; vy2 = node2->vY; vz2 = node2->vZ;

                        PBCPOSITION(param, xp, yp, zp, &x1, &y1, &z1);
                        PBCPOSITION(param, x1, y1, z1, &x2, &y2, &z2);


                        p1[X] = x1;  p1[Y] = y1;  p1[Z] = z1;
                        p2[X] = x2;  p2[Y] = y2;  p2[Z] = z2;

                        v1[X] = vx1;  v1[Y] = vy1;  v1[Z] = vz1;
                        v2[X] = vx2;  v2[Y] = vy2;  v2[Z] = vz2;

/*
 *                      It is possible to have a zero-length segment
 *                      (created by a previous collision).  If we find
 *                      such a segment, do not try to use it in any
 *                      subsequent collisions.
 */
                        vec1[X] = x2 - x1;
                        vec1[Y] = y2 - y1;
                        vec1[Z] = z2 - z1;

						len = sqrt(DotProduct(vec1, vec1));
						
                        if (len < 1.0e-20) {
                            continue;
                        }
						
/* 
 * 					    If one node is already pinned on this precipitate
 * 						we need to offset the position of the node.
 */
						frac = MIN(prec->lloop/len,1.0);
						if (node1->prec==i) {
						//if (node1->constraint==PINNED_NODE) {
							hinge = 1;
							x1 = x1*(1.0-frac) + x2*frac;
							y1 = y1*(1.0-frac) + y2*frac;
							z1 = z1*(1.0-frac) + z2*frac;
						} else if (node2->prec==i) {
							//} else if (node2->constraint==PINNED_NODE) {	
							hinge = 2;
							x2 = x2*(1.0-frac) + x1*frac;
							y2 = y2*(1.0-frac) + y1*frac;
							z2 = z2*(1.0-frac) + z1*frac;							
						} else {
							hinge = 0;
						}


/*
 *                      Find the minimum distance between the two segments
 *                      and determine if they should be collided.
 */
					    // GetMinDistSphere(x1, y1, z1,
					    //                  x2, y2, z2,
					    //                  xp, yp, zp,
					    //                  prec->r1, &dist, &L);
										 
						// GetMinDistEllipsoid(60.0, 50.0, 200.0, 0.0, -100.0, 200.0, 0.0, 0.0, 0.0,
						//                         prec, &dist, &L);
						// 				 			printf("%e %e\n",dist,L);
																			 
						GetMinDistEllipsoid(x1, y1, z1,
						                    x2, y2, z2,
						                    xp, yp, zp,
											prec, &dist, &L);
														 										
											
                        if (dist < mindist) {
                            cPoint[X] = x1*(1.0-L) + x2*L;
                            cPoint[Y] = y1*(1.0-L) + y2*L;
                            cPoint[Z] = z1*(1.0-L) + z2*L;
                            collisionConditionIsMet = 1;
                        } else {
                            collisionConditionIsMet = 0;
                        }

                            if (collisionConditionIsMet) {
/*
 *                              Segments are unconnected and colliding.
 *                              Identify the first node to be merged.  If the
 *                              collision point is close to one of the nodal
 *                              endpoints, use that node, otherwise insert a
 *                              new node in the segment.
 *
 *                              NOTE: The current domain owns node1 but may
 *                              not own node2.  If it does not own node2, we 
 *                              cannot allow the collision to use node2
 *                              even if the collision point is close to 
 *                              that node.
 */
                                adjustCollisionPoint = 0;

                                vec1[X] = cPoint[X] - p1[X];
                                vec1[Y] = cPoint[Y] - p1[Y];
                                vec1[Z] = cPoint[Z] - p1[Z];
    
                                vec2[X] = cPoint[X] - p2[X];
                                vec2[Y] = cPoint[Y] - p2[Y];
                                vec2[Z] = cPoint[Z] - p2[Z];

                                close2node1 = (DotProduct(vec1,vec1)<mindist2);
                                close2node2 = (DotProduct(vec2,vec2)<mindist2);
    
                                if ((node2->myTag.domainID != thisDomain) &&
                                    close2node2) {
                                    continue;
                                }

                                if (close2node1 && hinge!=1) {
                                    targetNode = node1;
                                    splitSeg1 = 0;
                                    adjustCollisionPoint = 1;
                                } else if (close2node2 && hinge!=2) {
                                    targetNode = node2;
                                    splitSeg1 = 0;
                                    adjustCollisionPoint = 1;
                                } else {
                                    splitSeg1 = 1;
                                }

/*
 *                              If we need to add a new node to the first
 *                              segment, do it now.
 */
                                if (splitSeg1) {
                                     real8 pos0[3], pos1[3];

                                     newx = x1 * (1.0-L) + x2*L;
                                     newy = y1 * (1.0-L) + y2*L;
                                     newz = z1 * (1.0-L) + z2*L;
    
                                     newvx = vx1 * (1.0-L) + vx2*L;
                                     newvy = vy1 * (1.0-L) + vy2*L;
                                     newvz = vz1 * (1.0-L) + vz2*L;
    
/*
 *                                   Estimate resulting forces on all segments
 *                                   involved in the split.
 */
                                     arm21 = GetArmID(home, node2, node1);
    
                                     oldfp0[X] = node1->armfx[arm12];
                                     oldfp0[Y] = node1->armfy[arm12];
                                     oldfp0[Z] = node1->armfz[arm12];
    
                                     oldfp1[X] = node2->armfx[arm21];
                                     oldfp1[Y] = node2->armfy[arm21];
                                     oldfp1[Z] = node2->armfz[arm21];
    
                                     pos0[X] = p1[X];   pos1[X] = p2[X];
                                     pos0[Y] = p1[Y];   pos1[Y] = p2[Y];
                                     pos0[Z] = p1[Z];   pos1[Z] = p2[Z];
    
                                     pnew[X] = newx;
                                     pnew[Y] = newy;
                                     pnew[Z] = newz;
    
                                     newNodeVel[X] = newvx;
                                     newNodeVel[Y] = newvy;
                                     newNodeVel[Z] = newvz;

                                     nodeVel[X] = vx1;
                                     nodeVel[Y] = vy1;
                                     nodeVel[Z] = vz1;

                                     burg1[X] = node1->burgX[arm12];
                                     burg1[Y] = node1->burgY[arm12];
                                     burg1[Z] = node1->burgZ[arm12];

                                     FindSubFSeg(home, pos0, pos1, burg1, oldfp0,
                                                 oldfp1, pnew, f0seg1, f1seg1,
                                                 f0seg2, f1seg2);
    
                                     oldTag1 = node1->myTag;
                                     oldTag2 = node2->myTag;

                                     FoldBox(param, &pos0[X], &pos0[Y], &pos0[Z]);
                                     FoldBox(param, &pnew[X], &pnew[Y], &pnew[Z]);

                                     splitStatus = SplitNode(home,
                                                             OPCLASS_COLLISION,
                                                             node1, pos0, pnew,
                                                             nodeVel,
                                                             newNodeVel, 1,
                                                             &arm12, globalOp,
                                                             &splitNode1,
                                                             &splitNode2, 0);
/*
 *									 If this is a hinge collision off of node1
 *									 reset its precipitate and pinned status just
 *								     in case SplitNode messed it up.
 */
									 if (hinge==1) {
										 splitNode1->prec = i;
										 splitNode1->constraint = PINNED_NODE;
									 }
/*
 *                                   If we were unable to split the node
 *                                   go back to looking for more collision
 *                                   candidates.
 */
                                     if (splitStatus == SPLIT_FAILED) {
                                         continue;
                                     }

/*
 *                                   The force estimates above are good enough
 *                                   for the remainder of this timestep, but
 *                                   mark the force and velocity data for
 *                                   some nodes as obsolete so that more
 *                                   accurate forces will be recalculated
 *                                   either at the end of this timestep, or
 *                                   the beginning of the next.
 */
                                     targetNode = splitNode2;

                                     MarkNodeForceObsolete(home, splitNode2);
    
                                     for (q = 0; q < splitNode2->numNbrs; q++) {
                                         tmpNbr = GetNodeFromTag(home, splitNode2->nbrTag[q]);
                                         if (tmpNbr != (Node_t *)NULL) {
                                             tmpNbr->flags |= NODE_RESET_FORCES;
                                         }
                                     }
    
/*
 *                                   Reset nodal forces on nodes involved in the
 *                                   split.
 */
                                     ResetSegForces(home, splitNode1,
                                                    &splitNode2->myTag,
                                                    f0seg1[X], f0seg1[Y],
                                                    f0seg1[Z], 1);
    
                                     ResetSegForces(home, splitNode2,
                                                    &splitNode1->myTag,
                                                    f1seg1[X], f1seg1[Y],
                                                    f1seg1[Z], 1);
    
                                     ResetSegForces(home, splitNode2,
                                                    &node2->myTag,
                                                    f0seg2[X], f0seg2[Y],
                                                    f0seg2[Z], 1);
    
                                     ResetSegForces(home, node2,
                                                    &splitNode2->myTag,
                                                    f1seg2[X], f1seg2[Y],
                                                    f1seg2[Z], 1);
    
                                     (void)EvaluateMobility(home, splitNode1);
                                     (void)EvaluateMobility(home, splitNode2);
                                     (void)EvaluateMobility(home, node2);

/*
 *                                  When debugging, dump some info on
 *                                  topological changes taking place and
 *                                  the nodes involved
 */
#ifdef DEBUG_TOPOLOGY_CHANGES
                                    if ((dbgDom < 0)||(dbgDom == home->myDomain)) {
                                        printf("  Split-1(SegCollision): "
                                               "(%d,%d)--(%d,%d) ==> "
                                               "(%d,%d)--(%d,%d)--(%d,%d)\n",
                                               oldTag1.domainID, oldTag1.index,
                                               oldTag2.domainID, oldTag2.index,
                                               splitNode1->myTag.domainID,
                                               splitNode1->myTag.index,
                                               splitNode2->myTag.domainID,
                                               splitNode2->myTag.index,
                                               node2->myTag.domainID,
                                               node2->myTag.index);
                                        PrintNode(splitNode1);
                                        PrintNode(splitNode2);
                                        PrintNode(node2);
                                     }
#endif
/*
 *                                   When we actually do a collision, we flag
 *                                   it so we don't attempt additional changes
 *                                   on either segment.  It's possible, however
 *                                   that we will split the segment but later
 *                                   determine a collision will not be done.
 *                                   Since the original node1/node2 segment
 *                                   has been bisected, we must treat it as
 *                                   if a collision had occurred so we do not
 *                                   attempt to collide the now non-existent
 *                                   node1/node2 segment.
 */
//                                     didCollision = 1;
									 
                                }  /* if (splitSeg1) */


                               FoldBox(param, &cPoint[X],&cPoint[Y],&cPoint[Z]);
    
/*							    
 *							   Collision "occurs" by flagging the node as pinned
 *							   at precipitate i. The mobility law then sets the
 *							   velocity for that node to zero.
 */							   
							   targetNode->prec = i;
							   targetNode->x = cPoint[X];
							   targetNode->y = cPoint[Y];
							   targetNode->z = cPoint[Z];
							   targetNode->constraint = PINNED_NODE;
							   printf("node flagged at precipitate\n");
							   
							    
/*
 *                             If the target node exists after the merge,
 *                             reset its velocity, and reset the topological
 *                             change exemptions for the target node; use
 *                             NodeTopologyExemptions() to get the basic
 *                             exemptions, then exempt all the node's arms
 *                             from additional segment collisions this cycle.
 */
                               if (targetNode != (Node_t *)NULL) {
/*
 *                                 If we are enforcing glide planes but
 *                                 allowing some fuzziness in the planes, we
 *                                 also need to recalculate the glide 
 *                                 planes for the segments attched to the
 *                                 collision node.
 */
                                   if (param->enforceGlidePlanes &&
                                       param->allowFuzzyGlidePlanes) {
                                       int n;
                                       for (n=0; n<targetNode->numNbrs; n++) {
                                           tmpNbr = GetNodeFromTag(home,
                                                   targetNode->nbrTag[n]);
                                           RecalcSegGlidePlane(home,
                                                               targetNode,
                                                               tmpNbr, 1);
                                       }
                                   }

                                   (void)EvaluateMobility(home, targetNode);

                                   targetNode->flags |= NO_COLLISIONS;
#ifdef DEBUG_LOG_MULTI_NODE_SPLITS
                                   targetNode->multiNodeLife = 0;
#endif
                               }
    
//                               didCollision = 1;
                               localCollisionCnt++;
							   

                            }  /* If conditions for collision are met */
                    }  /* Loop over node1 arms */
                }  /* while(nextIndex >= 0) */
            }  /* Loop over neighboring cell2s */
           }
          }
        }  /* for (i = 0 ...) */	
	
	
		return;
}

/*---------------------------------------------------------------------------
 *
 *      Function:       IntervalCollision
 *      Description:    Apply the interval halving method to segment-precipitate
 *					    collisions.                      
 *
 *      Arguments:
 *          Lout      	Normalized distance along segment of collision point
 *          hitTime     Normalized time of collision
 *          xyz1old     Position of node 1 at start of time step
 *          xyz2old     Position of node 2 at start of time step
 *          xyz1     	Position of node 1 at end of time step
 *          xyz2     	Position of node 2 at end of time step
 *          xyzp     	Position of precipitate centroid
 *			prec		Pointer to prec structure
 *			start		Normalized beginning of interval to test
 *			end			Normalized end of interval to test
 *			dr			Distance traveled by segment during time step
 *			Dout		Separation distance between segments at time loc
 *			loc			flag indicating time corresponding to Dout
 *							loc = 0 --> start
 *							loc = 1 --> end
 *			Dmin		Minimum separation distance over all tested inverals
 *			minTime		Time corresponding to Dmin
 *			Lmin		Normalized distance along segment corresponding to Dmin
 *			n			Counter for number of iterations
 *
 *-------------------------------------------------------------------------*/

static void	IntervalCollision(real8 *Lout, real8 *hitTime, 
  							real8 x1old, real8 y1old, real8 z1old,
  		                    real8 x2old, real8 y2old, real8 z2old,
  		                    real8 x1, real8 y1, real8 z1,
  		                    real8 x2, real8 y2, real8 z2,
  							real8 xp, real8 yp, real8 zp,
  							Prec_t *prec, real8 start, real8 finish, real8 dr, 
  							real8 *Dout, int loc, 
  							real8 *Dmin, real8 *minTime, real8 *LMin, int *n,
                            int OBB)

{

		real8 	eps = 1e-10;
		real8	tol = 1e-2;
		real8	x10, y10, z10, x20, y20, z20; 
		real8	x1f, y1f, z1f, x2f, y2f, z2f;
		real8	R;
		real8	Df, D0;
		real8	L0, Lf;
		real8	dint, maxDistance, mid; 
        real8   tmp1, tmp2, tmp3, tmp4;
		int		nmax = 200;

/*
 *		Increment the iteration counter.
 */
		(*n)++;
		if (*n>nmax) {
			*Lout = 0.0;
			*Dout = 0.0;
			*hitTime = NO_COLLISIONS_TIME;
			return;
		}

/*		
 *		Move the nodes to the positions at the beginning and end of the interval.
 */
		x10 = x1old+start*(x1-x1old);
		y10 = y1old+start*(y1-y1old);
		z10 = z1old+start*(z1-z1old);
		x20 = x2old+start*(x2-x2old);
		y20 = y2old+start*(y2-y2old);
		z20 = z2old+start*(z2-z2old);

		x1f = x1old+finish*(x1-x1old);
		y1f = y1old+finish*(y1-y1old);
		z1f = z1old+finish*(z1-z1old);
		x2f = x2old+finish*(x2-x2old);
		y2f = y2old+finish*(y2-y2old);
		z2f = z2old+finish*(z2-z2old);
		
/*		
 *		If this is the first time we are calling this for collision detection,
 *	    use the bounding sphere of the ellipsoid to do a screening test to
 *	    save computational time.
 */
/*		if (prec->sphere==0) {
		    if (fabs(finish-start-1.0) < eps) {
		        R = prec->rmax;
				GetMinDistSphere(x1old, y1old, z1old, x2old, y2old, z2old, xp, yp, zp, 
									R, &D0, &L0);
				GetMinDistSphere(x1, y1, z1, x2, y2, z2, xp, yp, zp, 
									R, &Df, &Lf);
		        maxDistance = dr;
		        if (maxDistance<D0 || maxDistance<Df) {
					*Lout = 0.0;
					*Dout = 0.0;
					*hitTime = NO_COLLISIONS_TIME;
					return;
		        }
		    }
		}
*/
/*
 *      First we do an approximate minimum distance calculation since the full
 *      calculation is very expensive. If a collision is ruled out with this calculation,
 *      we need to do the 
 */		

/*		
 *		    Calculate min separation distance at start and finish.  
 *		    Depending on which end of the interval was passed through, we
 *		    only need to calculate one. 
 */
		    if (loc==1) {
			    Df = *Dout;
			    Lf = *Lout;
                if (OBB == 1) {
                    OBBSegDistance(x10, y10, z10, 
                                   x20, y20, z20,
                                   xp, yp, zp, 
                                   prec->UT,
                                   prec->r1, prec->r2, prec->r3,
                                   &tmp4, &L0, 
                                   &tmp1, &tmp2, &tmp3);
					D0 = sqrt(tmp4); 
                } else {
			        GetMinDistEllipsoid(x10, y10, z10,
			                            x20, y20, z20,
			                            xp, yp, zp,
								        prec, &D0, &L0);
                }
		    } else {
			    D0 = *Dout;
			    L0 = *Lout;
                if (OBB == 1) {
                    OBBSegDistance(x1f, y1f, z1f, 
                                   x2f, y2f, z2f,
                                   xp, yp, zp, 
                                   prec->UT,
                                   prec->r1, prec->r2, prec->r3,
                                   &tmp4, &Lf, 
                                   &tmp1, &tmp2, &tmp3); 
					Df = sqrt(tmp4);			   
                } else {
			        GetMinDistEllipsoid(x1f, y1f, z1f,
			                            x2f, y2f, z2f,
			                            xp, yp, zp,
								        prec, &Df, &Lf);
                }
		    }

/*		
 *		    Update the minimum distance, locations, and time overall all of time
 */
		    *Dmin = MIN(*Dmin, D0);
		    *Dmin = MIN(*Dmin, Df);
		    if (*Dmin==D0) {
			    *minTime = start;
			    *LMin = L0;
		    } else if (*Dmin==Df) {
			    *minTime = finish;
			    *LMin = Lf;
		    }			

/*		
 *		    Calculate max distance moved by both segments over the interval. 
 */
		    dint = finish-start;
		    maxDistance = dint*dr;
/*
 *		    If the max distance is less than the minimum distance at the beginning
 *		    or end of the interval, there will be no collision in this interval.
 */
		    if (maxDistance<D0 || maxDistance<Df) {
			    *Lout = 0.0;
			    *Dout = 0.0;
			    *hitTime = NO_COLLISIONS_TIME;
			    return;
		    }

/*
 *		If the interval is smaller than the tolerance, consider the segments
 *		as collided.  Take the average of the minimun distance positions
 *		and time interval for output.
 */
		if (finish-start < COL_TOL) {
			//*Lout = (L0+Lf)/2.0;
			*Lout = L0;
			*Dout = 0.0;
			//*hitTime = (start+finish)/2.0;
			//with seg-seg collisions, we using (start+finish)/2, but here
			//we bias to the beginning of the interval because we are going
			//to snap the collision point onto the precipitate anyway
			*hitTime = start;
			return;
		}

/*		
 *		Bisect the interval and continue the recursion.
 */
		mid = (start + finish)/2.0;

/*		
 *		Check for a collision in the first half of the inteveral first.
 *		If no collision there, then look in the second half.
 */

		IntervalCollision(&L0, hitTime, 
		  				  x1old, y1old, z1old, x2old, y2old, z2old,
		  		          x1, y1, z1, x2, y2, z2,
		  				  xp, yp, zp, prec, 
						  start, mid, dr, &D0, 0, 
		  				  Dmin, minTime, LMin, n, OBB);

		if (*n>nmax) {
			*Lout = 0.0;
			*Dout = 0.0;
			*hitTime = NO_COLLISIONS_TIME;
			return;
		}

		if (*hitTime <=1.0) {
			*Lout = L0;
			*Dout = D0;
			return;
		}
							
		IntervalCollision(&Lf, hitTime, 
		  				  x1old, y1old, z1old, x2old, y2old, z2old,
		  		          x1, y1, z1, x2, y2, z2,
		  				  xp, yp, zp, prec, 
						  mid, finish, dr, &Df, 1, 
		  				  Dmin, minTime, LMin, n, OBB);

		*Lout = Lf;
		*Dout = Df;

		return; 
}


/*---------------------------------------------------------------------------
 *
 *      Function:       DetectCollisionPrecipitate
 *      Description:    Given the nodal positions from the last and current
 *						time step and a precipitate, determine if the
 *						segment and precipitate collided by using the
 *						interval halving method.                    
 *
 *      Arguments:
 *          cPoint      Will be set to the coordinates of the collision
 *                      point if the segment and precipitate collided.
 *			collided	Flag indicating if a collision occurred or not
 *          L			Normalized distance along segment at the collision time
 *          cTime       Set to the time (in units of current timestep
 *                      length (deltaTT)) at which the segment and 
 *						precipitate collided.
 *			xyziold		old position of node i
 *			xyzi		position of node i
 *			xyzp		position of precipitate centroid
 *			prec		precipitate structure
 *
 *-------------------------------------------------------------------------*/
static void DetectCollisionPrecipitate(real8 cPoint[3], int *collided,
										real8 *L, real8 *cTime,
										real8 x1old, real8 y1old, real8 z1old,
			                            real8 x2old, real8 y2old, real8 z2old,
			                            real8 x1, real8 y1, real8 z1,
			                            real8 x2, real8 y2, real8 z2,
										real8 xp, real8 yp, real8 zp,
										Prec_t *prec)                                  
{
		real8	drx, dry, drz, dr1, dr2, dr;
		real8	hitTime, Lc, dist2old, distold, dist; 
		real8	x1c, y1c, z1c, x2c, y2c, z2c; 
		real8	distMin, minTime, LMin, distinit;
        real8   tmp1, tmp2, tmp3;
		int		n;
        int     OBB = 1;

/*
 *		Calculate the maximum amount moved by either segment.
 */
		drx = x1-x1old;	dry = y1-y1old;	drz = z1-z1old;
		dr1 = sqrt(drx*drx+dry*dry+drz*drz);

		drx = x2-x2old;	dry = y2-y2old;	drz = z2-z2old;
		dr2 = sqrt(drx*drx+dry*dry+drz*drz);

		dr = MAX(dr1,dr2);

/*
 *      First, test for a collision against the ellipsoid's oriented
 *      bounding box (OBB). This is much cheaper than the ellipsoid
 *      case, and is used to screen away most of the collisions (thereby
 *      saving a lot of time.
 */	

        if (prec->sphere == 0) {

/* 
 *		    Compute the separation distance in the old positions to pass in.
 */

            OBBSegDistance(x1old, y1old, z1old,
		                   x2old, y2old, z2old,
                           xp, yp, zp, 
                           prec->UT,
                           prec->r1, prec->r2, prec->r3,
                           &dist2old, &Lc, 
                           &tmp1, &tmp2, &tmp3); 
						  
			distold = sqrt(dist2old);
		    LMin = Lc;
		    distMin = distold;
		    minTime = 0.0;
		    n = 0;
		
/*
 *		    Begin the interval halving method...
 */
		    IntervalCollision(&Lc, &hitTime, 
						      x1old, y1old, z1old,
		                      x2old, y2old, z2old,
		                      x1, y1, z1,
		                      x2, y2, z2,
							  xp, yp, zp,
							  prec, 0.0, 1.0, dr, 
							  &distold, 0, 
							  &distMin, &minTime, &LMin, &n, 
                              OBB);
    
            if (hitTime == NO_COLLISIONS_TIME) {
				//printf("avoided with OBB %e %e\n",hitTime,distMin);
			    *collided = 0;
			    *cTime = NO_COLLISIONS_TIME;
			    *L = 0.0;
			    cPoint[X] = 0.0;	
			    cPoint[Y] = 0.0;	
			    cPoint[Z] = 0.0;
		        return;
            }
        }
		//printf("made it past OBB\n");
/* 
 *		Compute the separation distance in the old positions to pass in.
 */
		GetMinDistEllipsoid(x1old, y1old, z1old,
		                    x2old, y2old, z2old,
		                    xp, yp, zp,
							prec, &distold, &Lc);
		LMin = Lc;
        distinit = distold;
		distMin = distold;
		minTime = 0.0;
		n = 0;
		
/*
 *		Begin the interval halving method...
 */
		IntervalCollision(&Lc, &hitTime, 
							x1old, y1old, z1old,
		                    x2old, y2old, z2old,
		                    x1, y1, z1,
		                    x2, y2, z2,
							xp, yp, zp,
							prec, 0.0, 1.0, dr, 
							&distold, 0, 
							&distMin, &minTime, &LMin, &n, 
                            !OBB);

/*
 *      If the collision time is less than the collision tolerance
 *      and the segment and precipitate started at a separation 
 *      distance greater than zero, it is likely a collision did not
 *      actually occur. Prevent the collision from being detected,
 *      unless distMin<0 with COL_TOL, as tested below.
 */
/*
        if (hitTime<COL_TOL && distinit>0.0) {
    		x1c = x1old+COL_TOL*(x1-x1old);
		    y1c = y1old+COL_TOL*(y1-y1old);
		    z1c = z1old+COL_TOL*(z1-z1old);
		    x2c = x2old+COL_TOL*(x2-x2old);
		    y2c = y2old+COL_TOL*(y2-y2old);
		    z2c = z2old+COL_TOL*(z2-z2old);
            GetMinDistEllipsoid(x1c, y1c, z1c,
		                        x2c, y2c, z2c,
		                        xp, yp, zp,
							    prec, &distold, &Lc);
            if (distold>0.0) {
                hitTime = NO_COLLISIONS_TIME;
            }
        }	
*/						

/*
 *		If the returned hit time (as a fraction of the last time step)
 *		is less than or equal to 1, there was a collision.  
 */
		if (hitTime<=1.0) {  
			x1c = x1old+hitTime*(x1-x1old);
			y1c = y1old+hitTime*(y1-y1old);
			z1c = z1old+hitTime*(z1-z1old);
			x2c = x2old+hitTime*(x2-x2old);
			y2c = y2old+hitTime*(y2-y2old);
			z2c = z2old+hitTime*(z2-z2old);

			cPoint[X] = x1c+Lc*(x2c-x1c);
			cPoint[Y] = y1c+Lc*(y2c-y1c);
			cPoint[Z] = z1c+Lc*(z2c-z1c);
			*collided = 1;
			*cTime = hitTime;
			*L = Lc;
		// } else if (distMin<0.1*prec->rmin) {
		// 	x1c = x1old+minTime*(x1-x1old);
		// 	y1c = y1old+minTime*(y1-y1old);
		// 	z1c = z1old+minTime*(z1-z1old);
		// 	x2c = x2old+minTime*(x2-x2old);
		// 	y2c = y2old+minTime*(y2-y2old);
		// 	z2c = z2old+minTime*(z2-z2old);
		//
		// 	cPoint[X] = x1c+LMin*(x2c-x1c);
		// 	cPoint[Y] = y1c+LMin*(y2c-y1c);
		// 	cPoint[Z] = z1c+LMin*(z2c-z1c);
		// 	*collided = 1;
		// 	*cTime = minTime;
		// 	*L = LMin;
		} else {
			*collided = 0;
			*cTime = NO_COLLISIONS_TIME;
			*L = 0.0;
			cPoint[X] = 0.0;	
			cPoint[Y] = 0.0;	
			cPoint[Z] = 0.0;
		}
		return;
}

/*---------------------------------------------------------------------------
 *
 *      Function:       HingeCollisionPrecipitate
 *      Description:    Given a the old and current positions for the free
 *						end of a hinge segment, determine if a collision
 *						occurs and return the collision point and time.                   
 *
 *      Arguments:
 *          cPoint      coordinates of collision point
 *			collided	flag indicating if a collision occurs
 *			(xp,yp,zp)	coordinates of precipitate's center
 *          (x,y,z)		coordiantes of free end of colliding segment
 *			(xold,yold,zold)	old coordinates
 *          (nx,ny,nz)	glide plane normal of segment
 *			param		param structure
 *			prec		precipitate structure
 *
 *-------------------------------------------------------------------------*/

void HingeCollisionPrecipitate(real8 cPoint[3], int *collided, Param_t *param,
					      real8 *cTime, real8 nx, real8 ny, real8 nz, 
						  real8 xold, real8 yold, real8 zold,
		                  real8 x, real8 y, real8 z,
					      real8 xp, real8 yp, real8 zp, Prec_t *prec) {
		
		int		projectSuccess;
		real8	dist, L;					 
		real8 	xdir, ydir, zdir;
		real8 	dxhit, dyhit, dzhit, dlhit2, dl2;
		real8	eps = 1e-6;
		
		projectSuccess = 1;
		
/*
 *		The collision occurs if either the current position of the free
 *		end is inside the precipitate, or the trajectory of the free
 *		end intersects the precipitate.
 */
		*collided = 
		  InsideEllipsoid(x, y, z, xp, yp, zp, prec,
		  (real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL);

		if (*collided == 0) {
			GetMinDistEllipsoid(xold, yold, zold,
	   		                    x, y, z,
	   		                    xp, yp, zp,
	   							prec, &dist, &L);
			*collided = (dist<= 0.0);
		}

/*
 *		If a collision occurred, find where along the trajectory that
 *		the point interesected the precipitate's surface - this is the
 *		collision point.
 */	  
		if (*collided) {


			cPoint[X] = xold;
			cPoint[Y] = yold;
			cPoint[Z] = zold;
			
			if (InsideEllipsoid(xold, yold, zold, xp, yp, zp, prec,
			(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL)==0) {
				xdir = x-xold;
				ydir = y-yold;
				zdir = z-zold;
				projectSuccess = EllipsoidProject(&cPoint[X],&cPoint[Y],&cPoint[Z],
									               xdir, ydir, zdir,
									               xp, yp, zp, prec);
				if (projectSuccess == 0) {
					printf("EllipsoidProject failed\n");
				}								   
				dxhit = cPoint[X] - xold;
				dyhit = cPoint[Y] - yold;
				dzhit = cPoint[Z] - zold;
				dlhit2 = dxhit*dxhit + dyhit*dyhit + dzhit*dzhit;
				dl2 = xdir*xdir + ydir*ydir + zdir*zdir; 
				*cTime = dlhit2/dl2;
			} else {
				*cTime = 0.0;
				projectSuccess = ProjectCollisionPoint(NULL, cPoint,
														xp, yp, zp,
														x, y, z, x, y, z,
														xold, yold, zold, xold, yold, zold,
														nx, ny, nz,
														param, prec);
				if (projectSuccess == 0) {
					printf("ProjectCollisionPoint failed2\n");
				}	
			}
		} else {
			*cTime = NO_COLLISIONS_TIME;
			cPoint[X] = 0.0;	
			cPoint[Y] = 0.0;	
			cPoint[Z] = 0.0;
		}
		
        if ((projectSuccess == 0) || (*cTime > 1.0)) {
			*collided = 0;
            *cTime = NO_COLLISIONS_TIME;
			cPoint[X] = 0.0;	
			cPoint[Y] = 0.0;	
			cPoint[Z] = 0.0;
        }					
					
}

/*---------------------------------------------------------------------------
 *
 *      Function:       ProjectCollisionPoint
 *      Description:    Given a node, a precipitate, and a collision point,
 *						project the collision point onto the surface of the
 *						precipitate  along the local ourward normal. 
 *						We need to satisfy all glide constraints.
 *
 *						There are two modes of operation:
 *						1) If enforceGlidePlanes is enabled, use predefined
 *						   glide planes.
 *						2) Otherwise, use the nodal trajectories to define
 *						   glide planes.
 *
 *						In some instances instead of passing in a node we pass
 *						in a glide plane normal.                    
 *
 *      Arguments:
 *			node		(not always specified) node whose position is to be 
 *						projected
 *          cPoint      coordinates of collision point
 *			(xp,yp,zp)	coordinates of precipitate's center
 *          (xi,yi,zi)	coordiantes of node i of colliding segment
 *			(xiold,yiold,ziold)	old coordinates
 *          (nx,ny,nz)	(not always specified) glide plane normal to satisfy
 *			param		param structure
 *			prec		precipitate structure
 *
 *-------------------------------------------------------------------------*/

int ProjectCollisionPoint(Node_t *node, real8 cPoint[3],
						   real8 xp, real8 yp, real8 zp,
						   real8 x1, real8 y1, real8 z1,
						   real8 x2, real8 y2, real8 z2,
						   real8 x1old, real8 y1old, real8 z1old,
						   real8 x2old, real8 y2old, real8 z2old,
						   real8 nx, real8 ny, real8 nz,
						   Param_t *param, Prec_t *prec) {
	 
		 int	i, j, numplanes, projectSuccess; 
		 real8 	vec1[3], vec2[3], tmpvec[3];
		 real8	xdir, ydir, zdir;
		 real8	nxtmp, nytmp, nztmp, planes[30];
		 real8	eps = 1e-10;
	 
 	
		vec1[X] = xp - cPoint[X];
		vec1[Y] = yp - cPoint[Y];
		vec1[Z] = zp - cPoint[Z];
				
		if (prec->sphere) {
			xdir = vec1[X];
			ydir = vec1[Y];
			zdir = vec1[Z];
		} else {
			Matrix33Vector3Multiply(prec->A, vec1, vec2);
			xdir = vec2[X];
			ydir = vec2[Y];
			zdir = vec2[Z];
		}				
		
		//printf("before %e %e %e\n",cPoint[X],cPoint[Y],cPoint[Z]);
		
		if (param->enforceGlidePlanes) {

			if (node == NULL) {
				Orthogonalize(&xdir,&ydir,&zdir,
								nx,ny,nz);
				projectSuccess = EllipsoidProject(&cPoint[X],&cPoint[Y],&cPoint[Z],
								                  xdir, ydir, zdir,
								                  xp, yp, zp, prec);
                if (projectSuccess == 0) return(0);
			} else {
/*
* 						Determine how many unique glide planes the node is on.
*/
		   		for (i = 0; i < node->numNbrs; i++) {
		   			nxtmp = node->nx[i];
		   			nytmp = node->ny[i];
		   			nztmp = node->nz[i];
		   			Normalize(&nxtmp,&nytmp,&nztmp);
		   			if (i==0) {
		   				planes[0] = nxtmp;
		   				planes[1] = nytmp;
		   				planes[2] = nztmp;
		   				numplanes = 1;
		   			} else {
		   				for (j=0; j<numplanes; j++) {
		   					Orthogonalize(&nxtmp,&nytmp,&nztmp,
		   									planes[3*j],planes[3*j+1],planes[3*j+2]);
		   				}
		   				if ((nxtmp*nxtmp+nytmp*nytmp+nztmp*nztmp)>eps) {
		   					Normalize(&nxtmp,&nytmp,&nztmp);
		   					planes[3*numplanes] = nxtmp;
		   					planes[3*numplanes+1] = nytmp;
		   					planes[3*numplanes+2] = nztmp;
		   					numplanes++;
		   				}
		   			}
		   		}
				if (numplanes<=2) {
					if (numplanes==1) {
	   					Orthogonalize(&xdir,&ydir,&zdir,
	   									planes[0],planes[1],planes[2]);
					} else if (numplanes==2) {
						vec1[0] = planes[0]; vec1[1] = planes[1]; vec1[2] = planes[2];
						vec2[0] = planes[3]; vec2[1] = planes[4]; vec2[2] = planes[5];
						cross(vec1, vec2, tmpvec);
						xdir = tmpvec[X]; ydir = tmpvec[Y]; zdir = tmpvec[Z];
					}
					projectSuccess = EllipsoidProject(&cPoint[X],&cPoint[Y],&cPoint[Z],
									                  xdir, ydir, zdir,
									                  xp, yp, zp, prec);
                    if (projectSuccess == 0) return(0);
				} else {
					//something is wrong - do nothing
					printf("more than 2 planes in ProjectCollisionPoint\n");
				}
			}
		} else {
			//use the displacement direction and line direction to
			//determine the glide plane
			
			//THIS CODE IS UNTESTED!
			vec1[X] = 0.5*((x1-x1old)+(x2-x2old));
			vec1[Y] = 0.5*((y1-y1old)+(y2-y2old));
			vec1[Z] = 0.5*((z1-z1old)+(z2-z2old));
			
			vec2[X] = x1-x2;
			vec2[Y] = y1-y2;
			vec2[Z] = z1-z2;
			
			cross(vec1, vec2, tmpvec);
			if (DotProduct(tmpvec,tmpvec)>eps) {
				Orthogonalize(&xdir,&ydir,&zdir,
								tmpvec[X],tmpvec[Y],tmpvec[Z]);
			
				projectSuccess = EllipsoidProject(&cPoint[X],&cPoint[Y],&cPoint[Z],
								                  xdir, ydir, zdir,
								                  xp, yp, zp, prec);
                if (projectSuccess == 0) return(0);
			}
		}
		
		return(1);
 }	

/*
 *	Define the structure for storing collision info.
 */
typedef struct {
	Node_t		*node1, *node2;
	Prec_t		*prec;
	real8		cTime, cPoint[3], L;
	real8		p1[3], p2[3];
	real8		x1, y1, z1, x2, y2, z2;
	real8		x1old, y1old, z1old, x2old, y2old, z2old;
	real8		xp, yp, zp;
	int			coldone, hinge;
} Coldat_t;



/*-------------------------------------------------------------------------
 *
 *      Function:     PrecipitateCollisions
 *      Description:  Loop over all precipitate-segment pairs in the same
 *					  or neighboring cell2s, and detect and handle 
 *					  collisions.
 *
 *					  This code uses the interval halving method for collision
 *					  detection, using the same algorithm as
 *					  RetroactiveCollisions in the implicit folder.		
 *
 *-----------------------------------------------------------------------*/

void PrecipitateCollisions(Home_t *home) {
        int     i, j, k, q, arm12, arm21, arm34, arm43;
        int     thisDomain, splitStatus, mergeStatus, precStatus;
        int     armAB, armBA;
        int     globalOp = 1, didCollision;
		int		insidetest1, insidetest2;
        int     close2node1, close2node2, close2node3, close2node4;
        int     splitSeg1, splitSeg2;
        int     cell2Index, nbrCell2Index, nextIndex;
        int     cell2X, cell2Y, cell2Z, cx, cy, cz;
        int     localCollisionCnt, globalCollisionCnt;
        int     collisionConditionIsMet;
        int     type, alreadydone, ndone, firstPass;
		int 	hinge, col, colnum, morecollisions;
		int		numplanes;
        int     projectSuccess;
	int	cxmin, cxmax, cymin, cymax, czmin, czmax;
        real8   mindist, mindist2, dist, L, eps, half;
        real8   cTime, cPoint[3], earliestcTime;
        real8   x1, y1, z1, vx1, vy1, vz1;
        real8   x2, y2, z2, vx2, vy2, vz2;
		real8	xp, yp, zp;
		real8	x1old, y1old, z1old, x2old, y2old, z2old;
        real8   dxhit, dyhit, dzhit, dlhit2, dl2;
		real8	xc1, yc1, zc1, xc2, yc2, zc2;
		real8	xtmp, ytmp, ztmp;
		real8	xdir, ydir, zdir;
        real8   newx, newy, newz;
        real8   newvx, newvy, newvz;
        real8   pnew[3], burg1[3], burg2[3];
        real8   oldfp0[3], oldfp1[3];
        real8   f0seg1[3], f1seg1[3], f0seg2[3], f1seg2[3];
        real8   nodeVel[3], newNodeVel[3];
        real8   newPos[3], newVel[3];
        real8   vec1[3], vec2[3], tmpvec[3];
        real8   p1[3], p2[3], v1[3], v2[3];
		real8	r1, len, lenold, frac, fracold;
		real8	nxtmp, nytmp, nztmp, planes[30];
        Tag_t   oldTag1, oldTag2;
        Node_t  *node1, *node2, *tmpNbr;
        Node_t  *targetNode;
        Node_t  *splitNode1, *splitNode2;
        Param_t *param;
		Prec_t  *prec;

        thisDomain = home->myDomain;
        param      = home->param;

/* QUESTION!  What is reasonable for mindist2? */
		mindist = 1; //param->rann;
        mindist2 = mindist*mindist;
        eps      = 1.0e-12;
        half     = 0.5;

        localCollisionCnt = 0;
        globalCollisionCnt = 0;

#ifdef DEBUG_TOPOLOGY_DOMAIN
        dbgDom = DEBUG_TOPOLOGY_DOMAIN;
#else
        dbgDom = -1;
#endif
		
		Coldat_t *colarray = malloc(2*MAX_COLS * sizeof(Coldat_t));

        real8	**done_points   = malloc(MAX_COLS*sizeof(real8 *));
        for(i=0;i<MAX_COLS;i++){			
			done_points[i]      = malloc(3 * sizeof(real8));
        }

/*
 *		Initialize collision variables.
 */
		earliestcTime = NO_COLLISIONS_TIME;
		cTime = NO_COLLISIONS_TIME;
		colnum = 0;
		morecollisions = 1;
        ndone = 0;
		firstPass = 1;
		
/*
 *		Unflag all nodes to start.
 */
       	for (i = 0; i < home->newNodeKeyPtr; i++) {
           	if ((tmpNbr = home->nodeKeys[i]) == (Node_t *)NULL) continue;
   			tmpNbr->flags &= ~NO_COLLISIONS_LOCAL;
   		}
		
/*
 *		Loop until all collisions are handled.
 */
		while (morecollisions) {
			
		//SortNodesForCollision(home);
		SortNodes(home,&home->prec_cell2_width);			

/*
 *		We loop over all possible precipitate-segment pairs that are collision
 *		candidates twice. 
 *		
 *		During the type=1 pass, we only look for hinge
 *		collisions. If a segment experiences a hinge collisions, its nodes
 *		are then flagged so that no additional collision tests are performed
 *		until after the next collision is handled.
 *		
 *		During the type=2 pass we look for non-hinge collisions only.
 *		
 *		We implement it in this way because hinge collisions are much 
 *		cheaper to detect, so they get priority. Often handling a hinge
 *		collision negates the need to test for a non-hinge collisions.
 */ 
		for (type = 1; type <= 2; type++) {
			
/*
 *      Start looping through native precipiatates looking for segments to collide...
 */
        for (i = 0; i < home->newPrecKeyPtr; i++) {
			
			prec = home->precKeys[i];
            
			if (prec == (Prec_t *)NULL) continue;
			
/*
 *			If the precipitate has no cutting resistance, don't bother 
 *			handling collisions with it.
 */
			if (fabs(prec->Fcut) < eps) continue;
			
			xp = prec->x; yp = prec->y; zp = prec->z;

/*
 *          Loop through all cell2s neighboring the prec.  Only
 *          nodes in these neighboring cell2s are candidates for
 *          collisions.
 *
 *          NOTE: All nodes are assigned cell2 membership prior to
 *          entering collision handling.  However, nodes added during
 *          node separation and collision handling are not given
 *          cell2 membership for this cycle.  In most cases this
 *          is not a problem since those nodes have usually been
 *          exempted from subsequent collisions this cycle.  There
 *          are certain circumstances, though, in which the exemptions
 *          have not been set.  If we encounter one such node that
 *          has no cell2 membership, skip it for this cycle, or bad
 *          things happen.
 */
            cell2Index = prec->cell2Idx;
            if (cell2Index < 0) {
                continue;
            }

            DecodeCell2Idx(home, cell2Index, &cell2X, &cell2Y, &cell2Z);

	    if (home->usePrecCell2 == 1) {
		cxmin = cell2X - 1;
		cxmax = cell2X + 1;

                cymin = cell2Y - 1;
                cymax = cell2Y + 1;

                czmin = cell2Z - 1;
                czmax = cell2Z + 1;
	    } else {
		cxmin = 1;
		cxmax = home->cell2nx - 1;

                cymin = 1;
                cymax = home->cell2ny - 1;

                czmin = 1;
                czmax = home->cell2nz - 1;
	    }

            for (cx = cxmin; cx <= cxmax; cx++) {
             for (cy = cymin; cy <= cymax; cy++) {
              for (cz = czmin; cz <= czmax; cz++) {

                nbrCell2Index = EncodeCell2Idx(home, cx, cy, cz);

/*
 *              Loop though all nodes in the neighbor cell2
 */
                nextIndex = home->cell2[nbrCell2Index];

                while (nextIndex >= 0) {

                   // if (didCollision) break;

                    node1 = home->cell2QentArray[nextIndex].node;
                    nextIndex = home->cell2QentArray[nextIndex].next;

                    if (node1 == (Node_t *)NULL) continue;
                    if (node1->flags & NO_COLLISIONS) continue;
                    if (node1->flags & NO_COLLISIONS_LOCAL) continue;

/*
 *                  Loop over all arms of node1.  Skip any arms that
 *                  terminate at node3 (those hinge arms will be dealt
 *                  with later)
 */
                    for (arm12 = 0; arm12 < node1->numNbrs; arm12++) {

                        node2 = GetNodeFromTag(home, node1->nbrTag[arm12]);

                        if (node2 == (Node_t *)NULL) continue;
                        if (node2->flags & NO_COLLISIONS) continue;
                        if (node2->flags & NO_COLLISIONS_LOCAL) continue;

                        if (CollisionNodeOrder(home, &node1->myTag,
                                               &node2->myTag) > 0) {
                            continue;
                        }

/*
 *                      Segment node1/node2 may only be used in a collision
 *                      if the segment is owned by the current domain.
 */
                        if (!DomainOwnsSeg(home, OPCLASS_COLLISION,
                                           thisDomain, &node2->myTag)) {
                            continue;
                        }

/*
 *						If both nodes are already pinned at a precipiate
 *						then they can't collide with a precipitate.
 */						
						if (node1->prec>=0 && node2->prec>=0) { 
							continue;
						}
/*
 *						Determine if this is a hinge collisions.
 */
						if (node1->prec==i) {
							hinge = 1;
						} else if (node2->prec==i) {
							hinge = 2;						
						} else {
							hinge = 0;
						}
						
/*
 *						Test to see if this segment is the proper type of
 *						collision.
 *						type == 1 ---> hinge
 *						type == 2 ---> non-hinge
 */ 
						if (((hinge == 0) && (type == 1)) ||
						    ((hinge > 0)  && (type == 2))) {
								continue;
						}
						
/*
 *						Test if any neighbors of either node are attached
 *						to this precipitate. If they are, this is a hinge
 *						collision so no need to test this segment.
 */
						/* this code doesn't work!!!!
						alreadydone = 0;
						for (j=0; j<node1->numNbrs; j++) {
							tmpNbr = GetNeighborNode(home,node1,j);
							if (j == arm12) continue;
							if (tmpNbr->prec == i) {
								alreadydone = 1;
								break;
							}
						}
						arm21 = GetArmID(home, node2, node1);	
						for (j=0; j<node2->numNbrs; j++) {
							tmpNbr = GetNeighborNode(home,node2,j);
							if (j == arm21) continue;
							if (tmpNbr->prec == i) {
								alreadydone = 1;
								break;
							}
						}
						if (alreadydone) continue;	
						*/				

                        x1 = node1->x; y1 = node1->y; z1 = node1->z;
                        x2 = node2->x; y2 = node2->y; z2 = node2->z;
						
                        x1old = node1->olderx; y1old = node1->oldery; z1old = node1->olderz;
                        x2old = node2->olderx; y2old = node2->oldery; z2old = node2->olderz;						

                        PBCPOSITION(param, xp, yp, zp, &x1, &y1, &z1);
                        PBCPOSITION(param, x1, y1, z1, &x2, &y2, &z2);
						
                        PBCPOSITION(param, x1, y1, z1, &x1old, &y1old, &z1old);
                        PBCPOSITION(param, x1old, y1old, z1old, &x2old, &y2old, &z2old);						

/*
 *						If either node started the time step inside the precipitate
 *						and is not pinned to a precipitate,	then we assume it was just 
 *						released and skip this segment.
 */
/*
						insidetest1 = InsideEllipsoid(x1old, y1old, z1old, xp, yp, zp, prec,
								(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL)
									  && (node1->prec<0);
						insidetest2 = InsideEllipsoid(x2old, y2old, z2old, xp, yp, zp, prec,
								(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL)
									  && (node2->prec<0);

						if(insidetest1 || insidetest2) continue; 
*/
						
						if (hinge == 0) {
				            OBBSegDistance(x1old, y1old, z1old,
						                   x2old, y2old, z2old,
				                           xp, yp, zp, 
				                           prec->UT,
				                           prec->r1, prec->r2, prec->r3,
				                           &dist, &L, 
				                           &xtmp, &ytmp, &ztmp); 
						   if (dist <= 0.0) {
						   		GetMinDistEllipsoid(x1old, y1old, z1old,
						   		                    x2old, y2old, z2old,
						   		                    xp, yp, zp,
						   							prec, &dist, &L);
								if (dist <= 0.0) {
#ifdef DEBUG_PREC_IMPENETRABLE
/*
 *									Print info on this segment if the 
 *									precipitate is impenetrable.
 */	
									if (prec->Fcut<0.0 && firstPass) {
										printf("Found segment penetrating an impenetrable precipitate %i!\n",i);
										printf("Node 1 is %i,%i at x=%e y=%e z=%e\n",
												node1->myTag.domainID,node1->myTag.index,
												node1->x,node1->y,node1->z);
										printf("Node 2 is %i,%i at x=%e y=%e z=%e\n",
												node2->myTag.domainID,node2->myTag.index,
												node2->x,node2->y,node2->z);
									}
#endif
									//if (prec->Fcut>=0.0) 
									continue;
								}	
						   }
						}
						
                        p1[X] = x1;  p1[Y] = y1;  p1[Z] = z1;
                        p2[X] = x2;  p2[Y] = y2;  p2[Z] = z2;

/*
 *                      It is possible to have a zero-length segment
 *                      (created by a previous collision).  If we find
 *                      such a segment, do not try to use it in any
 *                      subsequent collisions.
 */
                        vec1[X] = x2 - x1;
                        vec1[Y] = y2 - y1;
                        vec1[Z] = z2 - z1;

						len = sqrt(DotProduct(vec1, vec1));
						
                        if (len < 1.0e-20) {
                            continue;
                        }
						
                        vec1[X] = x2old - x1old;
                        vec1[Y] = y2old - y1old;
                        vec1[Z] = z2old - z1old;

						lenold = sqrt(DotProduct(vec1, vec1));
						
/* 
 * 					    If one node is already pinned on this precipitate
 * 						we need to offset the position of the other node.
 */
						frac = MIN(prec->lloop/len,1.0);
						fracold = MIN(prec->lloop/lenold,1.0);
/*
						if (node1->prec==i) {
							hinge = 1;
							x1 = x1*(1.0-frac) + x2*frac;
							y1 = y1*(1.0-frac) + y2*frac;
							z1 = z1*(1.0-frac) + z2*frac;
							x1old = x1old*(1.0-fracold) + x2old*fracold;
							y1old = y1old*(1.0-fracold) + y2old*fracold;
							z1old = z1old*(1.0-fracold) + z2old*fracold;
						} else if (node2->prec==i) {
							hinge = 2;
							x2 = x2*(1.0-frac) + x1*frac;
							y2 = y2*(1.0-frac) + y1*frac;
							z2 = z2*(1.0-frac) + z1*frac;
							x2old = x2old*(1.0-fracold) + x1old*fracold;
							y2old = y2old*(1.0-fracold) + y1old*fracold;
							z2old = z2old*(1.0-fracold) + z1old*fracold;							
						} else {
							hinge = 0;
						}
*/

/*
 *                      If it is a hinge collision, all we need to do is test for
 *                      whether the current position is inside the precipitate.
 *                      Otherwise we need to do a full collision test.
 */
						if (hinge == 1) {
							x1 = x1*(1.0-frac) + x2*frac;
							y1 = y1*(1.0-frac) + y2*frac;
							z1 = z1*(1.0-frac) + z2*frac;
							x1old = x1old*(1.0-fracold) + x2old*fracold;
							y1old = y1old*(1.0-fracold) + y2old*fracold;
							z1old = z1old*(1.0-fracold) + z2old*fracold;
							/*
						    collisionConditionIsMet = 
                                InsideEllipsoid(x1, y1, z1, xp, yp, zp, prec,
								(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL);
                            if (collisionConditionIsMet) {
                                xdir = x1-x1old;
                                ydir = y1-y1old;
                                zdir = z1-z1old;
                                cPoint[X] = x1old;
                                cPoint[Y] = y1old;
                                cPoint[Z] = z1old;
				                EllipsoidProject(&cPoint[X],&cPoint[Y],&cPoint[Z],
								                 xdir, ydir, zdir,
								                 xp, yp, zp, prec);
                                L = 0.0;
                                dxhit = cPoint[X] - x1old;
                                dyhit = cPoint[Y] - y1old;
                                dzhit = cPoint[Z] - z1old;
                                dlhit2 = dxhit*dxhit + dyhit*dyhit + dzhit*dzhit;
                                dl2 = xdir*xdir + ydir*ydir + zdir*zdir; 
                                cTime = dlhit2/dl2;
                                if (cTime > 1.0) cTime = 0.0;
                            }
							*/
							L = 0.0;
							HingeCollisionPrecipitate(cPoint, &collisionConditionIsMet,
												       param, &cTime, node1->nx[arm12], node1->ny[arm12], node1->nz[arm12],
									                   x1old, y1old, z1old,
									                   x1, y1, z1,
												       xp, yp, zp, prec);
						} else if (hinge == 2) {
							x2 = x2*(1.0-frac) + x1*frac;
							y2 = y2*(1.0-frac) + y1*frac;
							z2 = z2*(1.0-frac) + z1*frac;
							x2old = x2old*(1.0-fracold) + x1old*fracold;
							y2old = y2old*(1.0-fracold) + y1old*fracold;
							z2old = z2old*(1.0-fracold) + z1old*fracold;
							/*
						    collisionConditionIsMet = 
                                InsideEllipsoid(x2, y2, z2, xp, yp, zp, prec,
								(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL);
                            if (collisionConditionIsMet) {
                                xdir = x2-x2old;
                                ydir = y2-y2old;
                                zdir = z2-z2old;
                                cPoint[X] = x2old;
                                cPoint[Y] = y2old;
                                cPoint[Z] = z2old;
				                EllipsoidProject(&cPoint[X],&cPoint[Y],&cPoint[Z],
								                 xdir, ydir, zdir,
								                 xp, yp, zp, prec);
                                L = 1.0;
                                dxhit = cPoint[X] - x2old;
                                dyhit = cPoint[Y] - y2old;
                                dzhit = cPoint[Z] - z2old;
                                dlhit2 = dxhit*dxhit + dyhit*dyhit + dzhit*dzhit;
                                dl2 = xdir*xdir + ydir*ydir + zdir*zdir; 
                                cTime = dlhit2/dl2;
                                if (cTime > 1.0) {
									cTime = 0.0;
									printf("%e %e %i\n",dlhit2,dl2,InsideEllipsoid(x2old, y2old, z2old, xp, yp, zp, prec,
								(real8 *)NULL, (real8 *)NULL, (real8 *)NULL, (real8 *)NULL));
								}
                            }*/
							L = 1.0;
							HingeCollisionPrecipitate(cPoint, &collisionConditionIsMet,
													  param, &cTime, node1->nx[arm12], node1->ny[arm12], node1->nz[arm12],
									                   x2old, y2old, z2old,
									                   x2, y2, z2,
												       xp, yp, zp, prec);						
						} else {
						    DetectCollisionPrecipitate(cPoint, &collisionConditionIsMet,
												       &L, &cTime,
												       x1old, y1old, z1old,
									                   x2old, y2old, z2old,
									                   x1, y1, z1,
									                   x2, y2, z2,
												       xp, yp, zp, prec);
						}
						
/*
 *						If this is a hinge collision, we are guaranteed not to have
 *						had a collision if the angle between the segment and the
 *						precipitate normal is less than 90 degrees.
 */
/*
						if (hinge>0) { 
							if (hinge==1) {
				   				vec1[X] = p1[X] - xp;
				   				vec1[Y] = p1[Y] - yp;
				   				vec1[Z] = p1[Z] - zp;
				   				vec2[X] = x2 - p1[X];
				   				vec2[Y] = y2 - p1[Y];
				   				vec2[Z] = z2 - p1[Z];
							} else {
				   				vec1[X] = p2[X] - xp;
				   				vec1[Y] = p2[Y] - yp;
				   				vec1[Z] = p2[Z] - zp;
				   				vec2[X] = x1 - p2[X];
				   				vec2[Y] = y1 - p2[Y];
				   				vec2[Z] = z1 - p2[Z];
							}
							real8 np[3];
			   				if (prec->sphere) {
			   					VECTOR_COPY(np,vec1);
			   				} else {
			   					Matrix33Vector3Multiply(prec->A, vec1, np);
			   				}
							NormalizeVec(np);
							NormalizeVec(vec2);															   
							if (DotProduct(np,vec2)>0.0-eps) continue; 
					   }
*/

/*
 *                      Find the minimum distance between the two segments
 *                      and determine if they should be collided.
 */
/*
						DetectCollisionPrecipitate(cPoint, &collisionConditionIsMet,
												   &L, &cTime,
												   x1old, y1old, z1old,
									               x2old, y2old, z2old,
									               x1, y1, z1,
									               x2, y2, z2,
												   xp, yp, zp, prec);
*/

/*
 *						If we have already handled a collision with this
 *						collision point, don't add this to the list.
 */
						real8 dx, dy ,dz;
						alreadydone = 0;
						if (ndone>=MAX_COLS) break;
						for (j=0; j<ndone; j++) {
							dx = cPoint[X]-done_points[j][X];
							dy = cPoint[Y]-done_points[j][Y];
							dz = cPoint[Z]-done_points[j][Z];
							ZImage(param,&dx,&dy,&dz);
							if ((dx*dx+dy*dy+dz*dz)<0.01*prec->lloop*prec->lloop) {
								alreadydone = 1;
								break;
							} 
						}
						if (alreadydone) continue;
/*
 *						Add the info for this collision to the collisions array.
 */											  
			      		if (colnum>=MAX_COLS) break; 
                        if (collisionConditionIsMet) {
							colarray[colnum].node1 = node1;
							colarray[colnum].node2 = node2;
							colarray[colnum].cTime = cTime;
							colarray[colnum].cPoint[X] = cPoint[X];
							colarray[colnum].cPoint[Y] = cPoint[Y];
							colarray[colnum].cPoint[Z] = cPoint[Z];
							colarray[colnum].L = L;
							colarray[colnum].p1[X] = p1[X];
							colarray[colnum].p1[Y] = p1[Y];
							colarray[colnum].p1[Z] = p1[Z];
							colarray[colnum].p2[X] = p2[X];
							colarray[colnum].p2[Y] = p2[Y];
							colarray[colnum].p2[Z] = p2[Z];
							colarray[colnum].x1 = x1;
							colarray[colnum].y1 = y1;
							colarray[colnum].z1 = z1;
							colarray[colnum].x1old = x1old;
							colarray[colnum].y1old = y1old;
							colarray[colnum].z1old = z1old;
							colarray[colnum].x2 = x2;
							colarray[colnum].y2 = y2;
							colarray[colnum].z2 = z2;
							colarray[colnum].x2old = x2old;
							colarray[colnum].y2old = y2old;
							colarray[colnum].z2old = z2old;
							colarray[colnum].xp = xp;
							colarray[colnum].yp = yp;
							colarray[colnum].zp = zp;
							colarray[colnum].coldone = 0;
							colarray[colnum].hinge = hinge;
							colarray[colnum].prec = prec;
							colnum++;
							if (type == 1) {
								node1->flags |= NO_COLLISIONS_LOCAL;
								node2->flags |= NO_COLLISIONS_LOCAL;
							}
                        }  /* If conditions for collision are met */
                    }  /* Loop over node1 arms */
                }  /* while(nextIndex >= 0) */
            }  /* Loop over neighboring cell2s */
           }
          }
        }  /* for (i = 0 ...) */
		}  /* for (type = 0 ...) */
		
		firstPass = 0;

/*		
 *		Flag everything for no collisions so we don't duplicate searches.
 *		We will unflag modified nodes below to redo searches associated
 *		with those nodes.
 */
    	for (i = 0; i < home->newNodeKeyPtr; i++) {
        	if ((tmpNbr = home->nodeKeys[i]) == (Node_t *)NULL) continue;
			tmpNbr->flags |= NO_COLLISIONS_LOCAL;
		}
								
/*
 *		Find the earliest collision and handle it. This also tests whether or 
 *		not we have more collisions to handle (potentially).
 */
		earliestcTime = NO_COLLISIONS_TIME;
		morecollisions = 0;
		for (i=0; i<colnum; i++) {
			if (colarray[i].cTime<earliestcTime && colarray[i].coldone==0) {
				earliestcTime = colarray[i].cTime;
				col = i;
				morecollisions = 1;
			}
		}					
								
/*
 *		Protect against overstepping the size of our arrays.
 */
   		if (colnum>=MAX_COLS || ndone>=MAX_COLS) {
   			earliestcTime = NO_COLLISIONS_TIME;
   			morecollisions = 0;
   			printf("Warning: Precipitate collision detection was aborted due to max number"
   					" of collisions (MAX_COLS) being reached.\n");
   		}
		
/*		Unpack the earliest collision data if there was a collision */
		if (earliestcTime<NO_COLLISIONS_TIME) {	
			
			node1 = colarray[col].node1;
			node2 = colarray[col].node2;
			cPoint[X] = colarray[col].cPoint[X];
			cPoint[Y] = colarray[col].cPoint[Y];
			cPoint[Z] = colarray[col].cPoint[Z];
			L = colarray[col].L;
			p1[X] = colarray[col].p1[X];
			p1[Y] = colarray[col].p1[Y];
			p1[Z] = colarray[col].p1[Z];
			p2[X] = colarray[col].p2[X];
			p2[Y] = colarray[col].p2[Y];
			p2[Z] = colarray[col].p2[Z];
			x1 = colarray[col].x1;
			y1 = colarray[col].y1;
			z1 = colarray[col].z1;
			x1old = colarray[col].x1old;
			y1old = colarray[col].y1old;
			z1old = colarray[col].z1old;
			x2 = colarray[col].x2;
			y2 = colarray[col].y2;
			z2 = colarray[col].z2;
			x2old = colarray[col].x2old;
			y2old = colarray[col].y2old;
			z2old = colarray[col].z2old;
			xp = colarray[col].xp;
			yp = colarray[col].yp;
			zp = colarray[col].zp;
			hinge = colarray[col].hinge;
			prec = colarray[col].prec;
			
            vx1 = node1->vX; vy1 = node1->vY; vz1 = node1->vZ;
            vx2 = node2->vX; vy2 = node2->vY; vz2 = node2->vZ;
			
			arm12 = GetArmID(home,node1,node2);
			
			colarray[col].coldone = 1;

			done_points[ndone][X] = cPoint[X];
			done_points[ndone][Y] = cPoint[Y];
			done_points[ndone][Z] = cPoint[Z];
			ndone++;
			
#ifdef DEBUG_LOG_PREC_COLLISIONS
			printf("earlestcTime=%e with hinge=%d in domain %i\n",earliestcTime,hinge,thisDomain);				
			printf("Collided at x=%e y=%e z=%e\n",cPoint[X],cPoint[Y],cPoint[Z]);
			printf("Node 1 is %i,%i at x=%e y=%e z=%e\n",node1->myTag.domainID,node1->myTag.index,x1,y1,z1);
			printf("Node 2 is %i,%i at x=%e y=%e z=%e\n",node2->myTag.domainID,node2->myTag.index,x2,y2,z2);
			printf("Precipitate is %i at x=%e y=%e z=%e\n",prec->myTag.index,xp,yp,zp);
#endif
			
/*
 *			Unflag the associated nodes and all of their neighbors
 *			since handling this collision may change things.
 */
			node1->flags &= ~NO_COLLISIONS_LOCAL;
			for (i=0; i<node1->numNbrs; i++) {
				tmpNbr = GetNeighborNode(home,node1,i);
				tmpNbr->flags &= ~NO_COLLISIONS_LOCAL;
			}
			
			node2->flags &= ~NO_COLLISIONS_LOCAL;
			for (i=0; i<node2->numNbrs; i++) {
				tmpNbr = GetNeighborNode(home,node2,i);
				tmpNbr->flags &= ~NO_COLLISIONS_LOCAL;
			}
			
/*
 *			"Delete" all collisions associated with these nodes
 *			or any of their neighbors. We will redo the search on these
 *			nodes.
 */			
			for (i=0; i<colnum; i++) {
				if (colarray[i].coldone==0) {
					if (((colarray[i].node1->flags & NO_COLLISIONS_LOCAL)==0) ||
						((colarray[i].node2->flags & NO_COLLISIONS_LOCAL)==0)) {
						colarray[i].node1->flags &= ~NO_COLLISIONS_LOCAL;
						colarray[i].node2->flags &= ~NO_COLLISIONS_LOCAL;
						colarray[i].coldone = 1;
					}
				}
			}
			
/*
 *			If the collision point is within the looping distance
 *			of either node at the time of collision, try to use that
 *			node for the collision (rather than adding a new node).
 */			
			xtmp = node1->x;	ytmp = node1->y;	ztmp = node1->z;
			PBCPOSITION(param,node1->olderx,node1->oldery,node1->olderz,&xtmp,&ytmp,&ztmp);

			xc1 = node1->olderx + earliestcTime*(xtmp - node1->olderx);
			yc1 = node1->oldery + earliestcTime*(ytmp - node1->oldery);
			zc1 = node1->olderz + earliestcTime*(ztmp - node1->olderz);

			xtmp = node2->x;	ytmp = node2->y;	ztmp = node2->z;
			PBCPOSITION(param,node2->olderx,node2->oldery,node2->olderz,&xtmp,&ytmp,&ztmp);

			xc2 = node2->olderx + earliestcTime*(xtmp - node2->olderx);
			yc2 = node2->oldery + earliestcTime*(ytmp - node2->oldery);
			zc2 = node2->olderz + earliestcTime*(ztmp - node2->olderz);

            vec1[X] = cPoint[X] - xc1;
            vec1[Y] = cPoint[Y] - yc1;
            vec1[Z] = cPoint[Z] - zc1;

			ZImage(param,&vec1[X],&vec1[Y],&vec1[Z]);

            vec2[X] = cPoint[X] - xc2;
            vec2[Y] = cPoint[Y] - yc2;
            vec2[Z] = cPoint[Z] - zc2;

			ZImage(param,&vec2[X],&vec2[Y],&vec2[Z]);
			
			real8 dist1sq, dist2sq;
			dist1sq = DotProduct(vec1,vec1);
			dist2sq = DotProduct(vec2,vec2);
			
            close2node1 = (dist1sq<prec->lloop*prec->lloop);
            close2node2 = (dist2sq<prec->lloop*prec->lloop);
			

/*
 *			For non-hinge collision, priority to the closer node.
 */
			if (close2node1 && close2node2 && hinge==0) {
				if (dist1sq<dist2sq) 
					close2node2 = 0; 
				else 
					close2node1 = 0;
			}
			
/*
 *          NOTE: The current domain owns node1 but may
 *          not own node2.  If it does not own node2, we 
 *          cannot allow the collision to use node2
 *          even if the collision point is close to 
 *          that node.
 *
 *          In the segment collision code, we would just skip this collision.
 *          Here, we want to try a little harder to prevent missing collisions,
 *          since missing a collision means missing the formation of an Orowan
 *          loop.
 *
 *          If the collision point is close to node2, we will split node1
 *          unless the point is "really" close to node2.
 */

            if ((node2->myTag.domainID != thisDomain) &&
                close2node2) {
                close2node2 = 0;
                if (DotProduct(vec2,vec2)<1e-4*prec->lloop*prec->lloop) {
                    node2->flags |= NO_COLLISIONS;
printf("Missed a collision!\n");
                    continue;
                }
            }			

            if (close2node1 && hinge!=1) {
                targetNode = node1;
                splitSeg1 = 0;
/*
				ProjectCollisionPoint(targetNode, cPoint,
									  xp, yp, zp,
									  x1, y1, z1,
								      x2, y2, z2,
								      x1old, y1old, z1old,
								      x2old, y2old, z2old,
								      0.0, 0.0, 0.0,
								      param, prec);
*/
            } else if (close2node2 && hinge!=2) {
                targetNode = node2;
                splitSeg1 = 0;
/*
				ProjectCollisionPoint(targetNode, cPoint,
									  xp, yp, zp,
									  x1, y1, z1,
								      x2, y2, z2,
								      x1old, y1old, z1old,
								      x2old, y2old, z2old,
								      0.0, 0.0, 0.0,
								      param, prec);
*/
            } else {
                splitSeg1 = 1;
/*
				ProjectCollisionPoint(NULL, cPoint,
									  xp, yp, zp,
									  x1, y1, z1,
								      x2, y2, z2,
								      x1old, y1old, z1old,
								      x2old, y2old, z2old,
								      node1->nx[arm12], 
									  	node1->ny[arm12], 
											node1->nz[arm12],
								      param, prec);
*/
            }


/*
 *			Now we project the collision point onto the
 *			surface of the precipitate. If it is a hinge
 *          we already did this when we determined the 
 *          collision point.
 *
 *          If the projection fails, this may not actually
 *          be a collision. Flag the nodes for no collisions and abort!
 */
            if (hinge == 0) {
                if (splitSeg1 == 0) {
                    projectSuccess = ProjectCollisionPoint(targetNode, cPoint,
									                       xp, yp, zp,
									                       x1, y1, z1,
								                           x2, y2, z2,
								                           x1old, y1old, z1old,
								                           x2old, y2old, z2old,
								                           0.0, 0.0, 0.0,
								                           param, prec);
                } else { 
				    projectSuccess = ProjectCollisionPoint(NULL, cPoint,
									                       xp, yp, zp,
									                       x1, y1, z1,
								                           x2, y2, z2,
								                           x1old, y1old, z1old,
								                           x2old, y2old, z2old,
								                           node1->nx[arm12], 
									                       	 node1->ny[arm12], 
											                    node1->nz[arm12],
								                           param, prec);
                }
                if (projectSuccess == 0) {
printf("ProjectCollisionPoint failed2\n");
                    node1->flags |= NO_COLLISIONS;
                    node2->flags |= NO_COLLISIONS;
					continue;
                }
            }
            

/*
 *			If the projected collision point has fallen
 *			behind or on top of the hinge node, don't proceed with the
 *			collision.
 */
			if (hinge == 1) {
				PBCPOSITION(param,cPoint[X],cPoint[Y],cPoint[Z],&xc1,&yc1,&zc1);
				
	            vec1[X] = cPoint[X] - xc1;
	            vec1[Y] = cPoint[Y] - yc1;
	            vec1[Z] = cPoint[Z] - zc1;

				PBCPOSITION(param,xc1,yc1,zc1,&xc2,&yc2,&zc2);
				
	            vec2[X] = xc2 - xc1;
	            vec2[Y] = yc2 - yc1;
	            vec2[Z] = zc2 - zc1;
			
				if ((DotProduct(vec1,vec2)<0.0) || 
					(DotProduct(vec1,vec1)<1e-6)) {
					node1->flags |= NO_COLLISIONS;
					continue;
				}
			} else if (hinge == 2) {
				PBCPOSITION(param,cPoint[X],cPoint[Y],cPoint[Z],&xc2,&yc2,&zc2);
				
	            vec1[X] = cPoint[X] - xc2;
	            vec1[Y] = cPoint[Y] - yc2;
	            vec1[Z] = cPoint[Z] - zc2;

				PBCPOSITION(param,xc2,yc2,zc2,&xc1,&yc1,&zc1);
				
	            vec2[X] = xc1 - xc2;
	            vec2[Y] = yc1 - yc2;
	            vec2[Z] = zc1 - zc2;			
			
				if ((DotProduct(vec1,vec2)<0.0) || 
					(DotProduct(vec1,vec1)<1e-6)) {
					node2->flags |= NO_COLLISIONS;
					continue;
				}				
			}			

/*
 *          If we need to add a new node to the first
 *          segment, do it now.
 */
            if (splitSeg1) {
                 real8 pos0[3], pos1[3];

                 newx = x1 * (1.0-L) + x2*L;
                 newy = y1 * (1.0-L) + y2*L;
                 newz = z1 * (1.0-L) + z2*L;

                 newvx = vx1 * (1.0-L) + vx2*L;
                 newvy = vy1 * (1.0-L) + vy2*L;
                 newvz = vz1 * (1.0-L) + vz2*L;
    
/*
 *               Estimate resulting forces on all segments
 *               involved in the split.
 */
                 arm21 = GetArmID(home, node2, node1);

                 oldfp0[X] = node1->armfx[arm12];
                 oldfp0[Y] = node1->armfy[arm12];
                 oldfp0[Z] = node1->armfz[arm12];

                 oldfp1[X] = node2->armfx[arm21];
                 oldfp1[Y] = node2->armfy[arm21];
                 oldfp1[Z] = node2->armfz[arm21];

                 pos0[X] = p1[X];   pos1[X] = p2[X];
                 pos0[Y] = p1[Y];   pos1[Y] = p2[Y];
                 pos0[Z] = p1[Z];   pos1[Z] = p2[Z];

                 pnew[X] = newx;
                 pnew[Y] = newy;
                 pnew[Z] = newz;

                 newNodeVel[X] = newvx;
                 newNodeVel[Y] = newvy;
                 newNodeVel[Z] = newvz;

                 nodeVel[X] = vx1;
                 nodeVel[Y] = vy1;
                 nodeVel[Z] = vz1;

                 burg1[X] = node1->burgX[arm12];
                 burg1[Y] = node1->burgY[arm12];
                 burg1[Z] = node1->burgZ[arm12];

                 FindSubFSeg(home, pos0, pos1, burg1, oldfp0,
                             oldfp1, pnew, f0seg1, f1seg1,
                             f0seg2, f1seg2);

                 oldTag1 = node1->myTag;
                 oldTag2 = node2->myTag;

                 FoldBox(param, &pos0[X], &pos0[Y], &pos0[Z]);
                 FoldBox(param, &pnew[X], &pnew[Y], &pnew[Z]);
				 
				 xtmp = node1->olderx;
				 ytmp = node1->oldery;
				 ztmp = node1->olderz;

                 splitStatus = SplitNode(home,
                                         OPCLASS_COLLISION,
                                         node1, pos0, pnew,
                                         nodeVel,
                                         newNodeVel, 1,
                                         &arm12, globalOp,
                                         &splitNode1,
                                         &splitNode2, 0);
										 
/*
 *               If we were unable to split the node go back to looking for more 
 *				 collision candidates.
 */
                 if (splitStatus == SPLIT_FAILED) {
                     node1->flags |= NO_COLLISIONS;
                     continue;
                 }
				 
				 splitNode1->olderx = xtmp;
				 splitNode1->oldery = ytmp;
				 splitNode1->olderz = ztmp;
				 
				 splitNode2->olderx = splitNode2->x;
				 splitNode2->oldery = splitNode2->y;
				 splitNode2->olderz = splitNode2->z;
				 
				 splitNode1->flags &= ~NO_COLLISIONS_LOCAL;
				 splitNode2->flags &= ~NO_COLLISIONS_LOCAL;

/*
 *				 If this is a hinge collision off of node1
 *				 reset its precipitate and pinned status just
 *				 in case SplitNode messed it up.
 */
				 if (hinge==1) {
                     ChangePrecipitate(home, splitNode1, prec->myTag.index, 0);	   
					 //splitNode1->prec = prec->myTag.index;
					 //splitNode1->constraint = PINNED_NODE;	   
				 }
/*
 *               The force estimates above are good enough
 *               for the remainder of this timestep, but
 *               mark the force and velocity data for
 *               some nodes as obsolete so that more
 *               accurate forces will be recalculated
 *               either at the end of this timestep, or
 *               the beginning of the next.
 */
                 targetNode = splitNode2;

                 MarkNodeForceObsolete(home, splitNode2);

                 for (q = 0; q < splitNode2->numNbrs; q++) {
                     tmpNbr = GetNodeFromTag(home, splitNode2->nbrTag[q]);
                     if (tmpNbr != (Node_t *)NULL) {
                         tmpNbr->flags |= NODE_RESET_FORCES;
                     }
                 }
    
/*
 *               Reset nodal forces on nodes involved in the
 *               split.
 */
                 ResetSegForces(home, splitNode1,
                                &splitNode2->myTag,
                                f0seg1[X], f0seg1[Y],
                                f0seg1[Z], 1);

                 ResetSegForces(home, splitNode2,
                                &splitNode1->myTag,
                                f1seg1[X], f1seg1[Y],
                                f1seg1[Z], 1);

                 ResetSegForces(home, splitNode2,
                                &node2->myTag,
                                f0seg2[X], f0seg2[Y],
                                f0seg2[Z], 1);

                 ResetSegForces(home, node2,
                                &splitNode2->myTag,
                                f1seg2[X], f1seg2[Y],
                                f1seg2[Z], 1);

                 (void)EvaluateMobility(home, splitNode1);
                 (void)EvaluateMobility(home, splitNode2);
                 (void)EvaluateMobility(home, node2);

/*
 *               When debugging, dump some info on
 *               topological changes taking place and
 *               the nodes involved
 */
#ifdef DEBUG_TOPOLOGY_CHANGES
	                if ((dbgDom < 0)||(dbgDom == home->myDomain)) {
	                    printf("  Split-1(SegCollision): "
	                           "(%d,%d)--(%d,%d) ==> "
	                           "(%d,%d)--(%d,%d)--(%d,%d)\n",
	                           oldTag1.domainID, oldTag1.index,
	                           oldTag2.domainID, oldTag2.index,
	                           splitNode1->myTag.domainID,
	                           splitNode1->myTag.index,
	                           splitNode2->myTag.domainID,
	                           splitNode2->myTag.index,
	                           node2->myTag.domainID,
	                           node2->myTag.index);
	                    PrintNode(splitNode1);
	                    PrintNode(splitNode2);
	                    PrintNode(node2);
	                 }
#endif
									 
                }  /* if (splitSeg1) */
				
/*
 *				If the node to be collided is already pinned at a precipitate,
 *				abort this collision and keep looking, flagging the node so
 *				this collision isn't found again.
 */				
				if (targetNode->prec>=0) {
					targetNode->flags |= NO_COLLISIONS;
					continue;
				}
										  
               FoldBox(param, &cPoint[X],&cPoint[Y],&cPoint[Z]);

/*							    
 *			   Collision "occurs" by flagging the node as pinned
 *			   at the precipitate. The mobility law then sets the
 *			   velocity for that node to zero.
 */						
               precStatus = ChangePrecipitate(home, targetNode, prec->myTag.index, 1);
               if (precStatus==PREC_FAILED) {
                  targetNode->flags |= NO_COLLISIONS;
                  continue;
               }

			   targetNode->x = cPoint[X];
			   targetNode->y = cPoint[Y];
			   targetNode->z = cPoint[Z];
#ifdef _RETROCOLLISIONS
			   targetNode->olderx = cPoint[X];
			   targetNode->oldery = cPoint[Y];
			   targetNode->olderz = cPoint[Z];
#endif

/*
 *             Add these to the global Op list so they get done
 *             on other domains...
 */
                AddOp(home, RESET_COORD,
                    targetNode->myTag.domainID,
                    targetNode->myTag.index,
                    -1, -1,                 /* 2nd tag unneeded */
                    -1, -1,                 /* 3rd node tag unneeded */
                    0.0, 0.0, 0.0,          /* burgers vector unneeded */
                    cPoint[X], cPoint[Y], cPoint[Z],
                    0.0, 0.0, 0.0);         /* plane normal not needed */						   
							    
/*
 *             If we are enforcing glide planes but
 *             allowing some fuzziness in the planes, we
 *             also need to recalculate the glide 
 *             planes for the segments attched to the
 *             collision node.
 */
               if (param->enforceGlidePlanes &&
                   param->allowFuzzyGlidePlanes) {
                   int n;
                   for (n=0; n<targetNode->numNbrs; n++) {
                       tmpNbr = GetNodeFromTag(home,
                               targetNode->nbrTag[n]);
                       RecalcSegGlidePlane(home,
                                           targetNode,
                                           tmpNbr, 1);
                   }
               }
			   
               MarkNodeForceObsolete(home, targetNode);
			   
/*
 *             Mark all neighbor node forces for reset so we don't
 *             attempt to release any of them from precipitates with
 *             incorrect forces...
 */
               for (q = 0; q < targetNode->numNbrs; q++) {
                    tmpNbr = GetNodeFromTag(home, targetNode->nbrTag[q]);
                    if (tmpNbr != (Node_t *)NULL) {
                        tmpNbr->flags |= NODE_RESET_FORCES;
                    }
                }

               (void)EvaluateMobility(home, targetNode);
			   
			   
#ifdef DEBUG_LOG_MULTI_NODE_SPLITS
               targetNode->multiNodeLife = 0;
#endif

               localCollisionCnt++;

			   //if (home->cycle>=7) 
			   //   GenerateOutput(home, STAGE_CYCLE);
				   
           } //if(earliestcTime<NO_COLLISIONS_TIME)
							   
		}	//while(morecollisions)
			
/*
*		Unflag all nodes - everything should be hunky dory.
*/
       	for (i = 0; i < home->newNodeKeyPtr; i++) {
           	if ((tmpNbr = home->nodeKeys[i]) == (Node_t *)NULL) continue;
   			tmpNbr->flags &= ~NO_COLLISIONS;
   		}	

/*
 *      Free memory.
 */

        for(i=0;i<MAX_COLS;i++){			
			free(done_points[i]);
        }
        free(done_points);
	
		free(colarray);
printf("prec_collisioncnt = %i %i\n",localCollisionCnt,colnum);			
		return;
}

#endif
