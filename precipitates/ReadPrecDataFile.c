/*****************************************************************************
 *
 *	Module:		ReadPrecDataFile.c
 *	Description:	This module contains functions for loading the precipitate
 *					data at the beginning of a simulation.
 *
 *					This is a modified version of ReadDataFile.c
 *
 *
 *	Included functions:
 *              cholesky
 *				FreePrecstress
 *				LoadPrecStress
 *				ReadPrecDataFile
 *
 *****************************************************************************/

#ifdef _PREC

#include "Home.h"
#include "Tag.h"
#include "Util.h"
#include "Parse.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Take the cholesky decomposition in the manner described in FA Graybill
   (1976).
*/
/*
 *	This function was taken from the following URL:
 * 	http://www.stat.uchicago.edu/~mcpeek/software/MQLS/cholesky.c
 */
//int cholesky(real8 **orig, int n, real8 **aug, int mcol,real8 **chol, real8 **cholaug, int ofs)
int cholesky(real8 orig[3][3], int n, real8 aug[3][3], int mcol,real8 chol[3][3], real8 cholaug[3][3], int ofs)
	
     /* 
	Do the augmented cholesky decomposition as described in FA Graybill
	(1976) Theory and Application of the Linear Model. The original matrix
	must be symmetric positive definite. The augmentation matrix, or
	series of column vectors, are multiplied by C^-t, where C is the
	upper triangular cholesky matrix, ie C^t * C = M and M is the original
	matrix. Returns with a value of 0 if M is a non-positive definite 
	matrix. Returns with a value of 1 with succesful completion.

	Arguments:

	orig (input) double n x n array. The matrix to take the Cholesky
	      decomposition of.
	n    (input) integer. Number of rows and columns in orig.
	aug  (input) double n x mcol array. The matrix for the augmented
	      part of the decomposition.
	mcol (input) integer. Number of columns in aug.
	chol (output) double n x n array. Holds the upper triangular matrix
	      C on output. The lower triangular portion remains unchanged.
	      This maybe the same as orig, in which case the upper triangular
	      portion of orig is overwritten.
	cholaug (output) double n x mcol array. Holds the product C^-t * aug.
	         May be the same as aug, in which case aug is over written.
	ofs (input) integer. The index of the first element in the matrices.
	     Normally this is 0, but commonly is 1 (but may be any integer).
     */
{
	   int i, j, k, l;
	   int retval = 1;

	   for (i=ofs; i<n+ofs; i++) {
	      chol[i][i] = orig[i][i];
	      for (k=ofs; k<i; k++)
		 chol[i][i] -= chol[k][i]*chol[k][i];
	      if (chol[i][i] <= 0) {
		 fprintf(stderr,"\nERROR: non-positive definite matrix!\n");
		 printf("\nproblem from %d %f\n",i,chol[i][i]);
		 retval = 0;
		 return retval;
	      }
	      chol[i][i] = sqrt(chol[i][i]);

	      /*This portion multiplies the extra matrix by C^-t */
	      for (l=ofs; l<mcol+ofs; l++) {
		 cholaug[i][l] = aug[i][l];
		 for (k=ofs; k<i; k++) {
		    cholaug[i][l] -= cholaug[k][l]*chol[k][i];
		 }
		 cholaug[i][l] /= chol[i][i];
	      }

	      for (j=i+1; j<n+ofs; j++) {
		 chol[i][j] = orig[i][j];
		 for (k=ofs; k<i; k++)
		    chol[i][j] -= chol[k][i]*chol[k][j];
		 chol[i][j] /= chol[i][i];
	      }
	   }

	   return retval;
}


/*------------------------------------------------------------------------
 *
 *	Function:	FreePrecstress
 *	Description:	Free the memory of a precstress structure.
 *
 *      Arguments:
 *         	precstress	  Pointer to the prectress structure to be freed
 *
 *-----------------------------------------------------------------------*/
void FreePrecstress(Precstress_t *precstress) { 

		int		Ngrid, i, j, k;
		
		Ngrid = precstress->Ngrid;
		for (i=0; i<Ngrid; i++) {
			for (j=0; j<Ngrid; j++) {
				free(precstress->sxx[i][j]);
				free(precstress->syy[i][j]);
				free(precstress->szz[i][j]);
				free(precstress->syz[i][j]);
				free(precstress->sxz[i][j]);
				free(precstress->sxy[i][j]);
			}
			free(precstress->sxx[i]);
			free(precstress->syy[i]);
			free(precstress->szz[i]);
			free(precstress->syz[i]);
			free(precstress->sxz[i]);
			free(precstress->sxy[i]);
		}
		free(precstress->sxx);
		free(precstress->syy);
		free(precstress->szz);
		free(precstress->syz);
		free(precstress->sxz);
		free(precstress->sxy);
		
		free(precstress);

		return;
}

/*------------------------------------------------------------------------
 *
 *	Function:	LoadPrecStress
 *	Description:	Read the precipitate data file and load all necessary
 *					information.
 *
 *					For memory management purposes, the first precipitate
 *					that uses a precstress structure is labeled the 
 *					"owner." This is used at the end of the simulation 
 *					when memory is freed.
 *
 *      Arguments:
 *         	param	  Pointer to the param structure
 *          prec	  Pointer to the precipitate structure whose stress
 *					  data we are loading
 *			stress_array  Array of precstress pointers that have been
 *					  specified in the .prec file and already loaded
 *			max_file_num  Number of stress files that were loaded 
 *
 *-----------------------------------------------------------------------*/

void LoadPrecStress(Param_t *param, Prec_t *prec, 
					Precstress_t **stress_array, int max_file_num) {
	
		int		i, dilat, file_num;
		int		pois_check, rc_check, radius_check, estrain_check;
		real8	estar_vol, estar_dev[6], estar_norm;
		real8	eps = 1e-7;
		
	
/*
 *		If the eigenstrain tensor is all zeros, no need to load in
 *		a stress field.
 */
		if ((fabs(prec->estar[0])<eps && fabs(prec->estar[1])<eps &&
			 fabs(prec->estar[2])<eps && fabs(prec->estar[3])<eps &&
			 fabs(prec->estar[4])<eps && fabs(prec->estar[5])<eps) ||
			 (param->precMisfit == 0)) {	
			prec->misfit = 0;
			prec->precstress_owner = 0;
			return;	
		} else {
			prec->misfit = 1;
		}
		
/*
 *		If this is a dilatational sphere, and we aren't using a
 *		numerical stress field, no need to load a stress field.
 *		Simply compute the misfit volume and exit.
 */
#ifndef _NUMERICALSPHERE
		estar_vol = (prec->estar[0]+prec->estar[1]+prec->estar[2])/3.0;
		for (i=0; i<6; i++) estar_dev[i] = prec->estar[i];
		estar_dev[0] -= estar_vol;
		estar_dev[1] -= estar_vol;
		estar_dev[2] -= estar_vol;
		dilat = (fabs(estar_dev[0])<eps & fabs(estar_dev[1])<eps &
					fabs(estar_dev[2])<eps & fabs(estar_dev[3])<eps &
					fabs(estar_dev[4])<eps & fabs(estar_dev[5])<eps);
		if (prec->sphere && dilat) {
			prec->dV = 4.0*estar_vol*M_PI*prec->r1*prec->r1*prec->r1;
			prec->analytic_stress = 1;
			prec->precstress_owner = 0;
			return;
		}
#endif
		prec->analytic_stress = 0;
		
/*
 *		The eigenstrain elements are normalized by the first nonzero
 *		term in the Voigt vector - determine that component now.
 */		
   		i = 0;
   		while (fabs(prec->estar[i])<eps) i++;
   		estar_norm = prec->estar[i];
		
/*
 *		We need to use a numerical stress field. Loop over all of the
 *		files specified in the .prec file and find the one that matches
 *		the properties for this precipitate.
 */
		
		file_num = -1;
		for (i=0; i<=max_file_num; i++) {
			pois_check = (fabs(stress_array[i]->pois - param->pois) < eps);
			//rc_check = (fabs(stress_array[i]->rc - param->rc) < eps);
			//ignore core radius for now.... does it matter much????
			rc_check = 1;
			radius_check = (fabs(stress_array[i]->r2 - prec->r2/prec->r1) < eps &&
							fabs(stress_array[i]->r3 - prec->r3/prec->r1) < eps );
			estrain_check = (fabs(stress_array[i]->estar[0] - prec->estar[0]/estar_norm) < eps &&
							 fabs(stress_array[i]->estar[1] - prec->estar[1]/estar_norm) < eps &&
							 fabs(stress_array[i]->estar[2] - prec->estar[2]/estar_norm) < eps &&
							 fabs(stress_array[i]->estar[3] - prec->estar[3]/estar_norm) < eps &&
							 fabs(stress_array[i]->estar[4] - prec->estar[4]/estar_norm) < eps &&
			 				 fabs(stress_array[i]->estar[5] - prec->estar[5]/estar_norm) < eps);
			if (pois_check && rc_check && radius_check && estrain_check){
				file_num = i;
				break;
			}
		}
		
		if (file_num<0) {
            Fatal("Precipitate %d,%d does not match any of the "
                  "given precipitate stress files.\n"
				  "radius_check = %i\n"
				  "pois_check = %i\n"
				  "estrain_check = %i\n",
       	 		  prec->myTag.index,prec->myTag.domainID,
				  radius_check,pois_check,estrain_check);
		} 
		
/*
 *		We found the appropriate stress_file. Now we just need
 *		to set the precipitate pointers to this file's data
 *		and tabulate the scaling factor for the stress field
 *		(since the stress file was computed with unit
 *		shear modulus and normalized eigenstrain).
 */
		prec->precstress = stress_array[file_num];
		prec->sscale = param->shearModulus*estar_norm;
		//prec->rcut = stress_array[file_num]->rmax*prec->r1;
		prec->rcut = 10.0*prec->r1;
		if (stress_array[file_num]->used == 0) {
			prec->precstress_owner = 1;
			stress_array[file_num]->used = 1;
		} else {
			prec->precstress_owner = 0;
		}
	
		return;
}

/*------------------------------------------------------------------------
 *
 *	Function:	ReadPrecDataFile
 *	Description:	Read the precipitate data file and load all necessary
 *					information.
 *
 *
 *      Arguments:
 *         	home	  Pointer to the home structure
 *          precFile  Name of the nodal data file.  For segmented
 *                    restart files, this may be the base name
 *                    (i.e. no trailing sequence number) or the
 *                    name of the first file segment.
 *
 *-----------------------------------------------------------------------*/

void ReadPrecDataFile(Home_t *home, char *precFile)
{
        int      i, j, k, dom, iNbr;
        int      maxTokenLen, tokenType, pIndex, okay;
        int      valType, numVals;
        int      numDomains, numReadTasks;
        int      nextFileSeg, maxFileSeg, segsPerReader, taskIsReader;
        int      distIncomplete, readCount, numNbrs;
        int      fileSegCount = 1, fileSeqNum = 0;
        int      *globalMsgCnt, *localMsgCnt, **nodeLists, *listCounts;
        int      miscData[2];
        int      localPrecCount, globalPrecCount;
        int      binRead = 0;
        int      nextAvailableTag = 0;
		int		 stressfiles, file_num, max_file_num, Ngrid;
        real8    burgSumX, burgSumY, burgSumZ;
		real8	 diff, eps = 1e-10;
		real8	 tmpMat[3][3], tmpMat2[3][3], Umat[3][3], Smat[3][3];
		real8	 rmin, rmax;
        void     *valList;
        char     inLine[500], token[256];
        char     baseFileName[256], tmpFileName[256];
        FILE     *fpSeg, *fprec;
        Prec_t   *prec;
		Precstress_t *precstress, **stress_array;
        Param_t  *param;
        ParamList_t *precParamList;

        param      = home->param;
        numDomains = home->numDomains;
        fpSeg      = (FILE *)NULL;

        precParamList = home->precParamList;

        memset(inLine, 0, sizeof(inLine));
        maxTokenLen = sizeof(token);
		
		home->prec_rmax = 0.0;
		home->prec_cell2_width = 0.0;
		
/* 
 *		Only domain zero knows whether a precipitates file was given,
 *		so check if there was, and if not let the other processors
 *		know.
 */
		
		int noReadPrec = 0;
		if (home->myDomain==0 && precFile == (char *)NULL) {
			noReadPrec = 1;
		}

#ifdef	PARALLEL	
		MPI_Bcast(&noReadPrec, 1, MPI_INT, 0, MPI_COMM_WORLD);		  
#endif
		if (noReadPrec) {
			home->newPrecKeyPtr = 0;
			return;
		}

/*
 *      Only domain zero reads the initial stuff...
 */
        if (home->myDomain == 0) {

            if (precFile == (char *)NULL) {
                Fatal("ReadPrecDataFile: No precipitate data file provided");
            }

            snprintf(tmpFileName, sizeof(tmpFileName), "%s", precFile);
				  
            if ((fpSeg = fopen(tmpFileName, "r")) == (FILE *)NULL) {
                Fatal("Error %d opening file %s to read precipitate data",
                      errno, precFile);
            }


/*
 *          Get the first token.  This should either be a known
 *          parameter identifier, or a file version number.
 */
            tokenType = GetNextToken(fpSeg, token, maxTokenLen);
            pIndex = LookupParam(precParamList, token);

            if (pIndex < 0) {
/*
 *              If the token does not correspond to a known parameter, exit.
 */
                Fatal("ReadPrecDataFile: Unknown parameter.");

            } else {
/*
 *              Just go through the precipiate data file reading all
 *              the associated parameters.  Need to do special
 *              processing of the domain decomposition, and when
 *              when we hit the 'nodaldata' identifier, just break
 *              out of this loop.
 */
                while ((tokenType != TOKEN_ERR) && (tokenType != TOKEN_NULL)) {

                    if (pIndex >= 0) {
/*
 *                      Token represents a known parameter identifier, so
 *                      read the associated value(s).
 */
                        valType = precParamList->varList[pIndex].valType;
                        numVals = precParamList->varList[pIndex].valCnt;
                        valList = precParamList->varList[pIndex].valList;
                        okay = GetParamVals(fpSeg, valType, numVals, valList);
                        if (!okay) {
                            Fatal("Parsing Error obtaining values for "
                                  "parameter %s\n",
                                  precParamList->varList[pIndex].varName);
                        }

                    } else if (strcmp(token, "precData") == 0) {
/*
 *                          When we hit the precipitate data, we can break
 *                          out of the loop because we are assuming
 *                          all other data file parameters have been
 *                          processed.  If they have not, we have a
 *                          problem since processing of the nodal data
 *                          requires the other parameters.
 *                          Note: Remainder of the file should just
 *                          contain " = " followed by the nodal data,
 *                          so be sure to skip the next token before
 *                          processing the nodal data.
 */
                        tokenType = GetNextToken(fpSeg, token, maxTokenLen);
						stressfiles = 0;
                        break;
						
                    } else if (strcmp(token, "precStressFiles") == 0) {
/*
 *                          When we hit the precipitate stress file names, 
 *							we can break
 *                          out of the loop because we are assuming
 *                          all other data file parameters have been
 *                          processed.  If they have not, we have a
 *                          problem since processing of the nodal data
 *                          requires the other parameters.
 *                          Note: Remainder of the file should just
 *                          contain " = " followed by the nodal data,
 *                          so be sure to skip the next token before
 *                          processing the nodal data.
 */
                        tokenType = GetNextToken(fpSeg, token, maxTokenLen);
						stressfiles = 1;
                        break;
    
                    } else {
/*
 *                      If the parameter is not recognized, skip the
 *                      parameter and any associated value(s).
 */
                        printf("Ignoring unknown data file parameter %s\n",
                               token);
                        valType = V_NULL;
                        numVals = 0;
                        valList = (void *)NULL;
                        okay = GetParamVals(fpSeg, valType, numVals,
                                            valList);

                    }

                    tokenType = GetNextToken(fpSeg, token, maxTokenLen);

                    if ((tokenType == TOKEN_NULL)||(tokenType == TOKEN_ERR)) {
                        Fatal("Parsing error on file %s\n", tmpFileName);
                    }
                    pIndex = LookupParam(precParamList, token);
                }
            }

       	}  /* if (home->myDomain == 0) */

	   
#ifdef PARALLEL
/*
 *      Domain zero now needs to pass various pieces of data to
 *      the remote domains before any processes begin to read
 *      and distribute the nodal data... start with the param structure.
 */
      	MPI_Bcast((char *)param, sizeof(Param_t), MPI_CHAR, 0, MPI_COMM_WORLD);
			  	   
		MPI_Bcast((char *)tmpFileName, sizeof(tmpFileName),
		  										MPI_CHAR, 0, MPI_COMM_WORLD);
		if (home->myDomain != 0) {
			if ((fpSeg = fopen(tmpFileName, "r")) == (FILE *)NULL) {
			  Fatal("Error %d opening file %s to read precipitate data",
			        errno, precFile);
			}
			while (1) {
				tokenType = GetNextToken(fpSeg, token, maxTokenLen);
				if (strcmp(token, "precData") == 0) {
                    tokenType = GetNextToken(fpSeg, token, maxTokenLen);
					stressfiles = 0;
                    break;
	
                } else if (strcmp(token, "precStressFiles") == 0) {

                    tokenType = GetNextToken(fpSeg, token, maxTokenLen);
					stressfiles = 1;
                    break;
				}
			}		  
		}
		//MPI_Bcast(&stressfiles, 1, MPI_INT, 0, MPI_COMM_WORLD);		  
#endif

		
/*		
 *		If there are precipitate stress files given, first read them in.
 */
		max_file_num = -1;
		if (stressfiles) {
			
            tokenType = GetNextToken(fpSeg, token, maxTokenLen);
			
            while ((tokenType != TOKEN_ERR) && (tokenType != TOKEN_NULL)) {

/*
 *              If we hit the start of the precipitate data, 
 *				break out of the loop. 
 */
				if (strcmp(token, "precData") == 0) {
					tokenType = GetNextToken(fpSeg, token, maxTokenLen);
                    break;
				} 
/*
 *				Open the precipitate stress file.
 */				
	            if ((fprec = fopen(token, "r")) == (FILE *)NULL) {
	                Fatal("Error %d opening precipitate stress file %s",
	                      errno, token);
	            }
				printf("Reading file %s ... ",token);
				
				if (max_file_num==-1) {
/*
 *					Allocate memory for the first precstress pointer. 
 */
					stress_array = (Precstress_t **) malloc( sizeof(Precstress_t *) );
				} else {
/*
 *					Allocate memory for the next precstress pointer. 
 */					
					stress_array = (Precstress_t **)
						realloc(stress_array, (max_file_num+2) * sizeof(Precstress_t *));					
				}
				
/* 
 *				Allocate memory for the new precstress structure.
 */
				precstress = (Precstress_t *) malloc( sizeof(Precstress_t) );
				
				
/*
 *				Read in the number of grid points used, and the other 
 *				parameters for the precipitate stress field. We will
 *				use these later to load the correct stress field for
 *				each precipitate.
 */ 
                Getline(inLine, sizeof(inLine), fprec); 
                sscanf(inLine, "%d",&precstress->Ngrid);
				
                Getline(inLine, sizeof(inLine), fprec); 
                sscanf(inLine, "%lf",&precstress->rmax);
								
                Getline(inLine, sizeof(inLine), fprec); 
                sscanf(inLine, "%lf",&precstress->pois);
				
                Getline(inLine, sizeof(inLine), fprec); 
                sscanf(inLine, "%lf",&precstress->rc);
				
                Getline(inLine, sizeof(inLine), fprec); 
                sscanf(inLine, "%lf %lf %lf",&precstress->r1,
				                             &precstress->r2,
											 &precstress->r3);
				
                Getline(inLine, sizeof(inLine), fprec); 
                sscanf(inLine, "%lf %lf %lf %lf %lf %lf",
										&precstress->estar[0],
										&precstress->estar[1],
										&precstress->estar[2],
										&precstress->estar[3],
										&precstress->estar[4],
										&precstress->estar[5]);	

/*
 *				Allocate memory for the precipitate stress arrays. We store the stress data
 *				as 3D arrays for the (x,y,z) grid.
 */
				Ngrid = precstress->Ngrid;
				precstress->sxx = malloc(sizeof(real8 **) * Ngrid);
				precstress->syy = malloc(sizeof(real8 **) * Ngrid);
				precstress->szz = malloc(sizeof(real8 **) * Ngrid);
				precstress->syz = malloc(sizeof(real8 **) * Ngrid);
				precstress->sxz = malloc(sizeof(real8 **) * Ngrid);
				precstress->sxy = malloc(sizeof(real8 **) * Ngrid);
				for (i=0; i<Ngrid; i++) {
					precstress->sxx[i] = malloc(sizeof(real8 *) * Ngrid);
					precstress->syy[i] = malloc(sizeof(real8 *) * Ngrid);
					precstress->szz[i] = malloc(sizeof(real8 *) * Ngrid);
					precstress->syz[i] = malloc(sizeof(real8 *) * Ngrid);
					precstress->sxz[i] = malloc(sizeof(real8 *) * Ngrid);
					precstress->sxy[i] = malloc(sizeof(real8 *) * Ngrid);
					for (j=0; j<Ngrid; j++) {
						precstress->sxx[i][j] = malloc(sizeof(real8) * Ngrid);
						precstress->syy[i][j] = malloc(sizeof(real8) * Ngrid);
						precstress->szz[i][j] = malloc(sizeof(real8) * Ngrid);
						precstress->syz[i][j] = malloc(sizeof(real8) * Ngrid);
						precstress->sxz[i][j] = malloc(sizeof(real8) * Ngrid);
						precstress->sxy[i][j] = malloc(sizeof(real8) * Ngrid);
					}
				}
				
				//DON'T FORGET WE NEED TO FREE THESE AT THE END OF THE SIMULATION!
				
/*
 *				Read in and load the precipitate stress field.
 */
				for (i=0; i<Ngrid; i++) {
					for (j=0; j<Ngrid; j++) {
						for (k=0; k<Ngrid; k++) {
			                Getline(inLine, sizeof(inLine), fprec); 
		                    if (inLine[0] == 0) {
				                Fatal("Precipitate stress file %s has too few "
									  "lines of stress data for the given number "
									  "of grid points %d.",
				                      token,Ngrid);
		                    }
			                sscanf(inLine, "%lf %lf %lf %lf %lf %lf",
									&precstress->sxx[i][j][k],
									&precstress->syy[i][j][k],
									&precstress->szz[i][j][k],
									&precstress->syz[i][j][k],
									&precstress->sxz[i][j][k],
									&precstress->sxy[i][j][k]);
						}
					}
				}
                Getline(inLine, sizeof(inLine), fprec); 
                if (inLine[0] != 0) {
	                Fatal("Precipitate stress file %s has too many "
						  "lines of stress data for the given number "
						  "of grid points %d.",
	                      token,Ngrid);
                }				
				
				
				//For now, all alpha values are equal to 1 (r1)
				precstress->alpha = 1.0;
				
				//Toggle the structure as unused, will be toggled as used below
				//as appropriate. Unused structures are freed before exiting.
				precstress->used = 0;
								
				max_file_num++;						
				stress_array[max_file_num] = precstress;
				
				printf("done\n");
				
/*
 *				Read the next precipitate stress file name and keep going.
 */
				tokenType = GetNextToken(fpSeg, token, maxTokenLen);	
				
			}			
		} 

/*
 *      Allocate precKeys based on the number of precipitates.
 */
        home->precKeys =
		            (Prec_t **) malloc( param->precCount * sizeof(Prec_t *) );
        home->newPrecKeyPtr = 0;

        taskIsReader  = 1;
		
/*
 *		Initialize some stuff.
 */
		for (i=0; i<3; i++) {
			for (j=0; j<3; j++) {
				Smat[i][j] = 0.0;
			}
		}

/*
 *      All processes loop until all nodal data has been read in
 *      and distributed to the appropriate domains.
 */
        distIncomplete = 1;
        readCount = 0;

        while (distIncomplete) {

            while (taskIsReader) {

/*
 *              If we have a valid open file pointer now, continue
 *              reading nodal data.  If we don't, it's because the
 *              task has no more data to read so terminate its
 *              'reader' status.
 */
                if (fpSeg != (FILE *)NULL) {

                    Getline(inLine, sizeof(inLine), fpSeg);

/*
 *                  If we hit EOF, close the current segment and
 *                  loop back to open the next segment (if any)
 *                  this domain is responsible for reading.
 */
                    if (inLine[0] == 0) {
                        nextFileSeg++;
                        fclose(fpSeg);
                        fpSeg = (FILE *)NULL;
                        continue;  
                    }

/*
 *                  Got information on the next precipitate, so deal with it.
 */
                    

                    prec = (Prec_t *) malloc( sizeof(Prec_t) );

                    sscanf(inLine, "%d,%d %lf %lf %lf",
                           &prec->myTag.domainID, &prec->myTag.index,
                           &prec->x, &prec->y, &prec->z);
						   
                   Getline(inLine, sizeof(inLine), fpSeg); 
				   
                   sscanf(inLine, "%lf %lf %lf",
                          &prec->r1, &prec->r2, &prec->r3);
						   
/*
 *				    We require that the radii be specified largest to
 *					smallest.
 */
				    if (prec->r1<prec->r2 || prec->r2<prec->r3) {
						Fatal("Radii for precipitate %d,%d are not listed "
							  "largest to smallest.",
							  prec->myTag.domainID, prec->myTag.index);
				    }
						   
                    Getline(inLine, sizeof(inLine), fpSeg); 

                    sscanf(inLine, "%lf %lf %lf %lf %lf %lf",
                          &prec->estar[0],&prec->estar[1],&prec->estar[2],
					      &prec->estar[3],&prec->estar[4],&prec->estar[5]);

                    Getline(inLine, sizeof(inLine), fpSeg); 

                    sscanf(inLine, "%lf %lf %lf",
                           &prec->xdir[X],&prec->xdir[Y],&prec->xdir[Z]);
					
                    Getline(inLine, sizeof(inLine), fpSeg); 

                    sscanf(inLine, "%lf %lf %lf",
                           &prec->ydir[X],&prec->ydir[Y],&prec->ydir[Z]);                    						   
					
                    Getline(inLine, sizeof(inLine), fpSeg); 

                    sscanf(inLine, "%lf",&prec->Fcut); 

/*
printf("%d,%d %e %e %e\n   %e %e %e\n   %e %e %e %e %e %e\n   %e %e %e\n   %e %e %e\n   %e\n",
prec->myTag.domainID,prec->myTag.index,prec->x,prec->y,prec->z,prec->r1,prec->r2,prec->r3,
prec->estar[0],prec->estar[1],prec->estar[2],prec->estar[3],prec->estar[4],prec->estar[5],
prec->xdir[X],prec->xdir[Y],prec->xdir[Z],prec->ydir[X],prec->ydir[Y],prec->ydir[Z],prec->Fcut); 
*/
					
/*
 *					The "loop length", used to determine how collisions are detected
 *					with segments that already have a node pinned, is set to half of the
 *					minimum radius of curvature of the precipitate, given by
 *					rmin^2/rmax for an ellipsoid. However, we also need to ensure that
 *					collision will not be missed when remeshing events add new nodes that
 *					have already penetrated a precipitate. We can do so by making lloop
 *					less than the minimum segment length.
 */
					rmin = MIN(prec->r1,MIN(prec->r2,prec->r3));
					rmax = MAX(prec->r1,MAX(prec->r2,prec->r3));	   
				    prec->lloop = MAX(0.5*param->minSegPrec,MIN(0.5*rmin*rmin/rmax,0.9*param->minSeg));
					prec->rmax = rmax;
					prec->rmin = rmin;
					home->prec_rmax = MAX(home->prec_rmax,rmax);
					
/*
 *					If this is a spherical precipitate, we only need to load
 *					the rest of this crap if we are forcing the use of a
 *					numerical stress field (debugging).
 */
#ifdef _NUMERICALSPHERE
					prec->sphere = 0;
#else
					prec->sphere = (fabs(prec->r1-prec->r2)<eps && fabs(prec->r1-prec->r3)<eps);
#endif
					
#ifndef _NUMERICALSPHERE
				    if (prec->sphere==0)
#endif						   
					{	   
/*					
 *				    	Compute the z-direction and make sure xdir and ydir are
 *						orthogonal. Orthogonalize ydir wrt xdir since the input
 *						file has less precisiion than machine error - this ensures
 *						U is as close to an orthogonal matrix as possible.
 */  
					    NormalizeVec(prec->xdir);
					    NormalizeVec(prec->ydir);
						if (DotProduct(prec->xdir,prec->ydir)>1e-6) {
							Fatal("Axes of precipitate %d,%d are not orthogonal!",
										prec->myTag.domainID, prec->myTag.index);
						}
						Orthogonalize(&prec->ydir[X],&prec->ydir[Y],&prec->ydir[Z],
									   prec->xdir[X],prec->xdir[Y],prec->xdir[Z]);
			   			NormalizeVec(prec->ydir);  
						NormalizedCrossVector(prec->xdir,prec->ydir,prec->zdir);
			
/*
 *						Compute the A and LT matrices, and their inverses.
 */
						Umat[X][X] = prec->xdir[X]; 
						Umat[Y][X] = prec->xdir[Y]; 
						Umat[Z][X] = prec->xdir[Z];
					
						Umat[X][Y] = prec->ydir[X]; 
						Umat[Y][Y] = prec->ydir[Y]; 
						Umat[Z][Y] = prec->ydir[Z];
					
						Umat[X][Z] = prec->zdir[X]; 
						Umat[Y][Z] = prec->zdir[Y]; 
						Umat[Z][Z] = prec->zdir[Z];

		                for (i=0; i<3; i++) {
			                for (j=0; j<3; j++) {
				                Smat[i][j] = 0.0;
                                prec->LT[i][j] = 0.0;
			                }
		                }
					
						Smat[X][X] = 1.0/(prec->r1*prec->r1);
						Smat[Y][Y] = 1.0/(prec->r2*prec->r2);
						Smat[Z][Z] = 1.0/(prec->r3*prec->r3);
					
						Matrix33Transpose(Umat,prec->UT);	   	   
						Matrix33Mult33(Umat,Smat,tmpMat);
						Matrix33Mult33(tmpMat,prec->UT,prec->A);
					
					
						if (cholesky(prec->A, 3, tmpMat, 3, prec->LT, tmpMat2, 0)==0) {
							Fatal("Cannot find Cholesky decomposition for precipitate %d,%d!",
								  prec->myTag.domainID, prec->myTag.index);
						}
						   
/*
 *						Need to compute the inverses of A and LT since we use them a lot.
 *						We also use L^-1*L^-T a lot, so store that.
 */
						if (Matrix33Invert(prec->LT, prec->LTinv)==0) {
							Fatal("LT matrix for precipitate %d,%d is not "
								  "invertible!",prec->myTag.domainID, prec->myTag.index);
						}
						if (Matrix33Invert(prec->A, prec->Ainv)==0) {
							Fatal("A matrix for precipitate %d,%d is not "
								  "invertible!",prec->myTag.domainID, prec->myTag.index);
						}
						Matrix33Transpose(prec->LTinv,tmpMat);	   	   
						Matrix33Mult33(tmpMat,prec->LTinv,prec->Lmult);
												
					}

/*
 *					Load the stress field for the precipitate. We already loaded all of
 *					the stress files above, so we just need to perform some checks
 *					and then specify pointers.
 */
					LoadPrecStress(param, prec, stress_array, max_file_num);
		
                    FoldBox(param, &prec->x, &prec->y, &prec->z);

                    home->precKeys[prec->myTag.index] = prec;

                    if (prec->myTag.index >= home->newPrecKeyPtr) {
                        home->newPrecKeyPtr = prec->myTag.index + 1;
                    }
                    
                } else {
/*
 *                  No more files to be read by this task
 */
                    taskIsReader = 0;
                        distIncomplete = 0;
                }

/*
 *              We read the nodal data in blocks rather than all at once.
 *              If this domain has read in the maximum number of nodes
 *              allowed in a block, stop reading data and get ready
 *              to distribute it to the appropriate remote domains.
 */
                //if (readCount >= MAX_NODES_PER_BLOCK) {
                //    break;
                //}

            }  /* while (taskIsReader) */

        }  /* if (distIncomplete) */
		
/*
 *		Free the unused precstress structures (in case unnecessary files were
 *		listed in the .prec file).
 */
		for (i=0; i<=max_file_num; i++) {
			if (stress_array[i]->used==0) {
				FreePrecstress(stress_array[i]);
			}
		}
		
/*
 *		Free the array of precstress pointers, since we no longer need it.
 */
		if (max_file_num>=0) {
			free(stress_array);
		}
		
/*
 *      This is a good place for a quick sanity check that the sum of
 *      nodes on all domains equals the total node count from the
 *      data file.
 */
        localPrecCount = 0;
        globalPrecCount = 0;

        for (i = 0; i < home->newPrecKeyPtr; i++) {
            if (home->precKeys[i] != (Prec_t *)NULL) {
                localPrecCount++;
            }
        }

#ifdef PARALLEL
        // MPI_Reduce(&localPrecCount, &globalPrecCount, 1, MPI_INT, MPI_SUM,
        //            0, MPI_COMM_WORLD);
		globalPrecCount = localPrecCount;
#else
        globalPrecCount = localPrecCount;
#endif

        if ((home->myDomain == 0) && (param->precCount != globalPrecCount)) {
            Fatal("ReadPrecDataFile: Read %d precipitates, expected %d!",
                  globalPrecCount, param->precCount);
        }

        return;
}

#endif

