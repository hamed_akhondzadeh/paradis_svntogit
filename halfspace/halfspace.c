/***************************************************************************
 *
 *  Module      : halfspace.c
 *  Description : Calculates stress at free surfaces of the halfspace
 *                Apply Indentation Load and Displacement
 *  (Sylvie Aubry Fri Feb 22 2008)
 *  (Dibakar Datta & Wei Cai; April Fool Day , 2016)
 *
 **************************************************************************/


#include "Home.h"

#ifdef _HALFSPACE

#include "HS.h"

// ------ Materials Constant -------- //

#define lambda (halfspace->lambda)
#define mu     (halfspace->mu)
#define nu     lambda/(2*(lambda+mu))
#define mult   (1-nu)/mu
#define PI      3.14159265

// ------- Function to allocate global variables  -- //

void HS_allocations(Home_t *home, HalfSpace_t *halfspace)
{
    
    int NXMAX = halfspace->nx;
    int NYMAX = halfspace->ny;
    
    int i,j,k;
    
    /* ================================================================= */
    // ------------------ Specify External Indentation Load --------- //
 
 
 #ifdef _INDENFORCE
 
    halfspace->TzextSpec = (double *) malloc(sizeof(double)*NXMAX*NYMAX);
    
    if (halfspace->TzextSpec == NULL)
    {
      printf("Not enough memory to allocate TzextSpec\n");
      exit(0);
    }
    
 #endif
 
    /* ================================================================= */
    // ----- Declare Pressure and Displacement for Indentation --------- //
    
 #ifdef _INDENTATION
 
    //halfspace->d_indenter = 1.0;
    //halfspace->R_indenter = 50.0;
    
    /* ------------------- ----------------- -------------------- */
    halfspace->Uz_indenter = (double *) malloc(sizeof(double)*NXMAX*NYMAX);
    
    if (halfspace->Uz_indenter == NULL)
    {
      printf("Not enough memory to allocate Uz\n");
      exit(0);
    }
    /* ------------------- ----------------- -------------------- */
    halfspace->Uz_elas = (double *) malloc(sizeof(double)*NXMAX*NYMAX);
    
    if (halfspace->Uz_elas == NULL)
    {
      printf("Not enough memory to allocate Uz\n");
      exit(0);
    }
    /* ------------------- ----------------- -------------------- */
    halfspace->Uz_plas = (double *) malloc(sizeof(double)*NXMAX*NYMAX);
    
    if (halfspace->Uz_plas == NULL)
    {
      printf("Not enough memory to allocate Uz\n");
      exit(0);
    }
    /* ------------------- ----------------- -------------------- */
    halfspace->Tzext = (double *) malloc(sizeof(double)*NXMAX*NYMAX);
    
    if (halfspace->Tzext == NULL)
    {
      printf("Not enough memory to allocate Pz\n");
      exit(0);
    }
    /* ------------------- ----------------- -------------------- */
    
 #endif
 
    /* ================================================================= */
    
    halfspace->kx = (double *)malloc(sizeof(double)*NXMAX);
    if (halfspace->kx == NULL)
    {
        printf("Not enough memory to allocate kx\n");
        exit(0);
    }
    
    halfspace->ky = (double *)malloc(sizeof(double)*NYMAX);
    if (halfspace->ky == NULL)
    {
        printf("Not enough memory to allocate ky\n");
        exit(0);
    }
    
    halfspace->Tx = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
    if (halfspace->Tx == NULL)
    {
        printf("Not enough memory to allocate Tx\n");
        exit(0);
    }
    
    halfspace->Ty = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
    if (halfspace->Ty == NULL)
    {
        printf("Not enough memory to allocate Ty\n");
        exit(0);
    }
    
    halfspace->Tz = (fftw_complex *)malloc(sizeof(fftw_complex)*NXMAX*NYMAX);
    if (halfspace->Tz == NULL)
    {
        printf("Not enough memory to allocate Tz\n");
        exit(0);
    }
    
    halfspace->A = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
    if (halfspace->A == NULL)
    {
        printf("Not enough memory to allocate A\n");
        exit(0);
    }
    
    halfspace->B = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
    if (halfspace->B == NULL)
    {
        printf("Not enough memory to allocate B\n");
        exit(0);
    }
    
    halfspace->C = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
    if (halfspace->C == NULL)
    {
        printf("Not enough memory to allocate C\n");
        exit(0);
    }
    
    for (i=0;i<NXMAX;i++)
    {
        halfspace->A[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
        halfspace->B[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
        halfspace->C[i] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
        
        if (halfspace->A[i] == NULL || halfspace->B[i] == NULL ||
            halfspace->C[i] == NULL )
        {
            printf("Not enough memory to allocate ABC\n");
            exit(0);
        }
    }
    
    for (i=0;i<3;i++)
        for (j=0;j<3;j++)
        {
            halfspace->MInv[i][j] = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
            halfspace->Stress[i][j] = (COMPLEX **)malloc(sizeof(COMPLEX*)*NXMAX);
            
            if (halfspace->MInv[i][j] == NULL ||
                halfspace->Stress[i][j] == NULL)
            {
                printf("Not enough memory to allocate MInv or Stress array\n");
                exit(0);
            }
        }
    
    for (i=0;i<3;i++)
        for (j=0;j<3;j++)
            for (k=0;k<NXMAX;k++)
            {
                halfspace->MInv[i][j][k] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
                halfspace->Stress[i][j][k] = (COMPLEX *)malloc(sizeof(COMPLEX)*NYMAX);
                
                if (halfspace->MInv[i][j][k] == NULL ||
                    halfspace->Stress[i][j][k] == NULL)
                {
                    printf("Not enough memory to allocate MInv or Stress array\n");
                    exit(0);
                }
            }
    
    
    for (i=0;i<3;i++)
    {
        halfspace->Grid[i] = (double **)malloc(sizeof(double*)*NXMAX);
        
        if (halfspace->Grid[i] == NULL)
        {
            printf("Not enough memory to allocate Grid array\n");
            exit(0);
        }
    }
    
    for (i=0;i<3;i++)
        for (k=0;k<NXMAX;k++)
        {
            halfspace->Grid[i][k] = (double *)malloc(sizeof(double)*NYMAX);
            
            if (halfspace->Grid[i][k] == NULL)
            {
                printf("Not enough memory to allocate Grid array\n");
                exit(0);
            }
        }
    
    if (home->myDomain == 0) printf("All arrays allocated NXMAX = %d, NYMAX= %d\n\n\n",NXMAX,NYMAX);
    
}

void HS_Create_Matrices(HalfSpace_t *halfspace)
{
    Minvmatrix(halfspace);
}

void HS_Create_Grid(Param_t *param, HalfSpace_t *halfspace)
{
    int nx,ny,i,j;
    real8 x,y;
    real8 HSLx,HSLy,t;
    real8 difX;
    real8 difY;
    
    nx = halfspace->nx;
    ny = halfspace->ny;
    
    HSLx = halfspace->HSLx;
    HSLy = halfspace->HSLy;
    
    t = 0.0;
    
    difX = HSLx/(1.0*nx);
    difY = HSLy/(1.0*ny);
    
    for (i=0; i<nx; i++)
    {
        x = param->minSideX + difX*i;
        
        for (j=0; j<ny; j++)
        {
            y = param->minSideY + difY*j;
            
            halfspace->Grid[0][i][j] = x;
            halfspace->Grid[1][i][j] = y;
            halfspace->Grid[2][i][j] = t;
        }
    }
}


void HS_Create_kpoints(HalfSpace_t *halfspace)
{
    int i,j;
    
    int nx = halfspace->nx;
    int ny = halfspace->ny;
    
    real8 HSLx = halfspace->HSLx;
    real8 HSLy = halfspace->HSLy;
    
    int imax = nx/2 + nx % 2;
    int jmax = ny/2 + ny % 2;
    
    for (j=0; j<ny; j++)
    {
        if (j < jmax)
            halfspace->ky[j]=j*2*M_PI/HSLy;
        else
            halfspace->ky[j]=(j-ny)*2*M_PI/HSLy;
    }
    
    for (i=0; i<nx; i++)
    {
        if (i < imax)
            halfspace->kx[i]=i*2*M_PI/HSLx;
        else
            halfspace->kx[i]=(i-nx)*2*M_PI/HSLx;
    }
}


void HS_stress_boundary(Home_t *home,HalfSpace_t *halfspace)
{
    // Calculate Tractions on half space surfaces from stress in the system
    // F = sigma . n  = -T
    // Done every time step
    
    int i,j,k,l;
    real8 s[3][3],grids[3];
    
    int nx = halfspace->nx;
    int ny = halfspace->ny;
    real8 HSLx = halfspace->HSLx;
    real8 HSLy = halfspace->HSLy;
    real8 *tx, *ty, *tz, *tx_tot, *ty_tot, *tz_tot;
        
    // --------  Allocate tx, ty, tz ---------- // 
    
    tx = (real8 *)malloc(sizeof(double)*nx*ny);
    ty = (real8 *)malloc(sizeof(double)*nx*ny);
    tz = (real8 *)malloc(sizeof(double)*nx*ny);
    
    tx_tot = (real8 *)malloc(sizeof(double)*nx*ny);
    ty_tot = (real8 *)malloc(sizeof(double)*nx*ny);
    tz_tot = (real8 *)malloc(sizeof(double)*nx*ny);
    
    // Top surface
    for (i=0; i<nx; i++)
        for (j=0; j<ny; j++)
        {
            grids[0] = halfspace->Grid[0][i][j];
            grids[1] = halfspace->Grid[1][i][j];
            grids[2] = halfspace->Grid[2][i][j];
            
            AllSegmentStress(home,halfspace,grids[0],grids[1],grids[2],s);
            
#ifndef _NOYOFFESTRESS
            int ii,jj;
            real8 Ys[3][3];
            AllYoffeStress(home,halfspace,grids[0],grids[1],grids[2],Ys);
            for (ii=0; ii<3; ii++)
                for (jj=0; jj<3; jj++)
                    s[ii][jj] += Ys[ii][jj];
#endif
            
            tx[j+i*ny] = -s[0][2];
            ty[j+i*ny] = -s[1][2];
            tz[j+i*ny] = -s[2][2];
        }
    
#ifdef PARALLEL
    MPI_Allreduce(tx, tx_tot, nx*ny, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(ty, ty_tot, nx*ny, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(tz, tz_tot, nx*ny, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
    for (i = 0; i < nx; i++)
        for (j = 0; j < ny; j++)
        {
            tx_tot[j+i*ny] = tx[j+i*ny];
            ty_tot[j+i*ny] = ty[j+i*ny];
            tz_tot[j+i*ny] = tz[j+i*ny];
        }
#endif

  // ------- Compute Nodal Force ------ 
 
 /*
  #ifndef _INDENTATION
    
   Comp_NodalForce(home, halfspace);
  
  #endif
  
  */
  
  // --------- Function for indentation displacement ------
    
 #ifdef _INDENFORCE
 
  Indent_Specify_Tzext(halfspace);
    
 #endif
  
  // -----------------------   
    
 #ifdef _INDENTATION
    
    Indent_Init_Shape(halfspace);
    
  // -------------- Plasticity ------------
    
   #ifdef _PLASTICITY
   
   // if (home->myDomain == 0) printf("ACTIVATE PLASTIC DEFORMATION, \n");
    
    int cyclenow = home->cycle;
    
    if(cyclenow == 0)
    {
     if (home->myDomain == 0) printf("CyclePlas= %d, \n",cyclenow);
     Inden_Plas(home, halfspace);
    } 
    
   #endif
   
  // ------------------------------------------------
    
    Indent_Calc_Tzext_from_d(home, halfspace);
  
 #endif
    
 // -------------- Add External Indentation Loading and Displacement ---------------
    
    for (i=0; i<nx; i++)
    {
        for (j=0; j<ny; j++)
        {
        
#ifndef _CYGWIN
            halfspace->Tx[j+i*ny] = tx_tot[j+i*ny];
            halfspace->Ty[j+i*ny] = ty_tot[j+i*ny];
            
            // ------------ Traction in Z Direction ---------------------- 
            
            #ifdef _INDENTATION
            halfspace->Tz[j+i*ny] = tz_tot[j+i*ny]+halfspace->Tzext[j+i*ny];
            #else
                   #ifdef _INDENFORCE
                   halfspace->Tz[j+i*ny] = tz_tot[j+i*ny]+halfspace->TzextSpec[j+i*ny];
                   #else
                     halfspace->Tz[j+i*ny] = tz_tot[j+i*ny];
                   #endif
           #endif          
           
                        /* *************************** */
#else
            halfspace->Tx[j+i*ny][0] = tx_tot[j+i*ny];
            halfspace->Ty[j+i*ny][0] = ty_tot[j+i*ny];
            
          // ------------ Traction in Z Direction ---------------------- 
            #ifdef _INDENTATION
            halfspace->Tz[j+i*ny][0] = tz_tot[j+i*ny]+halfspace->Tzext[j+i*ny];
            #else
                   #ifdef _INDENFORCE
                   halfspace->Tz[j+i*ny][0] = tz_tot[j+i*ny]+halfspace->TzextSpec[j+i*ny];
                   #else
                     halfspace->Tz[j+i*ny][0] = tz_tot[j+i*ny];
                   #endif
           #endif          
           // ------------ ------------ ------------ ------------ --------
           
            halfspace->Tx[j+i*ny][1] = 0.0;
            halfspace->Ty[j+i*ny][1] = 0.0;
            halfspace->Tz[j+i*ny][1] = 0.0;
#endif
        }
    }
    
    free(tx);
    free(ty);
    free(tz);
    free(tx_tot);
    free(ty_tot);
    free(tz_tot);
}

// ------------------------- Start of Indentation ---------------------- //

#ifdef _INDENFORCE

void Indent_Specify_Tzext(HalfSpace_t *halfspace)
{
   int i,j;
    
   int nx = halfspace->nx, ny = halfspace->ny;
    
   double idbl, jdbl, nxdbl, nydbl;
   nxdbl = (double) nx; nydbl = (double) ny;
  
  // ---------------
  for (i=0; i<nx; i++)
    {
        idbl = (double) i;
        for (j=0; j<ny; j++)
        {
            jdbl = (double) j;
            
  #ifndef _CYGWIN
             if ((pow((idbl-nxdbl/2.0),2.0)+pow((jdbl-nydbl/2.0),2.0)) < pow(nxdbl/10.0,2.0))
              {
                halfspace->TzextSpec[j+i*ny] = -0*(1.0-pow(((idbl-nxdbl/2.0)/(nxdbl/10.0)),2.0)-pow(((jdbl-nydbl/2.0)/(nydbl/10.0)),2.0));
              }
  #else
           halfspace->TzextSpec[j+i*ny][0] = 0.0;
           halfspace->TzextSpec[j+i*ny][1] = 0.0;
  #endif
        }
   } 
 // -------------
 
  Comp_NodalForce(home, halfspace);
  
}

#endif

// ==============================================================
// ----------------- Implement Plasticity -----------------------
#ifdef _INDENTATION

 #ifdef _PLASTICITY
 
 // -----------------------------------------------
//        Some function files required 
// -----------------------------------------------
  
  double mvalcomp(double p1[2], double p2[2]){
         
         double x1 = p1[0]; double y1 = p1[1];
         double x2 = p2[0]; double y2 = p2[1];        
         double mval;
         mval = (y1 - y2)/(x1 - x2);
         return mval;
  }
  
  double cvalcomp(double p1[2], double m){
        double cval;
        cval = p1[1]-m*p1[0];
        return cval;
  }
  
  double normcomp(double v[3]){
         double normv = v[0]*v[0]+v[1]*v[1]+v[2]*v[2];
         normv = sqrt(normv);
         return normv;
  }

void Inden_Plas(Home_t *home, HalfSpace_t *halfspace)
{
  int i,j,nc;
  double dx, dy, lx, ly;
  double x0, y0, z0, x1, y1, z1, x2, y2, z2,x3, y3, z3;
  double tranVec[3], tranVecNorm, tranVecU[3], tranLen, tranx, trany, tranz;
  double pointsDis[3][4], pointsDisTran[3][4], pointsDisTranProj[3][4];
  
  //double mvalcomp(double p1[2], double p2[2]);
  //double cvalcomp(double p1[2], double m);
  //double normcomp(double v[3]);
  
  double m1, c1, m2, c2, m3, c3, m4, c4;
  double point0[2], point1[2], point2[2], point3[2];
  // double thetaProj = PI/4.0, burgProj = -1.0*cos(thetaProj);
  // double thetaProj = PI/4.0, burgProj = -10.0*cos(thetaProj);
  double thetaProj = PI/4.0, burgProj = -2.0*cos(thetaProj);
  
  halfspace->surfdis = burgProj; 
  
  int nx = halfspace->nx, ny = halfspace->ny; 
  real8 HSLx = halfspace->HSLx, HSLy = halfspace->HSLy;
  dx = (1.0/nx)*HSLx; dy = (1.0/ny)*HSLy;
  
  double vline1, vline2, vline3, vline4;

  // ----------------------------------------------- 

  Node_t *node0, *node1, *node2, *node3;
  
  nc = home->newNodeKeyPtr;
  
  // WARNING: hard-coded surface inprint for 4 nodes
  if (nc == 4) {
  
  //if (home->myDomain == 0) printf(" TestVal = %d,\n", nc);
  
  node0 = home->nodeKeys[0]; node1 = home->nodeKeys[1]; node2 = home->nodeKeys[2]; node3 = home->nodeKeys[3];
  
  x0 = node0->x; y0 = node0->y; z0 = node0->z; x1 = node1->x; y1 = node1->y; z1 = node1->z; 
  x2 = node2->x; y2 = node2->y; z2 = node2->z; x3 = node3->x; y3 = node3->y; z3 = node3->z; 
  
  pointsDis[0][0] = x0; pointsDis[0][1] = x1; pointsDis[0][2] = x2; pointsDis[0][3] = x3; 
  pointsDis[1][0] = y0; pointsDis[1][1] = y1; pointsDis[1][2] = y2; pointsDis[1][3] = y3;
  pointsDis[2][0] = z0; pointsDis[2][1] = z1; pointsDis[2][2] = z2; pointsDis[2][3] = z3;
  
  tranVec[0] = -x0; tranVec[1] = -y0;  tranVec[2] = -z0; tranVecNorm = normcomp(tranVec); 
  tranVecU[0] = tranVec[0]/tranVecNorm; tranVecU[1] = tranVec[1]/tranVecNorm; tranVecU[2] = tranVec[2]/tranVecNorm;
  tranLen = sqrt(tranVec[0]*tranVec[0] + tranVec[1]*tranVec[1] + tranVec[2]*tranVec[2]);
  tranx = tranLen*tranVecU[0]; trany = tranLen*tranVecU[1];  tranz = tranLen*tranVecU[2];
  
  for (i=0; i<nc; i++)
  {
    pointsDisTran[0][i] = pointsDis[0][i] + tranx;
    pointsDisTran[1][i] = pointsDis[1][i] + trany;
    pointsDisTran[2][i] = pointsDis[2][i] + tranz;
  }
  
  for (i=0; i<nc; i++)
  {
    pointsDisTranProj[0][i] = pointsDisTran[0][i];
    pointsDisTranProj[1][i] = pointsDisTran[1][i];
    pointsDisTranProj[2][i] = 0.0;
  }
  
  // ------- Take the points point0, point1, point2, point3 --------
  
  point0[0] = pointsDisTranProj[0][0];  point0[1] = pointsDisTranProj[1][0];
  point1[0] = pointsDisTranProj[0][1];  point1[1] = pointsDisTranProj[1][1];
  point2[0] = pointsDisTranProj[0][2];  point2[1] = pointsDisTranProj[1][2];
  point3[0] = pointsDisTranProj[0][3];  point3[1] = pointsDisTranProj[1][3];
  
  // ------- Equation # 1 ; [m1,c1] -------
  m1 = mvalcomp(point0, point1); c1 = cvalcomp(point0, m1);
  
 // ------- Equation # 2 ; [m2,c2] ------- 
  m2 = mvalcomp(point1, point2); c2 = cvalcomp(point1, m2);
  
 // ------- Equation # 3 ; [m3,c3] -------
  m3 = mvalcomp(point2, point3); c3 = cvalcomp(point2, m3);
  
 // ------- Equation # 4 ; [m4,c4] -------
  m4 = mvalcomp(point3, point0); c4 = cvalcomp(point3, m4);
  
 // ----------------------------------------------------------------

 for (i = 0; i<nx; i++)
 {   
   for (j = 0; j<ny; j++)
   {
      lx = ((1.0*j)/ny)*HSLy-HSLy/2.0;
      ly = ((1.0*i)/nx)*HSLx-HSLx/2.0;
    
      vline1 = m1*lx-ly+c1;  vline2 = m2*lx-ly+c2; 
      vline3 = m3*lx-ly+c3;  vline4 = m4*lx-ly+c4;
      
      if ((vline1 <= 0) && (vline2 >= 0) && (vline3 >= 0) && (vline4 <= 0))
      {
          halfspace->Uz_plas[j+i*ny] = burgProj;
      }
   }
 }
 
 }

 // ----------------------------------------------
 //    Write File to Save Plastic Deformation 
 // ----------------------------------------------
 
 FILE *fp;
 char format[15];
 sprintf(format, "PlasticDeformation.txt");

 fp = fopen(format,"w");
 
 for (i=0; i<nx; i++){
  for (j=0; j<ny; j++){
  fprintf(fp,"%20.12e\t",halfspace->Uz_plas[j+i*ny]);
  }  
  fprintf(fp,"\n");
 }
 fclose(fp);
 
}

 #endif

// --------------------------------------------------------------
// ==============================================================

// ==============================================================
// ----------- Initiate Initial Indenter Shape ------------------

void Indent_Init_Shape(HalfSpace_t *halfspace)
{
   int i,j;
    
   int nx = halfspace->nx;
   int ny = halfspace->ny;
    
   real8 HSLx = halfspace->HSLx;
   real8 HSLy = halfspace->HSLy;
    
   double R = halfspace->R_indenter;
   double lx, ly; // x & y coordinates along Lx and Ly directions
   
  for (i=0; i<nx; i++)
   {
   lx = ((1.0*i)/nx)*HSLx;
    for (j=0; j<ny; j++)
     {
      ly = ((1.0*j)/ny)*HSLy;
      halfspace->Uz_indenter[j+i*ny] = ((lx-HSLx/2.0)*(lx-HSLx/2.0)+(ly-HSLy/2.0)*(ly-HSLy/2.0))/(2.0*R);
     }
   } 
   
   // ---------------------------------------
   //  Store the Indenter Value 
   
  FILE *fp;
  char format[15];
  sprintf(format, "IniIndShape.txt");

  fp = fopen(format,"w");
 
  for (i=0; i<nx; i++){
    for (j=0; j<ny; j++){
     fprintf(fp,"%20.12e\t",halfspace->Uz_indenter[j+i*ny]);
     }  
    fprintf(fp,"\n");
    }
  fclose(fp);

   // --------------------------------------
   
}

// --------------------------------------------------------------
// ==============================================================

// ==============================================================
// -------------------- Implement Indentation  ------------------

void Indent_Calc_Tzext_from_d(Home_t *home,HalfSpace_t *halfspace)
{
  
  // ----------------------- Declare Variables ------------------
  
  int i,j,ind;
  double dx, dy;
    
  int nx = halfspace->nx, ny = halfspace->ny;
  real8 HSLx = halfspace->HSLx, HSLy = halfspace->HSLy;
  
  double areasum, forcesum;
  double *kx = halfspace->kx, *ky = halfspace->ky;
  double *oneoverkz, *kz;
  double tnow = home->param->timeNow;
  int cyclenow = home->cycle;
  // double d = halfspace->d_indenter;
  double d;
  double area_indent = halfspace->area_indent;
  double force_indent = halfspace->force_indent;
  
  double *pzext; // External pressure due to indentation  
  double *uz; // Displacement due to indentation 
  real8 tol; // tolerance
  real8 delta; // step size adjusting traction force
  real8 error; // error to check with the tolerance
  int maxiter; // max number of iteration
  int iter; // iteration
  int indarea; // Indentation temporary area 
  
  int filediv = 2500;
  // int filediv = 10;
  
  Node_t *NodeTmp; 
  double hdis0, hdisavg, zTmp;
  double fx_simg, fy_simg, fz_simg;
  
  // ---------------------------- Allocate/Define Variables ----------------------
  
  dx = (1.0/nx)*HSLx, dy = (1.0/ny)*HSLy;
   
 #ifndef _CYGWIN
    fftw_complex *pzkext;
    fftw_complex *uzk;
    fftw_complex *uztmp;
    fftw_complex *pz;
 #else
    complex *pzkext;
    complex *uzk;
    complex *uztmp;
    complex *pz;
 #endif 
   
  oneoverkz = (double *)malloc(sizeof(double)*nx*ny);
  pzkext = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
  uzk = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
  uztmp = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
  pz = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
  pzext = (double *)malloc(sizeof(double)*nx*ny);
  uz = (double *)malloc(sizeof(double)*nx*ny);  
   
 // ---- Allocate 1/kz ------ 
 
   oneoverkz[0] = 0;

      for (i=0; i<nx; i++)
        {
         for (j=0; j<ny; j++)
           {
               if ((i > 0) || (j > 0)){
                     oneoverkz[j+i*ny] = 1.0/sqrt(kx[i]*kx[i]+ky[j]*ky[j]);
                         }
            }
        }
        
 // ------ Values for indentation ------

  tol = 1e-6; // Tolerance 
  delta = 0.10; // step size adjusting traction force
  maxiter = 1000; // Number of iteration 
       
  // LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  // ================================ For Defining Loading ============================
  
  int stept = home->param->maxstep;
  int loadstep = round(0.50*stept);
  int numstep = 1; // User Input for number of step for loading and unloading 
  int stepmid = round(loadstep*(20.0/100.0));
  int totqsload = loadstep - (numstep*stepmid);
  int deltaload = round(totqsload/(1.0*numstep));
  
  // Unloading 
  int totqsunload = stept - loadstep - (numstep*stepmid);
  int deltaunload = round(totqsunload/(1.0*numstep));

 if (cyclenow == loadstep) 
 {
    halfspace->tnowmid = home->param->timeNow;
 }
   // LLLLLLLLLLLLLLLLLLLLLLLL  LoadStyle # 1 
 
 halfspace->d_indenter = halfspace->d0_indenter + halfspace->v_indenter*tnow;
 d = halfspace->d_indenter;
 
 printf(" d0, v, t, d, %20.12e, %20.12e, %20.12e, %20.12e,\n", halfspace->d0_indenter, halfspace->v_indenter, tnow, d);

   // LLLLLLLLLLLLLLLLLLLLLLLL  LoadStyle # 2 


   // LLLLLLLLLLLLLLLLLLLLLLLL  LoadStyle # 3

// =============================== End of Loading Definition ==========================
    
   
// +++++++++++++++++++++++++ Start Main Indentation Loop +++++++++++++++++++++++++++ //

{ 
 // ------------------ Initialize Surface Displacement and Pressure ------------ // 
    for (i=0; i<nx; i++)
      {
      for (j=0; j<ny; j++)
       {
        uz[j+i*ny] = halfspace->Uz_elas[j+i*ny];
        pzext[j+i*ny] = halfspace->Tzext[j+i*ny];
       }
     }
 // --------------------- Perform Main Iteration ------------------ //
 
 if(fabs(d) < fabs(halfspace->surfdis)){
      printf(" No Surface Imprint, %20.12e, %20.12e, \n", fabs(d), fabs(halfspace->surfdis));
  }
  
 // -------------------------------------- // 
 
 for (iter = 0; iter<maxiter ; iter++)
 {  
   // pz is complex internal variables for performing fourier transform 
   
   for (i=0; i<nx; i++)
   {
    for (j=0; j<ny; j++)
      {
         pz[j+i*ny]= pzext[j+i*ny];   
      }
    }
    
  // Fourier transform pz to 
    fourier_transform_forward(pz,(fftw_complex *)pzkext,nx,ny);
    
  // Obtain Fourier transform of uz 
  for (i=0; i<nx; i++)
   {
     for (j=0; j<ny; j++)
      {
         uzk[j+i*ny] = mult*pzkext[j+i*ny]*oneoverkz[j+i*ny];
      }
   }
   
  // Inverse Fourier Transform //
  fourier_transform_backward(uztmp,(fftw_complex *)uzk,nx,ny);
  
  // Get back the displacement 
    for (i=0; i<nx; i++)
     {
      for (j=0; j<ny; j++)
      {
        uz[j+i*ny] = creal(uztmp[j+i*ny]/(double)(nx*ny));
      }
     }
     
  // ------------- ------------ ------------ -------------- //
  // Adjust pz so that pz->0 where uz < u0-d (gap region)
  // Apply a loop here 
   error = 0;
   
   for (i=0; i<nx; i++)
   {
     for (j=0; j<ny; j++)
     {  
  
  // ****************** Plasticity ***************
  
  #ifdef _PLASTICITY  

      pzext[j+i*ny] = pzext[j+i*ny]-(uz[j+i*ny]-(halfspace->Uz_indenter[j+i*ny]-d)+halfspace->Uz_plas[j+i*ny])*delta;
         
  #endif  
  
  // ****************** No Plasticity **************
  
  #ifndef _PLASTICITY 
        pzext[j+i*ny] = pzext[j+i*ny]-(uz[j+i*ny]-(halfspace->Uz_indenter[j+i*ny]-d))*delta; 
  #endif      
    
  // ***********************************************
  
    // Adjust pressure 
        if (pzext[j+i*ny] > 0){
           pzext[j+i*ny] = 0;
            }    
    // Error estimate     
       error = error + (fabs((halfspace->Uz_indenter[j+i*ny]-d)-uz[j+i*ny])*fabs(pzext[j+i*ny]))/d;
     }
   }
   
   // ----- Check error with tolerance -----            
       if(error < tol){
         break; 
       }
       
   // ============== End of loop for iteration ========= 
   
   }
   
   // ----------- Assign the converged value to global variable ----------- 
   
    for (i=0; i<nx; i++)
    {
      for (j=0; j<ny; j++)
      {
        halfspace->Uz_elas[j+i*ny] = uz[j+i*ny];
        halfspace->Tzext[j+i*ny] = pzext[j+i*ny];
      }
    }
    
   // ----------- Find contact area and total force -----------
   
  indarea = 0; forcesum = 0.0; areasum = 0.0;

   for (i = 0; i<nx; i++)
   {
     for (j = 0; j<ny; j++)
     {
       forcesum = forcesum + halfspace->Tzext[j+i*ny];
        
       if(halfspace->Tzext[j+i*ny]<0.0)
       {
        indarea = indarea + 1;
       }
     }
   }
   
  areasum = (1.0*indarea)*dx*dx;
  forcesum = (-1.0*forcesum)*dx*dx;
  
  halfspace->area_indent = areasum;
  halfspace->force_indent = forcesum;
    
   // =================== Write nodal force ====================== //  

  Comp_NodalForce(home, halfspace);
  fx_simg = halfspace->fx_img;
  fy_simg = halfspace->fy_img;
  fz_simg = halfspace->fz_img;
  
 // =============================================================================== // 
 // ----------------------------------------------
 //    Write File to Save Surface Deformation  
 // ----------------------------------------------
 
if(cyclenow % filediv == 0){
 
  FILE *fp1;
  char format1[15];
 // sprintf(format1, "SurfaceDisp.txt");
  sprintf(format1, "SurfaceDisp%d.txt",cyclenow);

  fp1 = fopen(format1,"w");
 
  for (i=0; i<nx; i++){
   for (j=0; j<ny; j++){
     fprintf(fp1,"%20.12e\t",uz[j+i*ny]);
     }  
     fprintf(fp1,"\n");
   }
  fclose(fp1);
 
 // ----------------------------------------------
 
  FILE *fp2;
  char format2[15];
 // sprintf(format2, "Pressure.txt");
  sprintf(format2, "Pressure%d.txt",cyclenow);

  fp2 = fopen(format2,"w");
 
  for (i=0; i<nx; i++){
   for (j=0; j<ny; j++){
   fprintf(fp2,"%20.12e\t",pzext[j+i*ny]);
      }  
      fprintf(fp2,"\n");
    }
  fclose(fp2);
 }
 
// =============================================================================== //
// ----------------------- Write file name for indentation ----------------------- // 

    // Avg depth of dislocation //
    
/*
	hdisavg = 0.0;
    for (i = 0; i < home->newNodeKeyPtr; i++){
		 if ((NodeTmp = home->nodeKeys[i]) == (Node_t *)NULL) continue; // Nicolas' Correction
         zTmp = NodeTmp->z;
         hdisavg += zTmp;
     } 
    hdisavg = hdisavg/home->newNodeKeyPtr;
    
    NodeTmp = home->nodeKeys[0];
    	
	hdis0 = NodeTmp->z;  // Depth of 0th Node //  
	
*/

// -------------- Get the Indentation Information  -------- // 
    
	FILE *fp;
	char format[15];
	sprintf(format, "indendata.txt");

	fp = fopen(format,"a");
   
   // fprintf(fp,"%d, %20.12e, %20.12e, %20.12e, %20.12e, %20.12e, %20.12e, %20.12e, %20.12e, %20.12e, \n", cyclenow, tnow, d, areasum, forcesum, hdis0, hdisavg, fx_simg, fy_simg, fz_simg);
   //fprintf(fp,"%d, %20.12e, %20.12e, %20.12e, %20.12e, %20.12e, %20.12e, %20.12e, \n", cyclenow, tnow, d, areasum, forcesum, fx_simg, fy_simg, fz_simg);

    fprintf(fp,"%d, %20.12e, %20.12e, %20.12e, %20.12e,\n", cyclenow, tnow, d, areasum, forcesum);

    fclose(fp);  
 
 
 // ============================================================================= //   
    
}
 
 free(pzkext);
 free(uzk);
 free(uztmp);
 free(pz);
 free(pzext);
 free(uz);
}

#endif

// ------------------------- End of Indentation ----------------------
// ===================================================================

// ===================================================================
// -----------------------  Compute Nodal Force  ---------------------

void Comp_NodalForce(Home_t *home, HalfSpace_t *halfspace){

   Node_t *node1, *node2;
   int i1, i2, nc ; 
   double x, y;
   double xA, yA, zA, xB, yB, zB;
   double bX, bY, bZ;
   double tx, ty, tz, magt, sign;
   double sbx, sby, sbz;
   double sbxx, sbxy, sbxz, sbyx, sbyy, sbyz, sbzx, sbzy, sbzz;
   double gx, gy, gz;
   double fx_img, fy_img, fz_img;
   double px[3];
   double sdis[3][3], simg[3][3];
   double fx_seg[home->newNodeKeyPtr], fy_seg[home->newNodeKeyPtr], fz_seg[home->newNodeKeyPtr];
   
   Param_t *param;
   param = home->param;
   double b = param->burgMag;
 
  // Loop over all segments to average force // 
	
   int nseg = 0;
   fx_img = 0.0; fy_img = 0.0; fz_img = 0.0;
	
    for (i1 = 0; i1 < home->newNodeKeyPtr; i1++){
         node1 = home->nodeKeys[i1];
         if (!node1) continue;
         
         nc = node1->numNbrs;
                  
         xB = node1->x; yB = node1->y; zB = node1->z;
         
         for (i2 = 0; i2 < nc; i2++) {
              node2 = GetNeighborNode(home, node1, i2);
              if (!node2) continue;
              
              if (OrderNodes(node2, node1) != 1) continue;
              
              xA = node2->x; yA = node2->y; zA = node2->z;
              
              bX = node1->burgX[i2], bY = node1->burgY[i2], bZ = node1->burgZ[i2];

              tx = xA - xB; ty = yA - yB; tz = zA - zB;
                            
              magt = sqrt(tx*tx+ty*ty+tz*tz);
              
              tx = tx/magt; ty = ty/magt; tz = tz/magt;
              
              // Middle position // 
              
              px[0] = 0.50*(xA + xB); px[1] = 0.50*(yA + yB); px[2] = 0.50*(zA + zB);
              
              Init3x3(simg);
              DispStress(halfspace, px, simg);
              
              sbxx = b*simg[0][0];  sbxy = b*simg[0][1];  sbxz = b*simg[0][2]; 
              sbyx = b*simg[1][0];  sbyy = b*simg[1][1];  sbyz = b*simg[1][2]; 
              sbzx = b*simg[2][0];  sbzy = b*simg[2][1];  sbzz = b*simg[2][2]; 
              
              gx = bX*sbxx + bY*sbxy + bZ*sbxz;  
              gy = bX*sbyx + bY*sbyy + bZ*sbyz; 
              gz = bX*sbzx + bY*sbzy + bZ*sbzz;
              
              fx_seg[nseg] = gy*tz - gz*ty; 
              fy_seg[nseg] = gx*tz - gz*tx;
              fz_seg[nseg] = gx*ty - gy*tx;
              
              fx_img += fx_seg[nseg];
              fy_img += fy_seg[nseg];
              fz_img += fz_seg[nseg];
              
              nseg++;
            }    
    }
    
    halfspace->fx_img = fx_img / nseg;
    halfspace->fy_img = fy_img / nseg;
    halfspace->fz_img = fz_img / nseg;
    
  // if (home->myDomain == 0) printf("fx = %20.12e, fy = %20.12e, fz = %20.12e,\n",halfspace->fx_img, halfspace->fy_img, halfspace->fz_img); 		
}

// -------------------------------------------------------------------
// ===================================================================

int Split(Home_t *home,Node_t *nodeout,Node_t *nodein, real8 t)
{
    // Create a node between nodeout and nodein on the halfspace surface.
    // nodeout is then placed at the surface.
    
    // nodeout becomes splitNode1
    // new node is SplitNode2
    // nodein untouched
    
    int armCount, splitStatus, globalOp, armID, *armList;
    real8 nodeVel[3], newVel[3];
    Node_t *splitNode1, *splitNode2;
    real8 xout[3],xin[3];
    real8 pos[3],vec[3];
    Param_t *param;
    
    param = home->param;
    
    xout[0] = nodeout->x;
    xout[1] = nodeout->y;
    xout[2] = nodeout->z;
    
    xin[0] = nodein->x;
    xin[1] = nodein->y;
    xin[2] = nodein->z;
    
    vec[0] = xin[0] - xout[0];
    vec[1] = xin[1] - xout[1];
    vec[2] = xin[2] - xout[2];
    
    real8 lr = sqrt(vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]);
    vec[0] /= lr;vec[1] /=lr;vec[2] /=lr;
    
    // Velocity for nodeout
    if (fabs(vec[2]) > 0.05)
    {
        nodeVel[0] = nodeout->vX - nodeout->vZ * vec[0] / vec[2];
        nodeVel[1] = nodeout->vY - nodeout->vZ * vec[1] / vec[2];
        nodeVel[2] = 0.0;
    }
    else
    {
        nodeVel[0] = nodeout->vX*vec[2] - nodeout->vZ * vec[0];
        nodeVel[1] = nodeout->vY*vec[2] - nodeout->vZ * vec[1];
        nodeVel[2] = 0.0;
    }
    
    newVel[0] = nodeVel[0];  //  for splitNode2
    newVel[1] = nodeVel[1];
    newVel[2] = nodeVel[2];
    
    
    // Position of the new node
    GetSurfaceNode(param,nodeout,nodein,pos,t);
    
    globalOp = 1;
    armCount = 1;
    armID = GetArmID(home, nodeout, nodein);
    armList = &armID;
    
    
    // splitNode1 == new node
    // splitNode2 == node out
    splitStatus = SplitNode ( home, OPCLASS_REMESH,
                             nodeout, xout, pos,
                             nodeVel, newVel,
                             armCount, armList,
                             globalOp, &splitNode1,
                             &splitNode2, 0 );
    
    if (splitStatus == SPLIT_SUCCESS) 
    {
        splitNode1->fX = nodeout->fX;
        splitNode1->fY = nodeout->fY;
        splitNode1->fZ = nodeout->fZ;
        
        splitNode2->fX = nodeout->fX;
        splitNode2->fY = nodeout->fY;
        splitNode2->fZ = nodeout->fZ;
        
        
        MarkNodeForceObsolete(home, splitNode1);
        MarkNodeForceObsolete(home, splitNode2);
        MarkNodeForceObsolete(home, nodein);
        
        if (nodeout->constraint == 7)
        { splitNode1->constraint = 7; // split node on the surface
            splitNode2->constraint = 7; // old nodeout, outside halfspace   
        }
        else
        {
            splitNode1->constraint = 6; // split node on the surface
            splitNode2->constraint = 6; // old nodeout, outside halfspace
        }
    }
    
    return splitStatus;
}

void GetSurfaceNode(Param_t *param,Node_t *nodeout,Node_t *nodein,real8 pos[3],
                    real8 t)
{
    // Find the point on the surface between node out and in.
    // out is the node to be modified
    
    real8 xout = nodeout->x;
    real8 yout = nodeout->y;
    real8 zout = nodeout->z;
    
    real8 xin = nodein->x;
    real8 yin = nodein->y;
    real8 zin = nodein->z;
    
    PBCPOSITION(param,xout,yout,zout,&xin,&yin,&zin);
    
    if ( fabs(zin-zout) < 1.e-3)
    { // already at the surface or points parallel to the surface
        pos[0] = xout;
        pos[1] = yout;
    }
    else
    {
        real8 q,u,v;
        q = (t - zout)/(zin - zout);
        u = xout + (xin - xout)*q;
        v = yout + (yin - yout)*q;
        
        pos[0] = u; 
        pos[1] = v; 
    }
    
    pos[2] = t;
}

#endif
